/*! ajax.js v1.0
	(c) 2016
	@utor: Gonzalo Mamani Guachalla
	email:gmg.software.developer@gmail.com
*/
var base;
function init(b){
	base=b;
}
function get(ruta,atrib,id,control){//con mensaje de inicio
  $.ajax({
  	url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:18000000,
    beforeSend: function(){ if(control){ inicioEnvio(id)}},
    success: function(result){llegada(id,result)},
    error:function(){problemas(id)}
  }); 
  return false;
}
function get_2n(ruta1,atrib1,id1,control1,ruta2,atrib2,id2,control2){
	
  if(ruta1!=""){
		$.ajax({
		    url:ruta1,
		    async:true,
		    type: "POST",
		    contentType:false,
		    data:atrib1,
		    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
		    timeout:18000000,
		    beforeSend: function(){ if(control1){ inicioEnvio(id1);}},
		    success: function(result){llegada_2n(id1,result,ruta2,atrib2,id2,control2)},
		    error: function(){problemas(id1)}
		});
	}else{
    $("#"+id1).html("");
		get(ruta2,atrib2,id2,control2);
	}
  return false;
}
function get_3n(ruta1,atrib1,id1,control1,ruta2,atrib2,id2,control2,ruta3,atrib3,id3,control3){
  $.ajax({
    url:ruta1,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib1,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:18000000,
    beforeSend: function(){ if(control1){ inicioEnvio(id1);}},
    success: function(result){llegada_3n(id1,result,ruta2,atrib2,id2,control2,ruta3,atrib3,id3,control3)},
    error:function(){problemas(id1)}
  });
  return false;
}
function get_input(ruta,atrib,id,inputs,control){//con mensaje de inicio
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:18000000,
    beforeSend: function(){ if(control){ inicioEnvio(id)}},
    success: function(result){llegada_input(inputs,result,true)},
    error:function(){problemas(id)}
  }); 
  return false;
}
function get_input_v2(ruta,atrib,id,inputs,control){//con mensaje de inicio
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:18000000,
    beforeSend: function(){ if(control){ inicioEnvio(id)}},
    success: function(result){ llegada_input(inputs,result,false)},
    error:function(){problemas(id)}
  }); 
  return false;
}
function get_append(ruta,atrib,id,control){
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:18000000,
    beforeSend: function(){ if(control){ inicioEnvio(id)} },
    success: function(result){ llegada_append(id,result);},
    error:function(){ problemas(id)}
  }); 
  return false;
}
function get_append_disabled(ruta,atrib,id,control,ele){// desactiva un elemeto mientras carga la respuesta
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:18000000,
    beforeSend: function(){ $(ele).attr("disabled","disabled");if(control){ inicioEnvio(id)} },
    success: function(result){  $(ele).removeAttr("disabled"); llegada_append(id,result)},
    error:function(){  $(ele).removeAttr("disabled"); problemas(id)}
  }); 
  return false;
}
function get_array(ruta,atrib,array,control){
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:18000000,
    beforeSend: function(){ if(control){ inicioEnvio("")} },
    success: function(results){ llegada_array(array,results)},
    error:function(){ problemas("")}
  }); 
  return false;
}

function inicioEnvio(div){$("#"+div).html("<img src='"+base+"libraries/img/sistema/load.gif'>");}
function llegada(div,result){ 
	if(result!="logout"){ 
		$("#"+div).html(result);
		try{
			refresh_totales_compras();//solo para el caso de actualizar compras (nuevo y modificar);
		}catch(e){}
		try{
			refresh_totales_cuentas();//solo para el caso de actualizar cuenta (nuevo y modificar);
		}catch(e){}
	}else{
		alert("Su Session a expirado, inicie session por favor"); 
		window.location.href=base;
	}
}
function llegada_2n(div,result,ruta,atrib,id,control){
	if(result!="logout"){ 
		$("#"+div).html(result);
		get(ruta,atrib,id,control);
	}else{
		alert("Su Session a expirado, inicie session por favor"); 
		window.location.href=base;
	}
}
function llegada_3n(div,result,ruta1,atrib1,id1,control1,ruta2,atrib2,id2,control2){
	if(result!="logout"){ 
		$("#"+div).html(result);
		get_2n(ruta1,atrib1,id1,control1,ruta2,atrib2,id2,control2);
	}else{
		alert("Su Session a expirado, inicie session por favor"); 
		window.location.href=base;
	}
}
function llegada_input(inputs,result,disabled){ 
	var inp=inputs.split("|");
	if(result!="logout"){ 
		if(result!="fail"){ 
			if(result!=""){
				var res=result.split("|");
				for(var i=0;i<res.length;i++){
					var ele=$("#"+inp[i]);
					//console.log(result+" "+inp[i]+" "+ele.prop("tagName"));
					if(ele.prop("tagName")=='INPUT' || ele[0].tagName=='TEXTAREA'){
						ele.val(res[i]);
					}else{
						if(ele.prop("tagName")=='SELECT'){
							ele.html("");
							ele.html(res[i]);
						}else{
							ele.html("");
							ele.html(res[i]);
						}
					}
					if(disabled){ ele.attr({'disabled':'disabled'});}
				}
			}else{
				for(var i=0;i<inp.length;i++){
					var ele=$("#"+inp[i]);
					if(disabled){ ele.removeAttr('disabled');}
					if((ele[0].tagName).toUpperCase()=='INPUT' || (ele[0].tagName).toUpperCase()=='TEXTAREA'){
						ele.val("");
					}else{
						if((ele[0].tagName).toUpperCase()=='SELECT'){
							//ele.html("");
							//console.log("SELECTD");
						}
						else{
							ele.html("...");
						}
					}
				}
			}
			try{
				refresh_totales_compras();//solo para el caso de actualizar compras (nuevo y modificar);
			}catch(e){}
			try{
				refresh_totales_cuentas();//solo para el caso de actualizar cuenta (nuevo y modificar);
			}catch(e){}
		}else{
			msj(result);
		}

	}else{
		alert("Su Session a expirado, inicie session por favor"); 
		window.location.href=base;
	}
}
function llegada_append(div,result){
	if(result!="logout"){
		if(result!="fail"){
			$("#"+div).append(result);
			try{
				refresh_totales_compras();//solo para el caso de actualizar compras (nuevo y modificar);
			}catch(e){}
			try{
				refresh_totales_cuentas();//solo para el caso de actualizar cuenta (nuevo y modificar);
			}catch(e){}
		}else{
			msj(result)
		}
	}else{
		alert("Su Session a expirado, inicie session por favor"); 
		window.location.href=base;
	}
}
function llegada_array(array,results){
	if(results!="logout"){
		if(results!="fail"){
			var vt=(array.types).split("|");
			var vd=(array.divs).split("|");
			var vr=results.split("|");
			if(vt.length==vd.length && vd.length==vr.length){
				for(var i = 0; i < vt.length; i++){
					if(vt[i]=="div"){ $("#"+vd[i]).html(vr[i]); }
					if(vt[i]=="input"){ $("#"+vd[i]).val(vr[i]); }
				}
			}else{
				msj("fail "+vt.length+" "+vd.length+" "+vr.length);
			}
		}else{
			msj(results);
		}
	}else{
		alert("Su Session a expirado, inicie session por favor"); 
		window.location.href=base;
	}
}
function problemas(div){ msj("problem");}

/*--- funciones de set de datos ---*/
function set(ruta,atrib,id,control,ruta2,atrib2,id2,control2,id_closed){
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:18000000,
    beforeSend: function(){ if(control){ msj('load');}},
    success: function(result){ llegada_set(result,ruta2,atrib2,id2,control2,id_closed)},
    error:function(){problemas(id)}
  }); 
}
function set_disabled(ruta,atrib,id,control,ruta2,atrib2,id2,control2,id_closed,ele){//decativa el elemento mientras se carag
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:18000000,
    beforeSend: function(){ ele.attr("disabled","disabled"); if(control){ msj('load');}},
    success: function(result){ ele.removeAttr("disabled");llegada_set(result,ruta2,atrib2,id2,control2,id_closed)},
    error:function(){ ele.removeAttr("disabled"); problemas(id);}
  }); 
}
function llegada_set(result,ruta,atrib,id,control,id_closed){
  switch(result){
    case "ok": msj(result); if(id_closed!="NULL"){ hide_modal(id_closed);}; if(ruta!=''){ get(ruta,atrib,id,control);}break;
    case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base; break;
    default: msj(result); break;
  } 
}
function llegada_set(result,ruta,atrib,id,control,id_closed){
  switch(result){
    case "ok": msj(result); if(id_closed!="NULL"){ hide_modal(id_closed);}; if(ruta!=''){ get(ruta,atrib,id,control);}break;
    case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base; break;
    default: msj(result); break;
  } 
}
function set_2n(ruta,atrib,id,control,ruta2,atrib2,id2,control2,ruta3,atrib3,id3,control3,id_closed){
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:18000000,
    beforeSend: function(){ if(control){ msj('load');}},
    success: function(result){ llegada_2n_set(result,ruta2,atrib2,id2,control2,ruta3,atrib3,id3,control3,id_closed)},
    error:function(){problemas(id)}
  }); 
}
function llegada_2n_set(result,ruta,atrib,id,control,ruta2,atrib2,id2,control2,id_closed){
  switch(result){
    case "ok": msj(result); if(id!="NULL"){ hide_modal(id_closed);}; if(ruta!=''){ get_2n(ruta,atrib,id,control,ruta2,atrib2,id2,control2);}break;
    case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base; break;
    default: msj(result); break;
  } 
}
function set_3n(ruta,atrib,id,control,ruta2,atrib2,id2,control2,ruta3,atrib3,id3,control3,ruta4,atrib4,id4,control4,id_closed){
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:18000000,
    beforeSend: function(){ if(control){ msj('load');}},
    success: function(result){ llegada_3n_set(result,ruta2,atrib2,id2,control2,ruta3,atrib3,id3,control3,ruta4,atrib4,id4,control4,id_closed)},
    error:function(){problemas(id)}
  }); 
}
function llegada_3n_set(result,ruta,atrib,id,control,ruta2,atrib2,id2,control2,ruta3,atrib3,id3,control3,id_closed){
  switch(result){
    case "ok": msj(result); if(id!="NULL"){ hide_modal(id_closed);}; if(ruta!=''){ get_3n(ruta,atrib,id,control,ruta2,atrib2,id2,control2,ruta3,atrib3,id3,control3);}break;
    case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base; break;
    default: msj(result); break;
  } 
}
/*---- PARA BOTON CIRCULAR ----*/
function set_button(ruta,atrib,ruta2,atrib2,id2,control,ele,color,diametro){
  $.post(ruta,atrib,function(v_resp){
        resp=v_resp.split("|");
        //alert(v_resp);
        switch(resp[1]){
          case "ok": if(resp[0]==0){color='default';} color_button(ele,color,diametro); if(ruta2!=""){ get(ruta2,atrib2,id2,control);} break;
          case "error": msj(resp[1]); break;
          case "fail": msj(resp[1]);   break;
          case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base;  break;
          default: msj('default'); $("#alerta_text").text(resp[1]);
        }
    });
}
/*---- END PARA BOTON CIRCULAR ----*/
function set_id(ruta,atrib,id,control){// en uso movimiento de materiales
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:18000000,
    beforeSend: function(){ if(control){ msj('load');}},
    success: function(result){ var res=result.split("|"); llegada_set_id(res[0],res[1],id);},
    error:function(){problemas(id)}
  }); 
}
function llegada_set_id(result,valor,id) {
	switch(result){
	    case "ok": msj(result); reset_rev_mov(valor,id); break;
	    case "error": msj(result); break;
	    case "fail": msj(result); break;
	    case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base; break;
    default: msj('default'); $("#alerta_text").text(result); break;
  }
}
function form_cambiar_password(ide){
    modal('USUARIO: Cambiar contraseña','xs','1');
    btn_modal('cambiar_password',"'"+ide+"'",'',"",'1','xs');
    var atrib=new FormData();atrib.append('ide',ide);
    get(base+'login/form_cambiar_password',atrib,'content_1',true);
}
function cambiar_password(ide){
  var pass=$("#e_pass").val();
  var nuevo=$("#new_pass").val();
  var nuevo2=$("#rep_pass").val();
  if(pass!=""){
    if(password(nuevo,4,25)){
      if(nuevo==nuevo2){
        var atrib=new FormData();atrib.append('ide',ide);atrib.append('pass',pass);atrib.append('nuevo',nuevo);atrib.append('nuevo2',nuevo2);
        set(base+'login/cambiar_password',atrib,"",true,"",{},"","","1");
      }else{
        alerta("Las contraseñas no son iguales","top","new_pass");
        alerta("Las contraseñas no son iguales","top","rep_pass");
      }
    }else{
      alerta("Ingrese una contraseña válida","top","new_pass");
    }
  }else{
    alerta("Ingrese una contraseña","top","e_pass");
  }
}