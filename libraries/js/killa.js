$(function() {
    $("#side-menu").MenuIzq();
});
(function($){
    var nombrePluging="MenuIzq";
        defaults={toggle:true};
        function Menu(elemento,opciones){
            this.elemento=elemento;
            this.settings=$.extend({},defaults,opciones);
            this.inicio();
        }
        Menu.prototype={
            inicio: function(){
                var $this=$(this.elemento);
                var $toggle=this.settings.toggle;
                $this.find("li.active").has("ul").children("ul").addClass("collapse in");
                $this.find("li").not(".active").has("ul").children("ul").addClass("collapse");
                $this.find("li").has("ul").children("a").on("click", function (e) {
                    e.preventDefault();
                    $(this).parent("li").toggleClass("active").children("ul").collapse("toggle");
                    if ($toggle) {
                        $(this).parent("li").siblings().removeClass("active").children("ul.in").collapse("hide");
                    }
                });

            },
        };  
    $.fn[nombrePluging]=function(opciones){
        return this.each(function(){  
            if (!$.data(this, "plugin_" + nombrePluging)) {
                $.data(this, "plugin_" + nombrePluging, new Menu(this, opciones));
            }
        });
    }
})(jQuery);



var base;
function init(b){
	base=b;
  control_modal();
}
function control_modal(){
  $("#modal_closed_2").click(function() { hide_modal("2");});
  $(".closed_2").click(function() { hide_modal("2");});
  $(document).click(function(e){ if($(e.target).attr("id")=="modal_2"){ hide_modal("2");} });

  $("#modal_closed_3").click(function() { hide_modal("3");});
  $(".closed_3").click(function() { hide_modal("3");});
  $(document).click(function(e){ if($(e.target).attr("id")=="modal_3"){ hide_modal("3");} });

  $("#modal_closed_4").click(function() { hide_modal("4");});
  $(".closed_4").click(function() { hide_modal("4");});
  $(document).click(function(e){ if($(e.target).attr("id")=="modal_4"){ hide_modal("4");} });

  $("#modal_closed_5").click(function() { hide_modal("5");});
  $(".closed_5").click(function() { hide_modal("5");});
  $(document).click(function(e){ if($(e.target).attr("id")=="modal_5"){ hide_modal("5");} });
}

function ajax_respuesta(ruta,atrib,id){
  var bool="fail";
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:9000,
    beforeSend: function(){},
    success: function(result){ return result;},
    error:function(bool){ return bool;}
  }); 
}
function ajax_get_value(ruta,atrib,id){
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:9000,
    beforeSend: function(){ inicioEnvio_value(id)},
    success: function(result){llegada_value(id,result)},
    error:function(){problemas_value(id)}
  }); 
  return false;
}
function inicioEnvio_value(id){$("#"+id).val('Cargando...');}
function llegada_value(id,result){
    if(result!="logout"){
      $("#"+id).val(result);
      if(result!=""){ $("#"+id).attr({"disabled":"disabled"});}else{ $("#"+id).removeAttr("disabled");}
    }else{
      alert("Su Session a expirado, inicie session por favor"); window.location.href=base;
    }
}
function problemas_value(id){$("#"+id).val("Problemas con la conexion");}

function ajax_get_esp(ruta,atrib,id0,id1,id2,id3,id4){//AJAX ESPECIAL 
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:9000,
    beforeSend: function(){ },
    success: function(result){
      var res=result.split("|");
      if(res.length==5){
        $("#"+id0).val(res[0]);
        $("#"+id1).val(res[1]+"");
        $("#"+id2).val(res[2]+"");
        $("#"+id3).val(res[3]+"");
        $("#"+id4).val(res[4]+"");
      }else{
        ver_alerta("alerta_fail",result);
      }
    },
    error:function(){}
  }); 
  return false;
}

function ajax_get(ruta,atrib,id){
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:9000,
    beforeSend: function(){ inicioEnvio(id)},
    success: function(result){llegada(id,result)},
    error:function(){problemas(id)}
  }); 
  return false;
}
function ajax_get_append(ruta,atrib,id){
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:9000,
    beforeSend: function(){ },
    success: function(result){llegada_append(id,result)},
    error:function(){problemas(id)}
  }); 
  return false;
}


function ajax_get_2n(ruta1,atrib1,id1,ruta2,atrib2,id2){
  $.ajax({
    url:ruta1,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib1,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:9000,
    beforeSend: function(){ inicioEnvio(id1)},
    success: function(result){llegada_2n(id1,result,ruta2,atrib2,id2)},
    error:function(){problemas(id1)}
  });
  return false;
}

function inicioEnvio(div){$("#"+div).html('Cargando...');}
function llegada(div,result){$("#"+div).html(result);}
function llegada_append(div,result){$("#"+div).append(result);}
function problemas(div){$("#"+div).html("Problemas con la conexion");}
function llegada_2n(div,result,ruta,atrib,id){
    switch(result){
      case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base; break;
      default:$("#"+div).html(result); ajax_get(ruta,atrib,id);
    }
}
function post_set(ruta,atrib,ruta2,atrib2,id){
   $.post(ruta,atrib,function(resp){
        switch(resp){
          case "ok": ver_alerta("alerta_ok","Datos Actualizados con éxito !!!"); post_get(ruta2,atrib2,id);break;
          case "error": ver_alerta("alerta_error","Datos no actualizados, existio algunos problemas de conexion !!!"); break;
          case "fail": ver_alerta("alerta_fail","Error en la actualización, revise los datos porfavor..."); break;
          case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base; break;
          default: /*alert(msg);*/ver_alerta("alerta_error",resp);break;
        }
  });
}
function ajax_set(ruta,atrib,id,ruta2,atrib2,id2,control,id_closed){
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:240000,
    beforeSend: function(){ inicioEnvio(id)},
    success: function(result){ llegada_2n_set(result,ruta2,atrib2,id2,id_closed)},
    error:function(){problemas(id)}
  }); 
  return false;
}
function llegada_2n_set(result,ruta,atrib,id,id_closed){
  switch(result){
    case "ok": ver_alerta("alerta_ok","Datos Actualizados con éxito !!!"); if(id_closed!="NULL"){hide_modal(id_closed);}; if(ruta!=''){ajax_get(ruta,atrib,id);}break;
    case "error": ver_alerta("alerta_error","Datos no actualizados, existio algunos problemas de conexion !!!"); break;
    case "validate": ver_alerta("alerta_error","Datos no actualizados, usuario y contraseña incorrecta !!!"); break;
    case "numero_orden_exist": ver_alerta("alerta_fail","Datos no actualizados, Actualice Número de Orden !!!"); break;
    case "error_type": ver_alerta("alerta_error","Tipo de archivo no aceptado, verifique el(los) archivo(s)"); break;
    case "fail": ver_alerta("alerta_fail","Error en la actualización, revise los datos porfavor..."); break;
    case "exists": ver_alerta("alerta_fail","El NIT o CI ya se encuentra registrado en el sistema, revise los datos porfavor..."); break;
    case "exists_cuenta": ver_alerta("alerta_fail","El código de la cuenta ya existe"); break;//caso plan de cuentas
    case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base; break;
    default: /*ver_alerta("alerta_error",result)*/ver_modal("ERROR: Error",100,null,"1","b"); $("#content_1").html(result); break;
  } 
}

function post_set_button(ruta,atrib,ruta2,atrib2,id2,ele,color,diametro){
  $.post(ruta,atrib,function(v_resp){
        resp=v_resp.split("|");
        //alert(v_resp);
        switch(resp[1]){
          case "ok": if(resp[0]==0){color='default';} color_button(ele,color,diametro);if(ruta2!=""){post_get(ruta2,atrib2,id2);} break;
          case "error": ver_alerta("alerta_error","Error en la actualización","alert_info"); break;
          case "fail": ver_alerta("alerta_fail","Error en la actualización: Revise los datos por favor");   break;
          case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base;  break;
          default: ver_alerta("alerta_error",resp);
        }
    });
}
//COLOR CIRCULAR
/*cambia color circular*/
function color_button(ele,color,diametro){
  ele.className="";
  ele.className="btn-circle btn-circle-"+color+" btn-circle-"+diametro;
  //ele.innerHTML="<div class='btn-circle btn-circle-"+color+" btn-circle-"+diametro+"'></div>";
}
function ver_alerta(id,text){
  $("#"+id).removeAttr('style');
  $("#"+id+"_text").text(text);
  $("#"+id).fadeOut(8000);
}
function cerrar_alerta(id){
  $("#"+id).css({display:'none'});
}

function modal(encabezado,ancho,nivel,modal){
  $(".titulo_"+nivel).html(encabezado);
  $("#dialog_"+nivel).attr({"class":"modal-dialog modal-"+ancho});
  if(modal=="b"){
    $("#modal_"+nivel).modal({keyboard:true}); 
  }else{
    $("#modal_"+nivel).fadeIn(150);
    $("#dialog_"+nivel).fadeIn(0);
    $("#dialog_"+nivel).css({"transform":"translateY(0)","visibility":"visible"});

  }
}

function ver_modal(encabezado,ancho,alto,nivel,modal){
  $(".titulo_"+nivel).html(encabezado);
  if(modal=="b"){//modal bootstrap
      $(".dialogo_"+nivel).css({marginTop:"2px"});
      $(".dialogo_"+nivel).css({width:ancho+"%"}); $(".contenedor_"+nivel).css({width:ancho+"%"});
      $("#modal_"+nivel).modal({keyboard:true});    
  }else{//modal elaboracion propia
    $("#modal_"+nivel).fadeIn(500);
    var costado=(100-ancho)/2;
    $(".left_"+nivel).css({width:costado+"%"});
    $(".dialogo_"+nivel).css({width:ancho+"%"});
    $(".right_"+nivel).css({width:costado+"%"});
    $(".left_"+nivel).css({height:alto+"%"});
    $(".dialogo_"+nivel).css({height:alto+"%"});
    $(".right_"+nivel).css({height:alto+"%"});
    $(".footer_"+nivel).css({height:(100-alto)+"%"});
    $(".left_"+nivel).click(function(){ hide_modal(nivel); });
    $(".right_"+nivel).click(function(){ hide_modal(nivel); });
    $(".footer_"+nivel).click(function(){ hide_modal(nivel); });
    $(".dialogo_"+nivel).fadeIn(500);
    //hallando porcentaje
    var por=porcentaje(55,'h');
    var he=pixel(alto-(por*2),'h');
    $("#content_"+nivel).css({height:he+"px"});
  }
}
function confirm_modal(nombre,imagen){
  ver_modal("¿Desea Eliminar: "+nombre+"?",45,70,"5","p");
  addImg('img_logout',imagen);
  $("#e_user").focus();
}

function pixel(por,lado){
  var res="";
  if(lado=="h"){
    res=(por*$(document).height())/100;
  }
  if(lado=="w"){
    res=(por*$(document).width())/100;
  }
  return res;
}
function porcentaje(pixel,lado){
  var res="";
  if(lado=="h"){
    res=(pixel*100)/$(document).height();
  }
  if(lado=="w"){
    res=(pixel*100)/$(document).width();
  }
  return res;
}
function button_modal(id_g,funcion_g,atribs_g,id_i,funcion_i,atribs_i,id_e){
  if(funcion_g!=""){
    if(funcion_i!=""){
        $("#"+id_g).attr({"class":"col-xs-2 btn btn-success col-xs-offset-3"});
        $("#"+id_i).css({marginLeft:"5px"});
        $("#"+id_i).attr({"class":"col-xs-2 btn btn-info"});
        $("#"+id_e).css({marginLeft:"5px"});
        $("#"+id_e).attr({"class":"btn btn-danger col-xs-2"});
        addClick_v2(id_g,funcion_g,atribs_g);
        addClick_v2(id_i,funcion_g,atribs_i);
    }else{
        $("#"+id_g).attr({"class":"col-xs-2 btn btn-success col-xs-offset-4"});
        $("#"+id_i).removeAttr("style");
        $("#"+id_i).attr({"class":"hidden"});
        $("#"+id_e).css({marginLeft:"5px"});
        $("#"+id_e).attr({"class":"btn btn-danger col-xs-2"});  
        addClick_v2(id_g,funcion_g,atribs_g);    
    }
  }else{
    if(funcion_i!=""){
        $("#"+id_g).attr({"class":"hidden"});
        $("#"+id_i).removeAttr("style");
        $("#"+id_i).attr({"class":"col-xs-2 btn btn-info col-xs-offset-4"});
        $("#"+id_e).css({marginLeft:"5px"});
        $("#"+id_e).attr({"class":"btn btn-danger col-xs-2"});
        addClick_v2(id_i,funcion_i,atribs_i);  
    }else{
        $("#"+id_g).attr({"class":"hidden"});
        $("#"+id_i).removeAttr("style");
        $("#"+id_i).attr({"class":"hidden"});
        $("#"+id_e).removeAttr("style");
        $("#"+id_e).attr({"class":"btn btn-danger col-xs-2 col-xs-offset-5"});      
    }
  }
}




function hide_modal(nivel){//cerrar modal bootstrap
  switch(nivel){
    case "1": $("#modal_"+nivel).modal('hide'); break;
    default: $("#modal_"+nivel).fadeOut(250);$("#dialog_"+nivel).fadeOut(150); $("#dialog_"+nivel).css("transform","translateY(-200%)"); break;
  }
}

function addImg(id,imag){
  var img=document.getElementById(id);
 if(imag!="" && imag!=null){ img.src=base+imag; }
}
function addClick_v2(id,funcion,atribs){
  var ele=$("#"+id);
  if(funcion!=""){
    ele.attr({"onclick":funcion+"("+atribs+")"});
  }else{
    ele.attr({"onclick":""});
  }
}

function visible_2(contenedor,sub_contenedor){
  $("#"+contenedor).fadeIn(10);
  $("#"+sub_contenedor).fadeIn(500);
  if(sub_contenedor=='logout'){
    $("#e_user").val("");$("#e_user").focus();
    $("#e_password").val("");
  }
}
function no_visible_2(contenedor,sub_contenedor){
  $("#"+contenedor).fadeOut(600);
  $("#"+sub_contenedor).fadeOut(400);
}

//guardar:obtener id y col
function ajax_get_id(ruta,atrib,id){
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:9000,
    success: function(result){llegada_elemento(id,result)},
    error:function(){llegada_elemento(id,"error|Error en la conexión")}
  }); 
  return false;
}

function llegada_elemento(id,result){
  var v=result.split("|");
  $("#"+id).attr('name',v[0]);
  $("#"+id).val(v[1]);
}

function post_set_id(ruta,atrib,id){// en uso movimiento de materiales
  $.post(ruta,atrib,function(resp){
        var respuesta=resp.split("|");
        switch(respuesta[0]){
          case "ok": msj("Base de datos Actualizada...","alert_ok","modal_alert"); ver_alerta("alerta_ok","Datos Actualizados con éxito !!!"); reset_rev_mov(respuesta[1],id); break;
          case "error": msj("Error en la actualización","alert_info","modal_alert"); break;
          case "fail": msj("Error en la actualización: Revise los datos por favor","alert_error","modal_alert"); break;
          case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base;  break;
          case "N/M": msj("Fallas de session...","alert_error","modal_alert"); window.location.href=base;  break;
          default: alert(resp);
      } 
  });
}






function activar(activo){
  var padre=document.getElementById("menu-top").firstChild;//obtenemos todos los elemntos <li>
  while(padre!=null){//recorremos todos los <li>
    if(padre.nodeType==Node.ELEMENT_NODE){
        var hijo=padre.firstChild;
        if(hijo.id==activo){
          padre.className="activado";
        }else{
          padre.className="";
        }
      }
      padre=padre.nextSibling;
  }
}




function visible(contenedor,action,marTop,ancho,alto){
	var e=document.getElementById(contenedor);
	e.style.visibility="visible";
	var a=document.getElementById(action);
	a.style.visibility="visible";
	a.style.padding="1%";
  a.style.width=ancho;
	a.style.height=alto;
	a.style.marginTop=marTop;
  if(action=="modal"){
    var a=document.getElementById("modal_herramienta");
    a.style.padding="1%";
    a.style.width=ancho;
  }
}

function view_modal(nivel,top,ancho,encabezado){
    if (top!='') {$(".dialogo_"+nivel).css({marginTop:top});};
    if(ancho!=''){ $(".dialogo_"+nivel).css({width:ancho}); $(".contenedor_"+nivel).css({width:ancho})}
    $("#modal_"+nivel).modal({keyboard:true});
    $(".titulo_"+nivel).html(encabezado);    
}

function no_visible(contenedor,action){
	var e=document.getElementById(contenedor);
	e.style.visibility="hidden";
	var a=document.getElementById(action);
	a.style.visibility="hidden";
	a.style.padding="0%";
	a.style.height="0px";
	a.style.marginTop="0%";
}
function visible_logout(action,alto,id){
	var e=document.getElementById("modal_alert");
	e.style.visibility="visible";
	var a=document.getElementById(action);
	a.style.visibility="visible";
	a.style.padding="1%";
	a.style.height="auto";
	a.style.marginTop="3%";  
}

function closed_modal(ruta,atrib,destino,contenedor,modal){
  no_visible(contenedor,modal);
  post_get(ruta,atrib,destino);
}

function cargar(ruta,content,ruta2,result){
	$.post(ruta,function(resp){
		if(resp!="N/M"){
			$("#"+content).html(resp);
	     if(ruta2!=""){
	       post_get(ruta2,{},result);
	     }
	   }else{
	   	window.location.href=base;// en caso de error de numero de id de material 
	   } 
    });
}

function post_get(ruta,atrib,id){
  $.post(ruta,atrib,function(resp){
  	if(resp!="logout"){
      $("#"+id).html(resp);
  	}else{
  		alert("Su Session a expirado, inicie session por favor"); 
  		window.location.href=base; 
  	}
  });
}
function post_get_2n(ruta1,atrib1,id1,ruta2,atrib2,id2){
  $.post(ruta1,atrib1,function(resp){
    if(resp!="logout"){
      $("#"+id1).html(resp);
       if(ruta2!=""){
         post_get(ruta2,atrib2,id2);
       }
    }else{
      alert("Su Session a expirado, inicie session por favor"); 
      window.location.href=base; 
    }
  });
}

function get2N(ruta1,atrib1,id1,ruta2,atrib2,id2){
  $.post(ruta1,atrib1,function(resp){
      $("#"+id1).html(resp);
      if(ruta2!=""){
         post_get(ruta2,atrib2,id2);
      }
    });
}

function redirect(ruta,atrib,t){
  $.post(ruta,atrib,function(resp){
  	switch (resp){
  		case "ok": window.location.href=base+ruta; break;
      case "error": msj("Error en la redireccion","alert_error","modal_alert"); window.location.href=base+origen; break;
      case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base;  break;
      default: alert(resp);
  	}
  });
}


function post_set_sm(ruta,atrib,ruta2,atrib2,id2,ruta3,atrib3,id3){
   $.post(ruta,atrib,function(resp){
        switch(resp){
          case "ok": get2N(ruta2,atrib2,id2,ruta3,atrib3,id3); break;
          case "error": msj("Error en la actualización","alert_info","modal_alert"); break;
          case "fail": msj("Error en la actualización: Revise los datos por favor","alert_error","modal_alert");   break;
          case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base;  break;
          default: alert(resp);
        }
    });
}


function post_delete(ruta,atrib,ruta2,atrib2,id){

   $.post(ruta,atrib,function(resp){
        switch(resp){
          case "ok": msj("Datos Eliminados Correctamente...","alert_ok","modal_alert"); post_get(ruta2,atrib2,id); break;
          case "error": msj("Error en eliminacion","alert_info","modal_alert"); break;
          case "fail": msj("!!!Error: actualice la pagina","alert_error","modal_alert");  break;
          case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base;  break;
          case "N/M": msj("Fallas de session...","alert_error","modal_alert"); window.location.href=base;  break;
          default: alert(resp);break;
        }
  });
}

function post_delete_validar(rutaV,atribV,ruta,atrib,ruta2,atrib2,id){
   $.post(rutaV,atribV,function(resp){
        switch(resp){
          case "exist": no_visible('modal_alert','logout'); post_delete(ruta,atrib,ruta2,atrib2,id); break;
          case "none": alert("Imposible eliminar,datos incorrectos..."); break;
          case "error": alert("Imposible eliminar, error en eliminacion"); break;
          case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base;  break;
          default: alert(resp);break; 
        }
  });
}



function set3N(ruta1,atrib1,ruta2,atrib2,id1,ruta3,atrib3,id2){
	$.post(ruta1,atrib1,function(resp){
        switch(resp){
          case "ok": msj("Base de datos Actualizada...","alert_ok","modal_alert"); get2N(ruta2,atrib2,id1,ruta3,atrib3,id2); break;
          case "error": msj("Error en la actualización","alert_info","modal_alert"); break;
          case "fail": msj("Error en la actualización: Revise los datos por favor","alert_error","modal_alert");   break;
          case "exist_ci": msj("Error, el número de carnet ya se encuentra registrado","alert_info","modal_alert"); break;
          case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base;  break;
          case "N/M": msj("Fallas de session...","alert_error","modal_alert"); window.location.href=base;  break;//solo en caso de almacenes
          default: alert(resp); break;
        }
  	});
}

function set3N_fotos(ruta1,atrib1,ruta2,atrib2,id1,ruta3,atrib3,id2,files){
	$.post(ruta1,atrib1,function(resp){
        switch(resp){
          case "ok": msj("Base de datos Actualizada...","alert_ok","modal_alert"); get2N(ruta2,atrib2,id1,ruta3,atrib3,id2); break;
          case "error": msj("Error en la actualización","alert_info","modal_alert"); break;
          case "fail": msj("Error en la actualización: Revise los datos por favor","alert_error","modal_alert");   break;
          case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base;  break;
          case "N/M": msj("Fallas de session...","alert_error","modal_alert"); window.location.href=base;  break;//solo en caso de almacenes
        }
  	});
}



function ajax_file(ruta2,atrib2,id,rutaArchivo,archivos){
	$.ajax({
			url:rutaArchivo, //Url a donde la enviaremos
			type:'POST', //Metodo que usaremos
			contentType:false, //Debe estar en false para que pase el objeto sin procesar
			data:archivos, //Le pasamos el objeto que creamos con los archivos
			processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
			cache:false //Para que el formulario no guarde cache
		}).done(function(msg){//Escuchamos la respuesta y capturamos el mensaje msg
      /* visible('content_modal','modal','2%','60%','74%');
      $("#modal").html(msg);*/
      switch(msg){
        case "ok": msj("Datos Actualizados con éxito","alert_ok","modal_alert"); post_get(ruta2,atrib2,id);break;
        case "error": msj("0 archivos subidos","alert_error","modal_alert"); break;
        case "error_type": msj("Tipo de archivo no aceptado, verifique el(los) archivo(s)","alert_error","modal_alert"); break;
        case "fail": msj("Error en la actualización, revise los datos porfavor...","alert_error","modal_alert"); break;
        case "exist": msj("Algunos archivos ya existen en el servidor, Cambie los nombres de las imagenes por favor.","alert_info","modal_alert"); break;
        case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base; break;
        case "N/M": msj("Fallas en session no se encontro almacen...","alert_error","modal_alert"); window.location.href=base;  break;//solo en caso de almacenes
        default: /*alert(msg);*/visible('content_modal','modal','2%','60%','74%'); $("#modal").html(msg); break;
      } 
		});
}
function ajax_file_two(ruta2,id,rutaArchivo,archivos){//en caso de que el controlador devuelva una parametro
  $.ajax({
      url:rutaArchivo, //Url a donde la enviaremos
      type:'POST', //Metodo que usaremos
      contentType:false, //Debe estar en false para que pase el objeto sin procesar
      data:archivos, //Le pasamos el objeto que creamos con los archivos
      processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
      cache:true
    }).done(function(msg){//Escuchamos la respuesta y capturamos el mensaje msg
      /*Formato de respuesta resp|msg*/
      var v=msg.split("|");
      switch(v[1]){
        case "ok": msj("Datos Actualizados con éxito","alert_ok","modal_alert"); post_get(ruta2,{id:v[0]},id);break;
        case "error": msj("0 archivos subidos","alert_error","modal_alert"); break;
        case "error_type": msj("Tipo de archivo no aceptado, verifique el(los) archivo(s)","alert_error","modal_alert"); break;
        case "fail": msj("Error en la actualización, revise los datos porfavor...","alert_error","modal_alert"); break;
        case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base; break;
        default: alert(msg);/*visible('content_modal','modal','2%','60%','74%'); $("#modal").html(msg);*/ break;
      } 
    });
}

function ajax_file_3n(ruta1,atrib1,id1,ruta2,atrib2,id2,ruta3,atrib3,id3){
  if(ruta1!='' & atrib1!=''){
    $.ajax({
      url:ruta1, //Url a donde la enviaremos
      type:'POST', //Metodo que usaremos
      contentType:false, //Debe estar en false para que pase el objeto sin procesar
      data:atrib1, //Le pasamos el objeto que creamos con los archivos
      processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
      cache:true
    }).done(function(msg){//Escuchamos la respuesta y capturamos el mensaje msg
      /*Formato de respuesta resp|msg*/
      var v=msg.split("|");
      switch(v[1]){
        case "ok": msj("Datos Actualizados con éxito","alert_ok","modal_alert"); if(v[0]!=""){get2N(ruta2,{id:v[0]},id2,ruta3,atrib3,id3);}else{get2N(ruta2,atrib2,id2,ruta3,atrib3,id3);} break;
        case "error": msj("0 archivos subidos","alert_error","modal_alert"); break;
        case "error_type": msj("Tipo de archivo no aceptado, verifique el(los) archivo(s)","alert_error","modal_alert"); break;
        case "fail": msj("Error en la actualización, revise los datos porfavor...","alert_error","modal_alert"); break;
        case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base; break;
        default: /*alert(msg);*/visible('content_modal','modal','2%','60%','74%'); $("#modal").html(msg); break;
      } 
    });
  }
}

function ajax_file_3Niveles(ruta1,atrib1,ruta2,atrib2,id2,ruta3,atrib3,id3){
  if(ruta1!='' & atrib1!=''){
    $.ajax({
      url:ruta1, //Url a donde la enviaremos
      type:'POST', //Metodo que usaremos
      contentType:false, //Debe estar en false para que pase el objeto sin procesar
      data:atrib1, //Le pasamos el objeto que creamos con los archivos
      processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
      cache:true
    }).done(function(msg){//Escuchamos la respuesta y capturamos el mensaje msg
      /*Formato de respuesta resp|msg*/
      var v=msg.split("|");
      switch(v[1]){
        case "ok": msj("Datos Actualizados con éxito","alert_ok","modal_alert"); get2N(ruta2,{id:v[0]},id2,ruta3,atrib3,id3);break;
        case "error": msj("0 archivos subidos","alert_error","modal_alert"); break;
        case "error_type": msj("Tipo de archivo no aceptado, verifique el(los) archivo(s)","alert_error","modal_alert"); break;
        case "fail": msj("Error en la actualización, revise los datos porfavor...","alert_error","modal_alert"); break;
        case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base; break;
        default: /*alert(msg);*/visible('content_modal','modal','2%','60%','74%'); $("#modal").html(msg); break;
      } 
    });
  }
}
function esNumero(nro){
	if(!isNaN(nro)  & nro != null & !/^\s*$/.test(nro) & nro>=0)
		return true;
			return false;
}
function esCadena(cad){
	if(cad == null || cad.length == 0 || /^\s*$/.test(cad))
		return false;
			return true;
}
function esFecha(fecha){
  if(fecha.length>0 & fecha.indexOf("-")>-1){
    var vf=fecha.split("-");
    if(vf[0]>1900 & vf[0]<2500 & vf[1]>0 & vf[1]<13 & vf[2]>0 & vf[2]<32){
      return true;
    }else{
      return false;
    }
  }else{
    return false;
  }
}
function msj(msj,action,contenedor){
	$("#"+action+"_text").html(msj);
	visible(contenedor,action,'5%','30%','auto');
}
function alerta(content,align,id){
  $("#"+id).attr({'data-trigger':'focus','data-placement':align,'data-content':content});
  $('#'+id).popover('show');
}
function closed_alert(obj){
  //$("#"+obj.id).attr({'data-trigger':'','data-placement':'','data-content':''});
  $("#"+obj.id).removeAttr('data-trigger');
  $("#"+obj.id).removeAttr('data-placement');
  $("#"+obj.id).removeAttr('data-content');
  $("#"+obj.id).removeAttr('data-original-title');
  $('#'+obj.id).popover('hide');
}
function imprimir(id){
  /*var objeto=document.getElementById(id);  //obtenemos el objeto a imprimir
  var ventana=window.open('','');  //abrimos una ventana vacía nueva
  ventana.document.write(objeto.innerHTML);  //imprimimos el HTML del objeto en la nueva ventana
  //ventana.document.close();  //cerramos el documento
   //cerramos la ventana
  // setTimeout(function () { ventana.print();  ventana.close();}, 500);
  /*var ficha=document.getElementById(id);
  var ventimp=window.open(' ','popimpr');
  ventimp.document.write(ficha.innerHTML);
  ventimp.document.close();
 */
  var contents = $("#"+id).html();
 var css='<link rel="stylesheet" type="text/css" href="'+base+'libraries/css/print.css" media="print">';
  var frame1 = $('<iframe />');
  frame1[0].name = "frame1";
  frame1.css({ "position": "absolute", "top": "-1000000px" });
  $("body").append(frame1);
  var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
  frameDoc.document.open();
  
  frameDoc.document.write('<html><head>'+css+'</head><body>');
  frameDoc.document.write(contents);
  frameDoc.document.write('</body></html>');
  frameDoc.document.close();
  setTimeout(function () { window.frames["frame1"].focus(); window.frames["frame1"].print(); frame1.remove();}, 600);
}



function cerrar_modal(contenedor,div){
  no_visible(contenedor,div);
  if(contenedor=='content_modal',div=='modal'){//es modal datos,borramos la accion del boton
    addClick_v2('modal_ok','',"");
  }
}
//para caso capital_humano,salarios
function descarga_biometrico(){
  var name=$("#des_bio").attr("name");
  window.location.href=base+"libraries/files/salarios/empleados"
}

//reset

