 var x=$(document);
 x.ready(inicio);
 function inicio(){
 	$("#cliente").click(function(){activar("cliente");get_2n('cliente_proveedor/search_cliente',{},'search',false,'cliente_proveedor/view_cliente',{},'contenido',true); url("Cliente","cliente_proveedor");})
 	$("#proveedor").click(function(){activar("proveedor");get_2n('cliente_proveedor/search_proveedor',{},'search',false,'cliente_proveedor/view_proveedor',{},'contenido',true); url("Proveedor","cliente_proveedor?p=2");})
 	$("#config").click(function(){activar("config");get_2n('',{},'search',false,'cliente_proveedor/view_config',{},'contenido',true); url("Cliente-proveedor-configuracion","cliente_proveedor?p=3");})
 }
 function reset_all_view(id){
	if(id!="s_nit"){ $("#s_nit").val(""); }
	if(id!="s_razon"){ $("#s_razon").val(""); }
	if(id!="s_encargado"){ $("#s_encargado").val(""); }
	if(id!="s_telefono"){ $("#s_telefono").val(""); }
	if(id!="s_direccion"){ $("#s_direccion").val(""); }

	if(id!="s2_cod"){ $("#s2_cod").val(""); }
	if(id!="s2_nom"){ $("#s2_nom").val(""); }	
}
function reset_input_2(id){
	if(id!="s_cod2"){ $("#s_cod2").val(""); }	
	if(id!="s_nom2"){ $("#s_nom2").val(""); }
}
 function blur_all(id){
	if(id!="s_nit"){ OnBlur("s_nit");}
	if(id!="s_razon"){ OnBlur("s_razon");}
	if(id!="s_encargado"){ OnBlur("s_encargado");}
	if(id!="s_telefono"){ OnBlur("s_telefono");}
	if(id!="s_direccion"){ OnBlur("s_direccion");}

	if(id!="s_cod2"){ OnBlur("s_cod2");}
	if(id!="s_nom2"){ OnBlur("s_nom2");}
 }
 /*------- MANEJO DE CLIENTE -------*/
   	/*--- Buscador ---*/
   	function view_cliente(){
   		var ele=new FormData();
   		ele.append('nit',$("#s_nit").val());
   		ele.append('nom',$("#s_razon").val());
   		ele.append('resp',$("#s_encargado").val());
   		ele.append('telf',$("#s_telefono").val());
   		ele.append('dir',$("#s_direccion").val());
   		get('cliente_proveedor/view_cliente',ele,'contenido',true);
   		blur_all("");
   		return false;
   	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	function all_cliente(){
   		reset_all_view('');
   		view_cliente();
   	}
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
   	function new_cliente(){
   		modal('CLIENTE: Nuevo','md','1');
		btn_modal('save_cliente',"",'',"",'1','md');
		get('cliente_proveedor/new_cliente',{},'content_1',true);
   	}
   	function view_ciudades(id,pais){
   		var atrib=new FormData();atrib.append('idpa',pais);
   		get('cliente_proveedor/view_ciudades',atrib,id,false);
   		return false;	
   	}
   	function search_ci(){
   		var ci=$("#new_nit").val();
   		var inputs="new_raz|new_tel|new_ema|new_pai|new_ciu|new_dir|new_obs";
   		var atrib=new FormData();
   		atrib.append('ci',ci);
   		get_input('cliente_proveedor/search_ci',atrib,"NULL",inputs,false);
   	}
   	function save_cliente(){
   		var fot=$("#new_file").prop('files');
		var nit=$("#new_nit").val();
		var raz=$("#new_raz").val();
		var res=$("#new_res").val();
		var tel=$("#new_tel").val();
		var ema=$("#new_ema").val();
		var pai=$("#new_pai").val();
		var ciu=$("#new_ciu").val();
		var dir=$("#new_dir").val();
		var web=$("#new_web").val();
		var obs=$("#new_obs").val();
		if(entero(nit,6,25)){
			if(strSpace(raz,2,100)){
				if(pai!=""){
					if(ciu!=""){
						var guardar=true;
						if(res!=""){ if(!strSpace(res,2,150)){ guardar=false; alerta('Ingrese un nombre de responsable válido','top','new_res');}}
						if(tel!=""){ if(!entero(tel,7,15)){ guardar=false; alerta('Ingrese un numero de telefono válido','top','new_tel');}}
						if(ema!=""){ if(!email(ema)){ guardar=false; alerta('Ingrese un correo electronico válido','top','new_ema');}}
						if(dir!=""){ if(!direccion(dir,5,200)){ guardar=false; alerta('Ingrese una dirección de domicilio válida','top','new_dir');}}
						if(web!=""){ if(!webs(web)){ guardar=false; alerta('Ingrese una dirección web válida','top','new_web');}}
						if(!textarea(obs,0,900)){ guardar=false; alerta('Ingrese un contenido de observaciones válida','top','new_obs');}
						if(guardar){
							var ele=new FormData();
							ele.append('nit',nit);
							ele.append('raz',raz);
							ele.append('res',res);
							ele.append('tel',tel);
							ele.append('ema',ema);
							ele.append('pai',pai);
							ele.append('ciu',ciu);
							ele.append('dir',dir);
							ele.append('web',web);
							ele.append('obs',obs);
							ele.append('archivo',fot[0]);
							set('cliente_proveedor/save_cliente',ele,'NULL',true,'cliente_proveedor/view_cliente',{},'contenido',false,'1');
						}
					}else{
						alerta('Seleccione una ciudad','top','new_ciu');
					}
				}else{
					alerta('Seleccione un Pais','top','new_pai');
				}
			}else{
				alerta('Ingrese nombre o razon social válido','top','new_raz');
			}
		}else{
			alerta('Ingrese un NIT o CI válido...','top','new_nit');
		}
		return false;
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	function imprimir_clientes(json){
   		modal("CLIENTES: ConfiguraciÓn de impresion","lg","1");
 		btn_modal('',"",'imprimir',"'area'",'1','lg');
 		var atribs=new FormData();atribs.append('json',json);
 		get_2n('cliente_proveedor/config_imprimir_clientes',atribs,'content_1',true,'cliente_proveedor/imprimir_clientes',atribs,'area',true);
   	}
   	function arma_informe(json){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var v6=""; if($("#6:checked").val()){v6='ok'}
		var v7=""; if($("#7:checked").val()){v7='ok'}
		var v8=""; if($("#8:checked").val()){v8='ok'}
		var v9=""; if($("#9:checked").val()){v9='ok'}
		var v10=""; if($("#10:checked").val()){v10='ok'}
		var v11=""; if($("#11:checked").val()){v11='ok'}
		var v12=""; if($("#12:checked").val()){v12='ok'}
		var nro=$("#nro").val();
		var atribs=new FormData();
		atribs.append('json',json);
		atribs.append('v1',v1);
		atribs.append('v2',v2);
		atribs.append('v3',v3);
		atribs.append('v4',v4);
		atribs.append('v5',v5);
		atribs.append('v6',v6);
		atribs.append('v7',v7);
		atribs.append('v8',v8);
		atribs.append('v9',v9);
		atribs.append('v10',v10);
		atribs.append('v11',v11);
		atribs.append('v12',v12);
		atribs.append('nro',nro);
		get('cliente_proveedor/imprimir_clientes',atribs,'area',true);
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	function reportes_cliente(idcl){
   		modal('CLIENTE: Detalle','sm','1');
		btn_modal('',"",'',"",'1','sm');
		var ele=new FormData();ele.append('idcl',idcl)
		get('cliente_proveedor/detalle_cliente',ele,'content_1',true);		
   	}
   	function reportes_cliente_producto(idcl){
   		modal('CLIENTE: Productos','sm','1');
		button_modal('modal_ok_1','','','modal_print_1','imprimir',"'area'",'modal_closed_1');
		var ele=new FormData();ele.append('idcl',idcl)
		get('cliente_proveedor/detalle_cliente',ele,'content_1',true);		
   	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	//modificar
   	function configuracion_cliente(idcl){
   		modal('CLIENTE: Modificar','md','1');
		btn_modal('update_cliente',"'"+idcl+"'",'',"",'1','md');
		var ele=new FormData();ele.append('idcl',idcl)
		get('cliente_proveedor/form_update_cliente',ele,'content_1',true);		
   	}
	function update_cliente(idcl){
   		var fot=$("#up_file").prop('files');
		var nit=$("#up_nit").val();
		var raz=$("#up_raz").val();
		var res=$("#up_res").val();
		var tel=$("#up_tel").val();
		var ema=$("#up_ema").val();
		var pai=$("#up_pai").val();
		var ciu=$("#up_ciu").val();
		var dir=$("#up_dir").val();
		var web=$("#up_web").val();
		var obs=$("#up_obs").val();
		if(entero(nit,6,25)){
			if(strSpace(raz,2,100)){
				if(pai!=""){
					if(ciu!=""){
						var guardar=true;
						if(res!=""){ if(!strSpace(res,2,150)){ guardar=false; alerta('Ingrese un nombre de responsable válido','top','up_res');}}
						if(tel!=""){ if(!entero(tel,7,15)){ guardar=false; alerta('Ingrese un numero de telefono válido','top','up_tel');}}
						if(ema!=""){ if(!email(ema)){ guardar=false; alerta('Ingrese un correo electronico válido','top','up_ema');}}
						if(dir!=""){ if(!direccion(dir,5,200)){ guardar=false; alerta('Ingrese una dirección de domicilio válida','top','up_dir');}}
						if(web!=""){ if(!webs(web)){ guardar=false; alerta('Ingrese una dirección web válida','top','up_web');}}
						if(!textarea(obs,0,900)){ guardar=false; alerta('Ingrese un contenido de observaciones válida','top','up_obs');}
						if(guardar){
							var ele=new FormData();
							ele.append('idcl',idcl);							
							ele.append('nit',nit);
							ele.append('raz',raz);
							ele.append('res',res);
							ele.append('tel',tel);
							ele.append('ema',ema);
							ele.append('pai',pai);
							ele.append('ciu',ciu);
							ele.append('dir',dir);
							ele.append('web',web);
							ele.append('obs',obs);
							ele.append('archivo',fot[0]);
							set('cliente_proveedor/update_cliente',ele,'NULL',true,'cliente_proveedor/view_cliente',{},'contenido',false,'1');
						}
					}else{
						alerta('Seleccione una ciudad','top','up_ciu');
					}
				}else{
					alerta('Seleccione un Pais','top','up_pai');
				}
			}else{
				alerta('Ingrese nombre o razon social válido','top','up_raz');
			}
		}else{
			alerta('Ingrese un NIT o CI válido...','top','up_nit');
		}
		return false;
	}
	//modificar producto cliente
	function productos_cliente(idcl){
		var ele= new FormData();
		ele.append('idcl',idcl);
		modal('CLIENTE: Control de productos','lg','1');
		btn_modal('',"",'',"",'1','lg');
		get('cliente_proveedor/productos_cliente',ele,'content_1',false);
	}
	function form_adicionar_producto_cliente(idcl){
		modal('CLIENTE: Productos','sm','2');
		btn_modal('',"",'',"",'2','sm');
		var ele=new FormData();ele.append('idcl',idcl);
		get_2n('cliente_proveedor/search_productos_cliente',ele,'content_2',true,'cliente_proveedor/view_productos_cliente',ele,'search_2',true);
	}
	function search_productos_cliente(idcl){
		var cod=$("#s2_cod").val();
		var nom=$("#s2_nom").val();
		var atrib=new FormData();
		atrib.append('idcl',idcl);atrib.append('cod',cod);atrib.append('nom',nom);
		get('cliente_proveedor/view_productos_cliente',atrib,'search_2',true);
		return false;
	}
	function view_all_producto_cliente(idcl){
		reset_all_view("");
		search_productos_cliente(idcl);
	}
	function save_producto_cliente(ele,color,diametro,idcl,idpim,cont){
		var iduv=$("#uv"+cont).val();
		if(iduv!=""){
			//set_button('cliente_proveedor/save_producto_cliente',{idcl,idpim,iduv},,{idcl},'content_1',ele,color,diametro);
			var atrib=new FormData();
			atrib.append('idcl',idcl);
			set_button('cliente_proveedor/save_producto_cliente',{idcl,idpim,iduv},'cliente_proveedor/productos_cliente',atrib,"content_1",true,ele,color,diametro)
		}else{
			alerta("Seleccione un porcentaje utilidad de venta",'left','uv'+cont);
		}
	}
	function calcular_costo(idpc){
		var por=($("#uv"+idpc).val()).split("|");
		if(por[1]!="" && parseFloat(por[1])){
			var cos_pro=$("#cos_pro"+idpc).text();
			if(cos_pro!=""){
				if(cos_pro>=0 && parseFloat(cos_pro)){
					cos_pro=parseFloat(cos_pro);
					por=parseFloat(por[1]);
					var res=cos_pro+(cos_pro*por);
					console.log(res);
					$("#cos_ven_sug"+idpc).text(res);
					$("#cos_ven"+idpc).val(res);
				}else{
					alerta("Error!!, solo de acepta valores numéricos mayores o iguales a cero",'top','cos_pro'+idpc);
				}
			}else{
				alerta("Error!!, el valor vacio no es aceptado",'top','cos_pro'+idpc);
			}
		}else{
			alerta("Seleccione un porcentaje utilidad de venta",'top','uv'+idpc);
		}
	}
	function update_presio_venta(idcl,idpc){
		var cos=$("#cos_ven"+idpc).val();
		var uv=$("#uv"+idpc).val();
		if(cos>0 && cos!=""){
			if(uv!=""){
				var atrib= new FormData();
			 	atrib.append('idpc',idpc);
			 	atrib.append('cos',cos);
			 	atrib.append('uv',uv);
			 	var atrib2=new FormData();atrib2.append('idcl',idcl);
			 	set('cliente_proveedor/update_presio_venta',atrib,'NULL',true,'cliente_proveedor/productos_cliente',atrib2,'content_1',false,'NULL');
			}else{
				alerta("Seleccione un porcentaje utilidad de venta",'top','uv'+idpc);
			}
		}else{
			alerta("Ingrese una cantidad válida mayor que cero","top","#cos_ven"+idpc);
		}
		return false;
	}
	function confirmar_producto_cliente(idcl,idpc,nom,img){
		modal("PRODUCTO CLIENTE: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','drop_producto_cliente',"'"+idcl+"','"+idpc+"'");
   		var atrib=new FormData();atrib.append('idpc',idpc);atrib.append('img',img);atrib.append('nom',nom);
   		get('cliente_proveedor/confirmar_producto_cliente',atrib,'content_5',true);
	}
	function drop_producto_cliente(idcl,idpc){
		var atrib=new FormData();
		atrib.append('u',$("#e_user").val());
		atrib.append('p',$("#e_password").val());
		atrib.append('idpc',idpc);
		var atrib2=new FormData();atrib2.append('idcl',idcl);
		set('cliente_proveedor/drop_producto_cliente',atrib,'NULL',true,"cliente_proveedor/productos_cliente",atrib2,'content_1',false,'5');
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	function confirmar_cliente(id,cliente,img){
   		modal("CLIENTE: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','drop_cliente',"'"+id+"'");
   		var atribs=new FormData();atribs.append('idcl',id);
   		get('cliente_proveedor/confirmar',atribs,'content_5',true);
   	}
   	function drop_cliente(id){
		var ele=new FormData();
		ele.append('u',$("#e_user").val());
		ele.append('p',$("#e_password").val());
		ele.append('idcl',id);
		set('cliente_proveedor/drop_cliente',ele,'NULL',true,'cliente_proveedor/view_cliente',{},'contenido',false,'5');
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE CLIENTE -------*/
/*------- MANEJO DE PROVEEDOR -------*/
   	/*--- Buscador ---*/
   	function search_proveedor(){
   		var ele=new FormData();
   		ele.append('nit',$("#s_nit").val());
   		ele.append('nom',$("#s_razon").val());
   		ele.append('resp',$("#s_encargado").val());
   		ele.append('telf',$("#s_telefono").val());
   		ele.append('dir',$("#s_direccion").val());
   		get('cliente_proveedor/view_proveedor',ele,'contenido',true);
   		return false;
   	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	function all_proveedor(){
   		reset_all_view('');
   		search_proveedor();
   	}
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
   	function new_proveedor(){
   		modal('PROVEEDOR: Nuevo','md','1');
		btn_modal('save_proveedor',"",'',"",'1','md');
		get('cliente_proveedor/new_proveedor',{},'content_1',true);
   	}
   	function save_proveedor(){
   		var fot=$("#new_file").prop('files');
		var nit=$("#new_nit").val();
		var raz=$("#new_raz").val();
		var res=$("#new_res").val();
		var tel=$("#new_tel").val();
		var ema=$("#new_ema").val();
		var pai=$("#new_pai").val();
		var ciu=$("#new_ciu").val();
		var dir=$("#new_dir").val();
		var web=$("#new_web").val();
		var obs=$("#new_obs").val();
		if(entero(nit,6,25)){
			if(strSpace(raz,2,100)){
				if(pai!=""){
					if(ciu!=""){
						var guardar=true;
						if(res!=""){ if(!strSpace(res,2,150)){ guardar=false; alerta('Ingrese un nombre de responsable válido','top','new_res');}}
						if(tel!=""){ if(!entero(tel,7,15)){ guardar=false; alerta('Ingrese un numero de telefono válido','top','new_tel');}}
						if(ema!=""){ if(!email(ema)){ guardar=false; alerta('Ingrese un correo electronico válido','top','new_ema');}}
						if(dir!=""){ if(!direccion(dir,5,200)){ guardar=false; alerta('Ingrese una dirección de domicilio válida','top','new_dir');}}
						if(web!=""){ if(!webs(web)){ guardar=false; alerta('Ingrese una dirección web válida','top','new_web');}}
						if(!textarea(obs,0,900)){ guardar=false; alerta('Ingrese un contenido de observaciones válida','top','new_obs');}
						if(guardar){
							var ele=new FormData();
							ele.append('nit',nit);
							ele.append('raz',raz);
							ele.append('res',res);
							ele.append('tel',tel);
							ele.append('ema',ema);
							ele.append('pai',pai);
							ele.append('ciu',ciu);
							ele.append('dir',dir);
							ele.append('web',web);
							ele.append('obs',obs);
							ele.append('archivo',fot[0]);
							set('cliente_proveedor/save_proveedor',ele,'NULL',true,'cliente_proveedor/view_proveedor',{},'contenido',false,'1');
						}
					}else{
						alerta('Seleccione una ciudad','top','new_ciu');
					}
				}else{
					alerta('Seleccione un Pais','top','new_pai');
				}
			}else{
				alerta('Ingrese nombre o razon social válido','top','new_raz');
			}
		}else{
			alerta('Ingrese un NIT o CI válido...','top','new_nit');
		}
		return false;
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	function imprimir_proveedor(json){
   		modal("PROVEEDOR: Configuración de impresion","lg","1");
 		btn_modal('',"",'imprimir',"'area'",'1','lg');
 		var atribs=new FormData();atribs.append('json',json);
 		get_2n('cliente_proveedor/config_imprimir_proveedores',atribs,'content_1',true,'cliente_proveedor/imprimir_proveedores',atribs,'area',true);
   	}
   	function arma_informe_proveedor(json){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var v6=""; if($("#6:checked").val()){v6='ok'}
		var v7=""; if($("#7:checked").val()){v7='ok'}
		var v8=""; if($("#8:checked").val()){v8='ok'}
		var v9=""; if($("#9:checked").val()){v9='ok'}
		var v10=""; if($("#10:checked").val()){v10='ok'}
		var v11=""; if($("#11:checked").val()){v11='ok'}
		var v12=""; if($("#12:checked").val()){v12='ok'}
		var nro=$("#nro").val();
		var atribs=new FormData();
		atribs.append('json',json);
		atribs.append('v1',v1);
		atribs.append('v2',v2);
		atribs.append('v3',v3);
		atribs.append('v4',v4);
		atribs.append('v5',v5);
		atribs.append('v6',v6);
		atribs.append('v7',v7);
		atribs.append('v8',v8);
		atribs.append('v9',v9);
		atribs.append('v10',v10);
		atribs.append('v11',v11);
		atribs.append('v12',v12);
		atribs.append('nro',nro);
		get('cliente_proveedor/imprimir_proveedores',atribs,'area',true);
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	function reportes_proveedor(idpro){
   		modal('PROVEEDOR: Detalle','md','1');
		btn_modal('',"",'',"",'1','md');
		var ele=new FormData();ele.append('idpro',idpro)
		get('cliente_proveedor/detalle_proveedor',ele,'content_1',true);		
   	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	   	//modificar
   	function configuracion_proveedor(idpro){
   		modal('PROVEEDOR: Modificar','md','1');
		btn_modal('update_proveedor',"'"+idpro+"'",'',"",'1',"md");
		var ele=new FormData();ele.append('idpro',idpro)
		get('cliente_proveedor/form_update_proveedor',ele,'content_1',true);		
   	}
	function update_proveedor(idpro){
   		var fot=$("#up_file").prop('files');
		var nit=$("#up_nit").val();
		var raz=$("#up_raz").val();
		var res=$("#up_res").val();
		var tel=$("#up_tel").val();
		var ema=$("#up_ema").val();
		var pai=$("#up_pai").val();
		var ciu=$("#up_ciu").val();
		var dir=$("#up_dir").val();
		var web=$("#up_web").val();
		var obs=$("#up_obs").val();
		if(entero(nit,6,25)){
			if(strSpace(raz,2,100)){
				if(pai!=""){
					if(ciu!=""){
						var guardar=true;
						if(res!=""){ if(!strSpace(res,2,150)){ guardar=false; alerta('Ingrese un nombre de responsable válido','top','up_res');}}
						if(tel!=""){ if(!entero(tel,7,15)){ guardar=false; alerta('Ingrese un numero de telefono válido','top','up_tel');}}
						if(ema!=""){ if(!email(ema)){ guardar=false; alerta('Ingrese un correo electronico válido','top','up_ema');}}
						if(dir!=""){ if(!direccion(dir,5,200)){ guardar=false; alerta('Ingrese una dirección de domicilio válida','top','up_dir');}}
						if(web!=""){ if(!webs(web)){ guardar=false; alerta('Ingrese una dirección web válida','top','up_web');}}
						if(!textarea(obs,0,900)){ guardar=false; alerta('Ingrese un contenido de observaciones válida','top','up_obs');}
						if(guardar){
							var ele=new FormData();
							ele.append('idpro',idpro);							
							ele.append('nit',nit);
							ele.append('raz',raz);
							ele.append('res',res);
							ele.append('tel',tel);
							ele.append('ema',ema);
							ele.append('pai',pai);
							ele.append('ciu',ciu);
							ele.append('dir',dir);
							ele.append('web',web);
							ele.append('obs',obs);
							ele.append('archivo',fot[0]);
							set('cliente_proveedor/update_proveedor',ele,'NULL',true,'cliente_proveedor/view_proveedor',{},'contenido',false,'1');
						}
					}else{
						alerta('Seleccione una ciudad','top','up_ciu');
					}
				}else{
					alerta('Seleccione un Pais','top','up_pai');
				}
			}else{
				alerta('Ingrese nombre o razon social válido','top','up_raz');
			}
		}else{
			alerta('Ingrese un NIT o CI válido...','top','up_nit');
		}
		return false;
	}
	/*--- Manejo de materiales en el proveedor ---*/
	function materiales_proveedor(idpro){
		modal('PROVEEDOR: Materiales','md','1');
		btn_modal('',"",'',"",'1','md');
		var ele=new FormData();ele.append('idpro',idpro);
		get('cliente_proveedor/materiales_proveedor',ele,'content_1',true);	
	}
	function update_presio_compra(idpro,idmp){
		var can=$("#presio"+idmp).val();
		if(can>=0){
			var ele= new FormData();
		 	ele.append('idmp',idmp);
		 	ele.append('can',can);
		 	ele.append('idpro',idpro);
		 	set('cliente_proveedor/update_presio_compra',ele,'NULL',true,'cliente_proveedor/materiales_proveedor',ele,'content_1',false,'');
			
		}else{
			alerta('Ingrese una cantidad mayor a cero.','top','presio'+idmp);
		}
		return false;
	}
	function confirmar_material_proveedor(idpro,idmp,tipo){
		modal("ALMACEN: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','drop_material_proveedor',"'"+idpro+"','"+idmp+"','"+tipo+"'");
   		var atrib=new FormData();atrib.append('idmp',idmp);atrib.append('tipo',tipo);
   		get('cliente_proveedor/confirmar_material_proveedor',atrib,'content_5',true);
	}
	function drop_material_proveedor(idpro,idmp,tipo) {
		var atrib=new FormData();atrib.append('idmp',idmp);atrib.append('idpro',idpro);atrib.append('tipo',tipo);
		atrib.append('u',$("#e_user").val());
		atrib.append('p',$("#e_password").val());
		set('cliente_proveedor/drop_material_proveedor',atrib,'NULL',true,'cliente_proveedor/materiales_proveedor',atrib,'content_1',false,'5');
	}

		/*--- nuevo material en el proveedor ----*/
		function form_adicionar_material_proveedor(idpro){//adicionar materiales de almacenes
			modal("PROVEEDOR: Materiales",'md','2');
			btn_modal('','','','','2','md');
			var ele=new FormData();ele.append('idpro',idpro);
			get_2n('cliente_proveedor/search_materiales_proveedor',ele,'content_2',false,'cliente_proveedor/view_materiales_proveedor',ele,'contenido_2',true);
		}
		function search_proveedor_material(idpro){
			var cod=$("#s_cod2").val();
			var nom=$("#s_nom2").val();
			var alm=$("#s_alm2").val();
			var atrib=new FormData();
			atrib.append('idpro',idpro);atrib.append('cod',cod);atrib.append('nom',nom);atrib.append('ida',alm);
			get('cliente_proveedor/view_materiales_proveedor',atrib,'contenido_2',true);
			blur_all("");
			return false;
		}
		function save_material_proveedor(idpro,idmi,ele,color,diametro){
			var atrib=new FormData();atrib.append('idpro',idpro);
			set_button('cliente_proveedor/save_material_proveedor',{idpro,idmi},'cliente_proveedor/materiales_proveedor',atrib,'content_1',false,ele,color,diametro);
		}
		/*--- End nuevo material en el proveedor ----*/
		/*--- nuevo material indirecto (adicional) ----*/
		function form_adicionar_proveedor_indirectos(idpro){//adicionar materiales de almacenes
			modal("PROVEEDOR: Materiales idirectos",'md','2');
			btn_modal('','','','','2','md');
			var ele=new FormData();ele.append('idpro',idpro);
			get_2n('cliente_proveedor/search_proveedor_indirectos',ele,'content_2',false,'cliente_proveedor/view_proveedor_indirectos',ele,'contenido_2',true);
		}
		function search_proveedor_indirectos(idpro){
			var cod=$("#s_cod2").val();
			var nom=$("#s_nom2").val();
			var atrib=new FormData();
			atrib.append('idpro',idpro);atrib.append('cod',cod);atrib.append('nom',nom);
			get('cliente_proveedor/view_proveedor_indirectos',atrib,'contenido_2',true);
			blur_all("");
			return false;
		}
		function save_material_proveedor_2(idpro,idmi,ele,color,diametro){
			var atrib=new FormData();atrib.append('idpro',idpro);
			set_button('cliente_proveedor/save_material_proveedor_2',{idpro,idmi},'cliente_proveedor/materiales_proveedor',atrib,'content_1',false,ele,color,diametro);
		}
		/*--- End nuevo material indirecto (adicional) ----*/
		/*--- nuevo insumo (varios) ----*/
		function form_adicionar_proveedor_insumos(idpro){//adicionar materiales varios
			modal("PROVEEDOR: Otro tipo de materiales",'md','2');
			btn_modal('','','','','2','md');
			var ele=new FormData();ele.append('idpro',idpro);
			get_2n('cliente_proveedor/search_proveedor_insumos',ele,'content_2',false,'cliente_proveedor/view_proveedor_insumos',ele,'contenido_2',true);
		}
		function search_proveedor_insumos(idpro){
			var cod=$("#s_cod2").val();
			var nom=$("#s_nom2").val();
			var atrib=new FormData();
			atrib.append('idpro',idpro);atrib.append('cod',cod);atrib.append('nom',nom);
			get('cliente_proveedor/view_proveedor_insumos',atrib,'contenido_2',true);
			blur_all("");
			return false;
		}
		function save_proveedor_insumos(idpro,idmi,ele,color,diametro){
			var atrib=new FormData();atrib.append('idpro',idpro);
			set_button('cliente_proveedor/save_proveedor_insumos',{idpro,idmi},'cliente_proveedor/materiales_proveedor',atrib,'content_1',false,ele,color,diametro);
		}
		/*--- End nuevo insumo (varios) ----*/
		/*--- nuevo activos fijos ----*/
		function form_adicionar_proveedor_activos(idpro){//adicionar materiales varios
			modal("PROVEEDOR: Activos Fijos",'md','2');
			btn_modal('','','','','2','md');
			var ele=new FormData();ele.append('idpro',idpro);
			get_2n('cliente_proveedor/search_proveedor_activos',ele,'content_2',false,'cliente_proveedor/view_proveedor_activos',ele,'contenido_2',true);
		}
		function search_proveedor_activos(idpro){
			var cod=$("#s_cod2").val();
			var nom=$("#s_nom2").val();
			var atrib=new FormData();
			atrib.append('idpro',idpro);atrib.append('cod',cod);atrib.append('nom',nom);
			get('cliente_proveedor/view_proveedor_activos',atrib,'contenido_2',true);
			blur_all("");
			return false;
		}
		function save_proveedor_activos(idpro,idmi,ele,color,diametro){
			var atrib=new FormData();atrib.append('idpro',idpro);
			set_button('cliente_proveedor/save_proveedor_activos',{idpro,idmi},'cliente_proveedor/materiales_proveedor',atrib,'content_1',false,ele,color,diametro);
		}
		/*--- End nuevo activos fijos ----*/


	function form_adicionar_material_adicional(idpro){
		//visible('content_modal','modal','.2%','50%','85%');
		button_modal('modal_ok','','','modal_print','','','modal_closed');
		var ele=new FormData();ele.append('idpro',idpro);
		get_2n('cliente_proveedor/search_material_adicional_proveedor',ele,'content_3','cliente_proveedor/view_material_adicional_proveedor',ele,'search_contenido');
	}
	function form_adicionar_activo_fijo(idpro){
		//visible('content_modal','modal','.2%','50%','85%');
		button_modal('modal_ok','','','modal_print','','','modal_closed');
		var ele=new FormData();ele.append('idpro',idpro);
		get_2n('cliente_proveedor/search_activo_fijo_proveedor',ele,'content_3','cliente_proveedor/view_activo_fijo_proveedor',ele,'search_contenido');
	}
	function form_adicionar_material_vario(idpro){
		//visible('content_modal','modal','.2%','50%','85%');
		button_modal('modal_ok','','','modal_print','','','modal_closed');
		var ele=new FormData();ele.append('idpro',idpro);
		get_2n('cliente_proveedor/search_material_vario_proveedor',ele,'content_3','cliente_proveedor/view_material_vario_proveedor',ele,'search_contenido');
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	function confirmar_proveedor(idpro){
   		modal("PROVEEDOR: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','drop_proveedor',"'"+idpro+"'");
   		var atribs=new FormData();atribs.append('idpro',idpro);
   		get('cliente_proveedor/confirmar_proveedor',atribs,'content_5',true);
   	}
   	function drop_proveedor(id){
		var ele=new FormData();
		ele.append('u',$("#e_user").val());
		ele.append('p',$("#e_password").val());
		ele.append('idpro',id);
		set('cliente_proveedor/drop_proveedor',ele,'NULL',true,'cliente_proveedor/view_proveedor',{},'contenido',false,'5');
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE PROVEEDOR -------*/
/*------- MANEJO DE CONFIGURACION -------*/
   	/*--- Paises ---*/
   	function save_pais() {
   		var p=$("#p").val();
   		if(p.length>=2 && p.length<=100){
   			var atrib=new FormData();
   			atrib.append('p',p);
   			set('cliente_proveedor/save_pais',atrib,'NULL',true,'cliente_proveedor/view_config',{},'contenido',false);
   		}else{
   			alerta('Ingrese un nombre de pais válido','top','p');
   		}
   		return false;
   	}
   	function update_pais(idpa) {
   		var p=$("#p"+idpa).val();
   		if(p.length>=2 && p.length<=100){
   			var atrib=new FormData();
   			atrib.append('idpa',idpa);
   			atrib.append('p',p);
   			set('cliente_proveedor/update_pais',atrib,'NULL',true,'cliente_proveedor/view_config',{},'contenido',false);
   		}else{
   			alerta('Ingrese un nombre de pais válido','top','p'+idpa);
   		}
   		return false;
   	}
   	function alerta_pais(idpa){
   		modal("PAIS: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','delete_pais',"'"+idpa+"'");
   		var atrib=new FormData();atrib.append('idpa',idpa);
   		atrib.append('titulo','definitivamente el pais: '+$("#p"+idpa).val());
   		get('cliente_proveedor/alerta',atrib,'content_5',true);
   	}
   	function delete_pais(idpa) {
   		var atrib=new FormData();
   		atrib.append('idpa',idpa);
   		set('cliente_proveedor/delete_pais',atrib,'NULL',true,'cliente_proveedor/view_config',{},'contenido',false,'5');
   		return false;
   	}
   	/*--- End Paises ---*/
   	/*--- Ciudades ---*/
   	function save_ciudad() {
   		var c=$("#c").val();
   		var cp=$("#cp").val();
   		if(c.length>=2 && c.length<=100){
   			if(cp!=""){
	   			var atrib=new FormData();
	   			atrib.append('cp',cp);
	   			atrib.append('c',c);
	   			set('cliente_proveedor/save_ciudad',atrib,'NULL',true,'cliente_proveedor/view_config',{},'contenido',false);
   			}else{
   				alerta('Seleccione un pais donde pertenece la ciudad','top','cp');
   			}
   		}else{
   			alerta('Ingrese un nombre de ciudad válido','top','c');
   		}
   		return false;
   	}
   	function update_ciudad(idci) {
   		var c=$("#c"+idci).val();
   		var cp=$("#cp"+idci).val();
   		if(c.length>=2 && c.length<=100){
   			if(cp!=""){
   				var atrib=new FormData();
	   			atrib.append('idci',idci);
	   			atrib.append('cp',cp);
	   			atrib.append('c',c);
	   			set('cliente_proveedor/update_ciudad',atrib,'NULL',true,'cliente_proveedor/view_config',{},'contenido',false);
	   		}else{
   				alerta('Seleccione un pais donde pertenece la ciudad','top','cp'+idci);
   			}
   		}else{
   			alerta('Ingrese un nombre de ciudad válido','top','c'+idci);
   		}
   		return false;
   	}
   	function alerta_ciudad(idci){
   		modal("PAIS: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','delete_ciudad',"'"+idci+"'");
   		var atrib=new FormData();atrib.append('idci',idci);
   		atrib.append('titulo','definitivamente la ciudad: '+$("#c"+idci).val());
   		get('cliente_proveedor/alerta',atrib,'content_5',true);
   	}
   	function delete_ciudad(idci) {
   		var atrib=new FormData();
   		atrib.append('idci',idci);
   		set('cliente_proveedor/delete_ciudad',atrib,'NULL',true,'cliente_proveedor/view_config',{},'contenido',false,'5');
   		return false;
   	}
   	/*--- End Ciudades ---*/
   	/*--- control de porcentaje de utilidad de venta ---*/
   	function save_porcentaje(){
   		var des=$("#pu_des").val();
   		var por=$("#pu_por").val();
   		if(strSpace(des,2,150)){
   			if(esNumero(por) && por<=100 && por>=0){
   				var atrib=new FormData();
   				atrib.append('des',des);
   				atrib.append('por',por);
   				set('cliente_proveedor/save_porcentaje',atrib,'NULL',true,'cliente_proveedor/view_config',{},'contenido',false,'');
	   		}else{
	   			alerta("Ingrese un porcentaje válido","top","pu_por");
	   		}
   		}else{
   			alerta("Ingrese una descripcion válida","top","pu_des");
   		}
   		return false;
   	}
   	function update_porcentaje(iduv){
   		var des=$("#pu_des"+iduv).val();
   		var por=$("#pu_por"+iduv).val();
   		if(strSpace(des,2,150)){
   			if(esNumero(por) && por<=100 && por>=0){
   				var atrib=new FormData();
   				atrib.append('des',des);atrib.append('por',por);atrib.append('iduv',iduv);
   				set('cliente_proveedor/update_porcentaje',atrib,'NULL',true,'cliente_proveedor/view_config',{},'contenido',false,'');
	   		}else{
	   			alerta("Ingrese un porcentaje válido","top","pu_por"+iduv);
	   		}
   		}else{
   			alerta("Ingrese una descripcion válida","top","pu_des"+iduv);
   		}
   	}
   	function alerta_porcentaje(iduv){
   		modal("UTILIDAD DE VENTA: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','delete_porcentaje',"'"+iduv+"'");
   		var atrib=new FormData();atrib.append('iduv',iduv);
   		atrib.append('titulo','definitivamente el porcentaje: <b>'+$("#pu_des"+iduv).val()+"["+$("#pu_por"+iduv).val()+"%]</b>");
   		get('cliente_proveedor/alerta',atrib,'content_5',true);
   	}
   	function delete_porcentaje(iduv){
   		var atrib=new FormData();atrib.append('iduv',iduv);
   		set('cliente_proveedor/delete_porcentaje',atrib,'NULL',true,'cliente_proveedor/view_config',{},'contenido',false,'5');
   	}
   	/*--- End control de porcentaje de utilidad de venta ---*/
/*------- END MANEJO DE CONFIGURACION -------*/