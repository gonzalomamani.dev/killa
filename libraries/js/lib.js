//Hola 
$(function() {
    $("#side-menu").MenuIzq();
});
(function($){
    var nombrePluging="MenuIzq";
        defaults={toggle:true};
        function Menu(elemento,opciones){
            this.elemento=elemento;
            this.settings=$.extend({},defaults,opciones);
            this.inicio();
        }
        Menu.prototype={
            inicio: function(){
                var $this=$(this.elemento);
                var $toggle=this.settings.toggle;
                $this.find("li.active").has("ul").children("ul").addClass("collapse in");
                $this.find("li").not(".active").has("ul").children("ul").addClass("collapse");
                $this.find("li").has("ul").children("a").on("click", function (e) {
                    e.preventDefault();
                    $(this).parent("li").toggleClass("active").children("ul").collapse("toggle");
                    if ($toggle) {
                        $(this).parent("li").siblings().removeClass("active").children("ul.in").collapse("hide");
                    }
                });

            },
        };
    $.fn[nombrePluging]=function(opciones){
        return this.each(function(){  
            if (!$.data(this, "plugin_" + nombrePluging)) {
                $.data(this, "plugin_" + nombrePluging, new Menu(this, opciones));
            }
        });
    }
})(jQuery);

/*window.addEventListener("load",function(){
  setTimeout(function(){ 
    window.scrollTo(0,1);
  },0);
});*/
//CAMBIA CLASE DE BOTON
$("#modal_closed_2").click(function() { hide_modal("2");});
$(".closed_2").click(function() { hide_modal("2");});
$(document).click(function(e){ if($(e.target).attr("id")=="modal_2"){ hide_modal("2");} });

$("#modal_closed_3").click(function() { hide_modal("3");});
$(".closed_3").click(function() { hide_modal("3");});
$(document).click(function(e){ if($(e.target).attr("id")=="modal_3"){ hide_modal("3");} });

$("#modal_closed_4").click(function() { hide_modal("4");});
$(".closed_4").click(function() { hide_modal("4");});
$(document).click(function(e){ if($(e.target).attr("id")=="modal_4"){ hide_modal("4");} });

$("#modal_closed_5").click(function() { hide_modal("5");});
$(".closed_5").click(function() { hide_modal("5");});
$(document).click(function(e){ if($(e.target).attr("id")=="modal_5"){ hide_modal("5");} });

$("#modal_closed_6").click(function() { hide_modal("6");});
$(".closed_6").click(function() { hide_modal("6");});
$(document).click(function(e){ if($(e.target).attr("id")=="modal_6"){ hide_modal("6");} });

function hide_modal(nivel){//cerrar modal bootstrap
  switch(nivel){
    case "1": $("#modal_"+nivel).modal('hide'); break;
    default: $("#modal_"+nivel).fadeOut(250);$("#dialog_"+nivel).fadeOut(150); $("#dialog_"+nivel).css("transform","translateY(-200%)"); break;
  }
}
function activar(activo,titulo,dir){
  var padre=document.getElementById("menu-top").firstChild;//obtenemos todos los elemntos <li>
  while(padre!=null){//recorremos todos los <li>
    if(padre.nodeType==Node.ELEMENT_NODE){
        var hijo=padre.firstChild;
        if(hijo.id==activo){
          padre.className="activado";
          hijos=padre.firstChild;
          hijos=hijos.firstChild;
          while(hijos!=null){
            if(hijos.id=="txt_btn"){
              hijos.className="";
              break;
            }
            hijos=hijos.nextSibling;
          }
        }else{
          padre.className="";
          hijos=padre.firstChild;
          hijos=hijos.firstChild;
          while(hijos!=null){
            if(hijos.id=="txt_btn"){
              hijos.className="g-btn-sm";
              break;
            }
            hijos=hijos.nextSibling;
          }
        }
      }
      padre=padre.nextSibling;
  }
  url(titulo,dir);
}
function url(titulo,url){
  var data={foo: "bar"};
  $(document).attr('title',titulo);
  window.history.pushState(data,titulo,url);
}
function modal(encabezado,ancho,nivel){
  $(".titulo_"+nivel).html(encabezado);
  $("#dialog_"+nivel).attr({"class":"modal-dialog modal-"+ancho});
  if(nivel=="1"){
    $("#modal_"+nivel).modal({keyboard:true}); 
  }else{
    $("#modal_"+nivel).fadeIn(150);
    $("#dialog_"+nivel).fadeIn(0);
    $("#dialog_"+nivel).css({"transform":"translateY(0)","visibility":"visible"});

  }
}
function btn_modal(funcion_g,atribs_g,funcion_i,atribs_i,nivel,ancho){
  var id_g="modal_ok_"+nivel;
  var id_i="modal_print_"+nivel;
  if(funcion_g!=""){
    if(funcion_i!=""){//mostramos los tres botones
      var col="col-sm-2 col-xs-12";
      var offset="col-sm-offset-6";
      switch(ancho){
        case "md": col="col-sm-3 col-xs-12"; offset="col-sm-offset-3"; break;
        case "sm": col="col-sm-3 col-xs-12"; offset="col-sm-offset-3"; break;
        case "xs": col="col-sm-4 col-xs-12"; offset="";break;
      }
      $("#"+id_g).attr({"class":col+" "+offset});
      $("#"+id_i).attr({"class":col});
      $("#modal_closed_"+nivel).attr({"class":col+" btn-cerrar"});
      addClick_v2(id_g,funcion_g,atribs_g);
      addClick_v2(id_i,funcion_i,atribs_i);
    }else{//guardar y eliminar
        var col="col-sm-2 col-xs-12";
        var offset="col-sm-offset-8";
        switch(ancho){
          case "md": col="col-sm-3 col-xs-12"; offset="col-sm-offset-6"; break;
          case "sm": col="col-sm-3 col-xs-12"; offset="col-sm-offset-6"; break;
          case "xs": col="col-sm-4 col-xs-12"; offset="col-sm-offset-4";break;
        }
        $("#"+id_g).attr({"class":col+" "+offset});
        $("#"+id_i).attr({"class":"hidden"});
        $("#modal_closed_"+nivel).attr({"class":col+" btn-cerrar"});
        addClick_v2(id_g,funcion_g,atribs_g);    
    }
  }else{
    if(funcion_i!=""){
        var col="col-sm-2 col-xs-12";
        var offset="col-sm-offset-8";
        switch(ancho){
          case "md": col="col-sm-3 col-xs-12"; offset="col-sm-offset-6"; break;
          case "sm": col="col-sm-3 col-xs-12"; offset="col-sm-offset-6"; break;
          case "xs": col="col-sm-4 col-xs-12"; offset="col-sm-offset-4";break;
        }
        $("#"+id_g).attr({"class":"hidden"});
        $("#"+id_i).attr({"class":col+" "+offset});
        $("#modal_closed_"+nivel).attr({"class":col+" btn-cerrar"});
        addClick_v2(id_i,funcion_i,atribs_i);  
    }else{
        var col="col-sm-2 col-xs-12";
        var offset="col-sm-offset-10";
        switch(ancho){
          case "md": col="col-sm-3 col-xs-12"; offset="col-sm-offset-9"; break;
          case "sm": col="col-sm-3 col-xs-12"; offset="col-sm-offset-9"; break;
          case "xs": col="col-sm-4 col-xs-12"; offset="col-sm-offset-8";break;
        }
        $("#"+id_g).attr({"class":"hidden"});
        $("#"+id_i).attr({"class":"hidden"});
        $("#modal_closed_"+nivel).attr({"class":col+" "+offset+" btn-cerrar"});      
    }
  }
}
function button_modal(id_g,funcion_g,atribs_g,id_i,funcion_i,atribs_i,id_e){
  if(funcion_g!=""){
    if(funcion_i!=""){//mostramos los tres botones
        $("#"+id_g).attr({"class":"col-xs-2 btn btn-success col-xs-offset-3"});
        $("#"+id_i).css({marginLeft:"5px"});
        $("#"+id_i).attr({"class":"col-xs-2 btn btn-info"});
        $("#"+id_e).css({marginLeft:"5px"});
        $("#"+id_e).attr({"class":"btn btn-danger col-xs-2"});
        addClick_v2(id_g,funcion_g,atribs_g);
        addClick_v2(id_i,funcion_g,atribs_i);
    }else{
        $("#"+id_g).attr({"class":"col-sm-2 col-sm-offset-4 col-xs-12 btn btn-success"});
        $("#"+id_i).removeAttr("style");
        $("#"+id_i).attr({"class":"hidden"});
        //$("#"+id_e).css({marginLeft:"5px"});
        $("#"+id_e).attr({"class":"col-sm-2 col-xs-12 btn btn-danger"});  
        addClick_v2(id_g,funcion_g,atribs_g);    
    }
  }else{
    if(funcion_i!=""){
        $("#"+id_g).attr({"class":"hidden"});
        $("#"+id_i).removeAttr("style");
        $("#"+id_i).attr({"class":"col-xs-2 btn btn-info col-xs-offset-4"});
        //$("#"+id_e).css({marginLeft:"5px"});
        $("#"+id_e).attr({"class":"btn btn-danger col-xs-2"});
        addClick_v2(id_i,funcion_i,atribs_i);  
    }else{
        $("#"+id_g).attr({"class":"hidden"});
        $("#"+id_i).removeAttr("style");
        $("#"+id_i).attr({"class":"hidden"});
        $("#"+id_e).removeAttr("style");
        $("#"+id_e).attr({"class":"btn btn-danger col-xs-2 col-xs-offset-5"});      
    }
  }
}
function addClick_v2(id,funcion,atribs){
  var ele=$("#"+id+" button");
  if(funcion!=""){
    ele.attr({"onclick":funcion+"("+atribs+")"});
  }else{
    ele.attr({"onclick":""});
  }
}

/*mensaje de globos popover bootstrap*/
function alerta(content,align,id){
  $("#"+id).attr({'data-trigger':'focus','data-placement':align,'data-content':content});
  $('#'+id).popover('show');
  $("#"+id).click(function(){$("#"+id).popover('destroy')});
}

/*--- ver alerta costado superior derecho ---*/
function msj(tipo){
    var img="";
    var clase="";
    switch(tipo){
      case "ok": clase="success"; img=base+"libraries/img/sistema/alert-success.png"; texto="Datos Actualizados con éxito !!!"; break;
      case "fail": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="Error en la actualización, revise los datos porfavor..."; break;
      case "error":  clase="danger"; img=base+"libraries/img/sistema/alert-danger.png"; texto="Datos no actualizados, problemas de conexion !!!"; break;
      case "problem":  clase="danger"; img=base+"libraries/img/sistema/alert-danger.png"; texto="problemas con la conexion al servidor!!!"; break;
      case "validate": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="Datos no actualizados, usuario o contraseña incorrecta !!!"; break;
      case "numero_orden_exist": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="Datos no actualizados, Número de orden ya registrado, actualice número de orden !!!"; break;
      case "numero_pedido_exist": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="Datos no actualizados, Número de pedido ya registrado, actualice número de pedido!"; break;
      case "numero_folio_exist": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="Datos no actualizados, Número de folio ya registrado, actualice número de orden !!!"; break;
      case "error_type": clase="danger"; img=base+"libraries/img/sistema/alert-danger.png"; texto="Tipo de archivo no aceptado, verifique el(los) archivo(s)"; break;
      case "error_size_img": clase="danger"; img=base+"libraries/img/sistema/alert-danger.png"; texto="Tamaño de imagen muy grande, verifique que la imagen tenga un tamaño menor a 1MB"; break;
      case "ci_exist": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="El NIT o CI ya se encuentra registrado en el sistema, revise los datos en clientes,proveedores o empleados porfavor..."; break;
      case "name_user_exist": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="El nombre de usuario ya en encuentra en uso, ingreso otro nombre de usuario."; break;
      case "user_registrer": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="El usuario ya en encuentra registrado."; break;
      case "exists_cuenta": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="El código de la cuenta ya existe"; break;//caso plan de cuentas
      case "file_error": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="Imposible leer el archivo, verifique que el archivo no este protegido contra escritura y vuelva a intentarlo."; break;//caso plan de cuentas
      case "file_content_error": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="Archivo no admitido, el contenido del archivo no cumple con el formato establecido en el sistema, es decir no tiene al menos 4 columnas y mas de una registro de hora."; break;//caso plan de cuentas
      case "load": clase="loading"; img=base+"libraries/img/sistema/alert-load.gif"; texto="Realizando cambios... espere por favor"; break;
      default: clase="default"; img=base+"libraries/img/sistema/alert-warning.png"; texto=tipo; break;
    }
    $("#alerta").stop(); 
    $("#alerta").removeAttr('style');
    $("#alerta").removeAttr('class');
    $("#alerta").css({'opacity':'0.93'});
    $("#alerta").attr({'class':'alert alert-'+clase});
    $("#alerta_text").text(texto);
    $("#alerta_img").attr({'src':img});
    $("#alerta_img").attr({'class':"img-thumbnail img-circle"});
    if(tipo!="load"){
      $("#alerta").fadeOut(7000);
      $("#alerta").hover(function(){ $("#alerta").stop(); $("#alerta").css({'opacity':'.95'});});
      //$("#alerta").mouseout(function(){ $("#alerta").fadeOut(2000);});
    }
}
function close_alerta(id){
  $("#"+id).css({display:'none'});
}
function imprimir(id){
  var contents = $("#"+id).html();
  var css='<link rel="stylesheet" type="text/css" href="'+base+'libraries/css/print.css" media="print">';
  var frame1 = $('<iframe />');
  frame1[0].name = "frame1";
  frame1.css({ "position": "absolute", "top": "-1000000px" });
  $("body").append(frame1);
  var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
  frameDoc.document.open();
  frameDoc.document.write('<html><head>'+css+'</head><body>');
  frameDoc.document.write(contents);
  frameDoc.document.write('</body></html>');
  frameDoc.document.close();
  setTimeout(function () { window.frames["frame1"].focus(); window.frames["frame1"].print(); frame1.remove();}, 600);
}
function Onfocus(id){
  if($(this).width()>=1100){
    $("#"+id).focus();
  }
}
function OnBlur(id){  
  if($(this).width()<=1100){
    $("#"+id).blur();
  }
}

function color_button(ele,color,diametro){
  ele.className="";
  ele.className="btn-circle btn-circle-"+color+" btn-circle-"+diametro;
  //ele.innerHTML="<div class='btn-circle btn-circle-"+color+" btn-circle-"+diametro+"'></div>";
}

/*para ingreso y salida de materiales o productos*/
function block(ele,pos){
  var a=ele.value;
    if(!esNumero(a)){ 
      ele.value=0;
    }else{ 
      if($("#i"+pos).val()>0 || $("#s"+pos).val()>0){
        var saldo=parseFloat($("#c"+pos).val())+parseFloat($("#i"+pos).val())-parseFloat($("#s"+pos).val());  
        $("#sa"+pos).val(saldo); 
        if(saldo<0){
          input_invalido('sa'+pos)
        }else{
          input_valido('sa'+pos);
        }
      }
    }
}
function input_valido(id){
  var ele=$("#"+id);
  ele.css({background: "rgba(0,64,0,0.5)",color: "white"});

}
function input_invalido(id){
  var ele=$("#"+id);
  ele.css({background: "rgba(255,0,0,0.5)",color: "white"});
}
function reset_rev_mov(val,id){
  $("#c"+id).val(val);
  $("#i"+id).val(0);
  $("#s"+id).val(0);
  $("#sa"+id).val(val);
  $("#sa"+id).removeAttr('style');
}

/*esconde search*/
function Disabled(id){
  $("#"+id).css({"display":"none"});
}
function Visible(id){
  $("#"+id).css({"display":"visible"});
}


/*validaciones*/
function alfaNumerico(cad,min,max){
  var val="^[a-z0-9áÁéÉíÍóÓúÚñÑ]{"+min+","+max+"}$";
  var expr=new RegExp(val);
  if(cad.match(expr)){
    return true;
  }else{
    return false;
  }
}

function strSpace(cad,min,max){
  var val="^[a-z A-Z0-9áÁéÉíÍóÓúÚñÑ)+-.,:(°;ªº]{"+min+","+max+"}$";
  var expr=new RegExp(val);
  if(cad.match(expr)){
    return true;
  }else{
    return false;
  }
}

function strNoSpace(cad,min,max){
  var val="^[a-zA-Z0-9áÁéÉíÍóÓúÚñÑ)+-.,:(°;ªº]{"+min+","+max+"}$";
  var expr=new RegExp(val);
  if(cad.match(expr)){
    return true;
  }else{
    return false;
  }
}
function textarea(cad,min,max){
  var val="^[a-z A-Z0-9áÁéÉíÍóÓúÚñÑ)+-.,:(°;ªº]{"+min+","+max+"}$";
  var expr=new RegExp(val);
  if(cad.match(expr)){
    return true;
  }else{
    return false;
  }
}
function entero(num,min,max){
  if(isset(num) && isset(min) && isset(max)){
      if(esNumero(num)){
      var val="^[0-9]{"+min+","+max+"}$";
      var expr=new RegExp(val);
      if(num.match(expr)){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }else{
    return false;
  }
}
function decimal(num,maxentero,maxdecimal){
  if(isset(num) && isset(maxentero) && isset(maxdecimal)){
    var val="^[0-9]{1,"+maxentero+"}[.]?[0-9]{0,"+maxdecimal+"}$";
    var expr=new RegExp(val);
    if(num.match(expr)){
      return true;
    }else{
      return false;
    }
  }else{
    return false;
  }
}
function fecha(fecha){
  var val="^[0-9]{4,4}[-][0-9]{2,2}[-][0-9]{2,2}$";
  var expr=new RegExp(val);
  if(fecha.match(expr)){
    var vf=fecha.split("-");
    if(parseFloat(vf[0])>=1600 && parseFloat(vf[1])>0 && parseFloat(vf[1])<=12 && parseFloat(vf[2])>0 && parseFloat(vf[2])<=31){
      return true;      
    }else{
      return false;
    }
  }else{
    return false;
  } 
}
function password(cad,min,max){
  var val="^[a-z0-9]{"+min+","+max+"}$";
  var expr=new RegExp(val);
  if(cad.match(expr)){
    return true;
  }else{
    return false;
  }
}
function direccion(cad,min,max){
  var val="^[a-z A-Z0-9áÁéÉíÍóÓúÚñÑ/ªº)+-.,:(;]{"+min+","+max+"}$";
  var expr=new RegExp(val);
  if(cad.match(expr)){
    return true;
  }else{
    return false;
  }
}
function email(cad){
  var val="^[_a-z.0-9-.]*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$";
  var expr=new RegExp(val);
  if(cad.match(expr)){
    return true;
  }else{
    return false;
  }
}
function webs(cad){
  var val="^(https?:\/\/)?([\da-zA-Z\.-]+)\.([a-z\.]{2,6})([\/\w \?=.-]*)*\/?$";
  var expr=new RegExp(val);
  if(cad.match(expr)){
    return true;
  }else{
    return false;
  }
}
/*validadciones*/
function esNumero(nro){
  if(!isNaN(nro) & nro != null & !/^\s*$/.test(nro) & nro>=0)
    return true;
      return false;
}
function esCadena(cad){
  if(cad == null || cad.length == 0 || /^\s*$/.test(cad))
    return false;
      return true;
}
function isset(val){
  if(typeof val!='undefined'){
    return true;
  }else{
    return false;
  }
}
function number_format(num,des,decimal,miles){
  var res=0;
  if(num!="" && des!=""){
    var dig=10*des;
    res=Math.round(num*dig)/dig;
  }
  return res;
}
/*--- Manejo de cadenas ---*/
function mayuscula(cad) {
  return cad.toUpperCase();
}
function minuscula(cad) {
  return cad.toLowerCase();
}
function fotografia(img){modal("","xs","4");btn_modal('',"",'',"",'4','xs');$("#content_4").html("<img src='"+img+"' width='100%'>");}

