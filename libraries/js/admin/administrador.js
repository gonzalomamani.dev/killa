 var x=$(document);
 x.ready(inicio);
function inicio(){
	$("#usuario").click(function(){get_2n('administrador/search_usuario',{},'search',false,'administrador/view_usuario',{},'contenido',true); activar('usuario','Usuarios de sistema','administrador?p=1');});
	$("#privilegio").click(function(){get_2n('administrador/search_privilegio',{},'search',false,'administrador/view_privilegio',{},'contenido',true); activar('privilegio','Privilegios de usuario','administrador?p=2');});
}
function reset_all_view(id){
	//reset search usuario
	if(id!="s_ci"){ $("#s_ci").val(""); }
	if(id!="s_nom"){ $("#s_nom").val(""); }
	if(id!="s_tip"){ $("#s_tip").val(""); }
	if(id!="s_car"){ $("#s_car").val(""); }
	if(id!="s_usu"){ $("#s_usu").val(""); }
}
/*------- MANEJO DE USUARIOS -------*/
   	/*--- Buscador ---*/
	function view_usuarios(){
		var atrib=new FormData();
   		atrib.append('ci',$("#s_ci").val());
   		atrib.append('nom',$("#s_nom").val());
   		atrib.append('tip',$("#s_tip").val());
   		atrib.append('car',$("#s_car").val());
   		atrib.append('usu',$("#s_usu").val());
   		get('administrador/view_usuario',atrib,'contenido',true);
   		return false;
	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	function all_usuarios(){
   		reset_all_view('');
   		view_usuarios();
   	}
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
   	function new_usuario(){
		modal('USUARIO DE SISTEMA: Nuevo','md','1');
		btn_modal('save_usuario',"",'',"",'1','md');
		get('administrador/new_usuario',{},'content_1',true);
   	}
   	function search_ci(){
   		var ci=$("#n_ci").val();
   		if(ci!=""){
	   		var inputs="n_fot|n_ciu|n_nom1|n_nom2|n_pat|n_mat|n_car";
	   		var atrib=new FormData();
	   		atrib.append('ci',ci);
	   		get_input('administrador/search_ci',atrib,"NULL",inputs,false);
   		}
   	}
   	function save_usuario(){
		var fot=$('#n_fot').prop('files');
		var ci=$('#n_ci').val();
		var ciudad=$('#n_ciu').val();
		var nom=$('#n_nom1').val();
		var nom2=$('#n_nom2').val();
		var pat=$('#n_pat').val();
		var mat=$('#n_mat').val();
		var tus=$('#n_tus').val();
		var usu=$('#n_usu').val();
		var car=$('#n_car').val();
		if(entero(ci,6,8) && ci>=100000 && ci<=99999999){
			if(entero(ciudad,0,10)){
				if(strSpace(nom,2,20)){
						if(entero(tus,0,1) && tus>=0 && tus<=1){
							if(strNoSpace(usu,4,30)){
									var control=true;
									if(nom2!=""){ if(!strSpace(nom2,2,20)){ control=false; alerta('Ingrese un nombre válido',"top",'n_nom2');}}
									if(pat!=""){ if(!strSpace(pat,2,20)){ control=false; alerta('Ingrese un apellido válido',"top",'n_mat');}}
									if(mat!=""){ if(!strSpace(mat,2,20)){ control=false; alerta('Ingrese Apellido Paterno válido',"top",'n_pat');}}
									if(car!=""){ if(!strSpace(car,0,100)){ control=false; alerta('Ingrese un contenido válido',"top",'n_car');}}
									if(control){
										var atribs= new FormData();
										atribs.append("ci",ci);
										atribs.append("ciu",ciudad);
										atribs.append("nom1",nom);
										atribs.append("nom2",nom2);
										atribs.append("pat",pat);
										atribs.append("mat",mat);
										atribs.append("tus",tus);
										atribs.append("usu",usu);
										atribs.append("car",car);
										atribs.append("archivo",fot[0]);
										var atrib3=new FormData();atrib3.append('ci',$("#s_ci").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('tip',$("#s_tip").val());atrib3.append('car',$("#s_car").val());atrib3.append('usu',$("#s_usu").val());
										set('administrador/save_usuario',atribs,'NULL',true,'administrador/view_usuario',atrib3,'contenido',false,'1');
									}
							}else{
								alerta('Ingrese un valor válido',"top",'n_usu');
							}
						}else{
							alerta('Seleccione un tipo de empleado','top','n_tus');
						}
				}else{
					alerta('El campo de primer nombre es obligatorio',"top",'n_nom1');
				}
			}else{
				alerta('Seleccione una ciudad','top','n_ciu');
			}
		}else{
			alerta('Ingrese un nro de Carnet Válido',"top",'n_ci');
		}
		return false;
   	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	function imprimir_usuario(json){
   		modal("USUARIOS DE SISTEMA: Configuracion de impresion","xlg","1");
 		btn_modal('',"",'imprimir',"'area'",'1','xlg');
 		var atrib=new FormData();atrib.append('json',json);
 		get_2n('administrador/imprimir_usuario',atrib,'content_1',true,'administrador/arma_usuario',atrib,'area',true);
   	}
   	function arma_usuario(json){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var v6=""; if($("#6:checked").val()){v6='ok'}
		var v7=""; if($("#7:checked").val()){v7='ok'}
		var nro=$("#nro").val();
		var atrib=new FormData();
		atrib.append('json',json);
		atrib.append('v1',v1);
		atrib.append('v2',v2);
		atrib.append('v3',v3);
		atrib.append('v4',v4);
		atrib.append('v5',v5);
		atrib.append('v6',v6);
		atrib.append('v7',v7);
		atrib.append('nro',nro);
		get('administrador/arma_usuario',atrib,'area',true);
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	function detalle_usuario(ide){
   		modal("USUARIO DE SISTEMA: Detalle","md","1");
 		btn_modal('',"",'',"'area'",'1','md');
 		var atrib=new FormData();atrib.append('ide',ide);
 		get('administrador/detalle_usuario',atrib,'content_1',true);
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	function config_usuario(ide){
		modal('USUARIO DE SISTEMA: Modificar','md','1');
		btn_modal('update_usuario',"'"+ide+"'",'',"",'1','md');
		var atrib=new FormData();atrib.append('ide',ide);
		get('administrador/config_usuario',atrib,'content_1',true);
   	}
   	function update_usuario(ide){
		var fot=$('#n_fot').prop('files');
		var ci=$('#n_ci').val();
		var ciudad=$('#n_ciu').val();
		var nom=$('#n_nom1').val();
		var nom2=$('#n_nom2').val();
		var pat=$('#n_pat').val();
		var mat=$('#n_mat').val();
		var tus=$('#n_tus').val();
		var usu=$('#n_usu').val();
		var car=$('#n_car').val();
		if(entero(ci,6,8) && ci>=100000 && ci<=99999999){
			if(entero(ciudad,0,10)){
				if(strSpace(nom,2,20)){
						if(entero(tus,0,1) && tus>=0 && tus<=1){
							if(strNoSpace(usu,4,30)){
									var control=true;
									if(nom2!=""){ if(!strSpace(nom2,2,20)){ control=false; alerta('Ingrese un nombre válido',"top",'n_nom2');}}
									if(pat!=""){ if(!strSpace(pat,2,20)){ control=false; alerta('Ingrese un apellido válido',"top",'n_mat');}}
									if(mat!=""){ if(!strSpace(mat,2,20)){ control=false; alerta('Ingrese Apellido Paterno válido',"top",'n_pat');}}
									if(car!=""){ if(!strSpace(car,0,100)){ control=false; alerta('Ingrese un contenido válido',"top",'n_car');}}
									if(control){
										var atribs= new FormData();
										atribs.append("ide",ide);
										atribs.append("ci",ci);
										atribs.append("ciu",ciudad);
										atribs.append("nom1",nom);
										atribs.append("nom2",nom2);
										atribs.append("pat",pat);
										atribs.append("mat",mat);
										atribs.append("tus",tus);
										atribs.append("usu",usu);
										atribs.append("car",car);
										atribs.append("archivo",fot[0]);
										var atrib3=new FormData();atrib3.append('ci',$("#s_ci").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('tip',$("#s_tip").val());atrib3.append('car',$("#s_car").val());atrib3.append('usu',$("#s_usu").val());
										set('administrador/update_usuario',atribs,'NULL',true,'administrador/view_usuario',atrib3,'contenido',false,'1');
									}
							}else{
								alerta('Ingrese un valor válido',"top",'n_usu');
							}
						}else{
							alerta('Seleccione un tipo de empleado','top','n_tus');
						}
				}else{
					alerta('El campo de primer nombre es obligatorio',"top",'n_nom1');
				}
			}else{
				alerta('Seleccione una ciudad','top','n_ciu');
			}
		}else{
			alerta('Ingrese un nro de Carnet Válido',"top",'n_ci');
		}
		return false;
   	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	function confirmar_usuario(ide){
   		modal("USUARIO DE SISTEMA: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','drop_usuario',"'"+ide+"'");
   		var atribs=new FormData();atribs.append('ide',ide);
   		get('administrador/confirmar_usuario',atribs,'content_5',true);
   	}
   	function drop_usuario(ide){
		var ele=new FormData();
		ele.append('u',$("#e_user").val());
		ele.append('p',$("#e_password").val());
		ele.append('ide',ide);
		var atrib3=new FormData();atrib3.append('ci',$("#s_ci").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('tip',$("#s_tip").val());atrib3.append('car',$("#s_car").val());atrib3.append('usu',$("#s_usu").val());
		set('administrador/drop_usuario',ele,'NULL',true,'administrador/view_usuario',atrib3,'contenido',false,'5');
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE USUARIOS -------*/
/*------- MANEJO DE PRIVILEGIOS -------*/
   	/*--- Buscador ---*/
	function view_privilegio(){
		var atrib=new FormData();
   		atrib.append('ci',$("#s_ci").val());
   		atrib.append('nom',$("#s_nom").val());
   		get('administrador/view_privilegio',atrib,'contenido',true);
   		return false;
	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	function all_privilegio(){
   		reset_all_view('');
   		view_privilegio();
   	}
   	/*--- Nuevo ---*/
	function update_privilegio(idpri,col,ele,color,diametro){
		set_button('administrador/update_privilegio',{idpri,col},"",{},"",false,ele,color,diametro);
	}
	function detail_modal(idpri,col){
		switch(col){
			case "al": var v="almacen"; break;
			case "pr": var v="produccion"; break;
			case "mo": var v="movimientos"; break;
			case "ca": var v="capital humano"; break;
			case "cl": var v="cliente/proveedor"; break;
			case "ac": var v="activos fijos"; break;
			case "ot": var v="otros materiales e insumos"; break;
			case "co": var v="contabilidad"; break;
			case "ad": var v="administracion"; break;
		}
   		modal("PRIVILEGIOS: Detalle de "+v,"md","1");
 		btn_modal('',"",'',"",'1','md');
		var atrib=new FormData();atrib.append('idpri',idpri);atrib.append('col',col);
		get('administrador/detail_modal',atrib,'content_1',true);
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE PRIVILEGIOS -------*/

















function save_grupo() {
	var nom=$("#nom_gru").val();
	var des=$("#des_gru").val();
	if(esCadena(nom)){
		post_set("admin/save_grupo",{nom:nom,des:des},'admin/view_mmpp',{},'content')
	}else{
		msj("Ingrese un Nombre Valido","alert_info",'modal_alert');
	}
}

function mod_grupo(id){
  var nom=$("#mod_nom_gru"+id).val();
  var des=$("#mod_des_gru"+id).val();
  if(esCadena(nom)){
  	post_set("admin/mod_grupo",{id:id,nom:nom,des:des},'admin/view_mmpp',{},'content');
  }else{
  	msj("Ingrese un Nombre Valido","alert_info",'modal_alert')
  }
}

function drop_grupo(id){
	post_set("admin/drop_grupo",{id:id},'admin/view_mmpp',{},'content');
}

function mod_unidad(id){
  var nom=$("#mod_uni_nom"+id).val();
  var des=$("#mod_uni_abr"+id).val();
  var equ=$("#mod_uni_equ"+id).val();
  if(esCadena(nom)){
  	post_set("admin/mod_unidad",{id:id,nom:nom,des:des,equ:equ},'admin/view_mmpp',{},'content');
  }else{
  	msj("Ingrese un Nombre Valido","alert_info",'modal_alert')
  }
}
function drop_unidad(id){
	post_set("admin/drop_unidad",{id:id},'admin/view_mmpp',{},'content');
}

function save_color() {
	var nom=$("#nom_col").val();
	var des=$("#cod_col").val();
	if(esCadena(nom)){
		post_set("admin/save_color",{nom:nom,cod:des},'admin/view_mmpp',{},'content');
	}else{
		msj("Ingrese un Nombre Valido","alert_info",'modal_alert')
	}
}
function mod_color(id){
  var nom=$("#mod_col_nom"+id).val();
  var des=$("#mod_col_abr"+id).val();
  if(esCadena(nom)){
  	post_set("admin/mod_color",{id:id,nom:nom,cod:des},'admin/view_mmpp',{},'content');
  }else{
  	msj("Ingrese un Nombre Valido","alert_info",'modal_alert')
  }
}
function drop_color(id){
	post_set("admin/drop_color",{id:id},'admin/view_mmpp',{},'content');
}

function save_proveedor(){
	var nom=$("#nom_pro").val();
	var telf=$("#telf_pro").val();
	var dir=$("#dir_pro").val();
	//alert(nom+" "+telf+" "+dir);
	if(esCadena(nom)){
		post_set("admin/save_proveedor",{nom:nom,telf:telf,dir:dir},'admin/view_mmpp',{},'content');
	}else{
		msj("Ingrese un Nombre Valido","alert_info",'modal_alert')
	}
}
function change_proveedor(id){
	var nom=$("#mod_nom_pro"+id).val();
	var telf=$("#mod_tel_pro"+id).val();
	var dir=$("#mod_dir_pro"+id).val();
	//alert(nom+" "+telf+" "+dir);
	if(esCadena(nom)){
		post_set("admin/change_proveedor",{id:id,nom:nom,telf:telf,dir:dir},'admin/view_mmpp',{},'content');
	}else{
		msj("Ingrese un Nombre Valido","alert_info",'modal_alert');
	}
}
function drop_proveedor(id){
	post_set("admin/drop_proveedor",{id:id},'admin/view_mmpp',{},'content');
}

//usuario de sistema
function search_user(id){
	var ci=$("#search_ci").val();
	var nombre=$("#search_nombre").val();
	var paterno=$("#search_paterno").val();
	var materno=$("#search_materno").val();
	var cargo=$("#search_cargo").val();
	var atrib="";
	var val="";
	if(esNumero(ci)){
		atrib="ci",val=ci;
	}else{
		if(esCadena(nombre)){
			atrib="nombre",val=nombre;
		}else{
			if(esCadena(paterno)){
				atrib="paterno",val=paterno;
			}else{
				if(esCadena(materno)){
					atrib="materno",val=materno;
				}else{
					if(esCadena(cargo)){
						atrib="cargo",val=cargo;
					}else{
						msj("Ingrese un campo de busqueda","alert_info",'modal_alert')
					}
				}
			}
		}
	}
	if(atrib!="" & val!=""){
		post_get("admin/view_search_usuario",{search:atrib,val:val},id);
	}
}

function save_user(){
	var ced=$("#ci_user").val();
	var nom=$("#nom_user").val();
	var nom2=$("#nom2_user").val();
	var pat=$("#pat_user").val();
	var mat=$("#mat_user").val();
	var car=$("#car_user").val();
	var fot=$("#img_user").attr("name");
	
	
	if(esNumero(ced) & /^[0-9]{7,8}$/.test(ced)){
		if(esCadena(nom) & /^[A-Za-záéíóúñÁÉÍÓÚÑ]{3,20}$/.test(nom)){
			if(esCadena(pat) & /^[A-Za-záéíóúñÁÉÍÓÚÑ]{2,20}$/.test(pat)){
				post_set('admin/save_user',{ced:ced,nom:nom,nom2:nom2,pat:pat,mat:mat,car:car,fot:fot},'admin/view_all_usuario',{},'result')
			}else{
				msj("Ingrese un apellido paterno","alert_info",'modal_alert');
			}
		}else{
			msj("Ingrese un Nombre","alert_info",'modal_alert');
		}
	}else{
		msj("Ingrese un numero de Carnet valido","alert_info",'modal_alert');
	}
	
}


function all_user(id){
	post_get("admin/view_all_usuario",{},id);
}

function new_user(){
	post_get("admin/view_new_user",{},'result');
}



function msj(msj,action){
	$("#"+action+"_text").html(msj);
	visible(action,'15%','15%');
}
function esNumero(nro){
	if(!isNaN(nro)  & nro != null & !/^\s*$/.test(nro) & nro>=0)
		return true;
			return false;
}
function esCadena(cad){
	if(cad == null || cad.length == 0 || /^\s*$/.test(cad))
		return false;
			return true;
}

/*function reset_all_view(id){
	if(id!="search_ci"){
		$("#search_ci").val("");
	}
	if(id!="search_nombre"){
		$("#search_nombre").val("");
	}
	if(id!="search_paterno"){
		$("#search_paterno").val("");
	}
	if(id!="search_materno"){
		$("#search_materno").val("");
	}
	if(id!="search_cargo"){
		$("#search_cargo").val("");
	}
	
}*/

/*funciones encargadas del feriado*/
	function save_feriado(){
		var fe=$("#fec").val();
		var de=$("#des").val();
		if(esFecha(fe)){
			post_set("admin/new_feriado",{fecha:fe,des:de},'admin/view_capital_humano',{},'content');
		}else{
			msj("Seleccione una fecha válida","alert_info",'modal_alert');
		}
	}
	function drop_feriado(id){
		post_set("admin/drop_feriado",{id:id},'admin/view_capital_humano',{},'content');
	}
	function mod_feriado(id){
		var fe=$("#fec"+id).val();
		var de=$("#des"+id).val();
		
		if(esFecha(fe)){
			post_set("admin/mod_feriado",{id:id,fecha:fe,des:de},'admin/view_capital_humano',{},'content');
		}else{
			msj("Seleccione una fecha válida","alert_info",'modal_alert');
		}
	}


	/*function save_proceso(){
		var no=$("#nom").val();
		var de=$("#des").val();
		if(esCadena(no)){
			post_set("admin/new_proceso",{no:no,de:de},'admin/view_produccion',{},'content');
		}else{
			msj("Ingrese una nombre válido","alert_info",'modal_alert');
		}
	}
	function mod_proceso(id){
		var no=$("#nom"+id).val();
		var de=$("#des"+id).val();
		if(esCadena(no)){
			post_set("admin/update_proceso",{id:id,no:no,de:de},'admin/view_produccion',{},'content');
		}else{
			msj("Ingrese una nombre válido","alert_info",'modal_alert');
		}
	}
	function drop_proceso(id){
			post_set("admin/delete_proceso",{id:id},'admin/view_produccion',{},'content');
	}*/

	function save_material_indirecto(){
		var n=$("#material").val();
		if(esCadena(n)){
			post_set("admin/new_material_indirecto",{n:n},'admin/view_produccion',{},'content');
		}else{
			msj("Ingrese una nombre de material indirecto","alert_info",'modal_alert');
		}
	}
	function update_material_indirecto(m){
		var n=$("#nom_m"+m).val();
		if(esCadena(n)){
			post_set("admin/update_material_indirecto",{m:m,n:n},'admin/view_produccion',{},'content');
		}else{
			msj("Ingrese una nombre de material indirecto","alert_info",'modal_alert');
		}
	}
	function drop_material_indirecto(m){
		post_set("admin/drop_material_indirecto",{m:m},'admin/view_produccion',{},'content');
	}


