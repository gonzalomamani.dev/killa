 var base;
function init(b){
  base=b;
}
 var x=$(document);
 x.ready(inicio);
 function inicio(){
  get_2n('taller/search_empleado',{},'search',false,'taller/view_empleado',{},'contenido',true);
  activar("","Taller de producción","taller");
 }
function get(ruta,atrib,id,control){//con mensaje de inicio
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:9000,
    beforeSend: function(){ if(control){ inicioEnvio(id)}},
    success: function(result){llegada(id,result)},
    error:function(){problemas(id)}
  }); 
  return false;
}
function get_2n(ruta1,atrib1,id1,control1,ruta2,atrib2,id2,control2){
  if(ruta1!=""){
    Visible(id1);
    $.ajax({
        url:ruta1,
        async:true,
        type: "POST",
        contentType:false,
        data:atrib1,
        processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
        timeout:9000,
        beforeSend: function(){ if(control1){ inicioEnvio(id1);}},
        success: function(result){llegada_2n(id1,result,ruta2,atrib2,id2,control2)},
        error: function(){problemas(id1)}
    });
  }else{
    Disabled(id1);
    get(ruta2,atrib2,id2,control2);
  }
  return false;
}
function inicioEnvio(div){$("#"+div).html("<img src='"+base+"libraries/img/sistema/load.gif'>");}
function llegada(div,result){ 
  
    $("#"+div).html(result);
    try{
      refresh_totales_compras();//solo para el caso de actualizar compras (nuevo y modificar);
    }catch(e){}
    try{
      refresh_totales_cuentas();//solo para el caso de actualizar cuenta (nuevo y modificar);
    }catch(e){}

}
function llegada_2n(div,result,ruta,atrib,id,control){
  if(result!="fail"){ 
    $("#"+div).html(result);
    get(ruta,atrib,id,control);
  }else{
    window.location.href=base;
  }
}
function set_open_modal(ruta,atrib,id,control,ruta2,atrib2,id2,control2,titulo,nivel,ancho,id_closed){
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:240000,
    beforeSend: function(){ if(control){ msj('load');}},
    success: function(result){ llegada_set_open_modal(result,ruta2,atrib2,id2,control2,titulo,nivel,ancho,id_closed)},
    error:function(){problemas(id)}
  }); 
}
function llegada_set_open_modal(result,ruta,atrib,id,control,titulo,nivel,ancho,id_closed){
  switch(result){
    case "ok": 
      msj(result); 
      if(id_closed!="NULL"){ 
        hide_modal(id_closed);
      }
      if(ruta!=''){ 
         modal(titulo,ancho,nivel);
         btn_modal('',"",'',"",nivel,ancho);
         get(ruta,atrib,id,control);
      }
      break;
    default: msj(result); break;
  } 
}
function set(ruta,atrib,id,control,ruta2,atrib2,id2,control2,id_closed){
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:240000,
    beforeSend: function(){ if(control){ msj('load');}},
    success: function(result){ llegada_set(result,ruta2,atrib2,id2,control2,id_closed)},
    error:function(){problemas(id)}
  }); 
}
function llegada_set(result,ruta,atrib,id,control,id_closed){
  switch(result){
    case "ok": msj(result); if(id_closed!="NULL"){ hide_modal(id_closed);}; if(ruta!=''){ get(ruta,atrib,id,control);}break;
    case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base; break;
    default: msj(result); break;
  } 
}
function problemas(div){ msj("problem");}
function activar(activo,titulo,dir){
  var padre=document.getElementById("menu-top").firstChild;//obtenemos todos los elemntos <li>
  while(padre!=null){//recorremos todos los <li>
    if(padre.nodeType==Node.ELEMENT_NODE){
        var hijo=padre.firstChild;
        if(hijo.id==activo){
          padre.className="activado";
          hijos=padre.firstChild;
          hijos=hijos.firstChild;
          while(hijos!=null){
            if(hijos.id=="txt_btn"){
              hijos.className="";
              break;
            }
            hijos=hijos.nextSibling;
          }
        }else{
          padre.className="";
          hijos=padre.firstChild;
          hijos=hijos.firstChild;
          while(hijos!=null){
            if(hijos.id=="txt_btn"){
              hijos.className="g-btn-sm";
              break;
            }
            hijos=hijos.nextSibling;
          }
        }
      }
      padre=padre.nextSibling;
  }
  url(titulo,dir);
}
function url(titulo,url){
  var data={foo: "bar"};
  $(document).attr('title',titulo);
  window.history.pushState(data,titulo,url);
}
function Visible(id){
  $("#"+id).css({"display":"visible"});
}
function msj(tipo){
    var img="";
    var clase="";
    switch(tipo){
      case "ok": clase="success"; img=base+"libraries/img/sistema/alert-success.png"; texto="Datos Actualizados con éxito !!!"; break;
      case "fail": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="Error en la actualización, revise los datos porfavor..."; break;
      case "error":  clase="danger"; img=base+"libraries/img/sistema/alert-danger.png"; texto="Datos no actualizados, problemas de conexion !!!"; break;
      case "problem":  clase="danger"; img=base+"libraries/img/sistema/alert-danger.png"; texto="problemas con la conexion al servidor!!!"; break;
      case "validate": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="Contraseña incorrecta !!!"; break;
      case "validate_distint": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="Las nuevas contraseñas no coinciden..."; break;
      case "load": clase="loading"; img=base+"libraries/img/sistema/alert-load.gif"; texto="Realizando cambios... espere por favor"; break;
      default: clase="default"; img=base+"libraries/img/sistema/alert-warning.png"; texto=tipo; break;
    }
    $("#alerta").stop(); 
    $("#alerta").removeAttr('style');
    $("#alerta").removeAttr('class');
    $("#alerta").css({'opacity':'0.93'});
    $("#alerta").attr({'class':'alert alert-'+clase});
    $("#alerta_text").text(texto);
    $("#alerta_img").attr({'src':img});
    $("#alerta_img").attr({'class':"img-thumbnail img-circle"});
    if(tipo!="load"){
      $("#alerta").fadeOut(7000);
      $("#alerta").hover(function(){ $("#alerta").stop(); $("#alerta").css({'opacity':'.95'});});
      //$("#alerta").mouseout(function(){ $("#alerta").fadeOut(2000);});
    }
}
function modal(encabezado,ancho,nivel){
  $(".titulo_"+nivel).html(encabezado);
  $("#dialog_"+nivel).attr({"class":"modal-dialog modal-"+ancho});
  if(nivel=="1"){
    $("#modal_"+nivel).modal({keyboard:true}); 
  }else{
    $("#modal_"+nivel).fadeIn(150);
    $("#dialog_"+nivel).fadeIn(0);
    $("#dialog_"+nivel).css({"transform":"translateY(0)","visibility":"visible"});
  }
}
function btn_modal(funcion_g,atribs_g,funcion_i,atribs_i,nivel,ancho){
  var id_g="modal_ok_"+nivel;
  var id_i="modal_print_"+nivel;
  if(funcion_g!=""){
    if(funcion_i!=""){//mostramos los tres botones
      var col="col-sm-2 col-xs-12";
      var offset="col-sm-offset-6";
      switch(ancho){
        case "md": col="col-sm-3 col-xs-12"; offset="col-sm-offset-3"; break;
        case "sm": col="col-sm-3 col-xs-12"; offset="col-sm-offset-3"; break;
        case "xs": col="col-sm-4 col-xs-12"; offset="";break;
      }
      $("#"+id_g).attr({"class":col+" "+offset});
      $("#"+id_i).attr({"class":col});
      $("#modal_closed_"+nivel).attr({"class":col+" btn-cerrar"});
      addClick_v2(id_g,funcion_g,atribs_g);
      addClick_v2(id_i,funcion_i,atribs_i);
    }else{//guardar y eliminar
        var col="col-sm-2 col-xs-12";
        var offset="col-sm-offset-8";
        switch(ancho){
          case "md": col="col-sm-3 col-xs-12"; offset="col-sm-offset-6"; break;
          case "sm": col="col-sm-3 col-xs-12"; offset="col-sm-offset-6"; break;
          case "xs": col="col-sm-4 col-xs-12"; offset="col-sm-offset-4";break;
        }
        $("#"+id_g).attr({"class":col+" "+offset});
        $("#"+id_i).attr({"class":"hidden"});
        $("#modal_closed_"+nivel).attr({"class":col+" btn-cerrar"});
        addClick_v2(id_g,funcion_g,atribs_g);    
    }
  }else{
    if(funcion_i!=""){
        var col="col-sm-2 col-xs-12";
        var offset="col-sm-offset-8";
        switch(ancho){
          case "md": col="col-sm-3 col-xs-12"; offset="col-sm-offset-6"; break;
          case "sm": col="col-sm-3 col-xs-12"; offset="col-sm-offset-6"; break;
          case "xs": col="col-sm-4 col-xs-12"; offset="col-sm-offset-4";break;
        }
        $("#"+id_g).attr({"class":"hidden"});
        $("#"+id_i).attr({"class":col+" "+offset});
        $("#modal_closed_"+nivel).attr({"class":col+" btn-cerrar"});
        addClick_v2(id_i,funcion_i,atribs_i);  
    }else{
        var col="col-sm-2 col-xs-12";
        var offset="col-sm-offset-10";
        switch(ancho){
          case "md": col="col-sm-3 col-xs-12"; offset="col-sm-offset-9"; break;
          case "sm": col="col-sm-3 col-xs-12"; offset="col-sm-offset-9"; break;
          case "xs": col="col-sm-4 col-xs-12"; offset="col-sm-offset-8";break;
        }
        $("#"+id_g).attr({"class":"hidden"});
        $("#"+id_i).attr({"class":"hidden"});
        $("#modal_closed_"+nivel).attr({"class":col+" "+offset+" btn-cerrar"});      
    }
  }
}
function addClick_v2(id,funcion,atribs){
  var ele=$("#"+id+" button");
  if(funcion!=""){
    ele.attr({"onclick":funcion+"("+atribs+")"});
  }else{
    ele.attr({"onclick":""});
  }
}
function close_alerta(id){
  $("#"+id).css({display:'none'});
}
function Onfocus(id){
  if($(this).width()>=1100){
    $("#"+id).focus();
  }
}
$("#modal_closed_2").click(function() { hide_modal("2");});
$(".closed_2").click(function() { hide_modal("2");});
$(document).click(function(e){ if($(e.target).attr("id")=="modal_2"){ hide_modal("2");} });

$("#modal_closed_3").click(function() { hide_modal("3");});
$(".closed_3").click(function() { hide_modal("3");});
$(document).click(function(e){ if($(e.target).attr("id")=="modal_3"){ hide_modal("3");} });

$("#modal_closed_4").click(function() { hide_modal("4");});
$(".closed_4").click(function() { hide_modal("4");});
$(document).click(function(e){ if($(e.target).attr("id")=="modal_4"){ hide_modal("4");} });

$("#modal_closed_5").click(function() { hide_modal("5");});
$(".closed_5").click(function() { hide_modal("5");});
$(document).click(function(e){ if($(e.target).attr("id")=="modal_5"){ hide_modal("5");} });
function hide_modal(nivel){//cerrar modal bootstrap
  switch(nivel){
    case "1": $("#modal_"+nivel).modal('hide'); break;
    default: $("#modal_"+nivel).fadeOut(250);$("#dialog_"+nivel).fadeOut(150); $("#dialog_"+nivel).css("transform","translateY(-200%)"); break;
  }
}
function alerta(content,align,id){
  $("#"+id).attr({'data-trigger':'focus','data-placement':align,'data-content':content});
  $('#'+id).popover('show');
  $("#"+id).click(function(){$("#"+id).popover('destroy')});
}
function textarea(cad,min,max){
  var val="^[a-z A-Z0-9áÁéÉíÍóÓúÚñÑ)+-.,:(;ªº]{"+min+","+max+"}$";
  var expr=new RegExp(val);
  if(cad.match(expr)){
    return true;
  }else{
    return false;
  }
}
function alfaNumerico(cad,min,max){
  var val="^[a-z0-9]{"+min+","+max+"}$";
  var expr=new RegExp(val);
  if(cad.match(expr)){
    return true;
  }else{
    return false;
  }
}
/*----------------ALGORITM--------------*/
function view_empleado(){
  var nombre=$("#nombre").val();
  var atrib=new FormData();
  atrib.append("nombre",nombre);
  get("taller/view_empleado",atrib,"contenido",true);
  return false;
}
function all_empleado(){
  $("#nombre").val(""); view_empleado();
}
function confirmar(ide){
  modal("EMPLEADO: Contraseña","xs","5");
  btn_modal('validar',"'"+ide+"'",'',"",'5','xs');
  var atrib=new FormData();atrib.append('ide',ide);
  get('taller/confirmar',atrib,'content_5',true);
}
function validar(ide){
  var p=$("#e_password").val();
  if(alfaNumerico(p,4,25)){
    var atrib=new FormData();
    atrib.append('p',p);
    atrib.append('id',ide);
    set_open_modal('taller/validar',atrib,'NULL',true,'taller/view_tareas',atrib,'content_1',true,'EMPLEADO: Tareas','1','xlg','5');
  }else{
    alerta("Debe ingresar una contraseña válida","top","e_password");
  }
}
function view_tareas(ide){
   var atrib=new FormData();atrib.append('id',ide);
   get('taller/view_tareas',atrib,'content_1',true);
}
function config_pass(ide){
  modal("EMPLEADO: Cambiar contraseña","xs","2");
  btn_modal('change_pass',"'"+ide+"'",'',"",'2','xs');
  var atrib=new FormData();atrib.append('ide',ide);
  get('taller/config_pass',atrib,'content_2',true);
}
function change_pass(ide){
  if(alfaNumerico($("#pass").val(),4,25)){
    if(alfaNumerico($("#n_pass").val(),4,25)){
      if($("#n_pass").val()==$("#n_pass2").val()){
        var atrib=new FormData();atrib.append('ide',ide);atrib.append('pass',$("#pass").val());atrib.append('n_pass',$("#n_pass").val());atrib.append('n_pass2',$("#n_pass2").val());
        set('taller/change_pass',atrib,'NULL',true,"",{},'',false,'2');
      }else{
        alerta("Los valores no coinciden","top","n_pass");
        alerta("Los valores no coinciden","top","n_pass2");
      }
    }else{
      alerta("Ingrese una contraseña válida","top","n_pass");
    }
  }else{
    alerta("Ingrese una contraseña válida","top","pass");
  }  
}
function alert_inicio(idpt,ide,tarea) {
  modal("EMPLEADO: Iniciar tarea ","xs","5");
  addClick_v2('modal_ok_5','control_tarea',"'"+idpt+"','"+ide+"'");
  var atrib=new FormData();atrib.append('idpt',idpt);
  atrib.append('titulo','iniciar la tarea '+tarea+' asignada');
  atrib.append('descripcion','Una vez iniciada la tarea el sistema estara en espera de la tarea terminada.');
  get('taller/alerta',atrib,'content_5',true);
}
function alert_fin(idpt,ide,tarea) {
  modal("EMPLEADO: Finalizar tarea ","xs","5");
  addClick_v2('modal_ok_5','control_tarea',"'"+idpt+"','"+ide+"'");
  var atrib=new FormData();atrib.append('idpt',idpt);
  atrib.append('titulo','finalizar la tarea '+tarea+' iniciada');
  atrib.append('descripcion','Una vez terminada la tarea el sistema borrara la tarea de la ventada actual y lo pondra en el historia de trabajo.');
  get('taller/alerta',atrib,'content_5',true);
}
function control_tarea(idpt,ide){
  var obs=$("#obs"+idpt).val();
  if(textarea(obs,0,500)){
    var atrib=new FormData();
    atrib.append('idpt',idpt);atrib.append('ide',ide);atrib.append('id',ide);atrib.append('obs',obs);
    set('taller/control_tarea',atrib,'NULL',true,'taller/view_tareas',atrib,'content_1',false,'5');
  }else{
    alerta("Ingrese una valor válido","top","obs"+idpt);
  }
}
function ficha_tecnica(idp,idpim,iddp){
  modal("PRODUCTO: Ficha técnica","md","2");
  btn_modal('',"",'',"",'2','md');
  var atrib=new FormData();atrib.append('idp',idp);atrib.append('idpim',idpim);atrib.append('iddp',iddp);
  get('taller/ficha_tecnica',atrib,'content_2',true);
}
function materiales(idp,idpim,iddp){
  modal("PRODUCTO: Materiales requeridos","md","2");
  btn_modal('',"",'',"",'2','md');
  var atrib=new FormData();atrib.append('idp',idp);atrib.append('idpim',idpim);atrib.append('iddp',iddp);
  get('taller/materiales',atrib,'content_2',true);
}
function view_historial(ide){
  modal("EMPLEADO: historial de trabajo","xlg","1");
  btn_modal('',"",'',"",'1','xlg');
  var atrib=new FormData();atrib.append('id',ide);
  get('taller/view_historial',atrib,'content_1',true);
}