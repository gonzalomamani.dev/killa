 var x=$(document);
 x.ready(inicio);
 function inicio(){
 	$('#pedido').click(function(){get_2n('pedidos_ventas/search_pedido',{},'search',false,'pedidos_ventas/view_pedido',{},'contenido',true);activar("pedido","Pedidos","pedidos_ventas?p=1");})
 	$('#venta').click(function(){get_2n('pedidos_ventas/search_venta',{},'search',false,'pedidos_ventas/view_venta',{},'contenido',true);activar("venta","Ventas","pedidos_ventas?p=2");})
 	$('#compra').click(function(){get_2n('pedidos_ventas/search_compra',{},'search',false,'pedidos_ventas/view_compra',{},'contenido',true);activar("compra","Compras o Egresos","pedidos_ventas?p=3");})
 	$('#config').click(function(){get_2n('',{},'search',false,'pedidos_ventas/view_config',{},'contenido',true);activar("config","Configuración de movimientos","pedidos_ventas?p=5");})
 }
 	 function reset_input(id){
	 	//Caso pedidos
		if(id!="sp_num"){ $("#sp_num").val("");}
		if(id!="sp_nom"){ $("#sp_nom").val("");}
		if(id!="sp_cli"){ $("#sp_cli").val("");}
		//Caso ventas
		if(id!="sv_nit"){ $("#sv_nit").val("");}
		if(id!="sv_raz"){ $("#sv_raz").val("");}
		if(id!="sv_fe1" && id!="sv_fe2"){ $("#sv_fe1").val("");$("#sv_fe2").val("");}
		//caso compras
		if(id!="sc_nit"){ $("#sc_nit").val("");}
		if(id!="sc_pro"){ $("#sc_pro").val("");}
		if(id!="sc_fe1" && id!="sc_fe2"){ $("#sc_fe1").val("");$("#sc_fe2").val("");}
		if(id!="sc_am"){ $("#sc_am").val("");}
	}
/*------- MANEJO DE ALMACENES -------*/
   	/*--- Buscador ---*/
   	function view_pedido(){
		var atrib3=new FormData();
		atrib3.append('num',$("#sp_num").val());
		atrib3.append('nom',$("#sp_nom").val());
		atrib3.append('cli',$("#sp_cli").val());
		get('pedidos_ventas/view_pedido',atrib3,'contenido',true);
		return false;
	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
	function all_pedido(){
		reset_input("");
		view_pedido();
	}
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
   	function new_pedido(){
		modal("PEDIDO: Nuevo","lg","1");
		btn_modal('save_pedido',"",'',"",'1','lg');
		get('pedidos_ventas/new_pedido',{},'content_1',true);
	}
	function refresh_nro_orden(){
   		get('pedidos_ventas/refresh_nro_orden',{},'p_nro',false);
   	}
	function add_row_producto(ele,idcl){
 		if(idcl=="" || idcl==null){ var idcl=$("#p_cli").val(); }
 		if(entero(idcl,0,10)){
 			var rows=$("tbody#content_protuct tr").length;
 			if(rows>0){
 				var rows=0;
 				$("tbody#content_protuct tr").each(function(idx,el){
 					if(rows<($(el).attr("stack")*1)){ rows=$(el).attr("stack")*1;}
				});
 			}
 			$("#cli").attr({'disabled':'disabled'});
 			var atrib=new FormData();
	 		atrib.append('idcl',idcl);atrib.append('rows',rows);
 			get_append_disabled('pedidos_ventas/add_row_producto',atrib,'content_protuct',false,ele);
 			$("#p_cli").attr('disabled', 'disabled');
 		}else{
 			alerta('Seleccione un cliente','top','p_cli');
 		}
	}
	(function($){
 		$.fn.extend({
 			add_categorias: function(){//buscar categoria segun select
 				var idcl=$(this).attr("cli");
 				var rows=$(this).attr("posicion");
 				//console.log($(this).val()+" "+idcl+" "+rows+" "+$("#"+$(this).attr("id")+" option:selected").text());
 				if(entero(idcl,0,10) && entero(rows,0,10) && $(this).val()!=""){
 					var atrib=new FormData();atrib.append('idcl',idcl);atrib.append('rows',rows);atrib.append('codigo',$("#"+$(this).attr("id")+" option:selected").text());
 					get('pedidos_ventas/add_categorias',atrib,"categorias"+rows,true);
 				}else{
 					$("#categorias"+rows).html("");
 				}
 				return false;
 			}
 		});
 		$.fn.extend({
 			change_categorias: function($row){//modificar categoria
 				var idcl=$(this).attr("cli");
 				var rows=$(this).attr("posicion");
 				//console.log($(this).val()+" "+idcl+" "+rows+" "+$("#"+$(this).attr("id")+" option:selected").text());
 				if(entero(idcl,0,10) && entero(rows,0,10) && $(this).val()!=""){
 					var atrib=new FormData();atrib.append('idcl',idcl);atrib.append('rows',rows);atrib.append('codigo',$("#"+$(this).attr("id")+" option:selected").text());
 					get('pedidos_ventas/add_categorias',atrib,"categorias"+rows,false);
 				}else{
 					$("#categorias"+rows).html("");
 				}
 				return false;
 			}
 		});
	})(jQuery)
	function calcular_total_fila(idpim){//Calculando costo total de la fila
 		var cu=$("#cu"+idpim).val();
 		var cant=$("#cant"+idpim).val();
 		if(decimal(cu,6,1) && cu>=0){
 			if(entero(cant,0,7) && cant>0){
 				var tot=number_format(((cu*1)*(cant*1)),2,'.',',');
 				$("#tot"+idpim).val(tot);
	 		}else{
	 			$("#tot"+idpim).val(0);
	 		}
 		}else{
 			alerta('Valor no válido','top','cu'+idpim);$("#tot"+idpim).val(0);
 		}
 		return false;
 	}
	function drop_fila_2n(ele){
		var e=$(ele).parent();
		var e2=e.parent();
		var e2=e2.parent();
		//var tbody=e2.parent();
		var her=e2.siblings();
		//console.log(her.length);
		if(her.length==0){//ya no tiene hermanos
			var table=e2.parent().parent();
				var tr=table.parent().parent();
			tr.remove();
			var td=tr.parent().parent().parent();
			//alert(td.next("selected").html());
		}else{
			e2.remove();	
		}

	}
	function drop_fila(ele){
		var e=$(ele).parent();
		var e2=e.parent();
		var e2=e2.parent();
		e2.remove();
		var rows=$("tbody#content_protuct tr").length;
		if(rows==0){ $("#p_cli").removeAttr('disabled');}
	}
   	/*fuction modal_productos(){
   		var idcl=$("#p_cli").val();
 		if(entero(idcl,0,10)){
	   		var atrib=new FormData();atrib.append('idcl',idcl);
	   		modal("PRODUCTOS: Adicionar al pedido","sm","2");
			btn_modal('',"",'',"",'2','sm');
			get('pedidos_ventas/modal_productos',atrib,'content_2',true);
   		}else{
   			alerta('Seleccione un cliente','top','p_cli');
   		}
   	}*/
   	function save_pedido(){
   		var nro=$("#p_nro").html();
		var nom=$("#p_nom").val();
		var cli=$("#p_cli").val();
		var fec=$("#p_fec").val();
		var obs=$("#p_obs").val();
		if(entero(nro,0,10)){
			if(strSpace(nom,3,150)){
				if(entero(cli,0,10)){
					if(fecha(fec)){
						var control=true;
						if(obs!=""){ if(!textarea(obs,0,900)){ control=false; alerta("Ingrese un contenido válido","top","p_obs");}}
						if(control){
							control=true;
							var productos="";
							$("tbody#content_protuct tr input[name=cantidad]").each(function(idx,el){
								var cant=$(el).val();
								var idpim=$(el).attr("producto");
								if(entero(idpim,0,10)){
									if(entero(cant,0,7) && cant>0){
										if(productos==""){
											productos+=""+idpim+"|"+cant;
										}else{
											productos+="[|]"+idpim+"|"+cant;
										}
									}else{
										alerta('Ingrese una valor entero mayor a cero','top',$(el).attr("id"));
										control=false;
									}
								}else{
									msj("!Error de variables...¡, actualice la página.")
									control=false;
								}
							});
							if(control){
								if(productos!=""){
									var atrib=new FormData();
									atrib.append("productos",productos);												
									atrib.append("nro",nro);
									atrib.append("nom",nom);
									atrib.append("cli",cli);
									atrib.append("fec",fec);
									atrib.append("obs",obs);
									var atrib3=new FormData();atrib3.append('num',$("#sp_num").val());atrib3.append('cli',$("#sp_cli").val());atrib3.append('mon',$("#sp_mon").val());atrib3.append('fpe',$("#sp_fpe").val());atrib3.append('fen',$("#sp_fen").val());
									set('pedidos_ventas/save_pedido',atrib,'NULL',true,'pedidos_ventas/view_pedido',atrib3,'contenido',false,'1');
								}else{
									alerta("Ingrese al menos un producto en el pedido.","top","btn_add");
								}
							}
						}
					}else{
						alerta("Ingrese una fecha válida","top","p_fec");
					}
				}else{
					alerta("Seleccione un cliente","top","p_cli");
				}
			}else{
				alerta("Ingrese una nombre de pedido válido","top","p_nom");
			}
		}else{
			alerta("Número de pedido invalido, actualice el número porfavor.","top","p_act");
		}
   	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	function imprimir_pedidos(json){
   		modal("PEDIDOS: Configuracion de impresion","xlg","1");
 		btn_modal('',"",'imprimir',"'area'",'1','xlg');
 		var atrib=new FormData();atrib.append('json',json);
 		get_2n('pedidos_ventas/config_imprimir_pedidos',atrib,'content_1',true,'pedidos_ventas/imprimir_pedidos',atrib,'area',true);
   	}
   	function arma_informe_pedido(json){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var v6=""; if($("#6:checked").val()){v6='ok'}
		var v7=""; if($("#7:checked").val()){v7='ok'}
		var v8=""; if($("#8:checked").val()){v8='ok'}
		var v9=""; if($("#9:checked").val()){v9='ok'}
		var v10=""; if($("#10:checked").val()){v10='ok'}
		var v11=""; if($("#11:checked").val()){v11='ok'}
		var v12=""; if($("#12:checked").val()){v12='ok'}
		var v13=""; if($("#13:checked").val()){v13='ok'}
		var nro=$("#nro").val();
		var atrib=new FormData();
		atrib.append('json',json);
		atrib.append('v1',v1);
		atrib.append('v2',v2);
		atrib.append('v3',v3);
		atrib.append('v4',v4);
		atrib.append('v5',v5);
		atrib.append('v6',v6);
		atrib.append('v7',v7);
		atrib.append('v8',v8);
		atrib.append('v9',v9);
		atrib.append('v10',v10);
		atrib.append('v11',v11);
		atrib.append('v12',v12);
		atrib.append('v13',v13);
		atrib.append('nro',nro);
		get('pedidos_ventas/imprimir_pedidos',atrib,'area',true);
	}
   	/*--- End Imprimir ---*/
   	/*--- Pagos ---
   	function pagos(idpe){
   		modal("PEDIDO: Pagos del pedido","md","1");
	 	btn_modal('',"",'',"",'1','md');
		var atrib=new FormData();
		atrib.append('idpe',idpe);
	 	get('pedidos_ventas/pagos',atrib,'content_1',true);
   	}
   	function save_pago(idpe){
   		var fec=$("#p_fec").val();
   		var pag=$("#p_pag").val();
   		var obs=$("#p_obs").val();
   		if(fecha(fec)){
	   		if(decimal(pag,7,1) && pag>0 && pag<=9999999.9){
		   		if(textarea(obs,0,500)){
		   			var atrib=new FormData();atrib.append('idpe',idpe);atrib.append('fec',fec);atrib.append('pag',pag);atrib.append('obs',obs);
		   			var atrib3=new FormData();atrib3.append('num',$("#sp_num").val());atrib3.append('cli',$("#sp_cli").val());atrib3.append('mon',$("#sp_mon").val());atrib3.append('fpe',$("#sp_fpe").val());atrib3.append('fen',$("#sp_fen").val());
		   			set_2n('pedidos_ventas/save_pago',atrib,'NULL',true,'pedidos_ventas/pagos',atrib,'content_1',false,'pedidos_ventas/view_pedido',atrib3,'contenido',false,"NULL");
		   		}else{
		   			alerta("Ingrese un contenido válido","top","p_obs");
		   		}
	   		}else{
	   			alerta("Ingrese unvalor válido","top","p_pag");
	   		}
   		}else{
   			alerta("Ingrese una fecha válida","top","p_fec");
   		}
   		return false;
   	}
   	function update_pago(idpa,idpe){
   		var fec=$("#p_fec"+idpa).val();
   		var pag=$("#p_pag"+idpa).val();
   		var obs=$("#p_obs"+idpa).val();
   		if(fecha(fec)){
	   		if(decimal(pag,7,1) && pag>0 && pag<=9999999.9){
		   		if(textarea(obs,0,500)){
		   			var atrib=new FormData();atrib.append('idpe',idpe);atrib.append('idpa',idpa);atrib.append('fec',fec);atrib.append('pag',pag);atrib.append('obs',obs);
		   			var atrib3=new FormData();atrib3.append('num',$("#sp_num").val());atrib3.append('cli',$("#sp_cli").val());atrib3.append('mon',$("#sp_mon").val());atrib3.append('fpe',$("#sp_fpe").val());atrib3.append('fen',$("#sp_fen").val());
		   			set_2n('pedidos_ventas/update_pago',atrib,'NULL',true,'pedidos_ventas/pagos',atrib,'content_1',false,'pedidos_ventas/view_pedido',atrib3,'contenido',false,"NULL");
		   		}else{
		   			alerta("Ingrese un contenido válido","top","p_obs"+idpa);
		   		}
	   		}else{
	   			alerta("Ingrese unvalor válido","top","p_pag"+idpa);
	   		}
   		}else{
   			alerta("Ingrese una fecha válida","top","p_fec"+idpa);
   		}
   		return false;
   	}
   	function alerta_pago(idpa,idpe) {
		modal("PEDIDO: Eliminar Pago","xs","5");
		addClick_v2('modal_ok_5','drop_pago',"'"+idpa+"','"+idpe+"'");
		var atrib=new FormData();atrib.append('idpa',idpa);atrib.append('idpe',idpe);
		atrib.append('titulo','eliminar definitivamente el pago del pedido');
		get('insumo/alerta',atrib,'content_5',true);
	}
	function drop_pago(idpa,idpe){
		var atrib=new FormData();atrib.append('idpa',idpa);atrib.append('idpe',idpe);
		var atrib3=new FormData();atrib3.append('num',$("#sp_num").val());atrib3.append('cli',$("#sp_cli").val());atrib3.append('mon',$("#sp_mon").val());atrib3.append('fpe',$("#sp_fpe").val());atrib3.append('fen',$("#sp_fen").val());
		set_2n('pedidos_ventas/drop_pago',atrib,'NULL',true,'pedidos_ventas/pagos',atrib,'content_1',false,'pedidos_ventas/view_pedido',atrib3,'contenido',false,"5");
	}
   	--- End Pagos ---*/
   	/*--- Reportes ---*/
	function reportes_pedido(idpe){
	 	modal("PEDIDO: Detalle de pedido","md","1");
	 	btn_modal('',"",'imprimir',"'area'",'1','md');
		var atrib=new FormData();
		atrib.append('idpe',idpe);
	 	get('pedidos_ventas/reporte_pedido',atrib,'content_1',true);
	}
	function productos_pedido(idpe){
	 	modal("PEDIDO: productos en el pedido","md","1");
	 	btn_modal('',"",'imprimir',"'area'",'1','md');
		var atrib=new FormData();atrib.append('idpe',idpe);
	 	get('pedidos_ventas/productos_pedido',atrib,'content_1',true);
	}
	function productos_pedido_imagen(idpe){
	 	modal("PEDIDO: productos en el pedido","lg","1");
	 	btn_modal('',"",'imprimir',"'area'",'1','lg');
		var atrib=new FormData();atrib.append('idpe',idpe);
		var v1=""; if(!$("#1:checked").val()){atrib.append('v1','ok');}
		atrib.append('nro',$("#nro").val());
	 	get('pedidos_ventas/productos_pedido',atrib,'content_1',true);
	}
	function materiales_pedido(idpe){
	 	modal("PEDIDO: Materiales requeridos en el pedido","lg","1");
	 	btn_modal('',"",'imprimir',"'area'",'1','lg');
		var atrib=new FormData();atrib.append('idpe',idpe);
	 	get('pedidos_ventas/materiales_pedido',atrib,'content_1',true);
	}
	function materiales_pedido_imagen(idpe){
	 	modal("PEDIDO: Materiales requeridos en el pedido","lg","1");
	 	btn_modal('',"",'imprimir',"'area'",'1','lg');
		var atrib=new FormData();atrib.append('idpe',idpe);
		var v1=""; if(!$("#1:checked").val()){atrib.append('v1','ok');}
		atrib.append('nro',$("#nro").val());
	 	get('pedidos_ventas/materiales_pedido',atrib,'content_1',true);
	}
	function materiales_productos_pedido(idpe){
		var atrib=new FormData();
		atrib.append('idpe',idpe);
		get('pedidos_ventas/materiales_productos_pedido',atrib,'content_1',true);
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	function config_pedido(idpe){
	 	modal("PEDIDO: Modificar pedido","lg","1");
	 	btn_modal('update_pedido',"'"+idpe+"'",'',"",'1','lg');
		var atrib=new FormData();atrib.append('idpe',idpe);
		get('pedidos_ventas/config_pedido',atrib,'content_1',true);
	}
	/*function update_pedido(idpe){
		//var nro=$("#nro_ord").html();
		var cli=$("#cli").val();
		var fped=$("#fped").val();
		var obs=$("#obs").val();
		var est=($("#c_est").val()*1)+"";
		var cos=($("#c_cos").val()*1)+"";
		var des=($("#c_des").val()*1)+"";
		var tie=$("#segundos").val();
		var fini=$("#fini").val();
		var fest=$("#fest").val();
		var fent=$("#fent").val();
		if(entero(idpe,0,10)){
			if(entero(cli,0,10)){
				if(fecha(fped)){
					var rows=$("tbody#content_protuct tr").length;
					if(rows>0){
						if(valida_tabla_producto()){
							if(entero(est,0,10)){
								if(entero(cos,0,10)){
									if(fecha(fini)){
										if(fecha(fest)){
											if(fecha(fent)){
												var atrib=new FormData();
												var sw=true;
												var prod="";
												$("tbody#content_protuct tr td input[type=hidden]").each(function(idx,el){
													var i=$(el).val();
													if(sw){
														sw=false;
														prod=$("#prod"+i).val()+"|"+$("#can"+i).val();
													}else{
														prod+="[|]"+$("#prod"+i).val()+"|"+$("#can"+i).val();
													}
												});
												atrib.append("productos",prod);												
												//atrib.append("nro",nro);
												atrib.append("idpe",idpe);
												atrib.append("cli",cli);
												atrib.append("fped",fped);
												atrib.append("obs",obs);
												atrib.append("est",est);
												atrib.append("cos",cos);
												atrib.append("des",des);
												atrib.append("tie",tie);
												atrib.append("fini",fini);
												atrib.append("fest",fest);
												atrib.append("fent",fent);
												atrib3=new FormData();
												atrib3.append('num',$("#sp_num").val());atrib3.append('cli',$("#sp_cli").val());atrib3.append('mon',$("#sp_mon").val());atrib3.append('fpe',$("#sp_fpe").val());atrib3.append('fen',$("#sp_fen").val());
												set('pedidos_ventas/update_pedido',atrib,'NULL',true,'pedidos_ventas/view_pedido',atrib3,'contenido',false,'1');
											}else{
												alerta("Valor inválido","top","fent");
											}
										}else{
											alerta("Valor inválido","top","fest");
										}
									}else{
										alerta("Valor inválido","top","fini");
									}
								}else{
									alerta("Valor inválido","top","c_cos");
								}
							}else{
								alerta("Valor inválido","top","c_est");
							}
						}else{
							msj("verifique que todos los productos tengan datos validos...")
						}
					}else{
						alerta("Debe ingresar al menos un produto...","top","table-np");
					}
				}else{
					alerta("Ingrese una fecha válida","top","fped");
				}
			}else{
				alerta("Seleccione un cliente","top","cli");
			}
		}else{
			alerta("Número de pedido invalido, actualice el número porfavor.","top","nro_ord");
		}
	}*/
   	function config_sub_pedido(idpe,idsp){
	 	modal("PEDIDO: Sub pedido","lg","1");
	 	btn_modal('update_sub_pedido',"'"+idsp+"'",'',"",'1','lg');
		var atrib=new FormData();atrib.append('idpe',idpe); atrib.append('idsp',idsp);
		get('pedidos_ventas/config_sub_pedido',atrib,'content_1',true);
	}
   	function update_sub_pedido(idsp){
		var fec=$("#p_fec").val();
		var obs=$("#p_obs").val();
					if(fecha(fec)){
						var control=true;
						if(obs!=""){ if(!textarea(obs,0,500)){ control=false; alerta("Ingrese un contenido válido","top","p_obs");}}
						if(control){
							control=true;
							var productos="";
							$("tbody#content_protuct tr input[name=cantidad]").each(function(idx,el){
								var cant=$(el).val();
								var idpim=$(el).attr("producto");
								if(entero(idpim,0,10)){
									if(entero(cant,0,7) && cant>0){
										if(productos==""){
											productos+=""+idpim+"|"+cant;
										}else{
											productos+="[|]"+idpim+"|"+cant;
										}
									}else{
										alerta('Ingrese una valor entero mayor a cero','top',$(el).attr("id"));
										control=false;
									}
								}else{
									msj("!Error de variables...¡, actualice la página.")
									control=false;
								}
							});
							if(control){
								if(productos!=""){
									var atrib=new FormData();
									atrib.append("productos",productos);												
									atrib.append("fec",fec);
									atrib.append("obs",obs);
									atrib.append("idsp",idsp);
									var atrib3=new FormData();atrib3.append('num',$("#sp_num").val());atrib3.append('cli',$("#sp_cli").val());atrib3.append('mon',$("#sp_mon").val());atrib3.append('fpe',$("#sp_fpe").val());atrib3.append('fen',$("#sp_fen").val());
									set('pedidos_ventas/update_sub_pedido',atrib,'NULL',true,'pedidos_ventas/view_pedido',atrib3,'contenido',false);
								}else{
									alerta("Ingrese al menos un producto en el pedido.","top","btn_add");
								}
							}
						}
					}else{
						alerta("Ingrese una fecha válida","top","p_fec");
					}
   	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	function confirmar_pedido(idpe){
		modal("PEDIDO: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','drop_pedido',"'"+idpe+"'");
   		var atrib=new FormData();atrib.append('idpe',idpe);
   		get('pedidos_ventas/confirmar_pedido',atrib,'content_5',true);
	}
	function drop_pedido(idpe){
		var atrib=new FormData();
		atrib.append('u',$("#e_user").val());
		atrib.append('p',$("#e_password").val());
		atrib.append('idpe',idpe);
		var atrib3=new FormData();atrib3.append('num',$("#sp_num").val());atrib3.append('cli',$("#sp_cli").val());atrib3.append('mon',$("#sp_mon").val());atrib3.append('fpe',$("#sp_fpe").val());atrib3.append('fen',$("#sp_fen").val());
		set('pedidos_ventas/drop_pedido',atrib,'NULL',true,'pedidos_ventas/view_pedido',atrib3,'contenido',false,'5');
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE ALMACENES -------*/
/*------- MANEJO DE VENTAS -------*/
   	/*--- Buscador ---*/
   	function view_venta(){
		var ele=new FormData();
		ele.append('nit',$("#sv_nit").val());
		ele.append('raz',$("#sv_raz").val());
		ele.append('fe1',$("#sv_fe1").val());
		ele.append('fe2',$("#sv_fe2").val());
		get('pedidos_ventas/view_venta',ele,'contenido',true);
		return false;
	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
	function all_venta(){
		reset_input("");
		view_venta();
	}
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
   	function new_venta(){
   		modal("VENTA: Nuevo","lg","1");
		btn_modal('',"",'',"",'1','lg');
		get('pedidos_ventas/new_venta',{},'content_1',true);
	}
	function search_venta(idpe){
	   	modal("VENTA: Nuevo","lg","1");
	   	var atrib=new FormData();
	   	if(entero(idpe,0,10)){
	   		btn_modal('save_venta',"'"+idpe+"'",'',"",'1','lg');
	   		atrib.append('idpe',idpe);
	   	}else{
	   		btn_modal('',"",'',"",'1','lg');
	   	}
		get('pedidos_ventas/new_venta',atrib,'content_1',false);
	}
	function costo_venta_producto(pos){
		var cv=parseFloat($("#cv_"+pos).val());
		var monto=parseFloat($("#cu_"+pos).val());
		$("#cf_"+pos).val(cv*monto);
		//actualizando totales
		var cv=$("input[name='cv']");
		var cf=$("input[name='cf']");
		var s_cv=0;
		var s_cf=0;
		for(var i=0;i<cv.length;i++){
			s_cv+=parseFloat(cv[i].value);
			s_cf+=parseFloat(cf[i].value);
		}
		$("#t_cv").text(number_format(s_cv,2,'.',',')+"");
		$("#t_cf").text(number_format(s_cf,2,'.',',')+"");
		$("#cos_vs").val(number_format(s_cf,2,'.',','));
		$("#cos_v").val(number_format(s_cf,2,'.',','));
		calcula_costo_venta();
	}
	function calcula_costo_venta(){
		var cos=$("#cos_v").val()*1;
		var des=$("#des_v").val()*1;
		var pag=$("#pag_v").val()*1;
		$("#tot_v").val(number_format((cos-des),2,'.',','));
		$("#sal_v").val(number_format((cos-des-pag),2,'.',','));
		return false;
	}
	function save_venta(idpe){
		var cos_vs=$("#cos_vs").val();
		var cos_v=$("#cos_v").val();
		var des_v=$("#des_v").val();
		var fech=$("#fecha").val();
		var obs=$("#obs").val();
		if(fecha(fech)){
			if(decimal(cos_vs,7,1) && cos_vs>=0){
				if(decimal(cos_v,7,1) && cos_v>=0 && cos_v<=9999999.9){
					if(decimal(des_v,7,1) && des_v>=0 && des_v<=9999999.9){
						if(textarea(obs,0,800)){
							var rows=$("tbody#content_protuct tr").length;
							if(rows>0){
								var atrib=new FormData();
									var sw=true;
									var prod="";
									$("tbody#content_protuct tr td input[type=hidden]").each(function(idx,el){
										var i=$(el).val();
										console.log("i: "+i);
										if(sw){
											sw=false;
											prod=$("#iddp"+i).val()+"|"+$("#cv_"+i).val()+"|"+$("#obs_"+i).val();
										}else{
											prod+="[|]"+$("#iddp"+i).val()+"|"+$("#cv_"+i).val()+"|"+$("#obs_"+i).val();
										}
									});
									atrib.append("productos",prod);
									atrib.append('idpe',idpe);
									atrib.append('fecha',fecha);
									atrib.append('cos_vs',cos_vs);
									atrib.append('cos_v',cos_v);
									atrib.append('des_v',des_v);
									atrib.append('fecha',fech);
									atrib.append('obs',obs);
									var atrib3=new FormData();atrib3.append('nit',$("#sv_nit").val());atrib3.append('raz',$("#sv_raz").val());atrib3.append('fe1',$("#sv_fe1").val());atrib3.append('fe2',$("#sv_fe2").val());
									set('pedidos_ventas/save_venta',atrib,'NULL',true,'pedidos_ventas/view_venta',atrib3,'contenido',false,'1');
							}else{
								alerta('La venta debe tener al menos una producto','top','content_protuct');
							}
						}else{
							alerta('Ongrese una observacion válida','top','obs');
						}
					}else{
						alerta('Ingrese un valor numérico válido mayor a cero y menor a 9999999.9, con una sola decimal','top','des_v');
					}
				}else{
					alerta('Ingrese un valor numérico válido mayor a cero y menor a 9999999.9, con una sola decimal','top','cos_v');
				}
			}else{
				alerta('Ingrese un valor numérico válido mayor a cero','top','cos_vs');
			}
		}else{
			alerta('Seleccione un fecha de venta válida','top','fecha');
		}
		return false;
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	function imprimir_ventas(json){
   		modal("VENTAS: Configuracion de impresion","xlg","1");
 		btn_modal('',"",'imprimir',"'area'",'1','xlg');
 		var atrib=new FormData();atrib.append('json',json);
 		get_2n('pedidos_ventas/config_imprimir_ventas',atrib,'content_1',true,'pedidos_ventas/imprimir_ventas',atrib,'area',true);
   	}
   	function arma_ventas(json){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var v6=""; if($("#6:checked").val()){v6='ok'}
		var v7=""; if($("#7:checked").val()){v7='ok'}
		var v8=""; if($("#8:checked").val()){v8='ok'}
		var v9=""; if($("#9:checked").val()){v9='ok'}
		var v10=""; if($("#10:checked").val()){v10='ok'}
		var v11=""; if($("#11:checked").val()){v11='ok'}
		var v12=""; if($("#12:checked").val()){v12='ok'}
		var v13=""; if($("#13:checked").val()){v13='ok'}
		var nro=$("#nro").val();
		var atrib=new FormData();
		atrib.append('json',json);
		atrib.append('v1',v1);
		atrib.append('v2',v2);
		atrib.append('v3',v3);
		atrib.append('v4',v4);
		atrib.append('v5',v5);
		atrib.append('v6',v6);
		atrib.append('v7',v7);
		atrib.append('v8',v8);
		atrib.append('v9',v9);
		atrib.append('v10',v10);
		atrib.append('v11',v11);
		atrib.append('v12',v12);
		atrib.append('v13',v13);
		atrib.append('nro',nro);
		get('pedidos_ventas/imprimir_ventas',atrib,'area',true);
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	function reportes_venta(idv){
	 	modal("VENTA: Detalle de venta","lg","1");
	 	btn_modal('',"",'imprimir',"'area'",'1','lg');
		var atrib=new FormData();
		atrib.append('idv',idv);
	 	get('pedidos_ventas/detalle_venta',atrib,'content_1',true);
	}
	function productos_venta(idv){
	 	modal("VENTA: Productos en la venta","lg","1");
	 	btn_modal('',"",'imprimir',"'area'",'1','lg');
		var atrib=new FormData();atrib.append('idv',idv);
	 	get('pedidos_ventas/productos_venta',atrib,'content_1',true);
	}
	function productos_venta_imagen(idv){
	 	modal("VENTA: Productos en la venta","lg","1");
	 	btn_modal('',"",'imprimir',"'area'",'1','lg');
		var atrib=new FormData();atrib.append('idv',idv);
		var v1=""; if(!$("#1:checked").val()){atrib.append('v1','ok');}
		atrib.append('nro',$("#nro").val());
	 	get('pedidos_ventas/productos_venta',atrib,'content_1',true);
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	function config_venta(idv){
   		modal("VENTA: Configuración","lg","1");
		btn_modal('update_venta',"'"+idv+"'",'',"",'1','lg');
		var atrib=new FormData();atrib.append('idv',idv);
		get('pedidos_ventas/config_venta',atrib,'content_1',true);
	}
	function update_venta(idv){
		var cos_vs=$("#cos_vs").val();
		var cos_v=$("#cos_v").val();
		var des_v=$("#des_v").val();
		var fech=$("#fecha").val();
		var obs=$("#obs").val();
		if(fecha(fech)){
			if(decimal(cos_vs,7,1) && cos_vs>=0){
				if(decimal(cos_v,7,1) && cos_v>=0 && cos_v<=9999999.9){
					if(decimal(des_v,7,1) && des_v>=0 && des_v<=9999999.9){
						if(textarea(obs,0,800)){
							var rows=$("tbody#content_protuct tr").length;
							if(rows>0){
								var atrib=new FormData();
									var sw=true;
									var prod="";
									$("tbody#content_protuct tr td input[type=hidden]").each(function(idx,el){
										var i=$(el).val();
										console.log("i: "+i);
										if(sw){
											sw=false;
											prod=$("#iddp"+i).val()+"|"+$("#cv_"+i).val()+"|"+$("#obs_"+i).val();
										}else{
											prod+="[|]"+$("#iddp"+i).val()+"|"+$("#cv_"+i).val()+"|"+$("#obs_"+i).val();
										}
									});
									atrib.append("productos",prod);
									atrib.append('idv',idv);
									atrib.append('fecha',fecha);
									atrib.append('cos_vs',cos_vs);
									atrib.append('cos_v',cos_v);
									atrib.append('des_v',des_v);
									atrib.append('fecha',fech);
									atrib.append('obs',obs);
									var atrib3=new FormData();atrib3.append('nit',$("#sv_nit").val());atrib3.append('raz',$("#sv_raz").val());atrib3.append('fe1',$("#sv_fe1").val());atrib3.append('fe2',$("#sv_fe2").val());
									set('pedidos_ventas/update_venta',atrib,'NULL',true,'pedidos_ventas/view_venta',atrib3,'contenido',false,'1');
							}else{
								alerta('La venta debe tener al menos una producto','top','content_protuct');
							}
						}else{
							alerta('Ongrese una observacion válida','top','obs');
						}
					}else{
						alerta('Ingrese un valor numérico válido mayor a cero y menor a 9999999.9, con una sola decimal','top','des_v');
					}
				}else{
					alerta('Ingrese un valor numérico válido mayor a cero y menor a 9999999.9, con una sola decimal','top','cos_v');
				}
			}else{
				alerta('Ingrese un valor numérico válido mayor a cero','top','cos_vs');
			}
		}else{
			alerta('Seleccione un fecha de venta válida','top','fecha');
		}
		return false;
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	function confirmar_venta(idv){
		modal("PEDIDO: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','drop_venta',"'"+idv+"'");
   		var atrib=new FormData();atrib.append('idv',idv);
   		get('pedidos_ventas/confirmar_venta',atrib,'content_5',true);
	}
	function drop_venta(idv){
		var atrib=new FormData();
		atrib.append('u',$("#e_user").val());
		atrib.append('p',$("#e_password").val());
		atrib.append('idv',idv);
		var atrib3=new FormData();atrib3.append('nit',$("#sv_nit").val());atrib3.append('raz',$("#sv_raz").val());atrib3.append('fe1',$("#sv_fe1").val());atrib3.append('fe2',$("#sv_fe2").val());
		set('pedidos_ventas/drop_venta',atrib,'NULL',true,'pedidos_ventas/view_venta',atrib3,'contenido',false,'5');
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE VENTAS -------*/
/*------- MANEJO DE COMPRAS -------*/
   	/*--- Buscador ---*/
   	function view_compra(){
		var ele=new FormData();
		ele.append('nit',$("#sc_nit").val());
		ele.append('raz',$("#sc_pro").val());
		ele.append('fe1',$("#sc_fe1").val());
		ele.append('fe2',$("#sc_fe2").val());
		ele.append('am',$("#sc_am").val());
		get('pedidos_ventas/view_compra',ele,'contenido',true);
		return false;
	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
	function all_compra(){
		reset_input("");
		view_compra();
	}
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
   	//CON ORDEN
	   	function con_orden(){
	   		modal("COMPRA: Movimiento con número de orden","xlg","1");
			btn_modal('save_con_orden',"",'',"",'1','xlg');
			get('pedidos_ventas/con_orden',{},'content_1',true);
		}
		function refresh_orden_gasto(){
	   		get('pedidos_ventas/refresh_orden_gasto',{},'nro_ord',false);
	   	}
	   	function orden(){//con o sin orden de gasto
			var valor=$("#tg").val();
			var text="";
			  	switch(valor){
		   		case '1': text="gasto"; break;
		   		case '2': text="compra"; break;
		   		case '3': text="pago"; break;
		   	}
		   	$(".g_text").html(mayuscula(text));
		   	$("#g_text2").html(text);
		}
	   	function add_row_con_orden(btn){
			var rows=$("tbody#content_protuct tr").length;
			var cat=$("#cat").val();
			if(rows>0){
				var max=0;
				$("tbody#content_protuct tr td input[type=hidden]").each(function(idx,el){
					if($(el).val()>max){ max=$(el).val(); }
				});
				rows=max;
			}
			if(cat!=""){
				var atrib=new FormData();
			 	atrib.append('row',rows);atrib.append('cat',cat);
		 		get_append_disabled('pedidos_ventas/add_row_con_orden',atrib,'content_protuct',false,btn);
			}else{
				alerta('Seleccione una categoria del producto','top','cat');
			}
		}

		function selected_proveedor(row,cat){
			var nit=$("#nit"+row).val();
			var option="";
			$("select#prov"+row+" option").each(function(idx,el){
				if($(el).val()==nit){
					option+="<option value='"+$(el).val()+"' selected>"+$(el).text()+"</option>";
				}else{
					option+="<option value='"+$(el).val()+"'>"+$(el).text()+"</option>";
				}
			});
			$("select#prov"+row).html(option);
			select_materiales(nit,cat,row);
		}
		function selected_nit(row,cat){
			var prov=$("#prov"+row).val();
			var option="";
			$("select#nit"+row+" option").each(function(idx,el){
				if($(el).val()==prov){
					option+="<option value='"+$(el).val()+"' selected>"+$(el).text()+"</option>";
				}else{
					option+="<option value='"+$(el).val()+"'>"+$(el).text()+"</option>";
				}
			});
			$("select#nit"+row).html(option);
			select_materiales(prov,cat,row);
		}
		function select_materiales(idpro,cat,row){
			var atrib=new FormData(); atrib.append('idpro',idpro);atrib.append('cat',cat);
			reset_prod(row);
			get('pedidos_ventas/select_materiales',atrib,'mat'+row,false);
			
		}
		function monto_material(row,cat){
			var idmp=$("#mat"+row).val();
			var can=$("#can"+row).val();
			if(entero(idmp,0,10)){
				var inputs="cu"+row+"|i_sis"+row+"|imp"+row+"|med"+row;
				var atrib=new FormData(); atrib.append('idmp',idmp); atrib.append('can',can); atrib.append('cat',cat);
				get_input_v2('pedidos_ventas/monto_material',atrib,"NULL",inputs,false);
			}else{
				reset_prod(row);
			}
		}
		function calcula_monto_material(row){
			var cu=$("#cu"+row).val();
			var can=$("#can"+row).val()*1;
			if(can>=0){
				var res=number_format((cu*can),2,'.',',');
				$("#i_sis"+row).val(res);
				$("#imp"+row).val(res);
			}
			refresh_totales_compras();//solo para el caso de actualizar compras (nuevo y modificar);
		}
		function reset_prod(row){
			$("#prod"+row).html("<option value=''>Seleccionar...</option>");
			$("#cu"+row).val("");
			$("#i_sis"+row).val("");
			$("#imp"+row).val("");
			refresh_totales_compras();
		}
		function refresh_totales_compras(){
			var tot_sis=0;
			var tot=0;
			$("tbody#content_protuct input[id*=sis]").each(function(idx,el){ tot_sis+=$(el).val()*1; });
			$("tbody#content_protuct input[id*=imp]").each(function(idx,el){ tot+=$(el).val()*1; });
			$("#tot_sis").html(number_format(tot_sis,2,'.',','));
			$("#tot").html(number_format(tot,2,'.',','));
		}
		function save_con_orden(){
			var nro=$("#nro_ord").html()*1;
			var tg=$("#tg").val();
			var fech=$("#fecha").val();
			var obs=$("#obs").val();
			if(nro>0){
				if(entero(tg,0,1) && tg>0 && tg<=3){
					if(fecha(fech)){
						if(textarea(obs,0,700)){
							var rows=$("tbody#content_protuct tr").length;
							if(rows>0){
								if(valida_tabla_material()){
									var sw=true;
									var mat="";
										$("tbody#content_protuct tr td input[id*=pos]").each(function(idx,el){
											var i=$(el).val();
											if(sw){
												sw=false;
												mat=$("#cat"+i).val()+"|"+$("#mat"+i).val()+"|"+$("#can"+i).val()*1+"|"+$("#imp"+i).val()*1;
											}else{
												mat+="[|]"+$("#cat"+i).val()+"|"+$("#mat"+i).val()+"|"+$("#can"+i).val()*1+"|"+$("#imp"+i).val()*1;
											}
										});
									var atrib=new FormData();
									atrib.append('mat',mat);
									atrib.append('nro',nro);
									atrib.append('tg',tg);
									atrib.append('fecha',fech);
									atrib.append('obs',obs);
									var atrib3=new FormData();atrib3.append('nit',$("#sc_nit").val());atrib3.append('raz',$("#sc_pro").val());atrib3.append('fe1',$("#sc_fe1").val());atrib3.append('fe2',$("#sc_fe2").val());atrib3.append('am',$("#sc_am").val());
									set('pedidos_ventas/save_con_orden',atrib,'NULL',true,'pedidos_ventas/view_compra',atrib3,'contenido',false,'1');
								}
							}else{
								alerta("Debe ingresar al menos un material en la venta","top","content_protuct");
							}
						}else{
							alerta("Ingrese una observacion con contenido válido","top","obs");
						}
					}else{
						alerta("Ingrese una fecha válida","top","fecha");
					}
				}else{
					alerta("Seleccione un tipo de movimiento válido","top","tg");
				}
			}else{
				alerta("Número de pedido no válido, pulse el boton actualizar","top","nro_ord");
			}
		}
		function valida_tabla_material(){
			var control=true;
			$("tbody#content_protuct tr td input[id*=pos]").each(function(idx,el){
				var i=$(el).val();
				if(entero($("#prov"+i).val(),0,10)){
					if(entero($("#mat"+i).val(),0,10)){
						if(($("#can"+i).val()*1)>=0){
							if(($("#imp"+i).val()*1)>=0){
								
							}else{
								alerta("Ingrese una cantidad válida mayor que cero...","top","imp"+i); 
								control=false;
							}
						}else{
							alerta("Ingrese una cantidad válida mayor que cero...","top","can"+i); 
							control=false;
						}
					}else{
						alerta("Seleccione un material...","top","mat"+i); 
						control=false;
					}
				}else{
					alerta("Seleccione un proveedor...","top","prov"+i); 
					control=false;
				}
			});
			return control;
		}
	//SIN ORDEN
		function sin_orden(){
	   		modal("COMPRA: Movimiento sin número de orden","xlg","1");
			btn_modal('save_sin_orden',"",'',"",'1','xlg');
			get('pedidos_ventas/sin_orden',{},'content_1',true);
		}
		function add_row_sin_orden(btn){
			var rows=$("tbody#content_protuct tr").length;
			var cat=$("#cat").val();
			if(rows>0){
				var max=0;
				$("tbody#content_protuct tr td input[type=hidden]").each(function(idx,el){
					if($(el).val()>max){ max=$(el).val(); }
				});
				rows=max;
			}
			if(cat!=""){
				var atrib=new FormData();
			 	atrib.append('row',rows);atrib.append('cat',cat);
		 		get_append_disabled('pedidos_ventas/add_row_sin_orden',atrib,'content_protuct',false,btn);
			}else{
				alerta('Seleccione una categoria del producto','top','cat');
			}
		}
		function save_sin_orden(){
			var rows=$("tbody#content_protuct tr").length;
			if(rows>0){
				if(valida_tabla_material_so()){
					var sw=true;
					var mat="";
						$("tbody#content_protuct tr td input[id*=pos]").each(function(idx,el){
							var i=$(el).val();
							if(sw){
								sw=false;
								mat=$("#cat"+i).val()+"|"+$("#fech"+i).val()+"|"+$("#mat"+i).val()+"|"+$("#fac"+i).val()+"|"+$("#can"+i).val()*1+"|"+$("#imp"+i).val()*1;
							}else{
								mat+="[|]"+$("#cat"+i).val()+"|"+$("#fech"+i).val()+"|"+$("#mat"+i).val()+"|"+$("#fac"+i).val()+"|"+$("#can"+i).val()*1+"|"+$("#imp"+i).val()*1;
							}
						});
					var atrib=new FormData();
					atrib.append('mat',mat);
					var atrib3=new FormData();atrib3.append('nit',$("#sc_nit").val());atrib3.append('raz',$("#sc_pro").val());atrib3.append('fe1',$("#sc_fe1").val());atrib3.append('fe2',$("#sc_fe2").val());atrib3.append('am',$("#sc_am").val());
					set('pedidos_ventas/save_sin_orden',atrib,'NULL',true,'pedidos_ventas/view_compra',atrib3,'contenido',false,'1');
				}
			}else{
				alerta("Debe ingresar al menos un material en la venta","top","content_protuct");
			}
		}
		function valida_tabla_material_so(){
			var control=true;
			$("tbody#content_protuct tr td input[id*=pos]").each(function(idx,el){
				var i=$(el).val();
				if(fecha($("#fech"+i).val()+"")){
					if(entero($("#prov"+i).val(),0,10)){
						if(entero($("#mat"+i).val(),0,10)){
							if(($("#can"+i).val()*1)>=0){
								if(($("#imp"+i).val()*1)>=0){
									
								}else{
									alerta("Ingrese una cantidad válida mayor que cero...","top","imp"+i); 
									control=false;
								}
							}else{
								alerta("Ingrese una cantidad válida mayor que cero...","top","can"+i); 
								control=false;
							}
						}else{
							alerta("Seleccione un material...","top","mat"+i); 
							control=false;
						}
					}else{
						alerta("Seleccione un proveedor...","top","prov"+i); 
						control=false;
					}
				}else{
					alerta("Seleccione una fecha válida...","top","fech"+i); 
					control=false;
				}

			});
			return control;
		}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	function imprimir_compras(json){
   		modal("COMPRASS: Configuracion de impresion","xlg","1");
 		btn_modal('',"",'imprimir',"'area'",'1','xlg');
 		var atrib=new FormData();atrib.append('json',json);
 		get_2n('pedidos_ventas/config_imprimir_compras',atrib,'content_1',true,'pedidos_ventas/imprimir_compras',atrib,'area',true);
   	}
   	function arma_compras(json){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var v6=""; if($("#6:checked").val()){v6='ok'}
		var v7=""; if($("#7:checked").val()){v7='ok'}
		var v8=""; if($("#8:checked").val()){v8='ok'}
		var v9=""; if($("#9:checked").val()){v9='ok'}
		var v10=""; if($("#10:checked").val()){v10='ok'}
		var nro=$("#nro").val();
		var atrib=new FormData();
		atrib.append('json',json);
		atrib.append('v1',v1);
		atrib.append('v2',v2);
		atrib.append('v3',v3);
		atrib.append('v4',v4);
		atrib.append('v5',v5);
		atrib.append('v6',v6);
		atrib.append('v7',v7);
		atrib.append('v8',v8);
		atrib.append('v9',v9);
		atrib.append('v10',v10);
		atrib.append('nro',nro);
		get('pedidos_ventas/imprimir_compras',atrib,'area',true);
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	function reportes_compra(idga){
	 	modal("COMPRA: Detalle de compra o gasto","lg","1");
	 	btn_modal('',"",'imprimir',"'area'",'1','lg');
		var atrib=new FormData();
		atrib.append('idga',idga);
	 	get('pedidos_ventas/detalle_compra',atrib,'content_1',true);
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	function config_con_orden(idga){
   		modal("COMPRA: Configurar orden","xlg","1");
		btn_modal('update_con_orden',"'"+idga+"'",'',"",'1','xlg');
		var atrib=new FormData();atrib.append('idga',idga);
		get('pedidos_ventas/config_con_orden',atrib,'content_1',true);
   	}
   	function update_con_orden(idga){
		var nro=$("#nro_ord").html()*1;
		var tg=$("#tg").val();
		var fech=$("#fecha").val();
		var obs=$("#obs").val();
		if(nro>0){
			if(entero(tg,0,1) && tg>0 && tg<=3){
				if(fecha(fech)){
					if(textarea(obs,0,700)){
						var rows=$("tbody#content_protuct tr").length;
						if(rows>0){
							if(valida_tabla_material()){
								var sw=true;
								var mat="";
									$("tbody#content_protuct tr td input[id*=pos]").each(function(idx,el){
										var i=$(el).val();
										if(sw){
											sw=false;
											mat=$("#cat"+i).val()+"|"+$("#mat"+i).val()+"|"+$("#can"+i).val()*1+"|"+$("#imp"+i).val()*1;
										}else{
											mat+="[|]"+$("#cat"+i).val()+"|"+$("#mat"+i).val()+"|"+$("#can"+i).val()*1+"|"+$("#imp"+i).val()*1;
										}
									});
								var atrib=new FormData();
								atrib.append('idga',idga);
								atrib.append('mat',mat);
								atrib.append('nro',nro);
								atrib.append('tg',tg);
								atrib.append('fecha',fech);
								atrib.append('obs',obs);
								var atrib3=new FormData();atrib3.append('nit',$("#sc_nit").val());atrib3.append('raz',$("#sc_pro").val());atrib3.append('fe1',$("#sc_fe1").val());atrib3.append('fe2',$("#sc_fe2").val());atrib3.append('am',$("#sc_am").val());
								set('pedidos_ventas/update_con_orden',atrib,'NULL',true,'pedidos_ventas/view_compra',atrib3,'contenido',false,'1');
							}
						}else{
							alerta("Debe ingresar al menos un material en la venta","top","content_protuct");
						}
					}else{
						alerta("Ingrese una observacion con contenido válido","top","obs");
					}
				}else{
					alerta("Ingrese una fecha válida","top","fecha");
				}
			}else{
				alerta("Seleccione un tipo de movimiento válido","top","tg");
			}
		}else{
			alerta("Número de pedido no válido, pulse el boton actualizar","top","nro_ord");
		}
	}
	function config_sin_orden(idga,iddga){
   		modal("COMPRA: Configurar orden","xlg","1");
		btn_modal('update_sin_orden',"'"+idga+"','"+iddga+"'",'',"",'1','xlg');
		var atrib=new FormData();atrib.append('idga',idga);
		get('pedidos_ventas/config_sin_orden',atrib,'content_1',true);
   	}
	function update_sin_orden(idga,iddga){
		var rows=$("tbody#content_protuct tr").length;
		if(rows>0){
			if(valida_tabla_material_so()){
				var sw=true;
				var mat="";
					$("tbody#content_protuct tr td input[id*=pos]").each(function(idx,el){
						var i=$(el).val();
						if(sw){
							sw=false;
							mat=$("#cat"+i).val()+"|"+$("#fech"+i).val()+"|"+$("#mat"+i).val()+"|"+$("#fac"+i).val()+"|"+$("#can"+i).val()*1+"|"+$("#imp"+i).val()*1;
						}else{
							mat+="[|]"+$("#cat"+i).val()+"|"+$("#fech"+i).val()+"|"+$("#mat"+i).val()+"|"+$("#fac"+i).val()+"|"+$("#can"+i).val()*1+"|"+$("#imp"+i).val()*1;
						}
					});
				var atrib=new FormData();
				atrib.append('idga',idga);
				atrib.append('iddga',iddga);
				atrib.append('mat',mat);
				var atrib3=new FormData();atrib3.append('nit',$("#sc_nit").val());atrib3.append('raz',$("#sc_pro").val());atrib3.append('fe1',$("#sc_fe1").val());atrib3.append('fe2',$("#sc_fe2").val());atrib3.append('am',$("#sc_am").val());
				set('pedidos_ventas/update_sin_orden',atrib,'NULL',true,'pedidos_ventas/view_compra',atrib3,'contenido',false,'1');
			}
		}else{
			alerta("Debe ingresar al menos un material en la venta","top","content_protuct");
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	function confirmar_compra(iddga){
		modal("COMPRA O GASTO: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','drop_compra',"'"+iddga+"'");
   		var atrib=new FormData();atrib.append('iddga',iddga);
   		get('pedidos_ventas/confirmar_compra',atrib,'content_5',true);
	}
	function drop_compra(iddga){
		var atrib=new FormData();
		atrib.append('u',$("#e_user").val());
		atrib.append('p',$("#e_password").val());
		atrib.append('iddga',iddga);
		var atrib3=new FormData();atrib3.append('nit',$("#sc_nit").val());atrib3.append('raz',$("#sc_pro").val());atrib3.append('fe1',$("#sc_fe1").val());atrib3.append('fe2',$("#sc_fe2").val());atrib3.append('am',$("#sc_am").val());
		set('pedidos_ventas/drop_compra',atrib,'NULL',true,'pedidos_ventas/view_compra',atrib3,'contenido',false,'5');
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE COMPRAS -------*/

















/*--------------- MANEJO DE PEDIDOS ---------------*/
	/*--- busar pedido ---*/


	/*--- end buscar pedido ---*/
	/*--- nuevo pedido ---*//*

	function costo_producto(pos){
		var prod=$("#prod"+pos).val();
		var cli=$("#cli").val();
		if(entero(prod,1,10)){
			if(entero(prod,1,10)){

			}
			var atrib=new FormData();
			atrib.append('prod',prod);atrib.append('cliente',cliente);atrib.append('pos',pos);
			ajax_get('pedidos_ventas/costo_producto',atrib,'con_can'+pos);
		}else{
			$("#con_can"+pos).html("<div class='form-control input-sm' disabled='disabled'></div>");
		}
		costo_total_producto(pos);
	}
	function costo_total_producto(pos){
		var cu=parseFloat($("#cu"+pos).val());
		var can=parseFloat($("#can"+pos).val());
		$("#ct"+pos).val(cu*can);
		calcular_costo_tiempo();
		calcular_costo();
	}
	function calcular_costo(){
		var rows=$("tbody#content_protuct tr").length;
		/*var ct=0;
		for(var i = 2; i <= rows; i++){
			ct+=(($("#ct"+i).val())*1);
		}*/
		/*
		var ct=$("#c_est").val();
		$("#c_tot").val((parseFloat(ct)-parseFloat($("#c_des").val()*1)-parseFloat($("#c_pag").val()*1)));
	}


	/*--- end nuevo pedido ---*/
	/*--- reportes de pedidos ---*//*

	function pedido(idpe){
		var ele=new FormData();
		ele.append('idpe',idpe);
		ajax_get('pedidos_ventas/view_reportes_pedido',ele,'content_1');
	}

	/*--- end reportes de pedidos ---*/
	/*--- configuracion de pedidos ---*/
		/*-----modificar-----*//*

		function confirm_update_pedido(ele,idp){
			if(window.confirm("¿Desea eliminar el producto del pedido?")){
				drop_row_pedido_producto(ele);
			}else{

			}
		}
		function modificar_pedido(idpe){
			/*var cli=$("#clie").val();
		 	var fe1=$("#fe1").val();
		 	var fe2=$("#fe2").val();
		 	var obs=$("#obs").val();
		 	var monto=$("#monto").val();
		 	var adelanto=$("#adelanto").val();
		 	var files= new FormData();
		 	files.append('idpe',idpe);
			files.append('clie',cli);
			files.append('fe1',fe1);
			files.append('fe2',fe2);
			files.append('obs',obs);
			files.append('monto',monto);
			files.append('adelanto',adelanto);
		 	ajax_file('pedidos_ventas/view_all_pedido',{},'contenido','pedidos_ventas/update_pedido',files);
		 	var cli=$("#cliente").val();
			var fe1=$("#f1").val();
			var fe2=$("#f2").val();
			var obs=$("#observaciones").val();
			var monto=$("#monto").val();
			var adelanto=$("#adelanto").val();
			//var nro=$("#numero_orden").html();
			if(cli!=""){
				if(fe1!=""){
					if(fe2!=""){
						if(monto!=""){
							var atrib= new FormData();
						 	files.append('idpe',idpe);
							files.append('clie',cli);
							files.append('fe1',fe1);
							files.append('fe2',fe2);
							files.append('obs',obs);
							files.append('monto',monto);
							files.append('adelanto',adelanto);
							var rows=$("tbody#content_protuct tr").length;
							if(rows>0){
								var prod=$("#prod1").val()+"|"+$("#can1").val();
								for(var i = 2; i <= rows; i++){
									prod+="[|]"+$("#prod"+i).val()+"|"+$("#can"+i).val();
								}
								atrib.append("productos",prod);
								//ajax_file_3n(,files,'','pedidos_ventas/view_modificar_pedido',{},'content_1','pedidos_ventas/view_all_pedido',{},'contenido');
								ajax_set('pedidos_ventas/update_pedido',atrib,'NULL','pedidos_ventas/view_all_pedido',{},'contenido','1');
							}else{
								ver_alerta("alerta_fail","Debe ingresar al menos un produto...");
							}
						}else{
							ver_alerta("alerta_fail","ingrese una monto de pedido...");
						}
					}else{
						ver_alerta("alerta_fail","Ingrese ina fecha estimada de entrega...");
					}
				}else{
					ver_alerta("alerta_fail","Ingrese una fecha de pedido...");
				}
			}else{
				ver_alerta("alerta_fail","Seleccione un cliente...");
			}
		}
		/*----- end modificar -----*/
		/*----- productos -----*/
		/*function productos_pedido(idpe){
			button_modal('modal_ok_1','','','modal_print_1','','','modal_closed_1');
			post_get('pedidos_ventas/productos_pedido',{id:idpe},'content_1');
		}*//*
		function save_cantidad_pedido(idpe,iddp){
			var can=$("#can"+iddp).val();
			if(can>0){
				var files= new FormData();
			 	files.append('iddp',iddp);
			 	files.append('can',can);
				ajax_file('pedidos_ventas/productos_pedido',{id:idpe},'content_1','pedidos_ventas/save_cantidad_pedido',files);
			}
		}
		function drop_producto_pedido(idpe,iddp){
			var files= new FormData();
			files.append('iddp',iddp);
			ajax_file('pedidos_ventas/productos_pedido',{id:idpe},'content_1','pedidos_ventas/drop_cantidad_pedido',files);
		}
		function form_adicionar_producto(idpe){
			visible('content_modal','modal','.2%','65%','90%');
			button_modal('modal_ok','','','modal_print','','','modal_closed');
			var ele=new FormData();ele.append('idpe',idpe);
			ajax_get_2n('pedidos_ventas/search_view_productos',ele,'modal','pedidos_ventas/view_productos',ele,'search_contenido');
		}
		function save_producto(ele,color,idpe,idpim){
			post_set_button('pedidos_ventas/save_producto',{idpe:idpe,idpim:idpim},ele,color,'pedidos_ventas/productos_pedido',{id:idpe},'content_1')
		}
		/*----- end producto -----*/
	/*--- end configuracion de pedidos ---*/

/*--------------- END MANEJO DE PEDIDOS ---------------*//*

//ventas
function nueva_venta(){
	ver_modal("VENTA: Nuevo",98,null,"1","b");
	button_modal('modal_ok_1','',"",'modal_print_1','','','modal_closed_1');
 	post_get('pedidos_ventas/nueva_venta',{},'content_1');
}
function view_detalle_pedido(){
	var cod=$("#cod_ped").val();
	if(cod!=""){
		button_modal('modal_ok_1','save_venta',"'"+cod+"'",'modal_print_1','','','modal_closed_1');
		post_get('pedidos_ventas/view_detalle_pedido_venta',{id:cod},'modal_contenido');
	}else{

	}
}

function descuento(pos){
	var ct=parseFloat($("#ct"+pos).val());
	var des=parseFloat($("#des"+pos).val());
	$("#cf"+pos).val((ct-des));

	var des=$("input[name='des']");
	var cf=$("input[name='cf']");
	var s_des=0;
	var s_cf=0;
	for(var i=0;i<des.length;i++){
		s_des+=parseFloat(des[i].value);
		s_cf+=parseFloat(cf[i].value);
	}
	$("#t_des").text(s_des+"");
	$("#t_cf").text(s_cf+"");
}

function monto_total(pos){
	var cantidad=parseFloat($("#c2"+pos).val());
}
/*function save_venta(id){
	var cv=$("input[name='cv']");
	var cu=$("input[name='cu']");
	var des=$("input[name='des']");
	var fecha=$("#fecha").val();
	var datos="";
	for(var i=0;i<cv.length;i++){
		datos+=(des[i].alt+"[$]"+cv[i].value+"[$]"+cu[i].value+"[$]"+des[i].value+"|");
	}
	post_set('pedidos_ventas/save_venta',{id,datos,fecha},'pedidos_ventas/view_all_ventas',{},'contenido');
}*/

/*function reportes_venta(idv){
	ver_modal('1','1px','50%','90%','VENTA: Reportes');
	button_modal('modal_ok_1','',"",'modal_print_1','imprimir',"'area'",'modal_closed_1');
 	post_get('pedidos_ventas/reportes_venta',{idv},'content_1');
}*//*
function configuracion_venta(idv){
	ver_modal('1','1px','50%','95%','VENTA: Configuracion');
	button_modal('modal_ok_1','modificar_venta',"'"+idv+"'",'modal_print_1','',"'area'",'modal_closed_1');
 	post_get('pedidos_ventas/configuracion_venta',{idv},'content_1');
}
function modificar_venta(idv){
	var cv=$("input[name='cv']");
	var cu=$("input[name='cu']");
	var des=$("input[name='des']");
	var fecha=$("#fecha").val();
	var datos="";
	for(var i=0;i<cv.length;i++){
		datos+=(des[i].alt+"[$]"+cv[i].value+"[$]"+cu[i].value+"[$]"+des[i].value+"|");
	}
	post_set('pedidos_ventas/modificar_venta',{idv,datos,fecha},'pedidos_ventas/view_all_ventas',{},'contenido');
}

/*------- MANEJO DE CLIENTES --------*//*
function search_cliente(){
	if($("#nit").val()=="" && $("#razon").val()=="" && $("#direc").val()=="" && $("#tel").val()==""){
		ajax_get('pedidos_ventas/view_all_clientes',{},'contenido');
	}else{
		var ele=new FormData();
		ele.append('nit',$("#nit").val());
		ele.append('razon',$("#razon").val());
		ele.append('direc',$("#direc").val());
		ele.append('tel',$("#tel").val());
		ajax_get('pedidos_ventas/search_clientes',ele,'contenido');
	}
	return false;
}
function view_all_cliente(){
	ajax_get('pedidos_ventas/view_all_clientes',{},'contenido');
}
function form_new_cliente(){
	ver_modal('1','1px','50%','75%','CLIENTE: Nuevo');
	button_modal('modal_ok_1','save_cliente','','modal_print_1','','','modal_closed_1');
	ajax_get('pedidos_ventas/form_new_cliente',{},'content_1');
}
function save_cliente(){
	var nit=$("#new_nit").val();
	var raz=$("#new_raz").val();
	var tel=$("#new_tel").val();
	var ema=$("#new_ema").val();
	var pai=$("#new_pai").val();
	var ciu=$("#new_ciu").val();
	var dir=$("#new_dir").val();
	var obs=$("#new_obs").val();
	if($("#new_nit").val()!=""){
		if($("#new_raz").val()!=""){
			
			post_set('pedidos_ventas/save_cliente',{nit:nit,raz:raz,tel:tel,ema:ema,pai:pai,ciu:ciu,dir:dir,obs:obs},'pedidos_ventas/view_all_clientes',{},'contenido');

		}else{
			alerta('Ingrese nombre u razon social válido','left','new_raz');
		}
	}else{
		alerta('Ingrese un NIT o CI válido','left','new_nit');
	}
}
function reportes_cliente(cl){
	var ele= new FormData();
	 ele.append('cl',cl);
	ver_modal('1','1px','50%','75%','CLIENTE: Reportes');
	button_modal('modal_ok_1','','','modal_print_1','imprimir',"'area'",'modal_closed_1');
	ajax_get('pedidos_ventas/detalle_cliente',ele,'content_1');
}
function configuracion_cliente(cl){
	var ele= new FormData();
	 ele.append('cl',cl);
	ver_modal('1','1px','50%','75%','CLIENTE: Configuración');
	button_modal('modal_ok_1','modificar_cliente',"'"+cl+"'",'modal_print_1','',"",'modal_closed_1');
	ajax_get('pedidos_ventas/modificar_cliente',ele,'content_1');
}
function productos_cliente(cl){
	var ele= new FormData();
	 ele.append('cl',cl);
	button_modal('modal_ok_1','',"",'modal_print_1','',"",'modal_closed_1');
	ajax_get('pedidos_ventas/productos_cliente',ele,'content_1');
}
function form_adicionar_producto_cliente(idcl){
	visible('content_modal','modal','.2%','55%','90%');
	button_modal('modal_ok','','','modal_print','','','modal_closed');
	var ele=new FormData();ele.append('idcl',idcl);
	ajax_get_2n('pedidos_ventas/search_view_productos_cliente',ele,'modal','pedidos_ventas/view_productos_cliente',ele,'search_contenido');
}
function save_producto_cliente(ele,color,idcl,idpim){
	post_set_button('pedidos_ventas/save_producto_cliente',{idcl:idcl,idpim:idpim},ele,color,'pedidos_ventas/productos_cliente',{cl:idcl},'content_1')
}
function save_presio_venta(idcl,idpc){
	var can=$("#presio"+idpc).val();
	if(can>0){
		var files= new FormData();
	 	files.append('idpc',idpc);
	 	files.append('can',can);

		ajax_file('pedidos_ventas/productos_cliente',{cl:idcl},'content_1','pedidos_ventas/save_presion_venta',files);
	}
}
function drop_producto_cliente(idcl,idpc){
	var files= new FormData();
	files.append('idpc',idpc);
	ajax_file('pedidos_ventas/productos_cliente',{cl:idcl},'content_1','pedidos_ventas/drop_presio_venta',files);
}
/*------- END MANEJO DE CLIENTES --------*/