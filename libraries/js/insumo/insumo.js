 var x=$(document);
 x.ready(inicio);
 function inicio(){
 	$("#material").click(function(){activar("material");get_2n('insumo/search_material',{},'search',false,'insumo/view_material',{},'contenido',true); url("Materiales adicionales","insumo");})
 	$("#insumo").click(function(){activar("insumo");get_2n('insumo/search_insumo',{},'search',false,'insumo/view_insumo',{},'contenido',true); url("Otros materiales o insumos","insumo?p=2");})
 	$("#config").click(function(){activar("config");get_2n('',{},'search',false,'insumo/view_config',{},'contenido',true); url("Configuración","insumo?p=3");})
 }
    function reset_view(id){
		if(id!="s_cod"){$("#s_cod").val("");}
		if(id!="s_nom"){$("#s_nom").val("");}
		if(id!="s_uni"){$("#s_uni").val("");}
	}
	function blur_all(id){
		if(id!="s_cod"){ OnBlur("s_cod"); }
		if(id!="s_nom"){ OnBlur("s_nom"); }
		if(id!="s_uni"){ OnBlur("s_uni"); }
	}
	function blur_modal(id){
		if(id!="cod"){ OnBlur("cod"); }
		if(id!="nom"){ OnBlur("nom"); }
		if(id!="med"){ OnBlur("med"); }
		if(id!="des"){ OnBlur("des"); }
		if(id!="pro"){ OnBlur("pro"); }
	}
/*------- MANEJO DE MATERIALES ADICIONALES -------*/
   	/*--- Buscador ---*/
	function view_material(){
		var ele=new FormData();
		ele.append('cod',$("#s_cod").val());
		ele.append('nom',$("#s_nom").val());
		ele.append('uni',$("#s_uni").val());
		get("insumo/view_material",ele,'contenido',true);
		blur_all("");
		return false;
   	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	function view_all_material(){
		reset_view('');
		view_material();
	}
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
   	function new_material(){
		modal('MATERIAL ADICIONAL: Nuevo','md','1');
	 	btn_modal('save_material',"",'',"",'1','md');
	 	get('insumo/new_material',{},'content_1',true);
	}
	function save_material(){
		var fot=$("#fot").prop('files');
		var nom=$("#nom").val();
		var cod=$("#cod").val();
		var med=$("#med").val();
		var des=$("#des").val();
		var pro=$("#pro").val();
		if(strSpace(nom,3,200)){
			if(entero(med,1,10)){
				var guardar=true;
				if(cod!=""){ if(!strNoSpace(cod,2,15)){ alerta("Ingrese un código válido de material...","top","cod");guardar=false; }}
				if(des!=""){ if(!strSpace(des,5,700)){ alerta("Ingrese una descripción válida de material...","top","des");guardar=false; }}
				if(pro!=""){ if(!entero(pro,1,10)){ alerta("Seleccione un proveedor de material válido...","top","pro");guardar=false; }}
				if(guardar){
					var ele= new FormData();
					ele.append("nom",nom);
					ele.append("cod",cod);
					ele.append("med",med);
					ele.append("des",des);
					ele.append("pro",pro);
					ele.append("archivo",fot[0]);
					set('insumo/save_material',ele,'NULL',true,"insumo/view_material",{},'contenido',false,'1');
				}
			}else{
				alerta("Seleccione una Medida de material...","top","med");
			}
		}else{
			alerta("Ingrese un Nombre de material válido...","top","nom");
		}
		blur_modal("");
		return false;
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
	function imprimir_materiales(json){
   		modal("MATERIALES ADICIONALES: Configuracion de impresion","lg","1");
 		btn_modal('',"",'imprimir',"'area'",'1','lg');
 		var atrib=new FormData();atrib.append('json',json);
 		get_2n('insumo/config_imprimir_materiales',atrib,'content_1',true,'insumo/imprimir_materiales',atrib,'area',true);
   	}
   	function arma_informe(json){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var v6=""; if($("#6:checked").val()){v6='ok'}
		var v7=""; if($("#7:checked").val()){v7='ok'}
		var v8=""; if($("#8:checked").val()){v8='ok'}
		var v9=""; if($("#9:checked").val()){v9='ok'}
		var v10=""; if($("#10:checked").val()){v10='ok'}
		var nro=$("#nro").val();
		var atrib=new FormData();
		atrib.append('json',json);
		atrib.append('v1',v1);
		atrib.append('v2',v2);
		atrib.append('v3',v3);
		atrib.append('v4',v4);
		atrib.append('v5',v5);
		atrib.append('v6',v6);
		atrib.append('nro',nro);
		get('insumo/imprimir_materiales',atrib,'area',true);
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	function reportes_material(idmi){
		modal('MATERIALES ADICIONALES:: Detalle','sm','1');
		btn_modal('',"",'',"",'1','sm');
		var atrib=new FormData();atrib.append('idmi',idmi);
	 	get('insumo/detalle_material',atrib,'content_1',true);
	}	
   	/*--- End Reportes ---*/ 
   	/*--- configuracion ---*/
   	function form_update_material(idmi){
		modal('MATERIAL ADICIONAL: Modificar','md','1');
	 	btn_modal('update_material',"'"+idmi+"'",'',"",'1','md');
	 	var ele=new FormData(); ele.append('idmi',idmi);
	 	get('insumo/form_update_material',ele,'content_1',true);
	}
	function update_material(idmi){
		var fot=$("#fot").prop('files');
		var nom=$("#nom").val();
		var cod=$("#cod").val();
		var med=$("#med").val();
		var des=$("#des").val();
		var pro=$("#pro").val();
		if(strSpace(nom,3,200)){
			if(entero(med,1,10)){
				var guardar=true;
				if(cod!=""){ if(!strNoSpace(cod,2,15)){ alerta("Ingrese un código válido de material...","top","cod");guardar=false; }}
				if(des!=""){ if(!strSpace(des,5,700)){ alerta("Ingrese una descripción válida de material...","top","des");guardar=false; }}
				if(pro!=""){ if(!entero(pro,1,10)){ alerta("Seleccione un proveedor de material válido...","top","pro");guardar=false; }}
				if(guardar){
					var ele= new FormData();
					ele.append("idmi",idmi);
					ele.append("nom",nom);
					ele.append("cod",cod);
					ele.append("med",med);
					ele.append("des",des);
					ele.append("pro",pro);
					ele.append("archivo",fot[0]);
					set('insumo/update_material',ele,'NULL',true,"insumo/view_material",{},'contenido',false,'1');
				}
			}else{
				alerta("Seleccione una Medida de material...","top","med");
			}
		}else{
			alerta("Ingrese un Nombre de material válido...","top","nom");
		}
		blur_modal("");
		return false;
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	function confirmar_material(idmi){
		modal("MATERIAL ADICIONAL: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','drop_material',"'"+idmi+"'");
   		var atrib=new FormData();atrib.append('idmi',idmi);
   		get('insumo/confirmar_material',atrib,'content_5',true);
   	}
   	function drop_material(idmi){
		var atrib=new FormData();
		atrib.append('u',$("#e_user").val());
		atrib.append('p',$("#e_password").val());
		atrib.append('idmi',idmi);
		set('insumo/drop_material',atrib,'NULL',true,'insumo/view_material',{},'contenido',false,'5');
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE MATERIALES ADICIONALES -------*/
/*------- MANEJO DE INSUMOS -------*/
   	/*--- Buscador ---*/
	function view_insumo(){
		var ele=new FormData();
		ele.append('cod',$("#s_cod").val());
		ele.append('nom',$("#s_nom").val());
		ele.append('uni',$("#s_uni").val());
		get("insumo/view_insumo",ele,'contenido',true);
		blur_all("");
		return false;
   	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	function view_all_insumo(){
		reset_view('');
		view_insumo();
	}
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
   	function new_insumo(){
		modal('OTROS MATERIALES E INSUMOS: Nuevo','md','1');
	 	btn_modal('save_insumo',"",'',"",'1','md');
	 	get('insumo/new_insumo',{},'content_1',true);
	}
	function save_insumo(){
		var fot=$("#fot").prop('files');
		var nom=$("#nom").val();
		var cod=$("#cod").val();
		var med=$("#med").val();
		var des=$("#des").val();
		var pro=$("#pro").val();
		if(strSpace(nom,3,200)){
			if(entero(med,1,10)){
				var guardar=true;
				if(cod!=""){ if(!strNoSpace(cod,2,15)){ alerta("Ingrese un código válido de material...","top","cod");guardar=false; }}
				if(des!=""){ if(!strSpace(des,5,700)){ alerta("Ingrese una descripción válida de material...","top","des");guardar=false; }}
				if(pro!=""){ if(!entero(pro,1,10)){ alerta("Seleccione un proveedor de material válido...","top","pro");guardar=false; }}
				if(guardar){
					var ele= new FormData();
					ele.append("nom",nom);
					ele.append("cod",cod);
					ele.append("med",med);
					ele.append("des",des);
					ele.append("pro",pro);
					ele.append("archivo",fot[0]);
					set('insumo/save_insumo',ele,'NULL',true,"insumo/view_insumo",{},'contenido',false,'1');
				}
			}else{
				alerta("Seleccione una Medida de material...","top","med");
			}
		}else{
			alerta("Ingrese un Nombre de material válido...","top","nom");
		}
		blur_modal("");
		return false;
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
	function imprimir_insumo(json){
   		modal("OTROS MATERIALES E INSUMOS: Configuración de impresión","lg","1");
 		btn_modal('',"",'imprimir',"'area'",'1','lg');
 		var atrib=new FormData();atrib.append('json',json);
 		get_2n('insumo/config_imprimir_insumo',atrib,'content_1',true,'insumo/imprimir_insumo',atrib,'area',true);
   	}
   	function arma_informe_insumo(json){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var v6=""; if($("#6:checked").val()){v6='ok'}
		var v7=""; if($("#7:checked").val()){v7='ok'}
		var v8=""; if($("#8:checked").val()){v8='ok'}
		var v9=""; if($("#9:checked").val()){v9='ok'}
		var v10=""; if($("#10:checked").val()){v10='ok'}
		var nro=$("#nro").val();
		var atrib=new FormData();
		atrib.append('json',json);
		atrib.append('v1',v1);
		atrib.append('v2',v2);
		atrib.append('v3',v3);
		atrib.append('v4',v4);
		atrib.append('v5',v5);
		atrib.append('v6',v6);
		atrib.append('nro',nro);
		get('insumo/imprimir_insumo',atrib,'area',true);
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	function reportes_insumo(idmi){
		modal('OTROS MATERIALES E INSUMOS: Detalle','sm','1');
		btn_modal('',"",'',"",'1','sm');
		var atrib=new FormData();atrib.append('idmi',idmi);
	 	get('insumo/detalle_insumo',atrib,'content_1',true);
	}	
   	/*--- End Reportes ---*/ 
   	/*--- configuracion ---*/
   	function form_update_insumo(idmi){
		modal('OTROS MATERIALES E INSUMOS: Modificar','md','1');
	 	btn_modal('update_insumo',"'"+idmi+"'",'',"",'1','md');
	 	var ele=new FormData(); ele.append('idmi',idmi);
	 	get('insumo/form_update_insumo',ele,'content_1',true);
	}
	function update_insumo(idmi){
		var fot=$("#fot").prop('files');
		var nom=$("#nom").val();
		var cod=$("#cod").val();
		var med=$("#med").val();
		var des=$("#des").val();
		var pro=$("#pro").val();
		if(strSpace(nom,3,200)){
			if(entero(med,1,10)){
				var guardar=true;
				if(cod!=""){ if(!strNoSpace(cod,2,15)){ alerta("Ingrese un código válido de material...","top","cod");guardar=false; }}
				if(des!=""){ if(!strSpace(des,5,700)){ alerta("Ingrese una descripción válida de material...","top","des");guardar=false; }}
				if(pro!=""){ if(!entero(pro,1,10)){ alerta("Seleccione un proveedor de material válido...","top","pro");guardar=false; }}
				if(guardar){
					var ele= new FormData();
					ele.append("idmi",idmi);
					ele.append("nom",nom);
					ele.append("cod",cod);
					ele.append("med",med);
					ele.append("des",des);
					ele.append("pro",pro);
					ele.append("archivo",fot[0]);
					set('insumo/update_insumo',ele,'NULL',true,"insumo/view_insumo",{},'contenido',false,'1');
				}
			}else{
				alerta("Seleccione una Medida de material...","top","med");
			}
		}else{
			alerta("Ingrese un Nombre de material válido...","top","nom");
		}
		blur_modal("");
		return false;
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	function confirmar_insumo(idmi){
		modal("MATERIAL ADICIONAL: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','drop_insumo',"'"+idmi+"'");
   		var atrib=new FormData();atrib.append('idmi',idmi);
   		get('insumo/confirmar_insumo',atrib,'content_5',true);
   	}
   	function drop_insumo(idmi){
		var atrib=new FormData();
		atrib.append('u',$("#e_user").val());
		atrib.append('p',$("#e_password").val());
		atrib.append('idmi',idmi);
		set('insumo/drop_insumo',atrib,'NULL',true,'insumo/view_insumo',{},'contenido',false,'5');
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE INSUMOS -------*/
/*------- MANEJO DE CONFIGURACION -------*/
   	/*--- Manejo de unidad ---*/
   	function save_unidad(){
		var nom=$("#nom_u").val();
		var abr=$("#abr_u").val();
		var equ=$("#equ_u").val();
		var des=$("#des_equ").val();
		if(nom!="" && nom.length>=2 && nom.length<=40){
			if(abr!="" && abr.length>=1 && abr.length<=10){
				var atrib=new FormData();
				atrib.append("nom",nom);
				atrib.append("abr",abr);
				atrib.append("equ",equ);
				atrib.append("des",des);
				set("insumo/save_unidad",atrib,'NULL',true,'insumo/view_config/',{},'contenido',false);
			}else{
				alerta("Ingrese un abreviatura de unidad válido","top","abr_u");
			}
		}else{
			alerta("Ingrese un nombre de unidad válido","top","nom_u");
		}
		return false;
	}
	function update_unidad(idu){
		var nom=$("#nom_u"+idu).val();
		var abr=$("#abr_u"+idu).val();
		var equ=$("#equ_u"+idu).val();
		var des=$("#des_equ"+idu).val();
		if(nom!="" && nom.length>=2 && nom.length<=40){
			if(abr!="" && abr.length>=1 && abr.length<=10){
				var atrib=new FormData();
				atrib.append("idu",idu);
				atrib.append("nom",nom);
				atrib.append("abr",abr);
				atrib.append("equ",equ);
				atrib.append("des",des);
				set("insumo/update_unidad",atrib,'NULL',true,'insumo/view_config/',{},'contenido',false);
			}else{
				alerta("Ingrese un abreviatura de unidad válido","top","abr_u"+idu);
			}
		}else{
			alerta("Ingrese un nombre de unidad válido","top","nom_u"+idu);
		}
		return false;
	}
	function alerta_unidad(idu) {
		modal("UNIDAD DE MEDIDA: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','drop_unidad',"'"+idu+"'");
   		var atrib=new FormData();atrib.append('idu',idu);
   		atrib.append('titulo','definitivamente la unidad de medida: <b>'+$("#nom_u"+idu).val()+' ['+$("#abr_u"+idu).val()+']</b>');
   		get('insumo/alerta',atrib,'content_5',true);
	}
	function drop_unidad(idu){
		if(idu!=""){
			var atrib=new FormData();
			atrib.append("idu",idu);
			set("insumo/drop_unidad",atrib,'NULL',true,'insumo/view_config/',{},'contenido',false,'5');
		}else{
			msj("fail");
		}
	}
   	/*--- End Manejo de unidad ---*/
/*------- END MANEJO DE CONFIGURACION -------*/