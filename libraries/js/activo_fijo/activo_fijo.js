 var x=$(document);
 x.ready(inicio);
 function inicio(){
  $('#activo_fijo').click(function(){activar("activo_fijo");get_2n('activos_fijos/search_activo',{},'search',false,'activos_fijos/view_activo',{},'contenido',true);url("Activos Fijos","activos_fijos");})
  $('#configuracion').click(function(){activar("configuracion");get_2n('',{},'search',false,'activos_fijos/view_config',{},'contenido',true);url("Configuración - Activos Fijos","activos_fijos?p=2");})
 }
 /*------- MANEJO DE ACTIVO FIJO -------*/
  	function reset_all(id){
		if(id!="s_codigo"){ $("#s_codigo").val("");}
		if(id!="s_nombre"){ $("#s_nombre").val("");}
		if(id!="s_fecha"){ $("#s_fecha").val("");}
		if(id!="s_costo"){ $("#s_costo").val("");}
	}
 	/*---- Buscar activo fijo ----*/
 	function view_activo(){
		var atrib=new FormData();
		atrib.append('cod',$("#s_codigo").val());
		atrib.append('nom',$("#s_nombre").val());
		atrib.append('fec',$("#s_fecha").val());
		atrib.append('cos',$("#s_costo").val());
		get('activos_fijos/view_activo',atrib,'contenido',true);
		return false;
	}
	function all_activo(){
		reset_all("");
		view_activo();
	}
	/*---- Buscar activo fijo ----*/
	/*---- Nuevo activo fijo ----*/
	function new_activo() {
  		modal("ACTIVO FIJO: Nuevo","md","1");
 		btn_modal('save_activo',"",'',"",'1','md');
		get('activos_fijos/new_activo',{},'content_1',true);
	}
	function save_activo() {
		var fot=$("#n_fot").prop('files');
		var nom=$("#n_nom").val();
		var cod=$("#n_cod").val();
		var cos=$("#n_cos").val();
		var fec=$("#n_fec").val();
		var dep=$("#n_dep").val();
		var des=$("#n_des").val();
		if(strSpace(nom.trim(),3,200)){
			if(strNoSpace(cod.trim(),2,15)){
				if(decimal(cos,5,1)){
					if(fecha(fec)){
						if(entero(dep,1,10)){
							var control=true;
							if(des!=""){ if(!strSpace(des.trim(),5,700)){ control=false; alerta("Ingrese una descripción del activo fijo válida...","top","n_des");}}
							if(control){
								var ele=new FormData();
								ele.append('archivo',fot[0]);
								ele.append('nom',nom);
								ele.append('cod',cod);
								ele.append('cos',cos);
								ele.append('fec',fec);
								ele.append('dep',dep);
								ele.append('des',des);
								set('activos_fijos/save_activo',ele,'NULL',true,'activos_fijos/view_activo',{},'contenido',false,'1');
							}
						}else{
							alerta("Seleccione un porcentajde de depreciacion del activo fijo válida...","top","n_dep");
						}
					}else{
						alerta("Ingrese una fecha de inicio de trabajo del activo fijo válida...","top","n_fec");
					}
				}else{
					alerta("Ingrese un costo actual del activo fijo válido...","top","n_cos");
				}
			}else{
				alerta("Ingrese un código de activo fijo válido...","top","n_cod");
			}
		}else{
			alerta("Ingrese un nombre de activo fijo válido...","top","n_nom");
		}
		return false;
	}
	/*---- End Nuevo activo fijo ----*/
	/*--- Imprimir ---*/
   	function imprimir_activos(json){
   		modal("ACTIVO FIJO: Configuración de impresion","lg","1");
 		btn_modal('',"",'imprimir',"'area'",'1','lg');
 		var atrib=new FormData();atrib.append('json',json);
 		get_2n('activos_fijos/config_imprimir_activos',atrib,'content_1',true,'activos_fijos/imprimir_activos',atrib,'area',true);
   	}
   	function arma_informe(json){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var v6=""; if($("#6:checked").val()){v6='ok'}
		var v7=""; if($("#7:checked").val()){v7='ok'}
		var v8=""; if($("#8:checked").val()){v8='ok'}
		var v9=""; if($("#9:checked").val()){v9='ok'}
		var v10=""; if($("#10:checked").val()){v10='ok'}
		var nro=$("#nro").val();
		var atribs=new FormData();
		atribs.append('json',json);
		atribs.append('v1',v1);
		atribs.append('v2',v2);
		atribs.append('v3',v3);
		atribs.append('v4',v4);
		atribs.append('v5',v5);
		atribs.append('v6',v6);
		atribs.append('v7',v7);
		atribs.append('v8',v8);
		atribs.append('v9',v9);
		atribs.append('v10',v10);
		atribs.append('nro',nro);
		get('activos_fijos/imprimir_activos',atribs,'area',true);
	}
   	/*--- End Imprimir ---*/
	/*---- Reportes activo fijo ----*/
	function reportes_activo(id) {
		modal("ACTIVO FIJO: Detalle","sm","1");
		btn_modal('',"",'',"",'1','sm');
		var ele= new FormData();
		ele.append('id',id);
		get('activos_fijos/detalle_activo',ele,'content_1',true);
	}
	/*---- End Reportes activo fijo ----*/
	/*---- Configuración activo fijo ----*/
	function configuracion_activo(id){
		modal("ACTIVO FIJO: Configuración","md","1");
		btn_modal('update_activo',"'"+id+"'","",'','1','md');
		var ele= new FormData();
		ele.append('id',id);		
		get('activos_fijos/form_update_activo',ele,'content_1',true);
	}
	function update_activo(id){
		var fot=$("#n_fot").prop('files');
		var nom=$("#n_nom").val();
		var cod=$("#n_cod").val();
		var cos=$("#n_cos").val();
		var fec=$("#n_fec").val();
		var dep=$("#n_dep").val();
		var des=$("#n_des").val();
		if(strSpace(nom.trim(),3,200)){
			if(strNoSpace(cod.trim(),2,15)){
				if(decimal(cos,5,1)){
					if(fecha(fec)){
						if(entero(dep,1,10)){
							var control=true;
							if(des!=""){ if(!strSpace(des.trim(),5,700)){ control=false; alerta("Ingrese una descripción del activo fijo válida...","top","n_des");}}
							if(control){
								var ele=new FormData();
								ele.append('archivo',fot[0]);
								ele.append('id',id);
								ele.append('nom',nom);
								ele.append('cod',cod);
								ele.append('cos',cos);
								ele.append('fec',fec);
								ele.append('dep',dep);
								ele.append('des',des);
								set('activos_fijos/update_activo',ele,'NULL',true,'activos_fijos/view_activo',{},'contenido',false,'1');
							}
						}else{
							alerta("Seleccione un porcentajde de depreciacion del activo fijo válida...","top","n_dep");
						}
					}else{
						alerta("Ingrese una fecha de inicio de trabajo del activo fijo válida...","top","n_fec");
					}
				}else{
					alerta("Ingrese un costo actual del activo fijo válido...","top","n_cos");
				}
			}else{
				alerta("Ingrese un código de activo fijo válido...","top","n_cod");
			}
		}else{
			alerta("Ingrese un nombre de activo fijo válido...","top","n_nom");
		}
		return false;
	}
	/*---- End Configuración activo fijo ----*/
   	/*--- Eliminar ---*/
   	function confirmar(idaf){
   		modal("ACTIVO FIJO: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','drop_activo',"'"+idaf+"'");
   		var atrib=new FormData();atrib.append('idaf',idaf);
   		get('activos_fijos/confirmar',atrib,'content_5',true);
   	}
   	function drop_activo(idaf){
		var atrib=new FormData();
		atrib.append('u',$("#e_user").val());
		atrib.append('p',$("#e_password").val());
		atrib.append('idaf',idaf);
		set('activos_fijos/drop_activo',atrib,'NULL',true,'activos_fijos/view_activo',{},'contenido',false,'5');
	}
   	/*--- End Eliminar ---*/
 /*------- END MANEJO DE ACTIVO FIJO -------*/
 /*------- MANEJO DE CONFIGURACION -------*/
 	/*---- activo fijo - depreciaciones ----*/
 	function save_depreciacion() {
 		var nom=$("#nom_d").val();
 		var anio=$("#anio_d").val();
 		var coe=$("#coe_d").val();
 		if(strSpace(nom,2,100)){
 			if(decimal(anio,3,1) && anio<=999.9){
 				if(decimal(coe,3,2) && coe<=999.99){
	 				var atrib=new FormData();
	 				atrib.append('nom',nom);
	 				atrib.append('anio',anio);
	 				atrib.append('coe',coe);
	 				set('activos_fijos/save_depreciacion',atrib,'NULL',true,'activos_fijos/view_config',{},'contenido',false,'');
 				}else{
 					alerta("Ingrese un porcentaje de depreciación válido...","top","coe_d");
 				}
 			}else{
 				alerta("Ingrese un año de vida util válido...","top","anio_d");
 			}
 		}else{
 			alerta("Ingrese un nombre de porcentaje de depreciación válida...","top","nom_d");
 		}
 		return false;
 	}
 	function update_depreciacion(cod) {
 		var nom=$("#nom_d"+cod).val();
 		var anio=$("#anio_d"+cod).val();
 		var coe=$("#coe_d"+cod).val();
 		if(strSpace(nom,2,100)){
 			if(decimal(anio,3,1) && anio<=999.9){
 				if(decimal(coe,3,2) && coe<=999,99){
 				var atrib=new FormData();
 				atrib.append('cod',cod);
 				atrib.append('nom',nom);
 				atrib.append('anio',anio);
 				atrib.append('coe',coe);
 				set('activos_fijos/update_depreciacion',atrib,'NULL',true,'activos_fijos/view_config',{},'contenido',false,'');
 			}else{
 					alerta("Ingrese un porcentaje de depreciación válido...","top","coe_d"+cod);
 				}
 			}else{
 				alerta("Ingrese un año de vida util válido...","top","anio_d"+cod);
 			}
 		}else{
 			alerta("Ingrese un nombre de porcentaje de depreciación válida...","top","nom_d"+cod);
 		}
 		return false;
 	}
 	function alerta_depreciacion(iddpre){
   		modal("DEPRECIACIÓN: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','delete_depreciacion',"'"+iddpre+"'");
   		var atrib=new FormData();atrib.append('iddpre',iddpre);
   		atrib.append('titulo','definitivamente la depreciación: <b>'+$("#nom_d"+iddpre).val())+'</b>';
   		get('activos_fijos/alerta',atrib,'content_5',true);
   	}
 	function delete_depreciacion(cod){
 		var atrib=new FormData();
 		atrib.append('cod',cod);
 		set('activos_fijos/delete_depreciacion',atrib,'NULL',true,'activos_fijos/view_config',{},'contenido',false,'5');
 	}
 	/*---- end activo fijo - depreciaciones ----*/
 /*------- END MANEJO DE CONFIGURACION -------*/