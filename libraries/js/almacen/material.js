var aid='';
 function inicio(v){
 	aid=v;
  	$("#material").click(function(){activar("material"); get_2n('../../material/view_search',{},"search",false,"../../material/view_material/"+aid,{},"contenido",true);url("Materiales",aid);})
  	$("#ingreso").click(function(){get_2n('../../material/search_ingreso',{},"search",false,"../../material/view_ingreso/"+aid,{},"contenido",true);activar("ingreso","Ingreso de materiales",aid+"?p=2");})
  	$("#salida").click(function(){get_2n('../../material/search_salida',{},"search",false,"../../material/view_salida/"+aid,{},"contenido",true);activar("salida","Salida de material",aid+"?p=3");})
  	$("#config").click(function(){activar("config");get_2n('',{},'search',false,'../../material/view_config',{},'contenido',true);url("Configuracion",aid+"?p=5");})
 }
/*------- MANEJO DE MATERIALES -------*/
   	/*--- Buscador ---*/
   	function reset_all_view(id){
		if(id!="search_cod"){$("#search_cod").val("");}
		if(id!="search_nom"){$("#search_nom").val("");}
		if(id!="search_gru"){$("#search_gru").val("");}
		if(id!="search_can"){$("#search_can").val("");}
		if(id!="search_uni"){$("#search_uni").val("");}
		if(id!="search_col"){$("#search_col").val("");}
		//reset search proveedor
		if(id!="p_nit"){ $("#p_nit").val(""); }
		if(id!="p_razon"){ $("#p_razon").val(""); }
		if(id!="p_encargado"){ $("#p_encargado").val(""); }
		if(id!="p_telefono"){ $("#p_telefono").val(""); }
		if(id!="p_direccion"){ $("#p_direccion").val(""); }
	}
	function blur_all(id){
		if(id!="search_cod"){ OnBlur("search_cod"); }
		if(id!="search_nom"){ OnBlur("search_nom"); }
		if(id!="search_gru"){ OnBlur("search_gru"); }
		if(id!="search_cod"){ OnBlur("search_can"); }
		if(id!="search_cod"){ OnBlur("search_uni"); }
		if(id!="search_cod"){ OnBlur("search_col"); }
	}
   	function view_material(){
		var ele=new FormData();
		ele.append('cod',$("#search_cod").val());
		ele.append('nom',$("#search_nom").val());
		ele.append('gru',$("#search_gru").val());
		ele.append('can',$("#search_can").val());
		ele.append('col',$("#search_col").val());
		get("../../material/view_material/"+aid,ele,'contenido',true);
		blur_all("");
		return false;
   	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	function all_material(){
		reset_all_view('');
		view_material();
	}
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
   	function new_material(){
		modal('MATERIAL: Nuevo','md','1');
	 	btn_modal('save_material',"",'',"",'1','md');
	 	get('../../material/new_material/'+aid,{},'content_1',false);
	}
	function save_material(){
		var fot=$("#fot").prop('files');
		var nom=$("#nom").val();
		var cod=$("#cod").val();
		var gru=$("#gru").val();
		var col=$("#col").val();
		var med=$("#med").val();
		var des=$("#des").val();
		var pro=$("#pro").val();
		var cos_pro=$("#cos_pro").val();
		if(strSpace(nom,3,200)){
			if(strNoSpace(cod,2,15)){
				if(entero(gru,0,10)){
						if(entero(col,0,10)){
							if(entero(med,0,10)){
								var control=true;
								if(des!=""){if(!textarea(des,0,700)){ control=false;alerta("Ingrese un contenido válido","top","des");}}
								var files= new FormData();
								files.append("nom",nom);
								files.append("cod",cod);
								files.append("gru",gru);
								files.append("med",med);
								files.append("col",col);
								files.append("des",des);
								files.append("pro",pro);
								files.append("cos_pro",cos_pro);
								files.append("archivo",fot[0]);
								var atrib3=new FormData();atrib3.append('cod',$("#search_cod").val());atrib3.append('nom',$("#search_nom").val());atrib3.append('gru',$("#search_gru").val());atrib3.append('can',$("#search_can").val());atrib3.append('col',$("#search_col").val());
								set('../../material/save_material/'+aid,files,'NULL',true,"../../material/view_material/"+aid,atrib3,'contenido',false,'1');

							}else{
								alerta("Seleccione una Medida de material...","top","med");
							}
						}else{
							alerta("Selecciones un color de material...","top","col");
						}
				}else{
					alerta("Selecciones un Grupo de material...","top","gru");
				}
			}else{
				alerta("Ingrese un Código material válido...","top","cod");
			}
		}else{
			alerta("Ingrese un Nombre de material válido...","top","nom");
		}
		return false;
	}
	function form_add_material(){
		modal('MATERIAL: Adicionar Material existente',"md",'1');
	 	btn_modal('',"",'',"",'1','md');
	 	get_2n('../../material/form_add_material/'+aid,{},'content_1',false,'../../material/add_material/'+aid,{},'contenido_2',true);
	}
	function search_add_material(){
		var alm=$("#alm").val();
		var atrib=new FormData();atrib.append('alm',alm);
		get('../../material/add_material/'+aid,atrib,'contenido_2',true);
	}
	function add_material_almacen(idm,ida,ele,color,diametro){
		var atrib=new FormData();
		atrib.append('idm',idm);
		atrib.append('ida',ida);
		atrib.append('alm',$("#alm").val());
		var atrib3=new FormData();atrib3.append('cod',$("#search_cod").val());atrib3.append('nom',$("#search_nom").val());atrib3.append('gru',$("#search_gru").val());atrib3.append('can',$("#search_can").val());atrib3.append('col',$("#search_col").val());
		set_button('../../material/add_material_almacen/'+aid,{idm,ida},"../../material/view_material/"+aid,atrib3,'contenido',true,ele,color,diametro);
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	function imprimir_materiales(json){
   		modal("MATERIALES: Configuracion de impresion","lg","1");
 		btn_modal('',"",'imprimir',"'area'",'1','lg');
 		var atribs=new FormData();atribs.append('json',json);
 		atribs.append('v1',"ok");
		atribs.append('v2',"ok");
		atribs.append('v3',"ok");
		atribs.append('v4',"ok");
		atribs.append('v5',"ok");
		atribs.append('v6',"ok");
		atribs.append('v7',"ok");
		atribs.append('v8',"ok");
		atribs.append('v9',"ok");
		atribs.append('v10',"ok");
 		get_2n('../../material/config_imprimir_materiales/'+aid,atribs,'content_1',true,'../../material/imprimir_materiales/'+aid,atribs,'area',true);
   	}
   	function arma_informe(json){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var v6=""; if($("#6:checked").val()){v6='ok'}
		var v7=""; if($("#7:checked").val()){v7='ok'}
		var v8=""; if($("#8:checked").val()){v8='ok'}
		var v9=""; if($("#9:checked").val()){v9='ok'}
		var v10=""; if($("#10:checked").val()){v10='ok'}
		var nro=$("#nro").val();
		var atribs=new FormData();
		atribs.append('json',json);
		atribs.append('v1',v1);
		atribs.append('v2',v2);
		atribs.append('v3',v3);
		atribs.append('v4',v4);
		atribs.append('v5',v5);
		atribs.append('v6',v6);
		atribs.append('v7',v7);
		atribs.append('v8',v8);
		atribs.append('v9',v9);
		atribs.append('v10',v10);
		atribs.append('nro',nro);
		get('../../material/imprimir_materiales/'+aid,atribs,'area',true);
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	function reportes_material(idmi){
		modal('MATERIAL: Detalle','sm','1');
		btn_modal('',"",'',"",'1','sm');
		var ele=new FormData();ele.append('idmi',idmi);
	 	get('../../material/detalle_material/'+aid,ele,'content_1',true);
	}	
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	function configuracion_material(idmi){
		modal('MATERIAL: Modificar','md','1');
	 	btn_modal('update_material',"'"+idmi+"'",'',"",'1','md');
	 	var ele=new FormData(); ele.append('idmi',idmi);
	 	get('../../material/form_update_material/'+aid,ele,'content_1',true);
	}
	function update_material(idmi){
		var fot=$("#fot").prop('files');
		var nom=$("#nom").val();
		var cod=$("#cod").val();
		var gru=$("#gru").val();
		var med=$("#med").val();
		var col=$("#col").val();
		var des=$("#des").val();
		var pro=$("#pro").val();
		var cos_pro=$("#cos_pro").val();
		if(strSpace(nom,3,200)){
			if(strNoSpace(cod,2,15)){
				if(entero(gru,0,10)){
						if(entero(col,0,10)){
							if(entero(med,0,10)){
								var control=true;
								if(des!=""){if(!textarea(des,0,700)){ control=false;alerta("Ingrese un contenido válido","top","des");}}
								var files= new FormData();
								files.append("idmi",idmi);
								files.append("nom",nom);
								files.append("cod",cod);
								files.append("gru",gru);
								files.append("med",med);
								files.append("col",col);
								files.append("des",des);
								files.append("pro",pro);
								files.append("cos_pro",cos_pro);
								files.append("archivo",fot[0]);
								var atrib3=new FormData();atrib3.append('cod',$("#search_cod").val());atrib3.append('nom',$("#search_nom").val());atrib3.append('gru',$("#search_gru").val());atrib3.append('can',$("#search_can").val());atrib3.append('col',$("#search_col").val());
								set('../../material/update_material/'+aid,files,'NULL',true,"../../material/view_material/"+aid,atrib3,'contenido',false,'1');

							}else{
								alerta("Seleccione una Medida de material...","top","med");
							}
						}else{
							alerta("Selecciones un color de material...","top","col");
						}
				}else{
					alerta("Selecciones un Grupo de material...","top","gru");
				}
			}else{
				alerta("Ingrese un Código material válido...","top","cod");
			}
		}else{
			alerta("Ingrese un Nombre de material válido...","top","nom");
		}
		return false;
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	function confirmar_material(idmi){
   		modal("MATERIAL: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','drop_material',"'"+idmi+"'");
   		var atribs=new FormData();atribs.append('idmi',idmi);
   		get('../../material/confirmar_material/'+aid,atribs,'content_5',true);
   	}
   	function drop_material(idmi){
		var atribs=new FormData();
		atribs.append('u',$("#e_user").val());
		atribs.append('p',$("#e_password").val());
		atribs.append('idmi',idmi);
		var atrib3=new FormData();atrib3.append('cod',$("#search_cod").val());atrib3.append('nom',$("#search_nom").val());atrib3.append('gru',$("#search_gru").val());atrib3.append('can',$("#search_can").val());atrib3.append('col',$("#search_col").val());
		set('../../material/drop_material/'+aid,atribs,'NULL',true,"../../material/view_material/"+aid,atrib3,'contenido',false,'5');
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE MATERIALES -------*/
/*------- MANEJO DE INGRESO SALIDA DE MATERIAL -------*/
   	/*--- Buscador ---*/
   	function view_ingreso(){
   		var atrib=new FormData();
		atrib.append('nom',$("#search_nom").val());
		atrib.append('can',$("#search_can").val());
		atrib.append('uni',$("#search_uni").val());
		get("../../material/view_ingreso/"+aid,atrib,'contenido',true);
		blur_all("");
		return false;
   	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	function all_ingreso(){
		reset_all_view('');
		view_ingreso();
	}
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
   	/*--- End Nuevo ---*/
	function save_movimiento(idam,tipo){
		var sto=$("#c"+idam).val();
		var fech=$("#fech"+idam).val();
		var can=$("#can"+idam).val();
		var obs=$("#obs"+idam).val();
			if(fech!=""){
				if(decimal(can,9,7) && can>0){
					if(textarea(obs,0,300)){
						var control=true;
						if(tipo=="s"){
							if((sto*1)<(can*1)){ msj("Stock insuficiente, verifique la cantidad de salida...!"); control=false;}
							var emp=$("#emp"+idam).val();
							if(!entero(emp,0,10)){ alerta("Seleccione un empleado","top","emp"+idam); control=false;}
						}
						if(control){
							var atrib=new FormData();
							atrib.append('idam',idam);
							atrib.append('sto',sto);
							atrib.append('fech',fech);
							atrib.append('can',can);
							atrib.append('obs',obs);
							if(tipo=="s"){ atrib.append('emp',emp);}
							set_id("../../material/save_movimiento/"+aid,atrib,idam,true);
						}
					}else{
						alerta("Ingrese un contenido válido entre 9999999999.9999999","top","obs"+idam);
					}
				}else{
					alerta("Ingrese un cantidad válida mayor a cero...","top","can"+idam);
				}
			}else{
				alerta("Ingrese una fecha valida","top","fech"+idam);
			}
		return false;
	}
/*------- END MANEJO DE INGRESO SALIDA DE MATERIAL -------*/
/*------- SALIDA DE MATERIAL -------*/
   	/*--- Buscador ---*/
   	function view_salida(){
   		var atrib=new FormData();
		atrib.append('nom',$("#search_nom").val());
		atrib.append('can',$("#search_can").val());
		atrib.append('uni',$("#search_uni").val());
		get("../../material/view_salida/"+aid,atrib,'contenido',true);
		blur_all("");
		return false;
   	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	function all_salida(){
		reset_all_view('');
		view_salida();
	}
   	/*--- End Ver Todo ---*/
/*------- END SALIDA DE MATERIAL -------*/
/*------- MANEJO DE CONFIGURACIONES -------*/
   	/*--- Manejo de unidad ---*/
   	function save_unidad(){
		var nom=$("#nom_u").val();
		var abr=$("#abr_u").val();
		var equ=$("#equ_u").val();
		var des=$("#des_equ").val();
		if(strSpace(nom,2,40)){
			if(strSpace(abr,1,10)){
				var atrib=new FormData();
				atrib.append("nom",nom);
				atrib.append("abr",abr);
				atrib.append("equ",equ);
				atrib.append("des",des);
				set("../../material/save_unidad",atrib,'NULL',true,'../../material/view_config/',{},'contenido',false);
			}else{
				alerta("Ingrese un abreviatura de unidad válido","top","abr_u");
			}
		}else{
			alerta("Ingrese un nombre de unidad válido","top","nom_u");
		}
		return false;
	}
	function update_unidad(idu){
		var nom=$("#nom_u"+idu).val();
		var abr=$("#abr_u"+idu).val();
		var equ=$("#equ_u"+idu).val();
		var des=$("#des_equ"+idu).val();
		if(strSpace(nom,2,40)){
			if(strSpace(abr,1,10)){
				var atrib=new FormData();
				atrib.append("idu",idu);
				atrib.append("nom",nom);
				atrib.append("abr",abr);
				atrib.append("equ",equ);
				atrib.append("des",des);
				set("../../material/update_unidad",atrib,'NULL',true,'../../material/view_config/',{},'contenido',false);
			}else{
				alerta("Ingrese un abreviatura de unidad válido","top","abr_u"+idu);
			}
		}else{
			alerta("Ingrese un nombre de unidad válido","top","nom_u"+idu);
		}
		return false;
	}
	function alerta_unidad(idu) {
		modal("UNIDAD DE MEDIDA: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','drop_unidad',"'"+idu+"'");
   		var atrib=new FormData();atrib.append('idu',idu);
   		atrib.append('titulo','definitivamente la unidad de medida: '+$("#nom_u"+idu).val()+' ['+$("#abr_u"+idu).val()+']');
   		get('../../material/alerta',atrib,'content_5',true);
	}
	function drop_unidad(idu){
		if(idu!=""){
			var atrib=new FormData();
			atrib.append("idu",idu);
			set("../../material/drop_unidad",atrib,'NULL',true,'../../material/view_config/',{},'contenido',false,'5');
		}else{
			msj("fail");
		}
	}
   	/*--- End Manejo de unidad ---*/
   	/*--- End Manejo de color ---*/
   	function save_color(){
		var nom=$("#nom_c").val();
		var cod=$("#cod_c").val();
		if(strSpace(nom,2,50)){
			if(cod!=""){
				var atrib=new FormData();
				atrib.append("nom",nom);
				atrib.append("cod",cod);
				set("../../material/save_color",atrib,'NULL',true,'../../material/view_config/',{},'contenido',false);
			}else{
				alerta("Ingrese un color válido","top","cod_c");
			}
		}else{
			alerta("Ingrese un nombre de color válido","top","nom_c");
		}
		return false;
	}
	function update_color(idco){
		var nom=$("#nom_c"+idco).val();
		var cod=$("#cod_c"+idco).val();
		if(strSpace(nom,2,50)){
			if(cod!=""){
				var atrib=new FormData();
				atrib.append("idco",idco);
				atrib.append("nom",nom);
				atrib.append("cod",cod);
				set("../../material/update_color",atrib,'NULL',true,'../../material/view_config/',{},'contenido',false);
			}else{
				alerta("Ingrese un color válido","top","cod_c"+idco);
			}
		}else{
			alerta("Ingrese un nombre de color válido","top","nom_c"+idco);
		}
		return false;
	}
	function alerta_color(idco) {
		modal("UNIDAD DE MEDIDA: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','drop_color',"'"+idco+"'");
   		var atrib=new FormData();
   		atrib.append('titulo','definitivamente el color: <strong>'+$("#nom_c"+idco).val()+'</strong>');
   		get('../../material/alerta',atrib,'content_5',true);
	}
	function drop_color(idco){
		if(idco!=""){
			var atrib=new FormData();atrib.append("idco",idco);
			set("../../material/drop_color",atrib,'NULL',true,'../../material/view_config/',{},'contenido',false,'5');
		}else{
			msj("fail");
		}
	}
   	/*--- End Manejo de color ---*/  
	/*--- Manejo de grupo ---*/
   	function save_grupo(){
		var nom=$("#nom_g").val();
		var des=$("#des_g").val();
		if(strSpace(nom,2,50)){
			if(textarea(des,0,200)){
				var atrib=new FormData();
				atrib.append("nom",nom);
				atrib.append("des",des);
				set("../../material/save_grupo",atrib,'NULL',true,'../../material/view_config/',{},'contenido',false);
			}else{
				alerta("Ingrese una descripción de grupo valida","top","des_g");
			}
		}else{
			alerta("Ingrese un nombre de grupo válido","top","nom_g");
		}
		return false;
	}
	function update_grupo(idg){
		var nom=$("#nom_g"+idg).val();
		var des=$("#des_g"+idg).val();
		if(strSpace(nom,2,50)){
			if(textarea(des,0,200)){
				var atrib=new FormData();
				atrib.append("idg",idg);
				atrib.append("nom",nom);
				atrib.append("des",des);
				set("../../material/update_grupo",atrib,'NULL',true,'../../material/view_config/',{},'contenido',false);
			}else{
				alerta("Ingrese una descripción de grupo valida","top","des_g"+idg);
			}
		}else{
			alerta("Ingrese un nombre de grupo válido","top","nom_g"+idg);
		}
		return false;
	}
	function alerta_grupo(idg){
		modal("GRUPOS: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','drop_grupo',"'"+idg+"'");
   		var atrib=new FormData();
   		atrib.append('titulo','definitivamente el grupo: <strong>'+$("#nom_g"+idg).val()+'</strong>');
   		get('../../material/alerta',atrib,'content_5',true);
	}
	function drop_grupo(idg){
		if(idg!=""){
			var atrib=new FormData();
			atrib.append("idg",idg);
			set("../../material/drop_grupo",atrib,'NULL',true,'../../material/view_config/',{},'contenido',false,'5');
		}else{
			msj("fail");
		}
	}
	/*--- End Manejo de grupo ---*/
/*------- END MANEJO DE CONFIGURACIONES -------*/

