var aid='';
 function inicio(v){
 	aid=v;
 	get_2n('../../producto/view_search_producto/'+aid,{},"search",false,"../../producto/view_producto/"+aid,{},"contenido",true);
 	$("#reg").click(function(){activar("reg"); get_2n('../../producto/view_search_producto/'+aid,{},"search",false,"../../producto/view_producto/"+aid,{},"contenido",true);})
 }

 function reset_all_view(id){
 	if(id!="search_cod"){$("#search_cod").val("");}
	if(id!="search_nom"){$("#search_nom").val("");}
	if(id!="search_can"){$("#search_can").val("");}
 }
 function blur_all(id){
 	if(id!="search_cod"){ OnBlur("search_cod"); }
 	if(id!="search_nom"){ OnBlur("search_nom"); }
 	if(id!="search_can"){ OnBlur("search_can"); }
 }
 function reset_all_producto(id) {
 	if(id!="s2_cod"){$("#s2_cod").val("");}
	if(id!="s2_nom"){$("#s2_nom").val("");}
 }
/*------- MANEJO DE PRODUCTOS -------*/
   	/*--- Buscador ---*/
   	function view_producto(){
		var atrib=new FormData();
		atrib.append('cod',$("#search_cod").val());
		atrib.append('nom',$("#search_nom").val());
		atrib.append('can',$("#search_can").val());
		get("../../producto/view_producto/"+aid,atrib,'contenido',true);
		blur_all("");
		return false;
   	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	function view_all_producto(){
		reset_all_view('');
		view_producto();
	}
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
   	function form_select_producto(){
		modal("PRODUCTO: Adicionar",'sm',"1");
		btn_modal('',"",'',"",'1','sm');
	 	get_2n("../../producto/form_search_producto/"+aid,{},'content_1',true,"../../producto/form_select_producto/"+aid,{},'content_2',false);
	}
	function view_producto_2(){
		var cod=$("#s2_cod").val();
		var nom=$("#s2_nom").val();
		var atrib=new FormData();
		atrib.append('cod',cod);atrib.append('nom',nom);
		get("../../producto/form_select_producto/"+aid,atrib,'content_2',true);
		return false;
	}
	function all_producto_2() {
		reset_all_producto('');
		view_producto_2();
	}
	function change_almacen_produco(ele,color,diametro,idpim){
		set_button('../../producto/change_almacen_produco/'+aid,{idpim},"../../producto/view_producto/"+aid,{},"contenido",false,ele,color,diametro)
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	function imprimir_producto(json){
   		modal("ALMACEN DE PRODUCTOS: Configuracion de impresion","lg","1");
 		btn_modal('',"",'imprimir',"'area'",'1','lg');
 		var atrib=new FormData();atrib.append('json',json);
 		atrib.append('v1',"ok");
		atrib.append('v2',"ok");
		atrib.append('v3',"ok");
		atrib.append('v4',"ok");
		atrib.append('v5',"ok");
		atrib.append('v6',"ok");
		atrib.append('v7',"ok");
		atrib.append('v8',"ok");
		atrib.append('v9',"ok");
 		get_2n('../../producto/config_imprimir_producto/'+aid,atrib,'content_1',true,'../../producto/imprimir_producto/'+aid,atrib,'area',true);
   	}
   	function arma_informe(json){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var v6=""; if($("#6:checked").val()){v6='ok'}
		var v7=""; if($("#7:checked").val()){v7='ok'}
		var v8=""; if($("#8:checked").val()){v8='ok'}
		var v9=""; if($("#9:checked").val()){v9='ok'}
		var nro=$("#nro").val();
		var atrib=new FormData();
		atrib.append('json',json);
		atrib.append('v1',v1);
		atrib.append('v2',v2);
		atrib.append('v3',v3);
		atrib.append('v4',v4);
		atrib.append('v5',v5);
		atrib.append('v6',v6);
		atrib.append('v7',v7);
		atrib.append('v8',v8);
		atrib.append('v9',v9);
		atrib.append('nro',nro);
		get('../../producto/imprimir_producto/'+aid,atrib,'area',true);
	}
   	/*--- End Imprimir ---*/
   	function IngresoSalida(idap){
	 	var sto=$("#c"+idap).val();
	 	var i=$('#i'+idap).val();
	 	var s=$('#s'+idap).val();
	 	var sa=$('#sa'+idap).val();
	 	var fech=$("#fech").val();
	 	if(fech!=""){
		 	if(esNumero(i)){
		 		if(esNumero(s)){
		 			if((parseFloat(sto)+parseFloat(i)-parseFloat(s))>=0){
				 		var atrib=new FormData();
				 		atrib.append('idap',idap);
				 		atrib.append('ing',i);
				 		atrib.append('sal',s);
				 		atrib.append('fech',fech);
				 		set_id("../../producto/save_reg_mov/"+aid,atrib,idap,true);
				 	}else{
				 		msj("fail");
				 	}
		 		}else{
		 			alerta("Ingrese un Cantidad de salida válida...","top","s"+idap);
		 		}
		 	}else{
		 		alerta("Ingrese un Cantidad de ingreso válido...","top","i"+idap);
		 	}
	 	}else{
	 		alerta("Ingrese una fecha valida","left","fech"+idap);
	 	}
	 	return false;
	}
   	/*--- Eliminar ---*/
   	function confirmar(idap,nombre,url){
   		modal("PRODUCTO EN EL ALMACEN: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','drop_producto',"'"+idap+"'");
   		var atrib=new FormData(); atrib.append('idap',idap);atrib.append('url',url);atrib.append('nombre',nombre);
   		get("../../producto/confirmar/"+aid,atrib,'content_5',true);
   	}
   	function drop_producto(idap){
		var atrib=new FormData();
		atrib.append('u',$("#e_user").val());
		atrib.append('p',$("#e_password").val());
		atrib.append('idap',idap);
		//set('../../producto/drop_material/'+aid,atrib,'NULL',true,"../../material/all_view/"+aid,{},'contenido',false,'5');
		set('../../producto/drop_producto/'+aid,atrib,'NULL',true,"../../producto/view_producto/"+aid,{},'contenido',false,'5');
	}
   /*function drop_producto(idap){
   		var ele=new FormData();ele.append('idap',idap);
   		ele.append('u',$("#e_user").val());
		ele.append('p',$("#e_password").val());
		ajax_set('../../producto/drop_producto',ele,'NULL',"../../producto/view_producto/"+aid,{},'contenido','5');
   	}*/
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE PRODUCTOS -------*/


/*function search_add_producto(){
	var c=$("#cod").val();
	var n=$("#nom").val();
	var a=''
	var v=''
	if(esCadena(c)){
		a='c'
		v=c;
	}else{
		if(esCadena(n)){
			a='n'
			v=n;
		}else{
			msj("Ingreso un campo de busqueda","alert_info","modal_alert");
		}
	}
	if(a!='' & v!=''){
		post_get('../../producto/view_search_producto/'+aid,{a:a,v:v},'modal');
	}
	return false;
}

function add_material(p){
	post_set("../../producto/add_producto/"+aid,{p:p},'../../producto/all_view/'+aid,{},"result");
}*/

 /*function block(a,i){
 	var id=a.value;
 	if(!esNumero(id)){ a.value=0;}else{ var saldo=parseFloat($("#c"+i).val())+parseFloat($("#i"+i).val())-parseFloat($("#s"+i).val());  $("#sa"+i).val(saldo); if(saldo<0){input_invalido('sa'+i)}else{input_valido('sa'+i)}}
 }

function reset_rev_mov(val,id){
	$("#c"+id).val(val);
	$("#i"+id).val(0);
	input_default("i"+id)
	$("#s"+id).val(0);
	input_default("s"+id)
	$("#sa"+id).val(val);
	input_default_disabled("sa"+id);
}
function input_valido(id){
	var ele=$("#"+id);
	ele.css({background: "rgba(0,64,0,0.5)",color: "white"});

}
function input_invalido(id){
	var ele=$("#"+id);
	ele.css({background: "rgba(255,0,0,0.5)",color: "white"});
}
function input_default(id){
	var ele=$("#"+id);
	ele.css({background: "white",color: "black"});
}
function input_default_disabled(id){
	var ele=$("#"+id);
	ele.css({background: "rgba(235,235,228,0.9)",color: "rgba(84,84,84,0.9)", border:"1px solid rgba(84,84,84,0.4)"});
}


function logout(id,nom,img){
	$('#logout_text').html('<h2>¿Desea Eliminar El producto: '+nom+' del almacen?</h2>');
	visible_logout('logout',450,id);
	addImg('img_logout','libraries/img/productos/miniatura/'+img);
	addClick_v2('btn_eliminar','eliminar_producto_almacen',id);
}
function eliminar_producto_almacen(id){
	var usu=$('#e_user').val();
	var pass=$('#e_password').val();
	post_delete_validar('../../producto/valida_usuario',{u:usu,p:pass},'../../producto/eliminar_producto_almacen',{id:id},'../../producto/all_view/'+aid,{},'result');
}*/
