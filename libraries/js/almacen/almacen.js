 var x=$(document);
 x.ready(inicio);
 function inicio(){
 	$("#ver").click(function(){activar("ver");get_2n('almacen/view_search_almacen',{},'search',false,'almacen/view_almacen',{},'contenido',true); activar('ver','Almacenes','almacen?p=1');})
 	$("#hist").click(function(){activar("hist");get_2n('almacen/search_movimiento_material',{},'search',false,'almacen/view_movimiento_material',{},'contenido',true);  activar('hist','Historial de movimiento de materiales','almacen?p=2');})
 	$("#hist2").click(function(){activar("hist2");get_2n('almacen/view_search_movimiento_producto',{},'search',false,'almacen/view_movimiento_producto',{},'contenido',true);  activar('hist2','Historial de movimiento de productos','almacen?p=3');})
 }
/*------- MANEJO DE ALMACENES -------*/
   	/*--- Buscador ---*/
   	function reset_all_view(id){
		if(id!="search_nom"){ $("#search_nom").val(""); }
		if(id!="search_tipo"){ $("#search_tipo").val(""); }
		//caso historial de materiales
		if(id!="s_cod"){ $("#s_cod").val(""); }else{ $("#hra").val("");$("#fech_a").val("");$("#fech_b").val(""); }
		if(id!="s_nom"){ $("#s_nom").val(""); }else{ $("#hra").val("");$("#fech_a").val("");$("#fech_b").val(""); }
		if(id!="s_alm"){ $("#s_alm").val(""); }else{ $("#hra").val("");$("#fech_a").val("");$("#fech_b").val(""); }
		if(id!="s_usu"){ $("#s_usu").val(""); }else{ $("#hra").val("");$("#fech_a").val("");$("#fech_b").val(""); }	
		if(id!="s_sol"){ $("#s_sol").val(""); }else{ $("#hra").val("");$("#fech_a").val("");$("#fech_b").val(""); }	
	}
	function blur_all(){
		OnBlur("search_nom");
		OnBlur("search_tip");
		OnBlur("s_cod");
		OnBlur("s_nom");
		OnBlur("s_alm");
		OnBlur("s_usu");
		OnBlur("s_sol");
		OnBlur("fech_a");
		OnBlur("fech_b");
	}
	function view_almacen(){
		var cod=$("#search_cod").val();
		var nom=$("#search_nom").val();
		var tip=$("#search_tipo").val();
		var atrib=new FormData();
		atrib.append('cod',cod);
		atrib.append('nom',nom);
		atrib.append('tip',tip);
		get("almacen/view_almacen",atrib,'contenido',true);
		blur_all();
		return false;
	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	function all_almacen(){
   		reset_all_view("NULL");
		view_almacen();
	}
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
   	 function view_form_new_almacen(){
	 	modal("ALMACEN: Nuevo","sm","1");
	 	btn_modal('save_almacen',"",'',"","1","sm");
	 	get('almacen/view_form_new_almacen',{},'content_1',true);
	 }
	function save_almacen(){
	 	var fot=$("#archivo").prop('files');
	 	var cod=$("#cod").val();
	 	var nom=$("#nom").val();
	 	var tip=$("#tip").val();
		var des=$("#des").val();
		if(strSpace(cod,2,10)){
			if(strSpace(nom,3,100)){
				if(tip=="material" || tip=="producto"){
					var control=true;
					if(des!=""){ if(!textarea(des,0,400)){ control=false; alerta('Ingrese un contenido válido','top','des');}}
					if(control){
						var files= new FormData();
						files.append("cod",cod);
						files.append("nom",nom);
						files.append("tip",tip);
						files.append("des",des);
						files.append("archivo",fot[0]);
						var atrib3=new FormData();atrib3.append('cod',$("#search_cod").val());atrib3.append('nom',$("#search_nom").val());atrib3.append('tip',$("#search_tipo").val());
						set('almacen/save_almacen',files,'NULL',true,'almacen/view_almacen',atrib3,'contenido',false,'1');
					}
				}else{
					alerta('Seleccione un tipo de Almacen','top','tip');
				}
			}else{
				alerta('Ingrese un nombre de almacen valido','top','nom');
			}
		}else{
			alerta('Ingrese un código valido','top','cod');
		}
		return false;
	 }
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	function imprimir_almacen(json){
   		modal("ALMACEN: Configuracion de impresion","lg","1");
 		btn_modal('',"",'imprimir',"'area'",'1','lg');
 		var atrib=new FormData();atrib.append('json',json);
 		atrib.append('ite',"ok");
		atrib.append('fot',"ok");
		atrib.append('cod',"ok");
		atrib.append('nom',"ok");
		atrib.append('tip',"ok");
		atrib.append('des',"ok");
 		get_2n('almacen/config_imprimir_almacen',atrib,'content_1',true,'almacen/imprimir_almacen',atrib,'area',true);
   	}
   	function arma_informe(json){
		var ite=""; if($("#1:checked").val()){ite='ok'}
		var fot=""; if($("#2:checked").val()){fot='ok'}
		var cod=""; if($("#3:checked").val()){cod='ok'}
		var nom=""; if($("#4:checked").val()){nom='ok'}
		var tip=""; if($("#5:checked").val()){tip='ok'}
		var des=""; if($("#6:checked").val()){des='ok'}
		var nro=$("#nro").val();
		var atrib=new FormData();
		atrib.append('json',json);
		atrib.append('ite',ite);
		atrib.append('fot',fot);
		atrib.append('cod',cod);
		atrib.append('nom',nom);
		atrib.append('tip',tip);
		atrib.append('des',des);
		atrib.append('nro',nro);
		get('almacen/imprimir_almacen',atrib,'area',true);
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
  	function reportes_almacen(ida){
  		modal("ALMACEN: Detalle","sm","1");
 		btn_modal('',"",'',"",'1','sm');
 		var atrib=new FormData();atrib.append('id',ida);
 		get('almacen/reporte_almacen',atrib,'content_1',true);
  	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	function configuracion_almacen(ida){
	 	modal("ALMACEN: Modificar","sm","1");
	 	btn_modal('update_almacen',"'"+ida+"'",'',"",'1','sm');
	 	var atrib=new FormData();atrib.append('id',ida);
	 	get('almacen/form_update_almacen',atrib,'content_1',true);
	}
	function update_almacen(id){
	 	var fot=$("#archivo").prop('files');
	 	var cod=$("#cod").val();
	 	var nom=$("#nom").val();
		var des=$("#des").val();
		if(strSpace(cod,2,10)){
			if(strSpace(nom,3,100)){
					var control=true;
					if(des!=""){ if(!textarea(des,0,400)){ control=false; alerta('Ingrese un contenido válido','top','des');}}
					if(control){
						var atrib= new FormData();
						atrib.append("id",id);
						atrib.append("cod",cod);
						atrib.append("nom",nom);
						atrib.append("des",des);
						atrib.append("archivo",fot[0]);
						var atrib3=new FormData();atrib3.append('cod',$("#search_cod").val());atrib3.append('nom',$("#search_nom").val());atrib3.append('tip',$("#search_tipo").val());
						set('almacen/update_almacen',atrib,'NULL',true,'almacen/view_almacen',atrib3,'contenido',false,'1');
					}
			}else{
				alerta('Ingrese un nombre de almacen valido','top','nom');
			}
		}else{
			alerta('Ingrese un código valido','top','cod');
		}
		return false;
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	function confirmar(ida){
   		modal("ALMACEN: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','drop_almacen',"'"+ida+"'");
   		var atrib=new FormData();atrib.append('ida',ida);
   		get('almacen/confirmar',atrib,'content_5',true);
   	}
   	function drop_almacen(id){
		var atrib=new FormData();
		atrib.append('u',$("#e_user").val());
		atrib.append('p',$("#e_password").val());
		atrib.append('id',id);
		set('almacen/drop_almacen',atrib,'NULL',true,'almacen/view_almacen',{},'contenido',false,'5');
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE ALMACENES -------*/
/*------- MANEJO DE MOVIMIENTO DE MATERIALES -------*/
   	/*--- Buscador ---*/
   	function view_movimiento_material(){
		var nom=$("#s_nom").val();
		var alm=$("#s_alm").val();
		var usu=$("#s_usu").val();
		var sol=$("#s_sol").val();
		var fech_a=$("#fech_a").val();
		var fech_b=$("#fech_b").val();
		var tip=$("#s_tip").val();
		var atrib=new FormData();
		atrib.append('nom',nom);
		atrib.append('alm',alm);
		atrib.append('usu',usu);
		atrib.append('sol',sol);
		atrib.append('fech_a',fech_a);
		atrib.append('fech_b',fech_b);
		atrib.append('tip',tip);
		get("almacen/view_movimiento_material",atrib,'contenido',true);
		blur_all();
		return false;
	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	function all_movimiento_material(){
   		reset_all_view("");
		$("#hra").val("");$("#fech_a").val("");$("#fech_b").val("");
		view_movimiento_material();
	}
   	/*--- End Ver Todo ---*/
   	/*--- Imprimir ---*/
   	function imprimir_almacen_hm(json){
   		modal("MOVIMIENTO DE MATERIALES: Configuracion de impresion","lg","1");
 		btn_modal('',"",'imprimir',"'area'",'1',"lg");
 		var atrib=new FormData();atrib.append('json',json);
		atrib.append('v1',"ok");
		atrib.append('v2',"ok");
		atrib.append('v3',"ok");
		atrib.append('v4',"ok");
		atrib.append('v5',"ok");
		atrib.append('v6',"ok");
		atrib.append('v7',"ok");
		atrib.append('v8',"ok");
		atrib.append('v9',"ok");
		atrib.append('v10',"ok");
		atrib.append('v11',"ok");
		atrib.append('v12',"ok");
 		get_2n('almacen/config_imprimir_almacen_hm',atrib,'content_1',false,'almacen/imprimir_almacen_hm',atrib,'area',true);
   	}
   	function arma_informe_hm(json){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var v6=""; if($("#6:checked").val()){v6='ok'}
		var v7=""; if($("#7:checked").val()){v7='ok'}
		var v8=""; if($("#8:checked").val()){v8='ok'}
		var v9=""; if($("#9:checked").val()){v9='ok'}
		var v10=""; if($("#10:checked").val()){v10='ok'}
		var v11=""; if($("#11:checked").val()){v11='ok'}
		var v12=""; if($("#12:checked").val()){v12='ok'}
		var nro=$("#nro").val();
		var atrib=new FormData();
		atrib.append('json',json);
		atrib.append('v1',v1);
		atrib.append('v2',v2);
		atrib.append('v3',v3);
		atrib.append('v4',v4);
		atrib.append('v5',v5);
		atrib.append('v6',v6);
		atrib.append('v7',v7);
		atrib.append('v8',v8);
		atrib.append('v9',v9);
		atrib.append('v10',v10);
		atrib.append('v11',v11);
		atrib.append('v12',v12);
		atrib.append('nro',nro);
		get('almacen/imprimir_almacen_hm',atrib,'area',true);
	}
   	/*--- End Imprimir ---*/
   	/*--- Eliminar ---*/
   	function confirmar_hitorial_material(){
   		modal("MOVIMIENTO DE MATERIALES: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','borrar_historial_material',"");
   		get('almacen/confirmar_hitorial_material',{},'content_5',true);
   	}
   	function borrar_historial_material(){
   		var atrib=new FormData();
   		atrib.append('u',$("#e_user").val());
		atrib.append('p',$("#e_password").val());
   		set('almacen/borrar_historial_material',atrib,'NULL',true,'almacen/view_movimiento_material',{},'contenido',true,'5');
   	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE MOVIMIENTO DE MATERIALES -------*/
/*------- MANEJO DE MOVIMIENTO DE PRODUCTOS -------*/
   	/*--- Buscador ---*/
   	function view_movimiento_producto(){
		var cod=$("#s_cod").val();
		var nom=$("#s_nom").val();
		var alm=$("#s_alm").val();
		var usu=$("#s_usu").val();
		var hra=$("#hra").val();
		var fech_a=$("#fech_a").val();
		var fech_b=$("#fech_b").val();
		var atrib=new FormData();
		atrib.append('cod',cod);
		atrib.append('nom',nom);
		atrib.append('alm',alm);
		atrib.append('usu',usu);
		atrib.append('hra',hra);
		atrib.append('fech_a',fech_a);
		atrib.append('fech_b',fech_b);
		get("almacen/view_movimiento_producto",atrib,'contenido',true);
		blur_all();
		return false;
	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	function all_movimiento_producto(){
   		reset_all_view("");
		$("#hra").val("");$("#fech_a").val("");$("#fech_b").val("");
		view_movimiento_producto();
	}
   	/*--- End Ver Todo ---*/
   	/*--- Imprimir ---*/
   	function imprimir_almacen_hp(json){
   		modal("MOVIMIENTO DE PRODUCTOS: Configuracion de impresion","lg","1");
 		btn_modal('',"",'imprimir',"'area'","1","lg");
 		var atrib=new FormData();atrib.append('json',json);
		atrib.append('v1',"ok");
		atrib.append('v2',"ok");
		atrib.append('v3',"ok");
		atrib.append('v4',"ok");
		atrib.append('v5',"ok");
		atrib.append('v6',"ok");
		atrib.append('v7',"ok");
		atrib.append('v8',"ok");
		atrib.append('v9',"ok");
		atrib.append('v10',"ok");
		atrib.append('v11',"ok");
 		get_2n('almacen/config_imprimir_almacen_hp',atrib,'content_1',false,'almacen/imprimir_almacen_hp',atrib,'area',true);
   	}
   	function arma_informe_hp(json){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var v6=""; if($("#6:checked").val()){v6='ok'}
		var v7=""; if($("#7:checked").val()){v7='ok'}
		var v8=""; if($("#8:checked").val()){v8='ok'}
		var v9=""; if($("#9:checked").val()){v9='ok'}
		var v10=""; if($("#10:checked").val()){v10='ok'}
		var v11=""; if($("#11:checked").val()){v11='ok'}
		var nro=$("#nro").val();
		var atrib=new FormData();
		atrib.append('json',json);
		atrib.append('v1',v1);
		atrib.append('v2',v2);
		atrib.append('v3',v3);
		atrib.append('v4',v4);
		atrib.append('v5',v5);
		atrib.append('v6',v6);
		atrib.append('v7',v7);
		atrib.append('v8',v8);
		atrib.append('v9',v9);
		atrib.append('v10',v10);
		atrib.append('v11',v11);
		atrib.append('nro',nro);
		get('almacen/imprimir_almacen_hp',atrib,'area',true);
	}
   	/*--- End Imprimir ---*/
   	/*--- Eliminar ---*/
   	function confirmar_historial_producto(){
   		modal("MOVIMIENTO DE PRODUCTOS: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','borrar_historial_producto',"");
   		get('almacen/confirmar_hitorial_producto',{},'content_5',true);
   	}
   	function borrar_historial_producto(){
   		var atrib=new FormData();
   		atrib.append('u',$("#e_user").val());
		atrib.append('p',$("#e_password").val());
   		set('almacen/borrar_historial_producto',atrib,'NULL',true,'almacen/view_movimiento_producto',{},'contenido',true,'5');
   	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE MOVIMIENTO DE PRODUCTOS -------*/