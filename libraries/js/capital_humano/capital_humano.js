var x=$(document);
x.ready(inicio);
function inicio(){
  $('#empleado').click(function(){get_2n('capital_humano/search_empleado',{},'search',false,'capital_humano/view_empleado',{},'contenido',true);activar("empleado","Empleados","capital_humano?p=1");});
  $('#planilla').click(function(){get_2n('capital_humano/search_planilla',{},'search',false,'capital_humano/view_planilla',{},'contenido',true);activar("planilla","Planilla de sueldos","capital_humano?p=2");});
  $('#config').click(function(){get_2n('',{},'search',false,'capital_humano/view_config',{},'contenido',true);activar("config","Configuración capital humano","capital_humano?p=5");});
}
/*------- MANEJO DE CAPITAL HUMANO -------*/
   	/*--- Buscador ---*/
   	function reset_input(id){
		if(id!="s_cod"){ $("#s_cod").val(""); }
		if(id!="s_nom"){ $("#s_nom").val(""); }
		if(id!="s_ci"){ $("#s_ci").val(""); }
		if(id!="s_con"){ $("#s_con").val(""); }
		if(id!="s_sal"){ $("#s_sal").val(""); }
		if(id!="s_tip"){ $("#s_tip").val(""); }
	}
	function reset_input_3(id){
		if(id!="cod_3"){ $("#cod_3").val("");}
		if(id!="nom_3"){ $("#nom_3").val("");}
	}
	function blur_all(){
		OnBlur("s_cod");
		OnBlur("s_nom");
		OnBlur("s_ci");
		OnBlur("s_con");
		OnBlur("s_sal");
		OnBlur("s_tip");
	}
   	function view_empleados(){
		var atrib3=new FormData();
		atrib3.append('cod',$("#s_cod").val());
		atrib3.append('nom',$("#s_nom").val());
		atrib3.append('ci',$("#s_ci").val());
		atrib3.append('con',$("#s_con").val());
		atrib3.append('sal',$("#s_sal").val());
		atrib3.append('tip',$("#s_tip").val());
		get("capital_humano/view_empleado",atrib3,'contenido',true);
		blur_all();
		return false;
	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
	function all_empleado(){
		reset_input("");
		view_empleados();
	}
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
   	function new_empleado(){
 		modal("CAPITAL HUMANO: Nuevo","md","1");
 		btn_modal('save_empleado',"",'',"",'1','md');
 		var atrib=new FormData();
 		get('capital_humano/new_empleado',atrib,'content_1',true);
	}
	function search_ci(){
   		var ci=$("#n_ci").val();
   		if(ci!=""){
	   		var inputs="n_nom1|n_tel|n_ema|n_ciu|n_dir|n_obs";
	   		var atrib=new FormData();
	   		atrib.append('ci',ci);
	   		get_input('capital_humano/search_ci',atrib,"NULL",inputs,false);
   		}
   	}
	function save_empleado(){
		var fot=$('#n_fot').prop('files');
		var ci=$('#n_ci').val();
		var ciudad=$('#n_ciu').val();
		var nom=$('#n_nom1').val();
		var nom2=$('#n_nom2').val();
		var pat=$('#n_pat').val();
		var mat=$('#n_mat').val();
		var cod=$('#n_cod').val();
		var te=$('#n_tem').val();
		var car=$('#n_car').val();
		var form=$('#n_ga').val();
		var tc=$('#n_tc').val();
		var sal=$('#n_sal').val();
		var telf=$('#n_tel').val();
		var ema=$('#n_ema').val();
		var fn=$('#n_fn').val();
		var fi=$('#n_fi').val();
		var dir=$('#n_dir').val();
		var obs=$('#n_obs').val();
		if(entero(ci,6,8) && ci>=100000 && ci<=99999999){
			if(entero(ciudad,0,10)){
				if(strSpace(nom,2,20)){
					if(strSpace(pat,2,20)){
						if(entero(te,0,1) && te>=0 && te<=1){
							if(entero(tc,0,10)){
								if(decimal(sal,4,1) && sal>=0 && sal<=9999.9){
									var control=true;
									if(nom2!=""){ if(!strSpace(nom2,2,20)){ control=false; alerta('Ingrese un nombre válido',"top",'n_nom2');}}
									if(mat!=""){ if(!strSpace(mat,2,20)){ control=false; alerta('Ingrese un apellido válido',"top",'n_mat');}}
									if(cod!=""){ if(!entero(cod,0,10) || cod<0 || cod>9999999999){ control=false; alerta('Ingrese un nombre válido',"top",'n_cod');}}
									if(car!=""){ if(!strSpace(car,0,100)){ control=false; alerta('Ingrese un contenido válido',"top",'n_car');}}
									if(form!=""){ if(!strSpace(form,0,100)){ control=false; alerta('Ingrese un contenido válido',"top",'n_ga');}}
									if(telf!=""){ if(!entero(telf,6,15) || telf<100000 || telf>999999999999999){ control=false; alerta('Ingrese un contenido válido',"top",'n_tel');}}
									if(ema!=""){ if(!email(ema)){ control=false; alerta('Ingrese un contenido válido',"top",'n_ema');}}
									if(fn!=""){ if(!fecha(fn)){ control=false; alerta('Ingrese una fecha válida',"top",'n_fn');}}
									if(fi!=""){ if(!fecha(fi)){ control=false; alerta('Ingrese una fecha válida',"top",'n_fi');}}
									if(dir!=""){ if(!direccion(dir,0,200)){ control=false; alerta('Ingrese un contenido válido',"top",'n_dir');}}
									if(obs!=""){ if(!textarea(obs,0,900)){ control=false; alerta('Ingrese un contenido válido',"top",'n_obs');}}
									if(control){
										var atribs= new FormData();
										atribs.append("ci",ci);
										atribs.append("ciu",ciudad);
										atribs.append("nom1",nom);
										atribs.append("nom2",nom2);
										atribs.append("pat",pat);
										atribs.append("mat",mat);
										atribs.append("cod",cod);
										atribs.append("te",te);
										atribs.append("car",car);
										atribs.append("ga",form);
										atribs.append("tc",tc);
										atribs.append("sal",sal);
										atribs.append("tel",telf);
										atribs.append("ema",ema);
										atribs.append("nac",fn);
										atribs.append("ing",fi);
										atribs.append("dir",dir);
										atribs.append("obs",obs);
										atribs.append("archivo",fot[0]);
										var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('ci',$("#s_ci").val());atrib3.append('con',$("#s_con").val());atrib3.append('sal',$("#s_sal").val());atrib3.append('tip',$("#s_tip").val());
										set('capital_humano/save_empleado',atribs,'NULL',true,'capital_humano/view_empleado',atrib3,'contenido',false,'1');
									}
								}else{
									alerta('Ingrese salario válido del empleado',"top",'n_sal');
								}
							}else{
								alerta('Seleccione un tipo de contrato',"top",'n_tc');
							}
						}else{
							alerta('Seleccione un tipo de empleado','top','n_tem');
						}
					}else{
						alerta('Ingrese Apellido Paterno',"top",'n_pat');
					}
				}else{
					alerta('El campo de primer nombre es obligatorio',"top",'n_nom1');
				}
			}else{
				alerta('Seleccione una ciudad','top','n_ciu');
			}
		}else{
			alerta('Ingrese un nro de Carnet Válido',"top",'n_ci');
		}
		return false;
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	function imprimir_empleados(json){
   		modal("EMPLEADOS: Configuracion de impresion","xlg","1");
 		btn_modal('',"",'imprimir',"'area'",'1','xlg');
 		var atrib=new FormData();atrib.append('json',json);
 		get_2n('capital_humano/imprimir_empleados',atrib,'content_1',true,'capital_humano/arma_empleados',atrib,'area',true);
   	}
   	function arma_empleados(json){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var v6=""; if($("#6:checked").val()){v6='ok'}
		var v7=""; if($("#7:checked").val()){v7='ok'}
		var v8=""; if($("#8:checked").val()){v8='ok'}
		var v9=""; if($("#9:checked").val()){v9='ok'}
		var v10=""; if($("#10:checked").val()){v10='ok'}
		var v11=""; if($("#11:checked").val()){v11='ok'}
		var v12=""; if($("#12:checked").val()){v12='ok'}
		var v13=""; if($("#13:checked").val()){v13='ok';}
		var nro=$("#nro").val();
		var atrib=new FormData();
		atrib.append('json',json);
		atrib.append('v1',v1);
		atrib.append('v2',v2);
		atrib.append('v3',v3);
		atrib.append('v4',v4);
		atrib.append('v5',v5);
		atrib.append('v6',v6);
		atrib.append('v7',v7);
		atrib.append('v8',v8);
		atrib.append('v9',v9);
		atrib.append('v10',v10);
		atrib.append('v11',v11);
		atrib.append('v12',v12);
		atrib.append('v13',v13);
		atrib.append('nro',nro);
		get('capital_humano/arma_empleados',atrib,'area',true);
	}
   	/*--- End Imprimir ---*/
   	/*--- Estado ----*/
   	/*--- End Estado ----*/
   	function cambiar_estado(ide){
 		var atrib=new FormData();atrib.append('ide',ide);
 		var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('ci',$("#s_ci").val());atrib3.append('con',$("#s_con").val());atrib3.append('sal',$("#s_sal").val());atrib3.append('tip',$("#s_tip").val());
 		set('capital_humano/cambiar_estado',atrib,'NULL',true,'capital_humano/view_empleado',atrib3,'contenido',false);
	}
   	/*--- Reportes ---*/
   	function detalle_empleado(ide){
   		modal("EMPLEADO: Detalle","md","1");
 		btn_modal('',"",'imprimir',"'area'",'1','md');
 		var atrib=new FormData();atrib.append('ide',ide);
 		get('capital_humano/detalle_empleado',atrib,'content_1',true);
	}
	function ficha_control(ide,control){
   		modal("EMPLEADO: Ficha de control","md","1");
 		btn_modal('',"",'imprimir',"'area'",'1','md');
 		var atrib=new FormData();atrib.append('ide',ide);
 		if(control){
 			atrib.append('max',$("#max").val());
 			atrib.append('nro',$("#nro").val());
 			atrib.append('ges',$("#ges").val());
 		}
 		get('capital_humano/ficha_control',atrib,'content_1',true);
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/

   	function change_empleado(ide){
 		modal("EMPLEADOS: Modificar","md","1");
 		btn_modal('update_empleado',"'"+ide+"'",'',"",'1','md');
 		var atrib=new FormData();
 		atrib.append('ide',ide);
 		get('capital_humano/change_empleado',atrib,'content_1',true);
	}
	function update_empleado(ide){
		var fot=$('#n_fot').prop('files');
		var ci=$('#n_ci').val();
		var ciudad=$('#n_ciu').val();
		var nom=$('#n_nom1').val();
		var nom2=$('#n_nom2').val();
		var pat=$('#n_pat').val();
		var mat=$('#n_mat').val();
		var cod=$('#n_cod').val();
		var te=$('#n_tem').val();
		var car=$('#n_car').val();
		var form=$('#n_ga').val();
		var tc=$('#n_tc').val();
		var sal=$('#n_sal').val();
		var telf=$('#n_tel').val();
		var ema=$('#n_ema').val();
		var fn=$('#n_fn').val();
		var fi=$('#n_fi').val();
		var dir=$('#n_dir').val();
		var obs=$('#n_obs').val();
		if(entero(ci,6,8) && ci>=100000 && ci<=99999999){
			if(entero(ciudad,0,10)){
				if(strSpace(nom,2,20)){
					if(strSpace(pat,2,20)){
						if(entero(te,0,1) && te>=0 && te<=1){
							if(entero(tc,0,10)){
								if(decimal(sal,4,1) && sal>=0 && sal<=9999.9){
									var control=true;
									if(nom2!=""){ if(!strSpace(nom2,2,20)){ control=false; alerta('Ingrese un nombre válido',"top",'n_nom2');}}
									if(mat!=""){ if(!strSpace(mat,2,20)){ control=false; alerta('Ingrese un apellido válido',"top",'n_mat');}}
									if(cod!=""){ if(!entero(cod,0,10) || cod<0 || cod>9999999999){ control=false; alerta('Ingrese un nombre válido',"top",'n_cod');}}
									if(car!=""){ if(!strSpace(car,0,100)){ control=false; alerta('Ingrese un contenido válido',"top",'n_car');}}
									if(form!=""){ if(!strSpace(form,0,100)){ control=false; alerta('Ingrese un contenido válido',"top",'n_ga');}}
									if(telf!=""){ if(!entero(telf,6,15) || telf<100000 || telf>999999999999999){ control=false; alerta('Ingrese un contenido válido',"top",'n_tel');}}
									if(ema!=""){ if(!email(ema)){ control=false; alerta('Ingrese un contenido válido',"top",'n_ema');}}
									if(fn!=""){ if(!fecha(fn)){ control=false; alerta('Ingrese una fecha válida',"top",'n_fn');}}
									if(fi!=""){ if(!fecha(fi)){ control=false; alerta('Ingrese una fecha válida',"top",'n_fi');}}
									if(dir!=""){ if(!direccion(dir,0,200)){ control=false; alerta('Ingrese un contenido válido',"top",'n_dir');}}
									if(obs!=""){ if(!textarea(obs,0,900)){ control=false; alerta('Ingrese un contenido válido',"top",'n_obs');}}
									if(control){
										var atribs= new FormData();
										atribs.append("ide",ide);
										atribs.append("ci",ci);
										atribs.append("ciu",ciudad);
										atribs.append("nom1",nom);
										atribs.append("nom2",nom2);
										atribs.append("pat",pat);
										atribs.append("mat",mat);
										atribs.append("cod",cod);
										atribs.append("te",te);
										atribs.append("car",car);
										atribs.append("ga",form);
										atribs.append("tc",tc);
										atribs.append("sal",sal);
										atribs.append("tel",telf);
										atribs.append("ema",ema);
										atribs.append("nac",fn);
										atribs.append("ing",fi);
										atribs.append("dir",dir);
										atribs.append("obs",obs);
										atribs.append("archivo",fot[0]);
										var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('ci',$("#s_ci").val());atrib3.append('con',$("#s_con").val());atrib3.append('sal',$("#s_sal").val());atrib3.append('tip',$("#s_tip").val());
										set('capital_humano/update_empleado',atribs,'NULL',true,'capital_humano/view_empleado',atrib3,'contenido',false,'1');
									}
								}else{
									alerta('Ingrese salario válido del empleado',"top",'n_sal');
								}
							}else{
								alerta('Seleccione un tipo de contrato',"top",'n_tc');
							}
						}else{
							alerta('Seleccione un tipo de empleado','top','n_tem');
						}
					}else{
						alerta('Ingrese Apellido Paterno',"top",'n_pat');
					}
				}else{
					alerta('El campo de primer nombre es obligatorio',"top",'n_nom1');
				}
			}else{
				alerta('Seleccione una ciudad','top','n_ciu');
			}
		}else{
			alerta('Ingrese un nro de Carnet Válido',"top",'n_ci');
		}
		return false;
	}
	function config_empleado(ide){
 		modal("EMPLEADOS: Configuración","md","1");
 		btn_modal('',"",'',"",'1','md');
 		var atrib=new FormData();
 		atrib.append('ide',ide);
 		get('capital_humano/config_empleado',atrib,'content_1',true);
	}
	/*--- Ayudantes ---*/
		function view_ayudantes(ide){
			modal("EMPLEADOS: Ayudantes","sm","2");
	 		btn_modal('',"",'',"",'2','sm');
	 		var atrib=new FormData();
	 		atrib.append('ide',ide);
	 		get_2n('capital_humano/search_ayudantes',atrib,'content_2',true,'capital_humano/view_ayudantes',atrib,'view_ayudantes',true);
		}
		function search_ayudantes(ide){
			var ci=$("#cod_3").val();
			var nom=$("#nom_3").val();
			var atrib=new FormData();
			atrib.append('ide',ide);
			atrib.append('ci',ci);
			atrib.append('nom',nom);
			get('capital_humano/view_ayudantes',atrib,'view_ayudantes',true);
			return false;
		}
		function all_ayudantes(ide){
			reset_input_3("");
			search_ayudantes(ide)
		}
		function select_ayudante(maestro,ayudante,ele,color,diametro){
			var atrib3=new FormData();atrib3.append('ide',maestro);atrib3.append('anio',$("#anio").val());
			set_button('capital_humano/select_ayudante',{maestro,ayudante},"capital_humano/config_empleado",atrib3,"content_1",false,ele,color,diametro);
		}
		function alerta_ayudante(ide,iday,nombre,img) {
			modal("EMPLEADOS: Eliminar ayudante","xs","5");
		   	addClick_v2('modal_ok_5','drop_ayudante',"'"+ide+"','"+iday+"'");
		   	var atrib=new FormData();atrib.append('iday',iday);
		   	atrib.append('titulo','eliminar definitivamente el ayudante: <b>'+nombre+'</b> como ayudante el empleado');
		   	atrib.append('img',img);
		   	get('insumo/alerta',atrib,'content_5',true);
		}
		function drop_ayudante(ide,iday){
			var atrib=new FormData();atrib.append('iday',iday);
			var atrib3=new FormData();atrib3.append('ide',ide);atrib3.append('anio',$("#anio").val());
			set('capital_humano/drop_ayudante',atrib,'NULL',true,'capital_humano/config_empleado',atrib3,'content_1',false,'5');
		}
	/*--- End ayudantes ---*/
	/*--- Procesos ---*/
		function save_proceso(ide){
			var pro=$("#pro_e").val();
			var tip=$("#tip_e").val();
			if(entero(pro,0,10)){
				if(entero(tip,0,1) && tip>=0 && tip<=1){
					var atrib=new FormData();atrib.append('ide',ide);atrib.append('idpr',pro);atrib.append('tip',tip);
					var atrib2=new FormData();atrib2.append('ide',ide);atrib2.append('anio',$("#anio").val());
					var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('ci',$("#s_ci").val());atrib3.append('con',$("#s_con").val());atrib3.append('sal',$("#s_sal").val());atrib3.append('tip',$("#s_tip").val());
					set_2n('capital_humano/save_proceso',atrib,'NULL',true,'capital_humano/config_empleado',atrib2,'content_1',false,'capital_humano/view_empleado',atrib3,'contenido',false);
				}else{
					alerta('Seleccione un tipo válido','top','tip_e');
				}
			}else{
				alerta('Seleccione un proceso','top','pro_e');
			}
		}
		function update_proceso(ide,idep){
			var pro=$("#pro_e"+idep).val();
			var tip=$("#tip_e"+idep).val();
			if(entero(pro,0,10)){
				if(entero(tip,0,1) && tip>=0 && tip<=1){
					var atrib=new FormData();atrib.append('idep',idep);atrib.append('idpr',pro);atrib.append('tip',tip);
					var atrib2=new FormData();atrib2.append('ide',ide);atrib2.append('anio',$("#anio").val());
					var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('ci',$("#s_ci").val());atrib3.append('con',$("#s_con").val());atrib3.append('sal',$("#s_sal").val());atrib3.append('tip',$("#s_tip").val());
					set_2n('capital_humano/update_proceso',atrib,'NULL',true,'capital_humano/config_empleado',atrib2,'content_1',false,'capital_humano/view_empleado',atrib3,'contenido',false);
				}else{
					alerta('Seleccione un tipo válido','top','tip_e'+idep);
				}
			}else{
				alerta('Seleccione un proceso','top','pro_e'+idep);
			}	
		}
		function alerta_proceso(ide,idep,nombre) {
			modal("EMPLEADOS: Eliminar proceso","xs","5");
		   	addClick_v2('modal_ok_5','drop_proceso',"'"+ide+"','"+idep+"'");
		   	var atrib=new FormData();atrib.append('idep',idep);
		   	atrib.append('titulo','eliminar definitivamente el proceso: <b>'+nombre+'</b> como proceso que elabora el empleado');
		   	get('insumo/alerta',atrib,'content_5',true);
		}
		function drop_proceso(ide,idep){
			var atrib=new FormData();atrib.append('idep',idep);
			var atrib2=new FormData();atrib2.append('ide',ide);atrib2.append('anio',$("#anio").val());
			var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('ci',$("#s_ci").val());atrib3.append('con',$("#s_con").val());atrib3.append('sal',$("#s_sal").val());atrib3.append('tip',$("#s_tip").val());
			set_2n('capital_humano/drop_proceso',atrib,'NULL',true,'capital_humano/config_empleado',atrib2,'content_1',false,'capital_humano/view_empleado',atrib3,'contenido',false,'5');

		}
	/*--- End Procesos ---*/
	/*--- Anticipos ---*/
		function search_anticipo(ide){
			var anio=$("#anio").val();
			if(entero(anio,4,4) && anio>=1900 && anio<=3000){
				var atrib=new FormData();atrib.append('ide',ide);atrib.append('anio',anio);
				get('capital_humano/config_empleado',atrib,'content_1',false);
			}else{
				alerta('Ingrese un valor válido de 1900 a 3000','top','anio');
			}
			return false;
		}
		function save_anticipo(ide){
			var fec=$("#fec_e").val();
			var mon=$("#mon_e").val();
			var des=$("#des_e").val();
			if(fecha(fec)){
				if(decimal(mon,5,1) && mon>=0 && mon<=99999.9){
					if(textarea(des,0,300)){
						var atrib=new FormData();atrib.append('ide',ide);atrib.append('fec',fec);atrib.append('mon',mon);atrib.append('des',des);
						var atrib2=new FormData();atrib2.append('ide',ide);atrib2.append('anio',$("#anio").val());
						set('capital_humano/save_anticipo',atrib,'NULL',true,'capital_humano/config_empleado',atrib2,'content_1',false);
					}else{
						alerta('Ingrese un contenido válido','top','des_e');						
					}
				}else{
					alerta('Seleccione un valor válido','top','mon_e');
				}
			}else{
				alerta('Ingrese una fecha válida','top','fec_e');
			}
			return false;
		}
		function update_anticipo(ide,idan){
			var fec=$("#fec_e"+idan).val();
			var mon=$("#mon_e"+idan).val();
			var des=$("#des_e"+idan).val();
			if(fecha(fec)){
				if(decimal(mon,5,1) && mon>=0 && mon<=99999.9){
					if(textarea(des,0,300)){
						var atrib=new FormData();atrib.append('idan',idan);atrib.append('fec',fec);atrib.append('mon',mon);atrib.append('des',des);
						var atrib2=new FormData();atrib2.append('ide',ide);atrib2.append('anio',$("#anio").val());
						set('capital_humano/update_anticipo',atrib,'NULL',true,'capital_humano/config_empleado',atrib2,'content_1',false);
					}else{
						alerta('Ingrese un contenido válido','top','des_e'+idan);						
					}
				}else{
					alerta('Seleccione un valor válido','top','mon_e'+idan);
				}
			}else{
				alerta('Ingrese una fecha válida','top','fec_e'+idan);
			}
			return false;
		}
		function alerta_anticipo(ide,idan) {
			modal("EMPLEADOS: Eliminar proceso","xs","5");
		   	addClick_v2('modal_ok_5','drop_anticipo',"'"+ide+"','"+idan+"'");
		   	var atrib=new FormData();atrib.append('idan',idan);
		   	atrib.append('titulo','eliminar definitivamente el anticipo');
		   	get('insumo/alerta',atrib,'content_5',true);
		}
		function drop_anticipo(ide,idan){
			var atrib=new FormData();atrib.append('idan',idan);
			var atrib2=new FormData();atrib2.append('ide',ide);atrib2.append('anio',$("#anio").val());
			var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('ci',$("#s_ci").val());atrib3.append('con',$("#s_con").val());atrib3.append('sal',$("#s_sal").val());atrib3.append('tip',$("#s_tip").val());
			set_2n('capital_humano/drop_anticipo',atrib,'NULL',true,'capital_humano/config_empleado',atrib2,'content_1',false,'capital_humano/view_empleado',atrib3,'contenido',false,'5');

		}
	/*--- End Anticipos ---*/
	/*--- confirmar contraseña ---*/
	function confirmar_password(ide){
   		modal("EMPLEADO: Restablecer contraseña","xs","5");
   		addClick_v2('modal_ok_5','change_password',"'"+ide+"'");
   		var atribs=new FormData();atribs.append('ide',ide);
   		get('capital_humano/confirmar_password',atribs,'content_5',true);
   	}
   	function change_password(ide){
		var ele=new FormData();
		ele.append('u',$("#e_user").val());
		ele.append('p',$("#e_password").val());
		ele.append('ide',ide);
		set('capital_humano/change_password',ele,'NULL',true,'',{},'',false,'5');
	}
	/*--- End confirmar contraseña ---*/
	/*--- Control de feriados ---*/
	function update_c_feriado(ide){
		var feriado=$("#c_fec").val();
   		var atrib=new FormData();atrib.append('ide',ide);atrib.append('feriado',feriado);
		set('capital_humano/update_c_feriado',atrib,'NULL',true,'',{},'',false);
   	}
	/*--- End Control de feriados ---*/
	/*--- Control de feriados ---*/
	function update_c_descuento(ide){
		var des=$("#c_dec").val();
   		var atrib=new FormData();atrib.append('ide',ide);atrib.append('des',des);
		set('capital_humano/update_c_descuento',atrib,'NULL',true,'',{},'',false);
   	}
	/*--- End Control de feriados ---*/
/*--- End configuracion ---*/
/*--- Eliminar ---*/
   	function confirmar_empleado(ide,cliente,img){
   		modal("EMPLEADO: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','drop_empleado',"'"+ide+"'");
   		var atribs=new FormData();atribs.append('ide',ide);
   		get('capital_humano/confirmar_empleado',atribs,'content_5',true);
   	}
   	function drop_empleado(ide){
		var ele=new FormData();
		ele.append('u',$("#e_user").val());
		ele.append('p',$("#e_password").val());
		ele.append('ide',ide);
		var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('ci',$("#s_ci").val());atrib3.append('con',$("#s_con").val());atrib3.append('sal',$("#s_sal").val());atrib3.append('tip',$("#s_tip").val());
		set('capital_humano/drop_empleado',ele,'NULL',true,'capital_humano/view_empleado',atrib3,'contenido',false,'5');
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE CAPITAL HUMANO -------*/
/*------- MANEJO DE PLANILLA -------*/
   	/*--- Buscador ---*/
   	function view_planilla(){
		var ci=$("#s_ci").val();
		var nom=$("#s_nom").val();
		var f1=$("#f1").val();
		var f2=$("#f2").val();
		var atrib=new FormData();
		atrib.append('ci',ci);
		atrib.append('nom',nom);
		atrib.append('f1',f1);
		atrib.append('f2',f2);
		get("capital_humano/view_planilla",atrib,'contenido',true);
		return false;	
	}
   	/*--- End Buscador ---*/
   	/*--- Nuevo ---*/
   	function new_biometrico(){
   		modal("PLANILLA: Importar Archivo Biometrico","xs","1");
   		btn_modal('save_biometrico',"",'',"",'1','xs');
 		get("capital_humano/new_biometrico",{},'content_1');
   	}
   	function save_biometrico(){
   		var archivo=$("#archivo").prop('files');
   		var tipo=$("#tipo").val();
   		if(archivo.length==1){
   			if(entero(tipo,1,1) && tipo>=0 && tipo<=1){
		   		var atrib=new FormData();
		   		atrib.append('archivo',archivo[0]);
		   		atrib.append('tipo',tipo);
				var atrib2=new FormData();atrib2.append('f1',$("#f1").val());atrib2.append('f2',$("#f2").val());
				set('capital_humano/save_biometrico',atrib,'NULL',true,'capital_humano/view_planilla',atrib2,'contenido',false,"1");
   			}else{
   				alerta("Seleccione un tipo de busqueda válido","top","tipo");
   			}
	   	}else{
	   		alerta("Seleccione un archivo de formato .xls 2003","top","archivo");
	   	}
   	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	function imprimir_planilla(json,f1,f2){
   		modal("PLANILLA: Configuracion de impresion","xlg","1");
 		btn_modal('',"",'imprimir',"'area'",'1','xlg');
 		var atrib=new FormData();atrib.append('json',json);atrib.append('f1',f1);atrib.append('f2',f2);
 		get_2n('capital_humano/imprimir_planilla',atrib,'content_1',true,'capital_humano/arma_planilla',atrib,'area',true);
   	}
   	function arma_planilla(json,f1,f2){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var v6=""; if($("#6:checked").val()){v6='ok'}
		var v7=""; if($("#7:checked").val()){v7='ok'}
		var v8=""; if($("#8:checked").val()){v8='ok'}
		var v9=""; if($("#9:checked").val()){v9='ok'}
		var v10=""; if($("#10:checked").val()){v10='ok'}
		var v11=""; if($("#11:checked").val()){v11='ok'}
		var nro=$("#nro").val();
		var atrib=new FormData();
		atrib.append('json',json);
		atrib.append('v1',v1);
		atrib.append('v2',v2);
		atrib.append('v3',v3);
		atrib.append('v4',v4);
		atrib.append('v5',v5);
		atrib.append('v6',v6);
		atrib.append('v7',v7);
		atrib.append('v8',v8);
		atrib.append('v9',v9);
		atrib.append('v10',v10);
		atrib.append('v11',v11);
		atrib.append('nro',nro);
		atrib.append('f1',f1);
		atrib.append('f2',f2);
		get('capital_humano/arma_planilla',atrib,'area',true);
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	function reporte_planilla(ide,f1,f2){
   		modal("PLANILLA: Detalle","md","1");
   		btn_modal('',"",'imprimir',"'area'",'1','md');
   		var atrib=new FormData();atrib.append('ide',ide);atrib.append('f1',f1);atrib.append('f2',f2);
 		get("capital_humano/reporte_planilla",atrib,'content_1',true);
   	}
   	function nro_planilla(ide,f1,f2){
   		modal("PLANILLA: Detalle","md","1");
   		btn_modal('',"",'imprimir',"'area'",'1','md');
   		var nro=$("#nro").val();
   		if(entero(nro,0,2) && nro>=0){
	   		var atrib=new FormData();
	   		atrib.append('ide',ide); atrib.append('f1',f1); atrib.append('f2',f2); atrib.append("nro",nro);
	 		get("capital_humano/reporte_planilla",atrib,'content_1');
   		}else{
   			alerta("Seleccione un archivo de formato .xls 2003","top","archivo");
   		}
   	}
   	function pae(ide,f1,f2){
   		modal("PLANILLA: PAE","lg","1");
   		btn_modal('',"",'imprimir',"'area'",'1','lg');
   		var atrib=new FormData();atrib.append('ide',ide);atrib.append('f1',f1);atrib.append('f2',f2);
 		get("capital_humano/pae",atrib,'content_1',true);
   	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	function detalle_anticipos(ide,f1,f2){
   		if(entero(ide,0,10) && fecha(f1) && fecha(f2)){
   			modal("PLANILLA: Detalle de anticipos","sm","1");
	   		btn_modal('',"",'',"",'1','sm');
	   		var atrib=new FormData();atrib.append('ide',ide);atrib.append('f1',f1);atrib.append('f2',f2);
	 		get("capital_humano/detalle_anticipos",atrib,'content_1',true);
   		}else{
   			msj("fail");
   		}
   	}
		function save_planilla_anticipo(ide,f1,f2){
			var fec=$("#fec_e").val();
			var mon=$("#mon_e").val();
			var des=$("#des_e").val();
			if(fecha(f1) && fecha(f2)){
				if(fecha(fec)){
					if(decimal(mon,5,1) && mon>0 && mon<=99999.9){
						if(textarea(des,0,300)){
							var atrib=new FormData();atrib.append('ide',ide);atrib.append('fec',fec);atrib.append('mon',mon);atrib.append('des',des);
							var atrib2=new FormData();atrib2.append('ide',ide);atrib2.append('f1',f1);atrib2.append('f2',f2);
							var atrib3=new FormData();atrib3.append('ci',$("#s_ci").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('f1',$("#f1").val());atrib3.append('f2',$("#f2").val());
						 	set_2n('capital_humano/save_anticipo',atrib,'NULL',true,'capital_humano/detalle_anticipos',atrib2,'content_1',false,'capital_humano/view_planilla',atrib3,'contenido',false);
						}else{
							alerta('Ingrese un contenido válido','top','des_e');						
						}
					}else{
						alerta('Ingrese un valor válido','top','mon_e');
					}
				}else{
					alerta('Ingrese una fecha válida','top','fec_e');
				}
			}else{
				msj("Error en lectura de datos.");
			}
			return false;
		}
		function update_planilla_anticipo(ide,idan,f1,f2){
			var fec=$("#fec_e"+idan).val();
			var mon=$("#mon_e"+idan).val();
			var des=$("#des_e"+idan).val();
			if(fecha(f1) && fecha(f2)){
				if(fecha(fec)){
					if(decimal(mon,5,1) && mon>0 && mon<=99999.9){
						if(textarea(des,0,300)){
							var atrib=new FormData();atrib.append('idan',idan);atrib.append('fec',fec);atrib.append('mon',mon);atrib.append('des',des);
							var atrib2=new FormData();atrib2.append('ide',ide);atrib2.append('f1',f1);atrib2.append('f2',f2);
							var atrib3=new FormData();atrib3.append('ci',$("#s_ci").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('f1',$("#f1").val());atrib3.append('f2',$("#f2").val());
						 	set_2n('capital_humano/update_anticipo',atrib,'NULL',true,'capital_humano/detalle_anticipos',atrib2,'content_1',false,'capital_humano/view_planilla',atrib3,'contenido',false);
						}else{
							alerta('Ingrese un contenido válido','top','des_e'+idan);						
						}
					}else{
						alerta('Ingrese un valor válido','top','mon_e'+idan);
					}
				}else{
					alerta('Ingrese una fecha válida','top','fec_e'+idan);
				}
			}else{
				msj("Error en lectura de datos.");
			}
			return false;
		}
		function alerta_planilla_anticipo(ide,idan,f1,f2){
			modal("EMPLEADOS: Eliminar proceso","xs","5");
		   	addClick_v2('modal_ok_5','drop_planilla_anticipo',"'"+ide+"','"+idan+"','"+f1+"','"+f2+"'");
		   	var atrib=new FormData();atrib.append('idan',idan);
		   	atrib.append('titulo','eliminar definitivamente el anticipo');
		   	get('insumo/alerta',atrib,'content_5',true);
		}
		function drop_planilla_anticipo(ide,idan,f1,f2){
			var atrib=new FormData();atrib.append('idan',idan);
			var atrib2=new FormData();atrib2.append('ide',ide);atrib2.append('f1',f1);atrib2.append('f2',f2);
			var atrib3=new FormData();atrib3.append('ci',$("#s_ci").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('f1',$("#f1").val());atrib3.append('f2',$("#f2").val());
			set_2n('capital_humano/drop_anticipo',atrib,'NULL',true,'capital_humano/detalle_anticipos',atrib2,'content_1',false,'capital_humano/view_planilla',atrib3,'contenido',false,'5');
		}
   	function config_planilla(ide,f1,f2){
   		modal("PLANILLA: Configuración","md","1");
   		btn_modal('',"",'',"",'1','md');
   		var atrib=new FormData();atrib.append('ide',ide);atrib.append('f1',f1);atrib.append('f2',f2);
 		get("capital_humano/config_planilla",atrib,"content_1");
   	}

   	function add_hora(ide,fecha,f1,f2){
   		modal("PLANILLA: Adicionar hora","xs","2");
   		btn_modal('save_hora',"'"+ide+"','"+fecha+"','"+f1+"','"+f2+"'",'',"",'2','xs');
   		var atrib=new FormData();atrib.append('fecha',fecha);
		get("capital_humano/add_hora",atrib,"content_2");
		//ajax_get('capital_humano/add_hora',atribs,'content_2');
   	}
   	function save_hora(ide,fecha,f1,f2){
   		var hora=$("#hora_new").val();
   		if(hora!=""){
   			var atrib=new FormData();atrib.append('fecha',fecha);atrib.append('hora',hora);atrib.append('ide',ide);atrib.append('f1',f1);atrib.append('f2',f2);
			var atrib3=new FormData();atrib3.append('ci',$("#s_ci").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('f1',$("#f1").val());atrib3.append('f2',$("#f2").val());
			set_2n('capital_humano/save_hora',atrib,'NULL',true,'capital_humano/config_planilla',atrib,'content_1',false,'capital_humano/view_planilla',atrib3,'contenido',false,"2");
   		}else{
   			alerta("Ingrese una hora valida","top","hora_new");
   		}
   	}
   	function alerta_hora(ide,idhb,f1,f2){
   		modal("PLANILLA: Eliminar hora","xs","5");
		addClick_v2('modal_ok_5','drop_hora',"'"+ide+"','"+idhb+"','"+f1+"','"+f2+"'");
		var atrib=new FormData();
		atrib.append('titulo','eliminar definitivamente la hora registrada');
		atrib.append('descripcion','<strong class="text-danger">Si eliminar la hora puede influir en el tiempo de trabajo y monto de sueldo a pagar</strong>');
		get('insumo/alerta',atrib,'content_5',true);
   	}
   	function drop_hora(ide,idhb,f1,f2){
		var atrib=new FormData();atrib.append('ide',ide);atrib.append('idhb',idhb);atrib.append('f1',f1);atrib.append('f2',f2);
		var atrib3=new FormData();atrib3.append('ci',$("#s_ci").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('f1',$("#f1").val());atrib3.append('f2',$("#f2").val());
		set_2n('capital_humano/drop_hora',atrib,'NULL',true,'capital_humano/config_planilla',atrib,'content_1',false,'capital_humano/view_planilla',atrib3,'contenido',false,"5");
	}
	/*--- Control de feriados ---*/
	function update_planilla_c_feriado(ide,f1,f2){
		var feriado=$("#c_fec").val();
   		var atrib=new FormData();atrib.append('ide',ide);atrib.append('feriado',feriado);
   		var atrib2=new FormData();atrib2.append('ide',ide);atrib2.append('f1',f1);atrib2.append('f2',f2);
   		var atrib3=new FormData();atrib3.append('ci',$("#s_ci").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('f1',$("#f1").val());atrib3.append('f2',$("#f2").val());
		set_2n('capital_humano/update_c_feriado',atrib,'NULL',true,'capital_humano/config_planilla',atrib2,'content_1',false,'capital_humano/view_planilla',atrib3,'contenido',false);
   	}
	/*--- End Control de feriados ---*/
	/*--- Control de feriados ---*/
	function update_planilla_c_descuento(ide,f1,f2){
		var des=$("#c_dec").val();
   		var atrib=new FormData();atrib.append('ide',ide);atrib.append('des',des);
   		var atrib2=new FormData();atrib2.append('ide',ide);atrib2.append('f1',f1);atrib2.append('f2',f2);
   		var atrib3=new FormData();atrib3.append('ci',$("#s_ci").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('f1',$("#f1").val());atrib3.append('f2',$("#f2").val());
		set_2n('capital_humano/update_c_descuento',atrib,'NULL',true,'capital_humano/config_planilla',atrib2,'content_1',false,'capital_humano/view_planilla',atrib3,'contenido',false);
   	}
	/*--- End Control de feriados ---*/

   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE PLANILLA -------*/
/*------- MANEJO DE CONFIGURACION -------*/
   	/*--- Paises ---*/
   	function save_pais(){
   		var p=$("#p").val();
   		if(strSpace(p,2,100)){
   			var atrib=new FormData();
   			atrib.append('p',p);
   			set('capital_humano/save_pais',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false);
   		}else{
   			alerta('Ingrese un nombre de pais válido','top','p');
   		}
   		return false;
   	}
   	function update_pais(idpa) {
   		var p=$("#p"+idpa).val();
   		if(strSpace(p,2,100)){
   			var atrib=new FormData();
   			atrib.append('idpa',idpa);
   			atrib.append('p',p);
   			set('capital_humano/update_pais',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false);
   		}else{
   			alerta('Ingrese un nombre de pais válido','top','p'+idpa);
   		}
   		return false;
   	}
   	function alerta_pais(idpa){
   		modal("PAIS: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','delete_pais',"'"+idpa+"'");
   		var atrib=new FormData();atrib.append('idpa',idpa);
   		atrib.append('titulo','eliminar definitivamente el pais: '+$("#p"+idpa).val());
   		get('insumo/alerta',atrib,'content_5',true);
   	}
   	function delete_pais(idpa) {
   		var atrib=new FormData();
   		atrib.append('idpa',idpa);
   		set('capital_humano/delete_pais',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false,'5');
   		return false;
   	}
   	/*--- End Paises ---*/
   	/*--- Ciudades ---*/
   	function save_ciudad() {
   		var c=$("#c").val();
   		var cp=$("#cp").val();
   		if(strSpace(c,2,100)){
   			if(cp!=""){
	   			var atrib=new FormData();
	   			atrib.append('cp',cp);
	   			atrib.append('c',c);
	   			set('capital_humano/save_ciudad',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false);
   			}else{
   				alerta('Seleccione un pais donde pertenece la ciudad','top','cp');
   			}
   		}else{
   			alerta('Ingrese un nombre de ciudad válido','top','c');
   		}
   		return false;
   	}
   	function update_ciudad(idci) {
   		var c=$("#c"+idci).val();
   		var cp=$("#cp"+idci).val();
   		if(strSpace(c,2,100)){
   			if(cp!=""){
   				var atrib=new FormData();
	   			atrib.append('idci',idci);
	   			atrib.append('cp',cp);
	   			atrib.append('c',c);
	   			set('capital_humano/update_ciudad',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false);
	   		}else{
   				alerta('Seleccione un pais donde pertenece la ciudad','top','cp'+idci);
   			}
   		}else{
   			alerta('Ingrese un nombre de ciudad válido','top','c'+idci);
   		}
   		return false;
   	}
   	function alerta_ciudad(idci){
   		modal("PAIS: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','delete_ciudad',"'"+idci+"'");
   		var atrib=new FormData();atrib.append('idci',idci);
   		atrib.append('titulo','eliminar definitivamente la ciudad: '+$("#c"+idci).val());
   		get('insumo/alerta',atrib,'content_5',true);
   	}
   	function delete_ciudad(idci) {
   		var atrib=new FormData();
   		atrib.append('idci',idci);
   		set('capital_humano/delete_ciudad',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false,'5');
   		return false;
   	}
   	/*--- End Ciudades ---*/
   	/*--- Feriados ---*/
   	function save_feriado(){
   		var fec=$("#f_fec").val();
   		var tip=$("#f_tip").val();
   		var des=$("#f_des").val();
   		if(fecha(fec)){
   			if(entero(tip,0,10)){
	   			if(textarea(des,0,150)){
		   			var atrib=new FormData();
		   			atrib.append('fec',fec);
		   			atrib.append('tip',tip);
		   			atrib.append('des',des);
		   			set('capital_humano/save_feriado',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false);
	   			}else{
	   				alerta('Ingrese una contenido válido','top','f_des');
	   			}
   			}else{
   				alerta('Seleccione una opcion','top','f_tip');
   			}
   		}else{
   			alerta('Seleccione una fecha válida','top','f_fec');
   		}
   		return false;
   	}
   	function update_feriado(idf) {
   		var fec=$("#f_fec"+idf).val();
   		var tip=$("#f_tip"+idf).val();
   		var des=$("#f_des"+idf).val();
   		if(entero(idf,0,10)){
	   		if(fecha(fec)){
	   			if(entero(tip,0,10)){
		   			if(textarea(des,5,150)){
			   			var atrib=new FormData();
			   			atrib.append('idf',idf);
			   			atrib.append('fec',fec);
			   			atrib.append('tip',tip);
			   			atrib.append('des',des);
			   			set('capital_humano/update_feriado',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false);
		   			}else{
		   				alerta('Ingrese una contenido válido','top','f_des'+idf);
		   			}
				}else{
	   				alerta('Seleccione una opcion','top','f_tip'+idf);
	   			}
	   		}else{
	   			alerta('Seleccione una fecha válida','top','f_fec'+idf);
	   		}
   		}else{
   			msj("fail");
   		}
   		return false;
   	}
   	function alerta_feriado(idf,fecha,descripcion){
   		modal("FERIADO: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','delete_feriado',"'"+idf+"'");
   		var atrib=new FormData();atrib.append('idf',idf);
   		atrib.append('titulo','eliminar definitivamente el feriado : <b>'+descripcion+'</b> en fecha '+fecha);
   		get('insumo/alerta',atrib,'content_5',true);
   	}
   	function delete_feriado(idf){
   		var atrib=new FormData();
   		atrib.append('idf',idf);
   		set('capital_humano/delete_feriado',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false,'5');
   		return false;
   	}
   	/*--- End Feriados ---*/
	/*--- tipo de contrato ---*/
   	function save_tipo(){
   		var tip=$("#t_tip").val();
   		var hor=$("#t_hor").val();
   		var des=$("#t_des").val();
   		if(entero(tip,0,1) && tip>=0 && tip<=1){
   			if(hor>=0 && hor<=99){
   				if(textarea(des,0,150)){
					var atrib=new FormData();
		   			atrib.append('tip',tip);
		   			atrib.append('hor',hor);
		   			atrib.append('des',des);
   					set('capital_humano/save_tipo',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false);
   				}else{
   					alerta('Ingrese un contenido válido','top','t_des');	
   				}
   			}else{
   				alerta('Ingrese un valor válido','top','t_hor');
   			}
   		}else{
   			alerta('Seleccione una tipo válido','top','t_tip');
   		}
   		return false;
   	}
   	function select_principal(idtc){
   		if(entero(idtc,0,10)){
   			var atrib=new FormData();
		   	atrib.append('idtc',idtc);
   			set('capital_humano/select_principal',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false);
   		}else{
   			msj("fail");
   		}
   	}
   	function update_tipo(idtc){
   		var tip=$("#t_tip"+idtc).val();
   		var hor=$("#t_hor"+idtc).val();
   		var des=$("#t_des"+idtc).val();
   		if(entero(idtc,0,10)){
	   		if(entero(tip,0,1) && tip>=0 && tip<=1){
	   			if(hor>=0 && hor<=99){
	   				if(textarea(des,0,150)){
						var atrib=new FormData();
						atrib.append('idtc',idtc);
			   			atrib.append('tip',tip);
			   			atrib.append('hor',hor);
			   			atrib.append('des',des);
	   					set('capital_humano/update_tipo',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false);
	   				}else{
	   					alerta('Ingrese un contenido válido','top','t_des'+idtc);	
	   				}
	   			}else{
	   				alerta('Ingrese un valor válido','top','t_hor'+idtc);
	   			}
	   		}else{
	   			alerta('Seleccione una tipo válido','top','t_tip'+idtc);
	   		}
   		}else{
   			msj("fail");
   		}
   		return false;
   	}
   	function alerta_tipo(idtc,tipo,horas){
   		modal("TIPO DE CONTRATO: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','delete_tipo',"'"+idtc+"'");
   		var atrib=new FormData();atrib.append('idtc',idtc);
   		atrib.append('titulo','eliminar definitivamente el tipo de contrato de : <b>'+tipo+'</b> de '+horas+'h.');
   		get('insumo/alerta',atrib,'content_5',true);
   	}
   	function delete_tipo(idtc){
   		var atrib=new FormData();
   		atrib.append('idtc',idtc);
   		set('capital_humano/delete_tipo',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false,'5');
   		return false;
   	}
   	/*--- End tipo de contrato ---*/
	/*configuracion de sistema*/
	function update_configuracion(id){
		var valor=$("#configuracion"+id).val();
		var type=$("#configuracion"+id).data("type");
		if(valor!=undefined && type!=undefined){
			var control=true;
			if(type=="number"){
				if(!decimal(valor,6,4)){
					control=false;
				}
			}
			if(control){
				var atribs= new FormData();
				atribs.append('id',id);
				atribs.append('valor',valor);
				set('capital_humano/update_configuracion',atribs,'NULL',true,'capital_humano/view_config',{},'contenido',false);
			}
		}else{
			msj("Error de valriables, vuelva a cargar la página.");
		}
	}
	/*end configuracion de sistema*/
/*------- END MANEJO DE CONFIGURACION -------*/