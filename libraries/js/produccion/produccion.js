 var x=$(document);
 x.ready(inicio);

 function inicio(){
  	$('#producto').click(function(){get_2n('produccion/search_producto',{},'search',false,'produccion/view_producto',{},'contenido',true);activar("producto","Producto","produccion?p=1");})
  	$('#orden').click(function(){get_2n('produccion/search_orden',{},'search',false,'produccion/view_orden',{},'contenido',true);activar("orden","Orden de producción","produccion?p=2");})
  	$('#config').click(function(){get_2n('',{},'search',false,'produccion/view_config',{},'contenido',true);activar("config","Configuración de producción","produccion?p=5");});
 }

function reset_input(id){
	if(id!="s_cod"){ $("#s_cod").val("");}
	if(id!="s_nom"){ $("#s_nom").val("");}
	if(id!="s_fec"){ $("#s_fec").val(""); }
	if(id!="s_dis"){ $("#s_dis").val(""); }
	if(id!="sv_num"){ $("#sv_num").val("");}
	if(id!="sv_cli"){ $("#sv_cli").val("");}
	if(id!="sv_fpe" && id!="sv_fen"){ $("#sv_fpe").val("");$("#sv_fen").val("");}
}

function reset_input_3(id){
	if(id!="cod_3"){ $("#cod_3").val("");}
	if(id!="nom_3"){ $("#nom_3").val("");}
}

/*------- MANEJO DE PRODUCTOS -------*/
   	/*--- Buscador ---*/
   	function view_producto(){
   		var cod=$("#s_cod").val();
   		var nom=$("#s_nom").val();
   		var fec=$("#s_fec").val();
   		var dis=$("#s_dis").val();
   		var atrib=new FormData();
   		atrib.append('cod',cod);
   		atrib.append('nom',nom);
   		atrib.append('fec',fec);
   		atrib.append('dis',dis);
   		get('produccion/view_producto',atrib,'contenido',true);
   		return false;
   	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	function all_producto(){
   		reset_input("");
   		view_producto();
   	}
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
	 function new_producto(){
	   	modal("PRODUCTO: Nuevo","xlg","1");
		btn_modal('save_producto',"",'',"",'1','xlg');
		var ele=new FormData();
		get('produccion/new_producto',{},'content_1',true);
	 }
	 function save_producto(){
	 	var cod=$('#cod').val();
	 	var nom=$('#nom').val();
	 	var dis=$('#dis').val();
	 	var fec=$('#fec').val();
	 	var des=$('#des').val();
	 	var gru=$('#gru').val();
	 	//var cu=$('#cu').val();
	 	var peso=$('#peso').val();
	 	var etcu=$('#etcu').val();
	 	var corr=$('#corr').val();
	 	var pllo=$('#pllo').val();
	 	var relo=$('#relo').val();
	 	var etlo=$('#etlo').val();
	 	var repu=$('#repu').val();
	 	var cidelante=$('#cidelante').val();
	 	var cidetras=$('#cidetras').val();
	 	var cidentro=$('#cidentro').val();
	 	var alto=$('#alto').val();
	 	var ancho=$('#ancho').val();
	 	var largo=$('#largo').val();
	 	var car=$('#car').val();
		if(strSpace(cod,2,20)){
		 	if(strSpace(nom,3,150)){
		 		if(strSpace(dis,3,100)){
		 			if(fecha(fec)){
		 				if(entero(gru,0,10)){
		 					var guardar=true;
		 					if(des!=""){ if(!strSpace(des,5,900)){ guardar=false;alerta('Ingrese una descripcion valida','top','des');}}
		 					if(peso!=""){ if(!decimal(peso,6,2) && (peso<0 || peso>999999.99)){ guardar=false;alerta('Ingrese un valor valido','top','peso');}}
		 					if(etcu!=""){ if(!strSpace(etcu,0,150)){ guardar=false;alerta('Ingrese un contenido válido','top','etcu');}}
		 					if(corr!=""){ if(!strSpace(corr,0,150)){ guardar=false;alerta('Ingrese un contenido válido','top','corr');}}
		 					if(pllo!=""){ if(!strSpace(pllo,0,150)){ guardar=false;alerta('Ingrese un contenido válido','top','pllo');}}
		 					if(relo!=""){ if(!strSpace(relo,0,150)){ guardar=false;alerta('Ingrese un contenido válido','top','relo');}}
		 					if(etlo!=""){ if(!strSpace(etlo,0,150)){ guardar=false;alerta('Ingrese un contenido válido','top','etlo');}}
		 					if(repu!=""){ if(!strSpace(repu,0,150)){ guardar=false;alerta('Ingrese un contenido válido','top','repu');}}
		 					if(cidelante!=""){ if(!strSpace(cidelante,0,150)){ guardar=false;alerta('Ingrese un contenido válido','top','cidelante');}}
		 					if(cidetras!=""){ if(!strSpace(cidetras,0,150)){ guardar=false;alerta('Ingrese un contenido válido','top','cidetras');}}
		 					if(cidentro!=""){ if(!strSpace(cidentro,0,150)){ guardar=false;alerta('Ingrese un contenido válido','top','cidentro');}}
		 					if(ancho!=""){ if(!decimal(ancho,5,2) && (ancho<0 || ancho>99999.99)){ guardar=false;alerta('Ingrese valor válido','top','ancho');}}
		 					if(alto!=""){ if(!decimal(alto,5,2) && (alto<0 || alto>99999.99)){ guardar=false;alerta('Ingrese valor válido','top','alto');}}
		 					if(largo!=""){ if(!decimal(largo,5,2) && (largo<0 || largo>99999.99)){ guardar=false;alerta('Ingrese valor válido','top','largo');}}
		 					if(car!=""){ if(!strSpace(car,0,900)){ guardar=false;alerta('Ingrese un contenido válido','top','car');}}
		 					if(guardar){
			 					var atrib= new FormData();
			 					atrib.append('cod',cod);
								atrib.append('nom',nom);
								atrib.append('dis',dis);
								atrib.append('fec',fec);
								atrib.append('des',des);
								atrib.append('gru',gru);						
								atrib.append('peso',peso);
								atrib.append('etcu',etcu);
								atrib.append('corr',corr);
								atrib.append('pllo',pllo);
								atrib.append('relo',relo);
								atrib.append('etlo',etlo);						
								atrib.append('repu',repu);
								atrib.append('cidelante',cidelante);
								atrib.append('cidetras',cidetras);
								atrib.append('cidentro',cidentro);
								atrib.append('alto',alto);
								atrib.append('ancho',ancho);
								atrib.append('largo',largo);
								atrib.append('car',car);
				 				var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('dis',$("#s_dis").val());
				 				set('produccion/save_producto',atrib,'NULL',true,'produccion/view_producto',atrib3,'contenido',false,'1');
		 					}
			 			}else{
			 				alerta('Seleccione una grupo del producto','top','gru');	
			 			}
		 			}else{
			 			alerta('Seleccione una Fecha','top','fec');
			 		}
		 		}else{
		 			alerta('Ingrese un nombre de diseñador de producto válido','top','dis');
		 		}
		 	}else{
		 		alerta('Ingrese un nombre de producto','top','nom');
		 	}
		 }else{
		 	alerta('Ingrese un código de producto','top','cod');
		 }
		 return false;
	 }
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	function imprimir_productos(json){
   		modal("PRODUCTOS: Configuracion de impresion","xlg","1");
 		btn_modal('',"",'imprimir',"'area'",'1','xlg');
 		var atrib=new FormData();atrib.append('json',json);
 		get_2n('produccion/config_imprimir_productos',atrib,'content_1',true,'produccion/imprimir_productos',atrib,'area',true);
   	}
   	function arma_productos(json){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var v6=""; if($("#6:checked").val()){v6='ok'}
		var v7=""; if($("#7:checked").val()){v7='ok'}
		var v8=""; if($("#8:checked").val()){v8='ok'}
		var v9=""; if($("#9:checked").val()){v9='ok'}
		var v10=""; if($("#10:checked").val()){v10='ok'}
		var v11=""; if($("#11:checked").val()){v11='ok'}
		var nro=$("#nro").val();
		var atrib=new FormData();
		atrib.append('json',json);
		atrib.append('v1',v1);
		atrib.append('v2',v2);
		atrib.append('v3',v3);
		atrib.append('v4',v4);
		atrib.append('v5',v5);
		atrib.append('v6',v6);
		atrib.append('v7',v7);
		atrib.append('v8',v8);
		atrib.append('v9',v9);
		atrib.append('v10',v10);
		atrib.append('v11',v11);
		atrib.append('nro',nro);
		get('produccion/imprimir_productos',atrib,'area',true);
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	/*--- End Reportes ---*/
   		/*----FICHA TECNICA DEL PRODUCTO----*/
		function reportes(idp){
		 	modal("PRODUCTO: Detalle","md","1");
		 	btn_modal('',"",'imprimir',"'area'",'1','md');
		 	var atrib=new FormData();
		 	atrib.append('id',idp);
		 	get('produccion/ficha_tecnica',atrib,'content_1',false);
		}
		/*----END FICHA TECNICA DEL PRODUCTO----*/
		/*----COSTO TOTAL DE PRODUCCION DEL PRODUCTO----*/
		function costo_total_produccion(idp){
			modal("PRODUCTO: Costo de producción","md","1");
		 	btn_modal('',"",'imprimir',"'area'",'1','md');
		 	var atrib=new FormData();
		 	atrib.append('id',idp);
		 	get('produccion/costo_total_produccion',atrib,'content_1',false);
		}
		/*----END COSTO TOTAL DE PRODUCCION DEL PRODUCTO----*/
		/*----DETALLE DE PROCESOS----*/
		function detalle_procesos(idp){
			modal("PRODUCTO: Detalle de procesos","md","1");
		 	btn_modal('',"",'imprimir',"'area'",'1','md');
		 	var atrib=new FormData();
		 	atrib.append('idp',idp);
		 	get_2n('produccion/search_detalle_procesos',atrib,'content_1',false,'produccion/detalle_procesos',atrib,'area',true);
		}
		function search_detalle_procesos(idp){
			var nro=$("#nro").val();
			if(entero(nro,1,2) && nro>0){
				var atrib=new FormData();atrib.append('idp',idp);atrib.append('nro',nro);
				get('produccion/detalle_procesos',atrib,'area',true);
			}else{
				alerta('seleccione un valor válido','top','nro');
			}
		}
		/*----END DETALLE DE PROCESOS----*/
   	/*--- configuracion ---*/
   		/*----MODIFICAR DATOS DE PRODUCTO----*/
		function config(idp){
		 	modal("PRODUCTO: Configuración","xlg","1");
			btn_modal('update_producto',"'"+idp+"'",'','','1',"xlg");
			var ele=new FormData();ele.append('idp',idp);
			get('produccion/form_update_producto',ele,'content_1',false);
		}
		function update_producto(idp){
		 	var cod=$('#cod').val();
		 	var nom=$('#nom').val();
		 	var dis=$('#dis').val();
		 	var fec=$('#fec').val();
		 	var des=$('#des').val();
		 	var gru=$('#gru').val();
		 	//var cu=$('#cu').val();
		 	var peso=$('#peso').val();
		 	var etcu=$('#etcu').val();
		 	var corr=$('#corr').val();
		 	var pllo=$('#pllo').val();
		 	var relo=$('#relo').val();
		 	var etlo=$('#etlo').val();
		 	var repu=$('#repu').val();
		 	var cidelante=$('#cidelante').val();
		 	var cidetras=$('#cidetras').val();
		 	var cidentro=$('#cidentro').val();
		 	var alto=$('#alto').val();
		 	var ancho=$('#ancho').val();
		 	var largo=$('#largo').val();
		 	var car=$('#car').val();
			if(strSpace(cod,2,20)){
			 	if(strSpace(nom,3,150)){
			 		if(strSpace(dis,3,100)){
			 			if(fecha(fec)){
			 				if(entero(gru,0,10)){
			 					var guardar=true;
			 					if(des!=""){ if(!strSpace(des,5,900)){ guardar=false;alerta('Ingrese una descripcion valida','top','des');}}
			 					if(peso!=""){ if(!decimal(peso,6,2) && (peso<0 || peso>999999.99)){ guardar=false;alerta('Ingrese un valor valido','top','peso');}}
			 					if(etcu!=""){ if(!strSpace(etcu,0,150)){ guardar=false;alerta('Ingrese un contenido válido','top','etcu');}}
			 					if(corr!=""){ if(!strSpace(corr,0,150)){ guardar=false;alerta('Ingrese un contenido válido','top','corr');}}
			 					if(pllo!=""){ if(!strSpace(pllo,0,150)){ guardar=false;alerta('Ingrese un contenido válido','top','pllo');}}
			 					if(relo!=""){ if(!strSpace(relo,0,150)){ guardar=false;alerta('Ingrese un contenido válido','top','relo');}}
			 					if(etlo!=""){ if(!strSpace(etlo,0,150)){ guardar=false;alerta('Ingrese un contenido válido','top','etlo');}}
			 					if(repu!=""){ if(!strSpace(repu,0,150)){ guardar=false;alerta('Ingrese un contenido válido','top','repu');}}
			 					if(cidelante!=""){ if(!strSpace(cidelante,0,150)){ guardar=false;alerta('Ingrese un contenido válido','top','cidelante');}}
			 					if(cidetras!=""){ if(!strSpace(cidetras,0,150)){ guardar=false;alerta('Ingrese un contenido válido','top','cidetras');}}
			 					if(cidentro!=""){ if(!strSpace(cidentro,0,150)){ guardar=false;alerta('Ingrese un contenido válido','top','cidentro');}}
			 					if(ancho!=""){ if(!decimal(ancho,5,2) && (ancho<0 || ancho>99999.99)){ guardar=false;alerta('Ingrese valor válido','top','ancho');}}
			 					if(alto!=""){ if(!decimal(alto,5,2) && (alto<0 || alto>99999.99)){ guardar=false;alerta('Ingrese valor válido','top','alto');}}
			 					if(largo!=""){ if(!decimal(largo,5,2) && (largo<0 || largo>99999.99)){ guardar=false;alerta('Ingrese valor válido','top','largo');}}
			 					if(car!=""){ if(!strSpace(car,0,900)){ guardar=false;alerta('Ingrese un contenido válido','top','car');}}
			 					if(guardar){
				 					var atrib= new FormData();
				 					atrib.append('idp',idp);
				 					atrib.append('cod',cod);
									atrib.append('nom',nom);
									atrib.append('dis',dis);
									atrib.append('fec',fec);
									atrib.append('des',des);
									atrib.append('gru',gru);						
									atrib.append('peso',peso);
									atrib.append('etcu',etcu);
									atrib.append('corr',corr);
									atrib.append('pllo',pllo);
									atrib.append('relo',relo);
									atrib.append('etlo',etlo);		
									atrib.append('repu',repu);
									atrib.append('cidelante',cidelante);
									atrib.append('cidetras',cidetras);
									atrib.append('cidentro',cidentro);
									atrib.append('alto',alto);
									atrib.append('ancho',ancho);
									atrib.append('largo',largo);
									atrib.append('car',car);
					 				var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('dis',$("#s_dis").val());
					 				set('produccion/update_producto',atrib,'NULL',true,'produccion/view_producto',atrib3,'contenido',false,'1');
			 					}
				 			}else{
				 				alerta('Seleccione una grupo del producto','top','gru');	
				 			}
			 			}else{
				 			alerta('Seleccione una Fecha','top','fec');
				 		}
			 		}else{
			 			alerta('Ingrese un nombre de diseñador de producto válido','top','dis');
			 		}
			 	}else{
			 		alerta('Ingrese un nombre de producto','top','nom');
			 	}
			 }else{
			 	alerta('Ingrese un código de producto','top','cod');
			 }
			 return false;
		}
		/*----END MODIFICAR DATOS DE PRODUCTO----*/
		/*-----CONTROL DE MATERIALES Y MATERIALES INIRECTOS EN EL PRODUCTO----*/
		function materiales(idp){
			modal("PRODUCTO: Materiales en el producto","xlg","1");
			btn_modal('',"",'',"",'1',"xlg");
			var ele=new FormData(); ele.append('idp',idp);
			get('produccion/materiales',ele,'content_1',false);	
		}	
			/*--- Materiales ---*/
			function add_material_producto(idp){//ventana de adicion de materiales
				modal("PRODUCTO: Adicionar materiales","md","2");
				btn_modal('',"",'',"",'2','md');
			 	var atrib=new FormData(); atrib.append('idp',idp);
			 	get_2n('produccion/search_material_producto',atrib,'content_2',true,'produccion/view_material_producto',atrib,'contenido_2',true);
			}
			function search_producto_material(idp){
				var cod=$("#cod_3").val();
				var nom=$("#nom_3").val();
				var alm=$("#alm_3").val();
				var atrib=new FormData();atrib.append('cod',cod);atrib.append('nom',nom);atrib.append('alm',alm);atrib.append('idp',idp);
				get('produccion/view_material_producto',atrib,'contenido_2',true);
				return false;
			}
			function select_material(idp,idm,ele,color,diametro){
				var atrib=new FormData();atrib.append('idp',idp);
				set_button('produccion/save_producto_material',{idp,idm},"produccion/materiales",atrib,"content_1",false,ele,color,diametro);
			}
			function update_cantidad_material(idpm,idp){
				var can=$("#can_m"+idpm).val();
				if(decimal(can,7,2) && can>0 && can<=9999999.99){
						var atrib=new FormData();atrib.append('idpm',idpm);atrib.append('can',can);
						var atrib2=new FormData();atrib2.append('idp',idp);
						set('produccion/update_cantidad_material',atrib,'NULL',true,'produccion/materiales',atrib2,'content_1',false);
				}else{
					alerta('Ingrese una cantidad válida, entre 0 y 999999.9, con una decimal','top','can_m'+idpm);
				}
				return false;
			}
			function alerta_material(idpm,idp,nombre,img) {
				modal("MATERIAL: Eliminar","xs","5");
		   		addClick_v2('modal_ok_5','drop_material',"'"+idpm+"','"+idp+"'");
		   		var atrib=new FormData();atrib.append('idpm',idpm);
		   		atrib.append('titulo','definitivamente la el material: <b>'+nombre+'</b> del producto');
		   		atrib.append('img',img);
		   		get('insumo/alerta',atrib,'content_5',true);
			}
			function drop_material(idpm,idp){
				var atrib=new FormData();
				atrib.append('idpm',idpm);
				atrib.append('idp',idp);
				set('produccion/drop_material',atrib,'NULL',true,'produccion/materiales',atrib,'content_1',false,'5');
			}
			/*--- End Materiales ---*/
			/*--- Materiales indirectos---*/
			function add_indirecto_producto(idp){//ventana de adicion de materiales
				modal("PRODUCTO: Adicionar materiales indirectos","md","2");
				btn_modal('',"",'',"",'2','md');
			 	var atrib=new FormData(); atrib.append('idp',idp);
			 	get_2n('produccion/search_indirecto_producto',atrib,'content_2',true,'produccion/view_indirecto_producto',atrib,'contenido_2',true);
			}
			function search_indirecto_producto(idp){
				var cod=$("#cod_3").val();
				var nom=$("#nom_3").val();
				var atrib=new FormData();atrib.append('cod',cod);atrib.append('nom',nom);atrib.append('idp',idp);
				get('produccion/view_indirecto_producto',atrib,'contenido_2',true);
				return false;
			}
			function select_indirecto(idp,idma,ele,color,diametro){
				var atrib=new FormData();atrib.append('idp',idp);
				set_button('produccion/save_producto_indirecto',{idp,idma},"produccion/materiales",atrib,"content_1",false,ele,color,diametro);
			}
			function update_costo_indirecto(idpma,idp){
				var cos=$("#cos_mi"+idpma).val();
				if(decimal(cos,8,4) && cos>=0 && cos<=9999.9999){
						var atrib=new FormData();atrib.append('idpma',idpma);atrib.append('cos',cos);
						var atrib2=new FormData();atrib2.append('idp',idp);
						set('produccion/update_costo_indirecto',atrib,'NULL',true,'produccion/materiales',atrib2,'content_1',false);
				}else{
					alerta('Ingrese una costo válido, entre 0 y 9999.9999, con una maximo de 4 decimales','top','cos_mi'+idpma);
				}
				return false;
			}
			function alerta_indirecto(idpma,idp,nombre,img) {
				modal("MATERIAL: Eliminar","xs","5");
		   		addClick_v2('modal_ok_5','drop_indirecto',"'"+idpma+"','"+idp+"'");
		   		var atrib=new FormData();atrib.append('idpma',idpma);
		   		atrib.append('titulo','definitivamente la el material indirecto: <b>'+nombre+'</b> del producto');
		   		atrib.append('img',img);
		   		get('insumo/alerta',atrib,'content_5',true);
			}
			function drop_indirecto(idpma,idp){
				var atrib=new FormData();
				atrib.append('idpma',idpma);
				atrib.append('idp',idp);
				set('produccion/drop_indirecto',atrib,'NULL',true,'produccion/materiales',atrib,'content_1',false,'5');
			}
			/*--- End Materiales indirectos---*/

		/*-----END CONTROL DE MATERIALES Y MATERIALES INIRECTOS EN EL PRODUCTO----*/
		/*-----CONTROL DE PIEZAS EN EL PRODUCTO----*/
		function piezas(idp){
			modal("PRODUCTO: Adicionar piezas en el producto","xlg","1");
			btn_modal('',"",'',"",'1','xlg');
			var ele=new FormData(); ele.append('idp',idp);
			get('produccion/piezas',ele,'content_1',false);	
		}
		/*--- Manejo de grupos de piezas ---*/
			function adicionar_grupo(idp){
				var atrib=new FormData();
				atrib.append('idp',idp);
				set('produccion/adicionar_grupo',atrib,'NULL',true,'produccion/piezas',atrib,'content_1',false);
			}
			function confirmar_save_color_producto(idcp,idp){
				modal("PRODUCTO: Configurar color","xs","6");
		   		addClick_v2('modal_ok_6','save_color_producto',"'"+idcp+"','"+idp+"'");
		   		var atrib=new FormData();atrib.append('idcp',idcp);atrib.append('idp',idp);
		   		get('produccion/confirmar_save_color_producto',atrib,'content_6',true);
			}
			function save_color_producto(idcp,idp){
				var atrib=new FormData();atrib.append('idcp',idcp);atrib.append('idp',idp);atrib.append('u',$("#e_user").val());atrib.append('p',$("#e_password").val());
				var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('dis',$("#s_dis").val());
				set_2n('produccion/save_color_producto',atrib,'NULL',true,'produccion/piezas',atrib,'content_1',false,'produccion/view_producto',atrib3,'contenido',false,"6");
			}
			function confirmar_reset_color_producto(idcp,idp){
				modal("PRODUCTO: Configurar color","xs","6");
		   		addClick_v2('modal_ok_6','reset_color_producto',"'"+idcp+"','"+idp+"'");
		   		var atrib=new FormData();atrib.append('idcp',idcp);atrib.append('idp',idp);
		   		get('produccion/confirmar_reset_color_producto',atrib,'content_6',true);
			}
			function reset_color_producto(idcp,idp){
				var atrib=new FormData();atrib.append('idcp',idcp);atrib.append('idp',idp);atrib.append('u',$("#e_user").val());atrib.append('p',$("#e_password").val());
				var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('dis',$("#s_dis").val());
				set_2n('produccion/reset_color_producto',atrib,'NULL',true,'produccion/piezas',atrib,'content_1',false,'produccion/view_producto',atrib3,'contenido',false,"6");
			}
			function confirmar_grupo(idp,idcp) {
				modal("GRUPO DE PIEZAS: Eliminar","xs","5");
		   		addClick_v2('modal_ok_5','drop_grupo',"'"+idp+"','"+idcp+"'");
		   		var atrib=new FormData();atrib.append('idcp',idcp);
		   		get('produccion/confirmar_grupo',atrib,'content_5',true);
			}
			function drop_grupo(idp,idcp){
				var atrib=new FormData();
				atrib.append('u',$("#e_user").val());
				atrib.append('p',$("#e_password").val());
				atrib.append('idp',idp);atrib.append('idcp',idcp);
				set('produccion/drop_grupo',atrib,'NULL',true,'produccion/piezas',atrib,'content_1',false,'5');
			}
		/*--- End manejo de grupos de piezas ---*/
		/*--- Manejo de piezas ---*/
			function new_pieza(idp,idcp){
				modal("PRODUCTO: Adicionar pieza al grupo","md","2");
				btn_modal('save_pieza',"'"+idp+"','"+idcp+"'",'',"",'2','md');
			 	var atrib=new FormData();atrib.append('idp',idp);atrib.append('idcp',idcp);
			 	get('produccion/new_pieza',atrib,'content_2',true);
			}
			function save_pieza(idp,idcp){
				var file=$('#fot').prop('files');
			 	var gru=$('#gru').val();
			 	var alt=$('#alt').val();
			 	var anc=$('#anc').val();
			 	var can=$('#can').val();
			 	var des=$('#des').val();
			 		if(entero(gru,1,10)){
			 			if(decimal(alt,5,2) && alt>0 && alt<=99999.99){
			 				if(decimal(anc,5,2) && alt>0 && alt<=99999.99){
			 					if(entero(can,1,3) && can>0 && can<=999){
									var guardar=true;
				 					if(des!=""){ if(!strSpace(des,5,500)){ guardar=false; alerta('Ingrese una descripcion válida','top','des');}}
				 					if(guardar){
					 					var atrib= new FormData();
					 					atrib.append('idp',idp);
					 					atrib.append('idcp',idcp);
										atrib.append('gru',gru);
										atrib.append('alt',alt);
										atrib.append('anc',anc);
										atrib.append('can',can);
										atrib.append('des',des);
										atrib.append('archivo',file[0]);
							 			set('produccion/save_pieza',atrib,'NULL',true,'produccion/piezas',atrib,'content_1',false,'2');
				 					}
			 					}else{
			 						alerta('Cantidad inválida','top','can');
			 					}
			 				}else{
			 					alerta('Ancho de producto inválido','top','anc');
			 				}
			 			}else{
			 				alerta('Alto de producto inválido','top','alt');
			 			}
			 		}else{
			 			alerta('Seleccione un grupo de pieza válido','top','gru');
			 		}
			 	return false;
			}
			function add_proceso_material_adicional(idpi){
				modal("PIEZA: Adicionar procesos y materiales","md","2");
				btn_modal('',"",'',"",'2','md');
				var atrib=new FormData();atrib.append('idpi',idpi);
			 	get('produccion/add_proceso_material_adicional',atrib,'content_2',true);
			}
			function save_pieza_proceso(idpr,idpi,ele,color,diametro){
				set_button('produccion/save_pieza_proceso',{idpr,idpi},"","","",false,ele,color,diametro);
			}
			function save_pieza_material_adicional(idml,idpi,ele,color,diametro){
				set_button('produccion/save_pieza_material_adicional',{idml,idpi},"","","",false,ele,color,diametro);
			}
			function modificar_pieza(idpi,idcp,idp){
				modal("PIEZA: Modificar pieza en el grupo","md","2");
				btn_modal('update_pieza',"'"+idpi+"','"+idcp+"','"+idp+"'",'',"",'2',"md");
				var atrib=new FormData();atrib.append('idpi',idpi);atrib.append('idcp',idcp);atrib.append('idp',idp);
			 	get('produccion/modificar_pieza',atrib,'content_2',true);
			}
			function update_pieza(idpi,idcp,idp){
				var file=$('#fot').prop('files');
			 	var gru=$('#gru').val();
			 	var alt=$('#alt').val();
			 	var anc=$('#anc').val();
			 	var can=$('#can').val();
			 	var des=$('#des').val();
			 		if(entero(gru,1,10)){
			 			if(decimal(alt,5,2) && alt>0 && alt<=99999.99){
			 				if(decimal(anc,5,2) && alt>0 && alt<=99999.99){
			 					if(entero(can,1,3) && can>0 && can<=999){
									var guardar=true;
				 					if(des!=""){ if(!strSpace(des,5,500)){ guardar=false; alerta('Ingrese una descripcion válida','top','des');}}
				 					if(guardar){
					 					var atrib= new FormData();
					 					atrib.append('idp',idp);
					 					atrib.append('idpi',idpi);
					 					atrib.append('idcp',idcp);
										atrib.append('gru',gru);
										atrib.append('alt',alt);
										atrib.append('anc',anc);
										atrib.append('can',can);
										atrib.append('des',des);
										atrib.append('archivo',file[0]);
							 			set('produccion/update_pieza',atrib,'NULL',true,'produccion/piezas',atrib,'content_1',false,'2');
				 					}
			 					}else{
			 						alerta('Cantidad inválida','top','can');
			 					}
			 				}else{
			 					alerta('Ancho de producto inválido','top','anc');
			 				}
			 			}else{
			 				alerta('Alto de producto inválido','top','alt');
			 			}
			 		}else{
			 			alerta('Seleccione un grupo de pieza válido','top','gru');
			 		}
			 	return false;
			}

			function confirmar_pieza(idp,idpi) {
				modal("PIEZA: Eliminar","xs","5");
		   		addClick_v2('modal_ok_5','drop_pieza',"'"+idp+"','"+idpi+"'");
		   		var atrib=new FormData();atrib.append('idpi',idpi);
		   		get('produccion/confirmar_pieza',atrib,'content_5',true);
			}
			function drop_pieza(idp,idpi){
				var atrib=new FormData();
				atrib.append('u',$("#e_user").val());
				atrib.append('p',$("#e_password").val());
				atrib.append('idp',idp);atrib.append('idpi',idpi);
				set('produccion/drop_pieza',atrib,'NULL',true,'produccion/piezas',atrib,'content_1',false,'5');
			}
		/*--- End manejo de piezas ---*/
		/*--- Manejo de materiales en el grupo de piezas ---*/
			function add_imagen_pieza_material(idp,idpim){
				modal("PRODUCTO: Adicionar imagen al grupo","sm","2");
				btn_modal('save_imagen_pieza_material',"'"+idp+"','"+idpim+"'",'',"",'2','sm');
				var atrib=new FormData();atrib.append('idp',idp);atrib.append('idpim',idpim);
				get('produccion/add_imagen_pieza_material',atrib,'content_2',true);
			}
			function save_imagen_pieza_material(idp,idpim){
				var file=$('#fot').prop('files');
				var atrib= new FormData();
				atrib.append('idpim',idpim);
				for(i=0; i<file.length; i++){
					atrib.append('archivo'+i,file[i]); //Añadimos cada archivo a el arreglo con un indice direfente
				}
				var atrib2=new FormData();atrib2.append('idp',idp);atrib2.append('idpim',idpim);
				var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('dis',$("#s_dis").val());
				set_3n('produccion/save_imagen_pieza_material',atrib,'NULL',true,'produccion/add_imagen_pieza_material',atrib2,'content_2',false,'produccion/piezas',atrib2,'content_1',false,'produccion/view_producto',atrib3,'contenido',false,"");
			}
			function confirmar_material_grupo(idpim,idp){
				modal("PIEZA: Eliminar","xs","5");
		   		addClick_v2('modal_ok_5','drop_material_grupo',"'"+idpim+"','"+idp+"'");
		   		var atrib=new FormData();atrib.append('idpim',idpim);
		   		get('produccion/confirmar_material_grupo',atrib,'content_5',true);
			}
			function drop_material_grupo(idpim,idp){
				var atrib=new FormData();atrib.append('u',$("#e_user").val());atrib.append('p',$("#e_password").val());atrib.append('idp',idp);atrib.append('idpim',idpim);
				var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('dis',$("#s_dis").val());
				set_2n('produccion/drop_material_grupo',atrib,'NULL',true,'produccion/piezas',atrib,'content_1',false,'produccion/view_producto',atrib3,'contenido',false,"5");
			}
			function add_material_grupo(idp,idcp){//ventana de adicion de materiales
				modal("PRODUCTO: Adicionar materiales al grupo","md","2");
				btn_modal('',"",'',"",'2','md');
			 	var atrib=new FormData(); atrib.append('idp',idp);atrib.append('idcp',idcp);
			 	get_2n('produccion/search_material_grupo',atrib,'content_2',true,'produccion/view_material_grupo',atrib,'contenido_2',true);
			}
			function search_material_grupo(idcp,idp){
				var cod=$("#cod_3").val();
				var nom=$("#nom_3").val();
				var alm=$("#alm_3").val();
				var atrib=new FormData();atrib.append('cod',cod);atrib.append('nom',nom);atrib.append('alm',alm);atrib.append('idcp',idcp);atrib.append('idp',idp);
				get('produccion/view_material_grupo',atrib,'contenido_2',true);
				return false;
			}
			function adicionar_material_grupo(idcp,idm,idp){
				var atrib=new FormData();atrib.append('idcp',idcp);atrib.append('idm',idm);atrib.append('idp',idp);
				set('produccion/adicionar_material_grupo',atrib,'NULL',true,'produccion/piezas',atrib,'content_1',false);
			}
			function update_color_pieza(idp,idpim,idco){
				if(entero(idco,0,10)){
					var atrib= new FormData();atrib.append('idpim',idpim);atrib.append('idco',idco);
					var atrib2=new FormData();atrib2.append('idp',idp);atrib2.append('idpim',idpim);
					var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('dis',$("#s_dis").val());
					set_2n('produccion/update_color_pieza',atrib,'NULL',true,'produccion/piezas',atrib2,'content_1',false,'produccion/view_producto',atrib3,'contenido',false,"NULL");
				}else{
					msj("Color no válido");
				}
			}
			function drop_imagen_pieza_material(idp,idpim,idim){
				var atrib= new FormData();
				atrib.append('idim',idim);
				var atrib2=new FormData();atrib2.append('idp',idp);atrib2.append('idpim',idpim);
				//para mostrar todos los productos
				var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('dis',$("#s_dis").val());
				set_3n('produccion/drop_imagen_pieza_material',atrib,'NULL',true,'produccion/add_imagen_pieza_material',atrib2,'content_2',false,'produccion/piezas',atrib2,'content_1',false,'produccion/view_producto',atrib3,'contenido',false,"");
				
			}
			function save_portada(idp,idpim,idcp){
				var atrib= new FormData();atrib.append('idpim',idpim);atrib.append('idcp',idcp);atrib.append('idp',idp);
				var atrib2=new FormData();atrib2.append('idp',idp);atrib2.append('idpim',idpim);
				var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('dis',$("#s_dis").val());
				set_2n('produccion/save_portada',atrib,'NULL',true,'produccion/piezas',atrib2,'content_1',false,'produccion/view_producto',atrib3,'contenido',false,"");
			}
			function reset_portada(idp,idpim,idcp){
				var atrib= new FormData();atrib.append('idpim',idpim);atrib.append('idcp',idcp);atrib.append('idp',idp);
				var atrib2=new FormData();atrib2.append('idp',idp);atrib2.append('idpim',idpim);
				var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('dis',$("#s_dis").val());
				set_2n('produccion/reset_portada',atrib,'NULL',true,'produccion/piezas',atrib2,'content_1',false,'produccion/view_producto',atrib3,'contenido',false,"NULL");
			}
		/*--- End manejo de materiales en el grupo de piezas ---*/
		/*--- Manejo de materiales en el grupo de piezas ---*/
			function procesos(idp){//ventana de adicion de materiales
				modal("PRODUCTO: Control de procesos","xlg","1");
				btn_modal('',"",'',"",'1','xlg');
				var atrib=new FormData();atrib.append('idp',idp);
			 	get('produccion/procesos',atrib,'content_1',false);
			}
			function save_producto_proceso(idp){
				var pro=$("#pro").val();
				var nom=$("#nom").val();
				var h=$("#h").val();
				var m=$("#m").val();
				var s=$("#s").val();
				var cos=$("#cos").val();
				if(entero(pro,1,10)){
					if(strSpace(nom,2,200)){
						if(h==""){h="0";}if(m==""){m="0";}if(s==""){s="0";}
						if(entero(h,1,4) && h>=0 && h<=9999){
							if(entero(m,1,2) && m>=0 && m<=59){
								if(entero(s,1,2) && s>=0 && s<=59){
									if(decimal(cos,4,2) && cos>0 && cos<=9999.99){
										var atrib=new FormData();
										atrib.append('idp',idp);atrib.append('pro',pro);atrib.append('nom',nom);atrib.append('h',h);atrib.append('m',m);;atrib.append('s',s);;atrib.append('cos',cos);
										set('produccion/save_producto_proceso',atrib,'NULL',true,'produccion/procesos',atrib,'content_1',false);
									}else{
										alerta('Ingrese un costo de producción numérico mayor que cero hasta 9999.99.','top','cos');
									}
								}else{
									alerta('Ingrese un valor numérico entero de segundos entre 0 y 59.','top','s');
								}
							}else{
								alerta('Ingrese un valor numérico entero de minutos entre 0 y 59.','top','m');
							}
						}else{
							alerta('Ingrese un valor numérico entero de horas entre 0 y 9999.','top','h');
						}
					}else{
						alerta('Ingrese un nombre válido de 2 a 200 caracteres, puede incluir espacios.','top','nom');
					}
				}else{
					alerta('Selecciones un grupo de proceso','top','pro');
				}
				return false;
			}
			function save_tiempo_costo_pieza_proceso(idp,idpipr){
				var h=$("#h1_"+idpipr).val();
				var m=$("#m1_"+idpipr).val();
				var s=$("#s1_"+idpipr).val();
				var cos=$("#cos1_"+idpipr).val();
				if(h==""){h="0";}if(m==""){m="0";}if(s==""){s="0";}
				if(entero(h,1,4) && h>=0 && h<=9999){
					if(entero(m,1,2) && m>=0 && m<=59){
						if(entero(s,1,2) && s>=0 && s<=59){
							if(decimal(cos,4,2) && cos>0 && cos<=9999.99){
								var atrib=new FormData();
								atrib.append('idp',idp);atrib.append('idpipr',idpipr);atrib.append('h',h);atrib.append('m',m);;atrib.append('s',s);;atrib.append('cos',cos);
								set('produccion/save_tiempo_costo_pieza_proceso',atrib,'NULL',true,'produccion/procesos',atrib,'content_1',false);
							}else{
								alerta('Ingrese un costo de producción numérico mayor que cero hasta 9999.99.','top','cos1_'+idpipr);
							}
						}else{
							alerta('Ingrese un valor numérico entero de segundos entre 0 y 59.','top','s1_'+idpipr);
						}
					}else{
						alerta('Ingrese un valor numérico entero de minutos entre 0 y 59.','top','m1_'+idpipr);
					}
				}else{
					alerta('Ingrese un valor numérico entero de horas entre 0 y 9999.','top','h1_'+idpipr);
				}
				return false;
			}
			function save_tiempo_costo(idp,idpipr){
				var pro=$("#pro_"+idpipr).val();
				var nom=$("#nom_"+idpipr).val();
				var h=$("#h2_"+idpipr).val();
				var m=$("#m2_"+idpipr).val();
				var s=$("#s2_"+idpipr).val();
				var cos=$("#cos2_"+idpipr).val();
				if(entero(pro,1,10)){
					if(strSpace(nom,2,200)){
						if(h==""){h="0";}if(m==""){m="0";}if(s==""){s="0";}
						if(entero(h,1,4) && h>=0 && h<=9999){
							if(entero(m,1,2) && m>=0 && m<=59){
								if(entero(s,1,2) && s>=0 && s<=59){
									if(decimal(cos,4,2) && cos>0 && cos<=9999.99){
										var atrib=new FormData();
										atrib.append('idp',idp);atrib.append('idpipr',idpipr);atrib.append('pro',pro);atrib.append('nom',nom);atrib.append('h',h);atrib.append('m',m);;atrib.append('s',s);;atrib.append('cos',cos);
										set('produccion/save_tiempo_costo',atrib,'NULL',true,'produccion/procesos',atrib,'content_1',false);
									}else{
										alerta('Ingrese un costo de producción numérico mayor que cero hasta 9999.99.','top','cos2_'+idpipr);
									}
								}else{
									alerta('Ingrese un valor numérico entero de segundos entre 0 y 59.','top','s2_'+idpipr);
								}
							}else{
								alerta('Ingrese un valor numérico entero de minutos entre 0 y 59.','top','m2_'+idpipr);
							}
						}else{
							alerta('Ingrese un valor numérico entero de horas entre 0 y 9999.','top','h2_'+idpipr);
						}
					}else{
						alerta('Ingrese un nombre válido de 2 a 200 caracteres, puede incluir espacios.','top','nom_'+idpipr);
					}
				}else{
					alerta('Selecciones un grupo de proceso','top','pro_'+idpipr);
				}
				return false;
			}
			function alerta_producto_proceso(idp,idppr) {
				modal("PRODUCTO: Eliminar proceso","xs","5");
		   		addClick_v2('modal_ok_5','drop_producto_proceso',"'"+idp+"','"+idppr+"'");
		   		var atrib=new FormData();atrib.append('idppr',idppr);
		   		atrib.append('titulo','definitivamente el proceso del producto: <b>'+$("#nom_"+idppr).val()+'</b>');
		   		atrib.append('descripcion','<strong class="text-danger">Si eliminar el proceso puede influir en el costo de produccion del producto</strong>');
		   		get('insumo/alerta',atrib,'content_5',true);
			}
			function drop_producto_proceso(idp,idppr){
				var atrib=new FormData();atrib.append('idp',idp);atrib.append('idppr',idppr);
				set('produccion/drop_producto_proceso',atrib,'NULL',true,'produccion/procesos',atrib,'content_1',false,'5');
				//post_set('produccion/drop_producto_proceso',{pp:pp},'produccion/procesos',{id:id},'content_1');
			}

		/*--- End manejo de materiales en el grupo de piezas ---*/
	/*----ELIMINAR PRODUCTO----*/
	 function confirmar_producto(idp,img){
		modal("PRODUCTO: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','drop_producto',"'"+idp+"'");
   		var atrib=new FormData();atrib.append('idp',idp);atrib.append('img',img);
   		get('produccion/confirmar_producto',atrib,'content_5',true);
	 }
   	function drop_producto(idp){
		var atrib=new FormData();
		atrib.append('u',$("#e_user").val());
		atrib.append('p',$("#e_password").val());
		atrib.append('idp',idp);
		var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('dis',$("#s_dis").val())
		set('produccion/drop_producto',atrib,'NULL',true,'produccion/view_producto',atrib3,'contenido',false,'5');
	}
	/*----END ELIMINAR PRODUCTO----*/
/*------- END MANEJO DE PRODUCTOS -------*/
/*------- MANEJO DE ORDEN DE PRODUCCION -------*/
   	/*--- Buscador ---*/
   	function view_orden(){
		var ele=new FormData();
		ele.append('num',$("#sv_num").val());
		ele.append('cli',$("#sv_cli").val());
		ele.append('fpe',$("#sv_fpe").val());
		ele.append('fen',$("#sv_fen").val());
		ele.append('tip',$("input:radio[name=sv_tip]:checked").val());
		get('produccion/view_orden',ele,'contenido',true);
		return false;
	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
	function all_orden(){
		reset_input("");
		view_orden();
	}
   	/*--- End Ver Todo ---*/
   	/*--- Reportes ---*/
	function tiempo_tarea(idpe){ //ventana de adicion de materiales
		modal("TIEMPO POR PROCESO: Configurar","xlg","1");
		btn_modal('','','imprimir',"'area'",'1','xlg');
		var atribs=new FormData();atribs.append('idpe',idpe);
		get('produccion/tiempo_tarea',atribs,'content_1',true);
	}
	function procesos_piezas(idpe){ //
		modal("PROCESOS: Por producto - pieza","xlg","1");
		btn_modal('','','imprimir',"'area'",'1','xlg');
		var atribs=new FormData();atribs.append('idpe',idpe);
		get('produccion/procesos_piezas',atribs,'content_1',true);
	}
		function search_empleado_productos(idpe){
			var emp=$("#emp").val();
				if(entero(emp,0,10)){
					var atrib=new FormData();atrib.append('idpe',idpe);atrib.append('ide',emp);
					get('produccion/search_empleado_productos',atrib,'content_2',true);
				}else{
					alerta('Selecciones un empleado','top','proc');$("#content_2").html("");
				}
		}
	function para_armador(idpe){ //
		modal("CONTROL: Para armadores","xlg","1");
		btn_modal('','','imprimir',"'area'",'1','xlg');
		var atribs=new FormData();atribs.append('idpe',idpe);
		get('produccion/para_armador',atribs,'content_1',true);
	}
	function procesos_general(idpe){ //
		modal("PROCESOS: General del pedido","xlg","1");
		btn_modal('','','imprimir',"'area'",'1','xlg');
		var atribs=new FormData();atribs.append('idpe',idpe);
		get('produccion/procesos_general',atribs,'content_1',true);
	}
		function search_procesos_general(idpe,control){
			var proc=$("#proc").val();
			if(entero(proc,0,10)){
				var atrib=new FormData();atrib.append('idpe',idpe);atrib.append('proc',proc);
				if(control){
					atrib.append('emp',$("#emp").val());
				}
				get('produccion/search_procesos_general',atrib,'content_2',true);
			}else{
				alerta('Selecciones un proceso','top','proc');$("#content_2").html("");
			}
		}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	/*--- ASIGNACION DE TAREAS ---*/
		function view_orden_trabajo(id){//ventana de adicion de materiales
			modal("PRODUCCION: Asignar Tareas","lg","1");
			btn_modal('','','','','1','lg');
			var atribs=new FormData();atribs.append('id',id);
		 	get('produccion/view_orden_trabajo',atribs,'content_1',true);
		}
		function asignar_empleado(idpe,idpim,iddp){
			var ide=$("#emp_pr"+idpim).val();
			var tip=$("#tip_pr"+idpim).val();
			if(entero(ide,0,10)){
				if(tip==0 || tip==1){
					var atrib=new FormData();
					atrib.append('ide',ide);atrib.append('tip',tip);atrib.append('iddp',iddp);atrib.append('id',idpe);
					set('produccion/asignar_empleado',atrib,'NULL',true,'produccion/view_orden_trabajo',atrib,'content_1',false);
				}else{
					alerta('Selecciones un tipo','top','tip_pr'+idpim);	
				}
			}else{
				alerta('Selecciones un empleado','top','emp_pr'+idpim);
			}
		}
		function modificar_producto_empleado(idepr,idpe,iddp){
			var ide=$("#emp_as"+idepr).val();
			var tip=$("#tip_as"+idepr).val();
			if(entero(ide,0,10)){
				if(tip==0 || tip==1){
					var atrib=new FormData();
					atrib.append('ide',ide);atrib.append('tip',tip);atrib.append('idepr',idepr);atrib.append('iddp',iddp);atrib.append('id',idpe);
					set('produccion/modificar_producto_empleado',atrib,'NULL',true,'produccion/view_orden_trabajo',atrib,'content_1',false);
				}else{
					alerta('Selecciones un tipo','top','tip_pr'+idepr);	
				}
			}else{
				alerta('Selecciones un empleado','top','emp_pr'+idepr);
			}			
		}
		function alerta_producto_empleado(idepr,idpe){
			modal("PIEZA GRUPO: Eliminar","xs","5");
			addClick_v2('modal_ok_5','drop_producto_empleado',"'"+idepr+"','"+idpe+"'");
			var atrib=new FormData();atrib.append('idepr',idepr);
			atrib.append('titulo','eliminar definitivamente el empleado asigando al producto');
			get('insumo/alerta',atrib,'content_5',true);
		}
		function drop_producto_empleado(idepr,idpe){
			var atrib=new FormData();atrib.append('idepr',idepr);atrib.append('id',idpe);
			set('produccion/drop_producto_empleado',atrib,'NULL',true,'produccion/view_orden_trabajo',atrib,'content_1',false,'5');
		}







		function search_tarea(idpim,idp,idpe,iddp,can){
			var pr=$("#pr"+idpim).val();
			var atribs=new FormData();
			atribs.append('idpim',idpim);
			atribs.append('idp',idp);
			atribs.append('idpe',idpe);
			atribs.append('iddp',iddp);
			atribs.append('can',can);
			atribs.append('pr',pr);
			get('produccion/search_tarea',atribs,'tarea'+idpim,true);
		}
		function save_tarea_empleado(idproceso,iddp,idpim,pos){
			var vide=$("#emp"+idpim+"-"+pos).val();
			var atrib=new FormData();
			atrib.append('idproceso',idproceso);
			atrib.append('iddp',iddp);
			atrib.append('idpim',idpim);
			atrib.append('vide',vide);
			set_disabled('produccion/save_tarea_empleado',atrib,'NULL',true,'',{},'',false,'',$("#emp"+idpim+"-"+pos));
		}
		function save_tarea_empleado(idproceso,iddp,idpim,pos){
			var vide=$("#emp"+idpim+"-"+pos).val();
			var atrib=new FormData();
			atrib.append('idproceso',idproceso);
			atrib.append('iddp',iddp);
			atrib.append('idpim',idpim);
			atrib.append('vide',vide);
			set_disabled('produccion/save_tarea_empleado',atrib,'NULL',true,'',{},'',false,'',$("#emp"+idpim+"-"+pos));
		}
		function save_tarea_all_empleado(idp,idpr,iddp,tipo,idpe,idpim,can){
			var ide=$("#all_emp"+idpim).val();
			var atrib=new FormData();
			atrib.append('idpim',idpim);
			atrib.append('idp',idp);
			atrib.append('idpe',idpe);
			atrib.append('idpr',idpr);
			atrib.append('iddp',iddp);
			atrib.append('can',can);
			atrib.append('ide',ide);
			atrib.append('tipo',tipo);
			var pr=$("#pr"+idpim).val();
			atrib.append('pr',pr);
			set_disabled('produccion/save_tarea_all_empleado',atrib,'NULL',true,'produccion/search_tarea',atrib,'tarea'+idpim,false,'',$("#all_emp"+idpim));
		}
		function reset_date(idpt,idpim,idp,idpe,iddp,can){
			if(entero(idpt,0,10)){
				var pr=$("#pr"+idpim).val();
				var atrib=new FormData();
				atrib.append('idpt',idpt);
				atrib.append('idpim',idpim);
				atrib.append('idp',idp);
				atrib.append('idpe',idpe);
				atrib.append('iddp',iddp);
				atrib.append('pr',pr);
				atrib.append('can',can);
				set('produccion/reset_date',atrib,'NULL',true,'produccion/search_tarea',atrib,'tarea'+idpim,false);
			}else{
				msj("fail");
			}
		}
		function confirmar_tarea(idpt,idpim,idp,idpe,iddp,can){
	   		modal("TAREA DE EMPLEADO: Eliminar","xs","5");
	   		addClick_v2('modal_ok_5','drop_tarea',"'"+idpt+"','"+idpim+"','"+idp+"','"+idpe+"','"+iddp+"','"+can+"'");
	   		var atribs=new FormData();atribs.append('idpt',idpt);
	   		get('produccion/confirmar_tarea',atribs,'content_5',true);
	   	}
	   	function drop_tarea(idpt,idpim,idp,idpe,iddp,can){
			var ele=new FormData();
			ele.append('u',$("#e_user").val());
			ele.append('p',$("#e_password").val());
			ele.append('idpt',idpt);
			var pr=$("#pr"+idpim).val();
			var atrib=new FormData();atrib.append('idpim',idpim);atrib.append('idp',idp);atrib.append('idpe',idpe);atrib.append('iddp',iddp);atrib.append('pr',pr);atrib.append('can',can);
			set('produccion/drop_tarea',ele,'NULL',true,'produccion/search_tarea',atrib,'tarea'+idpim,false,'5');
		}
		function change_estado(id,tabla,estado){
			if(entero(id,0,10)){
				if(tabla>0 && tabla<4){
					if(estado>=0 && estado<=2){
						var atrib=new FormData();
						atrib.append('id',id);atrib.append('tabla',tabla);atrib.append('estado',estado);
						var atrib3=new FormData();atrib3.append('num',$("#sv_num").val());atrib3.append('cli',$("#sv_cli").val());atrib3.append('fpe',$("#sv_fpe").val());atrib3.append('fen',$("#sv_fen").val());atrib3.append('tip',$("input:radio[name=sv_tip]:checked").val());
						set('produccion/change_estado',atrib,'NULL',true,'produccion/view_orden',atrib3,'contenido',false);
					}else{
						msj("fail");
					}
				}else{
					msj("fail");
				}
			}else{
				msj("fail");
			}
		}
	/*--- END ASIGNACION DE TAREAS ---*/















	/*function asignar_tarea(iddp,idppr,idp,pedido){
		var e=$("#emp"+iddp+idppr+idp).val();
		if(esCadena(e)){
			post_set('produccion/save_tarea_empleado',{iddp:iddp,idppr:idppr,ide:e},'produccion/view_orden_trabajo',{id:pedido},'modal');
		}else{
			msj('Selecciones un empleado','alert_info','modal_alert');
		}
	}*/
	function save_tarea_proceso_pieza(iddp,idpim,idp,idpr,pos,idpe){//modificar las tareas de todo el grupo de piezas en al agisnacion de tareas
		var ide=$("#emp_g_"+iddp+"_"+pos).val();
		//console.log(iddp+" "+idpr+" "+ide+" "+pos);
		var atribs=new FormData();
		atribs.append('iddp',iddp);atribs.append('idpim',idpim);atribs.append('idp',idp);atribs.append('idpr',idpr);atribs.append('ide',ide);atribs.append('id',idpe);
		ajax_set('produccion/save_tarea_proceso_pieza',atribs,'NULL','produccion/view_orden_trabajo',atribs,'content_1','NULL');
	}
	function save_tarea_pieza(idpipr,iddp,ide,idpim,idpe){
		var atribs=new FormData();
		atribs.append('idpipr',idpipr);
		atribs.append('iddp',iddp);
		atribs.append('ide',ide);
		atribs.append('idpim',idpim);
		atribs.append('id',idpe);
		ajax_set('produccion/save_tarea_pieza',atribs,'NULL','',{},'','');
	}


   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE ORDEN DE PRODUCCION -------*/
/*------- MANEJO DE CONFIGURACION -------*/
	/*--- Manejo de grupo en la pieza---*/
	function save_pieza_grupo(){
		var nom=$("#nom_pg").val();
		var des=$("#des_pg").val();
		if(strSpace(nom,2,100)){
			var guardar=true;
			if(des!=""){ if(!strSpace(des,2,200)){ guardar=false;alerta('Ingrese una observación válida','top','des_pg');}}
			if(guardar){
				var atrib=new FormData(); atrib.append('nom',nom); atrib.append('des',des);
				set('produccion/save_pieza_grupo',atrib,'NULL',true,'produccion/view_config',{},'contenido',false);
			}
		}else{
			alerta('Ingrese un nombre válido','top','nom_pg');
		}
		return false;
	}
	function update_pieza_grupo(idpig){
		var nom=$("#nom_pg"+idpig).val();
		var des=$("#des_pg"+idpig).val();
		if(strSpace(nom,2,100)){
			var guardar=true;
			if(des!=""){ if(!strSpace(des,2,200)){ guardar=false;alerta('Ingrese una observación válida','top','des_pg'+idpig);}}
			if(guardar){
				var atrib=new FormData();atrib.append('idpig',idpig);atrib.append('nom',nom);atrib.append('des',des);
				set('produccion/update_pieza_grupo',atrib,'NULL',true,'produccion/view_config',{},'contenido',false);
			}
		}else{
			alerta('Ingrese un nombre válido','top','nom_pg'+idpig);
		}
		return false;
	}
	function alerta_pieza_grupo(idpig){
		modal("PIEZA GRUPO: Eliminar","xs","5");
		addClick_v2('modal_ok_5','drop_pieza_grupo',"'"+idpig+"'");
		var atrib=new FormData();atrib.append('idpig',idpig);
		atrib.append('titulo','eliminar definitivamente el grupo: <b>'+$("#nom_pg"+idpig).val()+'</b>');
		get('insumo/alerta',atrib,'content_5',true);
	}
	function drop_pieza_grupo(idpig){
		var atrib=new FormData();atrib.append('idpig',idpig);
		set('produccion/drop_pieza_grupo',atrib,'NULL',true,'produccion/view_config',{},'contenido',false,'5');
	}
	/*--- End manejo de grupo en la pieza---*/
	/*--- Manejo de materials liquidos y de recorrido ---*/
	function material_liquido(){
		modal("PIEZA: materiales liquidos","md","2");
		btn_modal('',"",'',"",'2','md');
	 	get_2n('produccion/search_material_liquido',{},'content_2',true,'produccion/view_material_liquido',{},'contenido_2',true);
	}
	function search_material_liquido(){
		var cod=$("#cod_3").val();
		var nom=$("#nom_3").val();
		var alm=$("#alm_3").val();
		var atrib=new FormData();atrib.append('cod',cod);atrib.append('nom',nom);atrib.append('alm',alm);
		get('produccion/view_material_liquido',atrib,'contenido_2',true);
		return false;
	}
	function save_material_liquido(idm,ida,ele,color,diametro){
		var den=$("#den"+idm+"_"+ida).val();
		var tip=$("#tip"+idm+"_"+ida).val();
		if(decimal(den,2,4) && den>=0 && den<=99.9999){
			if(entero(tip,1,1)){
				set_button('produccion/save_material_liquido',{idm,den,tip},'produccion/view_config',{},'contenido',false,ele,color,diametro);
			}else{
				alerta("Seleccione una opcion","top","tip"+idm+"_"+ida);
			}
		}else{
			alerta("Ingrese una valor numérico válido","top","den"+idm+"_"+ida);
		}
	}
	function update_material_liquido(idml){
		var den=$("#den_"+idml).val();
		var tip=$("#tip_"+idml).val();
		if(decimal(den,2,4) && den>=0 && den<=99.9999){
			if(entero(tip,1,1)){
				var atrib=new FormData();
				atrib.append('idml',idml);atrib.append('den',den);atrib.append('tip',tip);
				set('produccion/update_material_liquido',atrib,'NULL',true,'produccion/view_config',{},'contenido',false);
			}else{
				alerta("Seleccione una opcion","top","tip_"+idml);
			}
		}else{
			alerta("Ingrese una valor numérico válido","top","den_"+idml);
		}
		return false;
	}
	function alerta_material_liquido(idml,nom,img){
		modal("MATERIAL LIQUIDO: Eliminar","xs","5");
		addClick_v2('modal_ok_5','drop_material_liquido',"'"+idml+"'");
		var atrib=new FormData();atrib.append('idml',idml);
		atrib.append('titulo','eliminar definitivamente el material liquido: <b>'+nom+'</b>');
		atrib.append('img',img);
		get('insumo/alerta',atrib,'content_5',true);
	}
	function drop_material_liquido(idml){
		var atrib=new FormData();atrib.append('idml',idml);
		set('produccion/drop_material_liquido',atrib,'NULL',true,'produccion/view_config',{},'contenido',false,'5');
	}
	/*--- End manejo de materials liquidos ---*/
	/*--- Manejo de procesos de las piezas ---*/
	function save_proceso(){
		var nom=$("#nom_p").val();
		var des=$("#des_p").val();
		if(strSpace(nom,2,100)){
			var guardar=true;
			if(des!=""){ if(!strSpace(des,2,200)){ guardar=false;alerta('Ingrese una observación válida','top','des_p');}}
			if(guardar){
				var atrib=new FormData(); atrib.append('nom',nom); atrib.append('des',des);
				set('produccion/save_proceso',atrib,'NULL',true,'produccion/view_config',{},'contenido',false);
			}
		}else{
			alerta('Ingrese un nombre válido','top','nom_p');
		}
		return false;
	}
	function update_proceso(idpr){
		var nom=$("#nom_p"+idpr).val();
		var des=$("#des_p"+idpr).val();
		if(strSpace(nom,2,100)){
			var guardar=true;
			if(des!=""){ if(!strSpace(des,2,200)){ guardar=false;alerta('Ingrese una observación válida','top','des_p'+idpr);}}
			if(guardar){
				var atrib=new FormData(); atrib.append('nom',nom); atrib.append('des',des); atrib.append('idpr',idpr);
				set('produccion/update_proceso',atrib,'NULL',true,'produccion/view_config',{},'contenido',false);
			}
		}else{
			alerta('Ingrese un nombre válido','top','nom_p'+idpr);
		}
		return false;
	}
	function alerta_proceso(idpr){
		modal("PROCESO: Eliminar","xs","5");
		addClick_v2('modal_ok_5','drop_proceso',"'"+idpr+"'");
		var atrib=new FormData();atrib.append('idpr',idpr);
		atrib.append('titulo','eliminar definitivamente el proceso: <b>'+$("#nom_p"+idpr).val()+'</b>');
		get('insumo/alerta',atrib,'content_5',true);
	}
	function drop_proceso(idpr){
		var atrib=new FormData();atrib.append('idpr',idpr);
		set('produccion/drop_proceso',atrib,'NULL',true,'produccion/view_config',{},'contenido',false,'5');
	}
	/*--- End manejo de procesos de las piezas ---*/
	/*--- Manejo de grupo en el producto---*/
	function save_producto_grupo(){
		var nom=$("#nom_prg").val();
		var des=$("#des_prg").val();
		if(strSpace(nom,2,100)){
			var guardar=true;
			if(des!=""){ if(!strSpace(des,2,200)){ guardar=false; alerta('Ingrese una observación válida','top','des_prg');}}
			if(guardar){
				var atrib=new FormData(); atrib.append('nom',nom); atrib.append('des',des);
				set('produccion/save_producto_grupo',atrib,'NULL',true,'produccion/view_config',{},'contenido',false);
			}
		}else{
			alerta('Ingrese un nombre válido','top','nom_prg');
		}
		return false;
	}
	function update_producto_grupo(idpg){
		var nom=$("#nom_prg"+idpg).val();
		var des=$("#des_prg"+idpg).val();
		if(strSpace(nom,2,100)){
			var guardar=true;
			if(des!=""){ if(!strSpace(des,2,200)){ guardar=false;alerta('Ingrese una observación válida','top','des_prg'+idpg);}}
			if(guardar){
				var atrib=new FormData();atrib.append('idpg',idpg);atrib.append('nom',nom);atrib.append('des',des);
				set('produccion/update_producto_grupo',atrib,'NULL',true,'produccion/view_config',{},'contenido',false);
			}
		}else{
			alerta('Ingrese un nombre válido','top','nom_prg'+idpg);
		}
		return false;
	}
	function alerta_producto_grupo(idpg){
		modal("PRODUCTO GRUPO: Eliminar","xs","5");
		addClick_v2('modal_ok_5','drop_producto_grupo',"'"+idpg+"'");
		var atrib=new FormData();atrib.append('idpg',idpg);
		atrib.append('titulo','eliminar definitivamente el grupo: <b>'+$("#nom_prg"+idpg).val()+'</b>');
		get('insumo/alerta',atrib,'content_5',true);
	}
	function drop_producto_grupo(idpg){
		var atrib=new FormData();atrib.append('idpg',idpg);
		set('produccion/drop_producto_grupo',atrib,'NULL',true,'produccion/view_config',{},'contenido',false,'5');
	}
	/*--- End manejo de grupo en el producto---*/
/*------- END MANEJO DE CONFIGURACION -------*/