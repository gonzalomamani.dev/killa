var x=$(document);
 x.ready(inicio);
 function inicio(){
	$("#comprobante").click(function(){get_2n('contabilidad/search_comprobante',{},'search',false,'contabilidad/view_comprobante',{},'contenido',true); activar('comprobante','Comprobantes','contabilidad?p=1');});
	$("#libro_mayor").click(function(){get_2n('contabilidad/search_mayor',{},'search',false,'contabilidad/view_mayor',{},'contenido',true); activar('libro_mayor','Libro mayor','contabilidad?p=2');});
	$("#sumas_saldos").click(function(){get_2n('contabilidad/search_sumas',{},'search',false,'contabilidad/view_sumas',{},'contenido',true); activar('sumas_saldos','Sumas y Saldos','contabilidad?p=3');});
	$("#costo_pv").click(function(){var atrib=new FormData();atrib.append('nro','0');get_2n('contabilidad/search_costo',{},'search',false,'contabilidad/view_estado',atrib,'contenido',true); activar('costo_pv','Estado de costo de produccion de los vendido','contabilidad?p=4');});
	$("#estado").click(function(){var atrib=new FormData();atrib.append('nro','1');get_2n('contabilidad/search_estado',{},'search',false,'contabilidad/view_estado',atrib,'contenido',true); activar('estado','Estado de resultados','contabilidad?p=5');});
	$("#balance").click(function(){var atrib=new FormData();atrib.append('nro','2');get_2n('contabilidad/search_balance',{},'search',false,'contabilidad/view_estado',atrib,'contenido',true); activar('balance','Balance general','contabilidad?p=6');});
	$("#plan").click(function(){get_2n('contabilidad/search_plan',{},'search',false,'contabilidad/view_plan',atrib,'contenido',true); activar('plan','Plan de cuentas','contabilidad?p=7');});
	$("#config").click(function(){get_2n('',{},'search',false,'contabilidad/view_config',atrib,'contenido',true); activar('config','Configuración contabilidad','contabilidad?p=10');});
}

function reset_all_view(id){
	//reset search plan cuentas
	if(id!="p_codigo"){ $("#p_codigo").val(""); }
	if(id!="p_cuenta"){ $("#p_cuenta").val(""); }

	if(id!="search_cod"){ $("#search_cod").val(""); }
	if(id!="search_nom"){ $("#search_nom").val(""); }
	if(id!="search_tipo"){ $("#search_tipo").val(""); }
	if(id!="fech_a"){ $("#fech_a").val(""); }
	if(id!="usu"){ $("#usu").val(""); }
	if(id!="alm"){ $("#alm").val(""); }
	if(id!="cod"){ $("#cod").val(""); }
	if(id!="nom"){ $("#nom").val(""); }
}
/*------- MANEJO DE COMPROBANTES -------*/
   	/*--- Buscador ---*/
   	function view_comprobante(){
   		var tip=$("#s_tip").val();
   		var fol=$("#s_fol").val();
   		var f1=$("#s_f1").val();
   		var f2=$("#s_f2").val();
   		var atrib=new FormData();
   		atrib.append('tip',tip);atrib.append('fol',fol);atrib.append('f1',f1);atrib.append('f2',f2);
   		get('contabilidad/view_comprobante',atrib,'contenido',true);
   	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	function all_comprobante(){
   		get_2n('contabilidad/search_comprobante',{},'search',false,'contabilidad/view_comprobante',{},'contenido',true);
   	}
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
 	function new_comprobante(){
 		modal("COMPROBANTE: Nuevo","lg","1");
 		btn_modal('save_comprobante',"",'',"",'1','lg'); 		
 		get("contabilidad/new_comprobante",{},'content_1',true);
 	}
	function refresh_nro_cmp(){
		var tip=$("#c_tip").val();
		var fec=$("#c_fec").val();
		if(tip=="E" || tip=="I" || tip=="T"){
			if(fecha(fec)){
				var atrib=new FormData();
				atrib.append('tip',tip);atrib.append('fec',fec);
	   			get('contabilidad/refresh_nro_cmp',atrib,'c_nro',false);
			}else{
				$("#c_nro").html("");
				alerta('Seleccione una fecha válida','top','c_fec');
			}
		}else{
			$("#c_nro").html("");
			alerta('Seleccione un tipo de comprobante','top','c_tip');
			
		}
   	}
   	function add_row_cuenta(){
		var rows=$("tbody#content_cuenta tr").length;
		if(rows>0){
			var max=0;
			$("tbody#content_cuenta tr td input[type=hidden]").each(function(idx,el){
				if($(el).val()>max){ max=$(el).val(); }
			});
			rows=max;
		}
 		var tip=$("#c_tip").val();
 		var fec=$("#c_fec").val();
 		var nro=$("#c_nro").html();
 		if(tip=="E" || tip=="I" || tip=="T"){
			if(fecha(fec)){
				if(entero(nro,0,10)){
			 		var atrib=new FormData();
				 	atrib.append('row',rows);
			 		get_append('contabilidad/add_row_cuenta',atrib,'content_cuenta',false);
				}else{
					alerta('Calcular el número de comprobante','top','c_nro');
				}
			}else{
				alerta('Seleccione una fecha válida','top','c_fec');
			}
		}else{
			alerta('Seleccione un tipo de comprobante','top','c_tip');
		}
	}
	function drop_row_cuenta(ele){
		var e=$(ele).parent();
		var e2=e.parent();
		var e2=e2.parent();
		e2.remove();
		var rows=$("tbody#content_cuenta tr").length;
		if(rows==0){
			$("#c_tip").removeAttr('disabled');
			$("#c_fec").removeAttr('disabled');
		}
		refresh_totales_cuentas();// para el caso de eliminacion de nueva y modificar compra
	}
	function refresh_totales_cuentas(){
		var t_deb=0;
		var t_hab=0;
		$("tbody#content_cuenta input[id*=deb]").each(function(idx,el){ t_deb+=$(el).val()*1; });
		$("tbody#content_cuenta input[id*=hab]").each(function(idx,el){ t_hab+=$(el).val()*1; });
		$("#t_deb").html(number_format(t_deb,2,'.',','));
		$("#t_hab").html(number_format(t_hab,2,'.',','));

	}
	function save_comprobante(){
			var nro=$("#c_nro").html()*1;
 			var tip=$("#c_tip").val();
 			var fec=$("#c_fec").val();
			var glo=$("#c_glo").val();
			if(entero(nro+"",0,10) && nro>0){
				if(tip=="E" || tip=="I" || tip=="T"){
					if(fecha(fec)){
						if(textarea(glo,0,200)){
							var rows=$("tbody#content_cuenta tr").length;
							if(rows>0){
								if(valida_tabla_cuentas()){
									var sw=true;
									var ctas="";
										$("tbody#content_cuenta tr td input[id*=pos]").each(function(idx,el){
											var i=$(el).val();
											if(sw){
												sw=false;
												ctas=$("#cta"+i).val()+"|"+$("#ref"+i).val()+"|"+$("#deb"+i).val()*1+"|"+$("#hab"+i).val()*1;
											}else{
												ctas+="[|]"+$("#cta"+i).val()+"|"+$("#ref"+i).val()+"|"+$("#deb"+i).val()*1+"|"+$("#hab"+i).val()*1;
											}
										});
									var atrib=new FormData();
									atrib.append('ctas',ctas);
									atrib.append('nro',nro);
									atrib.append('tip',tip);
									atrib.append('fec',fec);
									atrib.append('glo',glo);
   									var atrib3=new FormData();atrib3.append('tip',$("#s_tip").val());atrib3.append('fol',$("#s_fol").val());atrib3.append('f1',$("#s_f1").val());atrib3.append('f2',$("#s_f2").val());
									set('contabilidad/save_comprobante',atrib,'NULL',true,'contabilidad/view_comprobante',atrib3,'contenido',false,'1');
								}
							}else{
								alerta("Debe ingresar al menos una cuenta en el comprobante","top","content_cuenta");
							}
						}else{
							alerta("Ingrese una observacion con contenido válido","top","c_glo");
						}
					}else{
						alerta("Ingrese una fecha válida","top","c_fec");
					}
				}else{
					alerta("Seleccione un tipo de comprobante válido","top","c_tip");
				}
			}else{
				alerta("Número de comprobante no válido, pulse el boton actualizar","top","c_nro");
			}
			return false;
		}
		function valida_tabla_cuentas(){
			var control=true;
			$("tbody#content_cuenta tr td input[id*=pos]").each(function(idx,el){
				var i=$(el).val();
				if(entero($("#cta"+i).val(),0,10)){
					if(textarea($("#ref"+i).val(),0,200)){ 
						if(($("#deb"+i).val()*1)>=0){
							if(($("#hab"+i).val()*1)>=0){
								
							}else{
								alerta("Ingrese una valor válido mayor que cero...","top","hab"+i); 
								control=false;
							}
						}else{
							alerta("Ingrese una valor válido mayor que cero...","top","deb"+i); 
							control=false;
						}
					}else{
						alerta("Ingrese una descripcion válida...","top","ref"+i); 
						control=false;
					}
				}else{
					alerta("Seleccione una cuenta...","top","cta"+i); 
					control=false;
				}
			});
			return control;
		}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	function imprimir_comprobantes(json,fecha1,fecha2){
   		modal("COMPROBANTE: Configuracion de impresion","xlg","1");
 		btn_modal('',"",'imprimir',"'area'",'1','xlg');
 		var atrib=new FormData();atrib.append('json',json);atrib.append('fecha1',fecha1);atrib.append('fecha2',fecha2);
 		get_2n('contabilidad/imprimir_comprobantes',atrib,'content_1',true,'contabilidad/arma_comprobantes',atrib,'area',true);
   	}
   	function arma_comprobantes(json,fecha1,fecha2){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var v6=""; if($("#6:checked").val()){v6='ok'}
		var v7=""; if($("#7:checked").val()){v7='ok'}
		var nro=$("#nro").val();
		var atrib=new FormData();
		atrib.append('json',json);
		atrib.append('v1',v1);
		atrib.append('v2',v2);
		atrib.append('v3',v3);
		atrib.append('v4',v4);
		atrib.append('v5',v5);
		atrib.append('v6',v6);
		atrib.append('v7',v7);
		atrib.append('nro',nro);
		atrib.append('fecha1',fecha1);atrib.append('fecha2',fecha2);
		get('contabilidad/arma_comprobantes',atrib,'area',true);
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	function reportes_comprobante(idcm){
	 	modal("COMPROBANTE: Detalle","md","1");
	 	btn_modal('',"",'imprimir',"'area'",'1','md');
		var atrib=new FormData();
		atrib.append('idcm',idcm);
	 	get('contabilidad/reportes_comprobante',atrib,'content_1',true);
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	function config_comprobante(idcm){
	 	modal("COMPROBANTE: Configuración","lg","1");
	 	btn_modal('update_comprobante',"'"+idcm+"'",'',"",'1','lg');
		var atrib=new FormData();
		atrib.append('idcm',idcm);
		get("contabilidad/config_comprobante",atrib,'content_1',true);
   	}
   	function refresh_nro_cmp_up(idcm){
		var tip=$("#c_tip").val();
		var fec=$("#c_fec").val();
		if(tip=="E" || tip=="I" || tip=="T"){
			if(fecha(fec)){
				var atrib=new FormData();
				atrib.append('tip',tip);atrib.append('fec',fec);atrib.append('idcm',idcm);
	   			get('contabilidad/refresh_nro_cmp_up',atrib,'c_nro',false);
			}else{
				$("#c_nro").html("");
				alerta('Seleccione una fecha válida','top','c_fec');
			}
		}else{
			$("#c_nro").html("");
			alerta('Seleccione un tipo de comprobante','top','c_tip');
			
		}
   	}	
 	function update_comprobante(idcm){
			var nro=$("#c_nro").html()*1;
 			var tip=$("#c_tip").val();
 			var fec=$("#c_fec").val();
			var glo=$("#c_glo").val();
			if(entero(nro+"",0,10) && nro>0){
				if(tip=="E" || tip=="I" || tip=="T"){
					if(fecha(fec)){
						if(textarea(glo,0,200)){
							var rows=$("tbody#content_cuenta tr").length;
							if(rows>0){
								if(valida_tabla_cuentas()){
									var sw=true;
									var ctas="";
										$("tbody#content_cuenta tr td input[id*=pos]").each(function(idx,el){
											var i=$(el).val();
											if(sw){
												sw=false;
												ctas=$("#cta"+i).val()+"|"+$("#ref"+i).val()+"|"+$("#deb"+i).val()*1+"|"+$("#hab"+i).val()*1;
											}else{
												ctas+="[|]"+$("#cta"+i).val()+"|"+$("#ref"+i).val()+"|"+$("#deb"+i).val()*1+"|"+$("#hab"+i).val()*1;
											}
										});
									var atrib=new FormData();
									atrib.append('idcm',idcm);
									atrib.append('ctas',ctas);
									atrib.append('nro',nro);
									atrib.append('tip',tip);
									atrib.append('fec',fec);
									atrib.append('glo',glo);
   									var atrib3=new FormData();atrib3.append('tip',$("#s_tip").val());atrib3.append('fol',$("#s_fol").val());atrib3.append('f1',$("#s_f1").val());atrib3.append('f2',$("#s_f2").val());
									set('contabilidad/update_comprobante',atrib,'NULL',true,'contabilidad/view_comprobante',atrib3,'contenido',false,'1');
								}
							}else{
								alerta("Debe ingresar al menos una cuenta en el comprobante","top","content_cuenta");
							}
						}else{
							alerta("Ingrese una observacion con contenido válido","top","c_glo");
						}
					}else{
						alerta("Ingrese una fecha válida","top","c_fec");
					}
				}else{
					alerta("Seleccione un tipo de comprobante válido","top","c_tip");
				}
			}else{
				alerta("Número de comprobante no válido, pulse el boton actualizar","top","c_nro");
			}
			return false;
 	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	function alerta_comprobante(idcm,tipo,folio){
		modal("COMPROBANTE: Eliminar","xs","5");
		addClick_v2('modal_ok_5','drop_comprobante',"'"+idcm+"'");
		var atrib=new FormData();
		atrib.append('titulo','eliminar definitivamente el comprobante de tipo: <b>'+tipo+' con folio '+folio+'</b>');
		get('insumo/alerta',atrib,'content_5',true);
	}
	function drop_comprobante(idcm){
		var atrib=new FormData();atrib.append('idcm',idcm);
		var atrib3=new FormData();atrib3.append('tip',$("#s_tip").val());atrib3.append('fol',$("#s_fol").val());atrib3.append('f1',$("#s_f1").val());atrib3.append('f2',$("#s_f2").val());
		set('contabilidad/drop_comprobante',atrib,'NULL',true,'contabilidad/view_comprobante',atrib3,'contenido',false,'5');
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE COMPROBANTES -------*/
/*------- MANEJO DE LIBRO MAYOR -------*/
   	/*--- Buscador ---*/
   	function view_mayor(){
   		var f1=$("#f1").val();
   		var f2=$("#f2").val();
   		var atrib=new FormData();atrib.append('f1',f1);atrib.append('f2',f2);
   		get('contabilidad/view_mayor',atrib,'contenido',true);
   	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	function all_mayor(){
   		get_2n('contabilidad/search_mayor',{},'search',false,'contabilidad/view_mayor',{},'contenido',true);
   	}
   	/*--- End Ver Todo ---*/
   	/*--- Imprimir ---*/
   	function imprimir_mayor(json,fecha1,fecha2){
   		
   		modal("LIBRO MAYOR: Configuracion de impresion","lg","1");
 		btn_modal('',"",'imprimir',"'area'",'1','lg');
 		var atrib=new FormData();atrib.append('json',json);atrib.append('fecha1',fecha1);atrib.append('fecha2',fecha2);
 		get_2n('contabilidad/imprimir_mayor',atrib,'content_1',true,'contabilidad/arma_mayor',atrib,'area',true);
   	}
   	function arma_mayor(json,fecha1,fecha2){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var nro=$("#nro").val();
		var atrib=new FormData();
		atrib.append('json',json);
		atrib.append('v1',v1);
		atrib.append('v2',v2);
		atrib.append('v3',v3);
		atrib.append('v4',v4);
		atrib.append('v5',v5);
		atrib.append('nro',nro);
		atrib.append('fecha1',fecha1);
		atrib.append('fecha2',fecha2);
		get('contabilidad/arma_mayor',atrib,'area',true);
	}
   	/*--- End Imprimir ---*/
/*------- END MANEJO DE LIBRO MAYOR -------*/
/*------- MANEJO DE SUMAS Y SALDOS -------*/
   	/*--- Buscador ---*/
   	function view_sumas(){
   		var f1=$("#f1").val();
   		var f2=$("#f2").val();
   		var atrib=new FormData();atrib.append('f1',f1);atrib.append('f2',f2);
   		get('contabilidad/view_sumas',atrib,'contenido',true);
   	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	function all_sumas(){
   		get_2n('contabilidad/search_sumas',{},'search',false,'contabilidad/view_sumas',{},'contenido',true);
   	}
   	/*--- End Ver Todo ---*/
   	/*--- Imprimir ---*/
   	function imprimir_sumas(json,fecha1,fecha2){
   		modal("SUMAS Y SALDOS: Configuracion de impresion","lg","1");
 		btn_modal('',"",'imprimir',"'area'",'1','lg');
 		var atrib=new FormData();atrib.append('json',json);atrib.append('fecha1',fecha1);atrib.append('fecha2',fecha2);
 		get_2n('contabilidad/imprimir_sumas',atrib,'content_1',true,'contabilidad/arma_sumas',atrib,'area',true);
   	}
   	function arma_sumas(json,fecha1,fecha2){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var v6=""; if($("#6:checked").val()){v6='ok'}
		var v7=""; if($("#7:checked").val()){v7='ok'}
		var nro=$("#nro").val();
		var atrib=new FormData();
		atrib.append('json',json);
		atrib.append('v1',v1);
		atrib.append('v2',v2);
		atrib.append('v3',v3);
		atrib.append('v4',v4);
		atrib.append('v5',v5);
		atrib.append('v6',v6);
		atrib.append('v7',v7);
		atrib.append('nro',nro);
		atrib.append('fecha1',fecha1);
		atrib.append('fecha2',fecha2);
		get('contabilidad/arma_sumas',atrib,'area',true);
	}
   	/*--- End Imprimir ---*/
/*------- END MANEJO DE SUMAS Y SALDOS -------*/
/*------- ESTADO DE COSTO DE PRODUCCION DE LOS VENDIDO -------*/
   	/*--- Buscador ---*/
   	function view_estado(nro){
   		var f1=$("#f1").val(); var f2=$("#f2").val();
   		var atrib=new FormData();atrib.append('f1',f1);atrib.append('f2',f2);atrib.append('nro',nro);
   		get('contabilidad/view_estado',atrib,'contenido',true);
   	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	function all_estado(nro){
   		var atrib=new FormData();atrib.append('nro',nro);
   		get_2n('contabilidad/search_costo',{},'search',false,'contabilidad/view_estado',atrib,'contenido',true);
   	}
   	/*--- End Ver Todo ---*/
   	/*--- Imprimir ---*/
   	function imprimir_estado(fecha1,fecha2,nro_estado){
   		modal("SUMAS Y SALDOS: Configuracion de impresion","lg","1");
   		switch(nro_estado){
   			case '0': modal("ESTADO DE COSTO DE PRODUCCIÓN Y COSTO DE PRODUCCIÓN DE LO VENDIDO: Configuracion de impresion","lg","1");break;
   			case '1': modal("ESTADO DE RESULTADOS: Configuracion de impresion","lg","1");break;
   			case '2': modal("BALANCE GENERAL: Configuracion de impresion","lg","1");break;
   		}
 		btn_modal('',"",'imprimir',"'area'",'1','lg');
 		var atrib=new FormData();atrib.append('fecha1',fecha1);atrib.append('fecha2',fecha2);atrib.append('nro_estado',nro_estado);
 		get_2n('contabilidad/imprimir_estado',atrib,'content_1',true,'contabilidad/arma_estado',atrib,'area',true);
   	}
   	function arma_estado(fecha1,fecha2,nro_estado){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var v6=""; if($("#6:checked").val()){v6='ok'}
		var v7=""; if($("#7:checked").val()){v7='ok'}
		var nro=$("#nro").val();
		var atrib=new FormData();
		atrib.append('nro',nro);
		atrib.append('fecha1',fecha1);
		atrib.append('fecha2',fecha2);
		atrib.append('nro_estado',nro_estado);
		get('contabilidad/arma_costo',atrib,'area',true);
	}
   	/*--- End Imprimir ---*/
/*------- END ESTADO DE COSTO DE PRODUCCION DE LOS VENDIDO -------*/
/*------- MANEJO DE PLAN DE CUENTAS -------*/
   	/*--- Buscador ---*/
	function view_plan(){
		var codigo=$("#p_codigo").val();
		var cuenta=$("#p_cuenta").val();
		var ele=new FormData();
		ele.append('codigo',codigo);
		ele.append('cuenta',cuenta);
		get("contabilidad/view_plan",ele,'contenido',true);
		return false;
	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	function all_plan(){
   		reset_all_view("NULL");
		view_plan();
	}
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
   	function new_plan(){
		modal("PLAN DE CUENTAS: Nueva cuenta o grupo","lg","1");
		btn_modal('save_plan',"",'',"",'1','lg');
	 	get('contabilidad/new_plan',{},'content_1',true);
	}
	function save_plan(){
	 	var tip=$("#tip").val();
	 	var gru=$("#gru").val();
	 	var sub_gru=$("#sub_gru").val();
	 	var rub=$("#rub").val();
	 	var cue=$("#cue").val();
	 	var cue_text=$("#cue_text").val();
	 	if(entero(tip,0,1) && tip>=0 && tip<=1){
		 	if(entero(gru,0,1) && gru>=0 && gru<=9){
		 			if(strSpace(cue_text,5,150)){
		 				var control=true;
		 				if(sub_gru!=""){ if(!entero(sub_gru,0,1) || sub_gru<0 || sub_gru>9){ alerta("Ingrese un valor de sub grupos válido","top","sub_gru"); control=false;}}
		 				if(rub!=""){ if(!entero(rub,0,1) || rub<0 || rub>9){ alerta("Ingrese un valor de rubro válido","top","rub"); control=false;}}
		 				if(cue!=""){ if(!entero(cue,0,4) || cue<0 || cue>9999){ alerta("Ingrese un valor de cuenta válida","top","cue"); control=false;}}
		 				if(control){
		 					var atrib=new FormData();
		 					atrib.append('tip',tip);
		 					atrib.append('gru',gru);
						 	atrib.append('sub_gru',sub_gru);
						 	atrib.append('rub',rub);
						 	atrib.append('cue',cue);
						 	atrib.append('cue_text',cue_text);
						 	var atrib3=new FormData();atrib3.append('codigo',$("#p_codigo").val());atrib3.append('cuenta',$("#p_cuenta").val());
		 					set('contabilidad/save_plan',atrib,'NULL',true,'contabilidad/view_plan',atrib3,'contenido',false,'1');
		 				}
				 	}else{
				 		alerta("Ingrese un nombre de cuenta válido","top","cue_text");
				 	}
		 	}else{
		 		alerta("Ingrese un valor de grupo válido","top","gru");
		 	}
	 	}else{
	 		alerta("Seleccione un tipo de registro","top","tip");
	 	}
	 	return false;
	 }
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	function imprimir_plan(json){
   		modal("PLAN DE CUENTAS: Configuracion de impresion","xlg","1");
 		btn_modal('',"",'imprimir',"'area'",'1','xlg');
 		var atrib=new FormData();atrib.append('json',json);
 		get_2n('contabilidad/imprimir_plan',atrib,'content_1',true,'contabilidad/arma_plan',atrib,'area',true);
   	}
   	function arma_plan(json){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var nro=$("#nro").val();
		var atrib=new FormData();
		atrib.append('json',json);
		atrib.append('v1',v1);
		atrib.append('v2',v2);
		atrib.append('v3',v3);
		atrib.append('v4',v4);
		atrib.append('v5',v5);
		atrib.append('nro',nro);
		get('contabilidad/arma_plan',atrib,'area',true);
	}
   	/*--- End Imprimir ---*/
   	/*-- Configuracion de botones de gestos y ventas ---*/
   	function change_estado(idpl,tipo){
   		var atrib=new FormData();atrib.append('idpl',idpl);atrib.append('tipo',tipo);
   		var atrib3=new FormData();atrib3.append('codigo',$("#p_codigo").val());atrib3.append('cuenta',$("#p_cuenta").val());
   		set('contabilidad/change_estado',atrib,'NULL',true,'contabilidad/view_plan',atrib3,'contenido',false,'1');
   	}
   	/*-- End Configuracion de botones de gestos y ventas ---*/
   	/*--- configuracion ---*/
   	function config_plan(idpl){
		modal("PLAN DE CUENTAS: Configuración","lg","1");
		btn_modal('update_plan',"'"+idpl+"'",'',"",'1','lg');
		var atrib=new FormData();
		atrib.append('idpl',idpl);
	 	get('contabilidad/config_plan',atrib,'content_1',true);
	}
	function update_plan(idpl){
	 	var tip=$("#tip").val();
	 	var gru=$("#gru").val();
	 	var sub_gru=$("#sub_gru").val();
	 	var rub=$("#rub").val();
	 	var cue=$("#cue").val();
	 	var cue_text=$("#cue_text").val();
	 	if(entero(tip,0,1) && tip>=0 && tip<=1){
		 	if(entero(gru,0,1) && gru>=0 && gru<=9){
		 			if(strSpace(cue_text,5,150)){
		 				var control=true;
		 				if(sub_gru!=""){ if(!entero(sub_gru,0,1) || sub_gru<0 || sub_gru>9){ alerta("Ingrese un valor de sub grupos válido","top","sub_gru"); control=false;}}
		 				if(rub!=""){ if(!entero(rub,0,1) || rub<0 || rub>9){ alerta("Ingrese un valor de rubro válido","top","rub"); control=false;}}
		 				if(cue!=""){ if(!entero(cue,0,4) || cue<0 || cue>9999){ alerta("Ingrese un valor de cuenta válida","top","cue"); control=false;}}
		 				if(control){
		 					var atrib=new FormData();
		 					atrib.append('idpl',idpl);
		 					atrib.append('tip',tip);
		 					atrib.append('gru',gru);
						 	atrib.append('sub_gru',sub_gru);
						 	atrib.append('rub',rub);
						 	atrib.append('cue',cue);
						 	atrib.append('cue_text',cue_text);
						 	var atrib3=new FormData();atrib3.append('codigo',$("#p_codigo").val());atrib3.append('cuenta',$("#p_cuenta").val());
		 					set('contabilidad/update_plan',atrib,'NULL',true,'contabilidad/view_plan',atrib3,'contenido',false,'1');
		 				}
				 	}else{
				 		alerta("Ingrese un nombre de cuenta válido","top","cue_text");
				 	}
		 	}else{
		 		alerta("Ingrese un valor de grupo válido","top","gru");
		 	}
	 	}else{
	 		alerta("Seleccione un tipo de registro","top","tip");
	 	}
	 	return false;
	 }
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	function confirmar_plan(idpl){
		modal("PLAN DE CUENTAS: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','drop_plan',"'"+idpl+"'");
   		var atrib=new FormData();atrib.append('idpl',idpl);
   		get('contabilidad/confirmar_plan',atrib,'content_5',true);
	}
	function drop_plan(idpl){
		var atrib=new FormData();
		atrib.append('u',$("#e_user").val());
		atrib.append('p',$("#e_password").val());
		atrib.append('idpl',idpl);
		var atrib3=new FormData();atrib3.append('codigo',$("#p_codigo").val());atrib3.append('cuenta',$("#p_cuenta").val());
		set('contabilidad/drop_plan',atrib,'NULL',true,'contabilidad/view_plan',atrib3,'contenido',false,'5');
	}

   	/*--- End Eliminar ---*/
/*------- END MANEJO DE PLAN DE CUENTAS -------*/
/*------- MANEJO DE CONFIGURACION -------*/
   	/*--- Manejo de cuentas de Egreso---*/
   	function save_cuenta_egreso(){
   		var cue=$("#eg_cue").val();
   		var por=$("#eg_por").val();
   		var tip=$("#eg_tip").val();
   		if(entero(cue,0,10)){
   			if(decimal(por,3,2) && por>0 && por<=100){
   				if(tip=="H" || tip=="D"){
   					var atrib=new FormData();atrib.append('cue',cue);atrib.append('por',por);atrib.append('tip',tip);
   					set('contabilidad/save_cuenta_egreso',atrib,'NULL',true,'contabilidad/view_config',{},'contenido',false);
   				}else{
   					alerta("Seleccione un tipo válido","top","eg_tip");
   				}
   			}else{
   				alerta("Ingrese un valor válido","top","eg_por");
   			}
   		}else{
   			alerta("Seleccione una cuenta válida","top","eg_cue");
   		}
   		return false;
   	}
   	function update_cuenta_egreso(idci){
   		var pos=$("#eg_pos"+idci).val();
   		var cue=$("#eg_cue"+idci).val();
   		var por=$("#eg_por"+idci).val();
   		var tip=$("#eg_tip"+idci).val();
   		if(entero(pos,0,10)){
   			if(entero(cue,0,10) || cue=="block"){
   				if(decimal(por,3,2) && por>0 && por<=100){
   					if(tip=="H" || tip=="D"){
   						var atrib=new FormData();
   						atrib.append('idci',idci);atrib.append('pos',pos);atrib.append('cue',cue);atrib.append('por',por);atrib.append('tip',tip);
   						set('contabilidad/update_cuenta_egreso',atrib,'NULL',true,'contabilidad/view_config',{},'contenido',false);
   					}else{
   						alerta("Seleccione un tipo válido","top","eg_tip"+idci);
   					}
   				}else{
   					alerta("Seleccione un valor válida","top","eg_por"+idci);
   				}
   			}else{
   				alerta("Seleccione una cuenta válida","top","eg_cue"+idci);
   			}
   		}else{
   			alerta("Seleccione una posicion válida","top","eg_pos"+idci);
   		}
   		return false;
   	}
   	function alerta_egreso(idci,nom,por) {
		modal("ESTRUCTURA COMPRA O EGRESO: Eliminar","xs","5");
		addClick_v2('modal_ok_5','drop_egreso',"'"+idci+"'");
		var atrib=new FormData();atrib.append('idci',idci);
		atrib.append('titulo','eliminar definitivamente la cuenta: <b>'+nom+' '+por+'%</b>');
		get('insumo/alerta',atrib,'content_5',true);
	}
	function drop_egreso(idci){
		var atrib=new FormData();atrib.append('idci',idci);
		set('contabilidad/drop_egreso',atrib,'NULL',true,'contabilidad/view_config',{},'contenido',false,'5');
	}
   	/*--- End Manejo cuentas de Egreso  ---*/
   	/*--- Manejo de cuentas de Ingreso---*/
   	function save_cuenta_ingreso(){
   		var cue=$("#in_cue").val();
   		var por=$("#in_por").val();
   		var tip=$("#in_tip").val();
   		if(entero(cue,0,10)){
   			if(decimal(por,3,2) && por>0 && por<=100){
   				if(tip=="H" || tip=="D"){
   					var atrib=new FormData();atrib.append('cue',cue);atrib.append('por',por);atrib.append('tip',tip);
   					set('contabilidad/save_cuenta_ingreso',atrib,'NULL',true,'contabilidad/view_config',{},'contenido',false);
   				}else{
   					alerta("Seleccione un tipo válido","top","in_tip");
   				}
   			}else{
   				alerta("Ingrese un valor válido","top","in_por");
   			}
   		}else{
   			alerta("Seleccione una cuenta válida","top","in_cue");
   		}
   		return false;
   	}
   	function update_cuenta_ingreso(idci){
   		var pos=$("#in_pos"+idci).val();
   		var cue=$("#in_cue"+idci).val();
   		var por=$("#in_por"+idci).val();
   		var tip=$("#in_tip"+idci).val();
   		if(entero(pos,0,10)){
   			if(entero(cue,0,10) || cue=="block"){
   				if(decimal(por,3,2) && por>0 && por<=100){
   					if(tip=="H" || tip=="D"){
   						var atrib=new FormData();
   						atrib.append('idci',idci);atrib.append('pos',pos);atrib.append('cue',cue);atrib.append('por',por);atrib.append('tip',tip);
   						set('contabilidad/update_cuenta_ingreso',atrib,'NULL',true,'contabilidad/view_config',{},'contenido',false);
   					}else{
   						alerta("Seleccione un tipo válido","top","in_tip"+idci);
   					}
   				}else{
   					alerta("Seleccione un valor válida","top","in_por"+idci);
   				}
   			}else{
   				alerta("Seleccione una cuenta válida","top","in_cue"+idci);
   			}
   		}else{
   			alerta("Seleccione una posicion válida","top","in_pos"+idci);
   		}
   		return false;
   	}
   	function alerta_ingreso(idci,nom,por) {
		modal("ESTRUCTURA VENTA O INGRESO: Eliminar","xs","5");
		addClick_v2('modal_ok_5','drop_ingreso',"'"+idci+"'");
		var atrib=new FormData();atrib.append('idci',idci);
		atrib.append('titulo','eliminar definitivamente la cuenta: <b>'+nom+' '+por+'%</b>');
		get('insumo/alerta',atrib,'content_5',true);
	}
	function drop_ingreso(idci){
		var atrib=new FormData();atrib.append('idci',idci);
		set('contabilidad/drop_ingreso',atrib,'NULL',true,'contabilidad/view_config',{},'contenido',false,'5');
	}
   	/*--- End Manejo cuentas de Ingreso ---*/
   	/*--- Manejo  config. estado de costo de produccion y prod. de los vend.  ---*/
   	function new_group_est(nro_estado){
   		var t="";
   		switch(nro_estado){
   			case '0': t="NUEVO GRUPO: Costo de Prod. y Prod. Vend."; break;
   			case '1': t="NUEVO GRUPO: Estado de resultados"; break;
   			case '2': t="NUEVO GRUPO: Balance General"; break;
   		}
		modal(t,"md","1");
		btn_modal('save_group_est',"'"+nro_estado+"'",'',"",'1','md');
		var atrib=new FormData();
		atrib.append('nro',nro_estado);
	 	get('contabilidad/new_group_est',atrib,'content_1',true);
	}
	function add_row_cuenta_epv(btn){
		var rows=$("tbody#content_cuentas tr").length;
		if(rows>0){
			var max=0;
			$("tbody#content_cuentas tr td input[type=hidden]").each(function(idx,el){
				if($(el).val()>max){ max=$(el).val(); }
			});
			rows=max;
		}
 		var atrib=new FormData();
	 	atrib.append('row',rows);
 		get_append_disabled('contabilidad/add_row_cuenta_epv',atrib,'content_cuentas',false,btn);
	}
	function add_row_grupo_epv(nro_estado,btn){
		var rows=$("tbody#content_operacion tr").length;
		if(rows>0){
			var max=0;
			$("tbody#content_operacion tr td input[type=hidden]").each(function(idx,el){
				if($(el).val()>max){ max=$(el).val(); }
			});
			rows=max;
		}
 		var atrib=new FormData();
	 	atrib.append('row',rows);atrib.append('nro',nro_estado);
 		get_append_disabled('contabilidad/add_row_grupo_epv',atrib,'content_operacion',false,btn);
	}
	function drop_fila(ele){
		var e=$(ele).parent();
		var e2=e.parent();
		var e2=e2.parent();
		e2.remove();
	}
	function save_group_est(nro){
		var sig=$("#signo").val();
		var sv=$("#sig_vis").prop('checked');
		var nom=$("#nom_cta_group").val();
		var nv=$("#nom_vis").prop('checked');
		var col=$("input:radio[name=co]:checked").val();
		var rows_cu=$("tbody#content_cuentas tr").length;
		var rows_co=$("tbody#content_operacion tr").length;
		if(entero(sig,0,1) && sig>=0 && sig<=1){
			if(sv==true || sv==false){
				if(strSpace(nom,0,100)){
					if(nv==true || nv==false){
						if(entero(col,0,1) && col>=0 && col<=2){
							//if(rows_cu>0){
								if(valida_content_cuentas() && valida_content_operacion()){
									var ctas="";var sw=true;
									$("tbody#content_cuentas tr td input[type=hidden]").each(function(idx,el){
										var i=$(el).val();
										if(sw){
											ctas=$("#cta"+i).val();sw=false;
										}else{
											ctas+="|"+$("#cta"+i).val();
										}
									});
									var ops="";var sw=true;
									$("tbody#content_operacion tr td input[type=hidden]").each(function(idx,el){
										var i=$(el).val();
										if(sw){
											ops=$("#gru_sig"+i).val()+"|"+$("#grupo"+i).val();sw=false;
										}else{
											ops+="[|]"+$("#gru_sig"+i).val()+"|"+$("#grupo"+i).val();
										}
									});
									//alert(ops);
									var atrib=new FormData();
									atrib.append('nro',nro);
									atrib.append('sig',sig);
									atrib.append('sv',sv);
									atrib.append('nom',nom);
									atrib.append('nv',nv);
									atrib.append('col',col);
									atrib.append('ctas',ctas);
									atrib.append('ops',ops);
									if(nro==1){ atrib.append('ecpv',$("#ecpv").prop('checked')); }
									set('contabilidad/save_group_est',atrib,'NULL',true,'contabilidad/view_config',{},'contenido',false,'1');
								}
							/*}else{
								alerta("Ingrese al menos una cuenta","top","content_cuentas");
							}*/
						}else{
							msj("fail");
						}
					}else{
						alerta("Error en la variable, actualice la pagina.","top","nom_vis");
					}
				}else{
					alerta("Ingrese un contenido válido","top","nom_cta_group");	
				}
			}else{
				alerta("Error en la variable, actualice la pagina.","top","sig_vis");
			}
		}else{
			alerta("Seleccione un opcion válida","top","signo");
		}
	}
	function mod_group_est(idee,nro_estado){
   		var t="";
		switch(nro_estado){
   			case '0': t="MODIFICAR GRUPO: Costo de Prod. y Prod. Vend."; break;
   			case '1': t="MODIFICAR GRUPO: Estado de resultados"; break;
   			case '2': t="MODIFICAR GRUPO: Balance General"; break;
   		}
		modal(t,"md","1");
		btn_modal('update_group_est',"'"+idee+"','"+nro_estado+"'",'',"",'1','md');
		var atrib=new FormData(); atrib.append('idee',idee);atrib.append('nro',nro_estado);
	 	get('contabilidad/mod_group_est',atrib,'content_1',true);
	}
	function update_group_est(idee,nro_estado){
		var sig=$("#signo").val();
		var sv=$("#sig_vis").prop('checked');
		var nom=$("#nom_cta_group").val();
		var nv=$("#nom_vis").prop('checked');
		var col=$("input:radio[name=co]:checked").val();
		var rows_cu=$("tbody#content_cuentas tr").length;
		var rows_co=$("tbody#content_operacion tr").length;
		if(entero(sig,0,1) && sig>=0 && sig<=1){
			if(sv==true || sv==false){
				if(strSpace(nom,0,100)){
					if(nv==true || nv==false){
						if(entero(col,0,1) && col>=0 && col<=2){
							//if(rows_cu>0){
								if(valida_content_cuentas() && valida_content_operacion()){
									var ctas="";var sw=true;
									$("tbody#content_cuentas tr td input[type=hidden]").each(function(idx,el){
										var i=$(el).val();
										if(sw){
											ctas=$("#cta"+i).val();sw=false;
										}else{
											ctas+="|"+$("#cta"+i).val();
										}
									});
									var ops="";var sw=true;
									$("tbody#content_operacion tr td input[type=hidden]").each(function(idx,el){
										var i=$(el).val();
										if(sw){
											ops=$("#gru_sig"+i).val()+"|"+$("#grupo"+i).val();sw=false;
										}else{
											ops+="[|]"+$("#gru_sig"+i).val()+"|"+$("#grupo"+i).val();
										}
									});
									//alert(ctas);
									var atrib=new FormData();
									atrib.append('idee',idee);
									atrib.append('nro',nro_estado);
									atrib.append('sig',sig);
									atrib.append('sv',sv);
									atrib.append('nom',nom);
									atrib.append('nv',nv);
									atrib.append('col',col);
									atrib.append('ctas',ctas);
									atrib.append('ops',ops);
									if(nro_estado==1){ atrib.append('ecpv',$("#ecpv").prop('checked')); }
									set('contabilidad/update_group_est',atrib,'NULL',true,'contabilidad/view_config',{},'contenido',false,'1');
								}
							/*}else{
								alerta("Ingrese al menos una cuenta","top","content_cuentas");
							}*/
						}else{
							msj("fail");
						}
					}else{
						alerta("Error en la variable, actualice la pagina.","top","nom_vis");
					}
				}else{
					alerta("Ingrese un contenido válido","top","nom_cta_group");	
				}
			}else{
				alerta("Error en la variable, actualice la pagina.","top","sig_vis");
			}
		}else{
			alerta("Seleccione un opcion válida","top","signo");
		}
	}
	function valida_content_cuentas(){
		var control=true;
		$("tbody#content_cuentas tr td input[type=hidden]").each(function(idx,el){
			var i=$(el).val();
			if(!entero($("#cta"+i).val(),0,10)){ alerta("Seleccione una cuenta...","top","cta"+i); control=false;}
		});
		return control;
	}
	function valida_content_operacion(){
		var control=true;
		$("tbody#content_operacion tr td input[type=hidden]").each(function(idx,el){
			var i=$(el).val();
			if(!entero($("#gru_sig"+i).val(),0,1) || $("#gru_sig"+i).val()<0 || $("#gru_sig"+i).val()>1){ alerta("Seleccione un signo válido...","top","gru_sig"+i); control=false;}
			if(!entero($("#grupo"+i).val(),0,10)){ alerta("Seleccione una cuenta válida...","top","grupo"+i); control=false;}
		});
		return control;
	}
   	function alerta_epv(idee,nom) {
		modal("Costo de Prod. y Prod. Vend.: Eliminar","xs","5");
		addClick_v2('modal_ok_5','drop_epv',"'"+idee+"'");
		var atrib=new FormData();atrib.append('idee',idee);
		atrib.append('titulo','eliminar definitivamente el grupo del estado: <b>'+nom+'</b>');
		get('insumo/alerta',atrib,'content_5',true);
	}
	function drop_epv(idee){
		var atrib=new FormData();atrib.append('idee',idee);
		set('contabilidad/drop_epv',atrib,'NULL',true,'contabilidad/view_config',{},'contenido',false,'5');
	}
























	function nro_cta_plan(row){
		var cta=$("#cta"+row).val();
		if(cta!=""){
			var atrib=new FormData();atrib.append('cta',cta)
			ajax_get('contabilidad/nro_cta_plan',atrib,"n_cta"+row);
		}else{
			$("#n_cta"+row).html("");
		}
	}


	function adicionar_row_grupo(nro_estado){
 		if(cliente!=""){
 			var atrib=new FormData();
	 		atrib.append('row',$("tbody#content_operacion tr").length);atrib.append('nro',nro_estado);
 			ajax_get_append('contabilidad/adicionar_row_grupo',atrib,'content_operacion');
 		}else{
 			ver_alerta("alerta_fail","Seleccione un cliente...");
 		}
	}
	function drop_row(id){
 	 	var ele;
 	 	$("tbody#"+id+" tr").each(function(idx,el){
			ele=el;
		});
		$(ele).remove();
	}



	function delete_group_est0(id){
		if(window.confirm("¿Desea eliminar el grupo?")){
			var atrib=new FormData();atrib.append('id',id);
			ajax_set('contabilidad/delete_group_est0',atrib,'NULL','contabilidad/view_configuracion',{},'contenido','NULL');
		}	
	}
   	/*--- End Manejo  config. estado de costo de produccion y prod. de los vend.  ---*/
/*------- END MANEJO DE CONFIGURACION -------*/



































/*------- MANEJO DE  CONFIGURACIONES -------*/
   	/*--- Manejo de grupo de cuentas  ---*/
   	function form_save_grupo(){
   		var cod=$("#cod_grupo").val();
   		var nom=$("#nom_grupo").val();
   		var ele=new FormData();
   		ele.append('cod',cod);
   		ele.append('nom',nom);
   		ajax_set('contabilidad/save_grupo',ele,'NULL','contabilidad/view_configuracion',{},'contenido','NULL')
   	}
   	function update_grupo(idgru){
   		var cod=$("#cod_grupo"+idgru).val();
   		var nom=$("#nom_grupo"+idgru).val();
   		var ele=new FormData();
   		ele.append('id',idgru);
   		ele.append('cod',cod);
   		ele.append('nom',nom);
   		ajax_set('contabilidad/update_grupo',ele,'NULL','contabilidad/view_configuracion',{},'contenido','NULL')
   	}
   	function drop_grupo(idgru){
   		var ele=new FormData();
   		ele.append('id',idgru);
   		ajax_set('contabilidad/drop_grupo',ele,'NULL','contabilidad/view_configuracion',{},'contenido','NULL')
   	}
   	/*--- End Manejo de grupo de cuentas  ---*/



/*------- END MANEJO DE CONFIGURACIONES -------*/



 /*funciones encargadas del PAE*/
function view_planilla(ide){
	var f1=$('#f1').val();
	var f2=$('#f2').val();
	if(f1!="" && f2!=""){
	 	visible('contenido_modal','modal','1%','95%','85%');
	 	post_get('contabilidad/view_planilla_pae',{f1:f1,f2:f2,ide:ide},'modal');
	}else{
		alert("Seleccione ambas fechas")	;
	}

}
 function view_horas_biometrico(ide){
 	var f1=$('#f1').val();
	var f2=$('#f2').val();
 	visible('contenido_modal','modal','1%','95%','85%');
 	post_get('contabilidad/view_horas_biometrico_pae',{f1:f1,f2:f2,ide:ide},'modal');
 }
 function estado_material(){
 	visible('contenido_modal','modal','1%','95%','85%');
 	post_get('contabilidad/imprimir_estado_material',{},'modal');
 }
  function estado_producto(){
 	visible('contenido_modal','modal','1%','95%','85%');
 	post_get('contabilidad/imprimir_estado_producto',{},'modal');
 }
 function datos_personales(){
 	visible('contenido_modal','modal','1%','95%','85%');
 	post_get('contabilidad/imprimir_personal',{},'modal');
 }
 function view_planilla_sueldos(){
 	visible('contenido_modal','modal','1%','95%','85%');
 	post_get('contabilidad/view_planilla_sueldos',{},'modal');
 }

 function planilla_sueldos(){
 	var f1=$("#f1").val();
 	var f2=$("#f2").val();
 	if(f1!="" & f1!=null & f2!="" & f2!=null){
 		post_get('contabilidad/planilla_sueldos',{f1:f1,f2:f2},'sub_result');
 	}else{
 		msj("selecciones las fechas por favor","alert_info","modal_alert");
 	}
 }



