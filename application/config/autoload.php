<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| AUTO-LOADER
| -------------------------------------------------------------------
| This file specifies which systems should be loaded by default.
|
| In order to keep the framework as light-weight as possible only the
| absolute minimal resources are loaded by default. For example,
| the database is not connected to automatically since no assumption
| is made regarding whether you intend to use it.  This file lets
| you globally define which systems you would like loaded with every
| request.
|
| -------------------------------------------------------------------
| Instructions
| -------------------------------------------------------------------
|
| These are the things you can load automatically:
|
| 1. Packages
| 2. Libraries
| 3. Helper files
| 4. Custom config files
| 5. Language files
| 6. Models
|
*/

/*
| -------------------------------------------------------------------
|  Auto-load Packges
| -------------------------------------------------------------------
| Prototype:
|
|  $autoload['packages'] = array(APPPATH.'third_party', '/usr/local/shared');
|
*/

$autoload['packages'] = array();


/*
| -------------------------------------------------------------------
|  Auto-load Libraries
| -------------------------------------------------------------------
| These are the classes located in the system/libraries folder
| or in your application/libraries folder.
|
| Prototype:
|
|	$autoload['libraries'] = array('database', 'session', 'xmlrpc');
*/

$autoload['libraries'] = array('database','session','validaciones','val','lib','conta','resize','excel');


/*
| -------------------------------------------------------------------
|  Auto-load Helper Files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['helper'] = array('url', 'file');
*/

$autoload['helper'] = array('url','form');


/*
| -------------------------------------------------------------------
|  Auto-load Config files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['config'] = array('config1', 'config2');
|
| NOTE: This item is intended for use ONLY if you have created custom
| config files.  Otherwise, leave it blank.
|
*/

$autoload['config'] = array();


/*
| -------------------------------------------------------------------
|  Auto-load Language files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['language'] = array('lang1', 'lang2');
|
| NOTE: Do not include the "_lang" part of your file.  For example
| "codeigniter_lang.php" would be referenced as array('codeigniter');
|
*/

$autoload['language'] = array();


/*
| -------------------------------------------------------------------
|  Auto-load Models
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['model'] = array('model1', 'model2');
|
*/

//$autoload['model'] = array('M_producto_material','M_material','M_proveedor','M_insumo_proveedor','M_movimiento_insumo','M_producto_proceso','M_proceso','M_pedido_producto_proceso_empleado','M_detalle_pedido','M_pedido','M_cliente','M_almacen_producto','M_anticipo','M_archivo_biometrico','M_feriado','M_tipo_contrato','M_planilla_sueldo','M_persona','M_empleado','M_almacen','M_usuario','M_insumo','M_grupo','M_unidad','M_color','M_producto','M_producto_insumo','M_imagen_producto');
$autoload['model'] = array('M_activo_fijo','M_almacen','M_almacen_material','M_almacen_producto','M_anticipo','M_archivo_biometrico','M_ayudante','M_categoria_pieza','M_categoria_producto','M_ciudad','M_cliente','M_color','M_color_producto','M_comprobante','M_cuentas_comprobante','M_depreciacion','M_descuento','M_detalle_comprobante','M_detalle_gasto','M_detalle_pedido','M_gasto','M_empleado','M_empleado_proceso','M_empleado_producto','M_estado_cuentas','M_estado_estructura','M_estado_operacion','M_feriado','M_grupo','M_grupo_cuenta','M_hora_biometrico','M_imagen_producto','M_material','M_material_adicional','M_material_item','M_material_liquido','M_material_proveedor','M_material_varios','M_movimiento_almacen','M_pago','M_pais','M_pedido','M_pedido_tarea','M_persona','M_pieza','M_pieza_grupo','M_pieza_material_liquido','M_pieza_proceso','M_plan_cuenta','M_privilegio','M_proceso','M_producto','M_producto_cliente','M_producto_grupo','M_producto_insumo','M_producto_material','M_producto_material_adicional','M_producto_proceso','M_proveedor','M_sub_pedido','M_tipo_contrato','M_unidad','M_utilidad_venta','M_usuario','M_venta','M_configuracion');


/* End of file autoload.php */
/* Location: ./application/config/autoload.php */