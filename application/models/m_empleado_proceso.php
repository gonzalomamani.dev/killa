<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_empleado_proceso extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('empleado_proceso');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('empleado_proceso',['idep' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM empleado_proceso WHERE idep='$id'");
		return $query->result();
	}
	function get_empleado_proceso($idpr){
		$col="p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
			e.ide,e.idtc,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.tipo,
			ep.idep,ep.idpr,ep.tipo,ep.fecha";
		$this->db->select($col);
		$this->db->from("empleado_proceso ep");
		$this->db->order_by("p.nombre", "asc");
		$this->db->where("ep.idpr = '$idpr'");
		$this->db->join('empleado e','ep.ide = e.ide','inner');
		$this->db->join("persona p","e.ci=p.ci","inner");
		$query=$this->db->get();
		return $query->result();
	}
	function get_proceso($ide){
		$col="ep.idep,ep.ide,ep.tipo,ep.fecha,
			pr.idpr,pr.nombre,pr.detalle";
		$this->db->select($col);
		$this->db->from("empleado_proceso ep");
		$this->db->order_by("pr.nombre", "asc");
		$this->db->where("ep.ide = '$ide'");
		$this->db->join('proceso pr','pr.idpr = ep.idpr','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($ide,$idpr,$tipo,$fecha){
		$datos=array(
			'ide' => $ide,
			'idpr' => $idpr,
			'tipo' => $tipo,
			'fecha' => $fecha
		);
		if($this->db->insert('empleado_proceso',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idpr,$tipo,$fecha){
		$datos=array(
			'idpr' => $idpr,
			'tipo' => $tipo,
			'fecha' => $fecha
		);
		if($this->db->update('empleado_proceso',$datos,array('idep' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('empleado_proceso',['idep' => $id])){
			return true;
		}else{
			return false;
		}
	}
}

/* End of file m_empleado_proceso.php */
/* Location: ./application/models/m_empleado_proceso.php*/