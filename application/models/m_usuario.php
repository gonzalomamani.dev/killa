<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_usuario extends CI_Model {
	function __construct(){
        parent::__construct();
    }
    function get_all(){
         $cols="p.ci,p.ciudad,p.nombre,p.nombre2,p.paterno,p.materno,p.fotografia,p.cargo,p.fecha_nac,
          u.idusu,u.usuario,u.password";
        $this->db->select($cols);
        $this->db->from("usuario u");
        $this->db->order_by("p.nombre", "asc");
        $this->db->join('persona p','u.ci = p.ci','inner');
        $query=$this->db->get();
        return $query->result();
   	}
    function get_id($idusu){
    	$query=$this->db->get_where('usuario',['idusu'=>$idusu]);
    	return $query->result();
   	}
   	function get_col($ci,$col){
   		$query=$this->db->query("SELECT $col FROM usuario WHERE ci='$ci'");
      return $query->result();
   	}
    function get_col_val($col,$val){
      $query=$this->db->get_where('usuario',[$col=>$val]);
      return $query->result();
    }
   	
    function get_where($cond,$val){
      $this->db->select("*");
      $this->db->from("usuario");
      $this->db->order_by("usuario.idusu", "asc");
      $this->db->where("usuario.$cond like '$val%'");
      $query=$this->db->get();
      return $query->result();
    }
   	function insertar($ci,$nombre,$nombre2,$paterno,$materno,$cargo,$fotografia){
      $salt="$2a$10$".sha1($ci)."$";
      $password=crypt($ci,$salt);
   		 $data = array(
           'ci' => $ci,
           'nombre' => $nombre,
           'nombre2' => $nombre2,
           'paterno' => $paterno,
           'materno'=>$materno,
           'cargo' => $cargo,
           'fotografia'=>$fotografia,
           'password' => $password
        );
       if ($this->db->insert('usuario', $data)){
          echo "ok";
       }else{
          echo "error";
       }
   	}
   	
    public function modificar_datos($ci,$nombre,$paterno,$materno,$cargo,$fotografia){
    	$data = array(
           'ci' => $ci,
           'nombres' => $nombre,
           'paterno' => $paterno,
           'materno'=>$materno,
           'cargo' => $cargo,
           'fotografia'=>$fotografia,
        );
        $this->db->update('usuario', $data, array('ci' => $ci));
    }
    public function modificar_logeo($ci,$usuario,$password){
    	$data = array(
    		'usuario' => $usuario,
    		'password' => $password
    	);
    	$this->db->update('usuario',$data,array('ci' => $ci));
    }
    public function eliminar($id){
        $this->db->delete('usuario',['ci'=>$id]);
    }
    public function validate($login,$pass){
    	$salt="$2a$10$".sha1($pass)."$";
    	$password=crypt($pass,$salt);
      $this->db->select("*");
      $this->db->from("usuario");
      $this->db->where("usuario collate utf8_bin ='$login' and password='$password'");
      $query=$this->db->get();
      return $query->result();
    	//$query=$this->db->get_where('usuario',['usuario '=>$login,'password'=>$password]);
    	//return $query->result();
    }
    public function validate_id($id,$login,$pass){
    	$salt="$2a$10$".sha1($pass)."$";
    	$password=crypt($pass,$salt);
    	$query=$this->db->get_where('usuario',['idusu'=>$id,'usuario'=>$login,'password'=>$password]);
    	return $query->result();
    }
}

/* End of file m_usuario.php */
/* Location: ./application/models/m_usuario.php */