<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_activo_fijo extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('activo_fijo');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('activo_fijo',['idaf' => $id]);
		return $query->result();
	}
	function get_depreciacion($idaf){// en uso, ACTIVOS FIJOS,
		$col="mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
				a.idaf,a.costo,a.fecha_inicio_trabajo,
				d.iddpre,d.nombre as nombre_depreciacion,d.anio,d.porcentaje,d.observaciones as observaciones_deprecicion";
		$this->db->select($col);
		$this->db->from("activo_fijo a");
		$this->db->order_by("mi.nombre", "asc");
		$this->db->where("a.idaf = $idaf");
		$this->db->join('material_item mi','a.idmi = mi.idmi','inner');
		$this->db->join('depreciacion d','a.iddpre = d.iddpre','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_all_deprecicion(){// en uso: ACTIVOS FIJOS,
		$col="mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
				a.idaf,a.costo,a.fecha_inicio_trabajo,
				d.iddpre,d.nombre as nombre_depreciacion,d.anio,d.porcentaje,d.observaciones as observaciones_deprecicion";
		$this->db->select($col);
		$this->db->from("activo_fijo a");
		$this->db->order_by("mi.nombre", "asc");
		$this->db->join('depreciacion d','a.iddpre = d.iddpre','inner');
		$this->db->join('material_item mi','a.idmi = mi.idmi','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_search($col,$val){
		$cols="mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
				a.idaf,a.costo,a.fecha_inicio_trabajo,
				d.iddpre,d.nombre as nombre_depreciacion,d.anio,d.porcentaje,d.observaciones as observaciones_deprecicion";
		$this->db->select($cols);
		$this->db->from("activo_fijo a");
		$this->db->order_by("mi.nombre", "asc");
		$this->db->join('depreciacion d','a.iddpre = d.iddpre','inner');
		$this->db->join('material_item mi','a.idmi = mi.idmi','inner');
		$this->db->where("$col like '$val%'");
		$query=$this->db->get();
		return $query->result();
	}
	function material_proveedor(){// en uso: COMPRAS
		$cols="a.idaf,iddpre,a.costo,a.fecha_inicio_trabajo,
			mi.idmi,mi.idu,mi.codigo,mi.nombre as nombre_material,mi.fotografia,mi.observaciones as obs_material,
			u.idu,u.nombre as nombre_u,u.abreviatura,
			mp.idmp,mp.costo_unitario as costo_material_proveedor,
			pr.idpro,pr.nit,pr.encargado,pr.url,
			p.ci,p.idci,p.nombre as nombre_proveedor,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas";
		$this->db->select($cols);
		$this->db->from("activo_fijo a");
		$this->db->join('material_item mi','mi.idmi=a.idmi','inner');
		$this->db->join('unidad u','u.idu=mi.idu','inner');
		$this->db->join('material_proveedor mp','mp.idmi=mi.idmi','inner');
		$this->db->join('proveedor pr','pr.idpro=mp.idpro','inner');
		$this->db->join('persona p','p.ci=pr.nit','inner');
		$this->db->group_by("p.ci");
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM activo_fijo WHERE idaf='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('activo_fijo',array($col => $val));
		return $query->result();
	}
	function insertar($idmi,$iddpre,$costo,$fecha_inicio_trabajo){
		$datos=array(
			'idmi' => $idmi,
			'iddpre' => $iddpre,
			'costo' => $costo,
			'fecha_inicio_trabajo' => $fecha_inicio_trabajo
		);
		if($this->db->insert('activo_fijo',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idmi,$iddpre,$costo,$fecha_inicio_trabajo){
		$datos=array(
			'idmi' => $idmi,
			'iddpre' => $iddpre,
			'costo' => $costo,
			'fecha_inicio_trabajo' => $fecha_inicio_trabajo
		);
		if($this->db->update('activo_fijo',$datos,array('idaf'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('activo_fijo',['idaf' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}

/* End of file m_activo_fijo.php */
/* Location: ./application/models/m_activo_fijo.php*/