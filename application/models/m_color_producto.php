<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_color_producto extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('color_producto');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('color_producto',['idcop' => $id]);
		return $query->result();
	}

	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM color_producto WHERE idcop='$id'");
		return $query->result();
	}
	function insertar($idpim,$idco){
		$datos=array(
			'idpim' => $idpim,
			'idco' => $idco
		);
		if($this->db->insert('color_producto',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idpim,$idcop,$pintado){
		$datos=array(
			'idpim' => $idpim,
			'idcop' => $idcop,
			'pintado' => $pintado
		);
		if($this->db->update('color_producto',$datos,array('idcop' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('color_producto',['idcop' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}

/* End of file m_color_producto.php */
/* Location: ./application/models/m_color_producto.php*/