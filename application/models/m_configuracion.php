<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_configuracion extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('configuracion');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('configuracion',['idconf' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM configuracion WHERE idconf='$id'");
		return $query->result();
	}
	function modificar($id,$valor){
		$datos=array(
			'valor' => $valor
		);
		if($this->db->update('configuracion',$datos,array('idconf' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('configuracion',['idconf' => $id])){
			return true;
		}else{
			return false;
		}
	}
}

/* End of file m_configuracion.php */
/* Location: ./application/models/m_configuracion.php*/