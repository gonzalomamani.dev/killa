<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_estado_operacion extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){// en uso: capital humano,
		$query=$this->db->get('estado_operacion');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('estado_operacion',['ideo' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM estado_operacion WHERE ideo='$id'");
		return $query->result();
	}

	function get_row($col,$val){
		$query=$this->db->query("SELECT * FROM estado_operacion WHERE $col='$val'");
		return $query->result();
	}
	
	function insertar($idee1,$idee2,$signo){
		$datos=array(
			'idee1' => $idee1,
			'idee2' => $idee2,
			'signo' => $signo
		);
		if($this->db->insert('estado_operacion',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idee1,$idee2,$signo){
		$datos=array(
			'idee1' => $idee1,
			'idee2' => $idee2,
			'signo' => $signo
		);
		if($this->db->update('estado_operacion',$datos,array('ideo'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('estado_operacion',['ideo' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function eliminar_row($col,$val){
		if($this->db->delete('estado_operacion',[''.$col => $val])){
			return true;
		}else{
			return false;
		}
	}		
}

/* End of file m_estado_operacion.php */
/* Location: ./application/models/m_estado_operacion.php*/