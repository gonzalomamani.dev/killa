<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_almacen_material extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('almacen_material');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('almacen_material',array('idam' => $id));
		return $query->result();
	}
	function get_col($id, $col){
		$query=$this->db->query("SELECT $col FROM almacen_material WHERE idam='$id'");
		return $query->result();
	}
	function get_row($col,$val){//en uso material
		$this->db->select("*");
	    $this->db->from("almacen_material");
	    $this->db->where("$col = '$val'");
	    $query=$this->db->get();
	    return $query->result();
	}
	function get_row_2n($col,$val,$col2,$val2){
		$this->db->select("*");
	    $this->db->from("almacen_material");
	    $this->db->where("$col = '$val'");
	    $this->db->where("$col2 = '$val2'");
	    $query=$this->db->get();
	    return $query->result();
	}
	function get_search($ida,$row,$val){//en uso MATERIALES,
		$col="am.idam,am.ida,am.cantidad,
			m.idm,m.idg,m.idco,m.costo_unitario,
				mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
				u.idu, u.nombre as medida,u.abreviatura, 
				g.idg,g.nombre as nombre_g, 
				c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($col);
		$this->db->from("almacen_material am");
		$this->db->where("am.ida = '$ida'"); 
		if($row!="" && $val!=""){
			if($row=="m.idco" || $row=="m.idg"){
				$this->db->where("$row = '$val'");
			}
			if($row=="mi.codigo" || $row=="mi.nombre" || $row=="am.cantidad"){
				$this->db->where("$row like '$val%'");
			}
		}
		$this->db->order_by("mi.nombre", "asc");
		$this->db->join('material m','m.idm = am.idm','inner');
		$this->db->join('material_item mi','mi.idmi = m.idmi','inner');
		$this->db->join('grupo g','m.idg = g.idg','inner');
		$this->db->join('unidad u','mi.idu = u.idu','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$query=$this->db->get();
		return $query->result();
	}

	function get_material($ida){//en uso: PROVEEDOR,materiales,Produccion
		$col="am.idam,am.ida,am.cantidad,
				m.idm,m.costo_unitario,
				mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
				u.idu, u.nombre as medida,u.abreviatura, 
				g.idg,g.nombre as nombre_g, 
				c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($col);
		$this->db->from("almacen_material am");
		$this->db->where("am.ida = '$ida'");
		$this->db->order_by("mi.nombre", "asc");
		$this->db->join('material m','m.idm = am.idm','inner');
		$this->db->join('material_item mi','mi.idmi = m.idmi','inner');
		$this->db->join('grupo g','m.idg = g.idg','inner');
		$this->db->join('unidad u','mi.idu = u.idu','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_material_search($ida,$col,$val){//en uso: PROVEEDOR, movimiento de materiales
		$cols="am.idam,am.ida,am.cantidad,
				m.idm,m.costo_unitario,
				mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
				u.idu, u.nombre as medida,u.abreviatura, 
				g.idg,g.nombre as nombre_g, 
				c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($cols);
		$this->db->from("almacen_material am");
	if($ida!=""){$this->db->where("am.ida = '$ida'");}
	if($col!="u.idu" && $col!="c.idco"){
		$this->db->where("$col like '$val%'");
	}else{
		$this->db->where("$col = '$val'");
	}
		$this->db->order_by("mi.nombre", "asc");
		$this->db->join('material m','m.idm = am.idm','inner');
		$this->db->join('material_item mi','mi.idmi = m.idmi','inner');
		$this->db->join('grupo g','m.idg = g.idg','inner');
		$this->db->join('unidad u','mi.idu = u.idu','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_material_idmi($ida,$idmi){//en uso: materiales
		$col="am.idam,am.ida,am.cantidad,
				m.idm,m.idg,m.idco,m.costo_unitario,
				mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
				u.idu, u.nombre as medida,u.abreviatura, 
				g.idg,g.nombre as nombre_g, 
				c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($col);
		$this->db->from("almacen_material am");
		$this->db->where("am.ida = '$ida'");
		$this->db->where("mi.idmi = '$idmi'");
		$this->db->order_by("mi.nombre", "asc");
		$this->db->join('material m','m.idm = am.idm','inner');
		$this->db->join('material_item mi','mi.idmi = m.idmi','inner');
		$this->db->join('grupo g','m.idg = g.idg','inner');
		$this->db->join('unidad u','mi.idu = u.idu','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$query=$this->db->get();
		return $query->result();
	}


	function get_material_idm($idm){ // en uso: detalle pedido,
		$col="am.idam,am.ida,sum(am.cantidad) as cantidad,
				m.idm,m.idg,m.idco,m.costo_unitario,
				mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones";
		$this->db->select($col);
		$this->db->from("almacen_material am");
		$this->db->where("m.idm = '$idm'");
		$this->db->group_by("m.idm");
		$this->db->order_by("mi.nombre", "asc");
		$this->db->join('material m','m.idm = am.idm','inner');
		$this->db->join('material_item mi','mi.idmi = m.idmi','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_material_idam($idam){ // en uso: movimiento de materiales,
		$col="am.idam,am.ida,sum(am.cantidad) as cantidad,
				m.idm,m.idg,m.idco,m.costo_unitario,
				mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones";
		$this->db->select($col);
		$this->db->from("almacen_material am");
		$this->db->where("am.idam = '$idam'");
		$this->db->join('material m','m.idm = am.idm','inner');
		$this->db->join('material_item mi','mi.idmi = m.idmi','inner');
		$query=$this->db->get();
		return $query->result();
	}


	function get_material_almacen($ida){//en uso: PRODUCCION,
		$cols="am.idam,am.ida,am.cantidad,
			m.idm,m.idg,m.costo_unitario,
			mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
			c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($cols);
		$this->db->from("almacen_material am");
		$this->db->where("am.ida = '$ida'");
		$this->db->order_by("mi.nombre", "asc");
		$this->db->join('material m','m.idm = am.idm','inner');
		$this->db->join('material_item mi','mi.idmi = m.idmi','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function search($ida,$col,$val){// en uso: PRODUCCION,
		$cols="am.idam,am.ida,am.cantidad,
				m.idm,m.idg,m.idco,m.costo_unitario,
				mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
				c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($cols);
		$this->db->from("almacen_material am");
		$this->db->where("am.ida = $ida and $col like '$val%'");
		$this->db->join('material m','m.idm = am.idm','inner');
		$this->db->join('material_item mi','mi.idmi = m.idmi','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
			$this->db->order_by("mi.nombre", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get_not_material($ida){// obt los materiales que no estan en el almacen. En uso: materiales
		$col="am.idam,am.ida,am.cantidad,
				m.idm,m.idg,m.idco,m.costo_unitario,
				mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
				u.idu, u.nombre as medida,u.abreviatura, 
				g.idg,g.nombre as nombre_g, 
				c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($col);
		$this->db->from("almacen_material am");
		$this->db->where("am.ida != '$ida'");
		$this->db->order_by("mi.nombre", "asc");
		$this->db->join('material m','m.idm = am.idm','inner');
		$this->db->join('material_item mi','mi.idmi = m.idmi','inner');
		$this->db->join('grupo g','m.idg = g.idg','inner');
		$this->db->join('unidad u','mi.idu = u.idu','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_not_material_where($ida,$almacen){// obt los materiales que no estan en el almacen. En uso: materiales
		$col="am.idam,am.ida,am.cantidad,
				m.idm,m.idg,m.idco,m.costo_unitario,
				mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
				u.idu, u.nombre as medida,u.abreviatura, 
				g.idg,g.nombre as nombre_g, 
				c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($col);
		$this->db->from("almacen_material am");
		$this->db->where("am.ida != '$ida'");
		$this->db->where("am.ida = '$almacen'");
		$this->db->order_by("mi.nombre", "asc");
		$this->db->join('material m','m.idm = am.idm','inner');
		$this->db->join('material_item mi','mi.idmi = m.idmi','inner');
		$this->db->join('grupo g','m.idg = g.idg','inner');
		$this->db->join('unidad u','mi.idu = u.idu','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$query=$this->db->get();
		return $query->result();
	}

	function insertar($ida,$idm,$cantidad){
		$datos=array(
			'ida' => $ida,
			'idm' => $idm,
			'cantidad' => $cantidad
		);
		if($this->db->insert('almacen_material',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($ida,$idm,$cantidad){
		$datos=array(
			'ida' => $ida,
			'idm' => $idm,
			'cantidad' => $cantidad
		);
		if($this->db->update('almacen_material',$datos,array('idam' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_cantidad($id, $cantidad){
		$datos=array(
			'cantidad' => $cantidad
		);
		if($this->db->update('almacen_material',$datos,array('idam' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->query("DELETE FROM almacen_material WHERE idam='$id'")){
			return true;
		}else{
			return false;
		}
	}
	
}

/* End of file M_almacen_material.php */
/* Location ./application/models/M_almacen_material.php*/