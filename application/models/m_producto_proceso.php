<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto_proceso extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('producto_proceso');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('producto_proceso',['idppr' => $id]);
		return $query->result();
	}

	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM producto_proceso WHERE idppr='$id'");
		return $query->result();
	}
	function get_proceso($idppr){// en iuso ORDEN DE PRODUCCION
		$col="pp.idppr,pp.idp,pp.sub_proceso,pp.tiempo_estimado,pp.costo,
				pr.idpr,pr.nombre,pr.detalle";
		$this->db->select($col);
		$this->db->from("producto_proceso pp");
		$this->db->order_by("pp.idppr", "asc");
		$this->db->where("pp.idppr = $idppr");
		$this->db->join('proceso pr','pr.idpr = pp.idpr','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_producto_proceso($idp){// en iuso ORDEN DE PRODUCCION
		$col="pp.idppr,pp.idp,pp.sub_proceso,pp.tiempo_estimado,pp.costo,
				pr.idpr,pr.nombre,pr.detalle";
		$this->db->select($col);
		$this->db->from("producto_proceso pp");
		$this->db->order_by("pp.idppr", "asc");
		$this->db->where("pp.idp = $idp");
		$this->db->join('proceso pr','pr.idpr = pp.idpr','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_tareas_proceso($idp,$idpr){// en uso ORDEN DE PRODUCCION
		$col="pp.idppr,pp.idp,pp.sub_proceso,pp.tiempo_estimado,pp.costo,
				pr.idpr,pr.nombre as nombre_proceso,pr.detalle";
		$this->db->select($col);
		$this->db->from("producto_proceso pp");
		$this->db->order_by("pr.nombre", "asc");
		$this->db->where("pp.idp = $idp");
		$this->db->where("pr.idpr = $idpr");
		$this->db->join('proceso pr','pr.idpr = pp.idpr','inner');
		$query=$this->db->get();
		return $query->result();
	}

	function modificar_v2($id,$tiempo_estimado,$costo){// end if PRODUCTO,
		$datos=array(
			'tiempo_estimado'=>$tiempo_estimado,
			'costo' => $costo
		);
		if($this->db->update('producto_proceso',$datos,array('idppr' => $id))){
			return true;
		}else{
			return false;
		}
	}

	function insertar($idp,$idpr,$sub_proceso,$tiempo_estimado,$costo){
		$datos=array(
			'idp' => $idp,
			'idpr' => $idpr,
			'sub_proceso' => $sub_proceso,
			'tiempo_estimado' => $tiempo_estimado,
			'costo' => $costo
		);
		if($this->db->insert('producto_proceso',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idp,$idpr,$sub_proceso,$tiempo_estimado,$costo){
		$datos=array(
			'idp' => $idp,
			'idpr' => $idpr,
			'sub_proceso' => $sub_proceso,
			'tiempo_estimado' => $tiempo_estimado,
			'costo' => $costo
		);
		if($this->db->update('producto_proceso',$datos,array('idppr' => $id))){
			return true;
		}else{
			return false;
		}
		
	}
	function eliminar($id){
		if($this->db->delete('producto_proceso',['idppr' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}