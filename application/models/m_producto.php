<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){// en uso: PRODUCTO,
		$cols="p.idp, p.cod, p.nombre, p.descripcion, p.fecha_creacion, p.c_u, p.peso, p.disenador, p.etiquetaLogo,p.plaquetaLogo,p.repujadoLogo,p.etiquetaCuero,p.correa,p.cierreDelante,p.cierreDetras,p.cierreDentro,p.largo,p.alto,p.ancho,p.repujado,p.otra_caracteristica,SUBSTRING_INDEX(p.cod,' ',1) as o_cod,SUBSTRING_INDEX(p.cod,' ', -1) as o_num,
				pg.idpg, pg.numero, pg.nombre as nombre_grupo, pg.descripcion as descripcion_grupo";
		$query=$this->db->query("SELECT $cols FROM producto p, producto_grupo pg WHERE pg.idpg = p.idpg order by o_cod,o_num+0 ");
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('producto',['idp' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM producto WHERE idp='$id'");
		return $query->result();
	}
	function get_row($col,$val){//En uso: CONFIGURACION->PRODCCION
		$query=$this->db->get_where('producto',array($col => $val));
		return $query->result();
	}
	function get_col_2N($id,$col1,$col2){
		$query=$this->db->query("SELECT $col1,$col2 FROM producto WHERE idp='$id'");
		return $query->result();
	}
	function get_search($col,$val){ //en uso: PRODUCTO
		$cols="p.idp, p.cod, p.nombre, p.descripcion, p.fecha_creacion, p.c_u, p.peso, p.disenador, p.etiquetaLogo,p.plaquetaLogo,p.repujadoLogo,p.etiquetaCuero,p.correa,p.cierreDelante,p.cierreDetras,p.cierreDentro,p.largo,p.alto,p.ancho,p.repujado,p.otra_caracteristica,SUBSTRING_INDEX(p.cod,' ',1) as o_cod,SUBSTRING_INDEX(p.cod,' ', -1) as o_num,
				pg.idpg, pg.numero, pg.nombre as nombre_grupo, pg.descripcion as descripcion_grupo";
		$where="";
		if($col=="p.fecha_creacion"){
			$where.="and ".$col." = '".$val."'";
		}else{
			if($col=="codigo"){
				$where.="and p.cod = '".$val."'";
			}else{
				$where.="and ".$col." like '%".$val."%'";
			}
		}
		$query=$this->db->query("SELECT $cols FROM producto p, producto_grupo pg WHERE pg.idpg = p.idpg ".$where." order by o_cod,o_num+0 ");
		return $query->result();
	}
	function get_accesorios($idp){ //en uso: almacen de productos impresion,CLIENTES,PRODUCTO,PEDIDO->REPORTE->MATERIAL
		$col="pm.idpm, pm.idp, pm.cantidad,
			m.idm,m.idg,m.costo_unitario,
			mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
			u.idu,u.nombre as nombre_u,u.abreviatura,u.equivalencia,
			c.idco, c.nombre as nombre_color, c.codigo as codigo_color";
		$this->db->select($col);
		$this->db->from("producto_material pm");
		$this->db->order_by("pm.idpm", "asc");
		$this->db->where("pm.idp = $idp");
		$this->db->join('material m','pm.idm = m.idm','inner');
		$this->db->join('material_item mi','mi.idmi = m.idmi','inner');
		$this->db->join('unidad u','mi.idu = u.idu','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_materiales_liquidos($idp){//en uso: almacen de productos impresion,,CLIENTES,PRODUCTO,PEDIDO->REPORTE->MATERIAL
		//$query=$this->db->query("SELECT cp.color_producto,p.nombre,sum(p.alto)as alto,sum(p.ancho) as ancho,pma.idml FROM categoria_pieza cp, pieza p, pieza_material_liquido pma WHERE( cp.idcp=p.idcp and p.idpi=pma.idpi and cp.idp='$idp') GROUP By(pma.idml)");
		$cols="sum(((pi.ancho+pi.alto)*2)*pi.unidades) as area, pi.unidades,
				pg.idpig,pg.nombre as nombre_p,
				ml.idml,ml.idm,ml.variable,ml.tipo,
				m.idm,m.idg,m.costo_unitario,
				mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
				u.idu,u.nombre as nombre_u,u.abreviatura,u.equivalencia,
				c.idco, c.nombre as nombre_color, c.codigo as codigo_color";
		$this->db->select($cols);
		$this->db->from("categoria_pieza cp");
		$this->db->where("cp.idp = '$idp'");
		$this->db->join('pieza pi','cp.idcp = pi.idcp','inner');
		$this->db->join('pieza_grupo pg','pg.idpig = pi.idpig','inner');
		$this->db->join('pieza_material_liquido pml','pi.idpi = pml.idpi','inner');
		$this->db->join('material_liquido ml','pml.idml = ml.idml','inner');
		$this->db->join('material m','ml.idm = m.idm','inner');
		$this->db->join('material_item mi','mi.idmi = m.idmi','inner');
		$this->db->join('unidad u','mi.idu = u.idu','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$this->db->group_by('pml.idml');
		$query=$this->db->get();
		return $query->result();
	}
	function get_materiales($idp){//en uso: almacen de productos impresion,,CLIENTES,PRODUCTO,PEDIDO->REPORTE->MATERIAL
		$cols="cp.color_producto,
				pi.idpi,pi.alto,pi.ancho,pi.unidades, SUM(pi.ancho*pi.alto*pi.unidades) as area,
				pg.idpig,pg.nombre as nombre_pi,
				cpr.portada,cpr.idpim,
				m.costo_unitario,
				mi.idmi,mi.codigo,mi.nombre,mi.fotografia,
				u.nombre as nombre_u, u.abreviatura,u.equivalencia,
				c.idco, c.nombre as nombre_color, c.codigo as codigo_color";
		$this->db->select($cols);
		$this->db->from('categoria_pieza cp');
		$this->db->where("cp.idp = '$idp'");
		$this->db->join('pieza pi','cp.idcp = pi.idcp','inner');
		$this->db->join('pieza_grupo pg','pg.idpig = pi.idpig','inner');
		$this->db->join("categoria_producto cpr","cp.idcp=cpr.idcp","inner");
		$this->db->join('material m','cpr.idm = m.idm','inner');
		$this->db->join('material_item mi','mi.idmi = m.idmi','inner');
		$this->db->join('unidad u','mi.idu = u.idu','inner');
		$this->db->join('color c','cpr.idco = c.idco','inner');
		$this->db->group_by('cpr.idpim');
		$query=$this->db->get();
		return $query->result();
	}
	function get_materiales_indirectos($idp){//en uso: almacen de productos impresion,
		$col="pma.idpma,pma.idp,pma.idma,pma.costo,
				ma.idma,
				mi.idmi,mi.codigo,mi.nombre,mi.fotografia,
				u.nombre as nombre_u, u.abreviatura,u.equivalencia";
		$this->db->select($col);
		$this->db->from("producto_material_adicional pma");
		$this->db->where("pma.idp = '$idp'");
		$this->db->join("material_adicional ma","pma.idma = ma.idma","inner");
		$this->db->join("material_item mi","ma.idmi = mi.idmi","inner");
		$this->db->join('unidad u','mi.idu = u.idu','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_procesos($idp){//en uso: almacen de productos impresion,
		$col="pp.idppr,pp.idp,pp.sub_proceso,pp.tiempo_estimado,pp.costo,
				pr.idpr,pr.nombre,pr.detalle";
		$this->db->select($col);
		$this->db->from("producto_proceso pp");
		$this->db->order_by("pp.idppr", "asc");
		$this->db->where("pp.idp = $idp");
		$this->db->join('proceso pr','pr.idpr = pp.idpr','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_procesos_pieza($idp){//en uso: almacen de productos impresion,
		$col="cp.idcp,cp.idp,
			pi.idpi,pi.unidades,
			pg.idpig,pg.nombre as nombre_pi,
			pipr.idpipr,pipr.idpr,pipr.tiempo_estimado,pipr.costo,
			pr.nombre";
		$this->db->select($col);
		$this->db->from("categoria_pieza cp");
		$this->db->where("cp.idp = '$idp'");
		$this->db->order_by("pipr.idpr", "asc");
		$this->db->order_by("cpr.idm", "asc");
		$this->db->join("categoria_producto cpr","cpr.idcp=cp.idcp","inner");
		$this->db->join("pieza pi","cp.idcp=pi.idcp","inner");
		$this->db->join("pieza_grupo pg","pg.idpig=pi.idpig","inner");
		$this->db->join("pieza_proceso pipr","pi.idpi=pipr.idpi","inner");
		$this->db->join("proceso pr","pr.idpr=pipr.idpr","inner");
		$query=$this->db->get();
		return $query->result();
	}
	function get_categorias_producto($idp){//en uso: PRODUCTO,
		$cols="p.idp,p.cod,p.nombre,p.peso,
			cpi.idcp,cpi.color_producto,
			cpr.idpim,cpr.idm,cpr.portada";
		$this->db->select($cols);
		$this->db->from("producto p");
		$this->db->where("p.idp = '$idp'");
		$this->db->join("categoria_pieza cpi","p.idp=cpi.idp","inner");
		$this->db->join("categoria_producto cpr","cpi.idcp=cpr.idcp","inner");
		$query=$this->db->get();
		return $query->result();
	}
	function get_piezas_grupo($idp){//en uso: ORDEN DE PRODUCCION
		$cols="p.idp,p.cod,p.nombre,p.peso,
			cpi.idcp,cpi.color_producto,
			pi.idpi,pi.idpig,pi.codigo as codigo_pieza,pi.nombre as nombre_piezas,pi.imagen";
		$this->db->select($cols);
		$this->db->from("producto p");
		$this->db->where("p.idp = '$idp'");
		$this->db->join("categoria_pieza cpi","p.idp=cpi.idp","inner");
		$this->db->join("pieza pi","cpi.idcp=pi.idcp","inner");
		$query=$this->db->get();
		return $query->result();
	}
	function get_piezas($idp){//en uso: PRODUCTO,
		$cols="p.idp,p.cod,p.nombre,p.peso,
			cpi.idcp,cpi.color_producto,
			pi.idpi,pi.idpig,pi.imagen";
		$this->db->select($cols);
		$this->db->from("producto p");
		$this->db->where("p.idp = '$idp'");
		$this->db->join("categoria_pieza cpi","p.idp=cpi.idp","inner");
		$this->db->join("pieza pi","cpi.idcp=pi.idcp","inner");
		$query=$this->db->get();
		return $query->result();
	}
	function get_producto($idp){
		$col="p.idp,p.cod,p.nombre";
		$this->db->select($col);
		$this->db->from("producto p");
		$this->db->where("p.idp = $idp");
		$query=$this->db->get();
		return $query->result();
	}
	function get_producto_grupo(){
		$col="p.idp, p.cod as codigo, p.nombre, p.descripcion, p.fecha_creacion,p.c_u,p.peso,disenador,etiquetaLogo,plaquetaLogo,repujadoLogo,etiquetaCuero,correa,cierreDelante,cierreDetras,cierreDentro,largo,alto,ancho,repujado,
				pg.idpg,pg.numero,pg.nombre as nombre_grupo,pg.descripcion as descripcion_grupo";
		$this->db->select($col);
		$this->db->from("producto p");
		$this->db->join('producto_grupo pg','pg.idpg = p.idpg','inner');
		$query=$this->db->get();
		return $query->result();
	}



	/*
	function get_producto_almacen_not_col_row($ida,$columna,$row){
		$col="p.idp, p.cod as codigo_p, p.nombre as nombre_p, p.descripcion as descripcion_p, p.fecha_creacion as fecha_creacion_p,p.c_u,p.peso";
		$this->db->select($col);
		$this->db->from("producto p");
		$this->db->where("ap.idp IS NULL and p.$columna like '$row%'");
		$this->db->join('almacen_producto ap','p.idp=ap.idp','left');
		$query=$this->db->get();
		return $query->result();
	}*/

	function get_categoria_color_true(){
		$col="p.idp, p.cod as codigo, p.nombre, p.descripcion, p.fecha_creacion,p.c_u,p.peso,disenador,etiquetaLogo,plaquetaLogo,repujadoLogo,etiquetaCuero,correa,cierreDelante,cierreDetras,cierreDentro,largo,alto,ancho,repujado,
				pg.idpg,pg.numero,pg.nombre as nombre_grupo,pg.descripcion as descripcion_grupo";
		$this->db->select($col);
		$this->db->from("producto p");
		$this->db->join('producto_grupo pg','pg.idpg = p.idpg','inner');
		$query=$this->db->get();
		return $query->result();
	}
	
	function get_material_liquido($idp){//en uso: detalle pedido,
		$col="cp.idp,cp.idcp,p.idpi,p.nombre,sum(((p.ancho+p.alto)*2)*p.unidades) as area,pml.idpml,ml.idml,ml.variable,ml.idm,ml.tipo";
		$this->db->select($col);
		$this->db->from("categoria_pieza cp");
		$this->db->where("cp.idp = $idp");
		$this->db->group_by("ml.idm");
		$this->db->join('pieza p','cp.idcp = p.idcp','inner');
		$this->db->join('pieza_material_liquido pml','p.idpi = pml.idpi','inner');
		$this->db->join('material_liquido ml','pml.idml = ml.idml','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_material($idp){//en uso: detalle pedido,
		$col="cp.idcp,cp.idp,cp.color_producto,SUM(p.alto*p.ancho*p.unidades) as area,cpr.idpim,cpr.idm,cpr.portada";
		$this->db->select($col);
		$this->db->from("categoria_pieza cp");
		$this->db->where("cp.idp = $idp");
		$this->db->group_by("cpr.idm");
		$this->db->order_by("cp.idcp");
		$this->db->join('pieza p','cp.idcp = p.idcp','inner');
		$this->db->join('categoria_producto cpr','cp.idcp = cpr.idcp','inner');
		$query=$this->db->get();
		return $query->result();
	}		
	function insertar($idp,$idpg,$cod,$nombre,$fecha_creacion,$descripcion,$disenador,$peso,$etiquetaLogo,$plaquetaLogo,$repujadoLogo,$etiquetaCuero,$correa,$cierreDelante,$cierreDetras,$cierreDentro,$largo,$alto,$ancho,$repujado,$otra_caracteristica){
		$datos=array(
			'idp' => $idp,
			'idpg' => $idpg,
			'cod' => $cod,
			'nombre' => $nombre,
			'fecha_creacion' => $fecha_creacion, 
			'descripcion' => $descripcion,
			'disenador' => $disenador,
			'peso' => $peso,
			'etiquetaLogo' => $etiquetaLogo,
			'plaquetaLogo' => $plaquetaLogo,
			'repujadoLogo' => $repujadoLogo,
			'etiquetaCuero' => $etiquetaCuero,
			'correa' => $correa,
			'cierreDelante' => $cierreDelante,
			'cierreDetras' => $cierreDetras,
			'cierreDentro' => $cierreDentro,
			'largo' => $largo,
			'alto' => $alto,
			'ancho' => $ancho,
			'repujado' => $repujado,
			'otra_caracteristica' => $otra_caracteristica
		);
		if($this->db->insert('producto',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idpg,$cod,$nombre,$fecha_creacion,$descripcion,$disenador,$peso,$etiquetaLogo,$plaquetaLogo,$repujadoLogo,$etiquetaCuero,$correa,$cierreDelante,$cierreDetras,$cierreDentro,$largo,$alto,$ancho,$repujado,$otra_caracteristica){
		$datos=array(
			'idpg' => $idpg,
			'cod' => $cod,
			'nombre' => $nombre,
			'fecha_creacion' => $fecha_creacion, 
			'descripcion' => $descripcion,
			'disenador' => $disenador,
			'peso' => $peso,
			'etiquetaLogo' => $etiquetaLogo,
			'plaquetaLogo' => $plaquetaLogo,
			'repujadoLogo' => $repujadoLogo,
			'etiquetaCuero' => $etiquetaCuero,
			'correa' => $correa,
			'cierreDelante' => $cierreDelante,
			'cierreDetras' => $cierreDetras,
			'cierreDentro' => $cierreDentro,
			'largo' => $largo,
			'alto' => $alto,
			'ancho' => $ancho,
			'repujado' => $repujado,
			'otra_caracteristica' => $otra_caracteristica
		);
		if($this->db->update('producto',$datos,array('idp' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('producto',['idp' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function max($col){// en uso: PRODUCCION->PRODUCTO,
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM producto");
		return $query->result();
	}
	function maxID(){
		$query=$this->db->query("SELECT IFNULL(max(idp),0) as max FROM producto");
		return $query->result();
	}
}

/* End of file m_producto.php */
/* Location: ./application/models/m_producto.php*/