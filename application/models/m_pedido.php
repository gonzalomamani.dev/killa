<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_pedido extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("idpe", "desc");
		$query=$this->db->get('pedido');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('pedido',['idpe' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM pedido WHERE idpe='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('pedido',[$col => $val]);
		return $query->result();
	}
	function get_search($atrib,$val,$order){// en uso PEDIDOS
		$cols="p.idpe, p.numero, p.nombre, p.observaciones,
				cl.idcl, cl.nit, cl.encargado, cl.url,
				pe.ci,pe.idci,pe.nombre as nombre_cliente,pe.telefono,pe.email,pe.direccion,pe.fotografia,pe.caracteristicas,
				c.idci,c.nombre as ciudad,
				pa.idpa,pa.nombre as pais";
		$this->db->select($cols);
		$this->db->from("pedido p");
		if(isset($order)){ if($order!=""){ $this->db->order_by($order, "asc");}else{ $this->db->order_by("p.numero", "desc");}	
		}else{ $this->db->order_by("p.numero", "desc");	}
		if(isset($atrib) && isset($val)){
			if($atrib!="" && $val!=""){
				if($atrib=="p.idpe" || $atrib=="cl.idcl"){ $this->db->where("$atrib = '$val'"); }
				if($atrib=="p.numero" || $atrib=="p.descuento"){ $this->db->where("$atrib like '$val%'"); }
				if($atrib=="p.nombre" || $atrib=="p.observaciones"){ $this->db->where("$atrib like '%$val%'"); }
			}
		}
		$this->db->join('cliente cl','p.idcl = cl.idcl','inner');
		$this->db->join('persona pe','pe.ci = cl.nit','inner');
		$this->db->join('ciudad c','c.idci = pe.idci','inner');
		$this->db->join('pais pa','c.idpa = pa.idpa','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_all_pedido($order){
		$col="p.idpe,p.numero,p.nombre,p.descuento,p.observaciones, 
				cl.idcl, cl.nit, cl.encargado, cl.url,
				pe.ci,pe.idci,pe.nombre as nombre_cliente,pe.telefono,pe.email,pe.direccion,pe.fotografia,pe.caracteristicas,
				c.idci,c.nombre as ciudad,
				pa.idpa,pa.nombre as pais";
		$this->db->select($col);
		$this->db->from("pedido p");
		if(isset($order)){ $this->db->order_by($order, "asc");	
		}else{ $this->db->order_by("p.numero", "desc");}
		$this->db->join('cliente cl','p.idcl = cl.idcl','inner');
		$this->db->join('persona pe','pe.ci = cl.nit','inner');
		$this->db->join('ciudad c','c.idci = pe.idci','inner');
		$this->db->join('pais pa','c.idpa = pa.idpa','inner');
		$query=$this->db->get();
		return $query->result();
	}

	/*function get_not_venta(){// en uso: PEDIDO,
		$col="*";
		$this->db->select($col);
		$this->db->from("pedido p");
		$this->db->order_by("p.numero", "desc");
		$this->db->join('venta v','p.idpe != v.idpe','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_complet($idpe){//en uso PEDIDO,
		$col="p.idpe,p.numero,p.fecha_pedido,p.tiempo,p.fecha_inicio,p.fecha_estimada,p.fecha_entrega,p.monto_sistema,p.monto_parcial,p.descuento,p.monto_total,p.observaciones,p.estado, 
				cl.idcl, cl.nit, cl.encargado, cl.url,
				pe.ci,pe.idci,pe.nombre,pe.telefono,pe.email,pe.direccion,pe.fotografia,pe.caracteristicas";
		$this->db->select($col);
		$this->db->from("pedido p");
		$this->db->where("p.idpe = '$idpe'");
		$this->db->order_by("idpe", "desc");
		$this->db->join('cliente cl','p.idcl = cl.idcl','inner');
		$this->db->join('persona pe','pe.ci = cl.nit','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_all_complet(){//en uso PEDIDO,
		$col="p.idpe,p.numero,p.fecha_pedido,p.tiempo,p.fecha_inicio,p.fecha_estimada,p.fecha_entrega,p.monto_sistema,p.monto_parcial,p.descuento,p.monto_total,p.observaciones,p.estado, 
				cl.idcl, cl.nit, cl.encargado, cl.url,
				pe.ci,pe.idci,pe.nombre,pe.telefono,pe.email,pe.direccion,pe.fotografia,pe.caracteristicas,
				c.idci,c.nombre as ciudad,
				pa.idpa,pa.nombre as pais";
		$this->db->select($col);
		$this->db->from("pedido p");
		$this->db->order_by("idpe", "desc");
		$this->db->join('cliente cl','p.idcl = cl.idcl','inner');
		$this->db->join('persona pe','pe.ci = cl.nit','inner');
		$this->db->join('ciudad c','c.idci = pe.idci','inner');
		$this->db->join('pais pa','c.idpa = pa.idpa','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_search($col,$val){//en uso PEDIDO,
		$cols="p.idpe,p.numero,p.fecha_pedido,p.tiempo,p.fecha_inicio,p.fecha_estimada,p.fecha_entrega,p.monto_sistema,p.monto_parcial,p.descuento,p.monto_total,p.observaciones,p.estado, 
				cl.idcl, cl.nit, cl.encargado, cl.url,
				pe.ci,pe.idci,pe.nombre,pe.telefono,pe.email,pe.direccion,pe.fotografia,pe.caracteristicas";
		$this->db->select($cols);
		$this->db->from("pedido p");
		$this->db->order_by("idpe", "desc");
		if($col=="p.monto_total" || $col=="p.numero"){
			$this->db->where("$col like '$val%'");
		}
		if($col=="cl.idcl"){
			$this->db->where("$col = '$val'");
		}
		if($col=="fecha_pedido" || $col=="fecha_entrega"){
			$vf=explode("|", $val);
			$this->db->where("p.".$col." >= '$vf[0]' AND p.".$col." <= '$vf[1]'");
		}
		if($col=="p.fecha_pedido" || $col=="p.fecha_entrega"){
			$this->db->where("$col >= '$val' AND $col <= '$val'");
		}
		$this->db->join('cliente cl','p.idcl = cl.idcl','inner');
		$this->db->join('persona pe','pe.ci = cl.nit','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_productos($idpe){// en uso en ORDEN DE PRODUCCION
		$col="pe.idpe,pe.numero,pe.estado,
			dp.iddp,sum(dp.cantidad) as cantidad,dp.estado,
			cpr.idpim,cpr.idm,
			cp.idcp,
			p.idp,p.cod as codigo, p.nombre";
		$this->db->select($col);
		$this->db->from("pedido pe");
		$this->db->where("pe.idpe = '$idpe'");
		$this->db->join('detalle_pedido dp','pe.idpe = dp.idpe','inner');
		$this->db->join('categoria_producto cpr','dp.idpim = cpr.idpim','inner');
		$this->db->join('categoria_pieza cp','cp.idcp = cpr.idcp','inner');
		$this->db->join('producto p','p.idp = cp.idp','inner');
		$this->db->group_by("p.idp");
		$this->db->order_by("p.nombre");
		$query=$this->db->get();
		return $query->result();		
	}

	function get_row_like($c,$val){
		$col="p.idpe,p.numero,p.fecha_pedido,p.tiempo,p.fecha_inicio,p.fecha_estimada,p.fecha_entrega,p.monto_sistema,p.monto_parcial,p.descuento,p.monto_total,p.observaciones,p.estado, 
				cl.idcl, cl.nit, cl.encargado, cl.url,
				pe.ci,pe.ciudad,pe.pais,pe.nombre,pe.telefono,pe.email,pe.direccion,pe.fotografia,pe.caracteristicas";
		$this->db->select($col);
		$this->db->from("pedido p");
		$this->db->order_by("p.idpe", "desc");
		if($c=='idpe' || $c=='monto' || $c=='adelanto'){
			$this->db->where("p.$c like '$val%'");
		}else{
			$this->db->where("p.$c = '$val'");
		}
		$this->db->join('cliente cl','p.idcl = cl.idcl','inner');
		$this->db->join('persona pe','pe.ci = cl.nit','inner');
		$query=$this->db->get();
		return $query->result();
	}

	function get_pedido_cliente($idpe){//en uso: PEDIDO,
		$col="p.idpe,p.numero,p.fecha_pedido,p.tiempo,p.fecha_inicio,p.fecha_estimada,p.fecha_entrega,p.monto_sistema,p.monto_parcial,p.descuento,p.monto_total,p.observaciones,p.estado, 
				cl.idcl, cl.nit, cl.encargado, cl.url,
				pe.ci,pe.nombre,pe.telefono,pe.email,pe.direccion,pe.fotografia,pe.caracteristicas,
				c.idci,c.nombre as ciudad,
				pa.idpa,pa.nombre as pais";
		$this->db->select($col);
		$this->db->from("pedido p");
		$this->db->where("p.idpe = '$idpe'");
		$this->db->join('cliente cl','p.idcl = cl.idcl','inner');
		$this->db->join('persona pe','pe.ci = cl.nit','inner');
		$this->db->join('ciudad c','c.idci = pe.idci','inner');
		$this->db->join('pais pa','c.idpa = pa.idpa','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_accesorios_requeridos($idpe){
		$col="prm.idi,SUM(prm.cantidad*dp.cantidad) as cantidad";
		$this->db->select($col);
		$this->db->from("detalle_pedido dp");
		$this->db->where("dp.idpe = '$idpe'");
		$this->db->group_by("prm.idi");
		$this->db->join('categoria_producto cpr','cpr.idpim = dp.idpim','inner');
		$this->db->join('categoria_pieza cpi','cpi.idcp = cpr.idcp','inner');
		$this->db->join('producto pr','pr.idp = cpi.idp','inner');
		$this->db->join('producto_material prm','prm.idp = pr.idp','inner');
		$query=$this->db->get();
		return $query->result();
	}*/
	function insertar($idpe,$idcl,$numero,$nombre,$observaciones){
		$datos=array(
			'idpe' => $idpe,
			'idcl' => $idcl,
			'numero' => $numero,
			'nombre' => $nombre,
			'observaciones' => $observaciones
		);
		if($this->db->insert('pedido',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($idpe,$idcl,$fecha_pedido,$tiempo,$fecha_inicio,$fecha_estimada,$fecha_entrega,$monto_sistema,$monto_parcial,$descuento,$monto_total,$observaciones){
		$datos=array(
			'idcl' => $idcl,
			'fecha_pedido' => $fecha_pedido,
			'tiempo' => $tiempo,
			'fecha_inicio' => $fecha_inicio,
			'fecha_estimada' => $fecha_estimada,
			'fecha_entrega' => $fecha_entrega,
			'monto_sistema' => $monto_sistema,
			'monto_parcial' => $monto_parcial,
			'descuento' => $descuento,
			'monto_total' => $monto_total,
			'observaciones' => $observaciones
		);
		if($this->db->update('pedido',$datos,array('idpe'=>$idpe))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_row_id($idpe,$col,$val){
		$datos=array( $col => $val );
		if($this->db->update('pedido',$datos,array('idpe'=>$idpe))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($idpe){
		if($this->db->delete('pedido',['idpe' => $idpe])){
			return true;
		}else{
			return false;
		}
	}
	function max($col){
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM pedido");
		return $query->result();
	}	
}

/* End of file m_pedido.php */
/* Location: ./application/models/m_pedido.php*/