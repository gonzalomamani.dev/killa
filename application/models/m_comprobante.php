<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_comprobante extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('comprobante');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('comprobante',['idcm' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM comprobante WHERE idcm='$id'");
		return $query->result();
	}

	function get_search($col,$val,$col2,$val2,$fecha1,$fecha2){
      	$cols="c.idcm,c.tipo,c.folio,c.fecha,c.glosa,
      			sum(dp.debe) as debe,sum(dp.haber) as haber";
    	$this->db->select($cols);
    	$this->db->from("comprobante c");
    	$this->db->where("c.fecha >= '$fecha1'");
    	$this->db->where("c.fecha <= '$fecha2'");
    	if($col!="" && $val!=""){ $this->db->where("$col = '$val'");}
		if($col2!="" && $val2!=""){ $this->db->where("$col2 = '$val2'");}
    	$this->db->join("detalle_comprobante dp","c.idcm = dp.idcm","inner");
    	$this->db->group_by("dp.idcm");
    	$this->db->order_by("c.fecha","asc");
    	$this->db->order_by("c.tipo","desc");
    	$this->db->order_by("c.folio","asc");
    	$query=$this->db->get();
    	return $query->result();
	}
	function insertar($idcm,$glosa,$tipo,$folio,$fecha){
		$datos=array(
			'idcm' => $idcm,
			'glosa' => $glosa,
			'tipo' => $tipo,
			'folio' => $folio,
			'fecha' => $fecha
		);
		if($this->db->insert('comprobante',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($idcm,$glosa,$tipo,$folio,$fecha){
		$datos=array(
			'glosa' => $glosa,
			'tipo' => $tipo,
			'folio' => $folio,
			'fecha' => $fecha
		);
		if($this->db->update('comprobante',$datos,array('idcm'=>$idcm))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_col_2n($id,$col,$val,$col2,$val2){
		$datos=array(
			$col => $val,
			$col2 => $val2
		);
		if($this->db->update('comprobante',$datos,array('idcm'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_row_2n($id,$col,$val,$col2,$val2){
		$datos=array(
			$col => $val,
			$col2 => $val2
		);
		if($this->db->update('comprobante',$datos,array('idcm'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('comprobante',['idcm' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function max_id($col){
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM comprobante");
		return $query->result();
	}
	function max_folio($tipo,$fecha1,$fecha2){
		$query=$this->db->query("SELECT IFNULL(max(folio),0) as folio FROM comprobante WHERE tipo='$tipo' AND fecha>='$fecha1' AND fecha<='$fecha2'");
		return $query->result();
	}		
}

/* End of file m_comprobante.php */
/* Location: ./application/models/m_comprobante.php*/