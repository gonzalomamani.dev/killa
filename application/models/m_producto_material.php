<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto_material extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('producto_material');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('producto_material',['idpi' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM producto_material WHERE idpi='$id'");
		return $query->result();
	}
	function get_where_producto_materiales($idp){//en uso: detalle pedido, PRODUCCION
		$col="pm.idpm, pm.idp, pm.cantidad,
			m.idm,m.idg,m.idco,m.costo_unitario,
			mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
			u.idu,u.nombre as nombre_unidad,u.abreviatura";
		$this->db->select($col);
		$this->db->from("producto_material pm");
		$this->db->order_by("pm.idpm", "asc");
		$this->db->where("pm.idp = $idp");
		$this->db->join('material m','pm.idm = m.idm','inner');
		$this->db->join('material_item mi','mi.idmi = m.idmi','inner');
		$this->db->join('unidad u','u.idu = mi.idu','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_col_2($col,$val,$col2,$val2){
		$query=$this->db->get_where('producto_material',[$col => $val,$col2 => $val2]);
		return $query->result();
	}
	function get_control($idm,$idp){// en uso: produccion,PRODUCTO
		$cols="pm.idpm,pm.idp,pm.idm,pm.cantidad";
		$this->db->select($cols);
		$this->db->from("producto_material pm");
		$this->db->where("pm.idm = $idm and pm.idp = $idp");
		$this->db->order_by("pm.idpm", "asc");
		$query=$this->db->get();
		return $query->result();
	}

	function insertar($idp,$idm,$cantidad){
		$datos=array(
			'idp' => $idp,
			'idm' => $idm,
			'cantidad' => $cantidad
		);
		if($this->db->insert('producto_material',$datos)){
			return true;
		}else{
			return false;
		}
	}
	
	function modificar($idpm,$idp,$idi){
		$datos=array(
			'idp' => $idp,
			'idi' => $idi,
		);
		if($this->db->update('producto_material',$datos,array('idpi'=>$idpm))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_cantidad($id,$cantidad){
		$datos=array(
			'cantidad' => $cantidad
		);
		if($this->db->update('producto_material',$datos,array('idpm'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('producto_material',['idpm' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function eliminar_2($idm,$idp){
		if($this->db->delete('producto_material',['idm' => $idm,'idp' => $idp])){
			return true;
		}else{
			return false;
		}
	}
}

/* End of file m_producto_material.php */
/* Location: ./application/models/m_producto_material.php*/