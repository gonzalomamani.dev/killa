<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_material_item extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('material_item');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('material_item',['idmi' => $id]);
		return $query->result();
	}

	function get_material(){// en uso: PROVEEDOR,PRODUCTO
		$col="mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,
			m.idm,m.idmi,m.idg,
			c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($col);
		$this->db->from("material_item mi");
		$this->db->join('material m','m.idmi=mi.idmi','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	function get_material_adicional(){// en uso: PROVEEDOR,INSUMO,PRODUCTO
		$col="mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
				u.idu,u.nombre as nombre_u,u.abreviatura,
			m.idma";
		$this->db->select($col);
		$this->db->from("material_item mi");
		$this->db->join('material_adicional m','m.idmi=mi.idmi','inner');
		$this->db->join('unidad u','u.idu=mi.idu','inner');
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	function get_activo_fijo(){
		$col="mi.idmi,mi.codigo,mi.nombre,mi.fotografia,
			m.idaf,m.iddpre,m.costo,m.fecha_inicio_trabajo";
		$this->db->select($col);
		$this->db->from("material_item mi");
		$this->db->join('activo_fijo m','m.idmi=mi.idmi','inner');
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	function get_material_vario(){
		$col="mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
			u.idu,u.nombre as nombre_u,u.abreviatura,
			m.idmv";
		$this->db->select($col);
		$this->db->from("material_item mi");
		$this->db->join('material_varios m','m.idmi=mi.idmi','inner');
		$this->db->join('unidad u','u.idu=mi.idu','inner');		
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	function get_row_search($col,$val,$tipo){// en uso: PROVEEDOR
		$cols="mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,";
		if($tipo=="material"){ $cols.="m.idm,m.idmi,m.idg,m.idco,c.idco, c.nombre as nombre_c, c.codigo as codigo_c";}
		if($tipo=="material_adicional"){ $cols.="m.idma";}
		if($tipo=="material_varios"){ $cols.="m.idmv";}
		if($tipo=="activo_fijo"){ $cols.="m.idaf,m.iddpre,m.costo,m.fecha_inicio_trabajo";}
		$this->db->select($cols);
		$this->db->from("material_item mi");
		if($tipo=="material"){$this->db->join('material m','m.idmi=mi.idmi','inner');$this->db->join('color c','m.idco = c.idco','inner'); $this->db->where("$col like '$val%'");}
		if($tipo=="material_adicional"){$this->db->join('material_adicional m','m.idmi=mi.idmi','inner'); $this->db->where("$col like '$val%'");}
		if($tipo=="material_varios"){$this->db->join('material_varios m','m.idmi=mi.idmi','inner'); $this->db->where("$col like '$val%'");}
		if($tipo=="activo_fijo"){$this->db->join('activo_fijo m','m.idmi=mi.idmi','inner'); $this->db->where("$col like '$val%'");}
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM material_item WHERE idmi='$id'");
		return $query->result();
	}
	function get_row($col,$val){// en uso: Configuracion de material->Unidad
		$query=$this->db->get_where('material_item',array($col => $val));
		return $query->result();
	}


	function insertar($idmi,$idu,$codigo,$nombre,$fotografia,$observaciones){
		$datos=array(
			'idmi' => $idmi,
			'idu' => $idu,
			'codigo' => $codigo,
			'nombre' => $nombre,
			'fotografia' => $fotografia,
			'observaciones' => $observaciones
		);
		if($this->db->insert('material_item',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($idmi,$idu,$codigo,$nombre,$fotografia,$observaciones){
		$datos=array(
			'idu' => $idu,
			'codigo' => $codigo,
			'nombre' => $nombre,
			'fotografia' => $fotografia,
			'observaciones' => $observaciones
		);
		if($this->db->update('material_item',$datos,array('idmi'=>$idmi))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('material_item',['idmi' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function max_id(){
		$col="max(idmi)as max";
		$this->db->select($col);
		$this->db->from("material_item");
		$query=$this->db->get();
		return $query->result();
	}
	function max($col){// en uso, ACTIVO FIJO,
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM material_item");
		return $query->result();
	}	
	
}

/* End of file m_material_item.php */
/* Location: ./application/models/m_material_item.php*/