<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_ayudante extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('ayudante');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('ayudante',['iday' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM ayudante WHERE iday='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->query("SELECT * FROM ayudante WHERE $col='$val'");
		return $query->result();
	}
	function get_row_complet($col,$val){
		$cols="e.ide,e.idtc,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.tipo,
			p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
			pa.idpa,pa.nombre as pais,
			ci.idci,ci.nombre as ciudad,ci.abreviatura,
			a.iday,a.maestro,a.ayudante,a.fecha";
		$this->db->select($cols);
		$this->db->from("ayudante a");
		$this->db->join("empleado e","e.ide=a.ayudante","inner");
		$this->db->join("persona p","p.ci=e.ci","inner");
		$this->db->join('ciudad ci','ci.idci = p.idci','inner');
		$this->db->join('pais pa','pa.idpa = ci.idpa','inner');
		$this->db->where("$col = '$val'");
		$query=$this->db->get();
		return $query->result();
	}
	function get_all_ayudantes($ide){//EN USO AYUDANTES 
		$cols="e.ide,e.idtc,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.tipo,
			p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
			pa.idpa,pa.nombre as pais,
			ci.idci,ci.nombre as ciudad,ci.abreviatura,
			a.iday,a.maestro,a.ayudante,a.fecha";
		$this->db->select($cols);
		$this->db->from("ayudante a");
		$this->db->join("empleado e","e.ide=a.ayudante","inner");
		$this->db->join("persona p","p.ci=e.ci","inner");
		$this->db->join('ciudad ci','ci.idci = p.idci','inner');
		$this->db->join('pais pa','pa.idpa = ci.idpa','inner');
		$this->db->where("e.ide != '$ide'");
		$query=$this->db->get();
		return $query->result();
	}
	function get_row_2n($col,$val,$col2,$val2){
		$this->db->select("*");
		$this->db->from("ayudante");
		$this->db->where("$col = '$val'");
		$this->db->where("$col2 = '$val2'");
		$query=$this->db->get();
		return $query->result();
	}

	function get_de_maestro($ide){
		$cols="e.ide,e.idtc,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.tipo,
			p.ci,p.ciudad,p.pais,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas";
		$this->db->select($cols);
		$this->db->from("ayudante a");
		$this->db->join("empleado e","e.ide=a.ayudante","inner");
		$this->db->join("persona p","p.ci=e.ci","inner");
		$this->db->where("a.maestro = '$ide'");
		$this->db->order_by("p.nombre");
		$query=$this->db->get();
		return $query->result();
	}

	function insertar($maestro,$ayudante,$fecha){
		$datos=array(
			'maestro' => $maestro,
			'ayudante' => $ayudante,
			'fecha' => $fecha
		);
		if($this->db->insert('ayudante',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$maestro,$ayudante,$fecha){
		$datos=array(
			'maestro' => $maestro,
			'ayudante' => $ayudante,
			'fecha' => $fecha
		);
		if($this->db->update('ayudante',$datos,array('iday'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('ayudante',['iday' => $id])){
			return true;
		}else{
			return false;
		}
	}	
}

/* End of file m_ayudante.php */
/* Location: ./application/models/m_ayudante.php*/