<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_material_varios extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('material_varios');
		return $query->result();
	}
	function get($id){
		$col="mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
			m.idmv";
		$this->db->select($col);
		$this->db->from("material_item mi");
		$this->db->where("m.idmv = $id");
		$this->db->join('material_varios m','m.idmi=mi.idmi','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function material_proveedor(){// en uso: COMPRAS
		$cols="m.idmv,
			mi.idmi,mi.idu,mi.codigo,mi.nombre as nombre_material,mi.fotografia,mi.observaciones as obs_material,
			u.idu,u.nombre as nombre_u,u.abreviatura,
			mp.idmp,mp.costo_unitario as costo_material_proveedor,
			pr.idpro,pr.nit,pr.encargado,pr.url,
			p.ci,p.idci,p.nombre as nombre_proveedor,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas";
		$this->db->select($cols);
		$this->db->from("material_varios m");
		$this->db->join('material_item mi','mi.idmi=m.idmi','inner');
		$this->db->join('unidad u','u.idu=mi.idu','inner');
		$this->db->join('material_proveedor mp','mp.idmi=mi.idmi','inner');
		$this->db->join('proveedor pr','pr.idpro=mp.idpro','inner');
		$this->db->join('persona p','p.ci=pr.nit','inner');
		$this->db->group_by("p.ci");
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM material_varios WHERE idmv='$id'");
		return $query->result();
	}
	function get_material_item($idmi){// en uso: INSUMOS,
		$col="mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
			u.idu,u.nombre as nombre_u,u.abreviatura,
			m.idmV";
		$this->db->select($col);
		$this->db->from("material_item mi");
		$this->db->where("mi.idmi = '$idmi'");
		$this->db->join('material_varios m','m.idmi=mi.idmi','inner');
		$this->db->join('unidad u','u.idu=mi.idu','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function search($col,$val){// en uso: INSUMO
		$cols="mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
				u.idu,u.nombre as nombre_u,u.abreviatura,
				m.idmv";
		$this->db->select($cols);
		$this->db->from("material_item mi");
		$this->db->join('material_varios m','m.idmi=mi.idmi','inner');
		$this->db->join('unidad u','u.idu=mi.idu','inner');
		if($col=="idu"){ $this->db->where("$col = '$val'"); }
		else{ $this->db->where("$col like '$val%'"); }
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}	
	function insertar($idmi){
		$datos=array(
			'idmi' => $idmi
		);
		if($this->db->insert('material_varios',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idmi){
		$datos=array(
			'idmi' => $idmi
		);
		if($this->db->update('material_varios',$datos,array('idmv'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('material_varios',['idmv' => $id])){
			return true;
		}else{
			return false;
		}
	}	
}

/* End of file m_material_varios.php */
/* Location: ./application/models/m_material_varios.php*/