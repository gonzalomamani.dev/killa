<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_gasto extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('gasto');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('gasto',['idga' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM gasto WHERE idga='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->query("SELECT * FROM gasto WHERE $col='$val'");
		return $query->result();
	}
	function get_complet(){

	}
	function insertar($idga,$ide,$orden_gasto,$fecha,$tipo,$observaciones){
		if($orden_gasto!=""){
			$datos=array(
				'idga' => $idga,
				'ide' => $ide,
				'orden_gasto' => $orden_gasto,
				'fecha' => $fecha,
				'tipo' => $tipo,
				'observaciones' => $observaciones
			);
		}else{
			$datos=array(
				'idga' => $idga,
				'ide' => $ide,
				'fecha' => $fecha,
				'tipo' => $tipo,
				'observaciones' => $observaciones
			);
		}

		if($this->db->insert('gasto',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$ide,$fecha,$tipo,$observaciones){
		$datos=array(
				'ide' => $ide,
				'fecha' => $fecha,
				'tipo' => $tipo,
				'observaciones' => $observaciones
		);
		if($this->db->update('gasto',$datos,array('idga'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('gasto',['idga' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function max_id($col){
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM gasto");
		return $query->result();
	}	
}

/* End of file m_gasto.php */
/* Location: ./application/models/m_gasto.php*/