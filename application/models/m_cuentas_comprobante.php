<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_cuentas_comprobante extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('cuentas_comprobante');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('cuentas_comprobante',['idci' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM cuentas_comprobante WHERE idci='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('cuentas_comprobante',[$col => $val]);
		return $query->result();
	}
	function get_row_2n($col,$val,$col2,$val2){
		$query=$this->db->get_where('cuentas_comprobante',[$col => $val,$col2 => $val2]);
		return $query->result();
	}
	function get_egreso(){
		$col="*";
		$this->db->select($col);
		$this->db->from("cuentas_comprobante cc");
		$this->db->where("cc.cuenta = 'E'");
		$this->db->order_by("cc.posicion", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get_ingreso(){
		$col="*";
		$this->db->select($col);
		$this->db->from("cuentas_comprobante cc");
		$this->db->where("cc.cuenta = 'I'");
		$this->db->order_by("cc.posicion", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	
	function insertar($idpl,$posicion,$porcentaje,$tipo,$cuenta,$control){
		$datos=array(
			'idpl' => $idpl,
			'posicion' => $posicion,
			'porcentaje' => $porcentaje,
			'tipo' => $tipo,
			'cuenta' => $cuenta,
			'control' => $control
		);
		if($this->db->insert('cuentas_comprobante',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idpl,$posicion,$porcentaje,$tipo,$cuenta){
		if($idpl==""){ $idpl=NULL; $control=0;}else{ $control=1;}
		$datos=array(
			'idpl' => $idpl,
			'posicion' => $posicion,
			'porcentaje' => $porcentaje,
			'tipo' => $tipo,
			'cuenta' => $cuenta,
			'control' => $control
		);
		if($this->db->update('cuentas_comprobante',$datos,array('idci'=>$id))){
			return true;
		}else{
			return false;
		}
	}

	function modificar_row($id,$col,$val){
		$datos=array(
			$col => $val
		);
		if($this->db->update('cuentas_comprobante',$datos,array('idci'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('cuentas_comprobante',['idci' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function max_id_2n($col,$col2,$val){
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM cuentas_comprobante WHERE $col2 = '$val'");
		return $query->result();
	}		
}

/* End of file m_cuentas_comprobante.php */
/* Location: ./application/models/m_cuentas_comprobante.php*/