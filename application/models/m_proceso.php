<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_proceso extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){// en uso Produccion,
		$this->db->order_by("nombre","asc");
		$query=$this->db->get('proceso');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('proceso',['idpr' => $id]);
		return $query->result();
	}

	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM proceso WHERE idpr='$id'");
		return $query->result();
	}

	function insertar($nombre,$detalle){
		$datos=array(
			'nombre' => $nombre,
			'detalle' => $detalle
		);
		if($this->db->insert('proceso',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$nombre,$detalle){
		$datos=array(
			'nombre' => $nombre,
			'detalle' => $detalle
		);
		if($this->db->update('proceso',$datos,array('idpr' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('proceso',['idpr' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}