<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_material_adicional extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('material_adicional');
		return $query->result();
	}
	function get_material_item($idmi){// en uso: MATERIALES ADICIONALES
		$col="mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
			u.idu,u.nombre as nombre_u,u.abreviatura,
			m.idma";
		$this->db->select($col);
		$this->db->from("material_item mi");
		$this->db->where("mi.idmi = '$idmi'");
		$this->db->join('material_adicional m','m.idmi=mi.idmi','inner');
		$this->db->join('unidad u','u.idu=mi.idu','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM material_adicional WHERE idma='$id'");
		return $query->result();
	}
	function search_material_adicional($col,$val){// en uso: PROVEEDOR,INSUMO
		$cols="mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
				u.idu,u.nombre as nombre_u,u.abreviatura,
				m.idma";
		$this->db->select($cols);
		$this->db->from("material_item mi");
		$this->db->join('material_adicional m','m.idmi=mi.idmi','inner');
		$this->db->join('unidad u','u.idu=mi.idu','inner');
		if($col=="idu"){ $this->db->where("$col = '$val'"); }
		else{ $this->db->where("$col like '$val%'"); }
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	function material_proveedor(){// en uso: COMPRAS
		$cols="m.idma,
			mi.idmi,mi.idu,mi.codigo,mi.nombre as nombre_material,mi.fotografia,mi.observaciones as obs_material,
			u.idu,u.nombre as nombre_u,u.abreviatura,
			mp.idmp,mp.costo_unitario as costo_material_proveedor,
			pr.idpro,pr.nit,pr.encargado,pr.url,
			p.ci,p.idci,p.nombre as nombre_proveedor,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas";
		$this->db->select($cols);
		$this->db->from("material_adicional m");
		$this->db->join('material_item mi','mi.idmi=m.idmi','inner');
		$this->db->join('unidad u','u.idu=mi.idu','inner');
		$this->db->join('material_proveedor mp','mp.idmi=mi.idmi','inner');
		$this->db->join('proveedor pr','pr.idpro=mp.idpro','inner');
		$this->db->join('persona p','p.ci=pr.nit','inner');
		$this->db->group_by("p.ci");
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idmi){
		$datos=array(
			'idmi' => $idmi
		);
		if($this->db->insert('material_adicional',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idmi){
		$datos=array(
			'idmi' => $idmi
		);
		if($this->db->update('material_adicional',$datos,array('idma'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('material_adicional',['idma' => $id])){
			return true;
		}else{
			return false;
		}
	}	
}

/* End of file m_material_adicional.php */
/* Location: ./application/models/m_material_adicional.php*/