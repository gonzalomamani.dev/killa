<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_material_proveedor extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('material_proveedor');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('material_proveedor',['idmp' => $id]);
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('material_proveedor',[$col => $val]);
		return $query->result();
	}
	function costo_promedio($idmi){
		$cols="AVG(costo_unitario) as costo";
		$this->db->select($cols);
		$this->db->from("material_proveedor");
		$this->db->where("idmi = '$idmi'");
		$this->db->group_by('idmi');
		$query=$this->db->get();
		return $query->result();
	}
	function get_proveedor($idmp){
		$cols="mp.idmp,mp.costo_unitario,
			p.idpro,p.nit,p.encargado,p.url";
		$this->db->select($cols);
		$this->db->from("material_proveedor mp");
		$this->db->where("mp.idmp = '$idmp'");
		$this->db->join('proveedor p','p.idpro=mp.idpro','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_material_item($idmp){// en uso: PROVEEDOR,
		$cols="mp.idmp,mp.costo_unitario,
			p.idpro,p.nit,p.encargado,p.url,
			mi.idmi,mi.codigo,mi.nombre,mi.fotografia";
		$this->db->select($cols);
		$this->db->from("material_proveedor mp");
		$this->db->where("mp.idmp = '$idmp'");
		$this->db->join('material_item mi','mp.idmi=mi.idmi','inner');
		$this->db->join('proveedor p','p.idpro=mp.idpro','inner');
		$query=$this->db->get();
		return $query->result();	
	}

	function get_material($idpro,$tipo){// en uso: proveedor,COMPRAS
		$cols="mi.idmi,mi.codigo,mi.nombre,mi.fotografia,
			mp.idmp,mp.idpro,mp.costo_unitario,";
		if($tipo=="material"){ $cols.="m.idm,m.costo_unitario as costo_unitario_material,c.nombre as nombre_color,c.codigo as codigo_color";}
		if($tipo=="material_adicional"){ $cols.="m.idma";}
		if($tipo=="material_vario"){ $cols.="m.idmv";}
		if($tipo=="activo_fijo"){ $cols.="m.idaf,m.iddpre,m.costo,m.fecha_inicio_trabajo";}
		$this->db->select($cols);
		$this->db->from("material_proveedor mp");
		$this->db->where("mp.idpro = '$idpro'");
		$this->db->join('material_item mi','mp.idmi=mi.idmi','inner');
		if($tipo=="material"){ $this->db->join('material m','m.idmi=mi.idmi','inner');$this->db->join('color c','c.idco=m.idco','inner');}
		if($tipo=="material_adicional"){ $this->db->join('material_adicional m','m.idmi=mi.idmi','inner');}
		if($tipo=="material_vario"){ $this->db->join('material_varios m','m.idmi=mi.idmi','inner');}
		if($tipo=="activo_fijo"){ $this->db->join('activo_fijo m','m.idmi=mi.idmi','inner');}
		$this->db->order_by('mi.nombre');
		$query=$this->db->get();
		return $query->result();
	}

	function get_material_row($col,$val,$tipo){// en uso: proveedor,COMPRAS
		$cols="mi.idmi,mi.codigo,mi.nombre,mi.fotografia,
			mp.idmp,mp.idpro,mp.costo_unitario,
			u.idu,u.nombre as nombre_u,u.abreviatura,";
		if($tipo=="material"){ $cols.="m.idm,m.costo_unitario as costo_unitario_material,c.nombre as nombre_color,c.codigo as codigo_color";}
		if($tipo=="material_adicional"){ $cols.="m.idma";}
		if($tipo=="material_vario"){ $cols.="m.idmv";}
		if($tipo=="activo_fijo"){ $cols.="m.idaf,m.iddpre,m.costo,m.fecha_inicio_trabajo";}
		$this->db->select($cols);
		$this->db->from("material_proveedor mp");
		$this->db->where("$col = '$val'");
		$this->db->join('material_item mi','mp.idmi=mi.idmi','inner');
		$this->db->join('unidad u','u.idu=mi.idu','inner');
		if($tipo=="material"){ $this->db->join('material m','m.idmi=mi.idmi','inner');$this->db->join('color c','c.idco=m.idco','inner');}
		if($tipo=="material_adicional"){ $this->db->join('material_adicional m','m.idmi=mi.idmi','inner');}
		if($tipo=="material_vario"){ $this->db->join('material_varios m','m.idmi=mi.idmi','inner');}
		if($tipo=="activo_fijo"){ $this->db->join('activo_fijo m','m.idmi=mi.idmi','inner');}
		$this->db->order_by('mi.nombre');
		$query=$this->db->get();
		return $query->result();
	}
	function get_row_material($col,$val){
		$cols="mi.idmi,mi.codigo,mi.nombre,mi.fotografia";
		$this->db->select($cols);
		$this->db->from("material_proveedor mp");
		$this->db->where("$col = '$val'");
		$this->db->join('material_item mi','mp.idmi=mi.idmi','inner');
		$this->db->join('material m','m.idmi=mi.idmi','inner');
		$this->db->join('proveedor p','p.idpro=mp.idpro','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_material_adicional($col,$val){
		$cols="mi.idmi,mi.codigo,mi.nombre,mi.fotografia";
		$this->db->select($cols);
		$this->db->from("material_proveedor mp");
		$this->db->where("$col = '$val'");
		$this->db->join('material_item mi','mp.idmi=mi.idmi','inner');
		$this->db->join('material_adicional m','m.idmi=mi.idmi','inner');
		$this->db->join('proveedor p','p.idpro=mp.idpro','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_activo_fijo($col,$val){
		$cols="mi.idmi,mi.codigo,mi.nombre,mi.fotografia";
		$this->db->select($cols);
		$this->db->from("material_proveedor mp");
		$this->db->where("$col = '$val'");
		$this->db->join('material_item mi','mp.idmi=mi.idmi','inner');
		$this->db->join('activo_fijo m','m.idmi=mi.idmi','inner');
		$this->db->join('proveedor p','p.idpro=mp.idpro','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_material_varios($col,$val){
		$cols="mi.idmi,mi.codigo,mi.nombre,mi.fotografia";
		$this->db->select($cols);
		$this->db->from("material_proveedor mp");
		$this->db->where("$col = '$val'");
		$this->db->join('material_item mi','mp.idmi=mi.idmi','inner');
		$this->db->join('material_varios m','m.idmi=mi.idmi','inner');
		$this->db->join('proveedor p','p.idpro=mp.idpro','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM material_proveedor WHERE idmp='$id'");
		return $query->result();
	}
	function get_row_2n($col,$val,$col2,$val2){// en uso PROVEEDOR,
		$cols="*";
		$this->db->select($cols);
		$this->db->from("material_proveedor mp");
		$this->db->where("$col = '$val'");
		$this->db->where("$col2 = '$val2'");
		$query=$this->db->get();
		return $query->result();
	}


	function insertar($idpro,$idmi,$costo_unitario,$fecha){
		$datos=array(
			'idpro' => $idpro,
			'idmi' => $idmi,
			'costo_unitario' => $costo_unitario,
			'fecha' => $fecha
		);
		if($this->db->insert('material_proveedor',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function set_row($id,$col,$val){
		$datos=array(
			$col => $val,
		);
		if($this->db->update('material_proveedor',$datos,array('idmp' => $id))){
			return true;
		}else{
			return false;
		}
	}
	/*function get_material($idpro){
		$cols="mp.idmp,mp.idpro, mp.costo, mp.fecha,
				m.ida,m.idg,m.idu,m.idco,m.nombre,m.c_u,m.cantidad,m.descripcion,m.cod,m.fotografia";
		$this->db->select($cols);
		$this->db->from("material_proveedor mp");
		$this->db->where("mp.idpro = '$idpro'");
		$this->db->order_by("m.nombre", "asc");
		$this->db->join('material m','m.idi = mp.idi','inner');
		$query=$this->db->get();
		return $query->result();
	}*/


	function modificar($id,$idpro,$idmi,$costo){
	if(!isset($costo)){
		$datos=array(
			'idpro' => $idpro,
			'idmi' => $idmi
		);
	}else{
		$datos=array(
			'idpro' => $idpro,
			'idmi' => $idmi,
			'costo_unitario' => $costo
		);
	}
		if($this->db->update('material_proveedor',$datos,array('idmp' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_row_id($id,$atrib,$val){
		$datos=array(
			$atrib => $val
		);
		if($this->db->update('material_proveedor',$datos,array('idmp' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('material_proveedor',['idmp' => $id])){
			return true;
		}else{
			return false;
		}
	}
}

/* End of file m_material_proveedor.php */
/* Location: ./application/models/m_material_proveedor.php*/