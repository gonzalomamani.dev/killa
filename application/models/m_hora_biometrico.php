<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_hora_biometrico extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('hora_biometrico');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('hora_biometrico',['idhb' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM hora_biometrico WHERE idhb='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->query("SELECT * FROM hora_biometrico WHERE $col='$val'");
		return $query->result();
	}
	function get_row_3n($col,$val,$col2,$val2,$col3,$val3){
		$this->db->select("*");
	    $this->db->from("hora_biometrico");
	    $this->db->where("$col = '$val'");
	    $this->db->where("$col2 = '$val2'");
	    $this->db->where("$col3 = '$val3'");
	    $query=$this->db->get();
	    return $query->result();
	}
	function exist_fecha($col,$val,$f1,$f2){
		$this->db->select("*");
	    $this->db->from("hora_biometrico");
		if($col!="" && $val!=""){ $this->db->where("$col = '$val'"); }
	    $this->db->where("fecha >= '$f1'");
	    $this->db->where("fecha <= '$f2'");
	    $this->db->order_by("ide");
	    $this->db->order_by("fecha");
	    $this->db->order_by("hora");
	    $query=$this->db->get();
	    return $query->result();		
	}
	function horas_antes($ide,$fecha_hora){
		$fecha=explode(" ", $fecha_hora);
		$this->db->select("*");
	    $this->db->from("hora_biometrico");
	    $this->db->where("ide = '$ide'");
	    $this->db->where("fecha = '$fecha[0]'");
	    $this->db->where("hora <= '$fecha[1]'");
	    $this->db->order_by("fecha");
	    $this->db->order_by("hora");
	    $query=$this->db->get();
	    return $query->result();		
	}
	function horas_despues($ide,$fecha_hora){
		$fecha=explode(" ", $fecha_hora);
		$this->db->select("*");
	    $this->db->from("hora_biometrico");
	    $this->db->where("ide = '$ide'");
	    $this->db->where("fecha = '$fecha[0]'");
	    $this->db->where("hora >= '$fecha[1]'");
	    $this->db->order_by("fecha");
	    $this->db->order_by("hora");
	    $query=$this->db->get();
	    return $query->result();		
	}

	function insertar($ide,$fecha,$hora){
		$datos=array(
			'ide' => $ide,
			'fecha' => $fecha,
			'hora' => $hora,
		);
		if($this->db->insert('hora_biometrico',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$ide,$fecha,$hora){
		$datos=array(
			'ide' => $ide,
			'fecha' => $fecha,
		);
		if($this->db->update('hora_biometrico',$datos,array('idhb'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('hora_biometrico',['idhb' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function eliminar_rango($f1,$f2){
		if($query=$this->db->query("DELETE FROM hora_biometrico WHERE fecha>='$f1' AND fecha<='$f2'")){
			return true;
		}else{
			return false;
		}
	}
	function max_id($col){
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM hora_biometrico");
		return $query->result();
	}	
}

/* End of file m_hora_biometrico.php */
/* Location: ./application/models/m_hora_biometrico.php*/