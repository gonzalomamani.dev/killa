<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_plan_cuenta extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){//en uso plan
		$col="p.idpl,p.grupo,p.sub_grupo,p.rubro,p.cuenta,p.cuenta_text, CONCAT((IFNULL((p.grupo),(''))),(IFNULL((p.sub_grupo),(''))),(IFNULL((p.rubro),(''))),(IFNULL((p.cuenta),('')))) AS codigo,p.tipo,p.gasto,p.venta,";
		$this->db->select($col);
		$this->db->from("plan_cuenta p");
		$this->db->order_by("p.grupo", "asc");
		$this->db->order_by("p.sub_grupo", "asc");
		$this->db->order_by("p.rubro", "asc");
		$this->db->order_by("p.cuenta", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get($id){
		$cols="p.idpl,p.grupo,p.sub_grupo,p.rubro,p.cuenta,p.cuenta_text, CONCAT((IFNULL((p.grupo),(''))),(IFNULL((p.sub_grupo),(''))),(IFNULL((p.rubro),(''))),(IFNULL((p.cuenta),('')))) AS codigo,p.tipo,p.gasto,p.venta,";
		$this->db->select($cols);
		$this->db->from("plan_cuenta p");
		$this->db->where("idpl = '$id'");
		$query=$this->db->get();
		return $query->result();
	}
	function get_col($id, $col){
		$query=$this->db->query("SELECT $col FROM plan_cuenta WHERE idpl='$id'");
		return $query->result();
	}
	function get_row($col,$val){// en uso COMPROBANTES
		$query=$this->db->get_where('plan_cuenta',[$col => $val]);
		return $query->result();
	}
	function get_col_1n($col,$val){
		$cols="*";
		$this->db->select($cols);
		$this->db->from("plan_cuenta");
		$this->db->where("$col = '$val' LIMIT 1");
		$query=$this->db->get();
		return $query->result();
	}
	function get_col_2n($col,$val,$col2,$val2){
		$cols="*";
		$this->db->select($cols);
		$this->db->from("plan_cuenta");
		$this->db->where("$col = '$val'");
		$this->db->where("$col2 = '$val2' LIMIT 1");
		$query=$this->db->get();
		return $query->result();
	}
	function get_col_3n($col,$val,$col2,$val2,$col3,$val3){
		$cols="*";
		$this->db->select($cols);
		$this->db->from("plan_cuenta");
		$this->db->where("$col = '$val'");
		$this->db->where("$col2 = '$val2'");
		$this->db->where("$col3 = '$val3' LIMIT 1");
		$query=$this->db->get();
		return $query->result();
	}
	function get_col_4n($col,$val,$col2,$val2,$col3,$val3,$col4,$val4){// en uso: contabilidad-Plan Cuentas
		$cols="*";
		$this->db->select($cols);
		$this->db->from("plan_cuenta");
		$this->db->where("$col = '$val' and $col2 = '$val2' and $col3 = '$val3' and $col4 = '$val4'");
		$query=$this->db->get();
		return $query->result();
	}
	function get_col_4n_id($col,$val,$col2,$val2,$col3,$val3,$col4,$val4,$idpl){// en uso: contabilidad-Plan Cuentas
		$cols="*";
		$this->db->select($cols);
		$this->db->from("plan_cuenta");
		$this->db->where("$col = '$val' and $col2 = '$val2' and $col3 = '$val3' and $col4 = '$val4' and idpl != '$idpl'");
		$query=$this->db->get();
		return $query->result();
	}
	function get_search($col,$val){//En uso: contabilidad->plan cuentas, contabilidad->configuracion
		$cols="p.idpl,p.grupo,p.sub_grupo,p.rubro,p.cuenta,p.cuenta_text, CONCAT((IFNULL((p.grupo),(''))),(IFNULL((p.sub_grupo),(''))),(IFNULL((p.rubro),(''))),(IFNULL((p.cuenta),('')))) AS codigo,p.tipo,p.gasto,p.venta,";
		$this->db->select($cols);
		$this->db->from("plan_cuenta p");
		if($col=='p.grupo' || $col=='p.gasto' || $col=='p.venta'){ $this->db->where("$col = '$val'"); }
		if($col=="p.cuenta_text"){ $this->db->where("$col like '$val%'"); }
		if($col=="codigo"){ $this->db->where("CONCAT((IFNULL((p.grupo),(''))),(IFNULL((p.sub_grupo),(''))),(IFNULL((p.rubro),(''))),(IFNULL((p.cuenta),('')))) like '$val%'"); }
		$this->db->order_by("p.grupo", "asc");
		$this->db->order_by("p.sub_grupo", "asc");
		$this->db->order_by("p.rubro", "asc");
		$this->db->order_by("p.cuenta", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function sub_grupos(){
		$col="*";
		$this->db->select($col);
		$this->db->from("plan_cuenta p");
		$this->db->group_by("p.sub_grupo");
		$query=$this->db->get();
		return $query->result();
	}
	function rubros(){
		$col="*";
		$this->db->select($col);
		$this->db->from("plan_cuenta p");
		$this->db->group_by("p.rubro");
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($grupo,$sub_grupo,$rubro,$cuenta,$cuenta_text,$tipo){
		$datos=array(
			'grupo' => $grupo,
			'sub_grupo' => $sub_grupo,
			'rubro' => $rubro,
			'cuenta' => $cuenta,
			'cuenta_text' => $cuenta_text,
			'tipo' => $tipo,
		);
		if($this->db->insert('plan_cuenta',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function set_row($id,$row,$val){
		$datos=array(
			$row => $val,
		);
		if($this->db->update('plan_cuenta',$datos,array('idpl' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$grupo,$sub_grupo,$rubro,$cuenta,$cuenta_text,$tipo){
		$datos=array(
			'grupo' => $grupo,
			'sub_grupo' => $sub_grupo,
			'rubro' => $rubro,
			'cuenta' => $cuenta,
			'cuenta_text' => $cuenta_text,
			'tipo' => $tipo
		);
		if($this->db->update('plan_cuenta',$datos,array('idpl' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('plan_cuenta',array('idpl' => $id))){
			return true;
		}else{
			return false;
		}
	}
}
/* End of file M_plan_cuenta.php */
/* Location ./application/models/M_plan_cuenta.php */