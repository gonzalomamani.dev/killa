<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto_grupo extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("nombre","asc");
		$query=$this->db->get('producto_grupo');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('producto_grupo',['idpg' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM producto_grupo WHERE idpg='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('producto_grupo',[$col => $val]);
		return $query->result();
	}
	function insertar($numero,$nombre,$descripcion){
		$datos=array(
			'numero' => $numero,
			'nombre' => $nombre,
			'descripcion' => $descripcion
		);
		if($this->db->insert('producto_grupo',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$numero,$nombre,$descripcion){
		$datos=array(
			'numero' => $numero,
			'nombre' => $nombre,
			'descripcion' => $descripcion
		);
		if($this->db->update('producto_grupo',$datos,array('idpg' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('producto_grupo',['idpg' => $id])){
			return true;
		}else{
			return false;
		}
	}
}

/* End of file m_producto_grupo.php */
/* Location: ./application/models/m_producto_grupo.php*/