<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_movimiento_almacen extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all($tipo){
		$cols="idhi,fecha_accion,date(fecha_accion) as fecha,time(fecha_accion) as hora,usuario,almacen,tipo,cod,nombre,fecha_usuario,cantidad,ingreso,salida";
		$this->db->select($cols);
		$this->db->from("movimiento_almacen");
		$this->db->where("tipo = '$tipo'");
		$this->db->order_by('fecha_accion');
		$query=$this->db->get();
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('movimiento_almacen',['idhi' => $id]);
		return $query->result();
	}

	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM movimiento_almacen WHERE idhi='$id'");
		return $query->result();
	}
	function get_row($col,$row,$tipo){
		$query=$this->db->query("SELECT * FROM movimiento_almacen WHERE $col like '$row%' and tipo='$tipo'");
		return $query->result();
	}
	function get_search($tipo,$where){//en uso movimiento material, 
		$cols="idhi,fecha_accion,date(fecha_usuario) as fecha,time(fecha_usuario) as hora,usuario,almacen,tipo,cod,nombre,fecha_usuario,cantidad,ingreso,salida,ci,solicitante,observaciones";
		$this->db->select($cols);
	    $this->db->from("movimiento_almacen");
	    $this->db->where("tipo = '$tipo'");
	    if($where!=""){ $this->db->where($where); }
	    $this->db->order_by("fecha_usuario","asc");
	    $query=$this->db->get();
	    return $query->result();
	}
	function insertar($almacen,$tipo,$usuario,$cod,$nombre,$fecha_usuario,$cantidad,$ingreso,$salida,$ci,$solicitante,$observaciones){
		$datos=array(
			'usuario' => $usuario,
			'almacen' => $almacen,
			'tipo' => $tipo,
			'cod' => $cod,
			'nombre' => $nombre,
			'fecha_usuario' => $fecha_usuario,
			'cantidad' => $cantidad,
			'ingreso' => $ingreso,
			'salida' => $salida,
			'ci' => $ci,
			'solicitante' => $solicitante,
			'observaciones' => $observaciones
		);
		if($this->db->insert('movimiento_almacen',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('movimiento_almacen',['idhi' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function eliminar_row($col,$val){
		if($this->db->delete('movimiento_almacen',[$col => $val])){
			return true;
		}else{
			return false;
		}
	}
}

/* End of file m_historial_insumo.php */
/* Location: ./application/models/m_historial_insumo.php*/