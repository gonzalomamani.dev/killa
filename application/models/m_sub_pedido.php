<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_sub_pedido extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("numero", "asc");
		$query=$this->db->get('sub_pedido');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('sub_pedido',['idsp' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM sub_pedido WHERE idsp='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('sub_pedido',[$col => $val]);
		return $query->result();
	}
	function get_row_2n($col,$val,$col2,$val2){
		$query=$this->db->get_where('sub_pedido',[$col => $val,$col2 => $val2]);
		return $query->result();
	}
	function get_material_requerido($idpe){
		$cols="sp.idsp,
			dp.iddp,dp.cantidad as cantidad_pedida,
			cp.idpim,
			cpi.idcp,
			p.idp,p.cod as codigo, p.nombre,
			pm.idpm,pm.idm, pm.cantidad as cantidad_requerida,
			m.idco,
			mi.codigo as codigo_material, mi.nombre as nombre_material, mi.fotografia,mi.idu";
		$this->db->select($cols);
		$this->db->from("sub_pedido sp");
		$this->db->join('detalle_pedido dp','sp.idsp = dp.idsp','inner');
		$this->db->join('categoria_producto cp','dp.idpim = cp.idpim','inner');
		$this->db->join('categoria_pieza cpi','cp.idcp = cpi.idcp','inner');
		$this->db->join('producto p','cpi.idp = p.idp','inner');
		$this->db->join('producto_material pm','p.idp = pm.idp','inner');
		$this->db->join('material m','pm.idm = m.idm','inner');
		$this->db->join('material_item mi','mi.idmi = m.idmi','inner');
		$this->db->where("sp.idpe = '$idpe'");
		$this->db->order_by("mi.nombre","asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get_productos($idpe){
		$query=$this->db->query("SELECT sp.idsp,
		dp.iddp,dp.cantidad as cantidad_pedida,
		cp.idpim,
		c.idco,c.nombre as nombre_color,
		cpi.idcp,
		p.idp,p.cod as codigo, p.nombre,p.descripcion,SUBSTRING_INDEX(p.cod,' ',1) as o_cod,SUBSTRING_INDEX(p.cod,' ', -1) as o_num,
		pg.idpg, pg.nombre as nombre_grupo
		FROM sub_pedido sp, detalle_pedido dp, categoria_producto cp,color c,categoria_pieza cpi, producto p, producto_grupo pg
		WHERE idpe='$idpe' and sp.idsp=dp.idsp and dp.idpim=cp.idpim and cp.idco=c.idco and cp.idcp=cpi.idcp and cpi.idp=p.idp and p.idpg=pg.idpg
		order by o_cod asc,o_num+0 asc");
		return $query->result();
	}
	function insertar($idsp,$idpe,$numero,$nombre,$fecha){
		$datos=array(
			'idsp' => $idsp,
			'idpe' => $idpe,
			'numero' => $numero,
			'nombre' => $nombre,
			'fecha' => $fecha
		);
		if($this->db->insert('sub_pedido',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($idsp,$fecha,$observaciones){
		$datos=array(
			'fecha' => $fecha,
			'observaciones' => $observaciones
		);
		if($this->db->update('sub_pedido',$datos,array('idsp'=>$idsp))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('sub_pedido',['idsp' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function max($col){
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM sub_pedido");
		return $query->result();
	}
	
}

/* End of file m_sub_pedido.php */
/* Location: ./application/models/m_sub_pedido.php*/