<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_anticipo extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('anticipo');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('anticipo',['idan' => $id]);
		return $query->result();
	}
	function get_anio($ide,$anio){// en uso Capital Humano,
       	$this->db->select("*");
       	$this->db->from("anticipo");
        $this->db->order_by("fecha_anticipo", "asc");
        $this->db->where("ide = '$ide'");
        $this->db->where("year(fecha_anticipo) = '$anio'");
        $query=$this->db->get();
        return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM anticipo WHERE idan='$id'");
		return $query->result();
	}

	function get_row($col,$val){
		//$this->db->get_where('anticipo',[$col => $val]);
       	$this->db->select("*");
       	$this->db->from("anticipo");
        $this->db->order_by("idan", "desc");
        $this->db->where("$col = $val");
        $query=$this->db->get();
        return $query->result();
	}
	function get_empleado_fecha($ide,$fini,$ffin){// en uso DETALLE DE ANTICIPOS,
		$this->db->select("*");
       	$this->db->from("anticipo");
        $this->db->where("ide = '$ide'");
        $this->db->where("fecha_anticipo >= '$fini'");
        $this->db->where("fecha_anticipo <= '$ffin'");
        $this->db->order_by("fecha_anticipo", "asc");
        $this->db->order_by("idan", "desc");
        $query=$this->db->get();
		return $query->result();
	}

	function get_empleado_mes_anio($ide,$mes,$anio){
		$query=$this->db->query("SELECT * FROM  `anticipo` WHERE ide =  '$ide' AND month(fecha_anticipo)='$mes' AND year(fecha_anticipo)<='$anio'");
		return $query->result();
	}
	
	function insertar($ide,$fecha_anticipo,$monto,$descripcion){
		$datos=array(
			'ide' => $ide,
			'fecha_anticipo'=>$fecha_anticipo,
			'monto'=>$monto,
			'descripcion' => $descripcion
		);
		if($this->db->insert('anticipo',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$fecha_anticipo,$monto,$descripcion){
		$datos=array(
			'fecha_anticipo'=>$fecha_anticipo,
			'monto'=>$monto,
			'descripcion' => $descripcion
		);
		if($this->db->update('anticipo',$datos,array('idan'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('anticipo',['idan' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function minimo($col){
		if($col=='fecha_anticipo'){
			$anio=date('Y');
			$cols="year(IFNULL(min($col),".$anio.")) as min ";
		}else{
			$cols="IFNULL(min($col),0) as min ";
		}
		$query=$this->db->query("SELECT $cols FROM anticipo");
		return $query->result();
	}	
}

/* End of file m_anticipo.php */
/* Location: ./application/models/m_anticipo.php*/