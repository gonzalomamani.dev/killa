<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_permiso extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	function get_all(){
        $query = $this->db->get('permiso'); 
        return $query->result();
	}

	function get($ci){
		$query=$this->db->get_where('permiso',['ci' => $ci]);
		return $query->result();
	}

	function get_col($ci,$col){
		$query=$this->db->query("SELECT $col FROM permiso WHERE ci=$ci");
      	return $query->result();
	}

	function modifica($ci,$col){
		/*!!!!verificar la sintaxis*/
		$res=get_permiso($ci,$col);
		if($res==1){
			$data = array(
				$nom_per.'' => 0
			);
		}if($res==0){
			$data = array(
				$nom_per.'' => 1
			);
		}
		$this->db->update('permiso', $data, array('ci' => $ci));
	}
}
/* End of file m_permiso.php */
/* Location: ./application/models/m_permiso.php */