<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_insumo_caracteristica extends CI_Model(){
	function __construct(){
		parent::__construct();
	}
	
	function get_all(){
		$query=$this->db->get('insumo_caracteristica');
		return $query->result();
	}

	function get($id){
		$query=$this->db->get_where('insumo_caracteristica',array('idic' => $id));
		return $query->result();
	}

	function get_col($id, $col){
		$query=$this->db->query("SELECT $col FROM insumo_caracteristica WHERE idic='$id'");
		return $query->result();
	}

	function insertar($id_insumo, $id_caracteristica){
		$datos=array(
			'idi' => $id_insumo,
			'idc' => $id_caracteristica
		);
		$this->db->insert('insumo_caracteristica',$datos);
	}

	function modificar($id,$id_insumo,$id_caracteristica){
		$datos=array(
			'idi' => $id_insumo,
			'idc' => $id_caracteristica
		);
		$this->db->update('insumo_caracteristica',$datos,array('idic' => $id));
	}

	function eliminar($id,$id_insumo,$id_caracteristica){
		$this->db->delete('insumo_caracteristica',array('idi' => $id_insumo, 'idc' => $id_caracteristica));
	}

}

/* End of file m_insumo_caracteristica.php */
/* Location ./application/models/m_insumo_caracteristica.php */