<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_caracteristica extends CI_Model(){
	function __construct(){
			parent::__construct();
	}
	
	function get_all(){
		$query=$this->db->get('caracteristica');
		return $query->result();
	}
	
	function get($id){
		$query=$this->db->get_where('caracteristica',array('idc' => $id));
		return $query->result();
	}

	function get_col($id, $col){
		$query=$this->db->query("SELECT $col FROM caracteristica WHERE idc='$id'");
		return $query->result();
	}
	function insertar($nombre){
		$datos=array(
			'nombre' => $nombre
		);
		$this->db->insert('caracteristica',$datos)
	}
	function modificar($id, $nombre){
		$datos=array(
			'nombre' => $nombre
		);
		$this->db->update('caracteristica',$datos,array('idc' => $id));
	}
	function eliminar($id){
		$this->db->delete('caracteristica',array('idc' => $id))
	}

}

/* End of file m_caracteristica.php*/
/* Location: ./application/models/m_caracteristica.php */