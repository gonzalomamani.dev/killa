<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_opcion extends CI_Model(){
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('opcion');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('opcion',array('ido' => $id));
		return $query->result();
	}
	function get_col($id, $col){
		$query=$this->db->query("SELECT $col FROM opcion WHERE ido='$id'");
		return $query->result();
	}
	function insertar($idcar, $opcion){
		$datos=array(
			'idc' => $idcar,
			'opcion' => $opcion
		);
		$this->db->insert('opcion',$datos);
	}
	function modificar($id, $idcar, $opcion){
		$datos=array(
			'idcar' => $idcar,
			'opcion' => $opcion
		);
		$this->db->update('opcion',$datos,array('ido' => $id));
	}
	function eliminar($id){
		$this->db->delete('opcion',array('ido' => $id));
	}                                                                                                                                                                           
}
/* End of file m_opcion */
/* Location: ./application/models/m_opcion.php*/  