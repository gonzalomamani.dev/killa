<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_material_liquido extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("tipo", "asc");
		$query=$this->db->get('material_liquido');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('material_liquido',['idml' => $id]);
		return $query->result();
	}

	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM material_liquido WHERE idml='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('material_liquido',array($col => $val));
		return $query->result();
	}
	function get_row_2n($col,$val,$col2,$val2){
		$query=$this->db->get_where('material_liquido',array($col => $val,$col2 => $val2));
		return $query->result();
	}
	function get_suma_material_liquido($idp){
		$cols="cp.color_producto, 
				sum(((pi.ancho+pi.alto)*2)*pi.unidades) as area, pi.unidades,
				pg.idpig,pg.nombre as nombre_pi,
				pml.idml,
				ml.idml,ml.idm,ml.variable,ml.tipo";
		$this->db->select($cols);
		$this->db->from("categoria_pieza cp");
		$this->db->where("cp.idp = '$idp'");
		$this->db->join('pieza pi','cp.idcp = pi.idcp','inner');
		$this->db->join('pieza_grupo pg','pg.idpig = pi.idpig','inner');
		$this->db->join('pieza_material_liquido pml','pi.idpi = pml.idpi','inner');
		$this->db->join('material_liquido ml','pml.idml = ml.idml','inner');
		$this->db->group_by('pml.idml');
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idm,$variable,$tipo){
		$datos=array(
			'idm' => $idm,
			'variable' => $variable,
			'tipo' => $tipo
		);
		if($this->db->insert('material_liquido',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$variable,$tipo){
		$datos=array(
			'variable' => $variable,
			'tipo' => $tipo
		);
		if($this->db->update('material_liquido',$datos,array('idml'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('material_liquido',['idml' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}

/* End of file m_material_liquido.php */
/* Location: ./application/models/m_material_liquido.php*/