<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_detalle_pedido extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$col="dp.iddp,dp.op,dp.cantidad, dp.estado,
				p.idpe, p.idcl,p.fecha_pedido,p.fecha_entrega,p.monto,p.saldo,p.observaciones,p.pais,p.ciudad, 
				cp.idpim, cp.idpi, cp.idma, cp.portada";
		$this->db->select($col);
		$this->db->from("detalle_pedido dp");
		$this->db->join('pedido p','p.idpe = dp.idpe','inner');
		$this->db->join('categoria_producto cp','cp.idpim = dp.idpim','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_producto($idsp,$atrib,$val,$order,$control_order,$group){//en uso: PEDIDO
		$cols="dp.iddp,dp.idsp,dp.op,dp.cantidad,dp.estado,dp.cantidad_producida,dp.obs_producida,dp.cantidad_vendida,dp.obs_vendida as observaciones_vendido,
				cp.idpim, cp.idm, cp.portada,
				cpi.idcp,cpi.color_producto,
				p.idp,p.cod as codigo,p.nombre,p.descripcion,SUBSTRING_INDEX(p.cod,' ',1) as o_cod,SUBSTRING_INDEX(p.cod,' ', -1) as o_num";
		$from="detalle_pedido dp,categoria_producto cp,categoria_pieza cpi,producto p";
		$where="dp.idsp = '$idsp' and cp.idpim = dp.idpim and cpi.idcp = cp.idcp and p.idp = cpi.idp";
		if($atrib!="" && $val!=""){
			if($atrib=="dp.iddp" || $atrib=="cp.idpim" || $atrib=="p.idp"){ $where.=" and $atrib = '$val'";}
			if($atrib=="p.cod" || $atrib=="p.nombre"){ $where.=" and $atrib like '%$val%'";}
		}
		if($group!=""){ $group="group by(".$group.")"; }
		if($order!="" && ($control_order=="asc" || $control_order=="desc")){ if($order!="p.cod"){ $order="order by ".$order." ".$control_order; }else{ $order="order by o_cod ".$control_order.",o_num+0 ".$control_order;} }
		$query=$this->db->query("SELECT $cols FROM $from WHERE $where $group $order");
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('detalle_pedido',['iddp' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM detalle_pedido WHERE iddp='$id'");
		return $query->result();
	}
	function get_row($col,$val){//en uso: PRODUCTO,PEDIDO
		$query=$this->db->get_where('detalle_pedido',array($col => $val));
		return $query->result();
	}	
	function get_row_2n($row1,$val1,$row2,$val2){
		$query=$this->db->query("SELECT * FROM detalle_pedido WHERE $row1='$val1' AND $row2='$val2'");
		return $query->result();
	}
	function insertar($idsp,$idpim,$cantidad,$op){
		$datos=array(
			'idsp' => $idsp,
			'idpim' => $idpim,
			'cantidad' => $cantidad,
			'op' => $op
		);
		if($this->db->insert('detalle_pedido',$datos)){
			return true;
		}else{
			return false;
		}
	}

	function modificar($iddp,$idpe,$idpim,$cantidad){
		$datos=array(
			'idpe' => $idpe,
			'idpim' => $idpim,
			'cantidad' => $cantidad
		);
		if($this->db->update('detalle_pedido',$datos,array('iddp'=>$iddp))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_vendido($iddp,$cantidad_vendida,$obs_vendida){// en uso: NUEMVA VENTA,
		$datos=array(
			'cantidad_vendida' => $cantidad_vendida,
			'obs_vendida' => $obs_vendida
		);
		if($this->db->update('detalle_pedido',$datos,array('iddp'=>$iddp))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_row_id($iddp,$row,$value){
		$datos=array(
			$row => $value,
		);
		if($this->db->update('detalle_pedido',$datos,array('iddp'=>$iddp))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($iddp){
		if($this->db->delete('detalle_pedido',['iddp' => $iddp])){
			return true;
		}else{
			return false;
		}
	}
	function eliminar_row($col,$val){
		if($this->db->delete('detalle_pedido',[$col.'' => $val])){
			return true;
		}else{
			return false;
		}
	}
	function max_id_2n($col,$atrib,$val){
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM detalle_pedido WHERE $atrib = '$val'");
		return $query->result();
	}	
}

/* End of file m_detalle_pedido.php */
/* Location: ./application/models/m_detalle_pedido.php*/