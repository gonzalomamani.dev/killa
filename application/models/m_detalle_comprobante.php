<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_detalle_comprobante extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('detalle_comprobante');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('detalle_comprobante',['iddcm' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM detalle_comprobante WHERE iddcm='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('detalle_comprobante',[$col => $val]);
		return $query->result();
	}
	function get_row_complet($col,$val){
		$cols="dp.iddcm, dp.idcm, dp.referencia, dp.debe, dp.haber,
				p.idpl, p.grupo, p.sub_grupo, p.rubro, p.cuenta, p.cuenta_text, p.tipo, p.gasto, p.venta";
    	$this->db->select($cols);
    	$this->db->from("detalle_comprobante dp");
    	$this->db->where("$col = '$val'");
    	$this->db->join("plan_cuenta p","p.idpl = dp.idpl","inner");
    	$query=$this->db->get();
    	return $query->result();
	}
	function get_group_cuentas($fecha1,$fecha2,$group,$idpl){//$group=>agrupar o no po cuenta //uso en libro mayor,sumas y saldos
		$cols="c.fecha,
				dc.iddcm,dc.idcm,dc.idpl,dc.referencia,
				p.grupo,p.sub_grupo,p.rubro,p.cuenta,p.cuenta_text, CONCAT((IFNULL((p.grupo),(''))),(IFNULL((p.sub_grupo),(''))),(IFNULL((p.rubro),(''))),(IFNULL((p.cuenta),('')))) AS codigo,";
		if($group){ $cols.="sum(dc.debe) as debe, sum(dc.haber)as haber";}else{ $cols.="dc.debe,dc.haber";}
		$this->db->select($cols);
		$this->db->from("comprobante c");
		$this->db->where("c.fecha >= '$fecha1'");
		$this->db->where("c.fecha <= '$fecha2'");
		if($idpl!=NULL){ $this->db->where("dc.idpl = '$idpl'"); }
		$this->db->join("detalle_comprobante dc","dc.idcm = c.idcm","inner");
		$this->db->join("plan_cuenta p","dc.idpl = p.idpl","inner");
		if($group){$this->db->group_by("dc.idpl");}
		$this->db->order_by("p.cuenta_text, c.fecha","asc");
		$query=$this->db->get();
		return $query->result();
	}

	function insertar($idcm,$idpl,$referencia,$debe,$haber){
		$datos=array(
			'idcm' => $idcm,
			'idpl' => $idpl,
			'referencia' => $referencia,
			'debe' => $debe,
			'haber' => $haber
		);
		if($this->db->insert('detalle_comprobante',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idcm,$idpl,$referencia,$debe,$haber){
		$datos=array(
			'idcm' => $idcm,
			'idpl' => $idpl,
			'referencia' => $referencia,
			'debe' => $debe,
			'haber' => $haber
		);
		if($this->db->update('detalle_comprobante',$datos,array('iddcm'=>$id))){
			return true;
		}else{
			return false;
		}
	}

	function eliminar($id){
		if($this->db->delete('detalle_comprobante',['iddcm' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function eliminar_col($col,$val){
		if($this->db->delete('detalle_comprobante',[$col.'' => $val])){
			return true;
		}else{
			return false;
		}
	}		
}

/* End of file m_detalle_comprobante.php */
/* Location: ./application/models/m_detalle_comprobante.php*/