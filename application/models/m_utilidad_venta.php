<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_utilidad_venta extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('utilidad_venta');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('utilidad_venta',['iduv' => $id]);
		return $query->result();
	}

	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM utilidad_venta WHERE iduv='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('utilidad_venta',array($col => $val));
		return $query->result();
	}
	function insertar($nombre, $porcentaje){
		$datos=array(
			'nombre' => $nombre,
			'porcentaje' => $porcentaje 
		);
		if($this->db->insert('utilidad_venta',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id, $nombre, $porcentaje){
		$datos=array(
			'nombre' => $nombre,
			'porcentaje' => $porcentaje 
		);
		if($this->db->update('utilidad_venta',$datos,array('iduv'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('utilidad_venta',['iduv' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}

/* End of file m_utilidad_venta.php */
/* Location: ./application/models/m_utilidad_venta.php*/