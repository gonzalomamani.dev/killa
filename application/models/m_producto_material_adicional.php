<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto_material_adicional extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('producto_material_adicional');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('producto_material_adicional',['idpma' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM producto_material_adicional WHERE idpma='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('producto_material_adicional',[$col => $val]);
		return $query->result();
	}
	function get_row_2n($col,$val,$col2,$val2){
		$query=$this->db->get_where('producto_material_adicional',[$col => $val,$col2 => $val2]);// en uso:PRODUCTO
		return $query->result();
	}
	function get_material_idp($idp){// En uso: almacen de productos,PRODUCCION
		$col="pma.idpma,pma.idp,pma.idma,pma.costo,
				ma.idma,
				mi.idmi,mi.codigo,mi.nombre,mi.fotografia,
				u.idu,u.nombre as nombre_unidad,u.abreviatura";
		$this->db->select($col);
		$this->db->from("producto_material_adicional pma");
		$this->db->where("pma.idp = '$idp'");
		$this->db->join("material_adicional ma","pma.idma = ma.idma","inner");
		$this->db->join("material_item mi","ma.idmi = mi.idmi","inner");
		$this->db->join('unidad u','u.idu = mi.idu','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idp,$idma,$costo){
		$datos=array(
			'idp' => $idp,
			'idma' => $idma,
			'costo' => $costo
		);
		if($this->db->insert('producto_material_adicional',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idp,$idma,$costo){
		$datos=array(
			'idp' => $idp,
			'idma' => $idma,
			'costo' => $costo
		);
		if($this->db->update('producto_material_adicional',$datos,array('idpma'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_col($id,$col,$val){
		$datos=array(
			$col => $val
		);
		if($this->db->update('producto_material_adicional',$datos,array('idpma'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('producto_material_adicional',['idpma' => $id])){
			return true;
		}else{
			return false;
		}
	}
}

/* End of file m_producto_material_adicional.php */
/* Location: ./application/models/m_producto_material_adicional.php*/