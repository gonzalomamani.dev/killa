<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_cliente extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$cols="p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
				pa.idpa,pa.nombre as pais,
				ci.idci,ci.nombre as ciudad, 
				c.idcl,c.nit,c.encargado,c.url";
		$this->db->select($cols);
		$this->db->from("cliente c");
		$this->db->join('persona p','c.nit = p.ci','inner');
		$this->db->join('ciudad ci','ci.idci = p.idci','inner');
		$this->db->join('pais pa','pa.idpa = ci.idpa','inner');
		$this->db->order_by("p.nombre", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get($id){
		$col="p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas, 
				pa.idpa,pa.nombre as pais,
				ci.idci,ci.nombre as ciudad, 
				c.idcl,c.nit,c.encargado,c.url";
		$this->db->select($col);
		$this->db->from("cliente c");
		$this->db->join('persona p','c.nit = p.ci','inner');
		$this->db->join('ciudad ci','ci.idci = p.idci','inner');
		$this->db->join('pais pa','pa.idpa = ci.idpa','inner');
		$this->db->where("c.idcl = '$id'");
		$this->db->order_by("p.nombre", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM cliente WHERE idcl='$id'");
		return $query->result();
	}
	function get_row_2($col,$val){//en uso, Clientes, 
      $query=$this->db->get_where('cliente',[$col => $val]);
      return $query->result();
    }
	function get_row($atrib,$val){
		$query=$this->db->get_where('cliente',[$atrib => $val]);
		return $query->result();
	}

	function get_cliente_row($col,$val){// en uso: Proveedor
		$query=$this->db->get_where('cliente',[$col => $val]);
		return $query->result();
	}
	function get_search($col,$val){// en uso:  cliente
		$cols="p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
				pa.idpa,pa.nombre as pais,
				ci.idci,ci.nombre as ciudad, 
				c.idcl,c.nit,c.encargado,c.url";
		$this->db->select($cols);
		$this->db->from("cliente c");
		$this->db->join('persona p','c.nit = p.ci','inner');
		$this->db->join('ciudad ci','ci.idci = p.idci','inner');
		$this->db->join('pais pa','pa.idpa = ci.idpa','inner');
		$this->db->order_by("p.nombre", "asc");
		if($col=="c.nit" || $col=="p.telefono"){ $this->db->where("$col like '$val%'");}
		if($col=="c.encargado" || $col=="p.direccion" || $col=="p.nombre"){ $this->db->where("$col like '%$val%'");}
		$this->db->order_by("p.nombre", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($nit,$encargado,$url){
		$datos=array(
			'nit' => $nit,
			'encargado' => $encargado,
			'url' => $url
		);
		if($this->db->insert('cliente',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$nit,$encargado,$url){
		$datos=array(
			'nit' => $nit,
			'encargado' => $encargado,
			'url' => $url
		);
		if($this->db->update('cliente',$datos,array('idcl'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('cliente',['idcl' => $id])){
			return true;
		}else{
			return false;
		}
	}	
}

/* End of file m_cliente.php */
/* Location: ./application/models/m_cliente.php*/