<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_estado_estructura extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){// en uso: capital humano,
		$query=$this->db->get('estado_estructura');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('estado_estructura',['idee' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM estado_estructura WHERE idee='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$cols="*";
		$this->db->select($cols);
		$this->db->from("estado_estructura");
		$this->db->where("$col = '$val'");
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idee,$posicion,$titulo,$t_visible,$signo,$s_visible,$tipo_estado,$columna,$ecpv){
		$datos=array(
			'idee' => $idee,
			'posicion' => $posicion,
			'titulo' => $titulo,
			't_visible' => $t_visible,
			'signo' => $signo,
			's_visible' => $s_visible,
			'tipo_estado' => $tipo_estado,
			'columna' => $columna,
			'ecpv' => $ecpv
		);
		if($this->db->insert('estado_estructura',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$titulo,$t_visible,$signo,$s_visible,$tipo_estado,$columna,$ecpv){
		$datos=array(
			'titulo' => $titulo,
			't_visible' => $t_visible,
			'signo' => $signo,
			's_visible' => $s_visible,
			'tipo_estado' => $tipo_estado,
			'columna' => $columna,
			'ecpv' => $ecpv
		);
		if($this->db->update('estado_estructura',$datos,array('idee'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('estado_estructura',['idee' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function eliminar_row($col,$val){
		if($this->db->delete('estado_estructura',[$col => $val])){
			return true;
		}else{
			return false;
		}
	}
	function max($col){
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM estado_estructura");
		return $query->result();
	}
	function max_where($col_max,$col,$val){
		$query=$this->db->query("SELECT IFNULL(max($col_max),0) as max FROM estado_estructura WHERE $col='$val'");
		return $query->result();
	}	
}

/* End of file m_estado_estructura.php */
/* Location: ./application/models/m_estado_estructura.php*/