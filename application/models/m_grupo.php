<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_grupo extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("nombre", "asc");
		$query=$this->db->get('grupo');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('grupo',['idg' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM grupo WHERE idg='$id'");
		return $query->result();
	}

	function get_all_use_insumo(){
		$query=$this->db->query("SELECT idg, nombre, descripcion, (SELECT count(*) FROM material WHERE material.idg=grupo.idg)as nro FROM grupo");
		return $query->result();
	}
	function insertar($nombre, $descripcion){
		$datos=array(
			'nombre' => $nombre,
			'descripcion' => $descripcion 
		);
		if($this->db->insert('grupo',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$nombre,$descripcion){
		$datos=array(
			'nombre' => $nombre,
			'descripcion' => $descripcion
		);
		if($this->db->update('grupo',$datos,array('idg' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('grupo',['idg' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}

/* End of file m_grupo.php */
/* Location: ./application/models/m_grupo.php*/