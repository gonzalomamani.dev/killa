<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_color extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("nombre", "asc");
		$query=$this->db->get('color');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('color',['idco' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM color WHERE idco='$id'");
		return $query->result();
	}
	function get_all_use_insumo(){
		$query=$this->db->query("SELECT idco, nombre, codigo, (SELECT count(*) FROM material WHERE material.idco=color.idco)as nro FROM color");
		return $query->result();
	}
	function insertar($nombre,$codigo){
		$datos=array(
			'nombre' => $nombre,
			'codigo' => $codigo 
		);
		if($this->db->insert('color',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$nombre,$codigo){
		$datos=array(
			'nombre' => $nombre,
			'codigo' => $codigo
		);
		if($this->db->update('color',$datos,array('idco' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('color',['idco' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}

/* End of file m_color.php */
/* Location: ./application/models/m_color.php*/