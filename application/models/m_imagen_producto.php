<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_imagen_producto extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('imagen_producto');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('imagen_producto',['idim' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM imagen_producto WHERE idim='$id'");
		return $query->result();
	}
	function get_row($row,$val){
		$query=$this->db->get_where('imagen_producto',[$row => $val]);
		return $query->result();
	}
	function get_col_prod($id,$col){//EN USO DETALLE DE PEDIDO
		$query=$this->db->query("SELECT $col FROM imagen_producto WHERE idpim='$id'");
		return $query->result();
	}
	function get_pieza_material($idpim){// en uso: Producto,
		$query=$this->db->get_where('imagen_producto',['idpim' => $idpim]);
		return $query->result();
	}
	function get_producto($idpim){//en uso: produccion,ANALIZAR
		$query=$this->db->get_where('imagen_producto',['idpim' => $idpim]);
		return $query->result();
	}
	
	function insertar($idpim,$nombre){
		$datos=array(
			'idpim' => $idpim,
			'nombre' => $nombre
		);
		if($this->db->insert('imagen_producto',$datos)){
			return true;
       }else{
          return false;
       }
	}
	function modificar($idpim,$nombre){
		$datos=array(
			'idpim' => $idpim,
			'nombre' => $nombre
		);
		if($this->db->update('imagen_producto',$datos,array('idim'=>$idim))){
			return true;
       }else{
          return false;
       }
	}
	function modificar_columna($idim,$columna,$valor){
		$datos=array(
			$columna => $valor
		);
		if($this->db->update('imagen_producto',$datos,array('idim'=>$idim))){
			return true;
       }else{
          return false;
       }
	}
	function eliminar($id){
		if($this->db->delete('imagen_producto',['idim' => $id])){
			return true;
       }else{
          return false;
       }
	}
	function eliminar_row($row,$val){
		if($this->db->delete('imagen_producto',[$row => $val])){
			return true;
       }else{
          return false;
       }
	}	
}

/* End of file m_imagen_producto.php */
/* Location: ./application/models/m_imagen_producto.php*/