<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_pago extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("idpa", "asc");
		$query=$this->db->get('pago');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('pago',['idpa' => $id]);
		return $query->result();
	}

	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM pago WHERE idpa='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$cols="*";
		$this->db->select($cols);
		$this->db->from("pago");
		$this->db->order_by("fecha", "asc");
		$this->db->where("$col = '$val'");
		$query=$this->db->get();
		return $query->result();
		return $query->result();
	}
	function insertar($idpe,$fecha,$monto,$observaciones){
		$datos=array(
			'idpe' => $idpe,
			'fecha' => $fecha,
			'monto' => $monto,
			'observaciones' => $observaciones
		);
		if($this->db->insert('pago',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$fecha,$monto,$observaciones){
		$datos=array(
			'fecha' => $fecha,
			'monto' => $monto,
			'observaciones' => $observaciones
		);
		if($this->db->update('pago',$datos,array('idpa' => $id))){
			return true;
		}else{
			return false;
		}
		
	}
	function eliminar($id){
		if($this->db->delete('pago',['idpa' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}

/* End of file m_pago.php */
/* Location: ./application/models/m_pago.php*/