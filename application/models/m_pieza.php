<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_pieza extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function get_all(){
		$query=$this->db->get('pieza');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('pieza',array('idpi' => $id));
		return $query->result();
	}
	function get_col($id, $col){
		$query=$this->db->query("SELECT $col FROM pieza WHERE idpi='$id'");
		return $query->result();
	}
	function get_row($col, $val){// en uso: PRODUCTO,
		$query=$this->db->get_where('pieza',[$col => $val]);
		return $query->result();
	}
	function get_cols($col,$val){
		$cols="*";
		$this->db->select($cols);
		$this->db->from("pieza");
		$this->db->where("$col = '$val'");
		$query=$this->db->get();
		return $query->result();
	}
	function get_row_2n($row1, $val1, $row2, $val2){
		$query=$this->db->query("SELECT * FROM pieza WHERE $row1='$val1' and $row2='$val2'");
		return $query->result();
	}
	function get_grupo($idpi){
		$col="p.idpi,p.idcp,p.alto,p.ancho,p.imagen,p.descripcion,p.fecha,p.unidades,
				pg.idpig,pg.nombre as nombre_grupo,pg.descripcion as descripcion_grupo,";
		$this->db->select($col);
		$this->db->from("pieza p");
		$this->db->where("p.idpi = '$idpi'");
		$this->db->join('pieza_grupo pg','pg.idpig=p.idpig','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_pieza_categoria($idcp){
		$col="p.idpi,p.idcp,p.alto,p.ancho,p.imagen,p.descripcion,p.fecha,p.unidades,
				pg.idpig,pg.nombre as nombre_grupo,pg.descripcion as descripcion_grupo,";
		$this->db->select($col);
		$this->db->from("pieza p");
		$this->db->where("p.idcp = '$idcp'");
		$this->db->join('pieza_grupo pg','pg.idpig=p.idpig','inner');
		$query=$this->db->get();
		return $query->result();
	}

	function get_pieza_grupo_all(){
		$col="p.idpi,p.idcp,p.alto,p.ancho,p.imagen,p.nombre,p.codigo,p.descripcion,p.fecha,
				pg.idpig,pg.nombre as nombre_grupo,pg.descripcion as descripcion_grupo,";
		$this->db->select($col);
		$this->db->from("pieza p");
		$this->db->join('pieza_grupo pg','pg.idpig=p.idpig','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_material($idpi){
		$col="m.idm,m.idg,m.costo_unitario,
			mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
			c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($col);
		$this->db->from("pieza pi");
		$this->db->where("pi.idpi = '$idpi'");
		$this->db->join("categoria_pieza cp","cp.idcp=pi.idcp","inner");
		$this->db->join("categoria_producto cpr","cpr.idcp=cp.idcp","inner");
		$this->db->join('material m','cpr.idm=m.idm','inner');
		$this->db->join('material_item mi','m.idmi=mi.idmi','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_pieza_producto($idp){
		$col="p.idpi,p.idping,p.idcp,p.alto,p.ancho,p.imagen,p.nombre,p.codigo,p.descripcion,p.fecha";
		$this->db->select($col);
		$this->db->from("pieza p");
		$query=$this->db->get();
		return $query->result();
	}

	function insertar($idpig,$idcp,$alto,$ancho,$imagen,$descripcion,$unidades){
		$datos=array(
			'idpig' => $idpig,
			'idcp' => $idcp,
			'alto' => $alto,
			'ancho' => $ancho,
			'imagen' => $imagen,
			'descripcion' => $descripcion,
			'unidades' => $unidades
		);
		if($this->db->insert('pieza',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idpig,$idcp,$alto,$ancho,$imagen,$descripcion,$unidades){
		$datos=array(
			'idpig' => $idpig,
			'idcp' => $idcp,
			'alto' => $alto,
			'ancho' => $ancho,
			'imagen' => $imagen,
			'descripcion' => $descripcion,
			'unidades' => $unidades
		);
		if($this->db->update('pieza',$datos,array('idpi' => $id))){
			return true;
		}else{
			return false;
		}
	}

	function eliminar($id){
		if($this->db->query("DELETE FROM pieza WHERE idpi='$id'")){
			return true;
		}else{
			return false;
		}
	}

	
	
}

/* End of file M_pieza.php */
/* Location ./application/models/M_pieza.php*/