<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_privilegio extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('privilegio');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('privilegio',['idpri' => $id]);
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->query("SELECT * FROM privilegio WHERE $col = '$val'");
		return $query->result();
	}
	function get_row_2n($col,$val,$col2,$val2){
		$query=$this->db->get_where('privilegio',[$col => $val,$col2 => $val2]);
		return $query->result();
	}
	function insertar($ide){
		$datos=array(
			'ide' => $ide
		);
		if($this->db->insert('privilegio',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$col,$val){
		$datos=array(
			$col => $val
		);
		if($this->db->update('privilegio',$datos,array('idpri' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('privilegio',['idpri' => $id])){
			return true;
		}else{
			return false;
		}
	}
}

/* End of file m_privilegio.php */
/* Location: ./application/models/m_privilegio.php*/