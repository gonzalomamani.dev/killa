<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_depreciacion extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('depreciacion');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('depreciacion',['iddpre' => $id]);
		return $query->result();
	}

	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM depreciacion WHERE iddpre='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('depreciacion',array($col => $val));
		return $query->result();
	}
	function insertar($nombre,$anio,$porcentaje,$observaciones){
		$datos=array(
			'nombre' => $nombre,
			'anio' => $anio,
			'porcentaje' => $porcentaje,
			'observaciones' => $observaciones
		);
		if($this->db->insert('depreciacion',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$nombre,$anio,$porcentaje,$observaciones){
		$datos=array(
			'nombre' => $nombre,
			'anio' => $anio,
			'porcentaje' => $porcentaje,
			'observaciones' => $observaciones
		);
		if($this->db->update('depreciacion',$datos,array('iddpre'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('depreciacion',['iddpre' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}

/* End of file m_depreciacion.php */
/* Location: ./application/models/m_depreciacion.php*/