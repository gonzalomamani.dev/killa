<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_empleado extends CI_Model{
	function __construct(){
        parent::__construct();
    }
    function get_all(){
        $query=$this->db->get('empleado');
        return $query->result();
    }
    function get($ide){
        $query=$this->db->get_where('empleado',array('ide' => $ide));
        return $query->result();
    }
    function get_empleado($col,$val){//EN USO: CAPITAL HUMANO
        $cols="e.ide,e.ci,e.idtc,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.c_feriado,e.tipo,e.c_descuento,e.estado,
            pa.idpa,pa.nombre as pais,
            ci.idci,ci.nombre as ciudad, ci.abreviatura,
            p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
            tc.idtc,tc.horas,tc.tipo as tipo_contrato,tc.principal,tc.descripcion";
        $this->db->select($cols);
        $this->db->from("empleado e");
        $this->db->join("persona p","p.ci = e.ci","inner");
        $this->db->join('ciudad ci','ci.idci = p.idci','inner');
        $this->db->join('pais pa','pa.idpa = ci.idpa','inner');
        $this->db->join('tipo_contrato tc','tc.idtc = e.idtc','inner');
        if($col!="" && $val!=""){
            if($col=="e.ide" || $col=='e.tipo' || $col=='e.idtc'){ $this->db->where("$col = '$val'");}
            if($col=='e.codigo' || $col=='p.ci' || $col=='p.telefono' || $col=='e.salario'){ $this->db->where("$col like '$val%'");}
            if($col=='nombre'){  $this->db->where("(p.nombre like '$val%' || e.nombre2 like '$val%' || e.paterno like '$val%' || e.materno like '$val%')");}
        }
        $this->db->where("e.tipo <= 1");
        $this->db->where("e.tipo >= 0");
        $this->db->order_by("e.fecha_ingreso","asc");
        $this->db->order_by("p.nombre");
        $query=$this->db->get();
        return $query->result();
    }    
    function get_empleado_activo($col,$val){//EN USO: PLANILLA
        $cols="e.ide,e.ci,e.idtc,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.c_feriado,e.tipo,e.c_descuento,e.estado,
            pa.idpa,pa.nombre as pais,
            ci.idci,ci.nombre as ciudad, ci.abreviatura,
            p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
            tc.idtc,tc.horas,tc.tipo as tipo_contrato,tc.principal,tc.descripcion";
        $this->db->select($cols);
        $this->db->from("empleado e");
        $this->db->join("persona p","p.ci = e.ci","inner");
        $this->db->join('ciudad ci','ci.idci = p.idci','inner');
        $this->db->join('pais pa','pa.idpa = ci.idpa','inner');
        $this->db->join('tipo_contrato tc','tc.idtc = e.idtc','inner');
        if($col!="" && $val!=""){
            if($col=='e.tipo' || $col=='e.idtc'){ $this->db->where("$col = '$val'");}
            if($col=='e.codigo' || $col=='p.ci' || $col=='p.telefono' || $col=='e.salario'){ $this->db->where("$col like '$val%'");}
            if($col=='nombre'){  $this->db->where("(p.nombre like '$val%' || e.nombre2 like '$val%' || e.paterno like '$val%' || e.materno like '$val%')");}
        }
        $this->db->where("e.tipo <= 1");
        $this->db->where("e.tipo >= 0");
        $this->db->where("e.estado = '1'");
        $this->db->order_by("e.fecha_ingreso","asc");
        $this->db->order_by("p.nombre");
        $query=$this->db->get();
        return $query->result();
    }
    function get_all_order($col){
       $cols="e.ide,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.c_feriado,c_descuento,e.tipo,e.admin_sistema,e.usuario,e.c_descuento,e.estado,
            pa.idpa,pa.nombre as pais,
            ci.idci,ci.nombre as ciudad, ci.abreviatura,
            p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
            tc.idtc,tc.horas,tc.tipo as tipo_contrato";
      $this->db->select($cols);
      $this->db->from("empleado e");
      $this->db->join("persona p","e.ci=p.ci","inner");
      $this->db->join('ciudad ci','ci.idci = p.idci','inner');
      $this->db->join('pais pa','pa.idpa = ci.idpa','inner');
      $this->db->join("tipo_contrato tc ","e.idtc=tc.idtc","inner");
      $this->db->where("e.tipo <= 1");
      $this->db->where("e.estado = '1'");
      $this->db->order_by($col);
      $query=$this->db->get();
      return $query->result();     
    }    
    function get_row($col,$val){//en uso, Clientes, Proveedor,EMPLEADOS
      $query=$this->db->get_where('empleado',[$col => $val]);
      return $query->result();
    }
    function get_row_2n($col,$val,$col2,$val2){//en uso, Clientes, Proveedor,EMPLEADOS
      $query=$this->db->get_where('empleado',[$col => $val,$col2 => $val2]);
      return $query->result();
    }
    function get_proceso($ide){// en uso FICHA DE CONTROL
      $col="e.ide,e.ci,e.idtc,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.c_feriado,e.tipo,e.c_descuento,e.estado,
        p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
        ep.idep,ep.ide,ep.tipo,ep.fecha,
        pr.idpr,pr.nombre as nombre_proceso,pr.detalle";
      $this->db->select($col);
      $this->db->from("empleado e");
      $this->db->order_by("pr.nombre", "asc");
      $this->db->where("ep.ide = '$ide'");
      $this->db->join("persona p","p.ci = e.ci","inner");
      $this->db->join("empleado_proceso ep","ep.ide = e.ide","inner");
      $this->db->join('proceso pr','pr.idpr = ep.idpr','inner');
      $query=$this->db->get();
      return $query->result();
    }
    function get_maestro_ayudantes($ide,$col,$val){//en uso capital humano,
        $cols="e.ide,e.idtc,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.tipo,e.c_descuento,e.estado,
            p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
            pa.idpa,pa.nombre as pais,
            ci.idci,ci.nombre as ciudad";
        $this->db->select($cols);
        $this->db->from("empleado e");
        $this->db->where("e.ide != $ide");
    if($col!="" && $val!=""){
        if($col=="p.ci"){ $this->db->where("$col like '$val%'");}
        if($col=="nombre"){ $this->db->where("(p.nombre like '$val%' || e.nombre2 like '$val%' || e.paterno like '$val%' || e.materno like '$val%')");}
    }
        $this->db->order_by("p.nombre", "asc");
        $this->db->join('persona p','e.ci = p.ci','inner');
        $this->db->join('ciudad ci','ci.idci = p.idci','inner');
        $this->db->join('pais pa','pa.idpa = ci.idpa','inner');
        $query=$this->db->get();
        return $query->result();
    }
    function insertar($idtc,$ci,$codigo,$nombre2,$paterno,$materno,$cargo,$formacion,$salario,$fecha_ingreso,$fecha_nacimiento,$tipo){//en uso:capital humano
        $salt="$2a$10$".sha1($ci)."$";
        $password=crypt($ci,$salt);
        $data = array(
          'idtc' => $idtc,
          'ci' => $ci,
          'codigo' => $codigo,
          'nombre2' => $nombre2,
          'paterno' => $paterno,
          'materno' => $materno,
          'cargo' => $cargo,
          'formacion' => $formacion,
          'salario' => $salario,
          'fecha_ingreso' => $fecha_ingreso,
          'fecha_nacimiento' => $fecha_nacimiento,
          'tipo' => $tipo,
          'password' => $password
        );
       if ($this->db->insert('empleado', $data)){
          return true;
       }else{
          return false;
       }
    }
    function modificar_row($id,$atrib,$val){//en uso:capital humano,  
        $data = array( $atrib => $val );
        if ($this->db->update('empleado', $data, array('ide' => $id))){
          return true;
        }else{
          return false;
        }
    }
    function modificar($id,$idtc,$ci,$codigo,$nombre2,$paterno,$materno,$cargo,$formacion,$salario,$fecha_ingreso,$fecha_nacimiento,$tipo){//en uso:capital humano,  
        $data = array(
          'idtc' => $idtc,
          'ci' => $ci,
          'codigo' => $codigo,
          'nombre2' => $nombre2,
          'paterno' => $paterno,
          'materno' => $materno,
          'cargo' => $cargo,
          'formacion' => $formacion,
          'salario' => $salario,
          'fecha_ingreso' => $fecha_ingreso,
          'fecha_nacimiento' => $fecha_nacimiento,
          'tipo' => $tipo
        );
        if ($this->db->update('empleado', $data, array('ide' => $id))){
          return true;
        }else{
          return false;
        }
    }
    public function validate($login,$pass){
      $salt="$2a$10$".sha1($pass)."$";
      $password=crypt($pass,$salt);
      $this->db->select("*");
      $this->db->from("empleado");
      $this->db->where("usuario collate utf8_bin ='$login' and password='$password' and usuario_sistema='1'");
      $query=$this->db->get();
      return $query->result();
    }
    /*---- Manejo de usuarios ----*/
    function search_usuarios($col,$val,$session){// en us USUARIOS
      $cols="e.ide,e.idtc,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.tipo,e.usuario_sistema,e.admin_sistema,e.usuario,e.c_descuento,e.estado,
            p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
            pa.idpa,pa.nombre as pais,
            ci.idci,ci.nombre as ciudad,ci.abreviatura,";
      $this->db->select($cols);
      $this->db->from("empleado e");
      $this->db->join("persona p","e.ci=p.ci","inner");
      $this->db->join('ciudad ci','ci.idci = p.idci','inner');
      $this->db->join('pais pa','pa.idpa = ci.idpa','inner');
    if($col!="" && $val!=""){
      if($col=="e.admin_sistema" || $col=="e.ide"){
          $this->db->where("$col = '$val'");        
      }
      if($col=="p.ci" || $col=="e.cargo" || $col=="e.usuario"){
        $this->db->where("$col like '$val%'");
      }
      if($col=="nombre"){
        $this->db->where("(p.nombre like '$val%' || e.nombre2 like '$val%' || e.paterno like '$val%' || e.materno like '$val%')");
      }
    }
      $this->db->where("e.usuario_sistema = 1");
      if(!$session){ $this->db->where("e.ide != ".$this->session->userdata('id')); }
      $this->db->order_by("p.nombre");
      $query=$this->db->get();
      return $query->result();
    }
    function insertar_usuario($ci,$nombre2,$paterno,$materno,$cargo,$admin_sistema,$usuario){//en uso:capital humano
        $salt="$2a$10$".sha1($ci)."$";
        $password=crypt($ci,$salt);
        $data = array(
          'ci' => $ci,
          'nombre2' => $nombre2,
          'paterno' => $paterno,
          'materno' => $materno,
          'cargo' => $cargo,
          'usuario_sistema' => '1',
          'admin_sistema' => $admin_sistema,
          'usuario' => $usuario,
          'password' => $password
        );
       if ($this->db->insert('empleado', $data)){
          return true;
       }else{
          return false;
       }
    }
    function modificar_usuario($id,$ci,$nombre2,$paterno,$materno,$cargo,$admin_sistema,$usuario,$password){//en uso:capital humano, 
      if($password==""){// para el caso que guradmos nuevos usuarioi y ya sta registrado en empleado
        $salt="$2a$10$".sha1($ci)."$";
        $password=crypt($ci,$salt);
        $data = array(
          'usuario_sistema' => '1',
          'admin_sistema' => $admin_sistema,
          'usuario' => $usuario,
          'password' => $password
        );
      }else{// para el caso que modificamos un usuario
        $data = array(
          'usuario_sistema' => '1',
          'nombre2' => $nombre2,
          'paterno' => $paterno,
          'materno' => $materno,
          'cargo' => $cargo,
          'admin_sistema' => $admin_sistema,
          'usuario' => $usuario
        );
      }
        if ($this->db->update('empleado', $data, array('ide' => $id))){
          return true;
        }else{
          return false;
        }
    }
    function restat_usuario($id){
      $data = array(
          'usuario_sistema' => '0',
          'admin_sistema' => '0',
          'usuario' => NULL
        );
        if ($this->db->update('empleado', $data, array('ide' => $id))){
          return true;
        }else{
          return false;
        }
    }
    /*---- End manejo de usuarios ----*/
    public function eliminar($id){
        if($this->db->delete('empleado',['ide'=>$id])){
            return true;
        }else{
            return false;
        }
    }













  /*  function get($ide){// en uso: cambiar contraseña
      $cols="e.ide,e.idtc,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.c_feriado,e.tipo,e.c_descuento,e.estado,
            pa.idpa,pa.nombre as pais,
            ci.idci,ci.nombre as ciudad,
            p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas";
      $this->db->select($cols);
      $this->db->from("empleado e");
      $this->db->where("e.ide = '$ide'");
      $this->db->join("persona p","e.ci=p.ci","inner");
      $this->db->join('ciudad ci','ci.idci = p.idci','inner');
      $this->db->join('pais pa','pa.idpa = ci.idpa','inner');
      $query=$this->db->get();
      return $query->result();
    }
    function get_all(){// en uso: capital humano,
      $cols="e.ide,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.c_feriado,c_descuento,e.tipo,e.admin_sistema,e.usuario,e.c_descuento,e.estado,
            pa.idpa,pa.nombre as pais,
            ci.idci,ci.nombre as ciudad, ci.abreviatura,
            p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
            tc.idtc,tc.horas,tc.tipo as tipo_contrato";
      $this->db->select($cols);
      $this->db->from("empleado e");
      $this->db->join("persona p","e.ci=p.ci","inner");
      $this->db->join('ciudad ci','ci.idci = p.idci','inner');
      $this->db->join('pais pa','pa.idpa = ci.idpa','inner');
      $this->db->join("tipo_contrato tc ","e.idtc=tc.idtc","inner");
      $this->db->where("e.tipo <= 1");
      $this->db->order_by("e.fecha_ingreso","asc");
      $this->db->order_by("p.nombre");
      $query=$this->db->get();
      return $query->result();
    }

    function get_complet($ide){// en uso: VENTAS->REPORTES,PLANILLA
      $cols="e.ide,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.c_feriado,c_descuento,e.tipo,e.admin_sistema,e.usuario,e.c_descuento,e.estado,
            pa.idpa,pa.nombre as pais,
            ci.idci,ci.nombre as ciudad, ci.abreviatura,
            p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
            tc.idtc,tc.horas,tc.tipo as tipo_contrato";
      $this->db->select($cols);
      $this->db->from("empleado e");
      $this->db->where("e.ide = '$ide'");
      $this->db->join("persona p","e.ci=p.ci","inner");
      $this->db->join('ciudad ci','ci.idci = p.idci','inner');
      $this->db->join('pais pa','pa.idpa = ci.idpa','inner');
      $this->db->join("tipo_contrato tc ","e.idtc = tc.idtc","inner");
      $query=$this->db->get();
      return $query->result();
    }




    function get_ci($ci){
      $query=$this->db->get_where('empleado',['ci' => $ci]);
       return $query->result();
    }
    function get_tc(){//en uso CAPITAL HUMANO,PRODUCCION,TALLER
      $col="e.ide,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.tipo,e.c_feriado,e.c_descuento,e.estado,
          p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
          pa.idpa,pa.nombre as pais,
          ci.idci,ci.nombre as ciudad,
          tc.idtc,tc.horas,tc.tipo as tipo_hora,tc.principal,tc.descripcion";
    	$this->db->select($col);
    	$this->db->from("empleado e");
    	$this->db->join("tipo_contrato tc ","e.idtc=tc.idtc","inner");
      $this->db->join("persona p","e.ci=p.ci","inner");
      $this->db->join('ciudad ci','ci.idci = p.idci','inner');
      $this->db->join('pais pa','pa.idpa = ci.idpa','inner');
      $this->db->where("e.tipo <= 1");
      $this->db->where("e.tipo >= 0");
      $this->db->order_by("p.nombre");
    	$query=$this->db->get();
    	return $query->result();
    }

    function get_tc_id($ide){//en uso: CAPITAL HUMANO,
       $col="e.ide,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.tipo,e.c_feriado,e.c_descuento,e.estado,
            p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
            pa.idpa,pa.nombre as pais,
            ci.idci,ci.nombre as ciudad,
            tc.idtc,tc.horas,tc.tipo as tipo_contrato";
      $this->db->select($col);
      $this->db->from("empleado e");
      $this->db->where("e.ide = '$ide'");
      $this->db->where("e.tipo <= 2");
      $this->db->join("tipo_contrato tc","e.idtc=tc.idtc","inner");
      $this->db->join("persona p","e.ci=p.ci","inner");
      $this->db->join('ciudad ci','ci.idci = p.idci','inner');
      $this->db->join('pais pa','pa.idpa = ci.idpa','inner');
      $query=$this->db->get();
      return $query->result();
    }
    function get_where($ide){//en uso: capital humano,
      $cols="e.ide,e.idtc,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.c_feriado,e.tipo,e.c_descuento,e.estado,
            p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
            pa.idpa,pa.nombre as pais,
            ci.idci,ci.nombre as ciudad";
      $this->db->select($cols);
      $this->db->from("empleado e");
      $this->db->order_by("p.nombre", "asc");
      $this->db->where("e.ide = '$ide'");
      $this->db->where("e.tipo <= 2");
      $this->db->join('persona p','e.ci = p.ci','inner');
    $this->db->join('ciudad ci','ci.idci = p.idci','inner');
      $this->db->join('pais pa','pa.idpa = ci.idpa','inner');
      $query=$this->db->get();
      return $query->result();
    }
    function get_search($col,$val){// EN USO TALLER
      $cols="e.ide,e.idtc,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.tipo,e.c_descuento,e.estado,
            p.ci,p.idci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas";
        $this->db->select($cols);
        $this->db->from("empleado e");
        $this->db->where("e.tipo <= 1");
        $this->db->where("e.tipo >= 0");
        if($col!="" && $val!=""){
          if($col=='nombre'){
            $this->db->where("(p.nombre like '$val%' || e.nombre2 like '$val%' || e.paterno like '$val%' || e.materno like '$val%')");
          }else{
            if($col=='e.tipo' || $col=='e.ide'){
              $this->db->where("$col = '$val'");
            }else{
              $this->db->where("$col  like '$val%'");
            }
          }
        }
        $this->db->order_by("p.nombre", "asc");
        $this->db->join('persona p','e.ci = p.ci','inner');
        $query=$this->db->get();
        return $query->result();
    }


    
    function get_persona_all(){//EN USO
      $col="p.ci,p.ciudad,p.pais,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
          e.ide,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.tipo,e.c_descuento,e.estado,
          tc.idtc,tc.nombre as nombre_tc ,tc.hras_por_dia,tc.cod as cod_tc";
        $this->db->select($col);
		    $this->db->from("empleado e");
	 	    $this->db->order_by("p.nombre", "asc");
        $this->db->where("e.tipo <= 2");
		    //$this->db->order_by("empleado.cod", "asc");
        $this->db->join("tipo_contrato tc","e.idtc=tc.idtc","inner");
		    $this->db->join('persona p','e.ci = p.ci','inner');
		    $query=$this->db->get();
		    return $query->result();
   	}

     /*function get_persona_empleado(){//EN USO TALLER
      $col="p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
            pa.idpa,pa.nombre as pais,
            ci.idci,ci.nombre as ciudad,
          e.ide,e.idtc,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.tipo";
        $this->db->select($col);
        $this->db->from("empleado e");
        $this->db->order_by("p.nombre", "asc");
        $this->db->where("e.tipo <= 2");
        $this->db->join('persona p','e.ci = p.ci','inner');
        $this->db->join('ciudad ci','ci.idci = p.idci','inner');
        $this->db->join('pais pa','pa.idpa = ci.idpa','inner');
        $query=$this->db->get();
        return $query->result();
    }
    function get_persona_empleado_id($ide){//EN USO TALLER
      $col="p.ci,p.ciudad,p.pais,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
            e.ide,e.idtc,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.tipo,e.c_descuento,e.estado,";
        $this->db->select($col);
        $this->db->from("empleado e");
        $this->db->where("e.ide = '$ide'");
        $this->db->where("e.tipo <= 2");
        $this->db->order_by("p.nombre", "asc");
        $this->db->join('persona p','e.ci = p.ci','inner');
        $query=$this->db->get();
        return $query->result();
    }
    function get_col($ide,$col){
      $query=$this->db->query("SELECT $col FROM empleado WHERE ide='$ide'");
      return $query->result();
    }









    public function validate_id($id,$login,$pass){
      $salt="$2a$10$".sha1($pass)."$";
      $password=crypt($pass,$salt);
      $query=$this->db->get_where('empleado',['idusu'=>$id,'usuario'=>$login,'password'=>$password]);
      return $query->result();
    }
    public function modificar_logeo($id,$usuario,$password){
      $data = array(
        'usuario' => $usuario,
        'password' => $password
      );
      $this->db->update('usuario',$data,array('ide' => $id));
    }
    public function modificar_password($id,$ci){
      $salt="$2a$10$".sha1($ci)."$";
      $password=crypt($ci,$salt);
      $data = array(
        'password' => $password
      );
      if($this->db->update('empleado',$data,array('ide' => $id))){
        return true;
      }else{
        return false;
      }
    }*/
}

/* End of file m_empleado.php */
/* Location: ./application/models/m_empleado.php */