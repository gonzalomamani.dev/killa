<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto_insumo extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("idpm", "desc");
		$query=$this->db->get('producto_insumo');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('producto_insumo',['idpm' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM producto_insumo WHERE idpm='$id'");
		return $query->result();
	}

	function get_producto_insumo_idp($idp){
		$col="pm.idpm, pm.idp, pm.costo,
				m.idma, m.nombre";
		$this->db->select($col);
		$this->db->from("producto_insumo pm");
		$this->db->where("pm.idp = '$idp'");
		$this->db->join('insumo m','pm.idma = m.idma','inner');
		$query=$this->db->get();
		return $query->result();
	}

	function insertar($idp,$idma,$costo){
		$datos=array(
			'idp' => $idp,
			'idma' => $idma,
			'costo' => $costo
		);
		if($this->db->insert('producto_insumo',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($idpm,$idp,$idma,$costo){
		$datos=array(
			'idp' => $idp,
			'idma' => $idma,
			'costo' => $costo
		);
		if($this->db->update('producto_insumo',$datos,array('idpm'=>$idpm))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($idpm){
		if($this->db->delete('producto_insumo',['idpm' => $idpm])){
			return true;
		}else{
			return false;
		}
	}	
}

/* End of file m_producto_insumo.php */
/* Location: ./application/models/m_producto_insumo.php*/