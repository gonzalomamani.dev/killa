<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_categoria_producto extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('categoria_producto');
		return $query->result();
	}
	function get($id){//en uso :detalle pedido producto,
		$query=$this->db->get_where('categoria_producto',array('idpim' => $id));
		return $query->result();
	}
	function get_col($id, $col){
		$query=$this->db->query("SELECT $col FROM categoria_producto WHERE idpim='$id'");
		return $query->result();
	}
	function get_row($row,$val){//en uso PRODUCTO,
		$query=$this->db->query("SELECT * FROM categoria_producto WHERE $row='$val'");
		return $query->result();
	}
	function get_row_2n($row,$val,$row2,$val2){ //en uso PRODUCTO,
		$query=$this->db->get_where('categoria_producto',array($row => $val,$row2 => $val2));
		return $query->result();
	}
	function get_producto($atrib,$val,$order,$group,$control_group){//en uso: alamcen de materiales,TALLER
		$cols="cp.idpim,cp.idcp,cp.idm,cp.portada,
				cpi.color_producto,
				p.idp, p.cod as codigo,p.nombre";
		$this->db->select($cols);
		$this->db->from("categoria_producto cp");
		$this->db->where("cpi.color_producto = '1'");
		if($atrib!="" && $val!=""){
			if($atrib=="cp.idpim" || $atrib=="cp.idp"){ $this->db->where("$atrib = '$val'");}
			if($atrib=="p.cod" || $atrib=="p.nombre"){ $this->db->where("$atrib like '%$val%'");}
		}
		$this->db->join('categoria_pieza cpi','cp.idcp=cpi.idcp','left');
		$this->db->join('producto p','cpi.idp=p.idp');
		if($group!="" && ($control_group=="asc" || $control_group=="desc")){ $this->group_by($group,$control_group);}
		if($order!=""){ $this->order_by($order);}
		$query=$this->db->get();
		return $query->result();
	}
	function get_colores_producto($idp){
		$col="cp.idpim,cp.idcp,cp.idm,cp.portada,cp.idco";
		$this->db->select($col);
		$this->db->from("categoria_producto cp");
		$this->db->where("p.idp = '$idp' and p.color_producto='1'");
		$this->db->join('categoria_pieza p','cp.idcp=p.idcp','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_colores_producto_cliente($idp,$idcl){
		$col="cp.idpim,cp.idcp,cp.idm,cp.portada,cp.idco,
			pc.idpc,pc.idcl,pc.iduv,pc.costo_unitario";
		$this->db->select($col);
		$this->db->from("categoria_producto cp");
		$this->db->where("p.idp = '$idp'");
		$this->db->where("p.color_producto = '1'");
		$this->db->where("pc.idcl = '$idcl'");
		$this->db->join('categoria_pieza p','cp.idcp=p.idcp','inner');
		$this->db->join('producto_cliente pc','pc.idpim=cp.idpim','inner');
		$query=$this->db->get();
		return $query->result();
	}







	function get_material($idpim){// EN USO TALLER
		$cols="m.idm,m.idg,m.costo_unitario,
				mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
				c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($cols);
		$this->db->from("categoria_producto cp");
		$this->db->where("cp.idpim = '$idpim'");
		$this->db->join('material m','cp.idm=m.idm','inner');
		$this->db->join('material_item mi','m.idmi=mi.idmi','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_row_producto($row,$val){//en uso: alamcen de materiales,TALLER
		$cols="cp.idpim,cp.idcp,cp.idm,cp.portada,
				cpi.color_producto,
				p.idp, p.cod as codigo,p.nombre";
		$this->db->select($cols);
		$this->db->from("categoria_producto cp");
		$this->db->where("$row = '$val'");
		$this->db->join('categoria_pieza cpi','cp.idcp=cpi.idcp','left');
		$this->db->join('producto p','cpi.idp=p.idp');
		$query=$this->db->get();
		return $query->result();
	}
	function get_col_2n($col1,$val1,$col2,$val2){
		$this->db->select("*");
		$this->db->from("categoria_producto");
		$this->db->where("$col1 = '$val1'");
		$this->db->where("$col2 = '$val2'");
		$query=$this->db->get();
		return $query->result();
	}
	function get_producto_almacen_not($ida){//en uso almacen producto,
		//$col="p.idp, p.cod as codigo_p, p.nombre as nombre_p, p.descripcion as descripcion_p, p.fecha_creacion as fecha_creacion_p,p.c_u,p.peso";
		$cols="cp.idpim,cp.idcp,cp.idm,cp.portada";
		$this->db->select($cols);
		$this->db->from("categoria_producto cp");
		$this->db->where("ap.idpim IS NULL");
		$this->db->where("cpi.color_producto = '1'");
		$this->db->join('almacen_producto ap','cp.idpim=ap.idpim','left');
		$this->db->join('categoria_pieza cpi','cp.idcp=cpi.idcp');
		$query=$this->db->get();
		return $query->result();
	}
	function get_col_imagen($col,$val){// en uso: PRODUCCION,
		$cols="cp.idpim,cp.idcp,cp.idm,cp.portada,
				i.idim,i.nombre";
		$this->db->select($cols);
		$this->db->from("categoria_producto cp");
		$this->db->where("$col = '$val'");
		$this->db->join('imagen_producto i','cp.idpim=i.idpim','inner');
		$query=$this->db->get();
		return $query->result();
	}
	/*function get_categoria_producto_pieza_all(){
		$col="cp.idpim,cp.idm,cp.portada,
				p.idpi,p.idp,p.alto,p.ancho,p.imagen,p.nombre,p.codigo,p.descripcion,p.fecha";
		$this->db->select($col);
		$this->db->from("categoria_producto cp");
		$this->db->join('pieza p','cp.idpi=p.idpi','inner');
		$query=$this->db->get();
		return $query->result();
	}

	
	function get_categoria_producto_pieza_idp($idp){
		$col="cp.idpim,cp.idm,cp.portada,
				p.idpi,p.idp,p.alto,p.ancho,p.imagen,p.nombre,p.codigo,p.descripcion,p.fecha";
		$this->db->select($col);
		$this->db->from("categoria_producto cp");
		$this->db->where("p.idp = '$idp' and p.color_producto='1' and cp.portada='1'");
		$this->db->join('pieza p','cp.idpi=p.idpi','inner');
		$query=$this->db->get();
		return $query->result();
	}

	function get_material_pieza($idpi){
		$col="cp.idpim,cp.idpi,cp.portada,
				m.idi,m.idusu,m.ida,m.idg,m.idu,m.idco,m.nombre,m.c_u,m.cantidad,m.descripcion,m.fecha_creacion,m.cod,m.fotografia";
		$this->db->select($col);
		$this->db->from("categoria_producto cp");
		$this->db->where("cp.idpi = '$idpi'");
		$this->db->join('material m','cp.idm=m.idi','inner');
		$query=$this->db->get();
		return $query->result();
	}*/
	
	function get_portada_producto($idp){
		$col="cp.idpim,cp.idcp,cp.idm,cp.portada,cp.idco";
		$this->db->select($col);
		$this->db->from("categoria_producto cp");
		$this->db->where("p.idp = '$idp' and p.color_producto='1' and cp.portada='1'");
		$this->db->join('categoria_pieza p','cp.idcp=p.idcp','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_categoria_producto_colores($idp){// en uso PRODUCTO,
		$col="cp.idpim,cp.idcp,cp.idm,cp.portada,
				c.idco,c.nombre,c.codigo";
		$this->db->select($col);
		$this->db->from("categoria_producto cp");
		$this->db->where("p.idp = '$idp' and p.color_producto = '1'");
		$this->db->join('categoria_pieza p','cp.idcp=p.idcp','inner');
		$this->db->join('color c','c.idco = cp.idco','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_categoria_producto($idpim){//en uso: detalle de pedido
		$col="cp.idpim,cp.idcp,cp.idm,cp.portada,
			p.idp";
		$this->db->select($col);
		$this->db->from("categoria_producto cp");
		$this->db->where("cp.idpim = '$idpim'");
		$this->db->join('categoria_pieza p','cp.idcp=p.idcp','inner');
		$query=$this->db->get();
		return $query->result();
	}

	function get_categoria_grupo($idcp){// en uso: PRODUCCION,PEDIDOS->REPORTE->PRODUCTO,
		$cols="cp.idpim,cp.idcp,cp.portada,cp.idco as idco2,
				m.idm,m.idg,m.costo_unitario,
				mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
				c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($cols);
		$this->db->from("categoria_producto cp");
		$this->db->where("cp.idcp = '$idcp'");
		$this->db->join('material m','cp.idm=m.idm','inner');
		$this->db->join('material_item mi','m.idmi=mi.idmi','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$query=$this->db->get();
		return $query->result();
	}

	function get_categoria_producto_material($idpim){// en uso: PRODUCCION, 
		$cols="cp.idpim,cp.idcp,cp.portada,
				m.idm,m.idg,m.idco,m.costo_unitario,
				mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones";
		$this->db->select($cols);
		$this->db->from("categoria_producto cp");
		$this->db->where("cp.idpim = '$idpim'");
		$this->db->join('material m','cp.idm=m.idm','inner');
		$this->db->join('material_item mi','m.idmi=mi.idmi','inner');
		$query=$this->db->get();
		return $query->result();
	}

	function insertar($idcp,$idm,$portada,$idco){
		$datos=array(
			'idcp' => $idcp,
			'idm' => $idm,
			'portada' => $portada,
			'idco' => $idco
		);
		if($this->db->insert('categoria_producto',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idcp,$idm,$portada){
		$datos=array(
			'idcp' => $idcp,
			'idm' => $idm,
			'portada' => $portada
		);
		if($this->db->update('categoria_producto',$datos,array('idpim' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_row($id,$col,$val){
		$datos=array( $col => $val );
		if($this->db->update('categoria_producto',$datos,array('idpim' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_material($id,$idm){
		$datos=array(
			'idm' => $idm
		);
		if($this->db->update('categoria_producto',$datos,array('idpim' => $id))){
			return true;
		}else{
			return false;
		}
	}

	function eliminar($id){
		if($this->db->query("DELETE FROM categoria_producto WHERE idpim='$id'")){
			return true;
		}else{
			return false;
		}
	}

	function set_col($idpim, $col, $val){
		$datos=array(
			$col => $val,
		);
		if($this->db->update('categoria_producto',$datos,array('idpim' => $idpim))){
			return true;
		}else{
			return false;
		}
	}
	function reset_portada($idcp){
		$datos=array(
			'portada' => 0,
		);
		if($this->db->update('categoria_producto',$datos,array('idcp' => $idcp))){
			return true;
		}else{
			return false;
		}
	}
	function max($col){// en uso: PRODUCCION->PRODUCTO,
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM categoria_producto");
		return $query->result();
	}
}

/* End of file M_categoria_producto.php */
/* Location ./application/models/M_categoria_producto.php*/