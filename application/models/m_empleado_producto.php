<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_empleado_producto extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('empleado_producto');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('empleado_producto',['idepr' => $id]);
		return $query->result();
	}

	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM empleado_producto WHERE idepr='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->query("SELECT * FROM empleado_producto WHERE $col='$val'");
		return $query->result();
	}
	function get_row_2n($col,$val,$col2,$val2){//en uso materiales->configuracion->color
		$query=$this->db->get_where('empleado_producto',array($col => $val,$col2 => $val2));
		return $query->result();
	}
	function get_productos($idpe){//obtiene los productos asignados e los empleados en el pedido
		$col="ep.idepr,ep.tipo,ep.ide,
			dp.iddp,dp.idpe,dp.op,dp.cantidad,dp.estado,dp.cantidad_producida,dp.obs_producida,dp.cantidad_vendida,dp.obs_vendida as observaciones_vendido";
		$this->db->select($col);
		$this->db->from("empleado_producto ep");
		$this->db->where("dp.idpe = $idpe");
		$this->db->order_by("ep.idepr");
		$this->db->join('detalle_pedido dp','dp.iddp = ep.iddp','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_productos_empleado($idpe,$ide){//obtiene los productos asignados e los empleados en el pedido
		$col="ep.idepr,ep.tipo,ep.ide,
			dp.iddp,dp.idpe,dp.idpim,dp.op,dp.cantidad,dp.estado,dp.cantidad_producida,dp.obs_producida,dp.cantidad_vendida,dp.obs_vendida as observaciones_vendido";
		$this->db->select($col);
		$this->db->from("empleado_producto ep");
		$this->db->where("dp.idpe = $idpe");
		$this->db->where("ep.ide = $ide");
		$this->db->order_by("ep.idepr");
		$this->db->join('detalle_pedido dp','dp.iddp = ep.iddp','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_empleados($idpe,$tipo){//obtiene los empleados asignados en el pedido
		$col="ep.idepr,ep.tipo as tipo_tarea,
			dp.iddp,dp.idpe,dp.op,dp.cantidad,dp.estado,dp.cantidad_producida,dp.obs_producida,dp.cantidad_vendida,dp.obs_vendida as observaciones_vendido,
			e.ide,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.tipo,
			p.ci,p.idci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas";
		$this->db->select($col);
		$this->db->from("empleado_producto ep");
		$this->db->where("dp.idpe = '$idpe'");
		$this->db->where("ep.tipo = '$tipo'");
		$this->db->group_by("p.ci");
		$this->db->order_by("p.nombre");
		$this->db->join('detalle_pedido dp','dp.iddp = ep.iddp','inner');
		$this->db->join('empleado e','ep.ide = e.ide','inner');
		$this->db->join("persona p","e.ci=p.ci","inner");
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($ide,$iddp,$tipo,$fecha){
		$datos=array(
			'ide' => $ide,
			'iddp' => $iddp,
			'tipo' => $tipo,
			'fecha' => $fecha
		);
		if($this->db->insert('empleado_producto',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$ide,$iddp,$tipo){
		$datos=array(
			'ide' => $ide,
			'iddp' => $iddp,
			'tipo' => $tipo
		);
		if($this->db->update('empleado_producto',$datos,array('idepr'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('empleado_producto',['idepr' => $id])){
			return true;
		}else{
			return false;
		}
	}	
}

/* End of file m_empleado_producto.php */
/* Location: ./application/models/m_empleado_producto.php*/