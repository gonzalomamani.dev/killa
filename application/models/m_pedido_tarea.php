<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_pedido_tarea extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('pedido_tarea');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('pedido_tarea',['idpt' => $id]);
		return $query->result();
	}

	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM pedido_tarea WHERE idpt='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('pedido_tarea',[$col => $val]);
		return $query->result();
	}
	function get_row_complet($col,$val){// en uso TALLER
		$cols="pt.idpt,pt.idpipr,pt.idppr,pt.ide,pt.ot,pt.fecha_asignacion,pt.fecha_inicio,pt.observacion_inicio,date(pt.fecha_inicio) as inicio,pt.fecha_fin,pt.observacion_fin,date(pt.fecha_fin) as fin,pt.tiempo_por_unidad,pt.estado as estado_ot,
			dp.iddp, dp.idpe,dp.idpim,dp.op,dp.cantidad,dp.estado as estado_op,
			e.ide,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.tipo,
			p.ci,p.idci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
			tc.idtc,tc.horas,tc.tipo as tipo_contrato";
		$this->db->select($cols);
		$this->db->from("pedido_tarea pt");
		$this->db->where("$col = '$val'");
	 	$this->db->order_by("dp.idpe", "desc");
	 	$this->db->order_by("pt.idpt", "asc");
        $this->db->join("detalle_pedido dp","dp.iddp = pt.iddp","inner");
        $this->db->join("empleado e","e.ide = pt.ide","inner");
        $this->db->join("persona p","e.ci=p.ci","inner");
        $this->db->join("tipo_contrato tc","e.idtc=tc.idtc","inner");
		$query=$this->db->get();
		return $query->result();
	}
	function get_row_2n($col,$val,$col2,$val2){//EN USO TALLER
		$query=$this->db->get_where('pedido_tarea',[$col => $val,$col2 => $val2]);
		return $query->result();
	}
	function get_row_3n($col,$val,$col2,$val2,$col3,$val3){
		$query=$this->db->get_where('pedido_tarea',[$col => $val,$col2 => $val2,$col3 => $val3]);
		return $query->result();
	}
	function get_detalle_pedido_proceso_empleado($iddp,$idppr,$ide){
		$query=$this->db->get_where('pedido_tarea',['iddp' => $id]);
		return $query->result();
	}
	function exist_pedido_proceso($iddp,$idppr){
		$resp="";
		$query=$this->db->get_where('pedido_tarea',['iddp' => $iddp,'idppr' => $idppr]);
		$result=$query->result();
		if(count($result)>0){
			$resp=$result[0]->idpt;
		}
		return $resp;
	}
	function get_tareas_pedido($idpe){// en uso produccion,
		$cols="pt.idpt,idpipr,idppr,pt.ide,pt.fecha_asignacion,pt.fecha_inicio,date(pt.fecha_inicio) as inicio,pt.fecha_fin,date(pt.fecha_fin) as fin,pt.tiempo_por_unidad,pt.estado,
				dp.iddp, dp.idpe,dp.idpim,dp.cantidad,dp.estado";
		$this->db->select($cols);
		$this->db->from("pedido_tarea pt");
		$this->db->where("dp.idpe = '$idpe'");
	 	$this->db->order_by("pt.idpt", "desc");
        $this->db->join("detalle_pedido dp","dp.iddp=pt.iddp","inner");
		$query=$this->db->get();
		return $query->result();
	}

	function get_detalle_pedido_producto_proceso($ide){
		$cols="p3e.idpt,p3e.ide,p3e.fecha_asignacion,p3e.fecha_inicio,p3e.fecha_fin,p3e.tiempo_por_unidad,p3e.estado,
				dp.iddp, dp.idpe,dp.idpim,dp.cantidad,dp.estado,
				pp.idppr,pp.idp,pp.idpr,pp.sub_proceso,pp.tiempo_estimado";
		$this->db->select($cols);
		$this->db->from("pedido_tarea p3e");
		if($ide!=""){$this->db->where("p3e.ide = '$ide'");}
	 	$this->db->order_by("p3e.idpt", "desc");
        $this->db->join("detalle_pedido dp","dp.iddp=p3e.iddp","inner");
		$this->db->join('producto_proceso pp','pp.idppr=p3e.idppr','inner');
		$query=$this->db->get();
		return $query->result();
	}
	/*function get_detalle_pedido_pieza_proceso($ide){
		$cols="p3e.idpt,p3e.ide,p3e.fecha_asignacion,p3e.fecha_inicio,p3e.fecha_fin,p3e.tiempo_por_unidad,p3e.estado,
				dp.iddp, dp.idpe,dp.idpim,dp.cantidad,dp.estado,
				pipr.idpipr,pipr.idpr,pipr.tiempo_estimado,
				pi.idpi,pi.nombre,
				cpi.idp";
		$this->db->select($cols);
		$this->db->from("pedido_tarea p3e");

		if($ide!=""){$this->db->where("p3e.ide = '$ide'");}
	 	$this->db->order_by("p3e.idpt", "desc");
        $this->db->join("detalle_pedido dp","dp.iddp=p3e.iddp","inner");
		$this->db->join('pieza_proceso pipr','pipr.idpipr=p3e.idpipr','inner');
		$this->db->join('pieza pi','pi.idpi=pipr.idpi','inner');
		$this->db->join('categoria_pieza cpi','cpi.idcp=pi.idcp','inner');
		$query=$this->db->get();
		return $query->result();
	}*/

	function search_registro_proceso($iddp,$idpr,$idpim){//bucamos si existen tareas del del pedido empleado y proceso
		$cols="pt.idpt,pt.ide,pt.fecha_asignacion,pt.fecha_inicio,pt.fecha_fin,pt.tiempo_por_unidad,pt.estado";
		$this->db->select($cols);
		$this->db->from("pedido_tarea pt");
//		$this->db->where("pt.ide = '$ide'");
		$this->db->where("pt.iddp = '$iddp'");
		$this->db->where("pipr.idpr = '$idpr'");
		$this->db->where("dp.idpim = '$idpim'");
		$this->db->join("pieza_proceso pipr","pipr.idpipr=pt.idpipr","inner");
		$this->db->join("detalle_pedido dp","dp.iddp=pt.iddp","inner");
		$query=$this->db->get();
		return $query->result();
	}
	function search_registro($iddp,$idpipr,$idpim){//bucamos si existen tareas del del pedido empleado y proceso
		$cols="pt.idpt,pt.ide,pt.fecha_asignacion,pt.fecha_inicio,pt.fecha_fin,pt.tiempo_por_unidad,pt.estado";
		$this->db->select($cols);
		$this->db->from("pedido_tarea pt");
		$this->db->where("pt.iddp = '$iddp'");
		$this->db->where("pipr.idpipr = '$idpipr'");
		$this->db->where("dp.idpim = '$idpim'");
		$this->db->join("pieza_proceso pipr","pipr.idpipr=pt.idpipr","inner");
		$this->db->join("detalle_pedido dp","dp.iddp=pt.iddp","inner");
		$query=$this->db->get();
		return $query->result();
	}
	
	
	function insertar_tarea($iddp,$ide,$idpipr,$idppr,$fecha_asignacion,$ot){//caso de pieza proceso
		if($idpipr==""){ $idpipr=NULL; }
		if($idppr==""){ $idppr=NULL; }
		$datos=array(
			'iddp' => $iddp,
			'ide' => $ide,
			'idpipr' => $idpipr,
			'idppr' => $idppr,
			'fecha_asignacion' => $fecha_asignacion,
			'ot' => $ot
		);
		if($this->db->insert('pedido_tarea',$datos)){
			return true;
	    }else{
	    	return false;
		}
	}
	function reset_date($id){
		$datos=array(
			'fecha_inicio' => NULL,
			'observacion_inicio' => NULL,
			'fecha_fin' => NULL,
			'observacion_fin' => NULL,
			'tiempo_por_unidad' => NULL
		);
		if($this->db->update('pedido_tarea',$datos,array('idpt' => $id))){
		  return true;
       }else{
          return false;
       }
	}
	function modificar_tarea($id,$iddp,$ide,$idpipr,$idppr,$fecha_asignacion){
		if($idpipr==""){ $idpipr=NULL; }
		if($idppr==""){ $idppr=NULL; }
		$datos=array(
			'iddp' => $iddp,
			'ide' => $ide,
			'idpipr' => $idpipr,
			'idppr' => $idppr,
			'fecha_asignacion' => $fecha_asignacion
		);
		if($this->db->update('pedido_tarea',$datos,array('idpt' => $id))){
		  return true;
       }else{
          return false;
       }
	}
	function modificar_row_id($id,$col,$val){//EN USO TALLER
		$datos=array(
			$col => $val,
		);
		if($this->db->update('pedido_tarea',$datos,array('idpt' => $id))){
		  return true;
       }else{
          return false;
       }
	}
	function modificar_row_2n($id,$col,$val,$col2,$val2){//EN USO TALLER
		$datos=array(
			$col => $val,
			$col2 => $val2
		);
		if($this->db->update('pedido_tarea',$datos,array('idpt' => $id))){
		  return true;
       }else{
          return false;
       }
	}
	function eliminar($id){
		if($this->db->delete('pedido_tarea',['idpt' => $id])){
		  return true;
       }else{
          return false;
       }
	}
	function eliminar_2n($col,$val,$col2,$val2){
		if($this->db->delete('pedido_tarea',[$col => $val,$col2 => $val2])){
		  return true;
       }else{
          return false;
       }
	}
	function max_row($col,$val,$column){
		$query=$this->db->query("SELECT IFNULL(max($column),0) as max FROM pedido_tarea WHERE $col = '$val'");
		return $query->result();
	}	
	
}