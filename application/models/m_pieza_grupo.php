<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_pieza_grupo extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function get_all(){// en uso Produccion,
		$this->db->order_by("nombre","asc");
		$query=$this->db->get('pieza_grupo');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('pieza_grupo',array('idpig' => $id));
		return $query->result();
	}
	function get_col($id, $col){
		$query=$this->db->query("SELECT $col FROM pieza_grupo WHERE idpig='$id'");
		return $query->result();
	}
	function insertar($nombre,$descripcion){
		$datos=array(
			'nombre' => $nombre,
			'descripcion' => $descripcion
		);
		if($this->db->insert('pieza_grupo',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$nombre,$descripcion){
		$datos=array(
			'nombre' => $nombre,
			'descripcion' => $descripcion
		);
		if($this->db->update('pieza_grupo',$datos,array('idpig' => $id))){
			return true;
		}else{
			return false;
		}
	}

	function eliminar($id){
		if($this->db->query("DELETE FROM pieza_grupo WHERE idpig='$id'")){
			return true;
		}else{
			return false;
		}
	}
	
}

/* End of file M_pieza_grupo.php */
/* Location ./application/models/M_pieza_grupo.php*/