<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_categoria_pieza extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('categoria_pieza');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('categoria_pieza',['idcp' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM categoria_pieza WHERE idcp='$id'");
		return $query->result();
	}
	function get_row($row,$val){
		$query=$this->db->query("SELECT * FROM categoria_pieza WHERE $row='$val'");
		return $query->result();
	}
	function get_row_2n($row,$val,$row2,$val2){// en uso: PRODUCCION,
		$query=$this->db->get_where('categoria_pieza',[$row => $val,$row2 => $val2]);
		return $query->result();
	}
	function get_categoria_pieza_producto($idcp){// en uso: detalle pedido producto
		$col="cp.idcp,cp.color_producto,
			p.idp,p.cod,p.nombre";
		$this->db->select($col);
		$this->db->from("categoria_pieza cp");
		$this->db->where("cp.idcp = '$idcp'");
		$this->db->join("producto p","p.idp=cp.idp","inner");
		$query=$this->db->get();
		return $query->result();
	}
	
	function get_categoria_pieza($idp){//en uso: produccion,
		$col="cp.idcp,cp.color_producto";
		$this->db->select($col);
		$this->db->from("categoria_pieza cp");
		$this->db->where("cp.idp = '$idp'");
		$query=$this->db->get();
		return $query->result();
	}
	
	function get_producto($idpim){// En uso: almacen de productos
		$col="p.idp,p.cod as codigo,p.nombre";
		$this->db->select($col);
		$this->db->from("categoria_pieza cpi");
		$this->db->where("cpr.idpim = '$idpim'");
		$this->db->join("categoria_producto cpr","cpi.idcp=cpr.idcp","inner");
		$this->db->join("producto p","p.idp=cpi.idp","inner");
		$query=$this->db->get();
		return $query->result();
	}
	function get_producto_idcp($idcp){// En uso: Producto
		$col="p.idp,p.cod as codigo,p.nombre";
		$this->db->select($col);
		$this->db->from("categoria_pieza cpi");
		$this->db->where("cpi.idcp = '$idcp'");
		$this->db->join("producto p","p.idp=cpi.idp","inner");
		$query=$this->db->get();
		return $query->result();
	}
	function get_categoria_pieza_color_producto($idp){
		$col="cp.idcp,cp.color_producto";
		$this->db->select($col);
		$this->db->from("categoria_pieza cp");
		$this->db->where("cp.idp = '$idp' and cp.color_producto='1'");
		$query=$this->db->get();
		return $query->result();
	}

	function get_proceso_piezas($idp){//en uso: PRODUCCION->procesos,
		$col="cp.idcp,cp.idp,
			pi.idpi,pi.unidades,pi.imagen,pi.descripcion,
			pg.idpig,pg.nombre,
			pipr.idpipr,pipr.tiempo_estimado,pipr.costo,
			pr.idpr,pr.nombre as nombre_proceso,pr.detalle";
		$this->db->select($col);
		$this->db->from("categoria_pieza cp");
		$this->db->where("cp.idp = '$idp'");
		$this->db->order_by("pipr.idpr", "asc");
		$this->db->join("pieza pi","cp.idcp=pi.idcp","inner");
		$this->db->join("pieza_grupo pg","pi.idpig=pg.idpig","inner");
		$this->db->join("pieza_proceso pipr","pi.idpi=pipr.idpi","inner");
		$this->db->join("proceso pr","pipr.idpr=pr.idpr","inner");
		$query=$this->db->get();
		return $query->result();
	}
	function get_tareas_proceso($idp,$idpr){//en uso: ORDEN DE PRODUCCION,
		$col="cpr.idpim, cpr.portada, cpr.idm,
			cp.idcp,cp.idp,cp.color_producto,
			pi.idpi,pi.idpig,pi.unidades,
			pipr.idpipr,pipr.tiempo_estimado,pipr.costo,
			pr.idpr,pr.nombre as nombre_proceso,pr.detalle";
		$this->db->select($col);
		$this->db->from("categoria_pieza cp");
		$this->db->where("cp.idp = '$idp'");
		$this->db->where("pr.idpr = '$idpr'");
		$this->db->order_by("cp.idcp", "asc");
		$this->db->join("categoria_producto cpr","cpr.idcp=cp.idcp","inner");
		$this->db->join("pieza pi","cp.idcp=pi.idcp","inner");
		$this->db->join("pieza_proceso pipr","pi.idpi=pipr.idpi","inner");
		$this->db->join("proceso pr","pipr.idpr=pr.idpr","inner");
		$query=$this->db->get();
		return $query->result();
	}


		function get_proceso_piezas_where($idp,$col,$val){//en uso: produccion,
		$cols="cp.idcp,cp.idp,pi.idpi,pi.nombre,pi.unidades,pipr.idpipr,pipr.idpr,pipr.tiempo_estimado,pipr.costo";
		$this->db->select($cols);
		$this->db->from("categoria_pieza cp");
		$this->db->where("cp.idp = '$idp'");
		$this->db->where("$col = '$val'");
		$this->db->order_by("pipr.idpr", "asc");
		$this->db->order_by("cpr.idm", "asc");
		$this->db->join("categoria_producto cpr","cpr.idcp=cp.idcp","inner");
		$this->db->join("pieza pi","cp.idcp=pi.idcp","inner");
		$this->db->join("pieza_proceso pipr","pi.idpi=pipr.idpi","inner");
		$query=$this->db->get();
		return $query->result();
	}
	function get_proceso_piezas_group($idp){//en uso: produccion,
		$col="cp.idcp,cp.idp,pi.idpi,pi.nombre,pi.unidades,pipr.idpipr,pipr.idpr,pipr.tiempo_estimado,pipr.costo,pr.idpr,pr.nombre as nombre_proceso";
		$this->db->select($col);
		$this->db->from("categoria_pieza cp");
		$this->db->where("cp.idp = '$idp'");
		$this->db->join("pieza pi","cp.idcp=pi.idcp","inner");
		$this->db->join("pieza_proceso pipr","pi.idpi=pipr.idpi","inner");
		$this->db->group_by("pipr.idpr");
		$this->db->order_by("pipr.idpr", "asc");
		$this->db->join("proceso pr","pr.idpr=pipr.idpr","inner");
		$query=$this->db->get();
		return $query->result();
	}	
	function insertar($idp,$color_producto){
		$datos=array(
			'idp' => $idp,
			'color_producto' => $color_producto
		);
		if($this->db->insert('categoria_pieza',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function get_categoria_color_producto($color_producto){//en uso: Contabilidad, Producto, alamcen de producto 
		$col="cp.idcp,cp.color_producto,
		p.idp, p.cod as codigo, p.nombre, p.descripcion, p.fecha_creacion,p.c_u,p.peso,disenador,etiquetaLogo,plaquetaLogo,repujadoLogo,etiquetaCuero,correa,cierreDelante,cierreDetras,cierreDentro,largo,alto,ancho,repujado";
		$this->db->select($col);
		$this->db->from("categoria_pieza cp");
		$this->db->where("cp.color_producto = '$color_producto'");
		$this->db->join('producto p','cp.idp = p.idp','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_where_categoria_color_producto($col,$val,$color_producto){//en uso: almacen de producto 
		$cols="cp.idcp,cp.color_producto,
		p.idp, p.cod as codigo, p.nombre, p.descripcion, p.fecha_creacion,p.c_u,p.peso,disenador,etiquetaLogo,plaquetaLogo,repujadoLogo,etiquetaCuero,correa,cierreDelante,cierreDetras,cierreDentro,largo,alto,ancho,repujado";
		$this->db->select($cols);
		$this->db->from("categoria_pieza cp");
		$this->db->where("cp.color_producto = '$color_producto'");
		$this->db->where("$col like '$val%'");
		$this->db->join('producto p','cp.idp = p.idp','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function modificar($id,$idp,$color_producto){
		$datos=array(
			'idp' => $idp,
			'color_producto' => $color_producto
		);
		if($this->db->update('categoria_pieza',$datos,array('idcp' => $id))){
			return true;
		}else{
			return false;
		}
		
	}
	function eliminar($id){
		if($this->db->delete('categoria_pieza',['idcp' => $id])){
			return true;
		}else{
			return false;
		}
	}

	function set_col($idcp,$col,$val){
		$datos=array(
			$col => $val
		);
		if($this->db->update('categoria_pieza',$datos,array('idcp' => $idcp))){
			return true;
		}else{
			return false;
		}
	}
	function reset_color_producto($idp){
		$datos=array(
			'color_producto' => 0
		);
		if($this->db->update('categoria_pieza',$datos,array('idp' => $idp))){
			return true;
		}else{
			return false;
		}
	}
	
}

/* End of file m_categoria_pieza.php */
/* Location: ./application/models/m_categoria_pieza.php*/