<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_persona extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('persona');
		return $query->result();
	}
	function get($id){
		$col="p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
			pa.idpa,pa.nombre as pais,
			ci.idci,ci.nombre as ciudad";
				$this->db->select($col);
		$this->db->from("persona p");
		$this->db->join('ciudad ci','ci.idci = p.idci','inner');
		$this->db->join('pais pa','pa.idpa = ci.idpa','inner');
		$this->db->where("p.ci = '$id'");
		$this->db->order_by("p.nombre", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM persona WHERE ci='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('persona',[$col => $val]);
		return $query->result();
	}
	function insertar($ci,$idci,$nombre,$telefono,$email,$direccion,$fotografia,$caracteristicas){
		$datos=array(
			'ci' => $ci,
			'idci' => $idci,
			'nombre' => $nombre,
			'telefono' => $telefono,
			'email' => $email,
			'direccion' => $direccion,
			'fotografia' => $fotografia,
			'caracteristicas' => $caracteristicas
		);
		if($this->db->insert('persona',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$ci,$idci,$nombre,$telefono,$email,$direccion,$fotografia,$caracteristicas){//en uso: CAPITAL HUMANO,
		$datos=array(
			'ci' => $ci,
			'idci' => $idci,
			'nombre' => $nombre,
			'telefono' => $telefono,
			'email' => $email,
			'direccion' => $direccion,
			'fotografia' => $fotografia,
			'caracteristicas' => $caracteristicas
		);
		if($this->db->update('persona',$datos,array('ci'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_usuario($id,$ci,$idci,$nombre,$fotografia){//en uso: ADMINISTRACION
		$datos=array(
			'ci' => $ci,
			'idci' => $idci,
			'nombre' => $nombre,
			'fotografia' => $fotografia
		);
		if($this->db->update('persona',$datos,array('ci'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($ci){
		if($this->db->delete('persona',['ci' => $ci])){
			return true;
		}else{
			return false;
		}
	}	
}

/* End of file m_persona.php */
/* Location: ./application/models/m_persona.php*/