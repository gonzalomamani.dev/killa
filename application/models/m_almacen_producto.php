<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_almacen_producto extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("idi", "asc");
		$query=$this->db->get('almacen_producto');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('almacen_producto',['idap'=>$id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM almacen_producto WHERE idap='$id'");
		return $query->result();
	}
	function get_row($col,$val){//en uso: PRODUCTO,
		$query=$this->db->get_where('almacen_producto',array($col => $val));
		return $query->result();
	}	
	function get_row_2n($col1,$val1,$col2,$val2){//en uso: Producto,
		$cols="*";
		$this->db->select($cols);
		$this->db->from("almacen_producto");
		$this->db->where("$col1 = '$val1' and $col2 = '$val2'");
		$query=$this->db->get();
		return $query->result();
	}	

	function get_producto_almacen_all(){
		$col="ap.idap,ap.ida, ap.fecha, ap.cantidad, 
				cp.idpim,cp.idpi,cp.idma,cp.color";
		$this->db->select($col);
		$this->db->from("almacen_producto ap");
		$this->db->join('categoria_producto cp','ap.idpim = cp.idpim','inner');
		$query=$this->db->get();
		return $query->result();
	}

	function get_producto_almacen_ida($ida){//en uso: PRODUCTO, 
		$cols="ap.idap,ap.ida, ap.fecha, ap.cantidad, 
				cp.idpim,cp.idcp,cp.idm,cp.portada,
				p.idp,p.cod as codigo,p.nombre,p.descripcion";
		$this->db->select($cols);
		$this->db->from("almacen_producto ap");
		$this->db->where("ap.ida = '$ida'");
		$this->db->join('categoria_producto cp','ap.idpim = cp.idpim','inner');
		$this->db->join('categoria_pieza cpi','cp.idcp = cpi.idcp','inner');
		$this->db->join('producto p','cpi.idp = p.idp','inner');
		$this->db->order_by('p.nombre');
		$query=$this->db->get();
		return $query->result();
	}

	function get_producto_almacen_ida_row($ida,$col,$val){//en uso: producto, movimiento de productos, 
		$cols="ap.idap,ap.ida, ap.fecha, ap.cantidad, 
				cp.idpim,cp.idcp,cp.idm,cp.portada,
				p.idp,p.cod as codigo, p.nombre";
		$this->db->select($cols);
		$this->db->from("almacen_producto ap");
		$this->db->where("ap.ida = '$ida'");
		$this->db->where("$col like '$val%'");
		$this->db->join('categoria_producto cp','ap.idpim = cp.idpim','inner');
		$this->db->join('categoria_pieza cpi','cp.idcp = cpi.idcp','inner');
		$this->db->join('producto p','cpi.idp = p.idp','inner');
		$this->db->order_by('p.nombre');
		$query=$this->db->get();
		return $query->result();
	}


	
	function insertar($ida,$idpim,$cantidad){// en uso: Producto
		$datos=array(
			'ida' => $ida,
			'idpim' => $idpim,
			'cantidad' => $cantidad
		);
		if($this->db->insert('almacen_producto',$datos)){
			return true;
		}else{
			return true;
		}
	}
	function modificar($idap,$ida,$idpim,$cantidad){
		$datos=array(
			'ida' => $ida,
			'idpim' => $idp,
			'cantidad' => $cantidad
		);
		if($this->db->update('almacen_producto',$datos,array('idap'=>$idap))){
			return true;
		}else{
			return true;
		}
	}
	function modificar_col($idap,$col,$val){
		$datos=array(
			$col.'' => $val
		);
		if($this->db->update('almacen_producto',$datos,array('idap'=>$idap))){
			return true;
		}else{
			return true;
		}
	}
	
	function eliminar($id){
		if($this->db->delete('almacen_producto',['idap' => $id])){
			return true;
		}else{
			return true;
		}
	}

	
}

/* End of file m_almacen_producto.php */
/* Location: ./application/models/m_almacen_producto.php*/