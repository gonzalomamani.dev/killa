<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_estado_cuentas extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){// en uso: capital humano,
		$query=$this->db->get('estado_cuentas');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('estado_cuentas',['idec' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM estado_cuentas WHERE idec='$id'");
		return $query->result();
	}
	function get_cta_estructura($val){
		$cols="e.idec,e.idpl,e.idee,e.texto,
				p.grupo,p.sub_grupo,p.rubro,p.cuenta,p.cuenta_text, CONCAT((IFNULL((p.grupo),(''))),(IFNULL((p.sub_grupo),(''))),(IFNULL((p.rubro),(''))),(IFNULL((p.cuenta),('')))) AS codigo";
		$this->db->select($cols);
		$this->db->from("estado_cuentas e");
		$this->db->where("e.idee = '$val'");
		$this->db->join('plan_cuenta p','p.idpl = e.idpl','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_row($col,$val){
		$cols="*";
		$this->db->select($cols);
		$this->db->from("estado_cuentas");
		$this->db->where("$col = '$val'");
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idpl,$idee){
		$datos=array(
			'idpl' => $idpl,
			'idee' => $idee
		);
		if($this->db->insert('estado_cuentas',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idpl,$idee,$texto){
		$datos=array(
			'idpl' => $idpl,
			'idee' => $idee,
			'texto' => $texto
		);
		if($this->db->update('estado_cuentas',$datos,array('idec'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('estado_cuentas',['idec' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function eliminar_row($col,$val){
		if($this->db->delete('estado_cuentas',[''.$col => $val])){
			return true;
		}else{
			return false;
		}
	}		
}

/* End of file m_estado_cuentas.php */
/* Location: ./application/models/m_estado_cuentas.php*/