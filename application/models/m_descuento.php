<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_descuento extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("idde", "asc");
		$query=$this->db->get('descuento');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('descuento',['idde' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM descuento WHERE idde='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$cols="*";
		$this->db->select($cols);
		$this->db->from("descuento");
		$this->db->order_by("fecha", "asc");
		$this->db->where("$col = '$val'");
		$query=$this->db->get();
		return $query->result();
		return $query->result();
	}
	function insertar($idpe,$fecha,$monto,$observaciones){
		$datos=array(
			'idpe' => $idpe,
			'fecha' => $fecha,
			'monto' => $monto,
			'observaciones' => $observaciones
		);
		if($this->db->insert('descuento',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$fecha,$monto,$observaciones){
		$datos=array(
			'fecha' => $fecha,
			'monto' => $monto,
			'observaciones' => $observaciones
		);
		if($this->db->update('descuento',$datos,array('idde' => $id))){
			return true;
		}else{
			return false;
		}
		
	}
	function eliminar($id){
		if($this->db->delete('descuento',['idde' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}

/* End of file m_descuento.php */
/* Location: ./application/models/m_descuento.php*/