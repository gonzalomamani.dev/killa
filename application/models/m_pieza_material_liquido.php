<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_pieza_material_liquido extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('pieza_material_liquido');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('pieza_material_liquido',['material_adicional' => $id]);
		return $query->result();
	}

	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM pieza_material_liquido WHERE idpml='$id'");
		return $query->result();
	}
	function get_row($col,$val){// en uso: CONFIGURACION->PRODUCION,
		$query=$this->db->get_where('pieza_material_liquido',array($col => $val));
		return $query->result();
	}
	function get_row_2n($col1,$val1,$col2,$val2){// en uso: Produccion,
		$query=$this->db->get_where('pieza_material_liquido',array($col1=>$val1,$col2=>$val2));
		return $query->result();
	}
	function insertar($idml,$idpi){
		$datos=array(
			'idml' => $idml,
			'idpi' => $idpi
		);
		if($this->db->insert('pieza_material_liquido',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idml,$idpi){
		$datos=array(
			'idml' => $idml,
			'idpi' => $idpi
		);
		if($this->db->update('pieza_material_liquido',$datos,array('idpml'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('pieza_material_liquido',['idpml' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}

/* End of file m_pieza_material_liquido.php */
/* Location: ./application/models/m_pieza_material_liquido.php*/