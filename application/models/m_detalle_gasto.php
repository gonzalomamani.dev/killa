<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_detalle_gasto extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('detalle_gasto');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('detalle_gasto',['iddga' => $id]);
		return $query->result();
	}
	function get_gasto($idga){// en usp COMPRAS,
		$col="ddg.iddga,dg.categoria,dg.codigo_control,dg.numero_factura,dg.autorizacion,dg.cantidad_sistema,dg.cantidad,dg.importe,dg.descuento,dg.ice,dg.excento,dg.neto,dg.debito,dg.idpl,dg.idcm,
				g.idga,g.ide,g.orden_gasto,g.fecha,g.tipo,g.observaciones";
		$this->db->select($col);
		$this->db->from("detalle_gasto dg");
		$this->db->where("g.idga = $idga");
		$this->db->join('gasto g','g.idga = dg.idga','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function view_gastos($col,$val){// en usp COMPRAS,
		$cols="dg.iddga,dg.categoria,dg.codigo_control,dg.numero_factura,dg.autorizacion,dg.cantidad_sistema,dg.cantidad,dg.importe,dg.descuento,dg.ice,dg.excento,dg.neto,dg.debito,dg.idpl,dg.idcm,
				g.idga,g.ide,g.orden_gasto,g.fecha,g.tipo,g.observaciones,
				mp.idmp,mp.costo_unitario,
				mi.idmi,mi.idu,mi.codigo,mi.nombre as nombre_material,mi.fotografia,mi.observaciones as observacion_material,
				pr.idpro,pr.nit,pr.encargado,pr.url,
				p.ci,p.idci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,";
		$this->db->select($cols);
		$this->db->from("detalle_gasto dg");
		$this->db->order_by("g.fecha","asc");
	if($col!="" && $val!=""){
		if($col=="p.ci" || $col=="p.nombre"){$this->db->where("$col like '$val%'");}
		if($col=="g.fecha" || $col=="dg.iddga" || $col=="pr.idpro" || $col=="g.idga"){$this->db->where("$col = '$val'");}
		if($col=="fecha"){$vf=explode("|", $val); $this->db->where("g.fecha >= '$vf[0]' AND g.fecha <= '$vf[1]'");}
		if($col=="anio_mes"){$vf=explode("-", $val); $this->db->where("year(g.fecha) = '$vf[0]' AND month(g.fecha) = '$vf[1]'");}
	}
		$this->db->join('gasto g','g.idga = dg.idga','inner');
		$this->db->join('material_proveedor mp','mp.idmp = dg.idmp','inner');
		$this->db->join('proveedor pr','pr.idpro = mp.idpro','inner');
		$this->db->join('persona p','p.ci = pr.nit','inner');
		$this->db->join('material_item mi','mi.idmi = mp.idmi','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_fecha($fecha1,$fecha2){
		$col="*";
		$this->db->select($col);
		$this->db->from("detalle_gasto dg");
		$this->db->where("g.fecha >= '$fecha1'");
		$this->db->where("g.fecha <= '$fecha2'");
		$this->db->order_by("dg.idga", "asc");
		$this->db->order_by("g.fecha", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get_fecha_cuenta($fecha1,$fecha2){
		$col="fecha,SUM(neto) as neto, idpl";
		$this->db->select($col);
		$this->db->from("detalle_gasto");
		$this->db->where("fecha >= '$fecha1'");
		$this->db->where("fecha <= '$fecha2'");
		$this->db->where("idpl != 'NULL'");
		$this->db->where("idpl != ''");
		$this->db->group_by("fecha");
		$this->db->group_by("idpl");
		$this->db->order_by("fecha", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get_detalle_gasto_producto($id){
		/*$col="dp.iddga,dp.idmp,dp.ga,dp.fecha,dp.tipo,dp.codigo_control,dp.numero_factura,dp.autorizacion,dp.cantidad_sistema,dp.cantidad,dp.importe,dp.descuento,dp.ice,dp.excento,dp.neto,dp.debito";
		$this->db->select($col);
		$this->db->from("detalle_gasto dp");
		$this->db->where("dp.idpe = '$idpe'");
		$query=$this->db->get();
		return $query->result();*/
	}
	function get_sin_order_fecha($fecha1,$fecha2){
		$col="dg.iddga,dg.idmp,g.fecha,dg.tipo,dg.codigo_control,dg.numero_factura,dg.autorizacion,dg.cantidad_sistema,dg.cantidad,dg.importe,dg.descuento,dg.ice,dg.excento,dg.neto,dg.debito,dg.idpl";
		$this->db->select($col);
		$this->db->from("detalle_gasto dg");
		$this->db->where("g.fecha >= '$fecha1'");
		$this->db->where("g.fecha <= '$fecha2'");
		$this->db->where("g.orden_gasto = '' OR g.orden_gasto = NULL");
		$this->db->order_by("g.fecha", "asc");
		$this->db->order_by("dg.iddga", "asc");
		$this->db->join('gasto g','g.idga = dg.idga','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM detalle_gasto WHERE iddga='$id'");
		return $query->result();
	}
	function get_row($col,$val){// en uso: PROVEEDOR,
		$query=$this->db->get_where('detalle_gasto',[$col => $val]);
		return $query->result();
	}
	function get_row_2n($row1,$val1,$row2,$val2){
		$query=$this->db->query("SELECT * FROM detalle_gasto WHERE $row1='$val1' AND $row2='$val2'");
		return $query->result();
	}
	function insertar_con_orden($idmp,$idga,$categoria,$cantidad,$importe,$neto){
		$datos=array(
			'idmp' => $idmp,
			'idga' => $idga,
			'categoria' => $categoria,
			'cantidad' => $cantidad,
			'importe' => $importe,
			'neto' => $neto
		);
		if($this->db->insert('detalle_gasto',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function insertar_sin_orden($idmp,$idga,$numero_factura,$categoria,$cantidad,$importe,$neto){
		$datos=array(
			'idmp' => $idmp,
			'idga' => $idga,
			'numero_factura' => $numero_factura,
			'categoria' => $categoria,
			'cantidad' => $cantidad,
			'importe' => $importe,
			'neto' => $neto
		);
		if($this->db->insert('detalle_gasto',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function insertar($idmp,$idga,$fecha,$categoria,$tipo,$codigo_control,$numero_factura,$autorizacion,$cantidad_sistema,$cantidad,$importe,$descuento,$ice,$excento,$neto,$debito){
		$datos=array(
			'idmp' => $idmp,
			'idga' => $idga,
			'fecha' => $fecha,
			'categoria' => $categoria,
			'tipo' => $tipo,
			'codigo_control' => $codigo_control,
			'numero_factura' => $numero_factura,
			'autorizacion' => $autorizacion,
			'cantidad_sistema' => $cantidad_sistema,
			'cantidad' => $cantidad,
			'importe' => $importe,
			'descuento' => $descuento,
			'ice' => $ice,
			'excento' => $excento,
			'neto' => $neto,
			'debito' => $debito
		);
		if($this->db->insert('detalle_gasto',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($iddga,$idmp,$idga,$fecha,$tipo,$codigo_control,$numero_factura,$autorizacion,$cantidad_sistema,$cantidad,$importe,$descuento,$ice,$excento,$neto,$debito){
		$datos=array(
			'idmp' => $idmp,
			'idga' => $idga,
			'fecha' => $fecha,
			'tipo' => $tipo,
			'codigo_control' => $codigo_control,
			'numero_factura' => $numero_factura,
			'autorizacion' => $autorizacion,
			'cantidad_sistema' => $cantidad_sistema,
			'cantidad' => $cantidad,
			'importe' => $importe,
			'descuento' => $descuento,
			'ice' => $ice,
			'excento' => $excento,
			'neto' => $neto,
			'debito' => $debito
		);
		if($this->db->update('detalle_gasto',$datos,array('iddga'=>$iddga))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_col($iddga,$col,$val){
		$datos=array(
			''.$col => $val
		);
		if($this->db->update('detalle_gasto',$datos,array('iddga'=>$iddga))){
			return true;
		}else{
			return false;
		}
	}

	function modificar_row_where($col,$val,$col2,$val2){
		$datos=array(
			''.$col2 => $val2
		);
		if($this->db->update('detalle_gasto',$datos,array(''.$col=>$val))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_sin_orden($iddga,$idmp,$idga,$numero_factura,$categoria,$cantidad,$importe,$neto){
		$datos=array(
			'idmp' => $idmp,
			'idga' => $idga,
			'numero_factura' => $numero_factura,
			'categoria' => $categoria,
			'cantidad' => $cantidad,
			'importe' => $importe,
			'neto' => $neto
		);
		if($this->db->update('detalle_gasto',$datos,array('iddga'=>$iddga))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar_row($col,$val){
		if($this->db->delete('detalle_gasto',[$col => $val])){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($iddga){
		if($this->db->delete('detalle_gasto',['iddga' => $iddga])){
			return true;
		}else{
			return false;
		}
	}	
}

/* End of file m_detalle_gasto.php */
/* Location: ./application/models/m_detalle_gasto.php*/