<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_grupo_cuenta extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("idgru", "asc");
		$query=$this->db->get('grupo_cuenta');
		return $query->result();
	}
	function get($id){
		$this->db->order_by("idgru", "asc");
		$query=$this->db->get_where('grupo_cuenta',array('idgru' => $id));
		return $query->result();
	}
	function get_col($id, $col){
		$query=$this->db->query("SELECT $col FROM grupo_cuenta WHERE idgru='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$this->db->order_by("nombre", "asc");
		$query=$this->db->get_where('grupo_cuenta',array($col => $val));
		return $query->result();
	}
	function insertar($idgru,$nombre){
		$datos=array(
			'idgru' => $idgru,
			'nombre' => $nombre
		);
		if($this->db->insert('grupo_cuenta',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idgru,$nombre){
		$datos=array(
			'idgru' => $idgru,
			'nombre' => $nombre
		);
		if($this->db->update('grupo_cuenta',$datos,array('idgru' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('grupo_cuenta',array('idgru' => $id))){
			return true;
		}else{
			return false;
		}
	}
}
/* End of file M_grupo_cuenta.php */
/* Location ./application/models/M_grupo_cuenta.php */