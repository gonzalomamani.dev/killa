<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_archivo_biometrico extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('archivo_biometrico');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('archivo_biometrico',['idab' => $id]);
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->query("SELECT * FROM archivo_biometrico WHERE $col='$val'");
		return $query->result();
	}
	function get_row_2n($col,$val,$col2,$val2){
		$this->db->select("*");
		$this->db->from("archivo_biometrico");
		$this->db->where("$col ='$val'");
		$this->db->where("$col2 ='$val2'");
		$query=$this->db->get();
		return $query->result();
	}
	
	function insertar($ide,$archivo,$fecha){
		$datos=array(
			'ide' => $ide,
			'archivo' => $archivo,
			'fecha' => $fecha
		);
		if($this->db->insert('archivo_biometrico',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$nombre,$anio,$mes){
		$datos=array(
			'nombre' => $nombre,
			'anio' => $anio,
			'mes' => $mes
		);
		if($this->db->update('archivo_biometrico',$datos,array('idab'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('archivo_biometrico',['idab' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function max_id($col){
		$query=$this->db->query("SELECT max($col)as max FROM archivo_biometrico");
		return $query->result();
	}	
}

/* End of file m_archivo_biometrico.php */
/* Location: ./application/models/m_archivo_biometrico.php*/