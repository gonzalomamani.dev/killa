<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_pieza_proceso extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('pieza_proceso');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('pieza_proceso',['idpipr' => $id]);
		return $query->result();
	}
	function get_proceso($idpipr){
		$cols="pipr.idpipr,pipr.tiempo_estimado,pipr.costo,pipr.tipo,
			pr.idpr,pr.nombre as nombre_proceso,pr.detalle,
            pi.idpi, pi.idpig, pi.idcp, pi.alto,pi.ancho,pi.imagen,pi.nombre as nombre_pieza,pi.codigo,pi.descripcion,pi.fecha, pi.unidades";
      $this->db->select($cols);
      $this->db->from("pieza_proceso pipr");
      $this->db->where("pipr.idpipr = '$idpipr'");
      //$this->db->order_by("pi.idcp");
      $this->db->join("proceso pr","pipr.idpr = pr.idpr","inner");
      $this->db->join("pieza pi","pipr.idpi = pi.idpi","inner");
      $query=$this->db->get();
      return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM pieza_proceso WHERE idpipr='$id'");
		return $query->result();
	}
	function get_row($col,$val){//En uso: CONFIGURACION->PRODCCION
		$query=$this->db->get_where('pieza_proceso',array($col => $val));
		return $query->result();
	}
	function get_row_2n($col1,$val1,$col2,$val2){
		$query=$this->db->get_where('pieza_proceso',array($col1=>$val1,$col2=>$val2));
		return $query->result();
	}
	function insertar($idpr,$idpi){
		$datos=array(
			'idpr' => $idpr,
			'idpi' => $idpi
		);
		if($this->db->insert('pieza_proceso',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar_tiempo_estimado_costo($id,$tiempo_estimado,$costo){// en uso: PRODUCTO,
		$datos=array(
			'tiempo_estimado'=>$tiempo_estimado,
			'costo' => $costo
		);
		if($this->db->update('pieza_proceso',$datos,array('idpipr' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idpr,$idpi){
		$datos=array(
			'idpr' => $idpr,
			'idpi' => $idpi
		);
		if($this->db->update('pieza_proceso',$datos,array('idpipr'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('pieza_proceso',['idpipr' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}

/* End of file m_pieza_proceso.php */
/* Location: ./application/models/m_pieza_proceso.php*/