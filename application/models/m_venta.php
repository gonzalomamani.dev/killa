<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_venta extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('venta');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('venta',['idv' => $id]);
		return $query->result();
	}
	function get_pedido($col,$val){// en uso: VENTAS,
		$cols="v.idv,v.ide,v.monto_sistema as monto_sistema_venta,v.monto_parcial as monto_parcial_venta,v.descuento as descuento_venta,v.monto_total,v.fecha,v.observaciones as observaciones_venta,
				p.idpe,p.numero,p.fecha_pedido,p.tiempo,p.fecha_inicio,p.fecha_estimada,p.fecha_entrega,p.monto_sistema,p.monto_parcial,p.descuento,p.monto_total as monto_total_pedido,p.observaciones,p.estado,
				cl.idcl, cl.nit, cl.encargado, cl.url,
				pe.ci,pe.nombre,pe.telefono,pe.email,pe.direccion,pe.fotografia,pe.caracteristicas,
				c.idci,c.nombre as ciudad,
				pa.idpa,pa.nombre as pais";
		$this->db->select($cols);
		$this->db->from("venta v");
		$this->db->order_by("v.idv", "desc");
	if($col!="" && $val!=""){
		if($col=="pe.ci" || $col=="pe.nombre"){$this->db->where("$col like '$val%'");}
		if($col=="v.fecha" || $col=="v.idv" || $col=="cl.idcl"){$this->db->where("$col = '$val'");}
		if($col=="fecha"){$vf=explode("|", $val); $this->db->where("v.fecha >= '$vf[0]' AND v.fecha <= '$vf[1]'");}
	}
		$this->db->join('pedido p','p.idpe = v.idpe','inner');
		$this->db->join('cliente cl','p.idcl = cl.idcl','inner');
		$this->db->join('persona pe','pe.ci = cl.nit','inner');
		$this->db->join('ciudad c','c.idci = pe.idci','inner');
		$this->db->join('pais pa','c.idpa = pa.idpa','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM venta WHERE idv='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('venta',array($col => $val));
		return $query->result();
	}
	function insertar($idpe, $ide, $monto_sistema,$monto_parcial, $descuento,$monto_total, $fecha, $observaciones){
		$datos=array(
			'idpe' => $idpe,
			'ide' => $ide,
			'monto_sistema' => $monto_sistema,
			'monto_parcial' => $monto_parcial,
			'descuento' => $descuento,
			'monto_total' => $monto_total,
			'fecha' => $fecha,
			'observaciones' => $observaciones
		);
		if($this->db->insert('venta',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id, $ide, $monto_sistema,$monto_parcial, $descuento,$monto_total, $fecha, $observaciones){
		$datos=array(
			'ide' => $ide,
			'monto_sistema' => $monto_sistema,
			'monto_parcial' => $monto_parcial,
			'descuento' => $descuento,
			'monto_total' => $monto_total,
			'fecha' => $fecha,
			'observaciones' => $observaciones
		);
		if($this->db->update('venta',$datos,array('idv'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('venta',['idv' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}

/* End of file m_venta.php */
/* Location: ./application/models/m_venta.php*/