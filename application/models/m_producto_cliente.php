<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto_cliente extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('producto_cliente');
		return $query->result();
	}
	function get_row($col,$val){// en uso: CLIENTE_PROVEEDOR->CONFIGURACION, 
		$query=$this->db->get_where('producto_cliente',[$col => $val]);
		return $query->result();
	}
	function get_productos_clientes($atrib,$val,$order,$control_order,$group){//en uso: PEDIDO,
		$cols="pc.idpc,pc.idcl,pc.iduv,pc.costo_unitario,
				cp.idpim,
				cpi.idcp,
				p.idp,p.cod as codigo,p.nombre,p.descripcion,SUBSTRING_INDEX(p.cod,' ',1) as o_cod,SUBSTRING_INDEX(p.cod,' ', -1) as o_num";
		$from="producto_cliente pc,categoria_producto cp,categoria_pieza cpi,producto p";
		$where="cp.idpim = pc.idpim and cpi.idcp = cp.idcp and p.idp = cpi.idp";
		if($atrib!="" && $val!=""){
			if($atrib=="pc.idcl" || $atrib=="cp.idpim" || $atrib=="p.idp"){ $where.=" and $atrib = '$val'";}
			if($atrib=="p.cod" || $atrib=="p.nombre"){ $where.=" and $atrib like '%$val%'";}
		}
		if($group!=""){ $group="group by(".$group.")"; }
		if($order!="" && ($control_order=="asc" || $control_order=="desc")){ if($order!="p.cod"){ $order="order by ".$order." ".$control_order; }else{ $order="order by o_cod ".$control_order.",o_num+0 ".$control_order;} }
		$query=$this->db->query("SELECT $cols FROM $from WHERE $where $group $order");
		return $query->result();
	}










	
	function get_row_2n($row1,$val1,$row2,$val2){//en uso:NUEVO PEDIDO,PEDIDO->CONFIG
		$col="pcl.idpc,pcl.idcl,pcl.idpim,pcl.iduv,pcl.costo_unitario";
		$this->db->select($col);
		$this->db->from("producto_cliente pcl");
		$this->db->where("pcl.$row1 = '$val1' and pcl.$row2 = '$val2'");
		$query=$this->db->get();
		return $query->result();
	}
	
	function get_productos($idcl){// en uso: Clientes,PEDIDO
		$cols="pc.idpc,pc.idcl,pc.iduv,pc.costo_unitario,
				cp.idpim,
				cpi.idcp,
				p.idp,p.cod as codigo,p.nombre,p.descripcion,
				c.codigo as codigo_color,c.nombre as nombre_color";
		$this->db->select($cols);
		$this->db->from("producto_cliente pc");
		$this->db->order_by("cp.idpim","asc");
		$this->db->where("pc.idcl = '$idcl'");
		$this->db->join('categoria_producto cp','cp.idpim = pc.idpim','inner');
		$this->db->join('categoria_pieza cpi','cpi.idcp = cp.idcp','inner');
		$this->db->join('producto p','p.idp = cpi.idp','inner');
		$this->db->join('material m','m.idm = cp.idm','inner');
		$this->db->join('color c','c.idco = m.idco','inner');
		$query=$this->db->get();
		return $query->result();
	}

	/*function get_producto_cliente($idcl){//segun el pedido
		$col="c.idpc, c.idcl,c.iduv,c.costo_unitario,
				cp.idpim, cp.idcp,cp.idm, cp.portada";
		$this->db->select($col);
		$this->db->from("producto_cliente c");
		$this->db->order_by("cp.idpim","asc");
		$this->db->where("c.idcl = '$idcl'");
		$this->db->join('categoria_producto cp','cp.idpim = c.idpim','inner');
		$query=$this->db->get();
		return $query->result();
	}*/
	function get_producto_cliente($idcl){//en uso: PEDIDO,
		$cols="pc.idpc, pc.idcl,pc.iduv,pc.costo_unitario,
				cp.idpim,cp.idm, cp.portada,
				cpi.idcp, cpi.idp,cpi.color_producto";
		$this->db->select($cols);
		$this->db->from("producto_cliente pc");
		$this->db->order_by("cp.idpim","asc");
		$this->db->where("pc.idcl = '$idcl'");
		$this->db->join('categoria_producto cp','cp.idpim = pc.idpim','inner');
		$this->db->join('categoria_pieza cpi','cpi.idcp = cp.idcp','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('producto_cliente',['idpc' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM producto_cliente WHERE idpc='$id'");
		return $query->result();
	}

	function insertar($idcl,$idpim,$iduv){
		$datos=array(
			'idcl' => $idcl,
			'idpim' => $idpim,
			'iduv' => $iduv
		);
		if($this->db->insert('producto_cliente',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($idpc,$idpe,$idpim,$cantidad,$estado){
		$datos=array(
			'idpe' => $idpe,
			'idpim' => $idpim,
			'cantidad' => $cantidad,
			'estado' => $estado
		);
		if($this->db->update('producto_cliente',$datos,array('idpc'=>$idpc))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_row_2n($idpc,$row,$value,$row2,$value2){//en uso clientes,
		$datos=array(
			$row => $value,
			$row2 => $value2
		);
		if($this->db->update('producto_cliente',$datos,array('idpc'=>$idpc))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($idpc){
		if($this->db->delete('producto_cliente',['idpc' => $idpc])){
			return true;
		}else{
			return false;
		}
	}	
}

/* End of file m_producto_cliente.php */
/* Location: ./application/models/m_producto_cliente.php*/