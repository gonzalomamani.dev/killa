<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_proveedor extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$col="p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
				pa.idpa,pa.nombre as pais,
				ci.idci,ci.nombre as ciudad,  
				pr.idpro,pr.nit,pr.encargado,pr.url";
		$this->db->select($col);
		$this->db->from("proveedor pr");
		$this->db->join('persona p','pr.nit = p.ci','inner');
		$this->db->join('ciudad ci','ci.idci = p.idci','inner');
		$this->db->join('pais pa','pa.idpa = ci.idpa','inner');
		$this->db->order_by("p.nombre", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get($id){//en uso: Proveedor,
		$col="p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas, 
				pa.idpa,pa.nombre as pais,
				ci.idci,ci.nombre as ciudad,  
				pr.idpro,pr.nit,pr.encargado,pr.url";
		$this->db->select($col);
		$this->db->from("proveedor pr");
		$this->db->join('persona p','pr.nit = p.ci','inner');
		$this->db->join('ciudad ci','ci.idci = p.idci','inner');
		$this->db->join('pais pa','pa.idpa = ci.idpa','inner');
		$this->db->where("pr.idpro = '$id'");
		$query=$this->db->get();
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM proveedor WHERE idpro='$id'");
		return $query->result();
	}
    function get_row($col,$val){//en uso, Clientes, 
      $query=$this->db->get_where('proveedor',[$col => $val]);
      return $query->result();
    }
	function get_row_like($atrib,$val){
		$col="p.ciudad,p.pais,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas, 
				pa.idpa,pa.nombre as pais,
				ci.idci,ci.nombre as ciudad,  
				pr.idpro,pr.nit,pr.encargado,pr.url";
		$this->db->select($col);
		$this->db->from("proveedor pr");
		$this->db->join('persona p','pr.nit = p.ci','inner');
		$this->db->join('ciudad ci','ci.idci = p.idci','inner');
		$this->db->join('pais pa','pa.idpa = ci.idpa','inner');
		$this->db->order_by("p.nombre", "asc");
		$this->db->where("$atrib like '$val%'");
		$query=$this->db->get();
		return $query->result();
	}
	function get_search($col,$val){// en uso:  cliente
		$cols="p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
				pa.idpa,pa.nombre as pais,
				ci.idci,ci.nombre as ciudad, 
				pr.idpro,pr.nit,pr.encargado,pr.url";
		$this->db->select($cols);
		$this->db->from("proveedor pr");
		$this->db->join('persona p','pr.nit = p.ci','inner');
		$this->db->join('ciudad ci','ci.idci = p.idci','inner');
		$this->db->join('pais pa','pa.idpa = ci.idpa','inner');
		$this->db->order_by("p.nombre", "asc");
		if($col=="pr.nit" || $col=="p.telefono"){ $this->db->where("$col like '$val%'");}
		if($col=="pr.encargado" || $col=="p.direccion" || $col=="p.nombre"){ $this->db->where("$col like '%$val%'");}
		$this->db->order_by("p.nombre", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($nit,$encargado,$url){
		$datos=array(
			'nit' => $nit,
			'encargado' => $encargado,
			'url' => $url
		);
		if($this->db->insert('proveedor',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$nit,$encargado,$url){
		$datos=array(
			'nit' => $nit,
			'encargado' => $encargado,
			'url' => $url
		);
		if($this->db->update('proveedor',$datos,array('idpro' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('proveedor',['idpro' => $id])){
			return true;
		}else{
			return false;
		}
	}
}

/* End of file m_historial_insumo.php */
/* Location: ./application/models/m_historial_insumo.php*/