<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pedidos_ventas extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('login');
		$this->session->set_userdata('ida','',true);
		$this->session->set_userdata('tipo','',true);
	}
	public function index(){
		if(!empty($this->session_id)){
			$privilegio=$privilegio=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			if($privilegio[0]->mo=="1"){
				if(!isset($_GET['p'])){
					if($privilegio[0]->mo1r==1){
						$listado['pestania']=1;
					}else{
						if($privilegio[0]->mo2r==1){ 
							$listado['pestania']=2;
						}else{
							if($privilegio[0]->mo3r==1){ 
								$listado['pestania']=3;
							}else{
								$listado['pestania']=0;
							}
						}
					}
				}else{
					$listado['pestania']=$_GET['p'];
				}
				$listado['privilegio']=$privilegio;
				$this->load->view('v_pedidos_ventas',$listado);
			}else{
				$this->val->redireccion($privilegio);
			}
		}else{
			redirect(base_url().'login/input',301);
		}
	}

/*------- MANEJO DE PEDIDOS -------*/
	public function search_pedido(){
		if(!empty($this->session_id)){
			$listado['clientes']=$this->M_cliente->get_all();
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('pedidos_ventas/pedidos/search',$listado);
		}else{
			echo "logout";
		}
	}

	public function view_pedido(){
		if(!empty($this->session_id)){
			$atrib=""; $val="";
			if(isset($_POST['num']) && isset($_POST['nom']) && isset($_POST['cli'])){
				if($_POST['num']!=""){
					$atrib='p.numero';$val=$_POST['num'];
				}else{
					if($_POST['cli']!=""){
						$atrib='cl.idcl';$val=$_POST['cli'];
					}else{
						if($_POST['nom']!=""){
							$atrib='p.nombre';$val=$_POST['nom'];
						}
					}
				}
			}
			$listado['pedidos']=$this->M_pedido->get_search($atrib,$val,"");
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('pedidos_ventas/pedidos/view',$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Nuevo ---*/
   	public function new_pedido(){
		if(!empty($this->session_id)){
			$listado['productos']=$this->M_producto->get_all();
			$listado['clientes']=$this->M_cliente->get_all();
			//$listado['productos']=$this->M_categoria_pieza->get_categoria_color_producto(1);
			$max=$this->M_pedido->max('numero');
			$listado['nro_pedido']=$max[0]->max+1;
			$this->load->view('pedidos_ventas/pedidos/3-nuevo/form',$listado);
		}else{
			echo "logout";
		}
	}
	public function refresh_nro_orden(){
		if(!empty($this->session_id)){
			$orden=$this->M_pedido->max('numero');
			echo $orden[0]->max+1;
		}else{
			echo "logout";
		}
	}
	/*public function modal_productos(){
		if(!empty($this->session_id)){
			if(isset($_POST['idcl'])){
				$idcl=$_POST['idcl'];
				if($this->val->entero($idcl,0,10)){
					$listado['productos']=$this->M_producto_cliente->get_productos_clientes('pc.idcl',$idcl,'p.cod','asc','p.cod');
					$listado['idcl']=$_POST['idcl'];
					$this->load->view('pedidos_ventas/pedidos/3-nuevo/modal/add_producto',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}	
	}*/

	public function add_row_producto(){
		if(!empty($this->session_id)){
			if(isset($_POST['idcl']) && isset($_POST['rows'])){
				$idcl=$_POST['idcl'];
				$rows=($_POST['rows']*1)+1;
				$listado['productos']=$this->M_producto_cliente->get_productos_clientes('pc.idcl',$idcl,'p.cod','asc','p.cod');
				$listado['idcl']=$idcl;
				$listado['rows']=$rows;
				$this->load->view('pedidos_ventas/pedidos/3-nuevo/complementos/row_producto',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function add_categorias(){
		if(!empty($this->session_id)){
			if(isset($_POST['idcl']) && isset($_POST['rows']) && isset($_POST['codigo'])){
				$idcl=$_POST['idcl'];
				$rows=$_POST['rows'];
				$codigo=trim($_POST['codigo']);
				if($this->val->entero($idcl,0,10) && $this->val->entero($rows,0,10) && $this->val->strSpace($codigo,0,20)){
					$productos=$this->M_producto->get_search("codigo",$codigo);
					$cliente=$this->M_cliente->get_row("idcl",$idcl);
					if(!empty($productos) && !empty($cliente)){
						$listado['productos']=$productos;
						$listado['idcl']=$idcl;
						$listado['rows']=$rows;
						$this->load->view('pedidos_ventas/pedidos/3-nuevo/categorias/categoria',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
				/*
				$listado['idcl']=$idcl;
				$listado['rows']=$rows;*/
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function calcular_fila(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpim']) && isset($_POST['idcl']) && isset($_POST['can'])){
				$idpim=$_POST['idpim'];
				$idcl=$_POST['idcl'];
				$can=$_POST['can'];
				$costo_venta=$this->M_producto_cliente->get_row_2n('idpim',$idpim,'idcl',$idcl);
				if(!empty($costo_venta)){
					echo number_format($costo_venta[0]->costo_unitario,1,'.',',')."|".$can."|".number_format(($costo_venta[0]->costo_unitario*$can),1,'.','');
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function calcular_costo_tiempo(){
		if(!empty($this->session_id)){
			if(isset($_POST['idcl']) && isset($_POST['des']) && isset($_POST['fini']) && isset($_POST['productos'])){
				$idcl=trim($_POST['idcl']);
				$des=trim($_POST['des']);
				$fini=trim($_POST['fini']);
				$productos=trim($_POST['productos']);
				if($this->val->entero($idcl,0,10) && $this->val->decimal($des,7,1) && $des>=0 && $this->val->fecha($fini)){
					$vpro=explode("[|]", $productos);
					if(count($vpro)>0){
						$cost=0;
						$tipo_final=0;
						for($i=0; $i<count($vpro); $i++){
							$tiempo=0;
							$v=explode("|", $vpro[$i]);
							if($v[0]!="" && $this->val->entero($v[0],0,10)){
								$pc=$this->M_producto_cliente->get_row_2n('idpim',$v[0],'idcl',$idcl);
								if(!empty($pc)){
									$cost+=($pc[0]->costo_unitario*$v[1]);
								}
								$cp=$this->M_categoria_producto->get($v[0]);	
								$pieza=$this->M_categoria_pieza->get($cp[0]->idcp);
								$horas=$this->M_categoria_pieza->get_proceso_piezas($pieza[0]->idp);
								for ($m=0; $m < count($horas) ; $m++) { $proceso_pieza=$horas[$m];
									$tiempo+=$proceso_pieza->tiempo_estimado;
								}
								$horas=$this->M_producto_proceso->get_producto_proceso($pieza[0]->idp);
								for($m=0; $m<count($horas);$m++){ $pp=$horas[$m];
									$tiempo+=$pp->tiempo_estimado;
								}
								$tiempo=$tiempo*$v[1];
								$tipo_final+=$tiempo;
							}
						}
						
						$t=explode(":", $this->lib->hms($tipo_final));
						$hora="";
						if($t[0]!=0 & $t[0]!=''){$hora.=$t[0].'h ';}
						if($t[1]!=0 & $t[1]!=''){$hora.=$t[1].'m ';}
						if($t[2]!=0 & $t[2]!=''){$hora.=$t[2].'s';}
						//calculando tiempo de entrega
						$feriados=$this->M_feriado->get_all();
						$carga_horaria=$this->M_tipo_contrato->get_fecha_principal();
						$ffin=$this->lib->fecha_de_entrega_pedido($fini,$tipo_final,$feriados,$carga_horaria);
						//$ffin="";
						///echo $cost."|".$cost."|".$des."|".($cost-$des)."|".$hora."|".$tipo_final."|",$fini."|",$ffin."|",$ffin;
						echo $cost."|".$cost."|".$des."|".($cost-$des)."|".$hora."|".$tipo_final."|".$fini."|",$ffin."|".$ffin;
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function save_pedido(){
		if(!empty($this->session_id)){
			if(isset($_POST['productos']) && isset($_POST['nro']) && isset($_POST['nom']) && isset($_POST['cli']) && isset($_POST['fec']) && isset($_POST['obs'])){
				$nro=trim($_POST['nro']);
				$nom=trim($_POST['nom']);
				$idcl=trim($_POST['cli']);
				$fec=trim($_POST['fec']);
				$obs=trim($_POST['obs']);
				$productos=trim($_POST['productos']);
				if($this->val->entero($nro,0,10) && $this->val->strSpace($nom,3,150) && $this->val->entero($idcl,0,10) && $this->val->fecha($fec) && $productos!=""){
					//validadndo los productos
					$vpro=explode("[|]", $productos);
					if(count($vpro)>0){
						$control=true;
						for($i=0;$i<count($vpro);$i++){
							$v=explode("|", $vpro[$i]);
							if(count($v)==2){
								if(!$this->val->entero($v[0],0,10) || !$this->val->entero($v[1],0,7)){
									$control=false;
								}
							}else{
								$control=false;
							}
						}
						if($control){
							$aux=$this->M_pedido->max('idpe');
							$idpe=($aux[0]->max)+1;
							$control=$this->M_pedido->get_row('numero',$nro);
							if(empty($control)){// si el numero de pedido no esta registrado
								$cliente=$this->M_cliente->get_row('idcl',$idcl);
								if(!empty($cliente)){
									if($this->M_pedido->insertar($idpe,$idcl,$nro,$nom,$obs)){
										$aux=$this->M_sub_pedido->max('idsp');
										$idsp=($aux[0]->max)+1;
										if($this->M_sub_pedido->insertar($idsp,$idpe,'1',"parte",$fec)){
											$vpro=explode("[|]", $productos);
											for($i=0;$i<count($vpro);$i++){
												$v=explode("|", $vpro[$i]);
												$aux=$this->M_detalle_pedido->max_id_2n('op','idsp',$idsp);
												$op=$aux[0]->max+1;
												if($this->M_detalle_pedido->insertar($idsp,$v[0],$v[1],$op)){}
											}
											echo "ok";
										}else{
											echo "error";
										}
									}else{
										echo "error";
									}
								}else{
									echo "fail";
								}
							}else{
								echo "numero_pedido_exist";
							}
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	public function config_imprimir_pedidos(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				$listado['pedidos']=$_POST['json'];
				$this->load->view('pedidos_ventas/pedidos/4-imprimir/config',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function imprimir_pedidos(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				if(isset($_POST['v1'])){ if($_POST['v1']!="ok"){ $listado['v1']="ok";} }
				if(isset($_POST['v2'])){ if($_POST['v2']!="ok"){ $listado['v2']="ok";} }
				if(isset($_POST['v3'])){ if($_POST['v3']!="ok"){ $listado['v3']="ok";} }
				if(isset($_POST['v4'])){ if($_POST['v4']!="ok"){ $listado['v4']="ok";} }
				if(isset($_POST['v5'])){ if($_POST['v5']!="ok"){ $listado['v5']="ok";} }
				if(isset($_POST['v6'])){ if($_POST['v6']!="ok"){ $listado['v6']="ok";} }
				if(isset($_POST['v7'])){ if($_POST['v7']!="ok"){ $listado['v7']="ok";} }
				if(isset($_POST['v8'])){ if($_POST['v8']!="ok"){ $listado['v8']="ok";} }
				if(isset($_POST['v9'])){ if($_POST['v9']!="ok"){ $listado['v9']="ok";} }
				if(isset($_POST['v10'])){ if($_POST['v10']!="ok"){ $listado['v10']="ok"; } }
				if(isset($_POST['v11'])){ if($_POST['v11']!="ok"){ $listado['v11']="ok"; } }
				if(isset($_POST['v12'])){ if($_POST['v12']!="ok"){ $listado['v12']="ok"; } }
				if(isset($_POST['v13'])){ if($_POST['v13']!="ok"){ $listado['v13']="ok"; } }
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=34;}
				$listado['pedidos']=$_POST['json'];
				$this->load->view('pedidos_ventas/pedidos/4-imprimir/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Pagos ---
   	public function pagos(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpe'])){
				$idpe=trim($_POST['idpe']);
				$pedido=$this->M_pedido->get_complet($idpe);
				if(!empty($pedido)){
					$listado['pedido']=$pedido;
					$listado['pagos']=$this->M_pago->get_row('idpe',$idpe);
					$this->load->view('pedidos_ventas/pedidos/pagos/view',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function save_pago(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpe']) && isset($_POST['fec']) && isset($_POST['pag']) && isset($_POST['obs'])){
				$idpe=$_POST['idpe'];
				$fec=$_POST['fec'];
				$pag=$_POST['pag'];
				$obs=$_POST['obs'];
				if($this->val->entero($idpe,0,10) && $this->val->fecha($fec) && $this->val->decimal($pag,7,1) && $pag>0 && $pag<=9999999.9 && $this->val->textarea($obs,0,500)){
					if($this->M_pago->insertar($idpe,$fec,$pag,$obs)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_pago(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpa']) && isset($_POST['fec']) && isset($_POST['pag']) && isset($_POST['obs'])){
				$idpa=$_POST['idpa'];
				$fec=$_POST['fec'];
				$pag=$_POST['pag'];
				$obs=$_POST['obs'];
				if($this->val->entero($idpa,0,10) && $this->val->fecha($fec) && $this->val->decimal($pag,7,1) && $pag>0 && $pag<=9999999.9 && $this->val->textarea($obs,0,500)){
					if($this->M_pago->modificar($idpa,$fec,$pag,$obs)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function drop_pago(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpa'])){
				$idpa=trim($_POST['idpa']);
				if($this->M_pago->eliminar($idpa)){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	--- End Pagos ---*/
   	/*--- Reportes ---*/
   	public function reporte_pedido(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpe'])){
				$idpe=trim($_POST['idpe']);
				/*$listado['pedido']=$this->M_pedido->get_pedido_cliente($idpe);
				$listado['detalle_pedidos']=$this->M_detalle_pedido->get_producto($idpe);
				$listado['pagos']=$this->M_pago->get_row('idpe',$idpe);*/
				if($this->val->entero($idpe,0,10)){
					$listado['materiales']=$this->M_sub_pedido->get_material_requerido($idpe);
					$listado['idpe']=$idpe;
					$this->load->view('pedidos_ventas/pedidos/5-reportes/accesorios_requeridos',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function productos_pedido(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpe'])){
				$idpe=trim($_POST['idpe']);
				if($this->val->entero($idpe,0,10)){
					$listado['productos']=$this->M_sub_pedido->get_productos($idpe);
					$listado['idpe']=$idpe;
					$this->load->view('pedidos_ventas/pedidos/5-reportes/productos',$listado);
				}else{
					echo "fail";
				}
				/*$listado['pedido']=$this->M_pedido->get_pedido_cliente($idpe);
				$listado['detalle_pedidos']=$this->M_detalle_pedido->get_producto($idpe);

				if(isset($_POST['v1'])){ if($_POST['v1']=="ok"){ $listado['v1']="ok";}}
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=34;}
				$this->load->view('pedidos_ventas/pedidos/5-reportes/productos_pedido',$listado);*/
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function materiales_pedido(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpe'])){
				$idpe=trim($_POST['idpe']);
				$listado['pedido']=$this->M_pedido->get_pedido_cliente($idpe);
				$listado['detalle_pedidos']=$this->M_detalle_pedido->get_producto($idpe);
				$listado['materiales_all']=$this->M_material->get_all_cantidad();
				if(isset($_POST['v1'])){ if($_POST['v1']=="ok"){ $listado['v1']="ok";}}
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=34;}
				$this->load->view('pedidos_ventas/pedidos/5-reportes/materiales_pedido',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function detalle_pedido(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpe'])){
				$idpe=trim($_POST['idpe']);
				$listado['pedido']=$this->M_pedido->get_all_complet($idpe);
				$listado['detalle_pedidos']=$this->M_detalle_pedido->get_row('idpe',$idpe);
				//$listado['productos']=$this->M_pedido->get_productos($idpe);
				$listado['pagos']=$this->M_pago->get_row('idpe',$idpe);
				$this->load->view('pedidos_ventas/pedidos/5-reportes/detalle_pedido',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function materiales_productos_pedido(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpe'])){
				$idpe=trim($_POST['idpe']);
				$listado['pedido']=$this->M_pedido->get_pedido_cliente($idpe);
				$listado['detalle_pedidos']=$this->M_detalle_pedido->get_producto($idpe);
				$listado['idpe']=$idpe;
				$this->load->view('pedidos_ventas/pedidos/5-reportes/materiales_productos_pedido',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	public function config_pedido(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpe'])){
				$idpe=trim($_POST['idpe']);
				if($this->val->entero($idpe,0,10)){
					$pedido=$this->M_pedido->get_search("p.idpe",$idpe,"");
					if(!empty($pedido)){
						$listado['clientes']=$this->M_cliente->get_all();
						$listado['pedido']=$pedido[0];
						$listado['sub_pedidos']=$this->M_sub_pedido->get_row("idpe",$idpe);
						$this->load->view('pedidos_ventas/pedidos/6-config/form',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_pedido(){
		if(!empty($this->session_id)){
			if(isset($_POST['productos']) && isset($_POST['idpe']) && isset($_POST['cli']) && isset($_POST['fped']) && isset($_POST['obs']) && isset($_POST['est']) && isset($_POST['cos']) && isset($_POST['des']) && isset($_POST['tie']) && isset($_POST['fini']) && isset($_POST['fest']) && isset($_POST['fent'])){
				$idpe=trim($_POST['idpe']);
				$idcl=trim($_POST['cli']);
				$fped=trim($_POST['fped']);
				$obs=trim($_POST['obs']);
				$est=trim($_POST['est'])*1;
				$cos=trim($_POST['cos'])*1;
				$des=trim($_POST['des'])*1;
				$tie=trim($_POST['tie'])*1;
				$fini=trim($_POST['fini']);
				$fest=trim($_POST['fest']);
				$fent=trim($_POST['fent']);
				$productos=trim($_POST['productos']);
				if($this->val->entero($idpe,0,10) && $this->val->entero($idcl,0,10) && $this->val->fecha($fped) && $this->val->fecha($fini) && $this->val->fecha($fest) && $this->val->fecha($fent)){
					if($this->M_pedido->modificar($idpe,$idcl,$fped,$tie,$fini,$fest,$fent,$est,$cos,$des,($cos-$des),$obs)){
						$vpro=explode("[|]", $productos);
						if($this->M_detalle_pedido->eliminar_row('idpe',$idpe)){
							for($i=0;$i<count($vpro);$i++){
								$v=explode("|", $vpro[$i]);
								$aux=$this->M_detalle_pedido->max_id($idpe,'op');
								$op=$aux[0]->max+1;
								if($this->M_detalle_pedido->insertar($idpe,$v[0],$v[1],$op)){}
							}
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function config_sub_pedido(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpe']) && isset($_POST['idsp'])){
				$idpe=trim($_POST['idpe']);
				$idsp=trim($_POST['idsp']);
				if($this->val->entero($idpe,0,10) && $this->val->entero($idsp,0,10)){
					$sub_pedido=$this->M_sub_pedido->get_row_2n("idpe",$idpe,"idsp",$idsp);
					$pedido=$this->M_pedido->get_search("p.idpe",$idpe,"");
					if(!empty($sub_pedido) && !empty($pedido)){
						$listado['sub_pedido']=$sub_pedido[0];
						$listado['sub_pedidos']=$this->M_sub_pedido->get_row("idpe",$idpe);
						$listado['pedido']=$pedido[0];
						$listado['clientes']=$this->M_cliente->get_all();
						$listado['productos_pedido']=$this->M_detalle_pedido->get_producto($sub_pedido[0]->idsp,"","","p.cod","asc","");
						$listado['productos']=$this->M_producto_cliente->get_productos_clientes('pc.idcl',$pedido[0]->idcl,'p.cod','asc','p.cod');
						$this->load->view('pedidos_ventas/pedidos/6-config/partes/view',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_sub_pedido(){
		if(!empty($this->session_id)){
			if(isset($_POST['productos']) && isset($_POST['fec']) && isset($_POST['obs']) && isset($_POST['idsp'])){
				$idsp=trim($_POST['idsp']);
				$fec=trim($_POST['fec']);
				$obs=trim($_POST['obs']);
				$productos=trim($_POST['productos']);
				if($this->val->entero($idsp,0,10) && $this->val->fecha($fec) && $productos!=""){
					$vpro=explode("[|]", $productos);
					if(count($vpro)>0){
						$control=true;
						for($i=0;$i<count($vpro);$i++){
							$v=explode("|", $vpro[$i]);
							if(count($v)==2){
								if(!$this->val->entero($v[0],0,10) || !$this->val->entero($v[1],0,7)){
									$control=false;
								}
							}else{
								$control=false;
							}
						}
						if($control){
							if($this->M_sub_pedido->modificar($idsp,$fec,$obs)){
								if($this->M_detalle_pedido->eliminar_row('idsp',$idsp)){
									$vpro=explode("[|]", $productos);
									for($i=0;$i<count($vpro);$i++){
										$v=explode("|", $vpro[$i]);
										$aux=$this->M_detalle_pedido->max_id_2n('op','idsp',$idsp);
										$op=$aux[0]->max+1;
										if($this->M_detalle_pedido->insertar($idsp,$v[0],$v[1],$op)){}
									}
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "error";
							}
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	public function confirmar_pedido(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpe'])){
				$idpe=$_POST['idpe'];
				$pedido=$this->M_pedido->get($idpe);
				if(!empty($pedido)){
					$desc="Se eliminara definitivamente el pedido";
					if($pedido[0]->estado==1){
						$desc=" Imposible Eliminar el producto, pues el pedido se encuentra en producción:";
						$listado['open_control']="false";
						$listado['control']="";
					}
				
				$listado['titulo']="eliminar el pedido <b>Nº ".$pedido[0]->numero."</b> definitivamente";
				$listado['desc']=$desc;
				$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function drop_pedido(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpe']) && isset($_POST['u']) && isset($_POST['p'])){
				$idpe=$_POST['idpe'];
				$u=$_POST['u'];
				$p=$_POST['p'];
				if($u==$this->session->userdata("login")){
					$usuario=$this->M_empleado->validate($u,$p);
					if(!empty($usuario)){
						$pedido=$this->M_pedido->get($idpe);
						if(!empty($pedido)){
							if($this->M_pedido->eliminar($idpe)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "validate";
					}
				}else{
					echo "validate";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE PEDIDOS -------*/
/*------- MANEJO DE VENTAS -------*/
	public function search_venta(){
		if(!empty($this->session_id)){
			$listado['clientes']=$this->M_cliente->get_all();
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('pedidos_ventas/ventas/search',$listado);
		}else{
			echo "logout";
		}
	}
	public function view_venta(){
		if(!empty($this->session_id)){
			$col="";$val="";
			if (isset($_POST['nit']) && isset($_POST['raz']) && isset($_POST['fe1']) && isset($_POST['fe2'])) {
				if($_POST['nit']!=""){
					$col="pe.ci";$val=$_POST['nit'];
				}else{
					if($_POST['raz']!=""){
						$col="cl.idcl";$val=$_POST['raz'];
					}else{
						if($_POST['fe1']!=""){
							if($_POST['fe2']!=""){
								$col="fecha";$val=$_POST['fe1']."|".$_POST['fe2'];
							}else{
								$col="v.fecha";$val=$_POST['fe1'];
							}
						}else{
							if($_POST['fe2']!=""){
								$col="v.fecha";$val=$_POST['fe2'];
							}
						}
					}
				}
			}
			$listado['ventas']=$this->M_venta->get_pedido($col,$val);
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('pedidos_ventas/ventas/view',$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Nuevo ---*/
   	public function new_venta(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpe'])){
				$pedido=$this->M_pedido->get($_POST['idpe']);
				if(!empty($pedido)){
					$listado['idpe']=$_POST['idpe'];
					$listado['pedido']=$pedido;
					$listado['detalle_pedido']=$this->M_detalle_pedido->get_producto($_POST['idpe']);
				}
			}
			$listado['pedidos']=$this->M_pedido->get_all();
			$listado['ventas']=$this->M_venta->get_all();
			$this->load->view('pedidos_ventas/ventas/3-nuevo/new',$listado);
		}else{
			echo "logout";
		}
	}
   	public function save_venta(){
		if(!empty($this->session_id)){
			if(isset($_POST['productos']) && isset($_POST['idpe']) && isset($_POST['cos_vs']) && isset($_POST['cos_v']) && isset($_POST['des_v']) && isset($_POST['fecha']) && isset($_POST['obs'])){
				$productos=$_POST['productos'];
				$idpe=$_POST['idpe'];
				$cos_vs=$_POST['cos_vs'];
				$cos_v=$_POST['cos_v'];
				$des_v=$_POST['des_v'];
				$fecha=$_POST['fecha'];
				$obs=$_POST['obs'];
				if($productos!="" && $this->val->entero($idpe,0,10) && $this->val->decimal($cos_vs,7,1) && $cos_vs>=0 && $this->val->decimal($cos_v,7,1) && $cos_v>=0 && $cos_v<=9999999.9 && $this->val->decimal($des_v,7,1) && $des_v>=0 && $des_v<=9999999.9 && $this->val->fecha($fecha) && $this->val->textarea($obs,0,800)){
					if($this->M_venta->insertar($idpe, $this->session->userdata('id'), $cos_vs,$cos_v, $des_v,($cos_v-$des_v), $fecha, $obs)){
						//actualizandodetalle de ventas
						$v_prod=explode("[|]", $productos);
						for($i=0; $i < count($v_prod); $i++){ 
							$prod=explode("|", $v_prod[$i]);
							if($this->M_detalle_pedido->modificar_vendido($prod[0],$prod[1],$prod[2])){ }
						}
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	public function config_imprimir_ventas(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				$listado['ventas']=$_POST['json'];
				$this->load->view('pedidos_ventas/ventas/4-imprimir/config',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function imprimir_ventas(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				if(isset($_POST['v1'])){ if($_POST['v1']!="ok"){ $listado['v1']="ok";} }
				if(isset($_POST['v2'])){ if($_POST['v2']!="ok"){ $listado['v2']="ok";} }
				if(isset($_POST['v3'])){ if($_POST['v3']!="ok"){ $listado['v3']="ok";} }
				if(isset($_POST['v4'])){ if($_POST['v4']!="ok"){ $listado['v4']="ok";} }
				if(isset($_POST['v5'])){ if($_POST['v5']!="ok"){ $listado['v5']="ok";} }
				if(isset($_POST['v6'])){ if($_POST['v6']!="ok"){ $listado['v6']="ok";} }
				if(isset($_POST['v7'])){ if($_POST['v7']!="ok"){ $listado['v7']="ok";} }
				if(isset($_POST['v8'])){ if($_POST['v8']!="ok"){ $listado['v8']="ok";} }
				if(isset($_POST['v9'])){ if($_POST['v9']!="ok"){ $listado['v9']="ok";} }
				if(isset($_POST['v10'])){ if($_POST['v10']!="ok"){ $listado['v10']="ok"; } }
				if(isset($_POST['v11'])){ if($_POST['v11']!="ok"){ $listado['v11']="ok"; } }
				if(isset($_POST['v12'])){ if($_POST['v12']!="ok"){ $listado['v12']="ok"; } }
				if(isset($_POST['v13'])){ if($_POST['v13']!="ok"){ $listado['v13']="ok"; } }
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=34;}
				$listado['ventas']=$_POST['json'];
				$this->load->view('pedidos_ventas/ventas/4-imprimir/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	public function detalle_venta(){
		if(!empty($this->session_id)){
			if(isset($_POST['idv'])){
				$venta=$this->M_venta->get_pedido('v.idv',$_POST['idv']);
				if(!empty($venta)){
					$listado['venta']=$venta;
					$listado['detalle_pedidos']=$this->M_detalle_pedido->get_row('idpe',$venta[0]->idpe);
					$listado['pagos']=$this->M_pago->get_row('idpe',$venta[0]->idpe);
					$this->load->view('pedidos_ventas/ventas/5-reportes/view',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function productos_venta(){
		if(!empty($this->session_id)){
			if(isset($_POST['idv'])){
				$venta=$listado['ventas']=$this->M_venta->get_pedido('v.idv',$_POST['idv']);
				if(!empty($venta)){
					$listado['venta']=$venta;
					$listado['detalle_pedidos']=$this->M_detalle_pedido->get_producto($venta[0]->idpe);
					if(isset($_POST['v1'])){ if($_POST['v1']=="ok"){ $listado['v1']="ok";}}
					if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=34;}
					$this->load->view('pedidos_ventas/ventas/5-reportes/productos_venta',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	public function config_venta(){
		if(!empty($this->session_id)){
			if(isset($_POST['idv'])){
				$venta=$this->M_venta->get_pedido('v.idv',$_POST['idv']);
				if(!empty($venta)){
					$listado['venta']=$venta;
					$listado['detalle_pedido']=$this->M_detalle_pedido->get_producto($venta[0]->idpe);
					$listado['pagos']=$this->M_pago->get_row('idpe',$venta[0]->idpe);
					$this->load->view('pedidos_ventas/ventas/6-config/view',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_venta(){
		if(!empty($this->session_id)){
			if(isset($_POST['productos']) && isset($_POST['idv']) && isset($_POST['cos_vs']) && isset($_POST['cos_v']) && isset($_POST['des_v']) && isset($_POST['fecha']) && isset($_POST['obs'])){
				$productos=$_POST['productos'];
				$idv=$_POST['idv'];
				$cos_vs=$_POST['cos_vs'];
				$cos_v=$_POST['cos_v'];
				$des_v=$_POST['des_v'];
				$fecha=$_POST['fecha'];
				$obs=$_POST['obs'];
				if($productos!="" && $this->val->entero($idv,0,10) && $this->val->decimal($cos_vs,7,1) && $cos_vs>=0 && $this->val->decimal($cos_v,7,1) && $cos_v>=0 && $cos_v<=9999999.9 && $this->val->decimal($des_v,7,1) && $des_v>=0 && $des_v<=9999999.9 && $this->val->fecha($fecha) && $this->val->textarea($obs,0,800)){
					if($this->M_venta->modificar($idv, $this->session->userdata('id'), $cos_vs,$cos_v, $des_v,($cos_v-$des_v), $fecha, $obs)){
						//actualizandodetalle de ventas
						$v_prod=explode("[|]", $productos);
						for($i=0; $i < count($v_prod); $i++){ 
							$prod=explode("|", $v_prod[$i]);
							if($this->M_detalle_pedido->modificar_vendido($prod[0],$prod[1],$prod[2])){ }
						}
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	public function confirmar_venta(){
		if(!empty($this->session_id)){
			if(isset($_POST['idv'])){
				$idv=$_POST['idv'];
				$venta=$this->M_venta->get_pedido("v.idv",$idv);
				if(!empty($venta)){
					$listado['titulo']="eliminar la venta en fecha ".$this->lib->format_date($venta[0]->fecha,'d ml Y')." del pedido <b>Nº ".$venta[0]->numero."</b> definitivamente.";
					$listado['desc']="Se eliminara definitivamente la venta";
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function drop_venta(){
		if(!empty($this->session_id)){
			if(isset($_POST['idv']) && isset($_POST['u']) && isset($_POST['p'])){
				$idv=$_POST['idv'];
				$u=$_POST['u'];
				$p=$_POST['p'];
				if($u==$this->session->userdata("login")){
					$usuario=$this->M_empleado->validate($u,$p);
					if(!empty($usuario)){
						$venta=$this->M_venta->get($idv);
						if(!empty($venta)){
							if($this->M_venta->eliminar($idv)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "validate";
					}
				}else{
					echo "validate";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE VENTAS -------*/
/*------- MANEJO DE COMPRAS -------*/
	public function search_compra(){
		if(!empty($this->session_id)){
			$listado['proveedores']=$this->M_proveedor->get_all();
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('pedidos_ventas/compras/search',$listado);
		}else{
			echo "logout";
		}
	}
	public function view_compra(){
		if(!empty($this->session_id)){
			$col="";$val="";
			if(isset($_POST['nit']) && isset($_POST['raz']) && isset($_POST['fe1']) && isset($_POST['fe2']) && isset($_POST['am'])){
				if($_POST['nit']!=""){
					$col="p.ci";$val=$_POST['nit'];
				}else{
					if($_POST['raz']!=""){
						$col="pr.idpro";$val=$_POST['raz'];
					}else{
						if($_POST['fe1']!=""){
							if($_POST['fe2']!=""){
								$col="fecha";$val=$_POST['fe1']."|".$_POST['fe2'];
							}else{
								$col="g.fecha";$val=$_POST['fe1'];
							}
						}else{
							if($_POST['fe2']!=""){
								$col="g.fecha";$val=$_POST['fe2'];
							}else{
								if($_POST['am']!=""){
									$col="anio_mes";$val=$_POST['am'];
								}
							}
						}
					}
				}
			}
			$listado['gastos']=$this->M_detalle_gasto->view_gastos($col,$val);
			$listado['cuentas']=$this->M_plan_cuenta->get_row('gasto','1');
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('pedidos_ventas/compras/view',$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Nuevo ---*/
	public function con_orden(){
		if(!empty($this->session_id)){
			$listado['proveedores']=$this->M_proveedor->get_all();
			//hallamos numero de orden
			$orden=$this->M_gasto->max_id('orden_gasto');
			$listado['orden']=$orden[0]->max+1;
			$this->load->view('pedidos_ventas/compras/3-nuevo/con_orden',$listado);
		}else{
			echo "logout";
		}
	}

	public function refresh_orden_gasto(){
		if(!empty($this->session_id)){
			$orden=$this->M_gasto->max_id('orden_gasto');
			echo $orden[0]->max+1;
		}else{
			echo "logout";
		}
	}
	public function add_row_con_orden(){
		if(!empty($this->session_id)){
			if(isset($_POST['row']) && isset($_POST['cat'])){
				$listado['row']=$_POST['row']+1;
				if($_POST['cat']>0 && $_POST['cat']<=4){
					$listado['cat']=$_POST['cat'];
					if($_POST['cat']==1){
						$listado['materiales_proveedores']=$this->M_material->material_proveedor();
					}
					if($_POST['cat']==2){
						$listado['materiales_proveedores']=$this->M_material_adicional->material_proveedor();
					}
					if($_POST['cat']==3){
						$listado['materiales_proveedores']=$this->M_activo_fijo->material_proveedor();
					}
					if($_POST['cat']==4){
						$listado['materiales_proveedores']=$this->M_material_varios->material_proveedor();
					}
					$this->load->view('pedidos_ventas/compras/3-nuevo/con_orden/row',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function select_materiales(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpro']) && isset($_POST['cat'])){
				$idpro=$_POST['idpro'];
				$cat=$_POST['cat'];
				if($this->val->entero($idpro,0,10) && $cat>0 && $cat<=4){
					$listado['idpro']=$idpro;
					switch ($cat){
						case '1': $materiales=$this->M_material_proveedor->get_material($idpro,'material'); break;
						case '2': $materiales=$this->M_material_proveedor->get_material($idpro,'material_adicional'); break;
						case '3': $materiales=$this->M_material_proveedor->get_material($idpro,'activo_fijo'); break;
						case '4': $materiales=$this->M_material_proveedor->get_material($idpro,'material_vario'); break;
					}
					if(!empty($materiales)){
						$options="<option value=''>Seleccionar...</option>";
						for($i=0; $i < count($materiales) ; $i++){ $material=$materiales[$i];
							$options.="<option value='".$material->idmp."'>".$material->nombre."</option>";
						}
						echo $options;
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function monto_material(){
		if(!empty($this->session_id)){
			if(isset($_POST['idmp']) && isset($_POST['can']) && isset($_POST['cat'])){
				$idmp=$_POST['idmp'];
				$can=$_POST['can']*1;
				$cat=$_POST['cat'];
				$resultado="";
				if($this->val->entero($idmp,0,10) && $can>=0 && $cat>0 && $cat<=4){
					switch ($cat){
						case '1': 
							$materiales=$this->M_material_proveedor->get_material_row('mp.idmp',$idmp,'material'); 
							if(!empty($materiales)){
								$importe=number_format(($materiales[0]->costo_unitario*$can),2,'.','');
								$resultado=$materiales[0]->costo_unitario."|".$importe."|".$importe."|".$materiales[0]->abreviatura;
							}
							break;
						case '3': 
							$materiales=$this->M_material_proveedor->get_material_row('mp.idmp',$idmp,'activo_fijo');
							if(!empty($materiales)){
								$importe=number_format(($materiales[0]->costo_unitario*$can),2,'.','');
								$resultado=$materiales[0]->costo_unitario."|".$importe."|".$importe."|".$materiales[0]->abreviatura;
							}
							break;
					}
					echo $resultado;
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function save_con_orden(){
		if(!empty($this->session_id)){
			if(isset($_POST['mat']) && isset($_POST['nro']) && isset($_POST['tg']) && isset($_POST['fecha']) && isset($_POST['obs'])){
				$mat=trim($_POST['mat']);
				$nro=trim($_POST['nro']);
				$tig=trim($_POST['tg']);
				$fec=trim($_POST['fecha']);
				$obs=trim($_POST['obs']);
				$articulos=explode("[|]", $mat);
				if($this->val->entero($tig,0,1) && $tig>0 && $tig<=3 && $this->val->fecha($fec) && $this->val->textarea($obs,0,700) && count($articulos)>0){
					$control=$this->M_gasto->get_row("orden_gasto",$nro);
					if(empty($control)){
						$aux=$this->M_gasto->max_id("idga");
						$idga=$aux[0]->max+1;
						if(count($articulos)>0){
							if($this->M_gasto->insertar($idga,$this->session->userdata('id'),$nro,$fec,$tig,$obs)){
								for ($i=0; $i < count($articulos) ; $i++) { 
									$material=explode("|", $articulos[$i]);
									if($this->M_detalle_gasto->insertar_con_orden($material[1],$idga,$material[0],$material[2],$material[3],$material[3])){
									}
								}
								echo "ok";
							}else{
								echo "error";
							}
						}
						
					}else{//el número de orden ya existe
						echo "numero_orden_exist";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}

	public function sin_orden(){
		if(!empty($this->session_id)){
			$this->load->view('pedidos_ventas/compras/3-nuevo/sin_orden');
		}else{
			echo "logout";
		}
	}
	public function add_row_sin_orden(){
		if(!empty($this->session_id)){
			if(isset($_POST['row']) && isset($_POST['cat'])){
				$listado['row']=$_POST['row']+1;
				if($_POST['cat']>0 && $_POST['cat']<=4){
					$listado['cat']=$_POST['cat'];
					if($_POST['cat']==1){
						$listado['materiales_proveedores']=$this->M_material->material_proveedor();
					}
					if($_POST['cat']==2){
						$listado['materiales_proveedores']=$this->M_material_adicional->material_proveedor();
					}
					if($_POST['cat']==3){
						$listado['materiales_proveedores']=$this->M_activo_fijo->material_proveedor();
					}
					if($_POST['cat']==4){
						$listado['materiales_proveedores']=$this->M_material_varios->material_proveedor();
					}
					$this->load->view('pedidos_ventas/compras/3-nuevo/sin_orden/row',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function save_sin_orden(){
		if(!empty($this->session_id)){
			if(isset($_POST['mat'])){
				$mat=trim($_POST['mat']);
				$articulos=explode("[|]", $mat);
					$aux=$this->M_gasto->max_id("idga");
					$idga=$aux[0]->max+1;
					if(count($articulos)>0){
						for ($i=0; $i < count($articulos) ; $i++) { 
							$material=explode("|", $articulos[$i]);
							$aux=$this->M_gasto->max_id("idga");
							$idga=$aux[0]->max+1;
							if($this->M_gasto->insertar($idga,$this->session->userdata('id'),"",$material[1],0,"")){
								if($this->M_detalle_gasto->insertar_sin_orden($material[2],$idga,$material[3],$material[0],$material[4],$material[5],$material[5])){
								}
							}
							
						}
						echo "ok";
					}else{
						echo "fail";
					}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	public function config_imprimir_compras(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				$listado['compras']=$_POST['json'];
				$this->load->view('pedidos_ventas/compras/4-imprimir/config',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function imprimir_compras(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				if(isset($_POST['v1'])){ if($_POST['v1']!="ok"){ $listado['v1']="ok";} }
				if(isset($_POST['v2'])){ if($_POST['v2']!="ok"){ $listado['v2']="ok";} }
				if(isset($_POST['v3'])){ if($_POST['v3']!="ok"){ $listado['v3']="ok";} }
				if(isset($_POST['v4'])){ if($_POST['v4']!="ok"){ $listado['v4']="ok";} }
				if(isset($_POST['v5'])){ if($_POST['v5']!="ok"){ $listado['v5']="ok";} }
				if(isset($_POST['v6'])){ if($_POST['v6']!="ok"){ $listado['v6']="ok";} }
				if(isset($_POST['v7'])){ if($_POST['v7']!="ok"){ $listado['v7']="ok";} }
				if(isset($_POST['v8'])){ if($_POST['v8']!="ok"){ $listado['v8']="ok";} }
				if(isset($_POST['v9'])){ if($_POST['v9']!="ok"){ $listado['v9']="ok";} }
				if(isset($_POST['v10'])){ if($_POST['v10']!="ok"){ $listado['v10']="ok"; } }
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=34;}
				$listado['compras']=$_POST['json'];
				$this->load->view('pedidos_ventas/compras/4-imprimir/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	public function detalle_compra(){
		if(!empty($this->session_id)){
			if(isset($_POST['idga'])){
				$compra=$this->M_detalle_gasto->view_gastos('g.idga',$_POST['idga']);
				if(!empty($compra)){
					$listado['compra']=$compra;
					$this->load->view('pedidos_ventas/compras/5-reportes/view',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
	public function config_con_orden(){
		if(!empty($this->session_id)){
			if(isset($_POST['idga'])){
				$compra=$this->M_detalle_gasto->view_gastos('g.idga',$_POST['idga']);
				if(!empty($compra)){
					$listado['compras']=$compra;
					$this->load->view('pedidos_ventas/compras/6-config/con_orden',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_con_orden(){
		if(!empty($this->session_id)){
			if(isset($_POST['idga']) && isset($_POST['mat']) && isset($_POST['nro']) && isset($_POST['tg']) && isset($_POST['fecha']) && isset($_POST['obs'])){
				$idga=trim($_POST['idga']);
				$mat=trim($_POST['mat']);
				$nro=trim($_POST['nro']);
				$tig=trim($_POST['tg']);
				$fec=trim($_POST['fecha']);
				$obs=trim($_POST['obs']);
				$articulos=explode("[|]", $mat);
				if($this->val->entero($idga,0,1) && $this->val->entero($tig,0,1) && $tig>0 && $tig<=3 && $this->val->fecha($fec) && $this->val->textarea($obs,0,700) && count($articulos)>0){
					if(count($articulos)>0){
						if($this->M_gasto->modificar($idga,$this->session->userdata('id'),$fec,$tig,$obs)){
							if($this->M_detalle_gasto->eliminar_row('idga',$idga)){
								for($i=0; $i < count($articulos) ; $i++){ 
									$material=explode("|", $articulos[$i]);
									if($this->M_detalle_gasto->insertar_con_orden($material[1],$idga,$material[0],$material[2],$material[3],$material[3])){
									}
								}
								echo "ok";
							}
						}else{
							echo "error";
						}
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function config_sin_orden(){
		if(!empty($this->session_id)){
			if(isset($_POST['idga'])){
				$compra=$this->M_detalle_gasto->view_gastos('g.idga',$_POST['idga']);
				if(!empty($compra)){
					$listado['compras']=$compra;
					$this->load->view('pedidos_ventas/compras/6-config/sin_orden',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_sin_orden(){
		if(!empty($this->session_id)){
			if(isset($_POST['mat']) && isset($_POST['idga']) && isset($_POST['iddga'])){
				$mat=trim($_POST['mat']);
				$idga=trim($_POST['idga']);
				$iddga=trim($_POST['iddga']);
				$articulos=explode("[|]", $mat);
				if(count($articulos)>0 && $this->val->entero($idga,0,10) && $this->val->entero($iddga,0,10)){
					$material=explode("|", $articulos[0]);
						if($this->M_gasto->modificar($idga,$this->session->userdata('id'),$material[1],0,"")){
							if($this->M_detalle_gasto->modificar_sin_orden($iddga,$material[2],$idga,$material[3],$material[0],$material[4],$material[5],$material[5])){
							}
							echo "ok";
						}else{
							echo "error";
						}
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	public function confirmar_compra(){
		if(!empty($this->session_id)){
			if(isset($_POST['iddga'])){
				$iddga=$_POST['iddga'];
				$control=$this->M_detalle_gasto->get($iddga);
				if(!empty($control)){
					$idga=$control[0]->idga;
					$gasto=$this->M_gasto->get($idga);
					if(!empty($gasto)){
						$listado['titulo']="eliminar el registro en fecha ".$this->lib->format_date($gasto[0]->fecha,'d ml Y');
						$listado['desc']="Se eliminara definitivamente el registro.";
						$this->load->view('estructura/form_eliminar',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function drop_compra(){
		if(!empty($this->session_id)){
			if(isset($_POST['iddga']) && isset($_POST['u']) && isset($_POST['p'])){
				$iddga=$_POST['iddga'];
				$u=$_POST['u'];
				$p=$_POST['p'];
				if($u==$this->session->userdata("login")){
					$usuario=$this->M_empleado->validate($u,$p);
					if(!empty($usuario)){
						$control=$this->M_detalle_gasto->get($iddga);
						if(!empty($control)){
							$idga=$control[0]->idga;
							$gasto=$this->M_gasto->get($idga);
							if(!empty($gasto)){
								$detalle_gasto=$this->M_detalle_gasto->get_row('idga',$idga);
								if(count($detalle_gasto)<=1){//solo tiene un elemento
									if($this->M_gasto->eliminar($idga)){
										echo "ok";
									}else{
										echo "error";
									}
								}else{
									if($this->M_detalle_gasto->eliminar($iddga)){
										echo "ok";
									}else{
										echo "error";
									}
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "validate";
					}
				}else{
					echo "validate";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE COMPRAS -------*/


























/*--------------- MANEJO DE PEDIDOS ---------------*/

	/*--- nuevo pedido ---*//*

	public function costo_producto(){
		if(!empty($this->session_id)){
			$idcl=$_POST['cliente'];
			$idpim=$_POST['prod'];
			$pos=$_POST['pos'];
			$producto_cliente=$this->M_producto_cliente->get_row_2n('idcl',$idcl,'idpim',$idpim);
			if(count($producto_cliente)>0){
				$resultado="<input type='number' id='cu".$pos."' class='form-control input-sm' value='".$producto_cliente[0]->costo_unitario."' disabled='disabled'>";
			}else{
				$resultado="<input type='number' id='cu".$pos."' class='form-control input-sm' value='0' disabled='disabled'>";
			}
			//$this->load->view('pedidos_ventas/pedidos/3-nuevo/complementos/row_producto',$listado);
			echo $resultado;
		}else{
			echo "logout";
		}
	}




	public function new_pedido_2(){
		if(!empty($this->session_id)){
			$clie=$_POST['clie'];
			$fped=$_POST['fped'];
			$obs=$_POST['obs'];
			$nro=$_POST['nro'];

			$monto_sistema=$_POST['monto_sistema'];
			$descuento=$_POST['descuento'];
			$adelanto=$_POST['adelanto'];

			$tiempo=$_POST['tiempo'];
			$fecha_inicio=$_POST['fecha_inicio'];
			$fecha_estimada=$_POST['fecha_estimada'];
			$fecha_entrega=$_POST['fecha_entrega'];

			$productos=trim($_POST['productos']);
			$aux=$this->M_pedido->max('idpe');
			$idpe=($aux[0]->max)+1;
			if($idpe==$nro){
				if($clie!="" && $fped!="" && $fecha_entrega!="" && $monto_sistema!="" && $productos!=""){
					if($this->M_pedido->insertar($idpe,$clie,$fped,$tiempo,$fecha_inicio,$fecha_estimada,$fecha_entrega,$monto_sistema,$descuento,$adelanto,$obs)){
						$vpro=explode("[|]", $productos);
						for($i=0;$i<count($vpro);$i++){
							$v=explode("|", $vpro[$i]);
							if($this->M_detalle_pedido->insertar($idpe,$v[0],$v[1])){}
						}
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "numero_orden_exist";
			}
		}else{
			echo "logout";
		}
	}
	/*--- end nuevo pedido ---*/
	/*--- reportes de pedidos ---*/


	/*--- end reportes de pedidos ---*/
	/*--- configuracion de pedidos ---*/
		/*-----modificar-----*/

	/*	public function update_pedido(){
			if(!empty($this->session_id)){
				$idpe=$_POST['idpe'];
				if($this->M_pedido->modificar($idpe,$_POST['clie'],$_POST['fe1'],$_POST['fe2'],$_POST['monto'],$_POST['adelanto'],$_POST['obs'])){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "logout";
			}
		}*/
		/*----- end modificar -----*/
		/*----- productos -----*/
		/*public function productos_pedido(){
			if(!empty($this->session_id)){
				$idpe=$_POST['id'];
				$listado['pedido']=$this->M_pedido->get($idpe);
				$listado['detalle_pedido']=$this->M_detalle_pedido->get_producto($idpe);
				$this->load->view('pedidos_ventas/pedidos/6-configuracion/form_adicionar_producto_pedido',$listado);
			}else{
				echo "logout";
			}
		}*//*
		public function save_cantidad_pedido(){
			if(!empty($this->session_id)){
				$iddp=$_POST['iddp'];
				$can=$_POST['can'];
				if($this->M_detalle_pedido->modificar_row_id($iddp,'cantidad',$can)){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "logout";
			}
		}
		public function drop_cantidad_pedido(){
			if(!empty($this->session_id)){
				$iddp=$_POST['iddp'];
				if($this->M_detalle_pedido->eliminar($iddp)){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "logout";
			}
		}
		public function search_view_productos(){
			if(!empty($this->session_id)){
				$idpe=$_POST['idpe'];
				//$listado['productos']=$this->M_categoria_pieza->get_categoria_color_producto(1);
				$listado['idpe']=$idpe;
				//$listado['detalle_pedidos']=$this->M_detalle_pedido->get_producto($idpe);
				$this->load->view('pedidos_ventas/pedidos/6-configuracion/productos/search_view_producto',$listado);
			}else{
				echo "logout";
			}
		}
		public function view_productos(){
			if(!empty($this->session_id)){
				$idpe=$_POST['idpe'];
				$listado['productos']=$this->M_categoria_pieza->get_categoria_color_producto(1);
				$listado['idpe']=$idpe;
				//$listado['detalle_pedidos']=$this->M_detalle_pedido->get_producto($idpe);
				$this->load->view('pedidos_ventas/pedidos/6-configuracion/productos/view_producto',$listado);
			}else{
				echo "logout";
			}
		}
		public function save_producto(){
			if(!empty($this->session_id)){
				$idpe=$_POST['idpe'];
				$idpim=$_POST['idpim'];
				$existe=$this->M_detalle_pedido->get_row_2n('idpe',$idpe,'idpim',$idpim);
				if(count($existe)>0){//desactivar
					if($this->M_detalle_pedido->eliminar($existe[0]->iddp)){
						echo "0|ok";
					}else{
						echo "|fail";
					}
				}else{//activar boton
					if($this->M_detalle_pedido->insertar($idpe,$idpim)){
						echo "1|ok";
					}else{
						echo "|fail";
					}
				}
				
			}else{
				echo "|logout";
			}
		}
		/*----- end producto -----*/
	/*--- end configuracion de pedidos ---*/

/*--------------- END MANEJO DE PEDIDOS ---------------*//*

//MANEJO DE VENTAS



	public function reportes_venta(){
		$idv=$_POST['idv'];
		$venta=$this->M_venta->get($idv);
		$listado['venta']=$venta;
		$listado['pedido']=$this->M_pedido->get($venta[0]->idpe);
		$listado['detalle_pedido']=$this->M_detalle_pedido->get_producto($venta[0]->idpe);
		$this->load->view('pedidos_ventas/ventas/5-reportes/form_detalle_venta',$listado);
	}
	public function configuracion_venta(){
		$idv=$_POST['idv'];
		$venta=$this->M_venta->get($idv);
		$listado['venta']=$venta;
		$listado['pedido']=$this->M_pedido->get($venta[0]->idpe);
		$listado['detalle_pedido']=$this->M_detalle_pedido->get_producto($venta[0]->idpe);
		$this->load->view('pedidos_ventas/ventas/6-configuracion/modificar',$listado);
	}

	public function modificar_venta(){
		if(!empty($this->session_id)){
			$idv=$_POST['idv'];
			$venta=$this->M_venta->get($idv);
			$idpe=$venta[0]->idpe;
			$datos=$_POST['datos'];
			$fecha=$_POST['fecha'];
			
			$s_monto=0;
			$s_des=0;
			$producto=explode("|", $datos);
			for ($i=0; $i < count($producto) ; $i++) { 
				$detalle=explode("[$]", $producto[$i]);
				if(count($detalle)>3){
					if($this->M_detalle_pedido->modificar_vendido($detalle[0],$detalle[1],'',$detalle[3])){
						$ct=$detalle[1]*$detalle[2];
						$s_monto+=$ct;
						$s_des+=$detalle[3];
					}
				}
				
			}
			if($this->M_venta->modificar($idv,$idpe,$s_monto,$s_des,$fecha,'')){
				echo "ok";
			}else{
				echo "error";
			}
		}else{
			echo "logout";
		}
	}
	public function modificar_detalle_pedido_venta(){
		if(!empty($this->session_id)){
			$idpe=$_POST['id'];
			$listado['pedido']=$this->M_pedido->get($idpe);
			$listado['detalle_pedido']=$this->M_detalle_pedido->get_producto($idpe);
			$this->load->view('pedidos_ventas/ventas/6-configuracion/modficar',$listado);
		}else{
			echo "logout";
		}
	}

/*-------- MANEJO DE CLIENTES ---------*//*
	public function view_search_clientes(){
		if(!empty($this->session_id)){
			$this->load->view('pedidos_ventas/clientes/vista/search_clientes');
		}else{
			echo "logout";
		}
	}

	public function view_all_clientes(){
		if(!empty($this->session_id)){
			$listado['clientes']=$this->M_cliente->get_all();
			$this->load->view('pedidos_ventas/clientes/vista/view',$listado);
		}else{
			echo "logout";
		}
	}
	public function search_clientes(){
		if(!empty($this->session_id)){
			$nit=$_POST['nit'];
			$razon=$_POST['razon'];
			$direccion=$_POST['direc'];
			$telefono=$_POST['tel'];
			//echo $telefono;
			$col="";$val="";
			if($this->validaciones->esEntero($nit,1,15)){
				$col='nit';$val=$nit;
			}else{
				if($this->validaciones->esPalabraConEspacio($razon,1,90)){
					$col='razon_social';$val=$razon;
				}else{
					if($this->validaciones->esPalabraConEspacio($direccion,1,200)){
						$col='direccion';$val=$direccion;
					}else{
						if($this->validaciones->esPalabraSinEspacio($telefono,1,15)){
							$col='telefono';$val=$telefono;
						}
					}
				}
			}
			//echo "`-".$col." ".$val;
			if($col!="" && $val!=""){
				$listado['clientes']=$this->M_cliente->get_row_like($col,$val);
			}else{
				$listado['clientes']=$this->M_cliente->get_all();
			}
			$this->load->view('pedidos_ventas/clientes/vista/view',$listado);		
		}else{
			echo "logout";
		}
	}
	public function form_new_cliente(){
		if(!empty($this->session_id)){
			$this->load->view('pedidos_ventas/clientes/nuevo/view');
		}else{
			echo "logout";
		}
	}
	public function save_cliente(){
		if(!empty($this->session_id)){
			$nit=$_POST['nit'];
			$raz=$_POST['raz'];
			$tel=$_POST['tel'];
			$ema=$_POST['ema'];
			$pai=$_POST['pai'];
			$ciu=$_POST['ciu'];
			$dir=$_POST['dir'];
			$obs=$_POST['obs'];
			if($this->M_cliente->insertar($raz,$nit,NULL,$dir,$tel,$ema,$pai,$ciu,$obs)){
				echo "ok";
			}else{
				echo "error";
			}

		}else{
			echo "logout";
		}
	}
	public function detalle_cliente(){
		if(!empty($this->session_id)){
			$id_cliente=$_POST['cl'];
			$listado['cli']=$this->M_cliente->get($id_cliente);
			$this->load->view('pedidos_ventas/clientes/vista/detalle_cliente',$listado);
		}else{
			echo "logout";
		}
	}
	public function modificar_cliente(){
		if(!empty($this->session_id)){
			$id_cliente=$_POST['cl'];
			$listado['cli']=$this->M_cliente->get($id_cliente);
			$this->load->view('pedidos_ventas/clientes/configuracion/modificar_cliente',$listado);
		}else{
			echo "logout";
		}
	}

	public function productos_cliente(){
		if(!empty($this->session_id)){
			$id_cliente=$_POST['cl'];
			$listado['cli']=$this->M_cliente->get($id_cliente);
			$listado['pc']=$this->M_producto_cliente->get_producto_cliente($id_cliente);
			$this->load->view('pedidos_ventas/clientes/configuracion/productos_cliente',$listado);
		}else{
			echo "logout";
		}
	}

	public function search_view_productos_cliente(){
		if(!empty($this->session_id)){
			$idcl=$_POST['idcl'];
			$listado['idcl']=$idcl;
			$this->load->view('pedidos_ventas/clientes/configuracion/productos/search_producto_cliente',$listado);
		}else{
			echo "logout";
		}
	}
	public function view_productos_cliente(){
		if(!empty($this->session_id)){
			$idcl=$_POST['idcl'];
			$listado['productos']=$this->M_categoria_pieza->get_categoria_color_producto(1);
			$listado['idcl']=$idcl;
			$this->load->view('pedidos_ventas/clientes/configuracion/productos/view_producto_cliente',$listado);
		}else{
			echo "logout";
		}
	}
	public function save_producto_cliente(){
		if(!empty($this->session_id)){
			$idcl=$_POST['idcl'];
			$idpim=$_POST['idpim'];
			$existe=$this->M_producto_cliente->get_row_2n('idcl',$idcl,'idpim',$idpim);
			if(count($existe)>0){//desactivar
				if($this->M_producto_cliente->eliminar($existe[0]->idpc)){
					echo "0|ok";
				}else{
					echo "|fail";
				}
			}else{//activar boton
				if($this->M_producto_cliente->insertar($idcl,$idpim)){
					echo "1|ok";
				}else{
					echo "|fail";
				}
			}
			
		}else{
			echo "|logout";
		}
	}

/*-------- END MANEJO DE CLIENTES ---------*/
}
/* End of file pedidos_ventas.php */
/* Location: ./application/controllers/pedidos_ventas.php */