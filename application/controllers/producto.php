<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class producto extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('login');
	}
	public function index(){
		if(!empty($this->session_id)){
				$listado['privilegio']="";
				$this->load->view('v_producto',$listado);	
		}else{
			redirect(base_url().'login',301);
		}
	}
/*------- MANEJO DE PRODUCTO -------*/
	public function view_search_producto($ida){
		if(!empty($this->session_id)){
			$this->load->view('almacen_producto/producto/search');
		}else{
			echo "logout";
		}
	}
	public function view_producto($ida){//En uso
		if(!empty($this->session_id)){
			if(isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['can'])){//si tenemos busqueda 
				$cod=$_POST['cod'];
				$nom=$_POST['nom'];
				$can=$_POST['can'];
				if($cod!=""){
					$atrib="p.cod";$val=$cod;
				}else{
					if($nom!=""){
						$atrib="p.nombre";$val=$nom;
					}else{
						if($can!=""){
							$atrib="ap.cantidad";$val=$can;
						}else{
							$atrib="";$cod="";
						}
					}
				}
				if($atrib!="" && $val!=""){
					$listado['categoria_productos']=$this->M_almacen_producto->get_producto_almacen_ida_row($ida,$atrib,$val);
				}else{
					$listado['categoria_productos']=$this->M_almacen_producto->get_producto_almacen_ida($ida);
				}
			}else{
				$listado['categoria_productos']=$this->M_almacen_producto->get_producto_almacen_ida($ida);
			}
			$this->load->view('almacen_producto/producto/view',$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Buscador ---*/
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
   	public function form_search_producto($ida){
		if(!empty($this->session_id)){
			$this->load->view('almacen_producto/producto/3-nuevo/search');
		}
	}
	public function form_select_producto($ida){
		if(!empty($this->session_id)){
			if(isset($_POST['cod']) && isset($_POST['nom'])){
				$cod=$_POST['cod'];
				$nom=$_POST['nom'];
				if($cod!=""){
					$atrib="p.cod";$val=$cod;
				}else{
					if($nom!=""){
						$atrib="p.nombre";$val=$nom;
					}else{
						$atrib="";$cod="";
					}
				}
				if($atrib!="" && $val!=""){
					$listado['productos']=$this->M_categoria_pieza->get_where_categoria_color_producto($atrib,$val,1);
				}else{
					$listado['productos']=$this->M_categoria_pieza->get_categoria_color_producto(1);
				}
			}else{
				$listado['productos']=$this->M_categoria_pieza->get_categoria_color_producto(1);
			}
				$listado['ida']=$ida;
				$this->load->view('almacen_producto/producto/3-nuevo/view',$listado);
		}else{
			echo "logout";
		}
	}
	public function change_almacen_produco($almacen){
		if(!empty($this->session_id)){
			$idpim=$_POST['idpim'];
			$existe=$this->M_almacen_producto->get_row_2n('ida',$almacen,'idpim',$idpim);
			if(count($existe)>0){//desactivar
				if($this->M_almacen_producto->eliminar($existe[0]->idap)){
					echo "0|ok";
				}else{
					echo "|fail";
				}
			}else{//activar boton
				if($this->M_almacen_producto->insertar($almacen,$idpim,0)){
					echo "1|ok";
				}else{
					echo "|fail";
				}
			}
			
		}else{
			echo "|logout";
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	public function config_imprimir_producto($ida){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				$listado['productos']=$_POST['json'];
				$this->load->view('almacen_producto/producto/4-imprimir/config_view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function imprimir_producto($ida){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				if(isset($_POST['v1'])){ if($_POST['v1']!="ok"){ $listado['v1']="ok";} }
				if(isset($_POST['v2'])){ if($_POST['v2']!="ok"){ $listado['v2']="ok";} }
				if(isset($_POST['v3'])){ if($_POST['v3']!="ok"){ $listado['v3']="ok";} }
				if(isset($_POST['v4'])){ if($_POST['v4']!="ok"){ $listado['v4']="ok";} }
				if(isset($_POST['v5'])){ if($_POST['v5']!="ok"){ $listado['v5']="ok";} }
				if(isset($_POST['v6'])){ if($_POST['v6']!="ok"){ $listado['v6']="ok";} }
				if(isset($_POST['v7'])){ if($_POST['v7']!="ok"){ $listado['v7']="ok";} }
				if(isset($_POST['v8'])){ if($_POST['v8']!="ok"){ $listado['v8']="ok";} }
				if(isset($_POST['v9'])){ if($_POST['v9']!="ok"){ $listado['v9']="ok";} }
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=35;}
				$almacen=$this->M_almacen->get($ida);
				if(!empty($almacen)){
					$listado['productos']=$_POST['json'];
					$listado['almacen']=$almacen[0];
					$this->load->view('almacen_producto/producto/4-imprimir/view',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	public function save_reg_mov($ida){//En uso
		if(!empty($this->session_id)){
			if(isset($_POST['idap']) && isset($_POST['ing']) && isset($_POST['sal']) && isset($_POST['fech'])){
				$idap=$_POST['idap'];
				$ing=$_POST['ing'];
				$sal=$_POST['sal'];
				$fech=$_POST['fech'];
				if($fech!=""){ $fech=str_replace('T', ' ', $fech); }
					$almacen=$this->M_almacen->get($ida);
					if(!empty($almacen)){
						$producto=$this->M_almacen_producto->get_producto_almacen_ida_row($ida,'ap.idap',$idap);
						if(!empty($producto)){
							if($ing>0 || $sal>0){
								$stock=$producto[0]->cantidad;
								$saldo=$stock+$ing-$sal;
								if($saldo>=0){
									if($this->M_almacen_producto->modificar_col($idap,'cantidad',$saldo)){
										$usuario=$this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');
										$color=$this->M_material->get_material_color($producto[0]->idm);
										if(!empty($color)){
											$col=$color[0]->nombre;
										}else{
											$col="";
										}
										if($this->M_movimiento_almacen->insertar($almacen[0]->nombre,'producto',$usuario,$producto[0]->codigo,$producto[0]->nombre.'-'.$col,$fech,$stock,$ing,$sal)){
										}
										echo "ok|".$saldo;
									}else{
										echo "error|0";
									}
								}else{
									echo "error|0";
								}
							}else{
								echo "ok|".$producto[0]->cantidad;
							}
						}else{
							echo "fail|0";
						}
					}else{
						echo "fail|0";
					}
			}else{
				echo "fail|0";
			}
		}else{
			echo "logout|0";
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	public function confirmar($ida){
		if(!empty($this->session_id)){
			if(isset($_POST['idap']) && isset($_POST['url']) && isset($_POST['nombre'])){
				$idap=$_POST['idap'];
				$url='./../../libraries/img/pieza_productos/miniatura/'.$_POST['url'];
				$almacen=$this->M_almacen->get($ida);
				if(!empty($almacen)){
					$listado['titulo']="el producto ".$_POST['nombre']." del ".$almacen[0]->nombre;
					$almacen_producto=$this->M_almacen_producto->get($idap);
					if(!empty($almacen_producto)){
						if($almacen_producto[0]->cantidad>0){
							$listado['control']="";
							$listado['open_control']="false";
							$listado['desc']="Imposible eliminar el producto del almacen, el producto debe tener stock cero para ser eliminado";
						}else{
							$listado['desc']="Se quitara el producto del almacen, el producto seguira disponible en produccion";
						}
						$listado['img']=$url;
						$this->load->view('estructura/form_eliminar',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function	drop_producto(){
   		if(!empty($this->session_id)){
   			if(isset($_POST['idap']) && isset($_POST['u']) && isset($_POST['p'])){
   				$idap=$_POST['idap'];
				$u=$_POST['u'];
				$p=$_POST['p'];
				if($u==$this->session->userdata("login")){
					$usuario=$this->M_empleado->validate($u,$p);
					if(!empty($usuario)){
						if($idap!=""){
							if($this->M_almacen_producto->eliminar($idap)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "validate";
					}
				}else{
					echo "validate";
				}
   			}else{
   				echo "fail";
   			}
   		}else{
			echo "logout";
		}
   	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE PRODUCTO -------*/


	/*public function add_producto($almacen){//En uso
		if(!empty($this->session_id)){
			$idp=$_POST['p'];
			if($this->M_almacen_producto->insertar($almacen,$idp,0)){
				echo "ok";
			}else{
				echo "error";
			}
		}else{
			echo "logout";
		}
	}

	public function view_search_producto2($almacen){//En uso
		if(!empty($this->session_id)){
				$atributo='';
				$valor=$_POST['v'];
				if($_POST['a']=='n'){$atributo='nombre';}else{if($_POST['a']=='c'){$atributo='cod';}}
				if($atributo!='' & $valor!=''){
					$listado['productos']=$this->M_producto->get_producto_almacen_not_col_row($almacen,$atributo,$valor);
					$this->load->view('almacen_producto/ajax/view_add_producto',$listado);
				}else{
					echo "fail";
				}
		}else{
			echo "logout";
		}
	}
	
	public function valida_usuario(){//En uso
		if(!empty($this->session_id)){
			$u=$_POST['u'];
			$p=$_POST['p'];
			if($u==$this->session->userdata("login")){
				$usuario=$this->M_usuario->validate($u,$p);
				if(!empty($usuario)){
					echo "exist";
				}else{
					echo "none";
				}
			}else{
				echo "none";
			}
		}else{
			echo "logout";
		}
	}

	public function eliminar_producto_almacen(){//En uso
		if(!empty($this->session_id)){
			$id=$_POST['id'];
			if($this->M_almacen_producto->eliminar($id)){
				echo "ok";
			}else{
				echo "error";
			}
		}else{
			echo "logout";
		}
	}*/
















	



	
}
/* End of file producto.php */
/* Location: ./application/controllers/producto.php */