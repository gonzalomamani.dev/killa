<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Almacen extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('login');
	}
	public function index(){
		if(!empty($this->session_id)){
			$privilegio=$privilegio=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			if($privilegio[0]->al=="1"){
				if(!isset($_GET['p'])){
					if($privilegio[0]->al1r==1){
						$listado['pestania']=1;
					}else{
						if($privilegio[0]->al2r==1){ 
							$listado['pestania']=2;
						}else{
							if($privilegio[0]->al3r==1){ 
								$listado['pestania']=3;
							}else{
								$listado['pestania']=0;
							}
						}
					}
				}else{
					$listado['pestania']=$_GET['p'];
				}
				$listado['privilegio']=$privilegio;
				$this->load->view('v_almacen',$listado);
			}else{
				$this->val->redireccion($privilegio);
			}
		}else{
			redirect(base_url().'login/input',301);
		}
	}
/*------- MANEJO DE ALMACENES -------*/
	public function view_search_almacen(){
		if(!empty($this->session_id)){
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('almacen/almacenes/search',$listado);
		}else{
			echo "logout";
		}
	}
	public function view_almacen(){
		if(!empty($this->session_id)){
			$col="";$val="";
			if(isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['tip'])){
				if($_POST['cod']!=""){
					$col="a.codigo";$val=$_POST['cod'];
				}else{
					if($_POST['nom']!=""){
						$col="a.nombre";$val=$_POST['nom'];
					}else{
						if($_POST['tip']!=""){
							$col="a.tipo";$val=$_POST['tip'];
						}
					}
				}
			}
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$listado['almacenes']=$this->M_almacen->get_search($col,$val);
			$this->load->view('almacen/almacenes/view',$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Nuevo ---*/
   	public function view_form_new_almacen(){
		if(!empty($this->session_id)){
			$this->load->view('almacen/almacenes/3-nuevo/form');
		}else{
			echo "logout";
		}
	}
	public function save_almacen(){
		if(!empty($this->session_id)){
			if(isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['tip']) && isset($_POST['des'])){
				$cod=trim($_POST['cod'])."";
				$nom=trim($_POST['nom'])."";
				$tip=trim($_POST['tip'])."";
				$des=trim($_POST['des'])."";
				if($this->val->strSpace($cod,2,10) && $this->val->strSpace($nom,3,100) && ($tip=="material" || $tip=="producto")){
					$control=true;
					if($des!=""){if(!$this->val->textarea($des,0,400)){ $control=false;}}
					if($control){
						$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/almacenes/','',$this->resize,NULL);
						if($img!='error' && $img!="error_type" && $img!="error_size_img"){
							if($this->M_almacen->insertar($cod,$nom,$tip,$img,$des)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo $img;
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}			
		}else{
			echo "logout";
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	public function config_imprimir_almacen(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				$listado['almacenes']=$_POST['json'];
				$this->load->view('almacen/almacenes/4-imprimir/config_view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function imprimir_almacen(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				if(isset($_POST['ite'])){ if($_POST['ite']!="ok"){ $listado['ite']="ok";} }
				if(isset($_POST['fot'])){ if($_POST['fot']!="ok"){ $listado['fot']="ok";} }
				if(isset($_POST['cod'])){ if($_POST['cod']!="ok"){ $listado['cod']="ok";} }
				if(isset($_POST['nom'])){ if($_POST['nom']!="ok"){ $listado['nom']="ok";} }
				if(isset($_POST['tip'])){ if($_POST['tip']!="ok"){ $listado['tip']="ok";} }
				if(isset($_POST['des'])){ if($_POST['des']!="ok"){ $listado['des']="ok";} }
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=35;}
				$listado['almacenes']=$_POST['json'];
				$this->load->view('almacen/almacenes/4-imprimir/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	public function reporte_almacen(){
		if(!empty($this->session_id)){
			$id=$_POST['id'];
			if($id!=""){
				$listado['almacen']=$this->M_almacen->get($id);
				$this->load->view('almacen/almacenes/5-reporte/view',$listado);
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	public function form_update_almacen(){
		if(!empty($this->session_id)){
			$id=$_POST['id'];
			if($id!=""){
				$listado['almacen']=$this->M_almacen->get($id);
				$listado['id']=$id;
				$this->load->view('almacen/almacenes/6-configuracion/modificar',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	
	public function update_almacen(){
		if(!empty($this->session_id)){
			if(isset($_POST['id']) && isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['des'])){
				$id=trim($_POST['id'])."";
				$cod=trim($_POST['cod'])."";
				$nom=trim($_POST['nom'])."";
				$des=trim($_POST['des'])."";
				if($this->val->entero($id,0,10) && $this->val->strSpace($cod,2,10) && $this->val->strSpace($nom,3,100)){
					$control=true;
					if($des!=""){if(!$this->val->textarea($des,0,400)){ $control=false;}}
					if($control){
						$almacen=$this->M_almacen->get($id);
						if(!empty($almacen)){
							$img=$almacen[0]->fotografia;
							$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/almacenes/','',$this->resize,$img,$id);
							if($img!='error' && $img!="error_type" && $img!="error_size_img"){
								if($this->M_almacen->modificar($id,$cod,$nom,$img,$des)){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo $img;
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}			
		}else{
			echo "logout";
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	public function confirmar(){
		if(!empty($this->session_id)){
			if(isset($_POST['ida'])){
				$ida=$_POST['ida'];
				$url='./libraries/img/almacenes/miniatura/';
				$almacen=$this->M_almacen->get($ida);
				if(!empty($almacen)){
					$listado['titulo']="eliminar el <b>".$almacen[0]->nombre."</b>";
					if($almacen[0]->tipo=='material'){
						$control=$this->M_almacen_material->get_row('ida',$ida);
					}else{
						$control=$this->M_almacen_producto->get_row('ida',$ida);
					}
					if(!empty($control)){
						$listado['control']=$control;
						$listado['open_control']="false";
						$listado['desc']="Imposible eliminar, el almacen no esta vacio, vacie el almacen antes de eliminarlo";					
					}else{
						$listado['desc']="Se eliminara definitivamente";
					}
					$img='default.png';
					if($almacen[0]->fotografia!=NULL && $almacen[0]->fotografia!=""){ $img=$almacen[0]->fotografia; }
					$listado['img']=$url.$img;
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function drop_almacen(){
		if(!empty($this->session_id)){
			$id=$_POST['id'];
			$u=$_POST['u'];
			$p=$_POST['p'];
			if($u==$this->session->userdata("login")){
				$usuario=$this->M_empleado->validate($u,$p);
				if(!empty($usuario)){
					$almacen=$this->M_almacen->get($id);
					if(!empty($almacen)){
						if($this->lib->eliminar_imagen($almacen[0]->fotografia,'./libraries/img/almacenes/')){
							if($this->M_almacen->eliminar($id)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "validate";
				}
			}else{
				echo "validate";
			}
			
		}else{
			echo "logout";
		}
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE ALMACENES -------*/
/*------- MANEJO DE MOVIMIENTO DE MATERIAL -------*/
	public function search_movimiento_material(){
		if(!empty($this->session_id)){
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('almacen/movimiento_material/search',$listado);
		}else{
			echo "logout";
		}
	}
	public function view_movimiento_material(){
		if(!empty($this->session_id)){
			$atrib="";$val="";
			if(isset($_POST['nom']) && isset($_POST['alm']) && isset($_POST['usu']) && isset($_POST['sol']) && isset($_POST['fech_a']) && isset($_POST['fech_b'])){
				$nom=trim($_POST['nom']);
				$alm=trim($_POST['alm']);
				$usu=trim($_POST['usu']);
				$sol=trim($_POST['sol']);
				$fech_a=trim($_POST['fech_a']);
				$fech_b=trim($_POST['fech_b']);
				$tip=trim($_POST['tip']);
				if($nom!=""){ $atrib.="nombre like '".$nom."%'"; }
				if($alm!=""){ if($atrib!=""){ $atrib.=" and ";}$atrib.="almacen like '".$alm."%'"; }
				if($usu!=""){ if($atrib!=""){ $atrib.=" and ";}$atrib.="usuario like '".$usu."%'"; }
				if($sol!=""){ if($atrib!=""){ $atrib.=" and ";}$atrib.="solicitante like '".$sol."%'"; }
				if($this->val->fecha($fech_a) || $this->val->fecha($fech_b)){
					if($this->val->fecha($fech_a)){
						if($this->val->fecha($fech_b)){
							if($atrib!=""){ $atrib.=" and ";}$atrib.="date(fecha_usuario) >= '".$fech_a."' and date(fecha_usuario) <= '".$fech_b."'";
						}else{
							if($atrib!=""){ $atrib.=" and ";}$atrib.="date(fecha_usuario) = '".$fech_a."'"; 
						}
					}else{
						if($atrib!=""){ $atrib.=" and ";}$atrib.="date(fecha_usuario) = '".$fech_b."'";
					}
				}
				if($tip==1){ if($atrib!=""){ $atrib.=" and ";}$atrib.="ingreso != '0'";}
				if($tip==2){ if($atrib!=""){ $atrib.=" and ";}$atrib.="salida != '0'";}
				
			}
			$listado['movimientos']=$this->M_movimiento_almacen->get_search('material',$atrib);
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('almacen/movimiento_material/view',$listado);	
		}else{
			echo "logout";
		}
	}
   	/*--- Imprimir ---*/
   	public function config_imprimir_almacen_hm(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				$listado['movimientos']=$_POST['json'];
				$this->load->view('almacen/movimiento_material/4-imprimir/reporte/config',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function imprimir_almacen_hm(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				if(isset($_POST['v1'])){ if($_POST['v1']!="ok"){ $listado['v1']="ok";} }
				if(isset($_POST['v2'])){ if($_POST['v2']!="ok"){ $listado['v2']="ok";} }
				if(isset($_POST['v3'])){ if($_POST['v3']!="ok"){ $listado['v3']="ok";} }
				if(isset($_POST['v4'])){ if($_POST['v4']!="ok"){ $listado['v4']="ok";} }
				if(isset($_POST['v5'])){ if($_POST['v5']!="ok"){ $listado['v5']="ok";} }
				if(isset($_POST['v6'])){ if($_POST['v6']!="ok"){ $listado['v6']="ok";} }
				if(isset($_POST['v7'])){ if($_POST['v7']!="ok"){ $listado['v7']="ok";} }
				if(isset($_POST['v8'])){ if($_POST['v8']!="ok"){ $listado['v8']="ok";} }
				if(isset($_POST['v9'])){ if($_POST['v9']!="ok"){ $listado['v9']="ok";} }
				if(isset($_POST['v10'])){ if($_POST['v10']!="ok"){ $listado['v10']="ok";} }
				if(isset($_POST['v11'])){ if($_POST['v11']!="ok"){ $listado['v11']="ok";} }
				if(isset($_POST['v12'])){ if($_POST['v12']!="ok"){ $listado['v12']="ok";} }
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=35;}
				$listado['movimientos']=$_POST['json'];
				$this->load->view('almacen/movimiento_material/4-imprimir/reporte/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function config_general(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				$listado['movimientos']=$_POST['json'];
				$this->load->view('almacen/movimiento_material/4-imprimir/reporte/config',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function imprimir_general(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				if(isset($_POST['v1'])){ if($_POST['v1']!="ok"){ $listado['v1']="ok";} }
				if(isset($_POST['v2'])){ if($_POST['v2']!="ok"){ $listado['v2']="ok";} }
				if(isset($_POST['v3'])){ if($_POST['v3']!="ok"){ $listado['v3']="ok";} }
				if(isset($_POST['v4'])){ if($_POST['v4']!="ok"){ $listado['v4']="ok";} }
				if(isset($_POST['v5'])){ if($_POST['v5']!="ok"){ $listado['v5']="ok";} }
				if(isset($_POST['v6'])){ if($_POST['v6']!="ok"){ $listado['v6']="ok";} }
				if(isset($_POST['v7'])){ if($_POST['v7']!="ok"){ $listado['v7']="ok";} }
				if(isset($_POST['v8'])){ if($_POST['v8']!="ok"){ $listado['v8']="ok";} }
				if(isset($_POST['v9'])){ if($_POST['v9']!="ok"){ $listado['v9']="ok";} }
				if(isset($_POST['v10'])){ if($_POST['v10']!="ok"){ $listado['v10']="ok";} }
				if(isset($_POST['v11'])){ if($_POST['v11']!="ok"){ $listado['v11']="ok";} }
				if(isset($_POST['v12'])){ if($_POST['v12']!="ok"){ $listado['v12']="ok";} }
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=35;}
				$listado['movimientos']=$_POST['json'];
				$this->load->view('almacen/movimiento_material/4-imprimir/reporte/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	public function confirmar_hitorial_material(){
		if(!empty($this->session_id)){
				$listado['titulo']="eliminar el historial de movimiento de los almacenes de materiales";
				$listado['desc']="Todos los registros se eliminaran definitivamente";
				$this->load->view('estructura/form_eliminar',$listado);
		}else{
			echo "logout";
		}
	}
   	public function borrar_historial_material(){
   		if(!empty($this->session_id)){
			$u=$_POST['u'];
			$p=$_POST['p'];
			if($u==$this->session->userdata("login")){
				$usuario=$this->M_empleado->validate($u,$p);
				if(!empty($usuario)){
					if($this->M_movimiento_almacen->eliminar_row('tipo','material')){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "validate";
				}
			}else{
				echo "validate";
			}
		}else{
			echo "logout";
		}
   	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE MOVIMIENTO DE MATERIAL -------*/
/*------- MANEJO DE MOVIMIENTO DE PRODUCTO -------*/
	public function view_search_movimiento_producto(){
		if(!empty($this->session_id)){
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('almacen/movimiento_producto/search',$listado);
		}else{
			echo "logout";
		}
	}
	public function view_movimiento_producto(){
		if(!empty($this->session_id)){
			$atrib="";$val="";
			if(isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['alm']) && isset($_POST['usu']) && isset($_POST['hra']) && isset($_POST['fech_a']) && isset($_POST['fech_b'])){
				$cod=$_POST['cod'];
				$nom=$_POST['nom'];
				$alm=$_POST['alm'];
				$usu=$_POST['usu'];
				$hra=$_POST['hra'];
				$fech_a=$_POST['fech_a'];
				$fech_b=$_POST['fech_b'];
				if($cod!=""){
					$atrib="cod";
					$val=$cod;
				}else{
					if($nom!=""){
						$atrib="nombre";
						$val=$nom;
					}else{
						if($alm!=""){
							$atrib="almacen";
							$val=$alm;
						}else{
							if($usu!=""){
								$atrib="usuario";
								$val=$usu;
							}else{
								if($hra!="" && $fech_a!="" && $fech_b!=""){
									$atrib="tres";
									$val=$hra."|".$fech_a."|".$fech_b;
								}else{
									if(($hra!="" && $fech_a!="") || ($hra!="" && $fech_b!="")){
										$atrib="dos";
										$val=$hra."|".$fech_a.$fech_b;								
									}else{
										if($fech_a!="" && $fech_b!=""){
											$atrib="uno";
											$val=$fech_a."|".$fech_b;								
										}else{
											if($hra!=""){
												$atrib="time(fecha_accion)";
												$val=$hra;	
											}else{
												if($fech_a!="" || $fech_b!=""){
													$atrib="date(fecha_accion)";
													$val=$fech_a.$fech_b;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			$listado['movimientos']=$this->M_movimiento_almacen->get_search('producto',$atrib,$val);
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view("almacen/movimiento_producto/view",$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Imprimir ---*/
   	public function config_imprimir_almacen_hp(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				$listado['movimientos']=$_POST['json'];
				$this->load->view('almacen/movimiento_producto/4-imprimir/config_view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function imprimir_almacen_hp(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				if(isset($_POST['v1'])){ if($_POST['v1']!="ok"){ $listado['v1']="ok";} }
				if(isset($_POST['v2'])){ if($_POST['v2']!="ok"){ $listado['v2']="ok";} }
				if(isset($_POST['v3'])){ if($_POST['v3']!="ok"){ $listado['v3']="ok";} }
				if(isset($_POST['v4'])){ if($_POST['v4']!="ok"){ $listado['v4']="ok";} }
				if(isset($_POST['v5'])){ if($_POST['v5']!="ok"){ $listado['v5']="ok";} }
				if(isset($_POST['v6'])){ if($_POST['v6']!="ok"){ $listado['v6']="ok";} }
				if(isset($_POST['v7'])){ if($_POST['v7']!="ok"){ $listado['v7']="ok";} }
				if(isset($_POST['v8'])){ if($_POST['v8']!="ok"){ $listado['v8']="ok";} }
				if(isset($_POST['v9'])){ if($_POST['v9']!="ok"){ $listado['v9']="ok";} }
				if(isset($_POST['v10'])){ if($_POST['v10']!="ok"){ $listado['v10']="ok";} }
				if(isset($_POST['v11'])){ if($_POST['v11']!="ok"){ $listado['v11']="ok";} }
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=35;}
				$listado['movimientos']=$_POST['json'];
				$this->load->view('almacen/movimiento_producto/4-imprimir/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Eliminar ---*/
   	public function confirmar_hitorial_producto(){
		if(!empty($this->session_id)){
				$listado['titulo']="el historial de movimiento de los almacenes de productos";
				$listado['desc']="Todos los registros se eliminaran definitivamente";
				$this->load->view('estructura/form_eliminar',$listado);
		}else{
			echo "logout";
		}
	}
   	public function borrar_historial_producto(){
   		if(!empty($this->session_id)){
			$u=$_POST['u'];
			$p=$_POST['p'];
			if($u==$this->session->userdata("login")){
				$usuario=$this->M_empleado->validate($u,$p);
				if(!empty($usuario)){
					if($this->M_movimiento_almacen->eliminar_row('tipo','producto')){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "validate";
				}
			}else{
				echo "validate";
			}
		}else{
			echo "logout";
		}
   	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE MOVIMIENTO DE PRODUCTO -------*/
/*----MANEJO DE FUNCIONES PARA LOS ALMACENES DE MATERIALES ----*/

	public function material($ida){
			if(!empty($this->session_id)){
				if(!empty($ida)){
					$exist=$this->M_almacen->get_id_mat($ida,'material');
					if(!empty($exist)){
						$privilegio = $this->M_privilegio->get_row("ide",$this->session->userdata("id"));
						if($privilegio[0]->al=="1"){
							if(!isset($_GET['p'])){
								$listado['pestania']=1;
							}else{
								$listado['pestania']=$_GET['p'];
							}
							$listado['privilegio']=$privilegio;
							$listado['almacen']=$ida;
							$this->load->view('v_material',$listado);
						}else{
							$this->val->redireccion($privilegio);
						}
					}else{
						echo "error";
					}
				}else{
					redirect(base_url(),301);
				}
			}else{
				redirect(base_url(),301);
			}
		
	}

//FUNCION PARA MANEJO DE ALMACEN DE PRODUCTOS
	public function producto($ida){
		if(!empty($this->session_id)){
			if(!empty($ida)){
				$exist=$this->M_almacen->get_id_mat($ida,'producto');
				if(!empty($exist)){
						$privilegio = $this->M_privilegio->get_row("ide",$this->session->userdata("id"));
						if($privilegio[0]->al=="1"){
							if(!isset($_GET['p'])){
								$listado['pestania']=1;
							}else{
								$listado['pestania']=$_GET['p'];
							}
							$listado['privilegio']=$privilegio;
							$listado['almacen']=$ida;
							$this->load->view('v_producto',$listado);
						}else{
							$this->val->redireccion($privilegio);
						}
				}else{
					echo "error";
				}
			}else{
				redirect(base_url(),301);
			}
		}else{
			redirect(base_url(),301);
		}	
	}
}

/* End of file almacen.php */
/* Location: ./application/controllers/almacen.php */