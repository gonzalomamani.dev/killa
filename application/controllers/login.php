<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller {
	private $session_id;
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('login');
	}
	public function index(){ //validamos usuario
		if(empty($this->session_id)){
			redirect(base_url().'login/input',301);
		}else{
			$this->val->redireccion($this->M_privilegio->get_row("ide",$this->session->userdata("id")));
		}
	}
	public function input(){
		$variables['exist']="";
		if(empty($this->session_id)){
			//usuario no logueado
			$this->load->library('form_validation');
			if($this->input->post()){
				$this->form_validation->set_rules('username','Nombre de usuario','required');
				$this->form_validation->set_rules('password','Password de usuario','required');
				if($this->form_validation->run() == true){
					$user=$this->M_empleado->validate($this->input->post('username'),$this->input->post('password'));
					if(empty($user)){
						$variables['exist']='falso';
						$this->load->view('v_login',$variables);
					}else{
						$persona=$this->M_persona->get($user[0]->ci);
						$this->session->set_userdata('control_session');
						$this->session->set_userdata('login',$this->input->post("username",true));
						$this->session->set_userdata('id',$user[0]->ide,true);
						$this->session->set_userdata('nombre',$persona[0]->nombre,true);
						$this->session->set_userdata('paterno',$user[0]->paterno,true);
						$this->session->set_userdata('cargo',$user[0]->cargo,true);
						$this->session->set_userdata('administrador',$user[0]->admin_sistema,true);
						$this->session->set_userdata('fotografia',$persona[0]->fotografia,true);
						$this->val->redireccion($this->M_privilegio->get_row("ide",$user[0]->ide));
					}
				}else{
					$this->load->view('v_login',$variables);
				}
			}else{
				$this->load->view('v_login',$variables);
			}
		}else{
			//usuario logueado
			$this->val->redireccion($this->M_privilegio->get_row("ide",$this->session->userdata("id")));
		}

	}

	public function logout(){
		$this->session->unset_userdata(array('login'=>'','id'=>'','nombre'=>'','paterno'=>'','cargo'=>'','fotografia'=>''));
		$this->session->sess_destroy('control_session');
		redirect(base_url().'login',301);
	}
	public function form_cambiar_password(){
		if(!empty($this->session_id)){
			if(isset($_POST['ide'])){
				$ide=$_POST['ide'];
				$empleado=$this->M_empleado->get($ide);
				if(!empty($empleado)){
					$listado['empleado']=$empleado[0];
					$this->load->view('estructura/change_login',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function cambiar_password(){
		if(!empty($this->session_id)){
			if(isset($_POST['ide']) && isset($_POST['pass']) && isset($_POST['nuevo']) && isset($_POST['nuevo2'])){
				$ide=$_POST['ide'];
				$pass=$_POST['pass'];
				$nuevo=$_POST['nuevo'];
				$nuevo2=$_POST['nuevo2'];
				if($this->val->password($nuevo,4,25)){
					if($nuevo==$nuevo2){
						$empleado=$this->M_empleado->get_complet($ide);
						if(!empty($empleado)){
							if($empleado[0]->usuario==$this->session->userdata("login")){
								$usuario=$this->M_empleado->validate($this->session->userdata("login"),$pass);
								if(!empty($usuario)){
									if($this->M_empleado->modificar_password($ide,$nuevo)){
										echo "ok";
									}else{
										echo "error";
									}
								}else{
									echo "validate";
								}
							}else{
								echo "validate";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */
