<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Material extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('login');
	}
	public function index(){
		if(!empty($this->session_id)){
			if(!isset($_GET['p'])){
				$listado['pestania']=1;
			}else{
				$listado['pestania']=$_GET['p'];
			}
			$listado['']="";
			$this->load->view('v_material',$listado);
		}else{
			redirect(base_url().'login',301);
		}
	}

/*------- MANEJO DE MATERIAL -------*/
	public function view_search(){
		if(!empty($this->session_id)){
			$listado['grupo']=$this->M_grupo->get_all();
			$listado['unidad']=$this->M_unidad->get_all();
			$listado['color']=$this->M_color->get_all();
			$this->load->view('almacen_mmpp/materia_prima/search',$listado);
		}else{
			echo "logout";
		}
	}
	public function view_material($almacen){
		if(!empty($this->session_id)){
			$atrib="";$val="";
			if(isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['gru']) && isset($_POST['can']) && isset($_POST['col'])){
				$cod=$_POST['cod'];
				$nom=$_POST['nom'];
				$gru=$_POST['gru'];
				$can=$_POST['can'];
				$col=$_POST['col'];
				if($cod!=""){
					$atrib="mi.codigo";$val=$cod;
				}else{
					if($nom!=""){
						$atrib="mi.nombre";$val=$nom;
					}else{
						if($gru!=""){
							$atrib="m.idg";$val=$gru;
						}else{
							if($can!=""){
								$atrib="am.cantidad";$val=$can;
							}else{
								if($col!=""){
									$atrib="m.idco";$val=$col;
								}
							}
						}
					}
				}
			}
			$listado['materiales']=$this->M_almacen_material->get_search($almacen,$atrib,$val);
			$this->load->view('almacen_mmpp/materia_prima/view',$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Nuevo ---*/
   	public function new_material($ida){
		if(!empty($this->session_id)){
			$listado['grupo']=$this->M_grupo->get_all();
			$listado['unidad']=$this->M_unidad->get_all();
			$listado['color']=$this->M_color->get_all();
			$listado['proveedores']=$this->M_proveedor->get_all();
			$this->load->view('almacen_mmpp/materia_prima/3-nuevo/form',$listado);
		}else{
			echo "logout";
		}
	}
	public function save_material($ida){
		if(!empty($this->session_id)){
			if(isset($_FILES) && isset($_POST['nom']) && isset($_POST['cod']) && isset($_POST['gru']) && isset($_POST['med']) && isset($_POST['col']) && isset($_POST['des']) && isset($_POST['pro']) && isset($_POST['cos_pro'])){
				$nom=$_POST['nom'];
				$cod=$_POST['cod'];
				$gru=$_POST['gru'];
				$med=$_POST['med'];
				$col=$_POST['col'];
				$des=$_POST['des'];
				$pro=$_POST['pro'];
				$cos_pro=$_POST['cos_pro'];
				if($this->val->entero($gru,0,10) && $this->val->strSpace($cod,2,15) && $this->val->strSpace($nom,3,200) && $this->val->entero($med,0,10) && $this->val->entero($col,0,10)){
					$name_foto=$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/materiales/','',$this->resize,NULL);
					if($name_foto!="error_type" && $name_foto!="error" && $name_foto!="error_size_img"){
						//hallando id de material item
						$max=$this->M_material_item->max_id();
						$idmi=$max[0]->max+1;
						if($this->M_material_item->insertar($idmi,$med,$cod,$nom,$name_foto,$des)){
							$max2=$this->M_material->max_id();
							$idm=$max2[0]->max+1;
							if($this->M_material->insertar($idm,$idmi,$gru,$col,$cos_pro)){
								if($this->M_almacen_material->insertar($ida,$idm,0)){
									if($pro!=""){
										if($this->M_material_proveedor->insertar($pro,$idmi,$cos_pro,date('Y-m-d'))){
											echo "ok";
										}else{
											echo "error";
										}
									}else{
										echo "ok";
									}
								}else{
									$this->M_material_item->eliminar($idmi);
									$this->M_material->eliminar($idm);
									echo "error";
								}
							}else{
								$this->M_material_item->eliminar($idmi);
								echo "error";
							}
						}else{
							echo "error";
						}
					}else{
						echo $name_foto;
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
			
		}else{
			echo "logout";
		}
	}
   	public function form_add_material($ida){
		if(!empty($this->session_id)){
			if(isset($ida)){
				$listado['almacenes']=$this->M_almacen->get_all_not($ida);
				$this->load->view('almacen_mmpp/materia_prima/3-nuevo/form_add',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function add_material($ida){
		if(!empty($this->session_id)){
			if(isset($ida)){
				if(isset($_POST['alm'])){
					$almacen=$_POST['alm'];
					if($almacen!=""){
						$listado['materiales']=$this->M_almacen_material->get_not_material_where($ida,$almacen);
					}else{
						$listado['materiales']=$this->M_almacen_material->get_not_material($ida);	
					}
				}else{
					$listado['materiales']=$this->M_almacen_material->get_not_material($ida);	
				}
				$listado['ida']=$ida;
				$this->load->view('almacen_mmpp/materia_prima/3-nuevo/material/add_material',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function add_material_almacen($ida){
		if(!empty($this->session_id)){
			if(isset($_POST['idm']) && isset($_POST['ida'])){
				if($_POST['idm']!="" && $_POST['ida']!=""){
					$idm=trim($_POST['idm']);
					$ida=trim($_POST['ida']);
					$existe=$this->M_almacen_material->get_row_2n('idm',$idm,'ida',$ida);
					if(!empty($existe)){
						if($existe[0]->cantidad==0){
							if($this->M_almacen_material->eliminar($existe[0]->idam)){
								echo "0|ok";
							}else{
								echo "|error";
							}
						}else{
							echo "|fail";
						}
					}else{
						if($this->M_almacen_material->insertar($ida,$idm,0)){
							echo "1|ok";
						}else{
							echo "|error";
						}
					}
				}else{
					echo "|fail";
				}
			}else{
				echo "|fail";
			}
		}else{
			echo "|logout";
		}
	}

   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	public function config_imprimir_materiales($ida){
		if(!empty($this->session_id)){

			if(isset($_POST['json'])){
				$listado['materiales']=$_POST['json'];
				$this->load->view('almacen_mmpp/materia_prima/4-imprimir/config_view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function imprimir_materiales($ida){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				if(isset($_POST['v1'])){ if($_POST['v1']!="ok"){ $listado['v1']="ok";} }
				if(isset($_POST['v2'])){ if($_POST['v2']!="ok"){ $listado['v2']="ok";} }
				if(isset($_POST['v3'])){ if($_POST['v3']!="ok"){ $listado['v3']="ok";} }
				if(isset($_POST['v4'])){ if($_POST['v4']!="ok"){ $listado['v4']="ok";} }
				if(isset($_POST['v5'])){ if($_POST['v5']!="ok"){ $listado['v5']="ok";} }
				if(isset($_POST['v6'])){ if($_POST['v6']!="ok"){ $listado['v6']="ok";} }
				if(isset($_POST['v7'])){ if($_POST['v7']!="ok"){ $listado['v7']="ok";} }
				if(isset($_POST['v8'])){ if($_POST['v8']!="ok"){ $listado['v8']="ok";} }
				if(isset($_POST['v9'])){ if($_POST['v9']!="ok"){ $listado['v9']="ok";} }
				if(isset($_POST['v10'])){ if($_POST['v10']!="ok"){ $listado['v10']="ok";} }
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=34;}
				$almacen=$this->M_almacen->get($ida);
				if(!empty($almacen)){
					$listado['materiales']=$_POST['json'];
					$listado['almacen']=$almacen[0];
					$this->load->view('almacen_mmpp/materia_prima/4-imprimir/view',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	public function detalle_material($almacen){
		if(!empty($this->session_id)){
			$idmi=$_POST['idmi'];
			$listado['material']=$this->M_almacen_material->get_material_idmi($almacen,$idmi);
			$this->load->view('almacen_mmpp/materia_prima/5-reportes/view',$listado);
		}else{
			echo "logout";
		}
	}

   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	public function form_update_material($almacen){
		if(!empty($this->session_id)){
			$idmi=$_POST['idmi'];
			$listado['grupo']=$this->M_grupo->get_all();
			$listado['unidad']=$this->M_unidad->get_all();
			$listado['color']=$this->M_color->get_all();
			$proveedores=$this->M_material_proveedor->get_row('idmi',$idmi);
			if(count($proveedores)<=1){
				if(count($proveedores)==1){
					$listado['proveedor']=$proveedores[0];
				}
				$listado['proveedores']=$this->M_proveedor->get_all();
			}
			$listado['material']=$this->M_almacen_material->get_material_idmi($almacen,$idmi);
			$listado['idmi']=$idmi;
			$this->load->view('almacen_mmpp/materia_prima/6-configuracion/modificar',$listado);
		}else{
			echo "logout";
		}
	}
	public function update_material(){
		if(!empty($this->session_id)){
			if(isset($_FILES) && isset($_POST['idmi']) && isset($_POST['nom']) && isset($_POST['cod']) && isset($_POST['gru']) && isset($_POST['med']) && isset($_POST['col']) && isset($_POST['des'])){
				$idmi=trim($_POST['idmi']);
				$nom=trim($_POST['nom']);
				$cod=trim($_POST['cod']);
				$gru=trim($_POST['gru']);
				$med=trim($_POST['med']);
				$col=trim($_POST['col']);
				$des=trim($_POST['des']);
				$pro=trim($_POST['pro']);
				$cos_pro=trim($_POST['cos_pro']);
				$material_item=$this->M_material_item->get($idmi);
				if(!empty($material_item)){
					if($this->val->entero($gru,0,10) && $this->val->strSpace($cod,2,15) && $this->val->strSpace($nom,3,200) && $this->val->entero($med,0,10) && $this->val->entero($col,0,10)){
						$img=$material_item[0]->fotografia;
						$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/materiales/','',$this->resize,$img,$idmi);
						if($img!="error_type" && $img!="error" && $img!="error_size_img"){
							if($this->M_material_item->modificar($idmi,$med,$cod,$nom,$img,$des)){
								if($this->M_material->modificar_idmi($idmi,$gru,$col)){
									//buscando si el material tiene un proveedor
									$control=$this->M_material_proveedor->get_row('idmi',$idmi);
									if(empty($control)){
										if($pro!="" && $cos_pro!=""){
											if($this->M_material_proveedor->insertar($pro,$idmi,$cos_pro,date('Y-m-d'))){
												if($this->M_material->modificar_idmi_row($idmi,'costo_unitario',$cos_pro)){
													echo "ok";
												}else{
													echo "error";
												}
											}else{
												echo "error";
											}
										}else{
											echo "ok";
										}
									}else{
										if(count($control)==1){
											if($this->M_material_proveedor->modificar($control[0]->idmp,$pro,$idmi,$cos_pro)){
												if($this->M_material->modificar_idmi_row($idmi,'costo_unitario',$cos_pro)){
													echo "ok";
												}else{
													echo "error";
												}
											}else{
												echo "error";
											}
										}else{
											echo "ok";
										}
									}
								}else{
									echo "error";
								}
							}else{
								echo "error";
							}
						}else{
							echo $img;
						}
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}	
			}else{
				echo "fail";
			}		
		}else{
			echo "logout";
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	public function confirmar_material($ida){
		if(!empty($this->session_id)){
			if(isset($_POST['idmi'])){
				$idmi=$_POST['idmi'];
				$url='./../../libraries/img/materiales/miniatura/';
				$material=$this->M_almacen_material->get_material_idmi($ida,$idmi);
				if(count($material)>0){
					$listado['titulo']="el material ".$material[0]->nombre;
					//buscando si el material se encuentra el varios almacenes
					$control=$this->M_almacen_material->get_row_2n('ida',$ida,'idm',$material[0]->idm);
					if(count($control)>1){//el material se encuentra en mas un almacen
						if($control[0]->cantidad>0){
							$listado['open_control']="true";
							$listado['control']="";
							$listado['desc']="Imposible Eliminar: El material debe estar con cantidad 0, vacie la cantidad de material hasta llegar a stock 0";	
						}else{
							$listado['desc']="Se eliminara el material del almacen";
						}
					}else{
						$control=$this->M_categoria_producto->get_row_producto('cp.idm',$material[0]->idm);
						if(!empty($control) || $material[0]->cantidad>0){
							$listado['open_control']="true";
							if(!empty($control)){
								$listado['control']=$control;
								$listado['desc']="Imposible Eliminar: El material esta siendo usado en los siguinetes productos:";
							}else{
								$listado['control']="";
								$listado['desc']="Imposible Eliminar: El material debe estar con cantidad 0, vacie la cantidad de material hasta llegar a stock 0";	
							}
						}else{
							// controlando si esta asignado a un proveedor
							$control=$this->M_material_proveedor->get_row('idmi',$idmi);//controla si el material esta asigando a un proveedor
							if(!empty($control)){
								$listado['desc']="Imposible Eliminar: El material se encuentra asignado a ".count($control)." proveedor(es).";
								$listado['open_control']="false";
								$listado['control']="";	
							}else{
								$listado['desc']="Tambien se eliminaran los lugares donde se relacione el material como: Materiales en el producto o Material Liquido";
							}
						}
					}
					$img='default.png';
					if($material[0]->fotografia!=NULL && $material[0]->fotografia!=""){ $img=$material[0]->fotografia; }
					$listado['img']=$url.$img;
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function drop_material($ida){
   		if(!empty($this->session_id)){
   			$idmi=$_POST['idmi'];
   			$u=$_POST['u'];
			$p=$_POST['p'];
			if($u==$this->session->userdata("login")){
				$usuario=$this->M_empleado->validate($u,$p);
				if(!empty($usuario)){
					if($idmi!=""){
						$material=$this->M_almacen_material->get_material_idmi($ida,$idmi);
						if(count($material)>0){
							$control=$this->M_almacen_material->get_row('idm',$material[0]->idm);
							if(count($control)>1){//el material se encuentra en mas un almacen
								if($this->M_almacen_material->eliminar($material[0]->idam)){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								$material_item=$this->M_material_item->get($idmi);
								if(!empty($material_item)){
									if($this->lib->eliminar_imagen($material_item[0]->fotografia,'./libraries/img/materiales/')){
					   					if($this->M_material_item->eliminar($idmi)){
					   						echo "ok";
					   					}else{
					   						echo "error";
					   					}
					   				}else{
					   					echo "error";
					   				}
								}else{
									echo "fail";
								}
							}
						}else{
							echo "fail";
						}
		   			}else{
		   				echo "fail";
		   			}
				}else{
					echo "validate";
				}
			}else{
				echo "validate";
			}
   		}else{
			echo "logout";
		}
   	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE MATERIAL -------*/
/*------- MANEJO DE INGRESO DE MATERIAL -------*/
	public function search_ingreso(){
		if(!empty($this->session_id)){
				$listado['grupo']=$this->M_grupo->get_all();
				$listado['unidad']=$this->M_unidad->get_all();
				$listado['color']=$this->M_color->get_all();
				$this->load->view('almacen_mmpp/ingresos/search',$listado);
		}else{
			echo "logout";
		}
	}
	public function view_ingreso($ida){
		if(!empty($this->session_id)){
			if(isset($_POST['nom']) && isset($_POST['can']) && isset($_POST['uni'])){
				$nom=$_POST['nom'];
				$can=$_POST['can'];
				$uni=$_POST['uni'];
					if ($nom!="") {
						$col="mi.nombre";$val=$nom;
					}else{
						if ($can!=""){
							$col="am.cantidad";$val=$can;
						}else{
							if ($uni!="") {
								$col="u.idu";$val=$uni;
							}else{
								$col="";$val="";
							}
						}
					}
				if($col!="" && $val!=""){
					$listado['materiales']=$this->M_almacen_material->get_material_search($ida,$col,$val);
				}else{
					$listado['materiales']=$this->M_almacen_material->get_material($ida);
				}
			}else{
				$listado['materiales']=$this->M_almacen_material->get_material($ida);
			}				
			$this->load->view('almacen_mmpp/ingresos/view',$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Nuevo ---*/
   	public function save_movimiento($ida){
		if(!empty($this->session_id)){
			if(isset($_POST['idam']) && isset($_POST['sto']) && isset($_POST['fech']) && isset($_POST['can']) && isset($_POST['obs'])){
				$idam=$_POST['idam'];
				$fech=str_replace('T', ' ', $_POST['fech']);
				$can=$_POST['can'];
				$obs=$_POST['obs'];
				$vfecha=explode(" ", $fech);
				if($this->val->entero($idam,0,10) && $this->val->fecha($vfecha[0]) && $can>0 && $this->val->decimal($can,10,7) && $this->val->textarea($obs,0,300)){
					$material=$this->M_almacen_material->get_material_idam($idam);
					if(!empty($material)){
						$stock=$material[0]->cantidad;
						$control=true;
						$ci="";
						$solicitante="";
						if(isset($_POST['emp'])){
							if($stock>=$can){
								$saldo=$stock-$can;
								$empleado=$this->M_empleado->get_empleado('e.ide',$_POST['emp']);
								if(!empty($empleado)){
									$ci=$empleado[0]->ci;
									$solicitante=$empleado[0]->nombre." ".$empleado[0]->nombre2." ".$empleado[0]->paterno;
								}else{
									$control=false;
								}
							}else{
								$control=false;
							}
						}else{
							$saldo=$stock+$can;
						}
						if($control){
							$almacen=$this->M_almacen->get_col($ida,'nombre');
							if($this->M_almacen_material->modificar_cantidad($idam, $saldo)){
								$usuario=$this->session->userdata('nombre')." ".$this->session->userdata('nombre2')." ".$this->session->userdata('paterno');
								$almacen=$this->M_almacen->get_col($ida,'nombre');
								if(isset($_POST['emp'])){
									$ing=0; $sal=$can;
								}else{
									$ing=$can; $sal="";
								}
								$resp=$this->M_movimiento_almacen->insertar($almacen[0]->nombre,'material',$usuario,$material[0]->codigo,$material[0]->nombre,$fech,$stock,$ing,$sal,$ci,$solicitante,$obs);
								echo "ok|".$saldo;
							}else{
								echo "error|0";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout|0";
		}
	}


   	/*--- End Nuevo ---*/
/*------- END MANEJO DE INGRESO DE MATERIAL -------*/
/*------- SALIDA DE MATERIAL -------*/
	public function search_salida(){
		if(!empty($this->session_id)){
			$listado['grupo']=$this->M_grupo->get_all();
			$listado['unidad']=$this->M_unidad->get_all();
			$listado['color']=$this->M_color->get_all();
			$this->load->view('almacen_mmpp/salidas/search',$listado);
		}else{
			echo "logout";
		}
	}
	public function view_salida($ida){
		if(!empty($this->session_id)){
			if(isset($_POST['nom']) && isset($_POST['can']) && isset($_POST['uni'])){
				$nom=$_POST['nom'];
				$can=$_POST['can'];
				$uni=$_POST['uni'];
					if($nom!=""){
						$col="mi.nombre";$val=$nom;
					}else{
						if ($can!=""){
							$col="am.cantidad";$val=$can;
						}else{
							if ($uni!="") {
								$col="u.idu";$val=$uni;
							}else{
								$col="";$val="";
							}
						}
					}
				if($col!="" && $val!=""){
					$listado['materiales']=$this->M_almacen_material->get_material_search($ida,$col,$val);
				}else{
					$listado['materiales']=$this->M_almacen_material->get_material($ida);
				}
			}else{
				$listado['materiales']=$this->M_almacen_material->get_material($ida);
			}
			$listado['empleados']=$this->M_empleado->get_all_order("p.nombre");			
			$this->load->view('almacen_mmpp/salidas/view',$listado);
		}else{
			echo "logout";
		}
	}
/*------- END SALIDA DE MATERIAL -------*/
/*------- MANEJO DE CONFIGURACIONES -------*/
	public function search_config(){
		if(!empty($this->session_id)){
			$this->load->view('almacen_mmpp/configuracion/search');
		}else{
			echo "logout";
		}
	}
	public function view_config(){
		if(!empty($this->session_id)){
			$listado['grupos']=$this->M_grupo->get_all();
			$listado['unidades']=$this->M_unidad->get_all();
			$listado['colores']=$this->M_color->get_all();
			$this->load->view('almacen_mmpp/configuracion/view',$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Manejo de unidad ---*/
	public function save_unidad(){
		if(!empty($this->session_id)){
			if(isset($_POST['nom']) && isset($_POST['abr']) && isset($_POST['equ']) && isset($_POST['des'])){
				$nom=$_POST['nom'];
				$abr=$_POST['abr'];
				$equ=$_POST['equ'];
				$des_equ=$_POST['des'];
				if($this->val->strSpace($nom,2,40) && $this->val->strSpace($abr,1,10)){
					if($this->M_unidad->insertar($nom,$abr,$equ,$des_equ)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}			
		}else{
			echo "logout";
		}
	}

	public function update_unidad(){
		if(!empty($this->session_id)){
			if(isset($_POST['idu']) && isset($_POST['nom']) && isset($_POST['abr']) && isset($_POST['equ']) && isset($_POST['des'])){
				$idu=$_POST['idu'];
				$nom=$_POST['nom'];
				$abr=$_POST['abr'];
				$equ=$_POST['equ'];
				$des_equ=$_POST['des'];
				if($this->val->strSpace($nom,2,40) && $this->val->strSpace($abr,1,10)){
					if($this->M_unidad->modificar($idu,$nom,$abr,$equ,$des_equ)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}	
		}else{
			echo "logout";
		}
	}
	public function drop_unidad(){
		if (!empty($this->session_id)) {
			if(isset($_POST['idu'])){
				$idu=$_POST['idu'];
				if($idu!=""){
					if($this->M_unidad->eliminar($idu)){
						echo "ok";
					}else{
						echo "error";
					}					
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}			
		}else{
			echo "logout";
		}
	}
   	/*--- End Manejo de unidad ---*/
	/*--- Manejo de color ---*/
	public function save_color(){
		if(!empty($this->session_id)){
			if(isset($_POST['nom']) && isset($_POST['cod'])){
				$nom=$_POST['nom'];
				$cod=$_POST['cod'];
				if($this->val->strSpace($nom,2,50) && $cod!=""){
					if($this->M_color->insertar($nom,$cod)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}			
		}else{
			echo "logout";
		}
	}

	public function update_color(){
		if(!empty($this->session_id)){
			if(isset($_POST['idco']) && isset($_POST['nom']) && isset($_POST['cod'])){
				$idco=$_POST['idco'];
				$nom=$_POST['nom'];
				$cod=$_POST['cod'];
				if($this->val->strSpace($nom,2,50) && $cod!=""){
					if($this->M_color->modificar($idco,$nom,$cod)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}	
		}else{
			echo "logout";
		}
	}
	public function drop_color(){
		if (!empty($this->session_id)) {
			if(isset($_POST['idco'])){
				$idco=$_POST['idco'];
				if($idco!=""){
					if($this->M_color->eliminar($idco)){
						echo "ok";
					}else{
						echo "error";
					}					
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}			
		}else{
			echo "logout";
		}
	}
   	/*--- End Manejo de color ---*/
	/*--- Manejo de grupo ---*/
	public function save_grupo(){
		if(!empty($this->session_id)){
			if(isset($_POST['nom']) && isset($_POST['des'])){
				$nom=$_POST['nom'];
				$des=$_POST['des'];
				if($this->val->strSpace($nom,2,50) && $this->val->textarea($des,0,200)){
					if($this->M_grupo->insertar($nom,$des)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}			
		}else{
			echo "logout";
		}
	}

	public function update_grupo(){
		if(!empty($this->session_id)){
			if(isset($_POST['idg']) && isset($_POST['nom']) && isset($_POST['des'])){
				$idg=$_POST['idg'];
				$nom=$_POST['nom'];
				$des=$_POST['des'];
				if($this->val->strSpace($nom,2,50) && $this->val->textarea($des,0,200)){
					if($this->M_grupo->modificar($idg,$nom,$des)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}	
		}else{
			echo "logout";
		}
	}
	public function drop_grupo(){
		if (!empty($this->session_id)) {
			if(isset($_POST['idg'])){
				$idg=$_POST['idg'];
				if($idg!=""){
					if($this->M_grupo->eliminar($idg)){
						echo "ok";
					}else{
						echo "error";
					}					
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}			
		}else{
			echo "logout";
		}
	}
   	/*--- End Manejo de grupo ---*/
/*------- END MANEJO DE CONFIGURACIONES -------*/
/*------- ALERTA ---------*/
	public function alerta(){
		if(!empty($this->session_id)){
			if(isset($_POST['titulo'])){
				$listado['titulo']=$_POST['titulo'];
			}else{
				$listado['titulo']="";
			}
			if(isset($_POST['descripcion'])){
				$listado['desc']=$_POST['descripcion'];
			}else{
				$listado['desc']="";
			}
			if(isset($_POST['img'])){
				$listado['img']=$_POST['img'];
			}else{
				$listado['img']=base_url().'libraries/img/sistema/warning.png';
			}
			$listado['control']="";
			$listado['open_control']="false";
			$this->load->view('estructura/form_eliminar',$listado);
		}else{
			echo "logout";
		}
	}
/*------- END ALERTA ---------*/




	
}
/* End of file material.php */
/* Location: ./application/controllers/material.php */