<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Activos_fijos extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('login');
	}
	public function index(){
		if(!empty($this->session_id)){
			if(!isset($_GET['p'])){
				$listado['pestania']=1;
			}else{
				$listado['pestania']=$_GET['p'];
			}
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('v_activo_fijo',$listado);
		}else{
			redirect(base_url().'login/input',301);
		}
	}
 /*------- MANEJO DE ACTIVO FIJO -------*/
	public function search_activo(){
		if(!empty($this->session_id)){
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$listado['depreciaciones']=$this->M_depreciacion->get_all();
			$this->load->view('activo_fijo/activo_fijo/search',$listado);
		}else{
			echo "logout";
		}
	}

	public function view_activo(){
		if(!empty($this->session_id)){
			if(isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['fec']) && isset($_POST['cos'])){
				if($_POST['cod']!=""){
					$atrib="mi.codigo";$val=$_POST['cod'];
				}else{
					if($_POST['nom']!=""){
						$atrib="mi.nombre";$val=$_POST['nom'];
					}else{
						if($_POST['fec']!=""){
							$atrib="a.fecha_inicio_trabajo";$val=$_POST['fec'];
						}else{
							if($_POST['cos']!=""){
								$atrib="a.costo";$val=$_POST['cos'];
							}else{
								$atrib="";$val="";
							}
						}
					}
				}
				if($atrib!="" && $val!=""){
					$listado['activos_fijos']=$this->M_activo_fijo->get_search($atrib,$val);
				}else{
					$listado['activos_fijos']=$this->M_activo_fijo->get_all_deprecicion();					
				}
			}else{
				$listado['activos_fijos']=$this->M_activo_fijo->get_all_deprecicion();
			}
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('activo_fijo/activo_fijo/view',$listado);
		}else{
			echo "logout";
		}
	}
	/*--- Nuevo ---*/
	public function new_activo(){
		if(!empty($this->session_id)){
			$listado['depreciaciones']=$this->M_depreciacion->get_all();
			$this->load->view('activo_fijo/activo_fijo/3-nuevo/form',$listado);
		}else{
			echo "logout";
		}
	}
	public function save_activo(){
		if(!empty($this->session_id)){
			if(isset($_POST['nom']) && isset($_POST['cod']) && isset($_POST['cos']) && isset($_POST['fec']) && isset($_POST['dep']) && isset($_POST['des'])){
				$cod=trim($_POST['cod'])."";
				$nom=trim($_POST['nom'])."";
				$cos=trim($_POST['cos'])."";
				$fec=trim($_POST['fec'])."";
				$dep=trim($_POST['dep'])."";
				$des=trim($_POST['des'])."";
				$aux=$this->M_depreciacion->get($dep);
				if($this->val->strSpace($nom,3,200) && $this->val->strNoSpace($cod,2,15) && $this->val->decimal($cos,5,1) && $this->val->fecha($fec) && count($aux)>0){
						$control=true;
						if($des!=""){ if(!$this->val->strSpace($des,5,700)){ $control=false;}}
						if($control){
							$img=$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/activos_fijos/','',$this->resize,'');
							if($img!="error_type" && $img!="error"){
								$idmi=$this->M_material_item->max('idmi');
								$idmi=$idmi[0]->max+1;
								if($this->M_material_item->insertar($idmi,1,$cod,$nom,$img,$des)){
									if($this->M_activo_fijo->insertar($idmi,$dep,$cos,$fec)){
										echo "ok";
									}else{
										if($this->M_material_item->eliminar($idmi)){}
										if($this->lib->eliminar_imagen($img,'./libraries/img/activos_fijos/')){}
										echo "error";
									}
								}else{
									if($this->lib->eliminar_imagen($img,'./libraries/img/activos_fijos/')){}
									echo "error";
								}
							}else{
								echo $img;
							}
						}else{
							echo "fail";
						}
				}else{
					echo "fail $cod";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	/*--- End Nuevo ---*/
	/*--- Imprimir ---*/
   	public function config_imprimir_activos(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				$listado['activos']=$_POST['json'];
				$this->load->view('activo_fijo/activo_fijo/4-imprimir/config',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function imprimir_activos(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				if(isset($_POST['v1'])){ if($_POST['v1']!="ok"){ $listado['v1']="ok";} }
				if(isset($_POST['v2'])){ if($_POST['v2']!="ok"){ $listado['v2']="ok";} }
				if(isset($_POST['v3'])){ if($_POST['v3']!="ok"){ $listado['v3']="ok";} }
				if(isset($_POST['v4'])){ if($_POST['v4']!="ok"){ $listado['v4']="ok";} }
				if(isset($_POST['v5'])){ if($_POST['v5']!="ok"){ $listado['v5']="ok";} }
				if(isset($_POST['v6'])){ if($_POST['v6']!="ok"){ $listado['v6']="ok";} }
				if(isset($_POST['v7'])){ if($_POST['v7']!="ok"){ $listado['v7']="ok";} }
				if(isset($_POST['v8'])){ if($_POST['v8']!="ok"){ $listado['v8']="ok";} }
				if(isset($_POST['v9'])){ if($_POST['v9']!="ok"){ $listado['v9']="ok";} }
				if(isset($_POST['v10'])){ if($_POST['v10']!="ok"){ $listado['v10']="ok"; } }
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=34;}
				$listado['activos']=$_POST['json'];
				$this->load->view('activo_fijo/activo_fijo/4-imprimir/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	/*--- End Imprimir ---*/
	/*--- Reportes ---*/
	public function detalle_activo(){
		if(!empty($this->session_id)){
			if(isset($_POST['id'])){
				$id=$_POST['id'];
				$listado['activo_fijo']=$this->M_activo_fijo->get_depreciacion($id);
				$this->load->view('activo_fijo/activo_fijo/5-reportes/detalle',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	/*--- End Reportes ---*/
	/*--- Configuracion ---*/
	public function form_update_activo(){
		if(!empty($this->session_id)){
			if(isset($_POST['id'])){
				$id=$_POST['id'];
				$listado['activo_fijo']=$this->M_activo_fijo->get_depreciacion($id);
				$listado['depreciaciones']=$this->M_depreciacion->get_all();
				$listado['idaf']=$id;
				$this->load->view('activo_fijo/activo_fijo/6-configuracion/form_modificar',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_activo(){
		if(!empty($this->session_id)){
			if(isset($_POST['id']) && isset($_POST['nom']) && isset($_POST['cod']) && isset($_POST['cos']) && isset($_POST['fec']) && isset($_POST['dep']) && isset($_POST['des'])){
				$idaf=trim($_POST['id']);
				$cod=trim($_POST['cod']);
				$nom=trim($_POST['nom']);
				$cos=trim($_POST['cos']);
				$fec=trim($_POST['fec']);
				$dep=trim($_POST['dep']);
				$des=trim($_POST['des']);
				$aux=$this->M_depreciacion->get($dep);
				if($this->val->strSpace($nom,3,200) && $this->val->strNoSpace($cod,2,15) && $this->val->decimal($cos,5,1) && $this->val->fecha($fec) && count($aux)>0){
						$control=true;
						if($des!=""){ if(!$this->val->strSpace($des,5,700)){ $control=false;}}
						if($control){
							$aux=$this->M_activo_fijo->get_depreciacion($idaf);
							if(!empty($aux)){
								$fot=$aux[0]->fotografia;
								$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/activos_fijos/','',$this->resize,$fot,$aux[0]->idaf);
								if($img!="error_type" && $img!="error"){
									$idmi=$aux[0]->idmi;
									if($this->M_material_item->modificar($idmi,1,$cod,$nom,$img,$des)){
										if($this->M_activo_fijo->modificar($idaf,$idmi,$dep,$cos,$fec)){
											echo "ok";
										}else{
											if($this->M_material_item->eliminar($idmi)){}
											if($this->lib->eliminar_imagen($img,'./libraries/img/activos_fijos/')){}
											echo "error";
										}
									}else{
										if($this->lib->eliminar_imagen($img,'./libraries/img/activos_fijos/')){}
										echo "error";
									}
								}else{
									echo $img;
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	/*--- End Configuracion ---*/
   	/*--- Eliminar ---*/
   	public function confirmar(){
		if(!empty($this->session_id)){
			if(isset($_POST['idaf'])){
				$idaf=$_POST['idaf'];
				$url='./libraries/img/activos_fijos/miniatura/';
				$activo=$this->M_activo_fijo->get_depreciacion($idaf);
				if(!empty($activo)){
					$control=$this->M_material_proveedor->get_row('idmi',$activo[0]->idmi);//controla si el activo fijo esta asigando a un proveedor
					if(!empty($control)){
						$listado['desc']="Imposible Eliminar: El activo fijo se encuentra asigando a ".count($control)." proveedor(es).";
						$listado['open_control']="false";
						$listado['control']="";	
					}else{
						$listado['desc']="Se eliminara definitivamente el activo fijo.";
					}
					$img='default.png';
					if($activo[0]->fotografia!=NULL && $activo[0]->fotografia!=""){ $img=$activo[0]->fotografia; }
					$listado['titulo']="el activo fijo <b>".$activo[0]->nombre."</b>";
					$listado['img']=$url.$img;
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function drop_activo(){
		if(!empty($this->session_id)){
			if(isset($_POST['idaf']) && isset($_POST['u']) && isset($_POST['p'])){
				$id=$_POST['idaf'];
				$u=$_POST['u'];
				$p=$_POST['p'];
				if($u==$this->session->userdata("login")){
					$usuario=$this->M_empleado->validate($u,$p);
					if(!empty($usuario)){
						$activo=$this->M_activo_fijo->get_depreciacion($id);
						if(!empty($activo)){
							if($this->lib->eliminar_imagen($activo[0]->fotografia,'./libraries/img/activos_fijos/')){
								if($this->M_material_item->eliminar($activo[0]->idmi)){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "validate";
					}
				}else{
					echo "validate";
				}
			}else{
				echo "fail";
			}			
		}else{
			echo "logout";
		}
	}
   	/*--- End Eliminar ---*/
 /*------- END MANEJO DE ACTIVO FIJO -------*/
/*------- MANEJO DE CONFIGURACION -------*/
	public function view_config(){
		if(!empty($this->session_id)){
			$listado['depreciaciones']=$this->M_depreciacion->get_all();
			$this->load->view('activo_fijo/configuracion/view',$listado);
		}else{
			echo "logout";
		}
	}
	/*--- activos fijos depreciciones ---*/
	public function save_depreciacion(){
		if(!empty($this->session_id)){
			if(isset($_POST['nom']) && isset($_POST['anio']) && isset($_POST['coe'])){
				$nom=$_POST['nom'];
				$anio=$_POST['anio'];
				$coe=$_POST['coe'];
				if($this->val->strSpace($nom,2,100) && $this->val->decimal($anio,3,1) && $anio<=999.9 && $this->val->decimal($coe,3,2) && $coe<=999.99){
					$coe=$coe/100;
					if($this->M_depreciacion->insertar($nom,$anio,$coe,"")){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_depreciacion(){
		if(!empty($this->session_id)){
			if(isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['anio']) && isset($_POST['coe'])){
				$id=$_POST['cod'];
				$nom=$_POST['nom'];
				$anio=$_POST['anio'];
				$coe=$_POST['coe'];
				if($this->val->strSpace($nom,2,100) && $this->val->decimal($anio,3,1) && $anio<=999.9 && $this->val->decimal($coe,3,2) && $coe<=999.99){
					$coe=$coe/100;
					if($this->M_depreciacion->modificar($id,$nom,$anio,$coe,"")){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function delete_depreciacion(){
		if(!empty($this->session_id)){
			$id=$_POST['cod'];
			if($this->M_depreciacion->eliminar($id)){
				echo "ok";
			}else{
				echo "error";
			}
		}else{
			echo "logout";
		}
	}
	/*--- end activos fijos depreciciones ---*/
/*------- END MANEJO DE CONFIGURACION -------*/
/*------- ALERTA ---------*/
	public function alerta(){
		if(!empty($this->session_id)){
			if(isset($_POST['titulo'])){
				$listado['titulo']=$_POST['titulo'];
			}else{
				$listado['titulo']="";
			}
			if(isset($_POST['descripcion'])){
				$listado['desc']=$_POST['descripcion'];
			}else{
				$listado['desc']="";
			}
			if(isset($_POST['img'])){
				$listado['img']=$_POST['img'];
			}else{
				$listado['img']=base_url().'libraries/img/sistema/warning.png';
			}
			$listado['control']="";
			$listado['open_control']="false";
			$this->load->view('estructura/form_eliminar',$listado);
		}else{
			echo "logout";
		}
	}
/*------- END ALERTA ---------*/

}
/* End of file activo_fijo.php */
/* Location: ./application/controllers/activo_fijo.php */