<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cliente_proveedor extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('login');
	}
	public function index(){
		if(!empty($this->session_id)){
			$privilegio=$privilegio=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			if(!isset($_GET['p'])){
				if($privilegio[0]->cl1r==1){
					$listado['pestania']=1;
				}else{
					if($privilegio[0]->cl2r==1){ 
						$listado['pestania']=2;
					}else{
						if($privilegio[0]->cl3r==1){ 
							$listado['pestania']=3;
						}else{
							$listado['pestania']=0;
						}
					}
				}
			}else{
				$listado['pestania']=$_GET['p'];
			}
			$listado['privilegio']=$privilegio;
			$this->load->view('v_cliente_proveedor',$listado);	
		}else{
			redirect(base_url().'login',301);
		}
	}
/*------- MANEJO DE CLIENTES -------*/
	public function search_cliente(){
		if(!empty($this->session_id)){
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('cliente_proveedor/cliente/search',$listado);
		}else{
			echo "logout";
		}
	}
	public function view_cliente(){
		if(!empty($this->session_id)){
			if(isset($_POST['nit']) && isset($_POST['nom']) && isset($_POST['resp'])  && isset($_POST['telf']) && isset($_POST['dir'])){//si tenemos busqueda 
				$nit=$_POST['nit'];
				$nom=$_POST['nom'];
				$resp=$_POST['resp'];
				$telf=$_POST['telf'];
				$dir=$_POST['dir'];
				if($nit!=""){
					$atrib="c.nit";$val=$nit;
				}else{
					if($nom!=""){
						$atrib="p.nombre";$val=$nom;
					}else{
						if($resp!=""){
							$atrib="c.encargado";$val=$resp;
						}else{
							if($telf!=""){
								$atrib="p.telefono";$val=$telf;
							}else{
								if($dir!=""){
									$atrib="p.direccion";$val=$dir;
								}else{
									$atrib="";$cod="";
								}
							}
						}
					}
				}
				if($atrib!="" && $val!=""){
					$listado['clientes']=$this->M_cliente->get_search($atrib,$val);
				}else{
					$listado['clientes']=$this->M_cliente->get_all();
				}
			}else{
				$listado['clientes']=$this->M_cliente->get_all();
			}
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('cliente_proveedor/cliente/view',$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Buscador ---*/
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
	public function new_cliente(){
		if(!empty($this->session_id)){
			$listado['paises']=$this->M_pais->get_all();
			$this->load->view('cliente_proveedor/cliente/3-nuevo/form',$listado);
		}else{
			echo "logout";
		}
	}
	public function view_ciudades(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpa'])){
				$ciudades=$this->M_ciudad->get_row('idpa',$_POST['idpa']);
				$res="<option value=''>Seleccionar...</option>";
				for ($i=0; $i < count($ciudades); $i++) { $ciudad=$ciudades[$i];
					$res.="<option value='".$ciudad->idci."'>".$ciudad->nombre."</option>";
				}
				echo $res;
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function search_ci(){
		if(!empty($this->session_id)){
			if(isset($_POST['ci'])){
				$ci=$_POST['ci'];
				if($ci!=""){
					$p=$this->M_persona->get($ci);
					if(count($p)>0){
						$control=$this->M_ciudad->get_pais($p[0]->idci);
						if(!empty($control)){
							$paises=$this->M_pais->get_all();
							$ciudades=$this->M_ciudad->get_row('idpa',$control[0]->idpa);
							$option_pais="<option value=''>Seleccionar...</option>";
							for($pa=0;$pa<count($paises);$pa++){ $option_pais.="<option value='".$paises[$pa]->idpa."'>".$paises[$pa]->nombre."</option>"; }
							$option_pais.="<option value='".$control[0]->idpa."' selected>".$control[0]->nombre_pais."</option>";
							$option_ciudad="<option value=''>Seleccionar...</option>";
							for($ci=0;$ci<count($ciudades);$ci++){ $option_ciudad.="<option value='".$ciudades[$ci]->idci."'>".$ciudades[$ci]->nombre."</option>"; }
							$option_ciudad.="<option value='".$control[0]->idci."' selected>".$control[0]->nombre_ciudad."</option>";
							echo $p[0]->nombre."|".$p[0]->telefono."|".$p[0]->email."|".$option_pais."|".$option_ciudad."|".$p[0]->direccion."|".$p[0]->caracteristicas;							
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function save_cliente(){
		if(!empty($this->session_id)){
			if(isset($_POST['nit']) && isset($_POST['raz']) && isset($_POST['res']) && isset($_POST['tel']) && isset($_POST['ema']) && isset($_POST['pai']) && isset($_POST['ciu']) && isset($_POST['web']) && isset($_POST['dir']) && isset($_POST['obs'])){
				$nit=trim($_POST['nit']);
				$raz=trim($_POST['raz']);
				$res=trim($_POST['res']);
				$tel=trim($_POST['tel']);
				$ema=trim($_POST['ema']);
				$pai=trim($_POST['pai']);
				$ciu=trim($_POST['ciu']);
				$web=trim($_POST['web']);
				$dir=trim($_POST['dir']);
				$obs=trim($_POST['obs']);
				if($this->validaciones->esEntero($nit,6,25) && $this->validaciones->esPalabraConEspacio($raz,2,100) && $ciu!=""){
					$control=$this->M_persona->get($nit);
					if(empty($control)){
						$img=$this->validaciones->subir_imagen_miniatura($_FILES,'./libraries/img/personas/','',$this->resize,$nit);
						if($img!='error' && $img!="error_type"){
							if($this->M_persona->insertar($nit,$ciu,$raz,$tel,$ema,$dir,$img,$obs)){
								if($this->M_cliente->insertar($nit,$res,$web)){
									echo "ok";
								}else{
									$this->M_persona->eliminar($nit);
									echo "error";
								}
							}else{
								echo "error";
							}
						}else{
							echo $img;
						}

					}else{
						$p=$control[0];
						if($p->nombre==$raz && $p->telefono==$tel && $p->email==$ema && $p->direccion==$dir && $p->caracteristicas==$obs){
							$control=$this->M_cliente->get_cliente_row('nit',$nit);
							if(empty($control)){
								if($this->M_cliente->insertar($p->ci,$res,$web)){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "ci_exist";//cliente ya registrado
							}
						}else{
							echo "ci_exist";
						}
					}
				}else{
					echo "fail";
				}

			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	public function config_imprimir_clientes(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				$listado['clientes']=$_POST['json'];
				$this->load->view('cliente_proveedor/cliente/4-imprimir/config',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function imprimir_clientes(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				if(isset($_POST['v1'])){ if($_POST['v1']!="ok"){ $listado['v1']="ok";} }
				if(isset($_POST['v2'])){ if($_POST['v2']!="ok"){ $listado['v2']="ok";} }
				if(isset($_POST['v3'])){ if($_POST['v3']!="ok"){ $listado['v3']="ok";} }
				if(isset($_POST['v4'])){ if($_POST['v4']!="ok"){ $listado['v4']="ok";} }
				if(isset($_POST['v5'])){ if($_POST['v5']!="ok"){ $listado['v5']="ok";} }
				if(isset($_POST['v6'])){ if($_POST['v6']!="ok"){ $listado['v6']="ok";} }
				if(isset($_POST['v7'])){ if($_POST['v7']!="ok"){ $listado['v7']="ok";} }
				if(isset($_POST['v8'])){ if($_POST['v8']!="ok"){ $listado['v8']="ok";} }
				if(isset($_POST['v9'])){ if($_POST['v9']!="ok"){ $listado['v9']="ok";} }
				if(isset($_POST['v10'])){ if($_POST['v10']!="ok"){ $listado['v10']="ok"; } }
				if(isset($_POST['v11'])){ if($_POST['v11']!="ok"){ $listado['v11']="ok"; } }
				if(isset($_POST['v12'])){ if($_POST['v12']!="ok"){ $listado['v12']="ok"; } }
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=34;}
				$listado['clientes']=$_POST['json'];
				$this->load->view('cliente_proveedor/cliente/4-imprimir/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	public function detalle_cliente(){
		if(!empty($this->session_id)){
			$idcl=$_POST['idcl'];
			$listado['cliente']=$this->M_cliente->get($idcl);
			$this->load->view('cliente_proveedor/cliente/5-reportes/detalle_cliente',$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	public function form_update_cliente(){
		if(!empty($this->session_id)){
			if(isset($_POST['idcl'])){
				$idcl=$_POST['idcl'];
				$cliente=$this->M_cliente->get($idcl);
				if(!empty($cliente)){
					$ciudades=$this->M_ciudad->get_row('idpa',$cliente[0]->idpa);
					$listado['cliente']=$cliente;
					$listado['ciudades']=$ciudades;
					$listado['paises']=$this->M_pais->get_all();
					$listado['idcl']=$idcl;
					$this->load->view('cliente_proveedor/cliente/6-configuracion/modificar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_cliente(){
		if(!empty($this->session_id)){
			if(isset($_POST['idcl']) && isset($_POST['nit']) && isset($_POST['raz']) && isset($_POST['res']) && isset($_POST['tel']) && isset($_POST['ema']) && isset($_POST['pai']) && isset($_POST['ciu']) && isset($_POST['web']) && isset($_POST['dir']) && isset($_POST['obs'])){
				$idcl=trim($_POST['idcl']);
				$nit=trim($_POST['nit']);
				$raz=trim($_POST['raz']);
				$res=trim($_POST['res']);
				$tel=trim($_POST['tel']);
				$ema=trim($_POST['ema']);
				$pai=trim($_POST['pai']);
				$ciu=trim($_POST['ciu']);
				$web=trim($_POST['web']);
				$dir=trim($_POST['dir']);
				$obs=trim($_POST['obs']);
				if($this->validaciones->esEntero($nit,6,25) && $this->validaciones->esPalabraConEspacio($raz,2,100) && $ciu!="" && $idcl!=""){
					$control=$this->M_cliente->get($idcl);
					if(!empty($control)){
						$guardar=true;//controlar si ya se encuentra registrado el ci o nit
						if($nit!=$control[0]->nit){
							$aux=$this->M_persona->get($nit);
							if(!empty($aux)){
								$guardar=false;
							}
						}
						if($guardar){
							$img=$control[0]->fotografia;
							$img=$this->validaciones->cambiar_imagen_miniatura($_FILES,'./libraries/img/personas/','',$this->resize,$img,$idcl.''.$control[0]->ci);//
							if($img!='error' && $img!="error_type"){
								if($this->M_persona->modificar($control[0]->ci,$nit,$ciu,$raz,$tel,$ema,$dir,$img,$obs)){
									if($this->M_cliente->modificar($idcl,$nit,$res,$web)){
										echo "ok";
									}else{
										echo "error";
									}
								}else{
									echo "error";
								}
							}else{
								echo $img;
							}
						}else{
							echo "ci_exist";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	//modificar producto cliente
	public function productos_cliente(){
		if(!empty($this->session_id)){
			$idcl=$_POST['idcl'];
			$cliente=$this->M_cliente->get($idcl);
			if(!empty($cliente)){
				$listado['cliente']=$cliente;
				$listado['utilidad_ventas']=$this->M_utilidad_venta->get_all();
				$listado['productos']=$this->M_producto_cliente->get_productos($idcl);
				$this->load->view('cliente_proveedor/cliente/6-configuracion/productos',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function search_productos_cliente(){
		if(!empty($this->session_id)){
			$idcl=$_POST['idcl'];
			$listado['idcl']=$idcl;
			$this->load->view('cliente_proveedor/cliente/6-configuracion/productos/search',$listado);
		}else{
			echo "logout";
		}
	}
	public function view_productos_cliente(){
		if(!empty($this->session_id)){
			$idcl=$_POST['idcl'];
			if(isset($_POST['cod']) && isset($_POST['nom'])){
				if($_POST['cod']!=""){
					$atrib='p.cod';$val=$_POST['cod'];
				}else{
					if($_POST['nom']!=""){
						$atrib='p.nombre';$val=$_POST['nom'];
					}else{
						$atrib="";$val="";
					}
				}
				if($atrib=="" && $val==""){
					$listado['productos']=$this->M_categoria_pieza->get_categoria_color_producto(1);	
				}else{
					$listado['productos']=$this->M_categoria_pieza->get_where_categoria_color_producto($atrib,$val,1);
				}
			}else{
				$listado['productos']=$this->M_categoria_pieza->get_categoria_color_producto(1);
			}
			$listado['utilidad_ventas']=$this->M_utilidad_venta->get_all();
			$listado['idcl']=$idcl;
			$this->load->view('cliente_proveedor/cliente/6-configuracion/productos/view',$listado);
		}else{
			echo "logout";
		}
	}
	public function save_producto_cliente(){
		if(!empty($this->session_id)){
			if(isset($_POST['idcl']) && isset($_POST['idpim']) && isset($_POST['iduv'])){
				$idcl=$_POST['idcl'];
				$idpim=$_POST['idpim'];
				$iduv=$_POST['iduv'];
				if ($idcl!="" && $idpim!="" && $iduv!=""){
					$existe=$this->M_producto_cliente->get_row_2n('idcl',$idcl,'idpim',$idpim);
					if(count($existe)>0){//desactivar
						if($this->M_producto_cliente->eliminar($existe[0]->idpc)){
							echo "0|ok";
						}else{
							echo "|fail";
						}
					}else{//activar boton
						if($this->M_producto_cliente->insertar($idcl,$idpim,$iduv)){
							echo "1|ok";
						}else{
							echo "|fail";
						}
					}
				}else{
					echo "|fail";
				}
			}else{
				echo "|fail";
			}			
		}else{
			echo "|logout";
		}
	}
	public function update_presio_venta(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpc']) && isset($_POST['cos']) && isset($_POST['uv'])){
				$idpc=$_POST['idpc'];
				$cos=$_POST['cos'];
				$uv=$_POST['uv'];
				if($idpc!="" && $cos!="" && $uv!=""){
					if($this->M_producto_cliente->modificar_row_2n($idpc,'costo_unitario',$cos,'iduv',$uv)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function confirmar_producto_cliente(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpc']) && isset($_POST['img']) && isset($_POST['nom'])){
				$idpc=$_POST['idpc'];
				$pimg=$_POST['img'];
				$nom=$_POST['nom'];
				$listado['titulo']=" el producto <strong>".$nom."</strong> asignado al cliente";
				$listado['desc']="el registro se eliminara definitivamente";
				$img="default.png";
				$url=base_url().'libraries/img/pieza_productos/miniatura/';
				if($pimg!="default.png"){ $img=$pimg; }
				$listado['img']=$url.$img;
				$this->load->view('estructura/form_eliminar',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function drop_producto_cliente(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpc']) && isset($_POST['u']) && isset($_POST['p'])){
				$idpc=$_POST['idpc'];
				$u=$_POST['u'];
				$p=$_POST['p'];
				if($u==$this->session->userdata("login")){
					$usuario=$this->M_empleado->validate($u,$p);
					if(!empty($usuario)){
						if($idpc!=""){
							if($this->M_producto_cliente->eliminar($idpc)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "validate";
					}
				}else{
					echo "validate";
				}
			}else{
				echo "fail";
			}			
		}else{
			echo "logout";
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	public function confirmar(){
   		if(!empty($this->session_id)){
   			if(isset($_POST['idcl'])){
				$idcl=$_POST['idcl'];
				$url="./libraries/img/personas/miniatura/";
				$cliente=$this->M_cliente->get($idcl);
				if(count($cliente)>0){
					$listado['titulo']="el cliente".$cliente[0]->nombre;
					$control=$this->M_pedido->get_row('idcl',$cliente[0]->idcl);
					if(count($control)>0){//el cliente se encuentra en uso el los pedidos
						$listado['open_control']="true";
						$listado['control']="";
						$listado['desc']="Imposible Eliminar: El cliente actualmente se encuentra en uso de ".count($control)." pedido(s).";	
					}else{
						$listado['desc']="Tambien se eliminaran los costos de venta de productos por cliente.";
					}
					$img='default.png';
					if($cliente[0]->fotografia!=NULL && $cliente[0]->fotografia!=""){ $img=$cliente[0]->fotografia; }
					$listado['img']=$url.$img;
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
   		}else{
   			echo "logout";
   		}
   	}

   	public function drop_cliente(){
		if(!empty($this->session_id)){
			if(isset($_POST['idcl']) && isset($_POST['u']) && isset($_POST['p']) ){
				$id=$_POST['idcl'];
				$u=$_POST['u'];
				$p=$_POST['p'];
				if($id!=""){
					if($u==$this->session->userdata("login")){
						$usuario=$this->M_empleado->validate($u,$p);
						if(!empty($usuario)){
							if($id!=""){
								$cliente=$this->M_cliente->get($id);
								if(!empty($cliente)){
									//verificando si existe en empleado o proveedor
									$empleado=$this->M_empleado->get_row('ci',$cliente[0]->ci);
									$proveedor=$this->M_proveedor->get_row('nit',$cliente[0]->ci);
									if(count($empleado)>0 || count($proveedor)>0){//existe, borramos solo a cliente
										if($this->M_cliente->eliminar($cliente[0]->idcl)){
											echo "ok";
										}else{
											echo "error";
										}
									}else{//no existe, borramos persona y su fotografia
										if($this->validaciones->eliminar_imagen($cliente[0]->fotografia,'./libraries/img/personas/')){
											if($this->M_persona->eliminar($cliente[0]->ci)){
												echo "ok";
											}else{
												echo "error";
											}
										}else{
											echo "error";
										}	
									}
								}else{
									echo "fail";
								}							
							}else{
								echo "fail";
							}
						}else{
							echo "validate";
						}
					}else{
						echo "validate";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}			
		}else{
			echo "logout";
		}
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE CLIENTES -------*/
/*------- MANEJO DE PROVEEDOR -------*/
	public function search_proveedor(){
		if(!empty($this->session_id)){
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('cliente_proveedor/proveedor/search',$listado);
		}else{
			echo "logout";
		}
	}
	public function view_proveedor(){
		if(!empty($this->session_id)){
			if(isset($_POST['nit']) && isset($_POST['nom']) && isset($_POST['resp'])  && isset($_POST['telf']) && isset($_POST['dir'])){//si tenemos busqueda 
				$nit=$_POST['nit'];
				$nom=$_POST['nom'];
				$resp=$_POST['resp'];
				$telf=$_POST['telf'];
				$dir=$_POST['dir'];
				if($nit!=""){
					$atrib="pr.nit";$val=$nit;
				}else{
					if($nom!=""){
						$atrib="p.nombre";$val=$nom;
					}else{
						if($resp!=""){
							$atrib="pr.encargado";$val=$resp;
						}else{
							if($telf!=""){
								$atrib="p.telefono";$val=$telf;
							}else{
								if($dir!=""){
									$atrib="p.direccion";$val=$dir;
								}else{
									$atrib="";$cod="";
								}
							}
						}
					}
				}
				if($atrib!="" && $val!=""){
					$listado['proveedores']=$this->M_proveedor->get_search($atrib,$val);
				}else{
					$listado['proveedores']=$this->M_proveedor->get_all();
				}
			}else{
				$listado['proveedores']=$this->M_proveedor->get_all();
			}
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('cliente_proveedor/proveedor/view',$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Buscador ---*/

   	/*--- End Buscador ---*/
   	/*--- Nuevo ---*/
   	public function new_proveedor(){
		if(!empty($this->session_id)){
			$listado['paises']=$this->M_pais->get_all();
			$this->load->view('cliente_proveedor/proveedor/3-nuevo/form',$listado);
		}else{
			echo "logout";
		}
	}
	public function save_proveedor(){
		if(!empty($this->session_id)){
			if(isset($_POST['nit']) && isset($_POST['raz']) && isset($_POST['res']) && isset($_POST['tel']) && isset($_POST['ema']) && isset($_POST['pai']) && isset($_POST['ciu']) && isset($_POST['web']) && isset($_POST['dir']) && isset($_POST['obs'])){
				$nit=trim($_POST['nit']);
				$raz=trim($_POST['raz']);
				$res=trim($_POST['res']);
				$tel=trim($_POST['tel']);
				$ema=trim($_POST['ema']);
				$pai=trim($_POST['pai']);
				$ciu=trim($_POST['ciu']);
				$web=trim($_POST['web']);
				$dir=trim($_POST['dir']);
				$obs=trim($_POST['obs']);
				if($this->validaciones->esEntero($nit,6,25) && $this->validaciones->esPalabraConEspacio($raz,2,100) && $ciu!=""){
					$control=$this->M_persona->get($nit);
					if(empty($control)){
						$img=$this->validaciones->subir_imagen_miniatura($_FILES,'./libraries/img/personas/','',$this->resize,$nit);
						if($img!='error' && $img!="error_type"){
							if($this->M_persona->insertar($nit,$ciu,$raz,$tel,$ema,$dir,$img,$obs)){
								if($this->M_proveedor->insertar($nit,$res,$web)){
									echo "ok";
								}else{
									$this->M_persona->eliminar($nit);
									echo "error";
								}
							}else{
								echo "error";
							}
						}else{
							echo $img;
						}

					}else{
						$p=$control[0];
						if($p->nombre==$raz && $p->telefono==$tel && $p->email==$ema && $p->direccion==$dir && $p->caracteristicas==$obs){
							$control=$this->M_proveedor->get_row('nit',$nit);
							if(empty($control)){
								if($this->M_proveedor->insertar($p->ci,$res,$web)){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "ci_exist";//proveedor ya registrado
							}
						}else{
							echo "ci_exist";
						}
					}
				}else{
					echo "fail";
				}

			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
	public function config_imprimir_proveedores(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				$listado['proveedores']=$_POST['json'];
				$this->load->view('cliente_proveedor/proveedor/4-imprimir/config',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function imprimir_proveedores(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				if(isset($_POST['v1'])){ if($_POST['v1']!="ok"){ $listado['v1']="ok";} }
				if(isset($_POST['v2'])){ if($_POST['v2']!="ok"){ $listado['v2']="ok";} }
				if(isset($_POST['v3'])){ if($_POST['v3']!="ok"){ $listado['v3']="ok";} }
				if(isset($_POST['v4'])){ if($_POST['v4']!="ok"){ $listado['v4']="ok";} }
				if(isset($_POST['v5'])){ if($_POST['v5']!="ok"){ $listado['v5']="ok";} }
				if(isset($_POST['v6'])){ if($_POST['v6']!="ok"){ $listado['v6']="ok";} }
				if(isset($_POST['v7'])){ if($_POST['v7']!="ok"){ $listado['v7']="ok";} }
				if(isset($_POST['v8'])){ if($_POST['v8']!="ok"){ $listado['v8']="ok";} }
				if(isset($_POST['v9'])){ if($_POST['v9']!="ok"){ $listado['v9']="ok";} }
				if(isset($_POST['v10'])){ if($_POST['v10']!="ok"){ $listado['v10']="ok"; } }
				if(isset($_POST['v11'])){ if($_POST['v11']!="ok"){ $listado['v11']="ok"; } }
				if(isset($_POST['v12'])){ if($_POST['v12']!="ok"){ $listado['v12']="ok"; } }
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=34;}
				$listado['proveedores']=$_POST['json'];
				$this->load->view('cliente_proveedor/proveedor/4-imprimir/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	public function detalle_proveedor(){
		if(!empty($this->session_id)){
			$idpro=$_POST['idpro'];
			$listado['proveedor']=$this->M_proveedor->get($idpro);
			$this->load->view('cliente_proveedor/proveedor/5-reportes/detalle_proveedor',$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	//modificar
   	public function form_update_proveedor(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpro'])){
				$idpro=$_POST['idpro'];
				$proveedor=$this->M_proveedor->get($idpro);
				if(!empty($proveedor)){
					$ciudades=$this->M_ciudad->get_row('idpa',$proveedor[0]->idpa);
					$listado['proveedor']=$proveedor;
					$listado['ciudades']=$ciudades;
					$listado['paises']=$this->M_pais->get_all();
					$listado['idpro']=$idpro;
					$this->load->view('cliente_proveedor/proveedor/6-configuracion/modificar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_proveedor(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpro']) && isset($_POST['nit']) && isset($_POST['raz']) && isset($_POST['res']) && isset($_POST['tel']) && isset($_POST['ema']) && isset($_POST['pai']) && isset($_POST['ciu']) && isset($_POST['web']) && isset($_POST['dir']) && isset($_POST['obs'])){
				$idpro=trim($_POST['idpro']);
				$nit=trim($_POST['nit']);
				$raz=trim($_POST['raz']);
				$res=trim($_POST['res']);
				$tel=trim($_POST['tel']);
				$ema=trim($_POST['ema']);
				$pai=trim($_POST['pai']);
				$ciu=trim($_POST['ciu']);
				$web=trim($_POST['web']);
				$dir=trim($_POST['dir']);
				$obs=trim($_POST['obs']);
				if($this->validaciones->esEntero($nit,6,25) && $this->validaciones->esPalabraConEspacio($raz,2,100) && $ciu!="" && $idpro!=""){
						$control=$this->M_proveedor->get($idpro);
						if(!empty($control)){
							$guardar=true;
							if($control[0]->ci!=$nit){// es igual no buscamos controlamos si existe ci
								$existe=$this->M_persona->get($nit);
								if(!empty($existe)){
									$guardar=false;
								}
							}
							if($guardar){
								$img=$control[0]->fotografia;
								$img=$this->validaciones->cambiar_imagen_miniatura($_FILES,'./libraries/img/personas/','',$this->resize,$img,$idpro.''.$control[0]->ci);//
								if($img!='error' && $img!="error_type"){
									if($this->M_persona->modificar($control[0]->ci,$nit,$ciu,$raz,$tel,$ema,$dir,$img,$obs)){
										if($this->M_proveedor->modificar($idpro,$nit,$res,$web)){
											echo "ok";
										}else{
											echo "error";
										}
									}else{
										echo "error";
									}
								}else{
									echo $img;
								}
							}else{
								echo "ci_exist";
							}							
						}else{
							echo "fail";
						}

				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
		/*----  materiales en el proveedor ----*/
		public function materiales_proveedor(){
			if(!empty($this->session_id)){
				if (isset($_POST['idpro'])) {
					$idpro=$_POST['idpro'];
					if($idpro!=""){
						$listado['idpro']=$idpro;
						$listado['materiales']=$this->M_material_proveedor->get_material($idpro,"material");
						$listado['materiales_adicionales']=$this->M_material_proveedor->get_material($idpro,"material_adicional");
						$listado['materiales_varios']=$this->M_material_proveedor->get_material($idpro,"material_vario");
						$listado['activos_fijos']=$this->M_material_proveedor->get_material($idpro,"activo_fijo");
						$this->load->view('cliente_proveedor/proveedor/6-configuracion/materiales',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "logout";
			}
		}
		public function update_presio_compra(){
			if(!empty($this->session_id)){
				if(isset($_POST['idmp']) && isset($_POST['can'])){
					if($_POST['idmp']!="" && $_POST['can']!=""){
						$idmp=$_POST['idmp'];
						$can=$_POST['can'];
						$aux=$this->M_material_proveedor->get($idmp);
						if(count($aux)>0 && $can>=0){
							if($this->M_material_proveedor->modificar_row_id($idmp,'costo_unitario',$can)){
								//sacando el promedio de los materiales para el costo por unidad del material
								$promedio=$this->M_material_proveedor->costo_promedio($aux[0]->idmi);
								$costo_promedio=0;
								if(count($promedio)>0){ $costo_promedio=$promedio[0]->costo; }
								if($this->M_material->modificar_idmi_row($aux[0]->idmi,'costo_unitario',$costo_promedio)){}
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "logout";
			}
		}
		public function confirmar_material_proveedor(){
			if(!empty($this->session_id)){
				if(isset($_POST['idmp']) && isset($_POST['tipo'])){
					$idmp=$_POST['idmp'];
					$tipo=$_POST['tipo'];
					$material_proveedor=$this->M_material_proveedor->get_material_item($idmp);
					if(count($material_proveedor)>0){
						$img="default.png";
						if($tipo=="almacen"){$url="./libraries/img/materiales/miniatura/";}
						if($tipo=="material_adicional"){ $url="./libraries/img/materiales_adicionales/miniatura/";}
						if($tipo=="materiales_varios"){ $url="./libraries/img/materiales_varios/miniatura/";}
						if($tipo=="activos_fijos"){ $url="./libraries/img/activos_fijos/miniatura/";}
						$control=$this->M_detalle_gasto->get_row('idmp',$idmp);
						if(!empty($control)){
							$listado['control']="";
							$listado['open_control']="false";
							$listado['desc']="Imposible eliminar el material del proveedor, el material en el proveedor se encuentra en uso de ".count($control)." registro(s) de compras, gastos o egresos";
						}else{
							$listado['desc']="Se eliminara definitivamente el material en el proveedor";
						}
						if($material_proveedor[0]->fotografia!="" && $material_proveedor[0]->fotografia!=NULL){
							$img=$material_proveedor[0]->fotografia;
						}
						$listado['titulo']="el material <strong>".$material_proveedor[0]->nombre."</strong> del proveedor";
						$listado['img']=$url.$img;
						$this->load->view('estructura/form_eliminar',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "logout";
			}
		}
   	public function	drop_material_proveedor(){
   		if(!empty($this->session_id)){
   			if(isset($_POST['idmp']) && isset($_POST['u']) && isset($_POST['p']) && isset($_POST['tipo'])){
   				$idmp=$_POST['idmp'];
   				$tipo=$_POST['tipo'];
				$u=$_POST['u'];
				$p=$_POST['p'];
				if($u==$this->session->userdata("login")){
					$usuario=$this->M_empleado->validate($u,$p);
					if(!empty($usuario)){
						$aux=$this->M_material_proveedor->get($idmp);
						if(count($aux)>0){
							if($this->M_material_proveedor->eliminar($idmp)){
								echo "ok";
								if($tipo=="almacen"){
									//sacando el promedio de los materiales para el costo por unidad del material
									$promedio=$this->M_material_proveedor->costo_promedio($aux[0]->idmi);
									$costo_promedio=0;
									if(count($promedio)>0){ $costo_promedio=$promedio[0]->costo; }
									if($this->M_material->modificar_idmi_row($aux[0]->idmi,'costo_unitario',$costo_promedio)){}
								}

							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "validate";
					}
				}else{
					echo "validate";
				}
   			}else{
   				echo "fail";
   			}
   		}else{
			echo "logout";
		}
   	}
			/*---- Materiales de almacenes en el proveedor ----*/
			public function search_materiales_proveedor(){
				if(!empty($this->session_id)){
					if(isset($_POST['idpro'])){
						$idpro=$_POST['idpro'];
						if($idpro!=""){
							$listado['idpro']=$idpro;
							$listado['almacenes']=$this->M_almacen->get_material();
							$this->load->view('cliente_proveedor/proveedor/6-configuracion/materiales/search',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "logout";
				}
			}
			public function view_materiales_proveedor(){
				if(!empty($this->session_id)){
					if(isset($_POST['idpro'])){
						$idpro=$_POST['idpro'];
						if($idpro!=""){
							if(isset($_POST['ida']) && isset($_POST['cod']) && isset($_POST['nom'])){
								$cod=$_POST['cod'];
								$nom=$_POST['nom'];
								if($cod!=""){
									$atrib="mi.codigo"; $val=$cod;
								}else{
									if($nom!=""){
										$atrib="mi.nombre"; $val=$nom;
									}else{
										$atrib=""; $val="";
									}
								}
								if($atrib!="" && $val!=""){
									$listado['materiales']=$this->M_almacen_material->get_material_search($_POST['ida'],$atrib,$val);
								}else{
									if($_POST['ida']!=""){
										$listado['materiales']=$this->M_almacen_material->get_material($_POST['ida']);
									}else{
										$listado['materiales']=$this->M_material_item->get_material();
									}
								}
							}else{
								$listado['materiales']=$this->M_material_item->get_material();
							}
							$listado['idpro']=$idpro;
							$this->load->view('cliente_proveedor/proveedor/6-configuracion/materiales/view',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "logout";
				}
			}
			public function save_material_proveedor(){
				if(!empty($this->session_id)){
					$idpro=$_POST['idpro'];
					$idmi=$_POST['idmi'];
					$existe=$this->M_material_proveedor->get_row_2n('idpro',$idpro,'idmi',$idmi);
					if(count($existe)>0){//desactivar
						if($this->M_material_proveedor->eliminar($existe[0]->idmp)){
							echo "0|ok";
						}else{
							echo "|fail";
						}
					}else{//activar boton
						if($this->M_material_proveedor->insertar($idpro,$idmi,0,NULL)){
							echo "1|ok";
						}else{
							echo "|fail";
						}
					}
					//sacando el promedio de los materiales para el costo por unidad del material
					$promedio=$this->M_material_proveedor->costo_promedio($idmi);
					$costo_promedio=0;
					if(count($promedio)>0){ $costo_promedio=$promedio[0]->costo; }
					if($this->M_material->modificar_idmi_row($idmi,'costo_unitario',$costo_promedio)){}
					
				}else{
					echo "|logout";
				}
			}

			/*---- End materiales de almacenes en el proveedor ----*/
			/*---- Materiales indirectos en el proveedor ----*/
			public function search_proveedor_indirectos(){
				if(!empty($this->session_id)){
					if(isset($_POST['idpro'])){
						$idpro=$_POST['idpro'];
						if($idpro!=""){
							$listado['idpro']=$idpro;
							$this->load->view('cliente_proveedor/proveedor/6-configuracion/material_adicional/search',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "logout";
				}
			}
			public function view_proveedor_indirectos(){
				if(!empty($this->session_id)){
					if(isset($_POST['idpro'])){
						$idpro=$_POST['idpro'];
						if($idpro!=""){
							if(isset($_POST['cod']) && isset($_POST['nom'])){
								$cod=$_POST['cod'];
								$nom=$_POST['nom'];
								if($cod!=""){
									$atrib="mi.codigo"; $val=$cod;
								}else{
									if($nom!=""){
										$atrib="mi.nombre"; $val=$nom;
									}else{
										$atrib=""; $val="";
									}
								}
								if($atrib!="" && $val!=""){
									echo "entro";
									$listado['materiales']=$this->M_material_item->get_row_search($atrib,$val,"material_adicional");
								}else{
									$listado['materiales']=$this->M_material_item->get_material_adicional();
								}
							}else{
								$listado['materiales']=$this->M_material_item->get_material_adicional();
							}
							$listado['idpro']=$idpro;
							$this->load->view('cliente_proveedor/proveedor/6-configuracion/material_adicional/view',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "logout";
				}
			}
			public function save_material_proveedor_2(){
				if(!empty($this->session_id)){
					if(isset($_POST['idpro']) && isset($_POST['idmi'])){
						$idpro=$_POST['idpro'];
						$idmi=$_POST['idmi'];
						if($idpro!="" && $idmi!=""){
							$existe=$this->M_material_proveedor->get_row_2n('idpro',$idpro,'idmi',$idmi);
							if(count($existe)>0){//desactivar
								if($this->M_material_proveedor->eliminar($existe[0]->idmp)){
									echo "0|ok";
								}else{
									echo "|fail";
								}
							}else{//activar boton
								if($this->M_material_proveedor->insertar($idpro,$idmi,0,NULL)){
									echo "1|ok";
								}else{
									echo "|fail";
								}
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}					
				}else{
					echo "|logout";
				}
			}
			/*---- End materiales indirectos en el proveedor ----*/
			/*---- Materiales varios en el proveedor ----*/
			public function search_proveedor_insumos(){
				if(!empty($this->session_id)){
					if(isset($_POST['idpro'])){
						$idpro=$_POST['idpro'];
						if($idpro!=""){
							$listado['idpro']=$idpro;
							$this->load->view('cliente_proveedor/proveedor/6-configuracion/materiales_varios/search',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "logout";
				}
			}
			public function view_proveedor_insumos(){
				if(!empty($this->session_id)){
					if(isset($_POST['idpro'])){
						$idpro=$_POST['idpro'];
						if($idpro!=""){
							if(isset($_POST['cod']) && isset($_POST['nom'])){
								$cod=$_POST['cod'];
								$nom=$_POST['nom'];
								if($cod!=""){
									$atrib="mi.codigo"; $val=$cod;
								}else{
									if($nom!=""){
										$atrib="mi.nombre"; $val=$nom;
									}else{
										$atrib=""; $val="";
									}
								}
								if($atrib!="" && $val!=""){
									echo "entro";
									$listado['materiales']=$this->M_material_item->get_row_search($atrib,$val,"material_varios");
								}else{
									$listado['materiales']=$this->M_material_item->get_material_vario();
								}
							}else{
								$listado['materiales']=$this->M_material_item->get_material_vario();
							}
							$listado['idpro']=$idpro;
							$this->load->view('cliente_proveedor/proveedor/6-configuracion/materiales_varios/view',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "logout";
				}
			}
			/*---- End materiales varios en el proveedor ----*/
			/*---- Materiales varios en el activos fijos ----*/
			public function search_proveedor_activos(){
				if(!empty($this->session_id)){
					if(isset($_POST['idpro'])){
						$idpro=$_POST['idpro'];
						if($idpro!=""){
							$listado['idpro']=$idpro;
							$this->load->view('cliente_proveedor/proveedor/6-configuracion/activo_fijo/search',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "logout";
				}
			}
			public function view_proveedor_activos(){
				if(!empty($this->session_id)){
					if(isset($_POST['idpro'])){
						$idpro=$_POST['idpro'];
						if($idpro!=""){
							if(isset($_POST['cod']) && isset($_POST['nom'])){
								$cod=$_POST['cod'];
								$nom=$_POST['nom'];
								if($cod!=""){
									$atrib="mi.codigo"; $val=$cod;
								}else{
									if($nom!=""){
										$atrib="mi.nombre"; $val=$nom;
									}else{
										$atrib=""; $val="";
									}
								}
								if($atrib!="" && $val!=""){
									echo "entro";
									$listado['materiales']=$this->M_material_item->get_row_search($atrib,$val,"activo_fijo");
								}else{
									$listado['materiales']=$this->M_material_item->get_activo_fijo();
								}
							}else{
								$listado['materiales']=$this->M_material_item->get_activo_fijo();
							}
							$listado['idpro']=$idpro;
							$this->load->view('cliente_proveedor/proveedor/6-configuracion/activo_fijo/view',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "logout";
				}
			}
			public function save_proveedor_activos(){
				if(!empty($this->session_id)){
					$idpro=$_POST['idpro'];
					$idmi=$_POST['idmi'];
					$existe=$this->M_material_proveedor->get_row_2n('idpro',$idpro,'idmi',$idmi);
					if(count($existe)>0){//desactivar
						if($this->M_material_proveedor->eliminar($existe[0]->idmp)){
							echo "0|ok";
						}else{
							echo "|fail";
						}
					}else{//activar boton
						if($this->M_material_proveedor->insertar($idpro,$idmi,0,NULL)){
							echo "1|ok";
						}else{
							echo "|fail";
						}
					}
					//sacando el promedio de los materiales para el costo por unidad del material
					$promedio=$this->M_material_proveedor->costo_promedio($idmi);
					$costo_promedio=0;
					if(count($promedio)>0){ $costo_promedio=$promedio[0]->costo; }
					if($this->M_material->modificar_idmi_row($idmi,'costo_unitario',$costo_promedio)){}
					
				}else{
					echo "|logout";
				}
			}
			/*---- End materiales varios en el activos fijos ----*/

















/*


	public function search_material_adicional_proveedor(){
		if(!empty($this->session_id)){
			$idpro=$_POST['idpro'];
			$listado['idpro']=$idpro;
			$this->load->view('cliente_proveedor/proveedor/6-configuracion/material_adicional/search',$listado);
		}else{
			echo "logout";
		}
	}
	public function view_material_adicional_proveedor(){
		if(!empty($this->session_id)){
			$idpro=$_POST['idpro'];
			$listado['materiales']=$this->M_material_item->get_material_adicional();
			$listado['idpro']=$idpro;
			$this->load->view('cliente_proveedor/proveedor/6-configuracion/material_adicional/view',$listado);
		}else{
			echo "logout";
		}
	}
	public function search_activo_fijo_proveedor(){
		if(!empty($this->session_id)){
			$idpro=$_POST['idpro'];
			$listado['idpro']=$idpro;
			$this->load->view('cliente_proveedor/proveedor/6-configuracion/activo_fijo/search',$listado);
		}else{
			echo "logout";
		}
	}
	public function view_activo_fijo_proveedor(){
		if(!empty($this->session_id)){
			$idpro=$_POST['idpro'];
			$listado['materiales']=$this->M_material_item->get_activo_fijo();
			$listado['idpro']=$idpro;
			$this->load->view('cliente_proveedor/proveedor/6-configuracion/activo_fijo/view',$listado);
		}else{
			echo "logout";
		}
	}


	public function search_material_vario_proveedor(){
		if(!empty($this->session_id)){
			$idpro=$_POST['idpro'];
			$listado['idpro']=$idpro;
			$this->load->view('cliente_proveedor/proveedor/6-configuracion/materiales_varios/search',$listado);
		}else{
			echo "logout";
		}
	}
	public function view_material_vario_proveedor(){
		if(!empty($this->session_id)){
			$idpro=$_POST['idpro'];
			$listado['materiales']=$this->M_material_item->get_material_vario();
			$listado['idpro']=$idpro;
			$this->load->view('cliente_proveedor/proveedor/6-configuracion/materiales_varios/view',$listado);
		}else{
			echo "logout";
		}
	}*/

/*	public function drop_material_proveedor(){
		if(!empty($this->session_id)){
			if(isset($_POST['idmp'])){
				$aux=$this->M_material_proveedor->get($_POST['idmp']);
				if($this->M_material_proveedor->eliminar($_POST['idmp'])){
					if(count($aux)>0){
						$promedio=$this->M_material_proveedor->costo_promedio($aux[0]->idmi);
						$costo_promedio=0;
						if(count($promedio)>0){
							$costo_promedio=$promedio[0]->costo;
						}
						if($this->M_material->modificar_idmi_row($aux[0]->idmi,'costo_unitario',$costo_promedio)){}
					}
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}*/
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	public function confirmar_proveedor(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpro'])){
				$idpro=$_POST['idpro'];
				$proveedor=$this->M_proveedor->get($idpro);
				if(!empty($proveedor)){
					$img="default.png";
					if($proveedor[0]->fotografia!=NULL && $proveedor[0]->fotografia!=""){ $img=$proveedor[0]->fotografia;}
					$url='./libraries/img/personas/miniatura/'.$img;
					$listado['titulo']="el proveedor".$proveedor[0]->nombre;
					$control=$this->M_material_proveedor->get_row('idpro',$idpro);
					if(!empty($control)){
						$listado['control']="";
						$listado['open_control']="false";
						$listado['desc']="Imposible eliminar el proveedor, el proveedor no debe tener materiales asignados para ser eliminado. el proveedor posee en total ".count($control)." material(es)";
					}else{
						$listado['desc']="Se eliminara definitivamente el proveedor";
					}
					$listado['img']=$url;
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function	drop_proveedor(){
   		if(!empty($this->session_id)){
   			if(isset($_POST['idpro']) && isset($_POST['u']) && isset($_POST['p'])){
   				$idpro=$_POST['idpro'];
				$u=$_POST['u'];
				$p=$_POST['p'];
				if($u==$this->session->userdata("login")){
					$usuario=$this->M_empleado->validate($u,$p);
					if(!empty($usuario)){
						if($idpro!=""){
							$proveedor=$this->M_proveedor->get($idpro);
								if(!empty($proveedor)){
									//verificando si existe en empleado o proveedor
									$empleado=$this->M_empleado->get_row('ci',$proveedor[0]->ci);
									$cliente=$this->M_cliente->get_cliente_row('nit',$proveedor[0]->ci);
									if(count($empleado)>0 || count($cliente)>0){//existe, borramos solo a proveedor
										if($this->M_proveedor->eliminar($proveedor[0]->idpro)){
											echo "ok";
										}else{
											echo "error";
										}
									}else{//no existe, borramos persona y su fotografia
										if($this->validaciones->eliminar_imagen($proveedor[0]->fotografia,'./libraries/img/personas/')){
											if($this->M_persona->eliminar($proveedor[0]->ci)){
												echo "ok";
											}else{
												echo "error";
											}
										}else{
											echo "error";
										}	
									}
								}else{
									echo "fail";
								}
						}else{
							echo "fail";
						}
					}else{
						echo "validate";
					}
				}else{
					echo "validate";
				}
   			}else{
   				echo "fail";
   			}
   		}else{
			echo "logout";
		}
   	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE PROVEEDOR -------*/
/*------- MANEJO DE CONFIGURACION -------*/
	public function view_config(){
		if(!empty($this->session_id)){
			$listado['paises']=$this->M_pais->get_all();
			$listado['ciudades']=$this->M_ciudad->get_all();
			$listado['utilidad_ventas']=$this->M_utilidad_venta->get_all();
			$this->load->view('cliente_proveedor/config/view',$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Pais ---*/
   	public function save_pais(){
		if(!empty($this->session_id)){
			if(isset($_POST['p'])){
				if($this->validaciones->esPalabraConEspacio($_POST['p'],2,100)){
					if($this->M_pais->insertar($_POST['p'])){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_pais(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpa']) && isset($_POST['p'])){
				if($_POST['idpa']!="" && $this->validaciones->esPalabraConEspacio($_POST['p'],2,100)){
					if($this->M_pais->modificar($_POST['idpa'],$_POST['p'])){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function delete_pais(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpa'])){
				if($this->M_pais->eliminar($_POST['idpa'])){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Pais ---*/
   	/*--- Ciudad ---*/
   	public function save_ciudad(){
		if(!empty($this->session_id)){
			if(isset($_POST['cp']) && isset($_POST['c'])){
				if($this->validaciones->esPalabraConEspacio($_POST['c'],2,100) && isset($_POST['cp'])!=""){
					if($this->M_ciudad->insertar($_POST['cp'],$_POST['c'])){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_ciudad(){
		if(!empty($this->session_id)){
			if(isset($_POST['idci']) && isset($_POST['c']) && isset($_POST['cp'])){
				if($_POST['idci']!="" && $this->validaciones->esPalabraConEspacio($_POST['c'],2,100) && $_POST['cp']!=""){
					if($this->M_ciudad->modificar($_POST['idci'],$_POST['cp'],$_POST['c'])){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function delete_ciudad(){
		if(!empty($this->session_id)){
			if(isset($_POST['idci'])){
				if($this->M_ciudad->eliminar($_POST['idci'])){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Ciudad ---*/
	/*--- control de porcentaje de utilidad de venta ---*/
	public function save_porcentaje(){
		if(!empty($this->session_id)){
			if(isset($_POST['des']) && isset($_POST['por'])){
				$des=$_POST['des'];
				$por=$_POST['por'];
				if($this->validaciones->esPalabraConEspacio($des,2,150) && $this->validaciones->esNroDecimal($por,3,2) && $por<=100){
					$por=$por/100;
					if($this->M_utilidad_venta->insertar($des,$por)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_porcentaje(){
		if(!empty($this->session_id)){
			if(isset($_POST['des']) && isset($_POST['por']) && isset($_POST['iduv'])){
				$iduv=$_POST['iduv'];
				$des=$_POST['des'];
				$por=$_POST['por'];
				if($this->validaciones->esPalabraConEspacio($des,2,150) && $this->validaciones->esNroDecimal($por,3,2) && $por<=100 && $iduv!=""){
					$por=$por/100;
					if($this->M_utilidad_venta->modificar($iduv,$des,$por)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function delete_porcentaje(){
		if(!empty($this->session_id)){
			if(isset($_POST['iduv'])){
				$iduv=$_POST['iduv'];
				if($this->M_utilidad_venta->eliminar($iduv)){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	/*--- End control de porcentaje de utilidad de venta ---*/
/*------- END MANEJO DE CONFIGURACION -------*/
/*------- ALERTA ---------*/
	public function alerta(){
		if(!empty($this->session_id)){
			if(isset($_POST['titulo'])){
				$listado['titulo']=$_POST['titulo'];
			}else{
				$listado['titulo']="";
			}
			if(isset($_POST['descripcion'])){
				$listado['desc']=$_POST['descripcion'];
			}else{
				$listado['desc']="";
			}
			if(isset($_POST['img'])){
				$listado['img']=$_POST['img'];
			}else{
				$listado['img']=base_url().'libraries/img/sistema/warning.png';
			}
			$listado['control']="";
			$listado['open_control']="false";
			$this->load->view('estructura/form_eliminar',$listado);

		}else{
			echo "logout";
		}
	}
/*------- END ALERTA ---------*/


}
/* End of file cliente_proveedor.php */
/* Location: ./application/controllers/cliente_proveedor.php */