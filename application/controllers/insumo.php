<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Insumo extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('login');
	}
	public function index(){
		if(!empty($this->session_id)){
			if(!isset($_GET['p'])){
				$listado['pestania']=1;
			}else{
				$listado['pestania']=$_GET['p'];
			}
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('v_insumo',$listado);
		}else{
			redirect(base_url().'login/input',301);
		}
	}
/*------- MANEJO DE MATERIAL ADICIONAL -------*/
	public function search_material(){
		if(!empty($this->session_id)){
			$listado['unidades']=$this->M_unidad->get_all();
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('insumo/material_adicional/search',$listado);
		}else{
			echo "logout";
		}
	}
	public function view_material(){
		if(!empty($this->session_id)){
			if(isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['uni'])){
				if($_POST['cod']!=""){
					$atrib="mi.codigo";$val=$_POST['cod'];
				}else{
					if($_POST['nom']!=""){
						$atrib="mi.nombre";$val=$_POST['nom'];
					}else{
						if($_POST['uni']!=""){
							$atrib="u.idu";$val=$_POST['uni'];
						}else{
							$atrib="";$val="";
						}
					}
				}
				if($atrib!="" && $val!=""){
					$listado['materiales']=$this->M_material_adicional->search_material_adicional($atrib,$val);
				}else{
					$listado['materiales']=$this->M_material_item->get_material_adicional();	
				}
			}else{
				$listado['materiales']=$this->M_material_item->get_material_adicional();				
			}
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('insumo/material_adicional/view',$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Nuevo ---*/
   	public function new_material(){
		if(!empty($this->session_id)){
			$listado['unidad']=$this->M_unidad->get_all();
			$listado['proveedores']=$this->M_proveedor->get_all();
			$this->load->view('insumo/material_adicional/3-nuevo/form',$listado);
		}else{
			echo "logout";
		}
	}
	public function save_material(){
		if(!empty($this->session_id)){
			if(isset($_FILES) && isset($_POST['nom']) && isset($_POST['cod']) && isset($_POST['med']) && isset($_POST['des']) && isset($_POST['pro'])){
				$nom=$_POST['nom'];
				$cod=$_POST['cod'];
				$med=$_POST['med'];
				$des=$_POST['des'];
				$pro=$_POST['pro'];
				if($this->val->strSpace($nom,3,200) && count($med)>0){
					$guardar=true;
					if($cod!=""){ if(!$this->val->strNoSpace($cod,2,15)){ $guardar=false;}}
					if($des!=""){ if(!$this->val->strSpace($des,5,700)){ $guardar=false;}}
					if($guardar){
						$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/materiales_adicionales/','',$this->resize,'');
						if($img!="error_type" && $img!="error"){
							$max=$this->M_material_item->max('idmi');
							$idmi=$max[0]->max+1;
							if($this->M_material_item->insertar($idmi,$med,$cod,$nom,$img,$des)){
								if($this->M_material_adicional->insertar($idmi)){
									if($pro!=""){
										if($this->M_material_proveedor->insertar($pro,$idmi,0,date('Y-m-d'))){
											echo "ok";
										}else{
											echo "error";
										}
									}else{
										echo "ok";
									}
								}else{
									$this->M_material_item->eliminar($idmi);
									echo "error";
								}
							}else{
								echo "error";
							}
						}else{
							echo $img;
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	public function config_imprimir_materiales(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				$listado['materiales']=$_POST['json'];
				$this->load->view('insumo/material_adicional/4-imprimir/config',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function imprimir_materiales(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				if(isset($_POST['v1'])){ if($_POST['v1']!="ok"){ $listado['v1']="ok";}}
				if(isset($_POST['v2'])){ if($_POST['v2']!="ok"){ $listado['v2']="ok";}}
				if(isset($_POST['v3'])){ if($_POST['v3']!="ok"){ $listado['v3']="ok";}}
				if(isset($_POST['v4'])){ if($_POST['v4']!="ok"){ $listado['v4']="ok";}}
				if(isset($_POST['v5'])){ if($_POST['v5']!="ok"){ $listado['v5']="ok";}}
				if(isset($_POST['v6'])){ if($_POST['v6']!="ok"){ $listado['v6']="ok";}}
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=34;}
				$listado['materiales']=$_POST['json'];
				$this->load->view('insumo/material_adicional/4-imprimir/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	public function detalle_material(){
		if(!empty($this->session_id)){
			if(isset($_POST['idmi'])){
				$idmi=$_POST['idmi'];
				$material=$this->M_material_adicional->get_material_item($idmi);
				if(!empty($material)){
					$listado['material']=$material;
					$this->load->view('insumo/material_adicional/5-reportes/form',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	public function form_update_material(){
		if(!empty($this->session_id)){
			if(isset($_POST['idmi'])){
				$idmi=$_POST['idmi'];
				$material=$this->M_material_adicional->get_material_item($idmi);
				if(!empty($material)){
					$listado['unidad']=$this->M_unidad->get_all();
					$proveedores=$this->M_material_proveedor->get_row('idmi',$idmi);
					if(count($proveedores)<=1){
						if(count($proveedores)==1){
							$listado['proveedor']=$proveedores[0];
						}
						$listado['proveedores']=$this->M_proveedor->get_all();
					}
					$listado['material']=$material;
					$listado['idmi']=$idmi;
					$this->load->view('insumo/material_adicional/6-config/modificar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_material(){
		if(!empty($this->session_id)){
			if(isset($_FILES) && isset($_POST['idmi']) && isset($_POST['nom']) && isset($_POST['cod']) && isset($_POST['med']) && isset($_POST['des']) && isset($_POST['pro'])){
				$idmi=$_POST['idmi'];				
				$nom=$_POST['nom'];
				$cod=$_POST['cod'];
				$med=$_POST['med'];
				$des=$_POST['des'];
				$pro=$_POST['pro'];
				if($this->val->strSpace($nom,3,200) && count($med)>0){
					$guardar=true;
					if($cod!=""){ if(!$this->val->strNoSpace($cod,2,15)){ $guardar=false;}}
					if($des!=""){ if(!$this->val->strSpace($des,5,700)){ $guardar=false;}}
					if($guardar){
						$material_item=$this->M_material_item->get($idmi);
						$img=$material_item[0]->fotografia;
						$img=$this->validaciones->cambiar_imagen_miniatura($_FILES,'./libraries/img/materiales_adicionales/','',$this->resize,$img,$idmi);
						if($img!="error_type" && $img!="error"){
							if($this->M_material_item->modificar($idmi,$med,$cod,$nom,$img,$des)){
								$control=$this->M_material_proveedor->get_row('idmi',$idmi);
								if(empty($control)){
									if($pro!=""){
										if($this->M_material_proveedor->insertar($pro,$idmi,0,date('Y-m-d'))){
											echo "ok";
										}else{
											echo "error";
										}
									}else{
										echo "ok";
									}
								}else{
									if(count($control)==1){
										if($this->M_material_proveedor->modificar($control[0]->idmp,$pro,$idmi,0)){
											echo "ok";
										}else{
											echo "error";
										}
									}else{
										echo "ok";
									}
								}
							}else{
								echo "error";
							}
						}else{
							echo $img;
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	public function confirmar_material(){
		if(!empty($this->session_id)){
			if(isset($_POST['idmi'])){
				$idmi=$_POST['idmi'];
				$url='./libraries/img/materiales_adicionales/miniatura/';
				$material=$this->M_material_adicional->get_material_item($idmi);
				if(!empty($material)){
					$control=$this->M_producto_material_adicional->get_row('idma',$material[0]->idma);//control si el material esta asignado a un producto
					if(!empty($control)){
						$listado['desc']="Imposible Eliminar: El material se encuentra asignado a ".count($control)." producto(s).";
						$listado['open_control']="false";
						$listado['control']="";	
					}else{
						$control=$this->M_material_proveedor->get_row('idmi',$idmi);//controla si el materiales esta asigando a un proveedor
						if(!empty($control)){
							$listado['desc']="Imposible Eliminar: El material se encuentra asigando a ".count($control)." proveedor(es).";
							$listado['open_control']="false";
							$listado['control']="";	
						}else{
							$listado['desc']="Se eliminara definitivamente el material adicional.";
						}
					}
					$img='default.png';
					if($material[0]->fotografia!=NULL && $material[0]->fotografia!=""){ $img=$material[0]->fotografia; }
					$listado['titulo']="el material adicional <b>".$material[0]->nombre."</b>";
					$listado['img']=$url.$img;
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function drop_material(){
		if(!empty($this->session_id)){
			if(isset($_POST['idmi']) && isset($_POST['u']) && isset($_POST['p'])){
				$idmi=$_POST['idmi'];
				$u=$_POST['u'];
				$p=$_POST['p'];
				if($u==$this->session->userdata("login")){
					$usuario=$this->M_empleado->validate($u,$p);
					if(!empty($usuario)){
						$material=$this->M_material_adicional->get_material_item($idmi);
						if(!empty($material)){
							if($this->lib->eliminar_imagen($material[0]->fotografia,'./libraries/img/materiales_adicionales/')){
								if($this->M_material_item->eliminar($material[0]->idmi)){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "validate";
					}
				}else{
					echo "validate";
				}
			}else{
				echo "fail";
			}			
		}else{
			echo "logout";
		}
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE MATERIAL ADICIONAL -------*/
	public function search_insumo(){
		if(!empty($this->session_id)){
			$listado['unidades']=$this->M_unidad->get_all();
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('insumo/insumo/search',$listado);
		}else{
			echo "logout";
		}
	}
	public function view_insumo(){
		if(!empty($this->session_id)){
			if(isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['uni'])){
				if($_POST['cod']!=""){
					$atrib="mi.codigo";$val=$_POST['cod'];
				}else{
					if($_POST['nom']!=""){
						$atrib="mi.nombre";$val=$_POST['nom'];
					}else{
						if($_POST['uni']!=""){
							$atrib="u.idu";$val=$_POST['uni'];
						}else{
							$atrib="";$val="";
						}
					}
				}
				if($atrib!="" && $val!=""){
					$listado['materiales']=$this->M_material_varios->search($atrib,$val);
				}else{
					$listado['materiales']=$this->M_material_item->get_material_vario();	
				}
			}else{
				$listado['materiales']=$this->M_material_item->get_material_vario();				
			}
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('insumo/insumo/view',$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Nuevo ---*/
   	public function new_insumo(){
		if(!empty($this->session_id)){
			$listado['unidad']=$this->M_unidad->get_all();
			$listado['proveedores']=$this->M_proveedor->get_all();
			$this->load->view('insumo/insumo/3-nuevo/form',$listado);
		}else{
			echo "logout";
		}
	}
	public function save_insumo(){
		if(!empty($this->session_id)){
			if(isset($_FILES) && isset($_POST['nom']) && isset($_POST['cod']) && isset($_POST['med']) && isset($_POST['des']) && isset($_POST['pro'])){
				$nom=$_POST['nom'];
				$cod=$_POST['cod'];
				$med=$_POST['med'];
				$des=$_POST['des'];
				$pro=$_POST['pro'];
				if($this->val->strSpace($nom,3,200) && count($med)>0){
					$guardar=true;
					if($cod!=""){ if(!$this->val->strNoSpace($cod,2,15)){ $guardar=false;}}
					if($des!=""){ if(!$this->val->strSpace($des,5,700)){ $guardar=false;}}
					if($guardar){
						$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/materiales_varios/','',$this->resize,'');
						if($img!="error_type" && $img!="error"){
							$max=$this->M_material_item->max('idmi');
							$idmi=$max[0]->max+1;
							if($this->M_material_item->insertar($idmi,$med,$cod,$nom,$img,$des)){
								if($this->M_material_varios->insertar($idmi)){
									if($pro!=""){
										if($this->M_material_proveedor->insertar($pro,$idmi,0,date('Y-m-d'))){
											echo "ok";
										}else{
											echo "error";
										}
									}else{
										echo "ok";
									}
								}else{
									$this->M_material_item->eliminar($idmi);
									echo "error";
								}
							}else{
								echo "error";
							}
						}else{
							echo $img;
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	public function config_imprimir_insumo(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				$listado['materiales']=$_POST['json'];
				$this->load->view('insumo/insumo/4-imprimir/config',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function imprimir_insumo(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				if(isset($_POST['v1'])){ if($_POST['v1']!="ok"){ $listado['v1']="ok";}}
				if(isset($_POST['v2'])){ if($_POST['v2']!="ok"){ $listado['v2']="ok";}}
				if(isset($_POST['v3'])){ if($_POST['v3']!="ok"){ $listado['v3']="ok";}}
				if(isset($_POST['v4'])){ if($_POST['v4']!="ok"){ $listado['v4']="ok";}}
				if(isset($_POST['v5'])){ if($_POST['v5']!="ok"){ $listado['v5']="ok";}}
				if(isset($_POST['v6'])){ if($_POST['v6']!="ok"){ $listado['v6']="ok";}}
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=34;}
				$listado['materiales']=$_POST['json'];
				$this->load->view('insumo/insumo/4-imprimir/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	public function detalle_insumo(){
		if(!empty($this->session_id)){
			if(isset($_POST['idmi'])){
				$idmi=$_POST['idmi'];
				$material=$this->M_material_varios->get_material_item($idmi);
				if(!empty($material)){
					$listado['material']=$material;
					$this->load->view('insumo/insumo/5-reportes/form',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	public function form_update_insumo(){
		if(!empty($this->session_id)){
			if(isset($_POST['idmi'])){
				$idmi=$_POST['idmi'];
				$material=$this->M_material_varios->get_material_item($idmi);
				if(!empty($material)){
					$listado['unidad']=$this->M_unidad->get_all();
					$proveedores=$this->M_material_proveedor->get_row('idmi',$idmi);
					if(count($proveedores)<=1){
						if(count($proveedores)==1){
							$listado['proveedor']=$proveedores[0];
						}
						$listado['proveedores']=$this->M_proveedor->get_all();
					}
					$listado['material']=$material;
					$listado['idmi']=$idmi;
					$this->load->view('insumo/insumo/6-config/modificar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_insumo(){
		if(!empty($this->session_id)){
			if(isset($_FILES) && isset($_POST['idmi']) && isset($_POST['nom']) && isset($_POST['cod']) && isset($_POST['med']) && isset($_POST['des']) && isset($_POST['pro'])){
				$idmi=$_POST['idmi'];				
				$nom=$_POST['nom'];
				$cod=$_POST['cod'];
				$med=$_POST['med'];
				$des=$_POST['des'];
				$pro=$_POST['pro'];
				if($this->val->strSpace($nom,3,200) && count($med)>0){
					$guardar=true;
					if($cod!=""){ if(!$this->val->strNoSpace($cod,2,15)){ $guardar=false;}}
					if($des!=""){ if(!$this->val->strSpace($des,5,700)){ $guardar=false;}}
					if($guardar){
						$material_item=$this->M_material_item->get($idmi);
						$img=$material_item[0]->fotografia;
						$img=$this->validaciones->cambiar_imagen_miniatura($_FILES,'./libraries/img/materiales_varios/','',$this->resize,$img,$idmi);
						if($img!="error_type" && $img!="error"){
							if($this->M_material_item->modificar($idmi,$med,$cod,$nom,$img,$des)){
								$control=$this->M_material_proveedor->get_row('idmi',$idmi);
								if(empty($control)){
									if($pro!=""){
										if($this->M_material_proveedor->insertar($pro,$idmi,0,date('Y-m-d'))){
											echo "ok";
										}else{
											echo "error";
										}
									}else{
										echo "ok";
									}
								}else{
									if(count($control)==1){
										if($this->M_material_proveedor->modificar($control[0]->idmp,$pro,$idmi,0)){
											echo "ok";
										}else{
											echo "error";
										}
									}else{
										echo "ok";
									}
								}
							}else{
								echo "error";
							}
						}else{
							echo $img;
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	public function confirmar_insumo(){
		if(!empty($this->session_id)){
			if(isset($_POST['idmi'])){
				$idmi=$_POST['idmi'];
				$url='./libraries/img/materiales_varios/miniatura/';
				$material=$this->M_material_varios->get_material_item($idmi);
				if(!empty($material)){
					$control=$this->M_material_proveedor->get_row('idmi',$idmi);//controla si el materiales esta asigando a un proveedor
					if(!empty($control)){
						$listado['desc']="Imposible Eliminar: El material o insumo se encuentra asigando a ".count($control)." proveedor(es).";
						$listado['open_control']="false";
						$listado['control']="";	
					}else{
						$listado['desc']="Se eliminara definitivamente el material o insumo.";
					}
					$img='default.png';
					if($material[0]->fotografia!=NULL && $material[0]->fotografia!=""){ $img=$material[0]->fotografia; }
					$listado['titulo']="el material o insumo <b>".$material[0]->nombre."</b>";
					$listado['img']=$url.$img;
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function drop_insumo(){
		if(!empty($this->session_id)){
			if(isset($_POST['idmi']) && isset($_POST['u']) && isset($_POST['p'])){
				$idmi=$_POST['idmi'];
				$u=$_POST['u'];
				$p=$_POST['p'];
				if($u==$this->session->userdata("login")){
					$usuario=$this->M_empleado->validate($u,$p);
					if(!empty($usuario)){
						$material=$this->M_material_varios->get_material_item($idmi);
						if(!empty($material)){
							if($this->lib->eliminar_imagen($material[0]->fotografia,'./libraries/img/materiales_varios/')){
								if($this->M_material_item->eliminar($material[0]->idmi)){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "validate";
					}
				}else{
					echo "validate";
				}
			}else{
				echo "fail";
			}			
		}else{
			echo "logout";
		}
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE INSUMOS -------*/
/*------- MANEJO DE CONFIGURACION -------*/
	public function view_config(){
		if(!empty($this->session_id)){
			$listado['unidades']=$this->M_unidad->get_all();
			$this->load->view('insumo/config/view',$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Manejo de unidad ---*/
	public function save_unidad(){
		if(!empty($this->session_id)){
			if(isset($_POST['nom']) && isset($_POST['abr']) && isset($_POST['equ']) && isset($_POST['des'])){
				$nom=$_POST['nom'];
				$uni=$_POST['abr'];
				$equ=$_POST['equ'];
				$des_equ=$_POST['des'];
				if($this->validaciones->esPalabraConEspacio($nom,2,40) && $this->validaciones->esPalabraConEspacio($uni,1,10) && $equ>=0 && $this->validaciones->esPalabraConEspacio($des_equ,0,200)){
					if($this->M_unidad->insertar($nom,$uni,$equ,$des_equ)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}			
		}else{
			echo "logout";
		}
	}

	public function update_unidad(){
		if(!empty($this->session_id)){
			if(isset($_POST['idu']) && isset($_POST['nom']) && isset($_POST['abr']) && isset($_POST['equ']) && isset($_POST['des'])){
				$idu=$_POST['idu'];
				$nom=$_POST['nom'];
				$uni=$_POST['abr'];
				$equ=$_POST['equ'];
				$des_equ=$_POST['des'];
				if($this->val->strSpace($nom,2,40) && $this->val->strSpace($uni,1,10) && $equ>=0 && $this->validaciones->esPalabraConEspacio($des_equ,0,200)){
					if($this->M_unidad->modificar($idu,$nom,$uni,$equ,$des_equ)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}	
		}else{
			echo "logout";
		}
	}
	public function drop_unidad(){
		if (!empty($this->session_id)) {
			if(isset($_POST['idu'])){
				$idu=$_POST['idu'];
				if($idu!=""){
					if($this->M_unidad->eliminar($idu)){
						echo "ok";
					}else{
						echo "error";
					}					
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}			
		}else{
			echo "logout";
		}
	}
   	/*--- End Manejo de unidad ---*/
/*------- END MANEJO DE CONFIGURACION -------*/
/*------- ALERTA ---------*/
	public function alerta(){
		if(!empty($this->session_id)){
			if(isset($_POST['titulo'])){
				$listado['titulo']=$_POST['titulo'];
			}else{
				$listado['titulo']="";
			}
			if(isset($_POST['descripcion'])){
				$listado['desc']=$_POST['descripcion'];
			}else{
				$listado['desc']="";
			}
			if(isset($_POST['img'])){
				$listado['img']=$_POST['img'];
			}else{
				$listado['img']=base_url().'libraries/img/sistema/warning.png';
			}
			$listado['control']="";
			$listado['open_control']="false";
			$this->load->view('estructura/form_eliminar',$listado);
		}else{
			echo "logout";
		}
	}
/*------- END ALERTA ---------*/
}

/* End of file almacen.php */
/* Location: ./application/controllers/almacen.php */