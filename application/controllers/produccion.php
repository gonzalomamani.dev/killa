<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produccion extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('login');
		$this->session->set_userdata('ida','',true);
		$this->session->set_userdata('tipo','',true);
	}
	public function index(){
		if(!empty($this->session_id)){
			$privilegio=$privilegio=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			if($privilegio[0]->pr=="1"){
				if(!isset($_GET['p'])){
					if($privilegio[0]->pr1r==1){
						$listado['pestania']=1;
					}else{
						if($privilegio[0]->pr2r==1){ 
							$listado['pestania']=2;
						}else{
							if($privilegio[0]->pr3r==1){ 
								$listado['pestania']=5;
							}else{
								$listado['pestania']=0;
							}
						}
					}
				}else{
					$listado['pestania']=$_GET['p'];
				}
				$listado['privilegio']=$privilegio;
				$this->load->view('v_produccion',$listado);
			}else{
				$this->val->redireccion($privilegio);
			}
		}else{
			redirect(base_url().'login/input',301);
		}
	}
/*------- MANEJO DE PRODUCTOS -------*/
	public function search_producto(){
		if(!empty($this->session_id)){
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('produccion/producto/search',$listado);
		}else{
			echo "logout";
		}
	}
	public function view_producto(){
		if(!empty($this->session_id)){
			if(isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['fec']) && isset($_POST['dis'])){
				if($_POST['cod']!=""){
					$atrib='p.cod';$val=$_POST['cod'];
				}else{
					if($_POST['nom']!=""){
						$atrib='p.nombre';$val=$_POST['nom'];
					}else{
						if($_POST['fec']!=""){
							$atrib='p.fecha_creacion';$val=$_POST['fec'];
						}else{
							if($_POST['dis']!=""){
								$atrib='p.disenador';$val=$_POST['dis'];
							}else{
								$atrib="";$val="";
							}
						}
					}
				}
				if($atrib!="" && $val!=""){
					$listado['productos']=$this->M_producto->get_search($atrib,$val);
				}else{
					$listado['productos']=$this->M_producto->get_all();
				}
			}else{
				$listado['productos']=$this->M_producto->get_all();
			}
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('produccion/producto/view',$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Nuevo ---*/
	public function new_producto(){
		if(!empty($this->session_id)){
		 	//$listado['almacen']=$this->M_almacen->get_producto();
		 	$listado['grupos']=$this->M_producto_grupo->get_all();
			$this->load->view('produccion/producto/3-nuevo/view',$listado);
		}else{
			echo "logout";
		}
	}
	public function save_producto(){//guardamos el producto
		if(!empty($this->session_id)){
			if(isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['dis']) && isset($_POST['fec']) && isset($_POST['des']) && isset($_POST['gru']) && isset($_POST['peso']) && isset($_POST['etcu']) && isset($_POST['corr']) && isset($_POST['pllo']) && isset($_POST['relo']) && isset($_POST['etlo']) && isset($_POST['repu']) && isset($_POST['cidelante']) && isset($_POST['cidetras']) && isset($_POST['cidentro']) && isset($_POST['alto']) && isset($_POST['ancho']) && isset($_POST['largo']) && isset($_POST['car'])){
				$cod=trim($_POST['cod']);
				$nom=trim($_POST['nom']);
				$dis=trim($_POST['dis']);
				$fec=trim($_POST['fec']);
				$des=trim($_POST['des']);
				$gru=trim($_POST['gru']);
				//$cu=trim($_POST['cu']);
				$peso=trim($_POST['peso']);
				$etcu=trim($_POST['etcu']);
				$corr=trim($_POST['corr']);
				$pllo=trim($_POST['pllo']);
				$relo=trim($_POST['relo']);
				$etlo=trim($_POST['etlo']);
				$repu=trim($_POST['repu']);
				$cidelante=trim($_POST['cidelante']);
				$cidetras=trim($_POST['cidetras']);
				$cidentro=trim($_POST['cidentro']);
				$alto=trim($_POST['alto']);
				$ancho=trim($_POST['ancho']);
				$largo=trim($_POST['largo']);
				$car=trim($_POST['car']);
				if($this->val->strSpace($cod,2,20) && $this->val->strSpace($nom,3,150) && $this->val->strSpace($dis,3,100) && $this->val->fecha($fec) && $this->val->entero($gru,0,10)){
					$max=$this->M_producto->max('idp');
					$id=$max[0]->max+1;
					if($this->M_producto->insertar($id,$gru,$cod,$nom,$fec,$des,$dis,$peso,$etlo,$pllo,$relo,$etcu,$corr,$cidelante,$cidetras,$cidentro,$largo,$alto,$ancho,$repu,$car)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	public function config_imprimir_productos(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				$listado['productos']=$_POST['json'];
				$this->load->view('produccion/producto/4-imprimir/config',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function imprimir_productos(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				if(isset($_POST['v1'])){ if($_POST['v1']!="ok"){ $listado['v1']="ok";} }
				if(isset($_POST['v2'])){ if($_POST['v2']!="ok"){ $listado['v2']="ok";} }
				if(isset($_POST['v3'])){ if($_POST['v3']!="ok"){ $listado['v3']="ok";} }
				if(isset($_POST['v4'])){ if($_POST['v4']!="ok"){ $listado['v4']="ok";} }
				if(isset($_POST['v5'])){ if($_POST['v5']!="ok"){ $listado['v5']="ok";} }
				if(isset($_POST['v6'])){ if($_POST['v6']!="ok"){ $listado['v6']="ok";} }
				if(isset($_POST['v7'])){ if($_POST['v7']!="ok"){ $listado['v7']="ok";} }
				if(isset($_POST['v8'])){ if($_POST['v8']!="ok"){ $listado['v8']="ok";} }
				if(isset($_POST['v9'])){ if($_POST['v9']!="ok"){ $listado['v9']="ok";} }
				if(isset($_POST['v10'])){ if($_POST['v10']!="ok"){ $listado['v10']="ok"; } }
				if(isset($_POST['v11'])){ if($_POST['v11']!="ok"){ $listado['v11']="ok"; } }
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=34;}
				$listado['productos']=$_POST['json'];
				$this->load->view('produccion/producto/4-imprimir/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	/*--- End Reportes ---*/
   		/*-FICHA TECNICA DEL PRODUCTO-*/
		public function ficha_tecnica(){
			if(!empty($this->session_id)){
				$idp=trim($_POST['id']);
				$listado['productos']=$this->M_producto->get($idp);
				$listado['producto_materiales']=$this->M_producto_material->get_where_producto_materiales($idp);
				$listado['materiales_liquidos']=$this->M_material_liquido->get_suma_material_liquido($idp);//material liquido  de reccorrido
				$this->load->view('produccion/producto/5-reporte/ficha_tecnica',$listado);
			}else{
				echo "logout";
			}
		}
		/*-END FICHA TECNICA DEL PRODUCTO-*/
		/*-COSTO TOTAL DE PRODUCCION DEL PRODUCTO-*/
		public function costo_total_produccion(){
				if(!empty($this->session_id)){
					$idp=trim($_POST['id']);
					$listado['producto']=$this->M_producto->get($idp);
					$accesorios=$this->M_producto->get_accesorios($idp);
					$materiales=$this->M_producto->get_materiales($idp);
					$materiales_liquidos=$this->M_producto->get_materiales_liquidos($idp);
					$indirectos=$this->M_producto->get_materiales_indirectos($idp);
					$listado['materiales']=$this->lib->costo_materiales($accesorios,$materiales,$materiales_liquidos,$indirectos,"");
					$procesos=$this->M_producto->get_procesos($idp);
					$procesos_pieza=$this->M_producto->get_procesos_pieza($idp);
					$listado['procesos']=$this->lib->costo_procesos($procesos,$procesos_pieza);
					$listado['materiales_color_producto']=$this->lib->costo_materiales_color($materiales);
					$this->load->view('produccion/producto/5-reporte/costo_total_produccion',$listado);
				}else{
					echo "logout";
				}
		}
		/*-END COSTO TOTAL DE PRODUCCION DEL PRODUCTO-*/
		/*----DETALLE DE PROCESOS----*/
		public function search_detalle_procesos(){
			if(!empty($this->session_id)){
				if(isset($_POST['idp'])){
					$idp=$_POST['idp'];
					$producto=$this->M_producto->get($idp);
					if(!empty($producto)){
						$listado['producto']=$producto;
						$this->load->view('produccion/producto/5-reporte/procesos/search',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "logout";
			}
		}
		public function detalle_procesos(){
			if(!empty($this->session_id)){
				if(isset($_POST['idp'])){
					$idp=$_POST['idp'];
					$producto=$this->M_producto->get($idp);
					if(!empty($producto)){
						$listado['producto']=$producto;
						if(isset($_POST['nro'])){
							$listado['nro']=$_POST['nro'];
						}else{
							$listado['nro']=33;
						}
						$procesos=$this->M_producto->get_procesos($idp);
						$procesos_pieza=$this->M_producto->get_procesos_pieza($idp);
						$listado['procesos']=$this->lib->costo_procesos($procesos,$procesos_pieza);
						$this->load->view('produccion/producto/5-reporte/procesos/view',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "logout";
			}
		}
		/*----END DETALLE DE PROCESOS----*/
   	/*--- configuracion ---*/
	public function form_update_producto(){
		if(!empty($this->session_id)){
			if(isset($_POST['idp'])){
				$idp=trim($_POST['idp']);
				$producto=$this->M_producto->get($idp);
				if(!empty($producto)){
					$listado['grupos']=$this->M_producto_grupo->get_all();
					$listado['producto']=$producto;
					$this->load->view('produccion/producto/6-config/modificar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_producto(){//guardar modificar producto
		if(!empty($this->session_id)){
			if(isset($_POST['idp']) && isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['dis']) && isset($_POST['fec']) && isset($_POST['des']) && isset($_POST['gru']) && isset($_POST['peso']) && isset($_POST['etcu']) && isset($_POST['corr']) && isset($_POST['pllo']) && isset($_POST['relo']) && isset($_POST['etlo']) && isset($_POST['repu']) && isset($_POST['cidelante']) && isset($_POST['cidetras']) && isset($_POST['cidentro']) && isset($_POST['alto']) && isset($_POST['ancho']) && isset($_POST['largo']) && isset($_POST['car'])){
				$idp=trim($_POST['idp']);
				$cod=trim($_POST['cod']);
				$nom=trim($_POST['nom']);
				$dis=trim($_POST['dis']);
				$fec=trim($_POST['fec']);
				$des=trim($_POST['des']);
				$gru=trim($_POST['gru']);
				//$cu=trim($_POST['cu']);
				$peso=trim($_POST['peso']);
				$etcu=trim($_POST['etcu']);
				$corr=trim($_POST['corr']);
				$pllo=trim($_POST['pllo']);
				$relo=trim($_POST['relo']);
				$etlo=trim($_POST['etlo']);
				$repu=trim($_POST['repu']);
				$cidelante=trim($_POST['cidelante']);
				$cidetras=trim($_POST['cidetras']);
				$cidentro=trim($_POST['cidentro']);
				$alto=trim($_POST['alto']);
				$ancho=trim($_POST['ancho']);
				$largo=trim($_POST['largo']);
				$car=trim($_POST['car']);
				if($this->val->entero($idp,0,10) && $this->val->strSpace($cod,2,20) && $this->val->strSpace($nom,3,150) && $this->val->strSpace($dis,3,100) && $this->val->fecha($fec) && $this->val->entero($gru,0,10)){
					if($this->M_producto->modificar($idp,$gru,$cod,$nom,$fec,$des,$dis,$peso,$etlo,$pllo,$relo,$etcu,$corr,$cidelante,$cidetras,$cidentro,$largo,$alto,$ancho,$repu,$car)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
		/*---CONTROL DE MATERIALES Y MATERIALES INDIRECTOS EN EL PRODUCTO---*/
			public function materiales(){
				if(!empty($this->session_id)){
					if (isset($_POST['idp'])){
						$idp=trim($_POST['idp']);
						$producto=$this->M_producto->get($idp);
						if(!empty($producto)){
							$listado['producto']=$producto;
							$listado['materiales']=$this->M_producto_material->get_where_producto_materiales($idp);
							$listado['materiales_indirectos']=$this->M_producto_material_adicional->get_material_idp($idp);
							$this->load->view('produccion/producto/6-config/materiales',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "logout";
				}
			}
			/*--- Materiales ---*/
			public function search_material_producto(){
				if(!empty($this->session_id)){
					if(isset($_POST['idp'])){
						$idp=$_POST['idp'];
						if($idp!=""){
							$listado['idp']=$idp;
							$listado['almacenes']=$this->M_almacen->get_material();
							$listado['grupos']=$this->M_grupo->get_all();
							$this->load->view('produccion/producto/6-config/materiales/search',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "logout";
				}
			}
			public function view_material_producto(){
				if(!empty($this->session_id)){
					if(isset($_POST['idp'])){
						$idp=$_POST['idp'];
						if($idp!=""){
							if(isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['alm'])){
								$cod=$_POST['cod'];
								$nom=$_POST['nom'];
								$alm=$_POST['alm'];
								if($cod!=""){
										$atrib="mi.codigo";$val=$cod;
									}else{
										if($nom!=""){
											$atrib="mi.nombre";$val=$nom;
										}else{
											$atrib="";$val="";
										}
									}
								if($this->val->entero($alm,1,10)){
									if($atrib!="" && $val!=""){
										$listado['materiales']=$this->M_almacen_material->search($alm,$atrib,$val);
									}else{
										$listado['materiales']=$this->M_almacen_material->get_material_almacen($alm);
									}
								}else{
									if($atrib!="" && $val!=""){
										$listado['materiales']=$this->M_material_item->get_row_search($atrib,$val,'material');
									}else{
										$listado['materiales']=$this->M_material_item->get_material();
									}
								}
							}else{
								$listado['materiales']=$this->M_material_item->get_material();
							}
							$listado['idp']=$idp;
							$this->load->view('produccion/producto/6-config/materiales/view',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "logout";
				}
			}
			public function save_producto_material(){
					if(!empty($this->session_id)){
						if(isset($_POST['idp']) && isset($_POST['idm'])){
							$idp=trim($_POST['idp']);
							$idm=trim($_POST['idm']);
							$control=$this->M_producto_material->get_col_2('idp',$idp,'idm',$idm);
							if(empty($control)){
								if($this->M_producto_material->insertar($idp,$idm,0)){
									echo "1|ok";
								}else{
									echo "|error";
								}
							}else{
								if($this->M_producto_material->eliminar($control[0]->idpm)){
									echo "0|ok";
								}else{
									echo "|error";
								}
							}
						}else{
							echo "|fail";
						}
					}else{
						echo "|logout";
					}
			}
			public function update_cantidad_material(){
				if(!empty($this->session_id)){
					if(isset($_POST['idpm']) && isset($_POST['can'])){
						$idpm=trim($_POST['idpm']);
						$can=trim($_POST['can']);
						if($this->val->entero($idpm,1,10) && $this->val->decimal($can,7,2) && $can>0 && $can<=9999999.99){
							if($this->M_producto_material->modificar_cantidad($idpm,$can)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "logout";
				}
			}
			public function drop_material(){
				if(!empty($this->session_id)){
					if(isset($_POST['idpm'])){
						$idpm=trim($_POST['idpm']);
						if($this->M_producto_material->eliminar($idpm)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "logout";
				}
			}
			/*--- End Material ---*/
			/*--- Material indirecto---*/
			public function search_indirecto_producto(){
				if(!empty($this->session_id)){
					if(isset($_POST['idp'])){
						$idp=$_POST['idp'];
						if($idp!=""){
							$listado['idp']=$idp;
							$this->load->view('produccion/producto/6-config/material_indirecto/search',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "logout";
				}
			}
			public function view_indirecto_producto(){
				if(!empty($this->session_id)){
					if(isset($_POST['idp'])){
						$idp=$_POST['idp'];
						if($idp!=""){
							if(isset($_POST['cod']) && isset($_POST['nom'])){
								$cod=$_POST['cod'];
								$nom=$_POST['nom'];
								if($cod!=""){
									$atrib="mi.codigo";$val=$cod;
								}else{
									if($nom!=""){
										$atrib="mi.nombre";$val=$nom;
									}else{
										$atrib="";$val="";
									}
								}
								if($atrib!="" && $val!=""){
									$listado['materiales']=$this->M_material_item->get_row_search($atrib,$val,'material_adicional');
								}else{
									$listado['materiales']=$this->M_material_item->get_material_adicional();
								}
							}else{
								$listado['materiales']=$this->M_material_item->get_material_adicional();
							}
							$listado['idp']=$idp;
							$this->load->view('produccion/producto/6-config/material_indirecto/view',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "logout";
				}
			}
			public function save_producto_indirecto(){
				if(!empty($this->session_id)){
					if(isset($_POST['idp']) && isset($_POST['idma'])){
						$idp=trim($_POST['idp']);
						$idma=trim($_POST['idma']);
						$control=$this->M_producto_material_adicional->get_row_2n('idma',$idma,'idp',$idp);
						if(empty($control)){
							if($this->M_producto_material_adicional->insertar($idp,$idma,0)){
								echo "1|ok";
							}else{
								echo "|error";
							}
						}else{
							if($this->M_producto_material_adicional->eliminar($control[0]->idpm)){
								echo "0|ok";
							}else{
								echo "|error";
							}
						}
					}else{
						echo "|fail";
					}
				}else{
					echo "|logout";
				}
			}
			public function update_costo_indirecto(){
				if(!empty($this->session_id)){
					if(isset($_POST['idpma']) && isset($_POST['cos'])){
						$idpma=trim($_POST['idpma']);
						$cos=trim($_POST['cos']);
						if($this->val->entero($idpma,1,10) && $this->val->decimal($cos,8,4) && $cos>=0){
							if($this->M_producto_material_adicional->modificar_col($idpma,'costo',$cos)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "logout";
				}
			}
			public function drop_indirecto(){
				if(!empty($this->session_id)){
					if(isset($_POST['idpma'])){
						$idpma=trim($_POST['idpma']);
						if($this->M_producto_material_adicional->eliminar($idpma)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "logout";
				}
			}
			/*--- End Material indirecto---*/
		/*---END CONTROL DE MATERIALES Y MATERIALES INDIRECTOS EN EL PRODUCTO---*/
		/*---CONTROL DE PIEZAS EN EL PRODUCTO---*/
			public function piezas(){
				if(!empty($this->session_id)){
					if(isset($_POST['idp'])){
						$idp=trim($_POST['idp']);
						$producto=$this->M_producto->get($idp);
						if(!empty($producto)){
							$listado['producto']=$producto;
							$listado['categorias_pieza']=$this->M_categoria_pieza->get_categoria_pieza($idp);
							$listado['colores']=$this->M_color->get_all();
							$this->load->view('produccion/producto/6-config/piezas',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "logout";
				}
			}
			/*--- Manejo de grupos de piezas ---*/
				public function adicionar_grupo(){
					if(!empty($this->session_id)){
						if(isset($_POST['idp'])){
							$idp=trim($_POST['idp']);
							if($this->M_categoria_pieza->insertar($idp,0)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "logout";
					}
				}
				public function confirmar_save_color_producto(){
					if(!empty($this->session_id)){
						if(isset($_POST['idcp']) && isset($_POST['idp'])){
							$idp=$_POST['idp'];
							$idcp=$_POST['idcp'];
							$control=$this->M_categoria_pieza->get_row_2n('idp',$idp,'color_producto','1');
							if(!empty($control)){
								//vemos el grupo que representa a el color del poducto
								$categoria_producto=$this->M_categoria_producto->get_row('idcp',$control[0]->idcp);
								//verificando si el grupo tiene materiales
								if(!empty($categoria_producto)){
									$control_almacen=0;
									$control_pedido=0;
									$control_cliente=0;
									for ($i=0; $i < count($categoria_producto) ; $i++) { 
										$almacen_producto=$this->M_almacen_producto->get_row('idpim',$categoria_producto[$i]->idpim);
										if(!empty($almacen_producto)){ $control_almacen+=count($almacen_producto); }
									}
									//controlando si el registro esta asiganado en los delalles de pedido
									for ($i=0; $i < count($categoria_producto) ; $i++) { 
										$detalle_pedido=$this->M_detalle_pedido->get_row('idpim',$categoria_producto[$i]->idpim);
										if(!empty($detalle_pedido)){ $control_pedido+=count($detalle_pedido); }
									}
									//controlando si el registro esta asiganado en los clientes
									for ($i=0; $i < count($categoria_producto) ; $i++) { 
										$producto_cliente=$this->M_producto_cliente->get_row('idpim',$categoria_producto[$i]->idpim);
										if(!empty($producto_cliente)){ $control_cliente+=count($producto_cliente);}
									}
									if($control_almacen==0 && $control_pedido==0 && $control_cliente==0){
										$desc="Se eliminara definitivamente el grupo, las piezas y los materiales dentro el grupo";
									}else{
										$desc="<span class='text-danger'>Imposible asignar el grupo como color de producto, pues el grupo actual seleccionado como color del producto posee materiales que se encuentran en uso en:</span>";
										if($control_almacen>0){ $desc.="<strong><p> - Almacen de productos: ".$control_almacen." registro(s)</p></strong>";}
										if($control_pedido>0){ $desc.="<strong><p> - Detalle de pedido: ".$control_pedido." registro(s)</p></strong>";}
										if($control_cliente>0){ $desc.="<strong><p> - Asignado a clientes: ".$control_cliente." registro(s)</p></strong>";}
										$listado['open_control']="false";
										$listado['control']="";
									}
								}else{
									$desc="Ingrese sus datos para activar el grupo como color del producto.";
								}
							}else{
								$desc="Ingrese sus datos para activar el grupo como color del producto.";
							}
							$listado['titulo']="seleccionar el grupo como <b>color del producto</b>";
							$listado['desc']=$desc;
							$this->load->view('estructura/form_eliminar',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "logout";
					}
				}
				public function save_color_producto(){
					if(!empty($this->session_id)){
						if(isset($_POST['idcp']) && isset($_POST['idp']) && isset($_POST['u']) && isset($_POST['p'])){
							$idcp=trim($_POST['idcp']);
							$idp=trim($_POST['idp']);
							$u=$_POST['u'];
							$p=$_POST['p'];
							if($u==$this->session->userdata("login")){
								$usuario=$this->M_empleado->validate($u,$p);
								if(!empty($usuario)){
									//controlando si existe alguno color actuvado
									$control=$this->M_categoria_pieza->get_row_2n('idp',$idp,'color_producto','1');
									if(count($control)>0){//el producto tiene asignado un color
										$categoria_producto=$this->M_categoria_producto->get_row('idcp',$control[0]->idcp);
										if(!empty($categoria_producto)){//el grupo color de producto tiene materiales
											//eliminando imagenes de los materiales del grupo
											for ($cp=0; $cp < count($categoria_producto) ; $cp++){
												$imagenes=$this->M_imagen_producto->get_row('idpim',$categoria_producto[$cp]->idpim);
												$ruta='./libraries/img/pieza_productos/';
												for ($i=0; $i < count($imagenes); $i++){
													if($this->lib->eliminar_imagen($imagenes[$i]->nombre,$ruta)){ }
												}
												if($this->M_imagen_producto->eliminar_row('idpim',$categoria_producto[$cp]->idpim)){}
											}
											//quitando la portada del producto
											if($this->M_categoria_producto->reset_portada($control[0]->idcp)){
												//apagando todos los botones de los grupos
												if($this->M_categoria_pieza->reset_color_producto($idp)){
													//encendiendo el boton del grupo actual
													if($this->M_categoria_pieza->set_col($idcp,'color_producto',1)){
														echo "ok";
													}else{
														echo "error";
													}
												}else{
													echo "error";
												}
											}else{
												echo "error";
											}
										}else{// el grupo color de producto no tiene materiales
											$this->M_categoria_pieza->reset_color_producto($idp);
											if($this->M_categoria_pieza->set_col($idcp,'color_producto',1)){
												echo "ok";
											}else{
												echo "error";
											}
										}
									}else{//el producto no tiene asigando ningun color
										if($this->M_categoria_pieza->set_col($idcp,'color_producto',1)){
											echo "ok";
										}else{
											echo "error";
										}
									}
								}else{
									echo "validate";
								}
							}else{
								echo "validate";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "logout";
					}
				}
				public function confirmar_reset_color_producto(){
					if(!empty($this->session_id)){
						if(isset($_POST['idcp']) && isset($_POST['idp'])){
							$idp=$_POST['idp'];
							$idcp=$_POST['idcp'];
							//vemos el grupo que representa a el color del poducto
							$categoria_producto=$this->M_categoria_producto->get_row('idcp',$idcp);
							//verificando si el grupo tiene materiales
							if(!empty($categoria_producto)){
								$control_almacen=0;
								$control_pedido=0;
								$control_cliente=0;
								for ($i=0; $i < count($categoria_producto) ; $i++) { 
									$almacen_producto=$this->M_almacen_producto->get_row('idpim',$categoria_producto[$i]->idpim);
									if(!empty($almacen_producto)){ $control_almacen+=count($almacen_producto); }
								}
								//controlando si el registro esta asiganado en los delalles de pedido
								for ($i=0; $i < count($categoria_producto) ; $i++) { 
									$detalle_pedido=$this->M_detalle_pedido->get_row('idpim',$categoria_producto[$i]->idpim);
									if(!empty($detalle_pedido)){ $control_pedido+=count($detalle_pedido); }
								}
								//controlando si el registro esta asiganado en los clientes
								for ($i=0; $i < count($categoria_producto) ; $i++) { 
									$producto_cliente=$this->M_producto_cliente->get_row('idpim',$categoria_producto[$i]->idpim);
									if(!empty($producto_cliente)){ $control_cliente+=count($producto_cliente);}
								}
								if($control_almacen==0 && $control_pedido==0 && $control_cliente==0){
									$desc="Se eliminara definitivamente el grupo, las piezas y los materiales dentro el grupo";
								}else{
									$desc="<span class='text-danger'>Imposible desactivar el grupo como color de producto, pues el grupo actual posee materiales que se encuentran en uso en:</span>";
									if($control_almacen>0){ $desc.="<strong><p> - Almacen de productos: ".$control_almacen." registro(s)</p></strong>"; }
									if($control_pedido>0){ $desc.="<strong><p> - Detalle de pedido: ".$control_pedido." registro(s)</p></strong>"; }
									if($control_cliente>0){ $desc.="<strong><p> - Asignado a clientes: ".$control_cliente." registro(s)</p></strong>"; }
									$listado['open_control']="false";
									$listado['control']="";
								}
							}else{
								$desc="Ingrese sus datos para desactivar el grupo como color del producto.";
							}

							$listado['titulo']="desactivar el grupo como <b>color del producto</b>";
							$listado['desc']=$desc;
							$this->load->view('estructura/form_eliminar',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "logout";
					}
				}
				public function reset_color_producto(){
					if(!empty($this->session_id)){
						if(isset($_POST['idcp']) && isset($_POST['idp']) && isset($_POST['u']) && isset($_POST['p'])){
							$idcp=trim($_POST['idcp']);
							$idp=trim($_POST['idp']);
							$u=$_POST['u'];
							$p=$_POST['p'];
							if($u==$this->session->userdata("login")){
								$usuario=$this->M_empleado->validate($u,$p);
								if(!empty($usuario)){
									$categoria_producto=$this->M_categoria_producto->get_row('idcp',$idcp);
									if(!empty($categoria_producto)){//el grupo color de producto tiene materiales
										//eliminando imagenes de los materiales del grupo
										for($cp=0; $cp < count($categoria_producto) ; $cp++){
											$imagenes=$this->M_imagen_producto->get_row('idpim',$categoria_producto[$cp]->idpim);
											$ruta='./libraries/img/pieza_productos/';
											for ($i=0; $i < count($imagenes); $i++){
												if($this->lib->eliminar_imagen($imagenes[$i]->nombre,$ruta)){ }
											}
											if($this->M_imagen_producto->eliminar_row('idpim',$categoria_producto[$cp]->idpim)){}
										}
										//quitando la portada del producto
										if($this->M_categoria_producto->reset_portada($idcp)){
											//apagando todos los botones de los grupos
											if($this->M_categoria_pieza->reset_color_producto($idp)){
												echo "ok";
											}else{
												echo "error";
											}
										}else{
											echo "error";
										}
									}else{//el grupo color de producto no tiene materiales
										//apagando todos los botones de los grupos
										if($this->M_categoria_pieza->reset_color_producto($idp)){
											echo "ok";
										}else{
											echo "error";
										}
									}
								}else{
									echo "validate";
								}
							}else{
								echo "validate";
							}
						}else{
							echo "error";
						}
					}else{
						echo "logout";
					}
				}
				public function confirmar_grupo(){
					if(!empty($this->session_id)){
						if(isset($_POST['idcp'])){
							$idcp=$_POST['idcp'];
							$grupo=$this->M_categoria_pieza->get($idcp);
							if(!empty($grupo)){
								$categoria_producto=$this->M_categoria_producto->get_row('idcp',$idcp);
								$listado['titulo']="eliminar el grupo en el producto";
								if(!empty($categoria_producto)){
									$control_almacen=0;
									$control_pedido=0;
									$control_cliente=0;
									//controlando si el registro esta asiganado en los almacenes
									for ($i=0; $i < count($categoria_producto) ; $i++) { 
										$almacen_producto=$this->M_almacen_producto->get_row('idpim',$categoria_producto[$i]->idpim);
										if(!empty($almacen_producto)){ $control_almacen+=count($almacen_producto);}
									}
									//controlando si el registro esta asiganado en los delalles de pedido
									for ($i=0; $i < count($categoria_producto) ; $i++) { 
										$detalle_pedido=$this->M_detalle_pedido->get_row('idpim',$categoria_producto[$i]->idpim);
										if(!empty($detalle_pedido)){ $control_pedido+=count($detalle_pedido);}
									}
									//controlando si el registro esta asiganado en los clientes
									for ($i=0; $i < count($categoria_producto) ; $i++) { 
										$producto_cliente=$this->M_producto_cliente->get_row('idpim',$categoria_producto[$i]->idpim);
										if(!empty($producto_cliente)){ $control_cliente+=count($producto_cliente);}
									}

									if($control_almacen==0 && $control_pedido==0 && $control_cliente==0){
										$desc="Se eliminara definitivamente el grupo, las piezas y los materiales dentro el grupo";
									}else{
										$desc=" Imposible Eliminar el grupo, pues los materiales se encuentran en uso en:";
										if($control_almacen>0){ $desc.="<strong><p> - Almacen de productos: ".$control_almacen." registro(s)</p></strong>";}
										if($control_pedido>0){ $desc.="<strong><p> - Detalle de pedido: ".$control_pedido." registro(s)</p></strong>";}
										if($control_cliente>0){ $desc.="<strong><p> - Asignado a clientes: ".$control_cliente." registro(s)</p></strong>";}
										$listado['open_control']="false";
										$listado['control']="";
									}
								}else{
									$desc="Se eliminara definitivamente el grupo";
								}
								if($grupo[0]->color_producto==1){
									$desc.=", <span class='text-danger'>ademas el grupo representa al color del producto en consecuencia se eliminara las imagenes de portada y los colores almacenados del producto.</span>";
								}
								$listado['desc']=$desc;
								$this->load->view('estructura/form_eliminar',$listado);
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "logout";
					}
				}
				public function drop_grupo(){
					if(!empty($this->session_id)){
						if(isset($_POST['idcp']) && isset($_POST['u']) && isset($_POST['p'])){
							$idcp=trim($_POST['idcp']);
							$u=$_POST['u'];
							$p=$_POST['p'];
							if($u==$this->session->userdata("login")){
								$usuario=$this->M_empleado->validate($u,$p);
								if(!empty($usuario)){
									$control=$this->M_categoria_pieza->get_row('idcp',$idcp);
									if(!empty($control)){
										//eliminamos las imagenes de las piezas
										$piezas=$this->M_pieza->get_row('idcp',$idcp);
										if(!empty($piezas)){
											$ruta='./libraries/img/piezas/';
											for($i=0; $i<count($piezas); $i++){
												if($this->lib->eliminar_imagen($piezas[$i]->imagen,$ruta)){
												}
											}
										}
										//eliminamos las imagenes del color de los productos si corresponde
										$imagenes=$this->M_categoria_producto->get_col_imagen('cp.idcp',$idcp);
										if(!empty($imagenes)){
											$ruta='./libraries/img/pieza_productos/';
											for ($i=0; $i < count($imagenes); $i++){ 
												if($this->lib->eliminar_imagen($imagenes[$i]->nombre,$ruta)){ }
											}
										}
										//eliminando todo el grupo en cascada
										if($this->M_categoria_pieza->eliminar($idcp)){
											echo "ok";
										}else{
											echo "error";
										}
									}else{
										echo "fail";
									}
								}else{
									echo "validate";
								}
							}else{
								echo "validate";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "logout";
					}
				}
			/*--- End manejo de grupos de piezas ---*/
			/*--- Manejo de piezas ---*/
			
		public function new_pieza(){
			if(!empty($this->session_id)){
				if(isset($_POST['idp']) && isset($_POST['idcp'])){
					$idp=trim($_POST['idp']);
					$idcp=trim($_POST['idcp']);
					$listado['pieza_grupos']=$this->M_pieza_grupo->get_all();
					$listado['idp']=$idp;
					$listado['idcp']=$idcp;
					$this->load->view('produccion/producto/6-config/piezas/piezas/new',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "logout";
			}
		}
		public function save_pieza(){
			if(!empty($this->session_id)){
				if(isset($_FILES) && isset($_POST['idp']) && isset($_POST['idcp']) && isset($_POST['gru']) && isset($_POST['alt']) && isset($_POST['anc']) && isset($_POST['can']) && isset($_POST['des'])){
					$idp=trim($_POST['idp']);
					$idcp=trim($_POST['idcp']);
					$gru=trim($_POST['gru']);
					$alt=trim($_POST['alt']);
					$anc=trim($_POST['anc']);
					$can=trim($_POST['can']);
					$des=trim($_POST['des']);
					if($this->val->entero($idcp,1,10) && $this->val->entero($gru,1,10) && $this->val->decimal($alt,5,2)  && $this->val->decimal($anc,5,2) && $this->val->entero($can,1,3)){
						$guardar=true;
						if($des!=""){ if(!$this->val->strSpace($des,5,500)){ $guardar=false;}}
						if($guardar){
							$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/piezas/','',$this->resize,NULL);
							if($img!="error_type" && $img!="error"){
								if($this->M_pieza->insertar($gru,$idcp,$alt,$anc,$img,$des,$can)){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo $img;
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}	
			}else{
				echo "logout";
			}
		}
		public function add_proceso_material_adicional(){
			if(!empty($this->session_id)){
				if(isset($_POST['idpi'])){
					$idpi=trim($_POST['idpi']);
					$pieza=$this->M_pieza->get_grupo($idpi);
					if(!empty($pieza)){
						$listado['pieza']=$pieza;
						$listado['procesos']=$this->M_proceso->get_all();
						$listado['materiales_liquidos']=$this->M_material_liquido->get_all();
						$this->load->view('produccion/producto/6-config/piezas/piezas/proceso_material_adicional/view',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "logout";
			}
		}
		public function save_pieza_proceso(){
			if(!empty($this->session_id)){
				if(isset($_POST['idpr']) && isset($_POST['idpi'])){
					$idpr=trim($_POST['idpr']);
					$idpi=trim($_POST['idpi']);
					$existe=$this->M_pieza_proceso->get_row_2n('idpr',$idpr,'idpi',$idpi);
					if(count($existe)>0){
						if($this->M_pieza_proceso->eliminar($existe[0]->idpipr)){
							echo "0|ok";
						}else{
							echo "|error";
						}
					}else{
						if($this->M_pieza_proceso->insertar($idpr,$idpi)){
							echo "1|ok";
						}else{
							echo "|error";
						}
					}
				}else{
					echo "fail";
				}				
			}else{
				echo "|logout";
			}
		}
		public function save_pieza_material_adicional(){
			if(!empty($this->session_id)){
				$idml=trim($_POST['idml']);
				$idpi=trim($_POST['idpi']);
				
				$existe=$this->M_pieza_material_liquido->get_row_2n('idml',$idml,'idpi',$idpi);
				if(count($existe)>0){
					if($this->M_pieza_material_liquido->eliminar($existe[0]->idpml)){
						echo "0|ok";
					}else{
						echo "|error";
					}
				}else{
					if($this->M_pieza_material_liquido->insertar($idml,$idpi)){
						echo "1|ok";
					}else{
						echo "|error";
					}
				}
				
			}else{
				echo "|logout";
			}
		}
		public function modificar_pieza(){
			if(!empty($this->session_id)){
				if(isset($_POST['idpi']) && isset($_POST['idcp']) && isset($_POST['idp'])){
					$idpi=trim($_POST['idpi']);
					$listado['pieza']=$this->M_pieza->get($idpi);
					$listado['pieza_grupos']=$this->M_pieza_grupo->get_all();
					$listado['idcp']=trim($_POST['idcp']);
					$listado['idp']=trim($_POST['idp']);
					$this->load->view('produccion/producto/6-config/piezas/piezas/modificar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "logout";
			}
		}
		public function update_pieza(){
			if(!empty($this->session_id)){
				if(isset($_FILES) && isset($_POST['idpi']) && isset($_POST['idcp']) && isset($_POST['gru']) && isset($_POST['alt']) && isset($_POST['anc']) && isset($_POST['can']) && isset($_POST['des'])){
					$idpi=trim($_POST['idpi']);
					$idcp=trim($_POST['idcp']);
					$gru=trim($_POST['gru']);
					$alt=trim($_POST['alt']);
					$anc=trim($_POST['anc']);
					$can=trim($_POST['can']);
					$des=trim($_POST['des']);
					if($this->val->entero($idpi,1,10) && $this->val->entero($idcp,1,10) && $this->val->entero($gru,1,10) && $this->val->decimal($alt,5,2)  && $this->val->decimal($anc,5,2) && $this->val->entero($can,1,3)){
						$guardar=true;
						if($des!=""){ if(!$this->val->strSpace($des,5,500)){ $guardar=false;}}
						if($guardar){
							$pieza=$this->M_pieza->get($idpi);
							if(!empty($pieza)){
								$img=$pieza[0]->imagen;
								$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/piezas/','',$this->resize,$img,$idpi);
								if($img!="error_type" && $img!="error"){
									if($this->M_pieza->modificar($idpi,$gru,$idcp,$alt,$anc,$img,$des,$can)){
										echo "ok";
									}else{
										echo "error";
									}
								}else{
									echo $img;
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}			
			}else{
				echo "logout";
			}
		}
		public function confirmar_pieza(){
			if(!empty($this->session_id)){
				if(isset($_POST['idpi'])){
					$idpi=$_POST['idpi'];
					$pieza=$this->M_pieza->get($idpi);
					if(!empty($pieza)){
						$url="./libraries/img/piezas/miniatura/";$img="default.png";
						if($pieza[0]->imagen!=NULL && $pieza[0]->imagen!=""){ $img=$pieza[0]->imagen;}
						$control_proceso=$this->M_pieza_proceso->get_row('idpi',$idpi);
						$control_material=$this->M_pieza_material_liquido->get_row('idpi',$idpi);
						$listado['titulo']="eliminar la pieza en el producto.";
						$listado['img']=$url.$img;
						if(count($control_proceso)>0 && count($control_material)>0){
							$listado['desc']="Se eliminara definitivamente la pieza en el producto, ademas esta pieza se encuentra asignada con materiales y procesos,<span class='text-danger'> si elimina la pieza puede influir en el costo de producción del producto</span>";
						}else{
							$listado['desc']="Se eliminara definitivamente la pieza en el producto.";
						}
						$this->load->view('estructura/form_eliminar',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "logout";
			}
		}
	   	public function drop_pieza(){
			if(!empty($this->session_id)){
				if(isset($_POST['idpi']) && isset($_POST['u']) && isset($_POST['p'])){
					$idpi=$_POST['idpi'];
					$u=$_POST['u'];
					$p=$_POST['p'];
					if($u==$this->session->userdata("login")){
						$usuario=$this->M_empleado->validate($u,$p);
						if(!empty($usuario)){
							$pieza=$this->M_pieza->get($idpi);
							if(!empty($pieza)){
								if($this->lib->eliminar_imagen($pieza[0]->imagen,'./libraries/img/piezas/')){
									if($this->M_pieza->eliminar($pieza[0]->idpi)){
										echo "ok";
									}else{
										echo "error";
									}
								}else{
									echo "error";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "validate";
						}
					}else{
						echo "validate";
					}
				}else{
					echo "fail";
				}			
			}else{
				echo "logout";
			}
		}
	/*--- Manejo de materiales en el grupo de piezas ---*/
		public function search_material_grupo(){
			if(!empty($this->session_id)){
				if(isset($_POST['idcp']) && isset($_POST['idp'])){
					$idcp=$_POST['idcp'];
					$idp=$_POST['idp'];
					if($idcp!="" && $idp!=""){
						$listado['idcp']=$idcp;
						$listado['idp']=$idp;
						$listado['almacenes']=$this->M_almacen->get_material();
						$listado['grupos']=$this->M_grupo->get_all();
						$this->load->view('produccion/producto/6-config/piezas/materiales/search',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "logout";
			}
		}
		public function view_material_grupo(){
			if(!empty($this->session_id)){
				if(isset($_POST['idcp']) && isset($_POST['idp'])){
					$idcp=$_POST['idcp'];
					$idp=$_POST['idp'];
					if($idcp!=""){
						if(isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['alm'])){
							$cod=$_POST['cod'];
							$nom=$_POST['nom'];
							$alm=$_POST['alm'];
							if($cod!=""){
									$atrib="mi.codigo";$val=$cod;
								}else{
									if($nom!=""){
										$atrib="mi.nombre";$val=$nom;
									}else{
										$atrib="";$val="";
									}
								}
							if($this->val->entero($alm,1,10)){
								if($atrib!="" && $val!=""){
									$listado['materiales']=$this->M_almacen_material->search($alm,$atrib,$val);
								}else{
									$listado['materiales']=$this->M_almacen_material->get_material_almacen($alm);
								}
							}else{
								if($atrib!="" && $val!=""){
									$listado['materiales']=$this->M_material_item->get_row_search($atrib,$val,'material');
								}else{
									$listado['materiales']=$this->M_material_item->get_material();
								}
							}
						}else{
							$listado['materiales']=$this->M_material_item->get_material();
						}
						$listado['idcp']=$idcp;
						$listado['idp']=$idp;
						$this->load->view('produccion/producto/6-config/piezas/materiales/view',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "logout";
			}
		}
		public function adicionar_material_grupo(){
			if(!empty($this->session_id)){
				if(isset($_POST['idcp']) && isset($_POST['idm'])){
					$idcp=trim($_POST['idcp']);
					$idm=trim($_POST['idm']);
					//$control=$this->M_categoria_producto->get_row_2n('idcp',$idcp,'idm',$idm);
					//if(empty($control)){
						$color=$this->M_material->get_material_atributo_idm($idm,"color");
						if(!empty($color)){
							if($this->M_categoria_producto->insertar($idcp,$idm,0,$color[0]->idco)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					/*}else{
						echo "fail";
					}*/
				}else{
					echo "fail";
				}
			}else{
				echo "logout";
			}
		}
		public function update_color_pieza(){
			if(!empty($this->session_id)){
				if(isset($_POST['idpim']) && isset($_POST['idco'])){
					$idpim=trim($_POST['idpim']);
					$idco=trim($_POST['idco']);
					if($this->val->entero($idpim,0,10) && $this->val->entero($idco,0,10)){
						if($this->M_categoria_producto->modificar_row($idpim,'idco',$idco)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "logout";
			}			
		}
		public function add_imagen_pieza_material(){
			if(!empty($this->session_id)){
				if(isset($_POST['idpim']) && isset($_POST['idp'])){
					$idpim=trim($_POST['idpim']);
					$idp=trim($_POST['idp']);
					$listado['imagenes']=$this->M_imagen_producto->get_pieza_material($idpim);
					$listado['idp']=$idp;
					$listado['idpim']=$idpim;
					$this->load->view('produccion/producto/6-config/piezas/materiales/imagen/add',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "logout";
			}
		}
		public function save_imagen_pieza_material(){
			if(!empty($this->session_id)){
				if(isset($_POST['idpim']) && isset($_FILES)){
					$ruta='./libraries/img/pieza_productos/';
					for($i=0; $i<count($_FILES) ;$i++){
						$img=$this->lib->subir_imagen_miniatura($_FILES,$ruta,$i.'',$this->resize,'');
						if($img!="error_type" & $img!="error"){
							if($this->M_imagen_producto->insertar($_POST['idpim'],$img)){
							}
						}
					}
					echo "ok";
				}else{
					echo "fail";
				}				
			}else{
				echo "	logout";
			}
		}
		public function drop_imagen_pieza_material(){
			if(!empty($this->session_id)){
				if(isset($_POST['idim'])){
					$idim=trim($_POST['idim']);
					$control=$this->M_imagen_producto->get($idim);
					if(!empty($control)){
						if($this->M_imagen_producto->eliminar($idim)){
							if($this->lib->eliminar_imagen($control[0]->nombre,'./libraries/img/pieza_productos/')){
							}
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "ok";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "logout";
			}
		}

		public function confirmar_material_grupo(){
			if(!empty($this->session_id)){
				if(isset($_POST['idpim'])){
					$idpim=$_POST['idpim'];
					$material_grupo=$this->M_categoria_producto->get_categoria_producto_material($idpim);
					if(!empty($material_grupo)){
						//controlando si el registro esta asiganado en los almacenes
						$control_almacen=true;
						$almacen_producto=$this->M_almacen_producto->get_row('idpim',$material_grupo[0]->idpim);
						if(!empty($almacen_producto)){ $control_almacen=false; }
						//controlando si el registro esta asiganado en los delalles de pedido
						$control_pedido=true;
						$detalle_pedido=$this->M_detalle_pedido->get_row('idpim',$material_grupo[0]->idpim);
						if(!empty($detalle_pedido)){ $control_pedido=false; }
						//controlando si el registro esta asiganado en los clientes
						$control_cliente=true;
						$producto_cliente=$this->M_producto_cliente->get_row('idpim',$material_grupo[0]->idpim);
						if(!empty($producto_cliente)){ $control_cliente=false;}
						if($control_almacen && $control_pedido && $control_cliente){
							$desc="Se eliminara definitivamente el material del grupo.<br><span class='text-danger'> Si elimina la pieza puede influir en el costo de producción del producto</span>";
						}else{
							$desc=" Imposible Eliminar, pues los materiales se encuentran en uso en:";
							if(count($almacen_producto)>0){ $desc.="<strong><p> - Almacen de productos: ".count($almacen_producto)." registro(s)</p></strong>";}
							if(count($detalle_pedido)>0){ $desc.="<strong><p> - Detalle de pedido: ".count($detalle_pedido)." registro(s)</p></strong>";}
							if(count($producto_cliente)>0){ $desc.="<strong><p> - Asignado a clientes: ".count($producto_cliente)." registro(s)</p></strong>";}
							$desc.="<span class='text-danger'>Debe eliminar los registros anteriores para poder eliminar el material</span>";
							$listado['open_control']="false";
							$listado['control']="";
						}
						$url="./libraries/img/materiales/miniatura/";
						$img="default.png";
						if($material_grupo[0]->fotografia!=NULL && $material_grupo[0]->fotografia!=""){ $img=$material_grupo[0]->fotografia;}
						$listado['titulo']="eliminar el material del grupo";
						$listado['img']=$url.$img;
						$listado['desc']=$desc;
						$this->load->view('estructura/form_eliminar',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "logout";
			}
		}
		public function drop_material_grupo(){
			if(!empty($this->session_id)){
				if(isset($_POST['idpim']) && isset($_POST['u']) && isset($_POST['p'])){
					$idpim=$_POST['idpim'];
					$u=$_POST['u'];
					$p=$_POST['p'];
					if($u==$this->session->userdata("login")){
						$usuario=$this->M_empleado->validate($u,$p);
						if(!empty($usuario)){
							$categoria=$this->M_categoria_producto->get($idpim);
							if(!empty($categoria)){
								//eliminamos las imagenes del color de los productos si corresponde
								$imagenes=$this->M_categoria_producto->get_col_imagen('cp.idpim',$idpim);
								if(!empty($imagenes)){
									$ruta='./libraries/img/pieza_productos/';
									for ($i=0; $i < count($imagenes); $i++){ 
										if($this->lib->eliminar_imagen($imagenes[$i]->nombre,$ruta)){
										}
									}
								}
								//eliminando todo el grupo en cascada
								if($this->M_categoria_producto->eliminar($idpim)){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "validate";
						}
					}else{
						echo "validate";
					}
				}else{
					echo "fail";
				}			
			}else{
				echo "logout";
			}
		}
		public function save_portada(){
			if(!empty($this->session_id)){
				if(isset($_POST['idpim']) && isset($_POST['idcp']) && isset($_POST['idp'])){
					$idpim=trim($_POST['idpim']);
					$idcp=trim($_POST['idcp']);
					$idp=trim($_POST['idp']);
					$categorias=$this->M_categoria_pieza->get_categoria_pieza($idp);
					for ($i=0; $i < count($categorias) ; $i++){ $categoria=$categorias[$i]; 
						if($this->M_categoria_producto->reset_portada($categoria->idcp)){
						}
					}			
					if($this->M_categoria_producto->set_col($idpim,'portada',1)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "logout";
			}
		}
		public function reset_portada(){
			if(!empty($this->session_id)){
				if(isset($_POST['idp'])){
					$idpim=trim($_POST['idpim']);
					$idcp=trim($_POST['idcp']);
					$categorias=$this->M_categoria_pieza->get_categoria_pieza(trim($_POST['idp']));
					for ($i=0; $i < count($categorias) ; $i++) {$categoria=$categorias[$i]; 
						if($this->M_categoria_producto->reset_portada($categoria->idcp)){
						}
					}
					echo "ok";
				}else{
					echo "fail";
				}
			}else{
				echo "logout";
			}
		}

	/*--- End manejo de materiales en el grupo de piezas ---*/
	/*--- Manejo de pproceso en le producto ---*/
		public function procesos(){
			if(!empty($this->session_id)){
				if(isset($_POST['idp'])){
					$idp=trim($_POST['idp']);
					$listado['producto']=$this->M_producto->get($idp);
					$listado['procesos']=$this->M_proceso->get_all();
					$listado['producto_procesos']=$this->M_producto_proceso->get_producto_proceso($idp);
					$listado['procesos_pieza']=$this->M_categoria_pieza->get_proceso_piezas($idp);
					$this->load->view('produccion/producto/6-config/procesos',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "logout";
			}
		}
		public function save_producto_proceso(){
			if(!empty($this->session_id)){
				if(isset($_POST['idp']) && isset($_POST['pro']) && isset($_POST['nom']) && isset($_POST['h']) && isset($_POST['m']) && isset($_POST['s']) && isset($_POST['cos'])){
					$idp=$_POST['idp'];
					$pro=$_POST['pro'];
					$nom=$_POST['nom'];
					$h=$_POST['h'];
					$m=$_POST['m'];
					$s=$_POST['s'];
					$cos=$_POST['cos'];
					if($this->val->entero($idp,1,10) && $this->val->entero($pro,1,10) && $this->val->strSpace($nom,2,200) && $this->val->entero($h,1,4) && $h>=0 && $h<=9999 && $this->val->entero($m,1,2) && $m>=0 && $m<=59 && $this->val->entero($s,1,2) && $s>=0 && $s<=59 && $this->val->decimal($cos,4,2) && $cos>0 && $cos<=9999.99){
						$tiempo=($h*60*60)+($m*60)+$s;
						if($this->M_producto_proceso->insertar($idp,$pro,$nom,$tiempo,$cos)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "logout";
			}
		}
		public function save_tiempo_costo_pieza_proceso(){
			if(!empty($this->session_id)){
				if(isset($_POST['idp']) && isset($_POST['idpipr']) && isset($_POST['h']) && isset($_POST['m']) && isset($_POST['s']) && isset($_POST['cos'])){
					$idp=trim($_POST['idp']);
					$idpipr=trim($_POST['idpipr']);
					$h=trim($_POST['h']);
					$m=trim($_POST['m']);
					$s=trim($_POST['s']);
					$cos=trim($_POST['cos']);
					if($this->val->entero($idp,1,10) && $this->val->entero($idpipr,1,10) && $this->val->entero($h,1,4) && $h>=0 && $h<=9999 && $this->val->entero($m,1,2) && $m>=0 && $m<=59 && $this->val->entero($s,1,2) && $s>=0 && $s<=59 && $this->val->decimal($cos,4,2) && $cos>0 && $cos<=9999.99){
						$tiempo=($h*60*60)+($m*60)+$s;
						if($this->M_pieza_proceso->modificar_tiempo_estimado_costo($idpipr,$tiempo,$cos)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}				
			}else{
				echo "logout";
			}
		}
		public function save_tiempo_costo(){
			if(!empty($this->session_id)){
				if(isset($_POST['idp']) && isset($_POST['idpipr']) && isset($_POST['pro']) && isset($_POST['nom']) && isset($_POST['h']) && isset($_POST['m']) && isset($_POST['s']) && isset($_POST['cos'])){
					$idp=$_POST['idp'];
					$idpipr=$_POST['idpipr'];
					$pro=$_POST['pro'];
					$nom=$_POST['nom'];
					$h=$_POST['h'];
					$m=$_POST['m'];
					$s=$_POST['s'];
					$cos=$_POST['cos'];
					if($this->val->entero($idp,1,10) && $this->val->entero($idpipr,1,10) && $this->val->entero($pro,1,10) && $this->val->strSpace($nom,2,200) && $this->val->entero($h,1,4) && $h>=0 && $h<=9999 && $this->val->entero($m,1,2) && $m>=0 && $m<=59 && $this->val->entero($s,1,2) && $s>=0 && $s<=59 && $this->val->decimal($cos,4,2) && $cos>0 && $cos<=9999.99){
						$tiempo=($h*60*60)+($m*60)+$s;
						if($this->M_producto_proceso->modificar($idpipr,$idp,$pro,$nom,$tiempo,$cos)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}				
			}else{
				echo "logout";
			}
		}
		public function drop_producto_proceso(){
			if(!empty($this->session_id)){
				if(isset($_POST['idppr'])){
					$idppr=trim($_POST['idppr']);
					if($this->M_producto_proceso->eliminar($idppr)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "logout";
			}
		}

	/*--- End manejo de pproceso en le producto ---*/
	/*----ELIMINAR PRODUCTO----*/
   	public function confirmar_producto(){
		if(!empty($this->session_id)){
			if(isset($_POST['idp']) && isset($_POST['img']) ){
				$idp=$_POST['idp'];
				$img=$_POST['img'];
				$producto=$this->M_producto->get($idp);
				$desc="Se eliminara definitivamente el producto";
				$categoria_producto=$this->M_producto->get_categorias_producto($idp);
				if(!empty($categoria_producto)){
					$control_almacen=0;
					$control_pedido=0;
					$control_cliente=0;
					for ($cp=0; $cp < count($categoria_producto); $cp++) { $res=$categoria_producto[$cp];
						if($res->color_producto=='1'){
							//controlando si el registro esta asiganado en los almacenes
							$almacen_producto=$this->M_almacen_producto->get_row('idpim',$res->idpim);
							if(!empty($almacen_producto)){ $control_almacen+=count($almacen_producto); }
							//controlando si el registro esta asiganado en los delalles de pedido
							$detalle_pedido=$this->M_detalle_pedido->get_row('idpim',$res->idpim);
							if(!empty($detalle_pedido)){ $control_pedido+=count($detalle_pedido); }
							//controlando si el registro esta asiganado en los clientes
							$producto_cliente=$this->M_producto_cliente->get_row('idpim',$res->idpim);
							if(!empty($producto_cliente)){ $control_cliente+=count($producto_cliente);}
						}
					}
					if($control_almacen!=0 || $control_pedido!=0 || $control_cliente!=0){
						$desc=" Imposible Eliminar el producto, pues el producto se encuentra en uso en:";
						if($control_almacen>0){ $desc.="<strong><p> - Almacen de productos: ".$control_almacen." registro(s)</p></strong>";}
						if($control_pedido>0){ $desc.="<strong><p> - Detalle de pedido: ".$control_pedido." registro(s)</p></strong>";}
						if($control_cliente>0){ $desc.="<strong><p> - Asignado a clientes: ".$control_cliente." registro(s)</p></strong>";}
						$desc.="<span class='text-danger'>Debe eliminar los lugares donde este siendo usado e producto para poder eliminarlo</span>";
						$listado['open_control']="false";
						$listado['control']="";
					}
				}
				$listado['titulo']="eliminar el producto <b>".$producto[0]->nombre."</b> definitivamente";
				$listado['img']=$img;
				$listado['desc']=$desc;
				$this->load->view('estructura/form_eliminar',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function drop_producto(){
		if(!empty($this->session_id)){
			if(isset($_POST['idp']) && isset($_POST['u']) && isset($_POST['p'])){
				$idp=$_POST['idp'];
				$u=$_POST['u'];
				$p=$_POST['p'];
				if($u==$this->session->userdata("login")){
					$usuario=$this->M_empleado->validate($u,$p);
					if(!empty($usuario)){
						$categoria_producto=$this->M_producto->get_categorias_producto($idp);
						if(!empty($categoria_producto)){
							for ($cp=0; $cp < count($categoria_producto); $cp++) { $res=$categoria_producto[$cp];
								$imagenes=$this->M_imagen_producto->get_row('idpim',$res->idpim);
								$ruta='./libraries/img/pieza_productos/';
								for($i=0; $i < count($imagenes); $i++){
									if($this->lib->eliminar_imagen($imagenes[$i]->nombre,$ruta)){ }
								}
							}
						}
						$piezas=$this->M_producto->get_piezas($idp);
						if(!empty($piezas)){
							for($pi=0; $pi < count($piezas) ; $pi++){
								$ruta='./libraries/img/piezas/';
								if($this->lib->eliminar_imagen($piezas[$pi]->imagen,$ruta)){ }
							}
						}
						if($this->M_producto->eliminar($idp)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "validate";
					}
				}else{
					echo "validate";
				}
			}else{
				echo "fail";
			}			
		}else{
			echo "logout";
		}
	}

	/*----END ELIMINAR PRODUCTO----*/
/*------- END MANEJO DE PRODUCTOS -------*/
/*------- MANEJO DE ORDEN DE PRODUCCION -------*/
	public function search_orden(){
		if(!empty($this->session_id)){
			$listado['clientes']=$this->M_cliente->get_all();
			$this->load->view('produccion/orden_produccion/search',$listado);
		}else{
			echo "logout";
		}
	}
	public function view_orden(){
		if(!empty($this->session_id)){
			if(isset($_POST['num']) && isset($_POST['cli']) && isset($_POST['fpe']) && isset($_POST['fen']) && isset($_POST['tip'])){
				$col="";$val="";
				if($_POST['num']!=""){
					$col='p.numero';$val=$_POST['num'];
				}else{
					if($_POST['cli']!=""){
						$col='cl.idcl';$val=$_POST['cli'];
					}else{
						if($_POST['tip']==0 || $_POST['tip']==1){
							if($_POST['fpe']!=""){
								if($_POST['fen']!=""){
									if($_POST['tip']==0){
										$col="fecha_pedido";$val=$_POST['fpe']."|".$_POST['fen'];
									}else{
										$col="fecha_entrega";$val=$_POST['fpe']."|".$_POST['fen'];
									}
								}else{
									$col="p.fecha_pedido";$val=$_POST['fpe']."|".$_POST['fen'];
								}
							}else{
								if($_POST['fen']!=""){
									$col="p.fecha_entrega";$val=$_POST['fpe']."|".$_POST['fen'];
								}
							}
						}						
					}
				}
				if($col!="" && $val!=""){
					$listado['pedidos']=$this->M_pedido->get_search($col,$val);
				}else{
					$listado['pedidos']=$this->M_pedido->get_all_complet();
				}
			}else{
				$listado['pedidos']=$this->M_pedido->get_all_complet();
			}
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('produccion/orden_produccion/view',$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Reportes ---*/
	public function tiempo_tarea(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpe'])){
				$idpe=$_POST['idpe'];
				if(isset($_POST['nro'])){ $listado['nro']=$nro;}else{ $listado['nro']=34;}
				$listado['productos']=$this->M_detalle_pedido->get_producto($idpe);//sacamos productos
				$listado['feriados']=$this->M_feriado->get_all();
				$listado['idpe']=$idpe;
				$this->load->view('produccion/orden_produccion/5-reportes/tareas',$listado);
			}
		}else{
			echo "logout";
		}
	}
	public function procesos_piezas(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpe'])){
				$idpe=$_POST['idpe'];
				if(isset($_POST['nro'])){ $listado['nro']=$nro;}else{ $listado['nro']=34;}
				$listado['empleados']=$this->M_empleado_producto->get_empleados($idpe,"0");
				$listado['idpe']=$idpe;
				$this->load->view('produccion/orden_produccion/5-reportes/procesos_piezas',$listado);
			}
		}else{
			echo "logout";
		}
	}
	public function search_empleado_productos(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpe']) && isset($_POST['ide'])){
				$idpe=$_POST['idpe'];
				$ide=$_POST['ide'];
				if($this->val->entero($idpe,0,10) && $this->val->entero($ide,0,10)){
						$listado['productos']=$this->M_pedido->get_productos($idpe);//sacamos productos
						$listado['productos_empleado']=$this->M_empleado_producto->get_productos_empleado($idpe,$ide);
						$empleado=$this->M_empleado->get($ide);
						$listado['empleado']=$empleado[0];
						$this->load->view('produccion/orden_produccion/5-reportes/procesos_piezas/view',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function para_armador(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpe'])){
				$idpe=$_POST['idpe'];
				if(isset($_POST['nro'])){ $listado['nro']=$nro;}else{ $listado['nro']=34;}
				$listado['procesos']=$this->M_proceso->get_all();
				$listado['idpe']=$idpe;
				$this->load->view('produccion/orden_produccion/5-reportes/para_armado/armado',$listado);
			}
		}else{
			echo "logout";
		}
	}
	public function procesos_general(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpe'])){
				$idpe=$_POST['idpe'];
				if(isset($_POST['nro'])){ $listado['nro']=$nro;}else{ $listado['nro']=34;}
				$listado['procesos']=$this->M_proceso->get_all();
				$listado['idpe']=$idpe;
				$this->load->view('produccion/orden_produccion/5-reportes/procesos_general',$listado);
			}
		}else{
			echo "logout";
		}
	}
	public function search_procesos_general(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpe']) && isset($_POST['proc'])){
				$idpe=$_POST['idpe'];
				$proc=$_POST['proc'];
				if($this->val->entero($idpe,0,10) && $this->val->entero($proc,0,10)){
					$procesos=$this->M_proceso->get($proc);
					if(!empty($procesos)){
						if(isset($_POST['emp'])){
							$encargado=$this->M_empleado->get($_POST['emp']);
							if(!empty($encargado)){
								$listado['encargado']=$encargado[0];
							}
						}
						$listado['empleados']=$this->M_empleado_proceso->get_empleado_proceso($proc);
						$listado['productos']=$this->M_pedido->get_productos($idpe);//sacamos productos
						$listado['proceso']=$procesos[0];
						$listado['idpe']=$idpe;
						$this->load->view('produccion/orden_produccion/5-reportes/procesos_general/view',$listado);
					}else{
						echo "fail";	
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
	public function view_orden_trabajo(){
		if(!empty($this->session_id)){
			if(isset($_POST['id'])){
				$idpe=$_POST['id'];
				$listado['productos']=$this->M_detalle_pedido->get_producto($idpe);//sacamos productos
				$listado['empleados']=$this->M_empleado->get_all();
				$listado['idpe']=$idpe;
				$this->load->view('produccion/orden_produccion/6-config/tareas',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function asignar_empleado(){
		if(!empty($this->session_id)){
			if(isset($_POST['iddp']) && isset($_POST['ide']) && isset($_POST['tip'])){
				$ide=$_POST['ide'];
				$iddp=$_POST['iddp'];
				$tipo=$_POST['tip'];
				if($this->val->entero($ide,0,10) && $this->val->entero($iddp,0,10) && ($tipo==0 || $tipo==1)){
					if($this->M_empleado_producto->insertar($ide,$iddp,$tipo,date('Y-m-d'))){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function modificar_producto_empleado(){
		if(!empty($this->session_id)){
			if(isset($_POST['iddp']) && isset($_POST['idepr']) && isset($_POST['ide']) && isset($_POST['tip'])){
				$ide=$_POST['ide'];
				$iddp=$_POST['iddp'];
				$idepr=$_POST['idepr'];
				$tipo=$_POST['tip'];
				if($this->val->entero($iddp,0,10) && $this->val->entero($ide,0,10) && $this->val->entero($idepr,0,10) && ($tipo==0 || $tipo==1)){
					if($this->M_empleado_producto->modificar($idepr,$ide,$iddp,$tipo)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function drop_producto_empleado(){
		if(!empty($this->session_id)){
			if(isset($_POST['idepr'])){
				$idepr=$_POST['idepr'];
				if($this->val->entero($idepr,0,10)){
					if($this->M_empleado_producto->eliminar($idepr)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}











	public function search_tarea(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpim']) && isset($_POST['idp']) && isset($_POST['idpe']) && isset($_POST['iddp']) && isset($_POST['pr']) && isset($_POST['can'])){
				$idpim=trim($_POST['idpim']);
				$idp=trim($_POST['idp']);
				$idpe=trim($_POST['idpe']);
				$iddp=trim($_POST['iddp']);
				$can=trim($_POST['can']);
				$pr=explode("|", trim($_POST['pr']));
				if($this->val->entero($idpim,0,10) && $this->val->entero($idp,0,10) && $this->val->entero($idpe,0,10) && count($pr)==2){
					if($pr[0]=="pip" || $pr[0]=="prp"){
						$idpr=$pr[1];
						if($pr[0]=="pip"){
							$listado['tareas']=$this->M_categoria_pieza->get_tareas_proceso($idp,$pr[1]);$tipo=$pr[0];
						}else{
							$listado['tareas']=$this->M_producto_proceso->get_tareas_proceso($idp,$pr[1]);$tipo=$pr[0];
						}
						$listado['empleados']=$this->M_empleado_proceso->get_empleado_proceso($pr[1]);
						$listado['all_tareas']=$this->M_pedido_tarea->get_row('iddp',$iddp);
						$listado['idpe']=$idpe;
						$listado['idp']=$idp;
						$listado['idpim']=$idpim;
						$listado['iddp']=$iddp;
						$listado['can']=$can;
						$listado['tipo']=$tipo;
						$listado['idpr']=$idpr;
						$this->load->view('produccion/orden_produccion/6-config/tareas/view',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function save_tarea_empleado(){
		if(!empty($this->session_id)){
			if(isset($_POST['idproceso']) && isset($_POST['iddp']) && isset($_POST['idpim']) && isset($_POST['vide'])){
				$idproceso=trim($_POST['idproceso']);
				$iddp=trim($_POST['iddp']);
				$idpim=trim($_POST['idpim']);
				$vide=explode("|",$_POST['vide']);
				if($this->val->entero($idproceso,0,10) && $this->val->entero($iddp,0,10) && $this->val->entero($idpim,0,10) && count($vide)==2){
					$tipo=$vide[0];
					$ide=$vide[1];
					//verificando ids válidos
					if($tipo=="pip" || $tipo="prp"){
						if($tipo=="pip"){
							$tarea_proceso=$this->M_pieza_proceso->get($idproceso);
						}else{
							$tarea_proceso=$this->M_producto_proceso->get($idproceso);
						}
						$detalle_pedido=$this->M_detalle_pedido->get($iddp);
						$empleado=$this->M_empleado->get_row('ide',$ide);
						if(!empty($tarea_proceso) && !empty($detalle_pedido) && !empty($empleado)){
							//verificando si existe ya el registro
							if($tipo=="pip"){
								$tarea=$this->M_pedido_tarea->get_row_2n('iddp',$iddp,'idpipr',$idproceso);
							}else{
								$tarea=$this->M_pedido_tarea->get_row_2n('iddp',$iddp,'idppr',$idproceso);
							}
							if(empty($tarea)){
								if($tipo=="pip"){// caso pieza proceso
									$aux=$this->M_pedido_tarea->max_row('iddp',$iddp,'ot');
									$ot=$aux[0]->max+1;
									if($this->M_pedido_tarea->insertar_tarea($iddp,$ide,$idproceso,"",date("Y-m-d"),$ot)){
										echo "ok";
									}else{
										echo "error";
									}
								}else{// caso producto proceso
									$aux=$this->M_pedido_tarea->max_row('iddp',$iddp,'ot');
									$ot=$aux[0]->max+1;
									if($this->M_pedido_tarea->insertar_tarea($iddp,$ide,"",$idproceso,date("Y-m-d"),$ot)){
										echo "ok";
									}else{
										echo "error";
									}
								}
							}else{
								if($tipo=="pip"){// caso pieza proceso
									if($this->M_pedido_tarea->modificar_tarea($tarea[0]->idpt,$iddp,$ide,$idproceso,"",date("Y-m-d"))){
										echo "ok";
									}else{
										echo "error";
									}
								}else{// caso producto proceso
									if($this->M_pedido_tarea->modificar_tarea($tarea[0]->idpt,$iddp,$ide,"",$idproceso,date("Y-m-d"))){
										echo "ok";
									}else{
										echo "error";
									}
								}
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function save_tarea_all_empleado(){
		if(!empty($this->session_id)){
			if(isset($_POST['idp']) && isset($_POST['idpr']) && isset($_POST['iddp']) && isset($_POST['ide']) && isset($_POST['tipo'])){
				$idp=$_POST['idp'];
				$idpr=$_POST['idpr'];
				$iddp=$_POST['iddp'];
				$ide=$_POST['ide'];
				$tipo=$_POST['tipo'];
				if($this->val->entero($idp,0,10) && $this->val->entero($idpr,0,10) && $this->val->entero($iddp,0,10) && $this->val->entero($ide,0,10) && ($tipo=="pip" || $tipo=="prp")){
					if($tipo=="pip"){
						$tareas=$this->M_categoria_pieza->get_tareas_proceso($idp,$idpr);
						for($i=0; $i < count($tareas) ; $i++){ $tarea=$tareas[$i];
							//buscando si ya exite la tarea asignada
							$control=$this->M_pedido_tarea->get_row_2n('iddp',$iddp,'idpipr',$tarea->idpipr);
							if(empty($control)){
								$aux=$this->M_pedido_tarea->max_row('iddp',$iddp,'ot');
								$ot=$aux[0]->max+1;
								if($this->M_pedido_tarea->insertar_tarea($iddp,$ide,$tarea->idpipr,"",date("Y-m-d"),$ot)){}
							}else{
								if($this->M_pedido_tarea->modificar_tarea($control[0]->idpt,$iddp,$ide,$tarea->idpipr,"",date("Y-m-d"))){}
							}
						}
						echo "ok";
					}else{
						$tareas=$this->M_producto_proceso->get_tareas_proceso($idp,$idpr);
						for($i=0; $i < count($tareas) ; $i++){ $tarea=$tareas[$i];
							//buscando si ya exite la tarea asignada
							$control=$this->M_pedido_tarea->get_row_2n('iddp',$iddp,'idppr',$tarea->idppr);
							if(empty($control)){
								$aux=$this->M_pedido_tarea->max_row('iddp',$iddp,'ot');
								$ot=$aux[0]->max+1;
								if($this->M_pedido_tarea->insertar_tarea($iddp,$ide,"",$tarea->idppr,date("Y-m-d"),$ot)){}
							}else{
								if($this->M_pedido_tarea->modificar_tarea($control[0]->idpt,$iddp,$ide,"",$tarea->idppr,date("Y-m-d"))){}
							}
						}
						echo "ok";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function reset_date(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpt'])){
				$idpt=$_POST['idpt'];
				if($this->M_pedido_tarea->reset_date($idpt)){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function confirmar_tarea(){
   		if(!empty($this->session_id)){
   			if(isset($_POST['idpt'])){
				$idpt=$_POST['idpt'];
				//$url="./libraries/img/personas/miniatura/";
				$tarea=$this->M_pedido_tarea->get($idpt);
				if(count($tarea)>0){
					if($tarea[0]->fecha_inicio!=NULL || $tarea[0]->fecha_inicio!=""){
						if($tarea[0]->fecha_fin!=NULL || $tarea[0]->fecha_fin!=""){
							$listado['desc']="La tarea actualmente se encuentra ya concluida en su elaboración, ¿desea eliminarlar la tarea?.";
						}else{
							$listado['desc']="Imposible Eliminar: La tarea actualmente se encuentra en proceso de elaboración";
							$listado['open_control']="true";
							$listado['control']="";
						}
					}else{
						$listado['desc']="Se eliminaran definitivamente la tarea asignada.";
					}
					//$img='default.png';
					//if($empleado[0]->fotografia!=NULL && $empleado[0]->fotografia!=""){ $img=$empleado[0]->fotografia; }
					//$listado['img']=$url.$img;
					$listado['titulo']="eliminar la tarea del empleado en planta";
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
   		}else{
   			echo "logout";
   		}
   	}
   	public function drop_tarea(){
		if(!empty($this->session_id)){
			$idpt=$_POST['idpt'];
			$u=$_POST['u'];
			$p=$_POST['p'];
			if(strtolower($u)==strtolower($this->session->userdata("login"))){
				$usuario=$this->M_empleado->validate($u,$p);
				if(!empty($usuario)){
						$tarea=$this->M_pedido_tarea->get($idpt);
						if(!empty($tarea)){
							if($this->M_pedido_tarea->eliminar($idpt)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}			
				}else{
					echo "validate";
				}
			}else{
				echo "validate";
			}
		}else{
			echo "logout";
		}
	}
   	public function change_estado(){
		if(!empty($this->session_id)){
			if(isset($_POST['id']) && isset($_POST['tabla']) && isset($_POST['estado'])){
				$id=$_POST['id'];
				$tabla=$_POST['tabla'];
				$estado=$_POST['estado'];
				if($this->val->entero($id,0,10) && $tabla>0 && $tabla<4 && $estado>=0 && $estado<=2){
					switch ($tabla) {
						case '1':
							if($this->M_pedido->modificar_row_id($id,'estado',$estado)){
								echo "ok";
							}else{
								echo "error";
							}
							break;
						case '2':
							if($this->M_detalle_pedido->modificar_row_id($id,'estado',$estado)){
								echo "ok";
							}else{
								echo "error";
							}
							break;
						case '3':
							if($this->M_pedido_tarea->modificar_row_id($id,'estado',$estado)){
								echo "ok";
							}else{
								echo "error";
							}
							break;
						default:
							echo "fail";
							break;
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}















	/*public function save_tarea_proceso_pieza(){
		if(!empty($this->session_id)){
			if(isset($_POST['iddp']) && isset($_POST['idpim']) && isset($_POST['idp']) && isset($_POST['idpr']) && isset($_POST['ide'])){
				$iddp=$_POST['iddp'];
				$idpim=$_POST['idpim'];
				$idp=$_POST['idp'];
				$idpr=$_POST['idpr'];
				$ide=$_POST['ide'];
				if($iddp!="" && $idp!="" && $idpr!="" && $ide!=""){
					//sacamos todos los proceso de tipo idpr del producto de detalle de pedido
					$control=$this->M_pedido_tarea->search_registro_proceso($iddp,$idpr,$idpim);
					if(!empty($control)){//no existen regros de procesos asignados
						for ($i=0; $i < count($control) ; $i++) { $c=$control[$i];
							if($this->M_pedido_tarea->eliminar($c->idpt)){
							}
						}
					}
					$procesos=$this->M_categoria_pieza->get_proceso_piezas_where($idp,'pipr.idpr',$idpr);
					$count=0;
					for ($i=0; $i < count($procesos) ; $i++) { $p=$procesos[$i];
						if($this->M_pedido_tarea->insertar_pieza_proceso($iddp,$p->idpipr,$ide,date('Y-m-d'))){
							$count++;
						}
					}
					if(count($procesos)==$count){
						echo "ok";
					}else{
						echo "ok";
					}

				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}*/
	public function save_tarea_pieza(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpipr']) && isset($_POST['iddp']) && isset($_POST['ide']) && isset($_POST['idpim'])){
				$idpipr=$_POST['idpipr'];
				$iddp=$_POST['iddp'];
				$ide=$_POST['ide'];
				$idpim=$_POST['idpim'];
				$control=$this->M_pedido_tarea->search_registro($iddp,$idpipr,$idpim);
				if(!empty($control)){
					if($this->M_pedido_tarea->eliminar($control[0]->idpt)){
					}
				}
				if($this->M_pedido_tarea->insertar_pieza_proceso($iddp,$idpipr,$ide,date('Y-m-d'))){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	/*public function save_tarea_empleado(){
		if(!empty($this->session_id)){
			$iddp=$_POST['iddp'];
			$idppr=$_POST['idppr'];
			$ide=$_POST['ide'];
			$id3pe=$this->M_pedido_tarea->exist_pedido_proceso($iddp,$idppr);
			if($id3pe!=""){
				if($this->M_pedido_tarea->modificar($id3pe,$iddp,$idppr,$ide,0,date('Y-m-d'))){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				if($this->M_pedido_tarea->insertar($iddp,$idppr,$ide,0,date('Y-m-d'))){
					echo "ok";
				}else{
					echo "error";
				}
			}
		}else{
			echo "logout";
		}
	}*/

   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE ORDEN DE PRODUCCION -------*/
/*------- MANEJO DE CONFIGURACION -------*/
	public function view_config(){
		if(!empty($this->session_id)){
			$listado['pieza_grupos']=$this->M_pieza_grupo->get_all();
			$listado['procesos']=$this->M_proceso->get_all();
			$listado['materiales_liquidos']=$this->M_material_liquido->get_all();
			$listado['producto_grupos']=$this->M_producto_grupo->get_all();
			$this->load->view('produccion/configuracion/view',$listado);
		}else{
			echo "logout";
		}
	}

   	/*--- Manejo de grupo de piezas ---*/
   	public function save_pieza_grupo(){
		if(!empty($this->session_id)){
			if(isset($_POST['nom']) && isset($_POST['des'])){
				$nom=trim($_POST['nom']);
				$des=trim($_POST['des']);
				if($this->val->strSpace($nom,2,100)){
					$guardar=true;
					if($des!=""){ if(!$this->val->strSpace($des,2,200)){ $guardar=false;}}
					if($guardar){
						if($this->M_pieza_grupo->insertar($nom,$des)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{

			}
		}else{
			echo "logout";
		}
	}
	public function update_pieza_grupo(){
		if(!empty($this->session_id)){
			if(isset($_POST['nom']) && isset($_POST['des']) && isset($_POST['idpig'])){
				$idpig=trim($_POST['idpig']);
				$nom=trim($_POST['nom']);
				$des=trim($_POST['des']);
				if($this->val->strSpace($nom,2,100)){
					$guardar=true;
					if($des!=""){ if(!$this->val->strSpace($des,2,200)){ $guardar=false;}}
					if($guardar){
						if($this->M_pieza_grupo->modificar($idpig,$nom,$des)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function drop_pieza_grupo(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpig'])){
				$idpig=$_POST['idpig'];
				if($this->M_pieza_grupo->eliminar($idpig)){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End manejo de grupo de piezas ---*/
   	/*--- Manejo de materailes liquidos ---*/
   			public function search_material_liquido(){
				if(!empty($this->session_id)){
						$listado['almacenes']=$this->M_almacen->get_material();
						$listado['grupos']=$this->M_grupo->get_all();
						$this->load->view('produccion/configuracion/materiales/search',$listado);
				}else{
					echo "logout";
				}
			}
			public function view_material_liquido(){
				if(!empty($this->session_id)){
							if(isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['alm'])){
								$cod=$_POST['cod'];
								$nom=$_POST['nom'];
								$alm=$_POST['alm'];
								if($cod!=""){
										$atrib="mi.codigo";$val=$cod;
									}else{
										if($nom!=""){
											$atrib="mi.nombre";$val=$nom;
										}else{
											$atrib="";$val="";
										}
									}
								if($this->val->entero($alm,1,10)){
									if($atrib!="" && $val!=""){
										$listado['materiales']=$this->M_almacen_material->search($alm,$atrib,$val);
									}else{
										$listado['materiales']=$this->M_almacen_material->get_material_almacen($alm);
									}
									$listado['ida']=$alm;
								}else{
									if($atrib!="" && $val!=""){
										$listado['materiales']=$this->M_material_item->get_row_search($atrib,$val,'material');
									}else{
										$listado['materiales']=$this->M_material_item->get_material();
									}
									$listado['ida']="";
								}
							}else{
								$listado['materiales']=$this->M_material_item->get_material();
								$listado['ida']="";
							}
							$listado['materiales_liquidos']=$this->M_material_liquido->get_all();
							$this->load->view('produccion/configuracion/materiales/view',$listado);
				}else{
					echo "logout";
				}
			}
			public function save_material_liquido(){
					if(!empty($this->session_id)){
						if(isset($_POST['idm']) && isset($_POST['den']) && isset($_POST['tip'])){
							$idm=trim($_POST['idm']);
							$den=trim($_POST['den']);
							$tip=trim($_POST['tip']);
							if($this->val->entero($idm,1,10) && $this->val->decimal($den,2,4) && $den>=0 && $den<=99.9999 && $this->val->entero($tip,1,1)){
								$control=$this->M_material_liquido->get_row('idm',$idm);
								if(empty($control)){
									if($this->M_material_liquido->insertar($idm,$den,$tip)){
										echo "1|ok";
									}else{
										echo "|error";
									}
								}else{
									if($this->M_material_liquido->eliminar($control[0]->idml)){
										echo "0|ok";
									}else{
										echo "|error";
									}
								}
							}else{
								echo "|fail";
							}
						}else{
							echo "|fail";
						}
					}else{
						echo "|logout";
					}
			}
		public function update_material_liquido(){
			if(!empty($this->session_id)){
				if(isset($_POST['idml']) && isset($_POST['den']) && isset($_POST['tip'])){
					$idml=trim($_POST['idml']);
					$den=trim($_POST['den']);
					$tip=trim($_POST['tip']);
					if($this->val->entero($idml,1,10) && $this->val->decimal($den,2,4) && $den>=0 && $den<=99.9999 && $this->val->entero($tip,1,1)){
						if($this->M_material_liquido->modificar($idml,$den,$tip)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "logout";
			}
		}
		public function drop_material_liquido(){
			if(!empty($this->session_id)){
				if(isset($_POST['idml'])){
					$idml=$_POST['idml'];
					if($this->M_material_liquido->eliminar($idml)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "logout";
			}
		}
   	/*--- End manejo de materiales liquidos ---*/
	/*--- Manejo de configuracion de procesos*/
   	public function save_proceso(){
		if(!empty($this->session_id)){
			if(isset($_POST['nom']) && isset($_POST['des'])){
				$nom=trim($_POST['nom']);
				$des=trim($_POST['des']);
				if($this->val->strSpace($nom,2,100)){
					$guardar=true;
					if($des!=""){ if(!$this->val->strSpace($des,2,200)){ $guardar=false;}}
					if($guardar){
						if($this->M_proceso->insertar($nom,$des)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{

			}
		}else{
			echo "logout";
		}
	}
	public function update_proceso(){
		if(!empty($this->session_id)){
			if(isset($_POST['nom']) && isset($_POST['des']) && isset($_POST['idpr'])){
				$idpr=trim($_POST['idpr']);
				$nom=trim($_POST['nom']);
				$des=trim($_POST['des']);
				if($this->val->strSpace($nom,2,100)){
					$guardar=true;
					if($des!=""){ if(!$this->val->strSpace($des,2,200)){ $guardar=false;}}
					if($guardar){
						if($this->M_proceso->modificar($idpr,$nom,$des)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function drop_proceso(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpr'])){
				$idpr=$_POST['idpr'];
				if($this->M_proceso->eliminar($idpr)){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	/*--- End manejo de configuracion de procesos ---*/
   	/*--- Manejo de grupo de productos ---*/
   	public function save_producto_grupo(){
		if(!empty($this->session_id)){
			if(isset($_POST['nom']) && isset($_POST['des'])){
				$nom=trim($_POST['nom']);
				$des=trim($_POST['des']);
				if($this->val->strSpace($nom,2,100)){
					$guardar=true;
					if($des!=""){ if(!$this->val->strSpace($des,2,200)){ $guardar=false;}}
					if($guardar){
						if($this->M_producto_grupo->insertar(0,$nom,$des)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{

			}
		}else{
			echo "logout";
		}
	}
	public function update_producto_grupo(){
		if(!empty($this->session_id)){
			if(isset($_POST['nom']) && isset($_POST['des']) && isset($_POST['idpg'])){
				$idpg=trim($_POST['idpg']);
				$nom=trim($_POST['nom']);
				$des=trim($_POST['des']);
				if($this->val->strSpace($nom,2,100)){
					$guardar=true;
					if($des!=""){ if(!$this->val->strSpace($des,2,200)){ $guardar=false;}}
					if($guardar){
						if($this->M_producto_grupo->modificar($idpg,0,$nom,$des)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function drop_producto_grupo(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpg'])){
				$idpg=$_POST['idpg'];
				if($this->M_producto_grupo->eliminar($idpg)){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End manejo de grupo de productos ---*/
/*------- END MANEJO DE CONFIGURACION -------*/
}
/* End of file produccion.php */
/* Location: ./application/controllers/produccion.php */