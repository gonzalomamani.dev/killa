<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Taller extends CI_Controller {
	public function __construct(){
		parent::__construct();
	}
	public function index(){
		$listado['privilegio']="";
		$this->load->view('v_taller',$listado);
	}
	public function search_empleado(){
		$listado['privilegio']="";
		$this->load->view('taller/search',$listado);
	}
	public function view_empleado(){
		$atrib="";$val="";
		if(isset($_POST['nombre'])){
			if($_POST['nombre']!=""){
				$atrib="nombre";$val=$_POST['nombre'];
			}
		}
		$listado['empleados']=$this->M_empleado->get_search($atrib,$val);
		$this->load->view('taller/view',$listado);
	}
	public function confirmar(){
		if(isset($_POST['ide'])){
			$ide=$_POST['ide'];
			if($this->val->entero($ide,0,10)){
				$empleado=$this->M_empleado->get_search('e.ide',$ide);
				if(!empty($empleado)){
					//if($this->M_empleado->modificar_password($ide,$empleado[0]->ci)){ }
					$url="./libraries/img/personas/miniatura/";
					$img="default.png";
					if($empleado[0]->fotografia!=NULL && $empleado[0]->fotografia!=""){ $img=$empleado[0]->fotografia;}

					//$listado['detalle_pedidos_producto_procesos']=$this->M_pedido_tarea->get_detalle_pedido_pieza_proceso($ide);
					$listado['empleado']=$empleado[0];
					$this->load->view('taller/password/view',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "fail";
		}		
	}
	public function validar(){
		if(isset($_POST['id']) && isset($_POST['p'])){
			$ide=$_POST['id'];
			$pass=$_POST['p'];
			if($this->val->alfaNumerico($pass,4,25)){
				$password=$this->lib->password($pass);
	      		$empleado=$this->M_empleado->get_row('ide',$ide);
	      		if(!empty($empleado)){
	      			if($password==$empleado[0]->password){
	      				echo "ok";
	      			}else{
	      				echo "validate";
	      			}
	      		}else{
	      			echo "fail";
	      		}
			}else{
				echo "fail";
			}
		}else{
			echo "fail";
		}
	}
	public function view_tareas(){
		if(isset($_POST['id'])){
			$ide=$_POST['id'];
			if($this->val->entero($ide,0,10)){
				$empleado=$this->M_empleado->get_search('e.ide',$ide);
				if(!empty($empleado)){
					$listado['pedido_tareas']=$this->M_pedido_tarea->get_row_complet('e.ide',$ide);
					$listado['empleado']=$empleado;
					$this->load->view('taller/tareas/tareas',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "fail";
		}		
	}
	public function config_pass(){
		if(isset($_POST['ide'])){
			$ide=$_POST['ide'];
			if($this->val->entero($ide,0,10)){
				$empleado=$this->M_empleado->get($ide);
				if(!empty($empleado)){
					$listado['empleado']=$empleado[0];
					$this->load->view('taller/tareas/password/view',$listado);
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "fail";
		}
	}
	public function change_pass(){
		if(isset($_POST['ide']) && isset($_POST['pass']) && isset($_POST['n_pass']) && isset($_POST['n_pass2'])){
			$ide=$_POST['ide'];
			$pass=$_POST['pass'];
			$n_pass=$_POST['n_pass'];
			$n_pass2=$_POST['n_pass2'];
			if($this->val->entero($ide,0,10) && $this->val->alfaNumerico($pass,4,25) && $this->val->alfaNumerico($n_pass,4,25) && $this->val->alfaNumerico($n_pass2,4,25)){
	      		$empleado=$this->M_empleado->get_row('ide',$ide);
	      		if(!empty($empleado)){
		      		$password=$this->lib->password($pass);
	      			if($password==$empleado[0]->password){
	      				if($n_pass==$n_pass2){
	      					$password=$this->lib->password($n_pass);
		      				if($this->M_empleado->modificar_row($ide,'password',$password)){
		      					echo "ok";
		      				}else{
		      					echo "error";
		      				}
	      				}else{
	      					echo "validate_distint";
	      				}
	      			}else{
	      				echo "validate";
	      			}
	      		}else{
	      			echo "fail";
	      		}
			}else{
				echo "fail";
			}
		}else{
			echo "fail";
		}
	}	
	public function control_tarea(){// cambia horaa de tarea
		if(isset($_POST['idpt']) && isset($_POST['ide']) && isset($_POST['obs'])){
			$idpt=$_POST['idpt'];
			$ide=$_POST['ide'];
			$obs=$_POST['obs'];
			if($this->val->entero($idpt,0,10) && $this->val->entero($ide,0,10) && $this->val->textarea($obs,0,500)){
				$tarea=$this->M_pedido_tarea->get_row_2n('idpt',$idpt,'ide',$ide);
				if(!empty($tarea)){
					if($tarea[0]->fecha_inicio==NULL || $tarea[0]->fecha_inicio==""){
						if($this->M_pedido_tarea->modificar_row_2n($idpt,'fecha_inicio',date('Y-m-d H:i:s'),'observacion_inicio',$obs)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						if($this->M_pedido_tarea->modificar_row_2n($idpt,'fecha_fin',date('Y-m-d H:i:s'),'observacion_fin',$obs)){
							echo "ok";
						}else{
							echo "error";
						}
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "fail";
		}
	}


/*----- FICHA TECNICA ------*/
	public function ficha_tecnica(){
		if(isset($_POST['idp']) && isset($_POST['idpim']) && isset($_POST['iddp'])){
			$idp=trim($_POST['idp']);
			$idpim=trim($_POST['idpim']);
			$iddp=trim($_POST['iddp']);
			$producto=$this->M_producto->get($idp);
			if(!empty($producto) && $this->val->entero($idpim,0,10) && $this->val->entero($iddp,0,10)){
				$listado['producto']=$producto;
				$listado['producto_materiales']=$this->M_producto_material->get_where_producto_materiales($idp);
				$listado['materiales_liquidos']=$this->M_material_liquido->get_suma_material_liquido($idp);//material liquido  de reccorrido
				$listado['idpim']=$idpim;
				$listado['iddp']=$iddp;
				$this->load->view('taller/tareas/ficha_tecnica/ficha',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "fail";
		}
	}
	public function materiales(){
		if(isset($_POST['idp']) && isset($_POST['idpim']) && isset($_POST['iddp'])){
			$idp=trim($_POST['idp']);
			$idpim=trim($_POST['idpim']);
			$iddp=trim($_POST['iddp']);
			$producto=$this->M_producto->get($idp);
			$detalle_pedido=$this->M_detalle_pedido->get($iddp);
			if(!empty($producto) && !empty($detalle_pedido) && $this->val->entero($idpim,0,10) && $this->val->entero($iddp,0,10)){
				$listado['producto']=$producto;
				$accesorios=$this->M_producto->get_accesorios($idp);
				$materiales=$this->M_producto->get_materiales($idp);
				$materiales_liquidos=$this->M_producto->get_materiales_liquidos($idp);
				$indirectos=$this->M_producto->get_materiales_indirectos($idp);
				$listado['materiales']=$this->lib->costo_materiales($accesorios,$materiales,$materiales_liquidos,$indirectos,"");
				$listado['materiales_color_producto']=$this->lib->costo_materiales_color($materiales);
				$listado['detalle_pedido']=$detalle_pedido[0];
				$listado['idpim']=$idpim;
				$listado['iddp']=$iddp;
				$this->load->view('taller/tareas/ficha_tecnica/materiales',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "fail";
		}
	}
/*----- END FICHA TECNICA ------*/
/*------- ALERTA ---------*/
	public function alerta(){
			if(isset($_POST['titulo'])){
				$listado['titulo']=$_POST['titulo'];
			}else{
				$listado['titulo']="";
			}
			if(isset($_POST['descripcion'])){
				$listado['desc']=$_POST['descripcion'];
			}else{
				$listado['desc']="";
			}
			$listado['img']=base_url().'libraries/img/sistema/warning.png';
			$listado['control']="";
			$listado['open_control']="false";
			$this->load->view('estructura/form_eliminar',$listado);
	}
/*------- END ALERTA ---------*/
/*------ HISTORIAL -------*/
	public function view_historial(){
		if(isset($_POST['id'])){
			$ide=$_POST['id'];
			if($this->val->entero($ide,0,10)){
				$empleado=$this->M_empleado->get_search('e.ide',$ide);
				if(!empty($empleado)){
					$listado['pedido_tareas']=$this->M_pedido_tarea->get_row_complet('e.ide',$ide);
					$listado['empleado']=$empleado;
					$this->load->view('taller/historial/historial',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "fail";
		}	
	}
/*------ END HISTORIAL -------*/
		
}
/* End of file taller.php */
/* Location: ./application/controllers/taller.php */