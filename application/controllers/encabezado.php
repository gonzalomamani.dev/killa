<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produccion extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('login');
	}
	public function index(){
		if(!empty($this->session_id)){
			$listado['privilegio']="";
			$this->load->view('encabezado',$listado);
		}else{
			redirect(base_url().'login/input',301);
		}
	}
	
	
}

/* End of file produccion.php */
/* Location: ./application/controllers/produccion.php */