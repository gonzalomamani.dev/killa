<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contabilidad extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('login');
	}
	public function index(){
		if(!empty($this->session_id)){
			if(!isset($_GET['p'])){
				$listado['pestania']=1;
			}else{
				$listado['pestania']=$_GET['p'];
			}
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('v_contabilidad',$listado);
			//echo "usuario logeado";
		}else{
			redirect(base_url().'login',301);
		}
	}

/*------- MANEJO DE COMPROBANTES -------*/
	public function search_comprobante(){
		if(!empty($this->session_id)){
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('contabilidad/comprobante/search',$listado);
		}else{
			echo "logout";
		}
	}
	public function view_comprobante(){
		if(!empty($this->session_id)){
			$atrib="";$val="";
			$atrib2="";$val2="";
			if(isset($_POST['tip'])){
				if($_POST['tip']=="E" || $_POST['tip']=="I" || $_POST['tip']=="T"){
					$atrib='c.tipo';$val=$_POST['tip'];
				}
			}
			if(isset($_POST['fol'])){
				if($this->val->entero($_POST['fol'],0,10)){
					$atrib2='c.folio';$val2=$_POST['fol'];
				}
			}
			$fecha1="";$fecha2="";
			if(isset($_POST['f1']) && isset($_POST['f2'])){
				if($this->val->fecha($_POST['f1']) && $this->val->fecha($_POST['f2'])){
					$fecha1=$_POST['f1'];
					$fecha2=$_POST['f2'];
				}
			}
			if($fecha1=="" && $fecha2==""){
				$fecha1=date('Y-m-d');
				$vf=explode("-", $fecha1);
				$fecha1=$vf[0]."-".$vf[1]."-01";
				$fecha2=$vf[0]."-".$vf[1]."-".$this->validaciones->ultimo_dia($vf[1],$vf[0]);
			}
			$listado['fecha1']=$fecha1;
			$listado['fecha2']=$fecha2;
			$listado['comprobantes']=$this->M_comprobante->get_search($atrib,$val,$atrib2,$val2,$fecha1,$fecha2);
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('contabilidad/comprobante/view',$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Nuevo ---*/
   	public function new_comprobante(){
   		if(!empty($this->session_id)){
			$listado['cuentas']=$this->M_plan_cuenta->get_all();
			$this->load->view('contabilidad/comprobante/3-nuevo/view',$listado);
		}else{
			echo "logout";
		}
	}
   	public function refresh_nro_cmp(){
   		if(!empty($this->session_id)){
   			if(isset($_POST['tip']) && isset($_POST['fec'])){
   				$tip=$_POST['tip'];
   				$fec=$_POST['fec'];
   				if(($tip=="I" || $tip=="E" || $tip=="T") && $this->val->fecha($fec)){
					$vf=explode("-", $fec);
					$fecha1=$vf[0]."-".$vf[1]."-01";
					$fecha2=$vf[0]."-".$vf[1]."-".$this->validaciones->ultimo_dia($vf[1],$vf[0]);
					$max=$this->M_comprobante->max_folio($tip,$fecha1,$fecha2);
					echo $max[0]->folio+1;
   				}else{
   					echo "fail";
   				}
   			}else{
   				echo "fail";
   			}
		}else{
			echo "logout";
		}
	}
	public function add_row_cuenta(){
		if(!empty($this->session_id)){
			if(isset($_POST['row'])){
				$listado['row']=$_POST['row']+1;
				$listado['cuentas']=$this->M_plan_cuenta->get_row('tipo','1');
				$this->load->view('contabilidad/comprobante/3-nuevo/row/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function save_comprobante(){
   		if(!empty($this->session_id)){
   			if(isset($_POST['ctas']) && isset($_POST['nro']) && isset($_POST['tip']) && isset($_POST['fec']) && isset($_POST['glo'])){
   				$ctas=explode("[|]", trim($_POST['ctas']));
	   			$nro=trim($_POST['nro']);
	   			$tip=trim($_POST['tip']);
	   			$fec=trim($_POST['fec']);
	   			$glo=trim($_POST['glo']);
	   			if(count($ctas)>0 && $this->val->entero($nro,0,10) && $nro>0 && ($tip=="T" || $tip=="E" || $tip=="I") && $this->val->fecha($fec) && $this->val->textarea($glo,0,200)){
	   				$vf=explode("-", $fec);
					$fecha1=$vf[0]."-".$vf[1]."-01";
					$fecha2=$vf[0]."-".$vf[1]."-".$this->validaciones->ultimo_dia($vf[1],$vf[0]);
	   				$control=$this->M_comprobante->get_search('c.tipo',$tip,'c.folio',$nro,$fecha1,$fecha2);
	   				if(empty($control)){
		   				$aux=$this->M_comprobante->max_id('idcm');
		   				$idcm=$aux[0]->max+1;
		   				if($this->M_comprobante->insertar($idcm,$glo,$tip,$nro,$fec)){
		   					for($i=0;$i<count($ctas);$i++){
								$cta=explode("|", $ctas[$i]);
								if(count($cta)==4){
									if($this->M_detalle_comprobante->insertar($idcm,$cta[0],$cta[1],$cta[2],$cta[3])){}
								}
							}
							echo "ok";
		   				}else{
		   					echo "error";
		   				}
	   				}else{
	   					echo "numero_folio_exist";
	   				}
	   			}else{
	   				echo "fail";
	   			}
   			}else{
   				echo "fail";
   			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	public function imprimir_comprobantes(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				$listado['comprobantes']=$_POST['json'];
				$listado['fecha1']=$_POST['fecha1'];
				$listado['fecha2']=$_POST['fecha2'];
				$this->load->view('contabilidad/comprobante/4-imprimir/config',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function arma_comprobantes(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				if(isset($_POST['v1'])){ if($_POST['v1']!="ok"){ $listado['v1']="ok";} }
				if(isset($_POST['v2'])){ if($_POST['v2']!="ok"){ $listado['v2']="ok";} }
				if(isset($_POST['v3'])){ if($_POST['v3']!="ok"){ $listado['v3']="ok";} }
				if(isset($_POST['v4'])){ if($_POST['v4']!="ok"){ $listado['v4']="ok";} }
				if(isset($_POST['v5'])){ if($_POST['v5']!="ok"){ $listado['v5']="ok";} }
				if(isset($_POST['v6'])){ if($_POST['v6']!="ok"){ $listado['v6']="ok";} }
				if(isset($_POST['v7'])){ if($_POST['v7']!="ok"){ $listado['v7']="ok";} }
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=34;}
				$listado['comprobantes']=$_POST['json'];
				$listado['fecha1']=$_POST['fecha1'];
				$listado['fecha2']=$_POST['fecha2'];
				$this->load->view('contabilidad/comprobante/4-imprimir/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	public function reportes_comprobante(){
		if(!empty($this->session_id)){
			if(isset($_POST['idcm'])){
				$idcm=trim($_POST['idcm']);
				$comprobante=$this->M_comprobante->get($idcm);
				if(!empty($comprobante)){
					$listado['comprobante']=$comprobante[0];
					$listado['detalle_comprobante']=$this->M_detalle_comprobante->get_row_complet('dp.idcm',$idcm);
					$this->load->view('contabilidad/comprobante/5-reportes/view',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	public function config_comprobante(){
   		if(!empty($this->session_id)){
			if(isset($_POST['idcm'])){
				$idcm=trim($_POST['idcm']);
				$comprobante=$this->M_comprobante->get($idcm);
				if(!empty($comprobante)){
					$listado['comprobante']=$comprobante[0];
					$listado['detalle_comprobante']=$this->M_detalle_comprobante->get_row_complet('dp.idcm',$idcm);
					$listado['cuentas']=$this->M_plan_cuenta->get_row('tipo','1');
					$this->load->view('contabilidad/comprobante/6-config/view',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function refresh_nro_cmp_up(){
   		if(!empty($this->session_id)){
   			if(isset($_POST['idcm']) && isset($_POST['tip']) && isset($_POST['fec'])){
   				$idcm=$_POST['idcm'];
   				$tip=$_POST['tip'];
   				$fec=$_POST['fec'];
   				if(($tip=="I" || $tip=="E" || $tip=="T") && $this->val->fecha($fec) && $this->val->entero($idcm,0,10)){
   					$comprobante=$this->M_comprobante->get($idcm);
   					if(!empty($comprobante)){
   						$vf1=explode("-", $comprobante[0]->fecha);
   						$vf2=explode("-", $fec);
   						if($comprobante[0]->tipo==$tip && $vf1[0]==$vf2[0] && $vf1[1]==$vf2[1]){
   							echo $comprobante[0]->folio;
   						}else{
							$vf=explode("-", $fec);
							$fecha1=$vf[0]."-".$vf[1]."-01";
							$fecha2=$vf[0]."-".$vf[1]."-".$this->validaciones->ultimo_dia($vf[1],$vf[0]);
							$max=$this->M_comprobante->max_folio($tip,$fecha1,$fecha2);
							echo $max[0]->folio+1;
   						}
   					}else{
   						echo "fail";
   					}
   				}else{
   					echo "fail";
   				}
   			}else{
   				echo "fail";
   			}
		}else{
			echo "logout";
		}
	}
	public function update_comprobante(){
   		if(!empty($this->session_id)){
   			if(isset($_POST['idcm']) && isset($_POST['ctas']) && isset($_POST['nro']) && isset($_POST['tip']) && isset($_POST['fec']) && isset($_POST['glo'])){
   				$ctas=explode("[|]", trim($_POST['ctas']));
	   			$idcm=trim($_POST['idcm']);
	   			$nro=trim($_POST['nro']);
	   			$tip=trim($_POST['tip']);
	   			$fec=trim($_POST['fec']);
	   			$glo=trim($_POST['glo']);
	   			if($this->val->entero($idcm,0,10) && count($ctas)>0 && $this->val->entero($nro,0,10) && $nro>0 && ($tip=="T" || $tip=="E" || $tip=="I") && $this->val->fecha($fec) && $this->val->textarea($glo,0,200)){
	   				$comprobante=$this->M_comprobante->get($idcm);
	   				if(!empty($comprobante)){
		   				$vf=explode("-", $fec);
						$fecha1=$vf[0]."-".$vf[1]."-01";
						$fecha2=$vf[0]."-".$vf[1]."-".$this->validaciones->ultimo_dia($vf[1],$vf[0]);
		   				$control=$this->M_comprobante->get_search('c.tipo',$tip,'c.folio',$nro,$fecha1,$fecha2);
		   				if(empty($control)){
			   				if($this->M_comprobante->modificar($idcm,$glo,$tip,$nro,$fec)){
			   					if($this->M_detalle_comprobante->eliminar_col('idcm',$idcm)){
				   					for($i=0;$i<count($ctas);$i++){
										$cta=explode("|", $ctas[$i]);
										if(count($cta)==4){
											if($this->M_detalle_comprobante->insertar($idcm,$cta[0],$cta[1],$cta[2],$cta[3])){}
										}
									}
									echo "ok";
			   					}else{
			   						echo "error";
			   					}
			   				}else{
			   					echo "error";
			   				}
		   				}else{
		   					if($control[0]->idcm==$comprobante[0]->idcm){
				   				if($this->M_comprobante->modificar($idcm,$glo,$tip,$nro,$fec)){
				   					if($this->M_detalle_comprobante->eliminar_col('idcm',$idcm)){
					   					for($i=0;$i<count($ctas);$i++){
											$cta=explode("|", $ctas[$i]);
											if(count($cta)==4){
												if($this->M_detalle_comprobante->insertar($idcm,$cta[0],$cta[1],$cta[2],$cta[3])){}
											}
										}
										echo "ok";
				   					}else{
				   						echo "error";
				   					}
				   				}else{
				   					echo "error";
				   				}
		   					}else{
		   						echo "numero_folio_exist";
		   					}
		   				}
	   				}else{
	   					echo "fail";
	   				}
	   			}else{
	   				echo "fail";
	   			}
   			}else{
   				echo "fail";
   			}
		}else{
			echo "logout";
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	public function drop_comprobante(){
		if(!empty($this->session_id)){
			if(isset($_POST['idcm'])){
				$idcm=trim($_POST['idcm']);
				if($this->M_comprobante->eliminar($idcm)){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE COMPROBANTES -------*/
/*------- MANEJO DE LIBRO MAYOR -------*/
	public function search_mayor(){
		if(!empty($this->session_id)){
			$this->load->view('contabilidad/libro_mayor/search');
		}else{
			echo "logout";
		}
	}
	public function view_mayor(){
		if(!empty($this->session_id)){
			if(isset($_POST['f1']) && isset($_POST['f2'])){
				if($this->val->fecha($_POST['f1']) && $this->val->fecha($_POST['f2'])){
					$f1=$_POST['f1'];
					$f2=$_POST['f2'];
				}else{
					$f1=date('Y-m').'-01';
					$f2=date('Y-m').'-'.$this->lib->dosDigitos($this->lib->ultimo_dia(date('m'),date('Y')));
				}
			}else{
				$f1=date('Y-m').'-01';
				$f2=date('Y-m').'-'.$this->lib->dosDigitos($this->lib->ultimo_dia(date('m'),date('Y')));
			}
			$listado['fecha1']=$f1;
			$listado['fecha2']=$f2;
			$listado['cuentas']=$this->M_detalle_comprobante->get_group_cuentas($f1,$f2,true,NULL);//sacamos las cuentas agrupadas en el rango de fechas
			$this->load->view('contabilidad/libro_mayor/view',$listado);
		}else{
			echo "logout";
		}
	}
	/*--- Imprimir ---*/
   	public function imprimir_mayor(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				$listado['cuentas']=$_POST['json'];
				$listado['fecha1']=$_POST['fecha1'];
				$listado['fecha2']=$_POST['fecha2'];
				$this->load->view('contabilidad/libro_mayor/4-imprimir/config',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function arma_mayor(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				if(isset($_POST['v1'])){ if($_POST['v1']!="ok"){ $listado['v1']="ok";} }
				if(isset($_POST['v2'])){ if($_POST['v2']!="ok"){ $listado['v2']="ok";} }
				if(isset($_POST['v3'])){ if($_POST['v3']!="ok"){ $listado['v3']="ok";} }
				if(isset($_POST['v4'])){ if($_POST['v4']!="ok"){ $listado['v4']="ok";} }
				if(isset($_POST['v5'])){ if($_POST['v5']!="ok"){ $listado['v5']="ok";} }
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=24;}
				$listado['cuentas']=$_POST['json'];
				$listado['fecha1']=$_POST['fecha1'];
				$listado['fecha2']=$_POST['fecha2'];
				$this->load->view('contabilidad/libro_mayor/4-imprimir/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Imprimir ---*/
/*------- END MANEJO DE LIBRO MAYOR -------*/
/*------- MANEJO DE BALANCE DE SUMAS Y SALDOS -------*/
	public function search_sumas(){
		if(!empty($this->session_id)){
			$this->load->view('contabilidad/sumas_saldos/search');
		}else{
			echo "logout";
		}
	}
	public function view_sumas(){
		if(!empty($this->session_id)){
			if(isset($_POST['f1']) && isset($_POST['f2'])){
				if($this->val->fecha($_POST['f1']) && $this->val->fecha($_POST['f2'])){
					$f1=$_POST['f1'];
					$f2=$_POST['f2'];
				}else{
					$f1=date('Y-m').'-01';
					$f2=date('Y-m').'-'.$this->lib->dosDigitos($this->lib->ultimo_dia(date('m'),date('Y')));
				}
			}else{
				$f1=date('Y-m').'-01';
				$f2=date('Y-m').'-'.$this->lib->dosDigitos($this->lib->ultimo_dia(date('m'),date('Y')));
			}
			$listado['fecha1']=$f1;
			$listado['fecha2']=$f2;
			$listado['cuentas']=$this->M_detalle_comprobante->get_group_cuentas($f1,$f2,true,"");
			$this->load->view('contabilidad/sumas_saldos/view',$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Imprimir ---*/
   	public function imprimir_sumas(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				$listado['cuentas']=$_POST['json'];
				$listado['fecha1']=$_POST['fecha1'];
				$listado['fecha2']=$_POST['fecha2'];
				$this->load->view('contabilidad/sumas_saldos/4-imprimir/config',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function arma_sumas(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				if(isset($_POST['v1'])){ if($_POST['v1']!="ok"){ $listado['v1']="ok";} }
				if(isset($_POST['v2'])){ if($_POST['v2']!="ok"){ $listado['v2']="ok";} }
				if(isset($_POST['v3'])){ if($_POST['v3']!="ok"){ $listado['v3']="ok";} }
				if(isset($_POST['v4'])){ if($_POST['v4']!="ok"){ $listado['v4']="ok";} }
				if(isset($_POST['v5'])){ if($_POST['v5']!="ok"){ $listado['v5']="ok";} }
				if(isset($_POST['v6'])){ if($_POST['v6']!="ok"){ $listado['v6']="ok";} }
				if(isset($_POST['v7'])){ if($_POST['v7']!="ok"){ $listado['v7']="ok";} }
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=34;}
				$listado['cuentas']=$_POST['json'];
				$listado['fecha1']=$_POST['fecha1'];
				$listado['fecha2']=$_POST['fecha2'];
				$this->load->view('contabilidad/sumas_saldos/4-imprimir/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Imprimir ---*/
/*------- END MANEJO DE BALANCE DE SUMAS Y SALDOS -------*/
/*------- MANEJO ESTADO DE COSTO DE PRODUCCION DE LO VENDIDO -------*/
	public function search_costo(){
		if(!empty($this->session_id)){
			$this->load->view('contabilidad/estado_costo/search');
		}else{
			echo "logout";
		}
	}
	public function view_estado(){
		if(!empty($this->session_id)){
			if(isset($_POST['nro'])){
				$nro=$_POST['nro'];
				if(isset($_POST['f1']) && isset($_POST['f2'])){
					if($this->val->fecha($_POST['f1']) && $this->val->fecha($_POST['f2'])){
						$f1=$_POST['f1'];
						$f2=$_POST['f2'];
					}else{
						$f1=date('Y-m').'-01';
						$f2=date('Y-m').'-'.$this->lib->dosDigitos($this->lib->ultimo_dia(date('m'),date('Y')));
					}
				}else{
					$f1=date('Y-m').'-01';
					$f2=date('Y-m').'-'.$this->lib->dosDigitos($this->lib->ultimo_dia(date('m'),date('Y')));
				}
				$listado['fecha1']=$f1;
				$listado['fecha2']=$f2;
				$listado['cuentas']=$this->M_detalle_comprobante->get_group_cuentas($f1,$f2,true,"");
				switch($nro){
					case '0': $listado['estructura']=$this->M_estado_estructura->get_row('tipo_estado',$nro); $this->load->view('contabilidad/estado_costo/view',$listado); break;
					case '1': $listado['estructura']=$this->M_estado_estructura->get_all(); $this->load->view('contabilidad/estado/view',$listado); break;
					case '2': $listado['estructura']=$this->M_estado_estructura->get_row('tipo_estado',$nro); $this->load->view('contabilidad/balance/view',$listado); break;
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- Imprimir ---*/
   	public function imprimir_estado(){
		if(!empty($this->session_id)){
			if(isset($_POST['fecha1']) && isset($_POST['fecha2'])){
				$listado['fecha1']=$_POST['fecha1'];
				$listado['fecha2']=$_POST['fecha2'];
				$listado['nro_estado']=$_POST['nro_estado'];
				switch($_POST['nro_estado']){
					case '0': $this->load->view('contabilidad/estado_costo/4-imprimir/config',$listado); break;
					case '1': $this->load->view('contabilidad/estado/4-imprimir/config',$listado); break;
					case '2': $this->load->view('contabilidad/balance/4-imprimir/config',$listado); break;
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function arma_estado(){
		if(!empty($this->session_id)){
			if(isset($_POST['fecha1']) && isset($_POST['fecha2'])){
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=34;}
				$listado['fecha1']=$_POST['fecha1'];
				$listado['fecha2']=$_POST['fecha2'];
				$listado['cuentas']=$this->M_detalle_comprobante->get_group_cuentas($_POST['fecha1'],$_POST['fecha2'],true,"");
				switch($_POST['nro_estado']){
					case '0': $listado['estructura']=$this->M_estado_estructura->get_row('tipo_estado',$_POST['nro_estado']); $this->load->view('contabilidad/estado_costo/4-imprimir/view',$listado); break;
					case '1': $listado['estructura']=$this->M_estado_estructura->get_all(); $this->load->view('contabilidad/estado/4-imprimir/view',$listado); break;
					case '2': $listado['estructura']=$this->M_estado_estructura->get_row('tipo_estado',$_POST['nro_estado']); $this->load->view('contabilidad/balance/4-imprimir/view',$listado); break;
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Imprimir ---*/
/*------- END MANEJO ESTADO DE COSTO DE PRODUCCION DE LO VENDIDO -------*/
/*------- MANEJO DE BALANCE -------*/
	public function search_balance(){
		if(!empty($this->session_id)){
			$this->load->view('contabilidad/balance/search');
		}else{
			echo "logout";
		}
	}
	/*public function view_balance(){
		if(!empty($this->session_id)){
			$fecha1=date('Y-m-d');
			$vf=explode("-", $fecha1);
			$fecha1=$vf[0]."-01-01";
			$fecha2=$vf[0]."-12-".$this->validaciones->ultimo_dia('12',$vf[0]);
			$listado['fecha1']=$fecha1;
			$listado['fecha2']=$fecha2;
			$listado['cuentas']=$this->M_detalle_comprobante->get_group_cuentas($fecha1,$fecha2,true,"");
			$this->load->view('contabilidad/balance/view',$listado);
		}else{
			echo "logout";
		}
	}*/
   	/*--- Buscador ---*/
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE BALANCE -------*/

/*------- MANEJO DE ESTADO -------*/
	public function search_estado(){
		if(!empty($this->session_id)){
			$this->load->view('contabilidad/estado/search');
		}else{
			echo "logout";
		}
	}
	/*public function view_estado(){
		if(!empty($this->session_id)){
			
			$fecha1=date('Y-m-d');
			$vf=explode("-", $fecha1);
			$fecha1=$vf[0]."-01-01";
			$fecha2=$vf[0]."-12-".$this->validaciones->ultimo_dia('12',$vf[0]);
			$listado['fecha1']=$fecha1;
			$listado['fecha2']=$fecha2;
			$listado['cuentas']=$this->M_detalle_comprobante->get_group_cuentas($fecha1,$fecha2,true,"");
			$listado['nro']=$nro;
			$this->load->view('contabilidad/estado/view',$listado);
		}else{
			echo "logout";
		}
	}*/
   	/*--- Buscador ---*/
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE ESTADO -------*/
/*------- MANEJO DE PLAN DE CUENTAS -------*/
   	public function search_plan(){
		if(!empty($this->session_id)){
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('contabilidad/plan/search',$listado);
		}else{
			echo "logout";
		}
	}
	public function view_plan(){
		if(!empty($this->session_id)){
			$atrib="";$val="";
			if(isset($_POST['codigo']) || isset($_POST['cuenta'])){
				if($_POST['codigo']!=""){
					$atrib="codigo";$val=$_POST['codigo'];
				}else{
					if($_POST['cuenta']!=""){
						$atrib="p.cuenta_text";$val=$_POST['cuenta'];
					}
				}
				
			}
			$listado['cuentas']=$this->M_plan_cuenta->get_search($atrib,$val);
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('contabilidad/plan/view',$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Nuevo ---*/
   	public function new_plan(){
		if(!empty($this->session_id)){
			$listado['sub_grupos']=$this->M_plan_cuenta->sub_grupos();
			$listado['rubros']=$this->M_plan_cuenta->rubros();
			$this->load->view('contabilidad/plan/3-nuevo/form',$listado);
		}else{
			echo "logout";
		}
	}
	public function save_plan(){
		if(!empty($this->session_id)){
			if(isset($_POST['tip']) && isset($_POST['gru']) && isset($_POST['sub_gru']) && isset($_POST['rub']) && isset($_POST['cue']) && isset($_POST['cue']) && isset($_POST['cue_text'])){
				$tip=trim($_POST['tip']);
				$gru=trim($_POST['gru']);
				$sub_gru=trim($_POST['sub_gru']);
				$rub=trim($_POST['rub']);
				$cue=trim($_POST['cue']);
				$cue_text=trim($_POST['cue_text']);
				if($this->val->entero($tip,0,1) && $tip>=0 && $tip<=1 && $this->val->entero($gru,0,1) && $gru>=0 && $gru<=9 && $this->val->strSpace($cue_text,5,150)){
					$control=true;
					if($sub_gru!=""){ if(!$this->val->entero($sub_gru,0,1) || $sub_gru<0 || $sub_gru>9){ $control=false;}}
					if($rub!=""){ if(!$this->val->entero($rub,0,1) || $rub<0 || $rub>9){ $control=false;}}
					if($cue!=""){ if(!$this->val->entero($cue,0,4) || $cue<0 || $cue>9999){ $control=false;}}
					if($control){
						$cuentas=$this->M_plan_cuenta->get_col_4n('grupo',$gru,'sub_grupo',$sub_gru,'rubro',$rub,'cuenta',$cue);
						if(empty($cuentas)){
							if($this->M_plan_cuenta->insertar($gru,$sub_gru,$rub,$cue,$cue_text,$tip)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "exists_cuenta";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	public function imprimir_plan(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				$listado['cuentas']=$_POST['json'];
				$this->load->view('contabilidad/plan/4-imprimir/config',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function arma_plan(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				if(isset($_POST['v1'])){ if($_POST['v1']!="ok"){ $listado['v1']="ok";} }
				if(isset($_POST['v2'])){ if($_POST['v2']!="ok"){ $listado['v2']="ok";} }
				if(isset($_POST['v3'])){ if($_POST['v3']!="ok"){ $listado['v3']="ok";} }
				if(isset($_POST['v4'])){ if($_POST['v4']!="ok"){ $listado['v4']="ok";} }
				if(isset($_POST['v5'])){ if($_POST['v5']!="ok"){ $listado['v5']="ok";} }
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=38;}
				$listado['cuentas']=$_POST['json'];
				$this->load->view('contabilidad/plan/4-imprimir/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Imprimir ---*/
   	/*-- Configuracion de botones de gestos y ventas ---*/
   	public function change_estado(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpl']) && isset($_POST['tipo'])){
				$idpl=trim($_POST['idpl']);
				$tipo=trim($_POST['tipo']);
				if($this->val->entero($idpl,0,10) && $this->val->entero($tipo,0,1) && $tipo>=0 && $tipo<=2){
					$cuenta=$this->M_plan_cuenta->get($idpl);
					if(!empty($cuenta)){
						switch($tipo){//cambio de tipo de cuenta
							case '0':
								if($cuenta[0]->tipo==0){ $tipo=1;}else{ $tipo=0;}
								if($this->M_plan_cuenta->set_row($idpl,'tipo',$tipo)){
									echo "ok";
								}else{
									echo "error";
								}
								break;
							case '1':// si es gasto
								if($cuenta[0]->gasto==0){ $gasto=1;}else{ $gasto=0;}
								if($this->M_plan_cuenta->set_row($idpl,'gasto',$gasto)){
									echo "ok";
								}else{
									echo "error";
								}
								break;
							case '2':// si es ingreso
								if($cuenta[0]->venta==0){ $venta=1;}else{ $venta=0;}
								if($this->M_plan_cuenta->set_row($idpl,'venta',$venta)){
									echo "ok";
								}else{
									echo "error";
								}
								break;
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*-- End Configuracion de botones de gestos y ventas ---*/
   	/*--- Reportes ---*/
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	public function config_plan(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpl'])){
				$cuenta=$this->M_plan_cuenta->get($_POST['idpl']);
				if(!empty($cuenta)){
					$listado['cuenta']=$cuenta[0];
					$this->load->view('contabilidad/plan/6-config/view',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_plan(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpl']) && isset($_POST['tip']) && isset($_POST['gru']) && isset($_POST['sub_gru']) && isset($_POST['rub']) && isset($_POST['cue']) && isset($_POST['cue']) && isset($_POST['cue_text'])){
				$idpl=trim($_POST['idpl']);
				$tip=trim($_POST['tip']);
				$gru=trim($_POST['gru']);
				$sub_gru=trim($_POST['sub_gru']);
				$rub=trim($_POST['rub']);
				$cue=trim($_POST['cue']);
				$cue_text=trim($_POST['cue_text']);
				if($this->val->entero($idpl,0,10) && $this->val->entero($tip,0,1) && $tip>=0 && $tip<=1 && $this->val->entero($gru,0,1) && $gru>=0 && $gru<=9 && $this->val->strSpace($cue_text,5,150)){
					$control=true;
					if($sub_gru!=""){ if(!$this->val->entero($sub_gru,0,1) || $sub_gru<0 || $sub_gru>9){ $control=false;}}
					if($rub!=""){ if(!$this->val->entero($rub,0,1) || $rub<0 || $rub>9){ $control=false;}}
					if($cue!=""){ if(!$this->val->entero($cue,0,4) || $cue<0 || $cue>9999){ $control=false;}}
					if($control){
						$control=$this->M_plan_cuenta->get($_POST['idpl']);
						if(!empty($control)){
							$cuentas=$this->M_plan_cuenta->get_col_4n_id('grupo',$gru,'sub_grupo',$sub_gru,'rubro',$rub,'cuenta',$cue,$idpl);
							if(empty($cuentas)){
								if($this->M_plan_cuenta->modificar($idpl,$gru,$sub_gru,$rub,$cue,$cue_text,$tip)){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "exists_cuenta";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	public function confirmar_plan(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpl'])){
				$idpl=$_POST['idpl'];
				$cuenta=$this->M_plan_cuenta->get($idpl);
				if(!empty($cuenta)){
					$control=$this->M_detalle_comprobante->get_row('idpl',$idpl);
					if(!empty($control)){
						$desc=" Imposible Eliminar el registro, pues el registro se encuentra siendo usado en ".count($control)." registros de comprobantes.";
						$listado['open_control']="false";
						$listado['control']="";
					}else{
						$desc="Se eliminara definitivamente el registro";
					}
					$listado['titulo']="eliminar el registro <b>".$cuenta[0]->cuenta_text."</b> definitivamente";
					$listado['desc']=$desc;
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function drop_plan(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpl']) && isset($_POST['u']) && isset($_POST['p'])){
				$idpl=$_POST['idpl'];
				$u=$_POST['u'];
				$p=$_POST['p'];
				if($u==$this->session->userdata("login")){
					$usuario=$this->M_empleado->validate($u,$p);
					if(!empty($usuario)){
						$cuenta=$this->M_plan_cuenta->get($idpl);
						if(!empty($cuenta)){
							if($this->M_plan_cuenta->eliminar($idpl)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "validate";
					}
				}else{
					echo "validate";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE PLAN DE CUENTAS -------*/
/*------- MANEJO DE CONFIGURACIONES -------*/
	public function view_config(){
		if(!empty($this->session_id)){
			$listado['egresos']=$this->M_cuentas_comprobante->get_egreso();
			$listado['ingresos']=$this->M_cuentas_comprobante->get_ingreso();
			$listado['cuentas']=$this->M_plan_cuenta->get_row('tipo','1');
			$listado['estructura0']=$this->M_estado_estructura->get_all();//estado de costo de prod y prod. vend.
			$this->load->view('contabilidad/config/view',$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Manejo de Cuenta de Egreso ---*/
   	public function save_cuenta_egreso(){
		if(!empty($this->session_id)){
			if(isset($_POST['cue']) && isset($_POST['por']) && isset($_POST['tip'])){
				$cue=$_POST['cue'];
				$por=$_POST['por'];
				$tip=$_POST['tip'];
				if($this->val->entero($cue,0,10) && $this->val->decimal($por,3,2) && $por>0 && $por<=100 && ($tip=="H" || $tip=="D")){
					$max=$this->M_cuentas_comprobante->max_id_2n('posicion','cuenta','E');
					$pos=$max[0]->max+1;
					$por=$por/100;
					if($this->M_cuentas_comprobante->insertar($cue,$pos,$por,$tip,'E',1)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function update_cuenta_egreso(){
		if(!empty($this->session_id)){
			if(isset($_POST['idci']) && isset($_POST['pos']) && isset($_POST['cue']) && isset($_POST['por']) && isset($_POST['tip'])){
				$idci=$_POST['idci'];
				$pos=$_POST['pos'];
				$cue=$_POST['cue'];
				$por=$_POST['por'];
				$tip=$_POST['tip'];
				if($this->val->entero($idci,0,10) && $this->val->entero($pos,0,10) && ($this->val->entero($cue,0,10) || $cue=="block") && $this->val->decimal($por,3,2) && $por>0 && $por<=100 && ($tip=="H" || $tip=="D")){
					$por=$por/100;
					$cuenta=$this->M_cuentas_comprobante->get($idci);
					if(!empty($cuenta)){
						//cambiando posicion del destino
						$registro=$this->M_cuentas_comprobante->get_row_2n('posicion',$pos,'cuenta','E');
						if(count($registro)>0){
							if(!$this->val->entero($cue,0,10)){ $cue="";}
							if($registro[0]->idci!=$cuenta[0]->idci){
								if($this->M_cuentas_comprobante->modificar_row($registro[0]->idci,'posicion',$cuenta[0]->posicion)){
									if($this->M_cuentas_comprobante->modificar($idci,$cue,$pos,$por,$tip,'E')){
										echo "ok";
									}else{
										echo "error";
									}
								}
							}else{
								if($this->M_cuentas_comprobante->modificar($idci,$cue,$pos,$por,$tip,'E')){
									echo "ok";
								}else{
									echo "error";
								}
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function drop_egreso(){
		if(!empty($this->session_id)){
			if(isset($_POST['idci'])){
				$idci=$_POST['idci'];
				$cuenta=$this->M_cuentas_comprobante->get($idci);
				if(!empty($cuenta)){
					if($cuenta[0]->control!=0){
						if($this->M_cuentas_comprobante->eliminar($idci)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "error";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Manejo de Cuenta de Egreso ---*/
   	/*--- Manejo de Cuenta de Ingreso ---*/
   	public function save_cuenta_ingreso(){
		if(!empty($this->session_id)){
			if(isset($_POST['cue']) && isset($_POST['por']) && isset($_POST['tip'])){
				$cue=$_POST['cue'];
				$por=$_POST['por'];
				$tip=$_POST['tip'];
				if($this->val->entero($cue,0,10) && $this->val->decimal($por,3,2) && $por>0 && $por<=100 && ($tip=="H" || $tip=="D")){
					$max=$this->M_cuentas_comprobante->max_id_2n('posicion','cuenta','I');
					$pos=$max[0]->max+1;
					$por=$por/100;
					if($this->M_cuentas_comprobante->insertar($cue,$pos,$por,$tip,'I',1)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function update_cuenta_ingreso(){
		if(!empty($this->session_id)){
			if(isset($_POST['idci']) && isset($_POST['pos']) && isset($_POST['cue']) && isset($_POST['por']) && isset($_POST['tip'])){
				$idci=$_POST['idci'];
				$pos=$_POST['pos'];
				$cue=$_POST['cue'];
				$por=$_POST['por'];
				$tip=$_POST['tip'];
				if($this->val->entero($idci,0,10) && $this->val->entero($pos,0,10) && ($this->val->entero($cue,0,10) || $cue=="block") && $this->val->decimal($por,3,2) && $por>0 && $por<=100 && ($tip=="H" || $tip=="D")){
					$por=$por/100;
					$cuenta=$this->M_cuentas_comprobante->get($idci);
					if(!empty($cuenta)){
						//cambiando posicion del destino
						$registro=$this->M_cuentas_comprobante->get_row_2n('posicion',$pos,'cuenta','I');
						if(count($registro)>0){
							if(!$this->val->entero($cue,0,10)){ $cue="";}
							if($registro[0]->idci!=$cuenta[0]->idci){
								if($this->M_cuentas_comprobante->modificar_row($registro[0]->idci,'posicion',$cuenta[0]->posicion)){
									if($this->M_cuentas_comprobante->modificar($idci,$cue,$pos,$por,$tip,'I')){
										echo "ok";
									}else{
										echo "error";
									}
								}
							}else{
								if($this->M_cuentas_comprobante->modificar($idci,$cue,$pos,$por,$tip,'I')){
									echo "ok";
								}else{
									echo "error";
								}
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function drop_ingreso(){
		if(!empty($this->session_id)){
			if(isset($_POST['idci'])){
				$idci=$_POST['idci'];
				$cuenta=$this->M_cuentas_comprobante->get($idci);
				if(!empty($cuenta)){
					if($cuenta[0]->control!=0){
						if($this->M_cuentas_comprobante->eliminar($idci)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "error";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Manejo de Cuenta de Ingreso ---*/
	/*--- Manejo Estructura de costo de produccion y costo de produccion de lo vendido ---*/
  	public function new_group_est(){
		if(!empty($this->session_id)){
			$nro=$_POST['nro'];
			switch ($nro) {
				case '0': $this->load->view('contabilidad/config/estructura_produccion_vendido/new'); break;
				case '1': $this->load->view('contabilidad/config/estructura_estado/new'); break;
				case '2': $this->load->view('contabilidad/config/balance_general/new'); break;
			}
		}else{
			echo "logout";
		}
	}
	public function add_row_cuenta_epv(){
		if(!empty($this->session_id)){
			if(isset($_POST['row'])){
				$listado['row']=$_POST['row']+1;
				$listado['cuentas']=$this->M_plan_cuenta->get_all();
				$this->load->view('contabilidad/config/estructura_produccion_vendido/rows/row_cuenta',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function add_row_grupo_epv(){
		if(!empty($this->session_id)){
			if(isset($_POST['row']) && isset($_POST['nro'])){
				$listado['row']=$_POST['row']+1;
				$listado['grupos']=$this->M_estado_estructura->get_row('tipo_estado',$_POST['nro']);
				$this->load->view('contabilidad/config/estructura_produccion_vendido/rows/row_grupo_pv',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function save_group_est(){
		if(!empty($this->session_id)){
			if(isset($_POST['nro']) && isset($_POST['sig']) && isset($_POST['sv']) && isset($_POST['nom']) && isset($_POST['nv']) && isset($_POST['col']) && isset($_POST['ctas']) && isset($_POST['ops'])){
				$nro=$_POST['nro'];
				$sig=$_POST['sig'];
				$sv=$_POST['sv'];
				$nom=$_POST['nom'];
				$nv=$_POST['nv'];
				$col=$_POST['col'];
				$ctas=explode("|",$_POST['ctas']);
				$ops=explode("[|]",$_POST['ops']);
				if(($nro==0 || $nro==1 || $nro==2) && $this->val->entero($sig,0,1) && $sig>=0 && $sig<=1 && ($sv==true || $sv==false) && $this->val->strSpace($nom,0,100) && ($nv==true || $nv==false) && $this->val->entero($col,0,1) && $col>=0 && $col<=2 && count($ctas)>0){
					if($nv=="true"){ $nv=1;}else{ $nv=0;}
					if($sv=="true"){ $sv=1;}else{ $sv=0;}
					$aux=$this->M_estado_estructura->max('idee');
					$idee=$aux[0]->max+1;
					$aux=$this->M_estado_estructura->max_where('posicion','tipo_estado',$nro);
					$posicion=$aux[0]->max+1;
					$ecpv=0;
					if($nro==1 && isset($_POST['ecpv'])){ 
						$ecpv=$_POST['ecpv'];
						if($ecpv=="true"){ $ecpv=1; }else{ $ecpv=0; }
					}
					if($this->M_estado_estructura->insertar($idee,$posicion,$nom,$nv,$sig*1,$sv,$nro,$col,$ecpv)){
						for($i=0; $i < count($ctas); $i++){ 
							$cta=$ctas[$i];
							if($this->M_estado_cuentas->insertar($cta,$idee)){ }
						}
						for($i=0; $i < count($ops); $i++){ 
							$aux=explode("|", $ops[$i]);
							if(count($aux)>1){
								$gru_sig=$aux[0];
								$grupo=$aux[1];
								if($this->M_estado_operacion->insertar($idee,$grupo,$gru_sig*1)){ }
							}
						}
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
			/*$nro=$_POST['nro'];
			$sig=$_POST['sig'];
			$sv=$_POST['sv'];
			$nom=$_POST['nom'];
			$nv=$_POST['nv'];
			$col=$_POST['col'];
			$row_cu=$_POST['row_cu'];
			$row_co=$_POST['row_co'];
			if($nv!="" && $sig!="" && $sv!="" && $nro!=""){
				if($nv=="true"){ $nv=1;}else{ $nv=0;}
				if($sv=="true"){ $sv=1;}else{ $sv=0;}
				$ecpv=0;
				if($nro==1){ 
					$ecpv=$_POST['ecpv'];
					if($ecpv=="true"){ $ecpv=1; }else{ $ecpv=0; }
				}
				//$plan=$this->M_plan_cuenta->get($cta);
				//if($nom==""){ $nom=$plan[0]->cuenta_text;}
				$aux=$this->M_estado_estructura->max('idee');
				$idee=$aux[0]->max+1;
				$aux=$this->M_estado_estructura->max_where('posicion','tipo_estado',$nro);
				$posicion=$aux[0]->max+1;
				if($this->M_estado_estructura->insertar($idee,$posicion,$nom,$nv,$sig*1,$sv,$nro,$col,$ecpv)){
					for ($i=1; $i <= $row_cu; $i++) { 
						$cta=$_POST['cta'.$i];
						if($this->M_estado_cuentas->insertar($cta,$idee,"")){ }
					}
					for ($i=1; $i <= $row_co; $i++) { 
						$gru_sig=$_POST['gru_sig'.$i];
						$grupo=$_POST['grupo'.$i];
						if($this->M_estado_operacion->insertar($idee,$grupo,$gru_sig*1)){ }
					}
					echo "ok";
				}else{
					echo "error";
				}
				//echo " - ".$nom." - ".$nv." - Sig: ".$sig." - ".$sv." - ".$col." - ".$cta;
			}else{
				echo "fail";
			}	*/		
		}else{
			echo "logout";
		}
	}
  	public function mod_group_est(){
		if(!empty($this->session_id)){
			if(isset($_POST['nro']) && isset($_POST['idee'])){
				$nro=$_POST['nro'];
				$idee=$_POST['idee'];
				if($this->val->entero($idee,0,10)){
					$estado=$this->M_estado_estructura->get($idee);
					if(!empty($estado)){
						$listado['estado']=$estado;
						$listado['operaciones']=$this->M_estado_operacion->get_row('idee1',$idee);
						$listado['cuentas']=$this->M_plan_cuenta->get_all();
						$listado['grupos']=$this->M_estado_estructura->get_row('tipo_estado',$nro);
						switch ($nro) {
							case '0': $this->load->view('contabilidad/config/estructura_produccion_vendido/update',$listado); break;
							case '1': $this->load->view('contabilidad/config/estructura_estado/update',$listado); break;
							case '2': $this->load->view('contabilidad/config/balance_general/update',$listado); break;
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_group_est(){
		if(!empty($this->session_id)){
			if(isset($_POST['idee']) && isset($_POST['nro']) && isset($_POST['sig']) && isset($_POST['sv']) && isset($_POST['nom']) && isset($_POST['nv']) && isset($_POST['col']) && isset($_POST['ctas']) && isset($_POST['ops'])){
				$idee=$_POST['idee'];
				$nro=$_POST['nro'];
				$sig=$_POST['sig'];
				$sv=$_POST['sv'];
				$nom=$_POST['nom'];
				$nv=$_POST['nv'];
				$col=$_POST['col'];
				$ctas=explode("|",$_POST['ctas']);
				$ops=explode("[|]",$_POST['ops']);
				if(($nro==0 || $nro==1 || $nro==2) && $this->val->entero($sig,0,1) && $sig>=0 && $sig<=1 && ($sv==true || $sv==false) && $this->val->strSpace($nom,0,100) && ($nv==true || $nv==false) && $this->val->entero($col,0,1) && $col>=0 && $col<=2 && count($ctas)>0){
					if($nv=="true"){ $nv=1;}else{ $nv=0;}
					if($sv=="true"){ $sv=1;}else{ $sv=0;}
					$ecpv=0;
					if($nro==1 && isset($_POST['ecpv'])){ 
						$ecpv=$_POST['ecpv'];
						if($ecpv=="true"){ $ecpv=1; }else{ $ecpv=0; }
					}
					$control=$this->M_estado_estructura->get($idee);
					if(!empty($control)){
						if($this->M_estado_estructura->modificar($idee,$nom,$nv,$sig*1,$sv,$nro,$col,$ecpv)){
							if($this->M_estado_cuentas->eliminar_row('idee',$idee)){
								for($i=0; $i < count($ctas); $i++){ 
									$cta=$ctas[$i];
									if($this->M_estado_cuentas->insertar($cta,$idee)){ }
								}
							}
							if($this->M_estado_operacion->eliminar_row('idee1',$idee)){
								for($i=0; $i < count($ops); $i++){ 
									$aux=explode("|", $ops[$i]);
									if(count($aux)>1){
										$gru_sig=$aux[0];
										$grupo=$aux[1];
										if($this->M_estado_operacion->insertar($idee,$grupo,$gru_sig*1)){ }
									}
								}
							}
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
			/*$idee=$_POST['idee'];
			$nro=$_POST['nro'];
			$nom=$_POST['nom'];
			$nv=$_POST['nv'];
			$sig=$_POST['sig'];
			$sv=$_POST['sv'];
			$col=$_POST['col'];
			$row_cu=$_POST['row_cu'];
			$row_co=$_POST['row_co'];

			if($nv!="" && $sig!="" && $sv!=""){
				if($nv=="true"){ $nv=1;}else{ $nv=0;}
				if($sv=="true"){ $sv=1;}else{ $sv=0;}
				$ecpv=0;
				if($nro==1){ 
					$ecpv=$_POST['ecpv'];
					if($ecpv=="true"){ $ecpv=1; }else{ $ecpv=0; }
				}
				if($this->M_estado_estructura->modificar($idee,$nom,$nv,$sig*1,$sv,$col,$ecpv)){
					if($this->M_estado_cuentas->eliminar_col('idee',$idee)){
						for($i=1; $i <= $row_cu; $i++){ 
							$cta=$_POST['cta'.$i];
							if($this->M_estado_cuentas->insertar($cta,$idee,"")){ }
						}
					}
					if($this->M_estado_operacion->eliminar_col('idee1',$idee)){
						for($i=1; $i <= $row_co; $i++){ 
							$gru_sig=$_POST['gru_sig'.$i];
							$grupo=$_POST['grupo'.$i];
							if($this->M_estado_operacion->insertar($idee,$grupo,$gru_sig*1)){ }
						}
					}
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}	*/					
		}else{
			echo "logout";
		}
	}
	public function drop_epv(){
		if(!empty($this->session_id)){
			if(isset($_POST['idee'])){
				$idee=$_POST['idee'];
				if($this->M_estado_estructura->eliminar($idee)){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}






























	public function nro_cta_plan(){
		if(!empty($this->session_id)){
			$idpl=$_POST['cta'];
			$plan=$this->M_plan_cuenta->get_col($idpl,"CONCAT((IFNULL((grupo),(''))),(IFNULL((sub_grupo),(''))),(IFNULL((rubro),(''))),(IFNULL((cuenta),('')))) AS codigo");
			if(!empty($plan)){
				echo $plan[0]->codigo;
			}else{
				echo "";
			}
		}else{
			echo "logout";
		}
	}






   	/*--- End Manejo Estructura de costo de produccion y costo de produccion de lo vendido ---*/


































   
   	/*--- Ver Todo ---*/
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE CONFIGURACIONES -------*/



































/*------- MANEJO DE ESTADO DE COSTO DE PRODUCCION Y DE LOS VENDIDO -------*/
	/*public function view_search_costo_produccion_vendido(){
		if(!empty($this->session_id)){
			$this->load->view('contabilidad/costo_produccion_vendido/search');
		}else{
			echo "logout";
		}
	}
	public function view_estado(){
		if(!empty($this->session_id)){
			$nro=$_POST['nro'];
			$fecha1=date('Y-m-d');
			$vf=explode("-", $fecha1);
			$fecha1=$vf[0]."-01-01";
			$fecha2=$vf[0]."-12-".$this->validaciones->ultimo_dia('12',$vf[0]);
			$listado['fecha1']=$fecha1;
			$listado['fecha2']=$fecha2;
			$listado['cuentas']=$this->M_detalle_comprobante->get_group_cuentas($fecha1,$fecha2,true,"");
			switch($nro){
				case '0': $listado['estructura']=$this->M_estado_estructura->get_row('tipo_estado',$nro); $this->load->view('contabilidad/costo_produccion_vendido/view',$listado); break;
				case '1': $listado['estructura']=$this->M_estado_estructura->get_all(); $this->load->view('contabilidad/estado/view',$listado); break;
				case '2': $listado['estructura']=$this->M_estado_estructura->get_row('tipo_estado',$nro); $this->load->view('contabilidad/balance/view',$listado); break;
			}
		}else{
			echo "logout";
		}
	}*/
   	/*--- Buscador ---*/
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE ESTADO DE COSTO DE PRODUCCION Y DE LOS VENDIDO -------*/
/*------- MANEJO DE HOJA DE TRABAJO -------*/
	public function view_search_hoja_trabajo(){
		if(!empty($this->session_id)){
			$this->load->view('contabilidad/hoja_trabajo/search');
		}else{
			echo "logout";
		}
	}
	public function view_hoja_trabajo(){
		if(!empty($this->session_id)){
			$fecha1=date('Y-m-d');
			$vf=explode("-", $fecha1);
			$fecha1=$vf[0]."-01-01";
			$fecha2=$vf[0]."-12-".$this->validaciones->ultimo_dia('12',$vf[0]);
			$listado['fecha1']=$fecha1;
			$listado['fecha2']=$fecha2;
			$listado['cuentas']=$this->M_detalle_comprobante->get_group_cuentas($fecha1,$fecha2,true,"");
			$this->load->view('contabilidad/hoja_trabajo/view',$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Buscador ---*/
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE HOJA DE TRABAJO -------*/








}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */