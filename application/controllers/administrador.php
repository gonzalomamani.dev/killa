<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administrador extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('login');
		$this->session->set_userdata('ida','',true);
		$this->session->set_userdata('tipo','',true);
	}
	public function index(){
		if(!empty($this->session_id)){
			$privilegio=$privilegio=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			if(!isset($_GET['p'])){
				$listado['pestania']=1;
			}else{
				$listado['pestania']=$_GET['p'];
			}
			$listado['privilegio']=$privilegio;
			$listado['ci']=$this->session->userdata("ci");
			$this->load->view('v_administrador',$listado);
		}else{
			redirect(base_url().'login',301);
		}
	}
/*------- MANEJO DE USUARIOS -------*/
	public function search_usuario(){
		if(!empty($this->session_id)){
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view("administrador/usuario/search",$listado);
		}else{
			redirect(base_url().'login',301);
		}
	}
	public function view_usuario(){
		if(!empty($this->session_id)){
			$atrib="";
			$val="";
			if(isset($_POST['ci']) && isset($_POST['nom']) && isset($_POST['tip'])  && isset($_POST['car']) && isset($_POST['usu'])){//si tenemos busqueda 
				$ci=$_POST['ci'];
				$nom=$_POST['nom'];
				$tip=$_POST['tip'];
				$car=$_POST['car'];
				$usu=$_POST['usu'];
				if($ci!=""){
					$atrib="p.ci";$val=$ci;
				}else{
					if($nom!=""){
						$atrib="nombre";$val=$nom;
					}else{
						if($tip!=""){
							$atrib="e.admin_sistema";$val=$tip;
						}else{
							if($car!=""){
								$atrib="e.cargo";$val=$car;
							}else{
								if($usu!=""){
									$atrib="e.usuario";$val=$usu;
								}
							}
						}
					}
				}
			}
			$listado['usuarios']=$this->M_empleado->search_usuarios($atrib,$val,false);
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view("administrador/usuario/view",$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Nuevo ---*/
	public function new_usuario(){
		if(!empty($this->session_id)){
			$listado['ciudades']=$this->M_ciudad->get_all();
			$this->load->view("administrador/usuario/3-nuevo/view",$listado);
		}else{
			echo "logout";
		}
	}
	public function search_ci(){
		if(!empty($this->session_id)){
			if(isset($_POST['ci'])){
				$ci=$_POST['ci'];
				if($ci!=""){
					$p=$this->M_persona->get($ci);
					if(count($p)>0){
						$control=$this->M_ciudad->get_pais($p[0]->idci);
						if(!empty($control)){
							$img="";$ciu="";$nom1="";$nom2="";$pat="";$mat="";$car="";
							$empleado=$this->M_empleado->get_row('ci',$ci);
							if(!empty($empleado)){
								$nom2=$empleado[0]->nombre2;
								$pat=$empleado[0]->paterno;
								$mat=$empleado[0]->materno;
								$car=$empleado[0]->cargo;
							}
							$paises=$this->M_pais->get_all();
							$ciudades=$this->M_ciudad->get_row('idpa',$control[0]->idpa);
							$option_ciudad="<option value=''>Seleccionar...</option>";
							for($ci=0;$ci<count($ciudades);$ci++){ $option_ciudad.="<option value='".$ciudades[$ci]->idci."'>".$ciudades[$ci]->nombre."</option>"; }
							$option_ciudad.="<option value='".$control[0]->idci."' selected>".$control[0]->ciudad."</option>";
							echo "|".$option_ciudad."|".$p[0]->nombre."|".$nom2."|".$pat."|".$mat."|".$car;							
						}
					}else{
						
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function save_usuario(){
		if(!empty($this->session_id)){
			if(isset($_POST['ci']) && isset($_POST['ciu']) && isset($_POST['nom1']) && isset($_POST['nom2']) && isset($_POST['pat']) && isset($_POST['mat']) && isset($_POST['tus']) && isset($_POST['usu']) && isset($_POST['car']) && isset($_FILES)){
				$ci=trim($_POST['ci']);
				$ciu=trim($_POST['ciu']);
				$nom1=trim($_POST['nom1']);
				$nom2=trim($_POST['nom2']);
				$pat=trim($_POST['pat']);
				$mat=trim($_POST['mat']);
				$tus=trim($_POST['tus']);
				$usu=trim($_POST['usu']);
				$car=trim($_POST['car']);
				if($this->val->entero($ci,6,8) && $ci>100000 && $ci<=99999999 && $this->val->entero($ciu,0,10) && $this->val->strSpace($nom1,2,20) && $this->val->entero($tus,0,1) && $tus>=0 && $tus<=1 && $this->val->strNoSpace($usu,4,30)){
					$control=true;
					if($nom2!=""){ if(!$this->val->strSpace($nom2,2,20)){ $control=false;}}
					if($mat!=""){ if(!$this->val->strSpace($mat,2,20)){ $control=false;}}
					if($car!=""){ if(!$this->val->strSpace($car,0,100)){ $control=false;}}
					if($pat!=""){ if(!$this->val->strSpace($pat,2,20)){ $control=false;}}
					if($control){
						$usuario=$this->M_empleado->get_row('usuario',$usu);
						if(empty($usuario)){
							$persona=$this->M_persona->get_row('ci',$ci);
							if(empty($persona)){
									$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/personas/','',$this->resize,$ci);
									if($img!='error' && $img!="error_type"){
										if($this->M_persona->insertar($ci,$ciu,$nom1,"","","",$img,"")){
											//echo $img;
											if($this->M_empleado->insertar_usuario($ci,$nom2,$pat,$mat,$car,$tus,$usu)){
												echo "ok";
											}else{
												$this->M_persona->eliminar($ci);
												echo "error";
											}
										}else{
											echo "error";
										}
									}else{
										echo $img;
									}
							}else{
								$empleado=$this->M_empleado->get_row('ci',$ci);
								if(empty($empleado)){
									if($this->M_empleado->insertar_usuario($ci,$nom2,$pat,$mat,$car,$tus,$usu)){
										echo "ok";
									}else{
										echo "error";
									}
								}else{
									$control=$this->M_empleado->get_row_2n('ci',$ci,'usuario_sistema','1');
									if(empty($control)){
										if($this->M_empleado->modificar_usuario($empleado[0]->ide,$ci,$empleado[0]->nombre2,$empleado[0]->paterno,$empleado[0]->materno,$empleado[0]->cargo,$tus,$usu,"")){
											echo "ok";
										}else{
											echo "error";
										}
									}else{
										echo "user_registrer";
									}
								}
							}
						}else{
							echo "name_user_exist";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/

   	public function imprimir_usuario(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				$listado['usuarios']=$_POST['json'];
				$this->load->view('administrador/usuario/4-imprimir/config',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function arma_usuario(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				if(isset($_POST['v1'])){ if($_POST['v1']!="ok"){ $listado['v1']="ok";} }
				if(isset($_POST['v2'])){ if($_POST['v2']!="ok"){ $listado['v2']="ok";} }
				if(isset($_POST['v3'])){ if($_POST['v3']!="ok"){ $listado['v3']="ok";} }
				if(isset($_POST['v4'])){ if($_POST['v4']!="ok"){ $listado['v4']="ok";} }
				if(isset($_POST['v5'])){ if($_POST['v5']!="ok"){ $listado['v5']="ok";} }
				if(isset($_POST['v6'])){ if($_POST['v6']!="ok"){ $listado['v6']="ok";} }
				if(isset($_POST['v7'])){ if($_POST['v7']!="ok"){ $listado['v7']="ok";} }
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=34;}
				$listado['usuarios']=$_POST['json'];
				$this->load->view('administrador/usuario/4-imprimir/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}

   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
	public function detalle_usuario(){
		if(!empty($this->session_id)){
			if(isset($_POST['ide'])){
				$ide=$_POST['ide'];
				$usuario=$this->M_empleado->search_usuarios('e.ide',$ide,true);
				if(!empty($usuario)){
					$listado['usuario']=$usuario[0];
					$this->load->view("administrador/usuario/5-reporte/view",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
	public function config_usuario(){
		if(!empty($this->session_id)){
			if(isset($_POST['ide'])){
				$ide=$_POST['ide'];
				$usuario=$this->M_empleado->search_usuarios('e.ide',$ide,true);
				if(!empty($usuario)){
					$listado['usuario']=$usuario[0];
					$listado['ciudades']=$this->M_ciudad->get_all();
					$this->load->view("administrador/usuario/6-config/view",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_usuario(){
		if(!empty($this->session_id)){
			if(isset($_POST['ide']) && isset($_POST['ci']) && isset($_POST['ciu']) && isset($_POST['nom1']) && isset($_POST['nom2']) && isset($_POST['pat']) && isset($_POST['mat']) && isset($_POST['tus']) && isset($_POST['usu']) && isset($_POST['car']) && isset($_FILES)){
				$ide=trim($_POST['ide']);
				$ci=trim($_POST['ci']);
				$ciu=trim($_POST['ciu']);
				$nom1=trim($_POST['nom1']);
				$nom2=trim($_POST['nom2']);
				$pat=trim($_POST['pat']);
				$mat=trim($_POST['mat']);
				$tus=trim($_POST['tus']);
				$usu=trim($_POST['usu']);
				$car=trim($_POST['car']);
				if($this->val->entero($ide,0,10) && $this->val->entero($ci,6,8) && $ci>100000 && $ci<=99999999 && $this->val->entero($ciu,0,10) && $this->val->strSpace($nom1,2,20) && $this->val->entero($tus,0,1) && $tus>=0 && $tus<=1 && $this->val->strNoSpace($usu,4,30)){
					$control=true;
					if($nom2!=""){ if(!$this->val->strSpace($nom2,2,20)){ $control=false;}}
					if($mat!=""){ if(!$this->val->strSpace($mat,2,20)){ $control=false;}}
					if($car!=""){ if(!$this->val->strSpace($car,0,100)){ $control=false;}}
					if($pat!=""){ if(!$this->val->strSpace($pat,2,20)){ $control=false;}}
					if($control){
						$usuario=$this->M_empleado->get_row('ide',$ide);
						if(!empty($usuario)){
							$usuario=$usuario[0];
							$ci_org=$usuario->ci;
							$control_usuario=$this->M_empleado->get_row('usuario',$usu);
							if(empty($control_usuario)){
								$control=true;
							}else{
								if($usuario->ide==$control_usuario[0]->ide){
									$control=true;
								}else{
									$control=false;
								}
							}
							if($control){
								$control=$this->M_persona->get($ci);
								$guardar="ok";
								if(!empty($control)){
									if($ci==$ci_org){
										$guardar="ok";
									}else{
										$guardar="ci_exist";
									}
								}
								if($guardar=="ok"){
									$persona=$this->M_persona->get($ci_org);
									$img=$persona[0]->fotografia;
									$img=$this->validaciones->cambiar_imagen_miniatura($_FILES,'./libraries/img/personas/','',$this->resize,$img,$ide.''.$ci);//cambiar_imagen_miniatura($FILES,$ruta,$pos,$resize,$origen,$id)
										if($img!='error' && $img!="error_type"){
											if($this->M_persona->modificar_usuario($ci_org,$ci,$ciu,$nom1,$img)){
												if($this->M_empleado->modificar_usuario($ide,$ci,$nom2,$pat,$mat,$car,$tus,$usu,$usuario->password)){
													echo "ok";
												}else{
													echo "error";
												}
											}else{
												echo "error";
											}
										}else{
											echo $img;
										}
									}else{
										echo $guardar;
									}
							}else{
								echo "name_user_exist";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	public function confirmar_usuario(){
   		if(!empty($this->session_id)){
   			if(isset($_POST['ide'])){
				$ide=$_POST['ide'];
				$url="./libraries/img/personas/miniatura/";
				$empleado=$this->M_empleado->search_usuarios('e.ide',$ide,true);
				if(count($empleado)>0){
					$listado['titulo']="eliminar a ".$empleado[0]->nombre." ".$empleado[0]->nombre2." ".$empleado[0]->paterno." ".$empleado[0]->materno." como usuario(a) del sistema";
					$listado['desc']="Se eliminaran definitivamente el usuario y no podra ingresar mas al sistema.";
					$img='default.png';
					if($empleado[0]->fotografia!=NULL && $empleado[0]->fotografia!=""){ $img=$empleado[0]->fotografia; }
					$listado['img']=$url.$img;
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
   		}else{
   			echo "logout";
   		}
   	}
   	public function drop_usuario(){
		if(!empty($this->session_id)){
			$id=$_POST['ide'];
			$u=$_POST['u'];
			$p=$_POST['p'];
			if(strtolower($u)==strtolower($this->session->userdata("login"))){
				$usuario=$this->M_empleado->validate($u,$p);
				if(!empty($usuario)){
					if($id!=""){
						$empleado=$this->M_empleado->search_usuarios('e.ide',$id,true);
						if(!empty($empleado)){
							//verificando si existe cliente o proveedor
							if($empleado[0]->tipo==NULL && $empleado[0]->fecha_nacimiento==NULL && $empleado[0]->fecha_ingreso==NULL && $empleado[0]->salario==NULL){//solo es usuario de sistema
								//verificando si es cliente o proveedor
								$cliente=$this->M_cliente->get_cliente_row('nit',$empleado[0]->ci);
								$proveedor=$this->M_proveedor->get_row('nit',$empleado[0]->ci);
								if(empty($cliente) && empty($proveedor)){
									if($this->validaciones->eliminar_imagen($empleado[0]->fotografia,'./libraries/img/personas/')){
										if($this->M_persona->eliminar($empleado[0]->ci)){
											echo "ok";
										}else{
											echo "error";
										}
									}else{
										echo "error";
									}
								}else{
									if($this->M_empleado->eliminar($empleado[0]->ide)){
										echo "ok";
									}else{
										echo "error";
									}
								}
							}else{
								// es empleado en planta u oficina
								if($this->M_empleado->restat_usuario($id)){
									echo "ok";
								}else{
									echo "error";
								}
							}
						}else{
							echo "fail";
						}			
					}else{
						echo "fail";
					}
				}else{
					echo "validate";
				}
			}else{
				echo "validate";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE USUARIOS -------*/
/*------- MANEJO DE PRIVILEGIOS -------*/
	public function search_privilegio(){
		if(!empty($this->session_id)){
			$this->load->view("administrador/privilegio/search");
		}else{
			redirect(base_url().'login',301);
		}
	}
	public function view_privilegio(){
		if(!empty($this->session_id)){
			$atrib="";
			$val="";
			if(isset($_POST['ci']) && isset($_POST['nom'])){//si tenemos busqueda 
				$ci=$_POST['ci'];
				$nom=$_POST['nom'];
				if($ci!=""){
					$atrib="p.ci";$val=$ci;
				}else{
					if($nom!=""){
						$atrib="nombre";$val=$nom;
					}
				}
			}
			$listado['usuarios']=$this->M_empleado->search_usuarios($atrib,$val,true);
			$this->load->view("administrador/privilegio/view",$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Nuevo ---*/
	public function update_privilegio(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpri']) && isset($_POST['col'])){
				$idpri=trim($_POST['idpri']);
				$col=trim($_POST['col']);
				if($this->val->entero($idpri,0,10) && $this->val->strNoSpace($col,2,6)){
					$control=$this->M_privilegio->get_row_2n('idpri',$idpri,$col,"1");
					if(empty($control)){
						if($this->M_privilegio->modificar($idpri,$col,"1")){
							echo "1|ok";
						}else{
							echo "|error";
						}
					}else{
						if($this->M_privilegio->modificar($idpri,$col,"0")){
							echo "0|ok";
						}else{
							echo "|error";
						}
					}
				}else{
					echo "|fail";
				}
			}else{
				echo "|fail";
			}
		}else{
			echo "|logout";
		}
	}
	public function detail_modal(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpri']) && isset($_POST['col'])){
				$idpri=$_POST['idpri'];
				$col=$_POST['col'];
				if($this->val->entero($idpri,0,10) && $this->val->strNoSpace($col,2,6)){
					$privilegios=$this->M_privilegio->get($idpri);
					if(!empty($privilegios)){
						$listado['privilegio']=$privilegios[0];
						$listado['col']=$col;
						$this->load->view("administrador/privilegio/detalle/view",$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			redirect(base_url().'login',301);
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE PRIVILEGIOS -------*/













	public function view_mmpp(){
		if(!empty($this->session_id)){
			$listado['grupo']=$this->M_grupo->get_all_use_insumo();
			$listado['unidad']=$this->M_unidad->get_all_use_insumo();
			$listado['color']=$this->M_color->get_all_use_insumo();
			$listado['proveedores']=$this->M_proveedor->get_all();
			$this->load->view("administrador/view_mmpp",$listado);
		}else{
			echo "logout";
		}
	}
	public function save_grupo(){
		if(!empty($this->session_id)){
			$nom=$_POST['nom'];
			$des=$_POST['des'];
			if(count($nom)>0){
				$resp=$this->M_grupo->insertar($nom,$des);
				echo $resp;
			}else{
				echo "fail";
			}
			
		}else{
			echo "logout";
		}
	}

	public function mod_grupo(){
		if(!empty($this->session_id)){
			$id=$_POST['id'];
			$nom=$_POST['nom'];
			$des=$_POST['des'];
			if(count($nom)>0 & count($id)>0){
				$resp=$this->M_grupo->modificar($id,$nom,$des);
				echo $resp;
			}else{
				echo "fail";
			}
			
		}else{
			echo "logout";
		}
	}

	public function drop_grupo(){
		if (!empty($this->session_id)) {
			$id=$_POST['id'];
			if($id!=""){
				$del=$this->M_grupo->eliminar($id);
				echo $del;
				
			}else{
				echo "false";
			}
			
		}else{
			echo "logout";
		}
	}



	public function save_color(){
		if(!empty($this->session_id)){
			$nom=$_POST['nom'];
			$cod=$_POST['cod'];
			if(count($nom)>0){
				if(count($cod)<=0){
					$cod="#f9f9f9";	
				}
				$resp=$this->M_color->insertar($nom,$cod);
				echo $resp;
			}else{
				echo "fail";
			}
			
		}else{
			echo "logout";
		}
	}
	public function mod_color(){
		if(!empty($this->session_id)){
			$id=$_POST['id'];
			$nom=$_POST['nom'];
			$cod=$_POST['cod'];
			if(count($nom)>0 & count($id)>0){
				if(count($cod)<=0){
					$cod="#f9f9f9";
				}
				$resp=$this->M_color->modificar($id,$nom,$cod);
				echo $resp;
			}else{
				echo "fail";
			}
			
		}else{
			echo "logout";
		}
	}

	public function drop_color(){
		if (!empty($this->session_id)) {
			$id=$_POST['id'];
			if($id!=""){
				$del=$this->M_color->eliminar($id);
				echo $del;
				
			}else{
				echo "false";
			}
			
		}else{
			echo "logout";
		}
	}
	//proveedor 
	public function save_proveedor(){
		if(!empty($this->session_id)){
			$nom=$_POST['nom'];
			$telf=$_POST['telf'];
			$dir=$_POST['dir'];
			if(count($nom)>0 && $this->validaciones->esParrafo($nom,2,150)){
				if($this->M_proveedor->insertar($nom,$dir,$telf,'')){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function change_proveedor(){
		if(!empty($this->session_id)){
			$id=$_POST['id'];
			$nom=$_POST['nom'];
			$telf=$_POST['telf'];
			$dir=$_POST['dir'];
			if(count($nom)>0 && $this->validaciones->esParrafo($nom,2,150)){
				if($this->M_proveedor->modificar($id,$nom,$dir,$telf,'')){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}


	public function drop_proveedor(){
		if(!empty($this->session_id)){
			$id=$_POST['id'];
			if($this->M_proveedor->eliminar($id)){
				echo "ok";
			}else{
				echo "error";
			}
		}else{
			echo "logout";
		}
	}
	//ficha de control
	public function view_ficha_costos(){
		if(!empty($this->session_id)){
			$this->load->view("administrador/view_ficha_costos");
		}else{
			redirect(base_url().'login',301);
		}
	}

	public function view_form_usuario(){
		if(!empty($this->session_id)){
			$this->load->view("administrador/view_usuario");
		}else{
			redirect(base_url().'login',301);
		}
	}
	public function view_all_usuario(){
		if(!empty($this->session_id)){
			$listado['usuario']=$this->M_usuario->get_all();
			$this->load->view("administrador/ajax/usuario/a_usuario",$listado);
		}else{
			echo "logout";
		}
	}
	/*public function view_search_usuario(){
		if(!empty($this->session_id)){
			$atributo=$_POST['search'];
			$valor=$_POST['val'];
			if($atributo!="" & $valor!=""){
				$listado['usuario']=$this->M_usuario->get_where($atributo,$valor);
				$this->load->view("administrador/ajax/usuario/a_usuario",$listado);
			}else{
				echo "fail";
			}

		}else{
			echo "logout";
		}
	}*/

	public function view_new_user(){
		if(!empty($this->session_id)){
			$this->load->view("administrador/ajax/usuario/a_new_user");
		}else{
			echo "logout";
		}
	}
	public function save_user(){
		if(!empty($this->session_id)){
			$ci=$_POST['ced'];
			$nom=$_POST['nom'];
			$nom2=$_POST['nom2'];
			$pat=$_POST['pat'];
			$mat=$_POST['mat'];
			$car=$_POST['car'];
			$fot=$_POST['fot'];
			if(preg_match("/^[[:digit:]]{7,8}$/",$ci) & preg_match("/^[[:alpha:]áéíóú]{3,20}$/",$nom) & preg_match("/^[[:alpha:]áéíóú]{2,20}$/",$pat)){
				if(!preg_match("%\.(gif|jpe?g|png|GIF|JPE?G|PNG)$%i", $fot)){
					$fot="";
				}
				$res=$this->M_usuario->insertar($ci,$nom,$nom2,$pat,$mat,$car,$fot);
				echo $res;
			}else{
				echo "fail";
			}

		}else{
			echo "logout";
		}
	}

	public function uploadify(){
		$config['upload_path'] = "./libraries/img/usuarios";
		$config['allowed_types'] = '*';
		$config['max_size'] = 0;
		$this->load->library('upload', $config);

		if (!$this->upload->do_upload("userfile")){
			$response = $this->upload->display_errors();
		}
		else{
			$response = $this->upload->data();
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($response));
	}

	public function view_capital_humano(){
		if(!empty($this->session_id)){
			$listado['feriados']=$this->M_feriado->get_all();
			$this->load->view("administrador/view_capital_humano",$listado);
		}else{
			echo "logout";
		}
	}

	public function new_feriado(){
		if(!empty($this->session_id)){
			$fecha=$_POST['fecha'];
			$des=$_POST['des'];
			if($this->M_feriado->insertar($fecha,$des)){
				echo "ok";
			}else{
				echo "error";
			}
		}else{
			echo "logout";
		}
	}

	public function drop_feriado(){
		if(!empty($this->session_id)){
			$id=$_POST['id'];
			if($this->M_feriado->eliminar($id)){
				echo "ok";
			}else{
				echo "error";
			}
		}else{
			echo "logout";
		}
	}

	public function mod_feriado(){
		if(!empty($this->session_id)){
			$id=$_POST['id'];
			$fecha=$_POST['fecha'];
			$des=$_POST['des'];
			if($this->M_feriado->modificar($id,$fecha,$des)){
				echo "ok";
			}else{
				echo "error";
			}
		}else{
			echo "logout";
		}
	}

	public function view_produccion(){
		if(!empty($this->session_id)){
			$listado['procesos']=$this->M_proceso->get_all();
			$listado['materiales']=$this->M_insumo->get_all();
			$this->load->view("administrador/view_procesos",$listado);
		}else{
			echo "logout";
		}
	}


	/*public function update_material_indirecto(){
		if(!empty($this->session_id)){
			$idma=$_POST['m'];
			$nombre=$_POST['n'];
			if($this->validaciones->esPalabraConEspacio($nombre,1,300)){
				if($this->M_insumo->modificar($idma,$nombre)){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}*/
	/*public function drop_material_indirecto(){
		if(!empty($this->session_id)){
			$idma=$_POST['m'];
			if($this->M_insumo->eliminar($idma)){
				echo "ok";
			}else{
				echo "error";
			}
		}else{
			echo "logout";
		}
	}*/
}
/* End of file admin.php */
/* Location: ./application/controllers/admin.php */