<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class capital_humano extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('login');
		$this->session->set_userdata('ida','',true);
		$this->session->set_userdata('tipo','',true);
	}
	public function index(){
		if(!empty($this->session_id)){
			$privilegio=$privilegio=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			if($privilegio[0]->ca=="1"){
				if(!isset($_GET['p'])){
					if($privilegio[0]->ca1r==1){
						$listado['pestania']=1;
					}else{
						if($privilegio[0]->ca2r==1){ 
							$listado['pestania']=2;
						}else{
							if($privilegio[0]->ca3r==1){ 
								$listado['pestania']=5;
							}else{
								$listado['pestania']=0;
							}
						}
					}
				}else{
					$listado['pestania']=$_GET['p'];
				}
				$listado['privilegio']=$privilegio;
				$this->load->view('v_capital_humano',$listado);
			}else{
				$this->val->redireccion($privilegio);
			}
		}else{
			redirect(base_url().'login/input',301);
		}
	}
/*------- MANEJO DE CAPITAL HUMANO -------*/
	public function search_empleado(){
		if(!empty($this->session_id)){
			$listado['tipo_contratos']=$this->M_tipo_contrato->get_all();
			$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view('capital_humano/capital_humano/search',$listado);
		}else{
			echo "logout";
		}
	}
	public function view_empleado(){
		if(!empty($this->session_id)){
			$atrib="";$val="";
			if(isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['ci']) && isset($_POST['con']) && isset($_POST['sal']) && isset($_POST['tip'])){
				if($_POST['cod']!=""){
					$atrib='e.codigo';$val=$_POST['cod'];
				}else{
					if($_POST['nom']!=""){
						$atrib='nombre';$val=$_POST['nom'];
					}else{
						if($_POST['ci']!=""){
							$atrib='p.ci';$val=$_POST['ci'];
						}else{
							if($_POST['con']!=""){
								$atrib='e.idtc';$val=$_POST['con'];
							}else{
								if($_POST['sal']!=""){
									$atrib='e.salario';$val=$_POST['sal'];
								}else{
									if($_POST['tip']!=""){
										$atrib='e.tipo';$val=$_POST['tip'];
									}
								}
							}
						}
					}
				}
			}
		 	$listado['empleados']=$this->M_empleado->get_empleado($atrib,$val);
		 	$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view("capital_humano/capital_humano/view",$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Nuevo ---*/
	public function new_empleado(){
		 if(!empty($this->session_id)){
		 		$listado['contrato']=$this->M_tipo_contrato->get_all();
		 		$listado['ciudades']=$this->M_ciudad->get_all();
				$this->load->view("capital_humano/capital_humano/3-nuevo/view",$listado);
			}else{
				echo "logout";
			}
	}
	public function search_ci(){
		if(!empty($this->session_id)){
			if(isset($_POST['ci'])){
				$ci=$_POST['ci'];
				if($ci!=""){
					$p=$this->M_persona->get($ci);
					if(count($p)>0){
						$control=$this->M_ciudad->get_pais($p[0]->idci);
						if(!empty($control)){
							$paises=$this->M_pais->get_all();
							$ciudades=$this->M_ciudad->get_row('idpa',$control[0]->idpa);
							$option_ciudad="<option value=''>Seleccionar...</option>";
							for($ci=0;$ci<count($ciudades);$ci++){ $option_ciudad.="<option value='".$ciudades[$ci]->idci."'>".$ciudades[$ci]->nombre."</option>"; }
							$option_ciudad.="<option value='".$control[0]->idci."' selected>".$control[0]->ciudad."</option>";
							echo $p[0]->nombre."|".$p[0]->telefono."|".$p[0]->email."|".$option_ciudad."|".$p[0]->direccion."|".$p[0]->caracteristicas;							
						}
					}else{
						
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function save_empleado(){
		if(!empty($this->session_id)){
			if(isset($_POST['ci']) && isset($_POST['ciu']) && isset($_POST['nom1']) && isset($_POST['nom2']) && isset($_POST['pat']) && isset($_POST['mat']) && isset($_POST['cod']) && isset($_POST['te']) && isset($_POST['car']) && isset($_POST['ga']) && isset($_POST['tc']) && isset($_POST['sal']) && isset($_POST['tel']) && isset($_POST['ema']) && isset($_POST['nac']) && isset($_POST['ing']) && isset($_POST['dir']) && isset($_POST['obs']) && isset($_FILES)){
				$ci=trim($_POST['ci']);
				$ciu=trim($_POST['ciu']);
				$nom1=trim($_POST['nom1']);
				$nom2=trim($_POST['nom2']);
				$pat=trim($_POST['pat']);
				$mat=trim($_POST['mat']);
				$cod=trim($_POST['cod']);
				$te=trim($_POST['te']);
				$car=trim($_POST['car']);
				$ga=trim($_POST['ga']);
				$tc=trim($_POST['tc']);
				$sal=trim($_POST['sal']);
				$tel=trim($_POST['tel']);
				$ema=trim($_POST['ema']);
				$ing=trim($_POST['nac']);
				$nac=trim($_POST['ing']);
				$dir=trim($_POST['dir']);
				$obs=trim($_POST['obs']);
				if($this->val->entero($ci,6,8) && $ci>100000 && $ci<=99999999 && $this->val->entero($ciu,0,10) && $this->val->strSpace($nom1,2,20) && $this->val->strSpace($pat,2,20) && $this->val->entero($te,0,1) && $te>=0 && $te<=1 && $this->val->entero($tc,0,10) && $this->val->decimal($sal,4,1) && $sal>=0 && $sal<=9999.9){
					$control=true;
					if($nom2!=""){ if(!$this->val->strSpace($nom2,2,20)){ $control=false;}}
					if($mat!=""){ if(!$this->val->strSpace($mat,2,20)){ $control=false;}}
					if($cod!=""){ if(!$this->val->entero($cod,0,10) || $cod<0 || $cod>9999999999){ $control=false;}}
					if($car!=""){ if(!$this->val->strSpace($car,0,100)){ $control=false;}}
					if($ga!=""){ if(!$this->val->strSpace($ga,0,100)){ $control=false;}}
					if($tel!=""){ if(!$this->val->entero($tel,6,15) || $tel<100000 || $tel>999999999999999){ $control=false;}}
					if($ema!=""){ if(!$this->val->email($ema)){ $control=false;}}
					if($ing!=""){ if(!$this->val->fecha($ing)){ $control=false;}}
					if($nac!=""){ if(!$this->val->fecha($nac)){ $control=false;}}
					if($obs!=""){ if(!$this->val->textarea($obs,0,900)){ $control=false;}}
					if($control){
						$control=$this->M_tipo_contrato->get($tc);
						if(!empty($control)){
							$control=$this->M_ciudad->get($ciu);
							if(!empty($control)){
								$control=$this->M_persona->get($ci);
								if(empty($control)){
									$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/personas/','',$this->resize,$ci);
									if($img!='error' && $img!="error_type"){
										if($this->M_persona->insertar($ci,$ciu,$nom1,$tel,$ema,$dir,$img,$obs)){
											if($this->M_empleado->insertar($tc,$ci,$cod,$nom2,$pat,$mat,$car,$ga,$sal,$ing,$nac,$te)){
												echo "ok";
											}else{
												$this->M_persona->eliminar($nit);
												echo "error";
											}
										}else{
											echo "error";
										}
									}else{
										echo $img;
									}
								}else{
									$control=$this->M_empleado->get_row('ci',$ci);
									if(empty($control)){
										if($this->M_empleado->insertar($tc,$ci,$cod,$nom2,$pat,$mat,$car,$ga,$sal,$ing,$nac,$te)){
											echo "ok";
										}else{
											echo "error";
										}
									}else{
										echo "ci_exist";
									}
								}
								
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	public function imprimir_empleados(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				$listado['empleados']=$_POST['json'];
				$this->load->view('capital_humano/capital_humano/4-imprimir/config',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function arma_empleados(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				if(isset($_POST['v1'])){ if($_POST['v1']!="ok"){ $listado['v1']="ok";} }
				if(isset($_POST['v2'])){ if($_POST['v2']!="ok"){ $listado['v2']="ok";} }
				if(isset($_POST['v3'])){ if($_POST['v3']!="ok"){ $listado['v3']="ok";} }
				if(isset($_POST['v4'])){ if($_POST['v4']!="ok"){ $listado['v4']="ok";} }
				if(isset($_POST['v5'])){ if($_POST['v5']!="ok"){ $listado['v5']="ok";} }
				if(isset($_POST['v6'])){ if($_POST['v6']!="ok"){ $listado['v6']="ok";} }
				if(isset($_POST['v7'])){ if($_POST['v7']!="ok"){ $listado['v7']="ok";} }
				if(isset($_POST['v8'])){ if($_POST['v8']!="ok"){ $listado['v8']="ok";} }
				if(isset($_POST['v9'])){ if($_POST['v9']!="ok"){ $listado['v9']="ok";} }
				if(isset($_POST['v10'])){ if($_POST['v10']!="ok"){ $listado['v10']="ok"; } }
				if(isset($_POST['v11'])){ if($_POST['v11']!="ok"){ $listado['v11']="ok"; } }
				if(isset($_POST['v12'])){ if($_POST['v12']!="ok"){ $listado['v12']="ok"; } }
				if(isset($_POST['v13'])){ if($_POST['v13']!="ok"){ $listado['v13']="ok"; } }
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=34;}
				$listado['empleados']=$_POST['json'];
				$this->load->view('capital_humano/capital_humano/4-imprimir/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Estado ---*/
   	public function cambiar_estado(){
		if(!empty($this->session_id)){
			if(isset($_POST['ide'])){
				$ide=$_POST['ide'];
				$empleado=$this->M_empleado->get($ide);
				if(!empty($empleado)){
					$estado=$empleado[0]->estado;
					if($estado==1){ $estado=0;}else{$estado=1;}
					if($this->M_empleado->modificar_row($ide,'estado',$estado)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	/*--- End Estado ---*/
   	/*--- Reportes ---*/
   	public function detalle_empleado(){
		if(!empty($this->session_id)){
			if(isset($_POST['ide'])){
				$ide=$_POST['ide'];
				$empleado=$this->M_empleado->get_empleado('e.ide',$ide);
				if(!empty($empleado)){
					$listado['empleado']=$empleado[0];
					$this->load->view("capital_humano/capital_humano/5-reportes/view",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function ficha_control(){
		if(!empty($this->session_id)){
			if(isset($_POST['ide'])){
				$ide=$_POST['ide'];
				if(isset($_POST['max'])){ $listado['max']=$_POST['max'];}else{ $listado['max']=150;}
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=22;}
				if(isset($_POST['ges'])){ $listado['ges']=$_POST['ges'];}else{ $listado['ges']=date('Y');}
				$empleado=$this->M_empleado->get_proceso($ide);
				if(!empty($empleado)){
					$listado['empleado']=$empleado[0];
					$this->load->view("capital_humano/capital_humano/5-reportes/ficha_control",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
	public function change_empleado(){
		if(!empty($this->session_id)){
			if(isset($_POST['ide'])){
				$ide=$_POST['ide'];
				$listado['empleado']=$this->M_empleado->get_empleado('e.ide',$ide);
				$listado['contrato']=$this->M_tipo_contrato->get_all();
		 		$listado['ciudades']=$this->M_ciudad->get_all();
				$this->load->view("capital_humano/capital_humano/6-config/view",$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_empleado(){ // guarda datos del empleado si hacer cambios en el fotografia
		if(!empty($this->session_id)){
			if(isset($_POST['ide']) && isset($_POST['ci']) && isset($_POST['ciu']) && isset($_POST['nom1']) && isset($_POST['nom2']) && isset($_POST['pat']) && isset($_POST['mat']) && isset($_POST['cod']) && isset($_POST['te']) && isset($_POST['car']) && isset($_POST['ga']) && isset($_POST['tc']) && isset($_POST['sal']) && isset($_POST['tel']) && isset($_POST['ema']) && isset($_POST['nac']) && isset($_POST['ing']) && isset($_POST['dir']) && isset($_POST['obs']) && isset($_FILES)){
				$ide=trim($_POST['ide']);
				$ci=trim($_POST['ci']);
				$ciu=trim($_POST['ciu']);
				$nom1=trim($_POST['nom1']);
				$nom2=trim($_POST['nom2']);
				$pat=trim($_POST['pat']);
				$mat=trim($_POST['mat']);
				$cod=trim($_POST['cod']);
				$te=trim($_POST['te']);
				$car=trim($_POST['car']);
				$ga=trim($_POST['ga']);
				$tc=trim($_POST['tc']);
				$sal=trim($_POST['sal']);
				$tel=trim($_POST['tel']);
				$ema=trim($_POST['ema']);
				$ing=trim($_POST['nac']);
				$nac=trim($_POST['ing']);
				$dir=trim($_POST['dir']);
				$obs=trim($_POST['obs']);
				if($this->val->entero($ide,0,10) && $this->val->entero($ci,6,8) && $ci>100000 && $ci<=99999999 && $this->val->entero($ciu,0,10) && $this->val->strSpace($nom1,2,20) && $this->val->strSpace($pat,2,20) && $this->val->entero($te,0,1) && $te>=0 && $te<=1 && $this->val->entero($tc,0,10) && $this->val->decimal($sal,4,1) && $sal>=0 && $sal<=9999.9){
					$control=true;
					if($nom2!=""){ if(!$this->val->strSpace($nom2,2,20)){ $control=false;}}
					if($mat!=""){ if(!$this->val->strSpace($mat,2,20)){ $control=false;}}
					if($cod!=""){ if(!$this->val->entero($cod,0,10) || $cod<0 || $cod>9999999999){ $control=false;}}
					if($car!=""){ if(!$this->val->strSpace($car,0,100)){ $control=false;}}
					if($ga!=""){ if(!$this->val->strSpace($ga,0,100)){ $control=false;}}
					if($tel!=""){ if(!$this->val->entero($tel,6,15) || $tel<100000 || $tel>999999999999999){ $control=false;}}
					if($ema!=""){ if(!$this->val->email($ema)){ $control=false;}}
					if($ing!=""){ if(!$this->val->fecha($ing)){ $control=false;}}
					if($nac!=""){ if(!$this->val->fecha($nac)){ $control=false;}}
					if($obs!=""){ if(!$this->val->textarea($obs,0,900)){ $control=false;}}
					if($control){
						$control=$this->M_tipo_contrato->get($tc);
						if(!empty($control)){
							$control=$this->M_ciudad->get($ciu);
							if(!empty($control)){
								$empleado=$this->M_empleado->get_empleado('e.ide',$ide);
								if(!empty($empleado)){
									$ci_org=$empleado[0]->ci;
									$control=$this->M_persona->get($ci);
									$guardar="ok";
									if(!empty($control)){
										if($ci==$ci_org){
											$guardar="ok";
										}else{
											$guardar="ci_exist";
										}
									}
									if($guardar=="ok"){
										$img=$empleado[0]->fotografia;
										$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/personas/','',$this->resize,$img,$ci);//cambiar_imagen_miniatura($FILES,$ruta,$pos,$resize,$origen,$id)
										if($img!='error' && $img!="error_type"){
											if($this->M_persona->modificar($ci_org,$ci,$ciu,$nom1,$tel,$ema,$dir,$img,$obs)){
												if($this->M_empleado->modificar($ide,$tc,$ci,$cod,$nom2,$pat,$mat,$car,$ga,$sal,$ing,$nac,$te)){
													echo "ok";
												}else{
													$this->M_persona->eliminar($nit);
													echo "error";
												}
											}else{
												echo "error";
											}
										}else{
											echo $img;
										}
									}else{
										echo $guardar;
									}
								}else{
									echo "fail";
								}								
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function config_empleado(){
		if(!empty($this->session_id)){
			if(isset($_POST['ide'])){
				$ide=$_POST['ide'];
				if(isset($_POST['anio'])){
					if($_POST['anio']!=""){
						$anio=$_POST['anio'];
					}else{
						$anio=date('Y');
					}
				}else{
					$anio=date('Y');
				}
				$listado['empleado']=$this->M_empleado->get_empleado('e.ide',$ide);
				$listado['anticipos']=$this->M_anticipo->get_anio($ide,$anio);
				$listado['anio']=$anio;
				$listado['ayudantes']=$this->M_ayudante->get_row_complet('a.maestro',$ide);
				$listado['empleado_procesos']=$this->M_empleado_proceso->get_proceso($ide);
				$listado['procesos']=$this->M_proceso->get_all();
				$this->load->view("capital_humano/capital_humano/6-config/config",$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	/*--- Ayudantes ---*/
	public function search_ayudantes(){
		if(!empty($this->session_id)){
			if(isset($_POST['ide'])){
				$ide=$_POST['ide'];
				$empleado=$this->M_empleado->get($ide);
				if(!empty($empleado)){
					$listado['ide']=$ide;
					$this->load->view("capital_humano/capital_humano/6-config/ayudantes/search",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function view_ayudantes(){
		if(!empty($this->session_id)){
			if(isset($_POST['ide'])){
				$atrib="";$val="";
				if(isset($_POST['ci']) && isset($_POST['nom'])){
					if($_POST['ci']!=""){
						$atrib="p.ci";$val=$_POST['ci'];
					}else{
						if($_POST['nom']!=""){
							$atrib="nombre";$val=$_POST['nom'];
						}
					}
				}
				$ide=$_POST['ide'];
				$listado['ide']=$ide;
				$listado['ayudantes']=$this->M_empleado->get_maestro_ayudantes($ide,$atrib,$val);
				$this->load->view("capital_humano/capital_humano/6-config/ayudantes/view",$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function select_ayudante(){
		if(!empty($this->session_id)){
			if(isset($_POST['maestro']) && isset($_POST['ayudante'])){
				$maestro=$_POST['maestro'];
				$ayudante=$_POST['ayudante'];
				if($this->val->entero($maestro,0,10) && $this->val->entero($ayudante,0,10)){
					$control=$this->M_empleado->get($maestro);
					if(!empty($control)){
						$control=$this->M_empleado->get($ayudante);
						if(!empty($control)){
							$control=$this->M_ayudante->get_row_2n('maestro',$maestro,'ayudante',$ayudante);
							if(empty($control)){
								if($this->M_ayudante->insertar($maestro,$ayudante,date('Y-m-d'))){
									echo "1|ok";
								}else{
									echo "|error";
								}
							}else{
								if($this->M_ayudante->eliminar($control[0]->iday)){
									echo "0|ok";
								}else{
									echo "|error";
								}
							}
							
						}else{
							echo "|fail";
						}
					}else{
						echo "|fail";
					}
				}else{
					echo "|fail";
				}
			}else{
				echo "|fail";
			}
		}else{
			echo "|logout";
		}
	}
	public function drop_ayudante(){
		if(!empty($this->session_id)){
			if(isset($_POST['iday'])){
				$iday=$_POST['iday'];
				if($this->M_ayudante->eliminar($iday)){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	/*--- End ayudantes ---*/
	/*--- Procesos ---*/
	public function save_proceso(){
		if(!empty($this->session_id)){
			if(isset($_POST['ide']) && isset($_POST['idpr']) && isset($_POST['tip'])){
				$ide=$_POST['ide'];
				$idpr=$_POST['idpr'];
				$tip=$_POST['tip'];
				if($this->val->entero($ide,0,10) && $this->val->entero($idpr,0,10) && $this->val->entero($tip,0,1) && $tip>=0 && $tip<=1){
					if($this->M_empleado_proceso->insertar($ide,$idpr,$tip,date('Y-m-d H-m-s'))){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_proceso(){
		if(!empty($this->session_id)){
			if(isset($_POST['idep']) && isset($_POST['idpr']) && isset($_POST['tip'])){
				$idep=$_POST['idep'];
				$idpr=$_POST['idpr'];
				$tip=$_POST['tip'];
				if($this->val->entero($idep,0,10) && $this->val->entero($idpr,0,10) && $this->val->entero($tip,0,1) && $tip>=0 && $tip<=1){
					if($this->M_empleado_proceso->modificar($idep,$idpr,$tip,date('Y-m-d H-m-s'))){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function drop_proceso(){
		if(!empty($this->session_id)){
			if(isset($_POST['idep'])){
				$idep=$_POST['idep'];
				if($this->M_empleado_proceso->eliminar($idep)){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	/*--- End Procesos ---*/
	/*--- Anticipos ---*/
	public function save_anticipo(){
		if(!empty($this->session_id)){
			if(isset($_POST['ide']) && isset($_POST['fec']) && isset($_POST['mon']) && isset($_POST['des'])){
				$ide=$_POST['ide'];
				$fec=$_POST['fec'];
				$mon=$_POST['mon'];
				$des=$_POST['des'];
				if($this->val->entero($ide,0,10) && $this->val->fecha($fec) && $this->val->decimal($mon,5,1) && $mon>0 && $mon<=99999.9 && $this->val->textarea($des,0,300)){
					if($this->M_anticipo->insertar($ide,$fec,$mon,$des)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_anticipo(){
		if(!empty($this->session_id)){
			if(isset($_POST['idan']) && isset($_POST['fec']) && isset($_POST['mon']) && isset($_POST['des'])){
				$idan=$_POST['idan'];
				$fec=$_POST['fec'];
				$mon=$_POST['mon'];
				$des=$_POST['des'];
				if($this->val->entero($idan,0,10) && $this->val->fecha($fec) && $this->val->decimal($mon,5,1) && $mon>0 && $mon<=99999.9 && $this->val->textarea($des,0,300)){
					if($this->M_anticipo->modificar($idan,$fec,$mon,$des)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function drop_anticipo(){
		if(!empty($this->session_id)){
			if(isset($_POST['idan'])){
				$idan=$_POST['idan'];
				if($this->M_anticipo->eliminar($idan)){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	/*--- End Anticipos ---*/
	/*--- Restablecer contraceña ---*/
   	public function confirmar_password(){
   		if(!empty($this->session_id)){
   			if(isset($_POST['ide'])){
				$ide=$_POST['ide'];
				$url="./libraries/img/personas/miniatura/";
				$empleado=$this->M_empleado->get_empleado('e.ide',$ide);
				if(count($empleado)>0){
					$listado['titulo']="restablecer la contraseña de ".$empleado[0]->nombre." ".$empleado[0]->nombre2." ".$empleado[0]->paterno." ".$empleado[0]->materno;
					$listado['desc']="La contraseña por defecto sera el número de cedula de identidad del empleado.";
					$img='default.png';
					if($empleado[0]->fotografia!=NULL && $empleado[0]->fotografia!=""){ $img=$empleado[0]->fotografia; }
					$listado['img']=$url.$img;
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
   		}else{
   			echo "logout";
   		}
   	}
   	public function change_password(){
		if(!empty($this->session_id)){
			if(isset($_POST['ide'])){
				$ide=$_POST['ide'];
				$u=$_POST['u'];
				$p=$_POST['p'];
				if(strtolower($u)==strtolower($this->session->userdata("login"))){
					$usuario=$this->M_empleado->validate($u,$p);
					if(!empty($usuario)){
							$empleado=$this->M_empleado->get($ide);
							if(!empty($empleado)){
								$password=$this->lib->password($empleado[0]->ci);
								if($this->M_empleado->modificar_row($ide,'password',$password)){
			      					echo "ok";
			      				}else{
			      					echo "error";
			      				}
							}else{
								echo "fail";
							}			
					}else{
						echo "validate";
					}
				}else{
					echo "validate";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	/*--- End Restablecer contraceña ---*/
	/*--- Control de feriados ---*/
	public function update_c_feriado(){
   		if(!empty($this->session_id)){
   			if(isset($_POST['ide']) && isset($_POST['feriado'])){
   				$ide=$_POST['ide'];
   				$feriado=$_POST['feriado'];
   				if($this->val->entero($ide,0,10) && ($feriado==0 || $feriado==1)){
   					if($this->M_empleado->modificar_row($ide,'c_feriado',$feriado)){
   						echo "ok";
   					}else{
   						echo "error";
   					}
   				}else{
   					echo "fail";
   				}
   			}else{
   				echo "fail";
   			}
   		}else{
   			echo "logout";
   		}
   	}
	/*--- End control de feriados ---*/
	/*--- Control de feriados ---*/
	public function update_c_descuento(){
   		if(!empty($this->session_id)){
   			if(isset($_POST['ide']) && isset($_POST['des'])){
   				$ide=$_POST['ide'];
   				$des=$_POST['des'];
   				if($this->val->entero($ide,0,10) && ($des==0 || $des==1)){
   					if($this->M_empleado->modificar_row($ide,'c_descuento',$des)){
   						echo "ok";
   					}else{
   						echo "error";
   					}
   				}else{
   					echo "fail";
   				}
   			}else{
   				echo "fail";
   			}
   		}else{
   			echo "logout";
   		}
   	}
	/*--- End control de feriados ---*/
/*--- End configuracion ---*/
/*--- Eliminar ---*/
   	public function confirmar_empleado(){
   		if(!empty($this->session_id)){
   			if(isset($_POST['ide'])){
				$ide=$_POST['ide'];
				$url="./libraries/img/personas/miniatura/";
				$empleado=$this->M_empleado->get_empleado('e.ide',$ide);
				if(count($empleado)>0){
					$listado['titulo']="eliminar a ".$empleado[0]->nombre." ".$empleado[0]->nombre2." ".$empleado[0]->paterno." ".$empleado[0]->materno." como empleado(a) de la empresa";
					$control=$this->M_venta->get_row('ide',$ide);
					if(count($control)>0){//el empleado se encuentra en uso en las ventas
						$listado['open_control']="true";
						$listado['control']="";
						$listado['desc']="Imposible Eliminar: El empleado actualmente se encuentra en uso de ".count($control)." venta(s)., pues como encargado del sistema registro ".count($control)." venta(s).";	
					}else{
						$control=$this->M_gasto->get_row('ide',$ide);
						if(count($control)>0){//el empleado se encuentra en uso en las gasto
							$listado['open_control']="true";
							$listado['control']="";
							$listado['desc']="Imposible Eliminar: El empleado actualmente se encuentra en uso de ".count($control)." registro(s) de compras, pues como encargado del sistema registro ".count($control)." compras(s) o egreso(s).";	
						}else{
							$control=$this->M_archivo_biometrico->get_row('ide',$ide);
							if(count($control)>0){//el empleado se encuentra en uso en horas biometrico
								$listado['open_control']="true";
								$listado['control']="";
								$listado['desc']="Imposible Eliminar: El empleado actualmente se encuentra en uso de ".count($control)." registros de archivos biometrico, pues como encargado del sistema registro ".count($control)." archivos de horas biometrico.";	
							}else{
								$listado['desc']="Se eliminaran definitivamente el empleado.";
							}
						}
					}
					$img='default.png';
					if($empleado[0]->fotografia!=NULL && $empleado[0]->fotografia!=""){ $img=$empleado[0]->fotografia; }
					$listado['img']=$url.$img;
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
   		}else{
   			echo "logout";
   		}
   	}
   	public function drop_empleado(){
		if(!empty($this->session_id)){
			$id=$_POST['ide'];
			$u=$_POST['u'];
			$p=$_POST['p'];
			if(strtolower($u)==strtolower($this->session->userdata("login"))){
				$usuario=$this->M_empleado->validate($u,$p);
				if(!empty($usuario)){
					if($id!=""){
						$empleado=$this->M_empleado->get_empleado('e.ide',$id);
						if(!empty($empleado)){
							//verificando si existe cliente o proveedor
							$proveedor=$this->M_proveedor->get_row('nit',$empleado[0]->ci);
							$cliente=$this->M_cliente->get_row_2('nit',$empleado[0]->ci);
							if(empty($proveedor) && empty($cliente)){
								if($this->lib->eliminar_imagen($empleado[0]->fotografia,'./libraries/img/personas/')){
									if($this->M_persona->eliminar($empleado[0]->ci)){
										echo "ok";
									}else{
										echo "error";
									}
								}else{
									echo "error";
								}
							}else{
								if($this->M_empleado->eliminar($id)){
									echo "ok";
								}else{
									echo "error";
								}
							}
						}else{
							echo "fail";
						}			
					}else{
						echo "fail";
					}
				}else{
					echo "validate";
				}
			}else{
				echo "validate";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE CAPITAL HUMANO -------*/
/*------- MANEJO DE PLANILLA DE SUELDOS -------*/
	public function search_planilla(){//vista de buscador de salario
		if(!empty($this->session_id)){
		 	$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view("capital_humano/planilla/search",$listado);
		}else{
			echo "logout";
		}
	}
	public function view_planilla(){//vista de los salarios de todos los empleados
		if(!empty($this->session_id)){
			if(isset($_POST['f1']) && isset($_POST['f2'])){
				$vf=explode("-", $_POST['f1']);
				$f1=$vf[0]."-".$vf[1]."-01";
				$vf=explode("-", $_POST['f2']);
				$f2=$vf[0]."-".$vf[1]."-".$this->lib->dosDigitos($this->lib->ultimo_dia($vf[1],$vf[0]));
			}else{
				$aux=strtotime('-1 month' ,strtotime(date('Y-m').'-01'));
				$f1=date('Y-m-d',$aux);
				
				$f2=date('Y')."-".$this->lib->dosDigitos(date("m")-1).'-'.$this->lib->dosDigitos($this->lib->ultimo_dia(date('m')-1,date('Y')));
			}
			$atrib="";$val="";
			if(isset($_POST['nom']) && isset($_POST['ci'])){
				if($_POST['nom']!=""){
					$atrib='nombre';$val=$_POST['nom'];
				}else{
					if($_POST['ci']!=""){
						$atrib='p.ci';$val=$_POST['ci'];
					}
				}	
			}
			$listado['f1']=$f1;
			$listado['f2']=$f2;
		 	$listado['empleados']=$this->M_empleado->get_empleado_activo($atrib,$val);
			$listado['horas']=$this->M_hora_biometrico->exist_fecha("","",$f1,$f2);
			$listado['feriados']=$this->M_feriado->get_fechas($f1,$f2);
		 	$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
		 	$listado['configuraciones']=$this->M_configuracion->get_all();
			$this->load->view("capital_humano/planilla/view",$listado);
			
		}else{
			echo "logout";
		}
	}
	public function exportar_excel(){ //vista de los salarios de todos los empleados
		if(!empty($this->session_id)){
			if(isset($_POST['f1']) && isset($_POST['f2'])){
				$f1=$_POST['f1'];
				$f2=$_POST['f2'];
			}else{
				$f1=date('Y-m').'-01';
				$f2=date('Y-m').'-'.$this->validaciones->convierteMes($this->validaciones->ultimo_dia(date('m'),date('Y')));
			}
			$listado['f1']=$f1;
			$listado['f2']=$f2;
			$listado['empleado']=$this->M_empleado->get_all();
			$this->load->view("capital_humano/exportar",$listado);
		}else{
			echo "logout";
		}
	}
   	/*--- Buscador ---*/
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
	public function new_biometrico(){//vista de buscador de salario
		if(!empty($this->session_id)){
			$this->load->view("capital_humano/planilla/3-nuevo/view");
		}else{
			echo "logout";
		}
	}
	public function save_biometrico(){//vista de buscador de salario
		if(!empty($this->session_id)){
			if(isset($_FILES['archivo']) && isset($_POST['tipo'])){
				set_time_limit(180000);
				$tipo=$_POST['tipo'];//true or false
				$archivo=$_FILES['archivo'];
				/*Controlando si es una archivo válido*/
				$datos=$this->excel->read_file($archivo['tmp_name']);
				/*End Control*/
				if($this->lib->biometrico_valido($datos)){
					$nom_arch=$this->lib->subir_excel($archivo,'./libraries/files/biometrico/',rand(0,99999999).'-'.$this->session->userdata('id'),'xls');
					if($nom_arch!="error" && $nom_arch!="error_type"){
						if($this->M_archivo_biometrico->insertar($this->session->userdata('id'),$nom_arch,date('Y-m-d H:m:s'))){
							$datos=$this->excel->read_file('./libraries/files/biometrico/'.$nom_arch);//abrimos el archivo
							$fechas=json_decode($this->lib->min_max_fecha_biometrico($datos));
							if($this->M_hora_biometrico->eliminar_rango($fechas->fecha_min,$fechas->fecha_max)){
								$empleados=$this->M_empleado->get_all();
								//$hora_biometrico=$this->M_hora_biometrico->get_all();
								for($i=0; $i < count($empleados); $i++){ $empleado=$empleados[$i];
									$hras_empleado=json_decode($this->lib->para_insertar_biometrico($empleado,$tipo,$datos));
									foreach($hras_empleado as $row) :
										if($this->M_hora_biometrico->insertar($empleado->ide,$row->fecha,$row->hora)){ }
									endforeach;
								}
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "error";
						}
					}else{
						echo $nom_arch;
					}
				}else{
					echo "file_content_error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	public function imprimir_planilla(){
		if(!empty($this->session_id)){
			if(isset($_POST['json'])){
				$listado['empleados']=$_POST['json'];
				$listado['f1']=$_POST['f1'];
				$listado['f2']=$_POST['f2'];
				$this->load->view('capital_humano/planilla/4-imprimir/config',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	public function arma_planilla(){
		if(!empty($this->session_id)){
			if(isset($_POST['json']) && isset($_POST['f1']) && isset($_POST['f2'])){
				if(isset($_POST['v1'])){ if($_POST['v1']!="ok"){ $listado['v1']="ok";} }
				if(isset($_POST['v2'])){ if($_POST['v2']!="ok"){ $listado['v2']="ok";} }
				if(isset($_POST['v3'])){ if($_POST['v3']!="ok"){ $listado['v3']="ok";} }
				if(isset($_POST['v4'])){ if($_POST['v4']!="ok"){ $listado['v4']="ok";} }
				if(isset($_POST['v5'])){ if($_POST['v5']!="ok"){ $listado['v5']="ok";} }
				if(isset($_POST['v6'])){ if($_POST['v6']!="ok"){ $listado['v6']="ok";} }
				if(isset($_POST['v7'])){ if($_POST['v7']!="ok"){ $listado['v7']="ok";} }
				if(isset($_POST['v8'])){ if($_POST['v8']!="ok"){ $listado['v8']="ok";} }
				if(isset($_POST['v9'])){ if($_POST['v9']!="ok"){ $listado['v9']="ok";} }
				if(isset($_POST['v10'])){ if($_POST['v10']!="ok"){ $listado['v10']="ok"; } }
				if(isset($_POST['v11'])){ if($_POST['v11']!="ok"){ $listado['v11']="ok"; } }
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=34;}
				$listado['f1']=$_POST['f1'];
				$listado['f2']=$_POST['f2'];
				$listado['empleados']=$_POST['json'];
				$listado['horas']=$this->M_hora_biometrico->exist_fecha("","",$_POST['f1'],$_POST['f2']);
				$listado['feriados']=$this->M_feriado->get_fechas($_POST['f1'],$_POST['f2']);
				$listado['configuraciones']=$this->M_configuracion->get_all();
				$this->load->view('capital_humano/planilla/4-imprimir/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/

   	public function pae(){
   		if(!empty($this->session_id)){
			if(isset($_POST['ide']) && isset($_POST['f1']) && isset($_POST['f2'])){
				$empleado=$this->M_empleado->get_empleado('e.ide',$_POST['ide']);
				if(!empty($empleado)){
					$listado['ide']=$_POST['ide'];
					$listado['fe1']=$_POST['f1'];
					$listado['fe2']=$_POST['f2'];
					$listado['empleado']=$empleado[0];
					$listado['feriados']=$this->M_feriado->get_fechas($_POST['f1'],$_POST['f2']);
					$this->load->view('capital_humano/planilla/5-reportes/pae',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
   	}
	public function reporte_planilla(){
		if(!empty($this->session_id)){
			if(isset($_POST['ide']) && isset($_POST['f1']) && isset($_POST['f2'])){
				$ide=$_POST['ide'];
				$empleado=$this->M_empleado->get_empleado('e.ide',$ide);
				if(!empty($empleado)){
					if(!isset($_POST['nro'])){ $listado['nro']=63;
					}else{ $listado['nro']=$_POST['nro']; }
					$listado['ide']=$_POST['ide'];
					$listado['f1']=$_POST['f1'];
					$listado['f2']=$_POST['f2'];
					$listado['horas']=$this->M_hora_biometrico->exist_fecha('ide',$_POST['ide'],$_POST['f1'],$_POST['f2']);
					$vf=explode("-", $_POST['f1']);
					$fini=$vf[0]."-".$vf[1]."-01";
					$vf=explode("-", $_POST['f2']);
					$ffin=$vf[0]."-".$vf[1]."-31";
					$listado['feriados']=$this->M_feriado->get_fechas($fini,$ffin);
					$listado['empleado']=$empleado[0];
					$listado['anticipos']=$this->M_anticipo->get_empleado_fecha($ide,$_POST['f1'],$_POST['f2']);
					$listado['configuraciones']=$this->M_configuracion->get_all();
					$this->load->view("capital_humano/planilla/5-reportes/view",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "error";
			}
		}else{
			echo "logout";
		}
	}   	
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	public function detalle_anticipos(){
   		if(!empty($this->session_id)){
			if(isset($_POST['ide']) && isset($_POST['f1']) && isset($_POST['f2'])){
				$ide=$_POST['ide'];
				$f1=$_POST['f1'];
				$f2=$_POST['f2'];
				if($this->val->fecha($f1) && $this->val->fecha($f2) && $this->val->entero($ide,0,10)){
					$empleado=$this->M_empleado->get_empleado('e.ide',$ide);
					if(!empty($empleado)){
						$listado['empleado']=$empleado[0];
						$listado['anticipos']=$this->M_anticipo->get_empleado_fecha($ide,$f1,$f2);
						$listado['f1']=$f1;
						$listado['f2']=$f2;
						$this->load->view('capital_humano/planilla/6-config/anticipos/view',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
   	}
   	public function config_planilla(){
   		if(!empty($this->session_id)){
			if(isset($_POST['ide']) && isset($_POST['f1']) && isset($_POST['f2'])){
				$ide=$_POST['ide'];
				$empleado=$this->M_empleado->get_empleado('e.ide',$ide);
				if(count($empleado)>0){
					$listado['ide']=$_POST['ide'];
					$listado['f1']=$_POST['f1'];
					$listado['f2']=$_POST['f2'];
					$listado['horas']=$this->M_hora_biometrico->exist_fecha('ide',$_POST['ide'],$_POST['f1'],$_POST['f2']);
					$vf=explode("-", $_POST['f1']);
					$fini=$vf[0]."-".$vf[1]."-01";
					$vf=explode("-", $_POST['f2']);
					$ffin=$vf[0]."-".$vf[1]."-31";
					$listado['feriados']=$this->M_feriado->get_fechas($fini,$ffin);
					$listado['empleado']=$empleado[0];
					$listado['anticipos']=$this->M_anticipo->get_empleado_fecha($ide,$_POST['f1'],$_POST['f2']);
					$listado['configuraciones']=$this->M_configuracion->get_all();
					$this->load->view("capital_humano/planilla/6-config/view",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
   	}
   	public function add_hora(){
		if(!empty($this->session_id)){
			if(isset($_POST['fecha'])){
				$fecha=$_POST['fecha'];
				$listado['fecha']=$fecha;
				$this->load->view("capital_humano/planilla/6-config/add_hora",$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function save_hora(){
		if(!empty($this->session_id)){
			if(isset($_POST['fecha']) && isset($_POST['hora']) && isset($_POST['ide'])){
				$fecha=$_POST['fecha'];
				$hora=$_POST['hora'];
				$ide=$_POST['ide'];
				if($this->val->fecha($fecha) && $this->val->entero($ide,0,10)){
					if($this->M_hora_biometrico->insertar($ide,$fecha,$hora)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "error";
			}
		}else{
			echo "logout";
		}
	}
	public function drop_hora(){
		if(!empty($this->session_id)){
			if(isset($_POST['idhb'])){
				$idhb=$_POST['idhb'];
				if($this->M_hora_biometrico->eliminar($idhb)){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "error";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE PLANILLA DE SUELDOS -------*/
/*------- MANEJO DE CONFIGURACION -------*/
	public function view_config(){
		if(!empty($this->session_id)){
			$listado['paises']=$this->M_pais->get_all();
			$listado['ciudades']=$this->M_ciudad->get_all();
			$listado['feriados']=$this->M_feriado->get_all();
			$listado['tipo_contratos']=$this->M_tipo_contrato->get_all();
			$listado['configuraciones']=$this->M_configuracion->get_all();
			$this->load->view('capital_humano/config/view',$listado);
		}else{
			echo "logout";
		}
	}
	/*--- Pais ---*/
   	public function save_pais(){
		if(!empty($this->session_id)){
			if(isset($_POST['p'])){
				if($this->validaciones->esPalabraConEspacio($_POST['p'],2,100)){
					if($this->M_pais->insertar($_POST['p'])){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_pais(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpa']) && isset($_POST['p'])){
				if($_POST['idpa']!="" && $this->validaciones->esPalabraConEspacio($_POST['p'],2,100)){
					if($this->M_pais->modificar($_POST['idpa'],$_POST['p'])){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function delete_pais(){
		if(!empty($this->session_id)){
			if(isset($_POST['idpa'])){
				if($this->M_pais->eliminar($_POST['idpa'])){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Pais ---*/
   	/*--- Ciudad ---*/
   	public function save_ciudad(){
		if(!empty($this->session_id)){
			if(isset($_POST['cp']) && isset($_POST['c'])){
				if($this->validaciones->esPalabraConEspacio($_POST['c'],2,100) && isset($_POST['cp'])!=""){
					if($this->M_ciudad->insertar($_POST['cp'],$_POST['c'])){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_ciudad(){
		if(!empty($this->session_id)){
			if(isset($_POST['idci']) && isset($_POST['c']) && isset($_POST['cp'])){
				if($_POST['idci']!="" && $this->validaciones->esPalabraConEspacio($_POST['c'],2,100) && $_POST['cp']!=""){
					if($this->M_ciudad->modificar($_POST['idci'],$_POST['cp'],$_POST['c'])){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function delete_ciudad(){
		if(!empty($this->session_id)){
			if(isset($_POST['idci'])){
				if($this->M_ciudad->eliminar($_POST['idci'])){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
   	/*--- End Ciudad ---*/
   	/*--- manejo de feriados ---*/
	public function save_feriado(){
		if(!empty($this->session_id)){
			if(isset($_POST['fec']) && isset($_POST['tip']) && isset($_POST['des'])){
				$f=$_POST['fec'];
				$tip=$_POST['tip'];
				$d=$_POST['des'];
				if($this->val->fecha($f) && $this->val->entero($tip,0,10) && $this->val->textarea($d,5,150)){
					if($this->M_feriado->insertar($f,$tip,$d)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_feriado(){
		if(!empty($this->session_id)){
			if(isset($_POST['idf']) && isset($_POST['fec']) && isset($_POST['tip']) && isset($_POST['des'])){
				$idf=$_POST['idf'];
				$f=$_POST['fec'];
				$tip=$_POST['tip'];
				$d=$_POST['des'];
				if($this->val->entero($idf,0,10) && $this->val->fecha($f) && $this->val->entero($tip,0,10) && $this->val->textarea($d,5,150)){
					if($this->M_feriado->modificar($idf,$f,$tip,$d)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function delete_feriado(){
		if(!empty($this->session_id)){
			if(isset($_POST['idf'])){
				$idf=$_POST['idf'];
				if($this->M_feriado->eliminar($idf)){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}   	   	
   	/*--- End manejo de feriados ---*/
   	/*--- manejo de feriados ---*/
	public function save_tipo(){
		if(!empty($this->session_id)){
			if(isset($_POST['tip']) && isset($_POST['hor']) && isset($_POST['des'])){
				$tip=$_POST['tip'];
				$hor=$_POST['hor'];
				$des=$_POST['des'];
				if($this->val->entero($tip,0,1) && $tip>=0 && $tip<=1 && $hor>=0 && $hor<=99 && $this->val->textarea($des,0,150)){
					if($this->M_tipo_contrato->insertar($tip,$hor,$des)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function select_principal(){
		if(!empty($this->session_id)){
			if(isset($_POST['idtc'])){
				$idtc=$_POST['idtc'];
				if($this->val->entero($idtc,0,10)){
					//verificando si esta seleccionado
					$tipo=$this->M_tipo_contrato->get_row_2n('idtc',$idtc,'principal','1');
					if(empty($tipo)){
						if($this->M_tipo_contrato->reset_principal()){
							if($this->M_tipo_contrato->update_row($idtc,'principal','1')){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "error";
						}
					}else{
						if($this->M_tipo_contrato->reset_principal()){
							echo "ok";
						}else{
							echo "error";
						}	
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_tipo(){
		if(!empty($this->session_id)){
			if(isset($_POST['idtc']) && isset($_POST['tip']) && isset($_POST['hor']) && isset($_POST['des'])){
				$idtc=$_POST['idtc'];
				$tip=$_POST['tip'];
				$hor=$_POST['hor'];
				$des=$_POST['des'];
				if($this->val->entero($idtc,0,1) && $this->val->entero($tip,0,1) && $tip>=0 && $tip<=1 && $hor>=0 && $hor<=99 && $this->val->textarea($des,0,150)){
					if($this->M_tipo_contrato->modificar($idtc,$tip,$hor,$des)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function delete_tipo(){
		if(!empty($this->session_id)){
			if(isset($_POST['idtc'])){
				$idtc=$_POST['idtc'];
				if($this->M_tipo_contrato->eliminar($idtc)){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function update_configuracion(){
		if(!empty($this->session_id)){
			if(isset($_POST['id']) && isset($_POST['valor'])){
				$idconfig=trim($_POST['id']);
				$valor=trim($_POST['valor']);
				if($this->M_configuracion->modificar($idconfig,$valor)){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}  	
   	/*--- End manejo de feriados ---*/
/*------- END MANEJO DE CONFIGURACION -------*/

}
/* End of file capital_humano.php */
/* Location: ./application/controllers/capital_humano.php */