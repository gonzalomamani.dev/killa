<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Val{
	function strSpace($cad,$min,$max){
		$patron="/^[[:alnum:]áÁéÉíÍóÓúÚñÑ(+-°.,:;ª)º ]{".$min.",".$max."}$/";
		if(preg_match($patron,$cad)){
			return true;
		}else{
			return false;
		}
	}
	function strNoSpace($cad,$min,$max){
		$patron="/^[[:alnum:]áÁéÉíÍóÓúÚñÑ(+-°.,:;ª)º]{".$min.",".$max."}$/";
		if(preg_match($patron,$cad)){
			return true;
		}else{
			return false;
		}
	}
	function alfaNumerico($cad,$min,$max){
		$patron="/^[[:alnum:]]{".$min.",".$max."}$/";
		if(preg_match($patron,$cad)){
			return true;
		}else{
			return false;
		}
	}
	function textarea($cad,$min,$max){
		$patron="/^[[:alnum:]áÁéÉíÍóÓúÚñÑ+-.,:;ª)º(° ]{".$min.",".$max."}$/";
		if(preg_match($patron,$cad)){
			return true;
		}else{
			return false;
		}
	}
	function entero($nro,$min,$max){
		$patron="/^[0-9]{".$min.",".$max."}$/";
		if(preg_match($patron, $nro)){
			return true;
		}else{
			return false;
		}
	}
	function decimal($nro,$maxentero,$maxdecimal){
		$patron="/^[0-9]{1,".$maxentero."}+([.]?[0-9]{0,".$maxdecimal."})$/";
		if(preg_match($patron, $nro)){
			return true;
		}else{
			return false;
		}
	}
	function fecha($fecha){
		$patron="/^[0-9]{4}+([-][0-9]{2})+([-][0-9]{2})$/";
		if(preg_match($patron, $fecha)){
			$vf=explode("-", $fecha);
			if($vf[0]>=1600 && $vf[1]>0 && $vf[1]<=12 && $vf[2]>0 && $vf[2]<=31){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	function password($cad,$min,$max){
		$patron="/^[a-z0-9]{".$min.",".$max."}$/";
		if(preg_match($patron,$cad)){
			return true;
		}else{
			return false;
		}
	}
	function email($mail){
		$patron="/^[_a-z.0-9-.]*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/";
		if(preg_match($patron, $mail)){
			return true;
		}else{
			return false;
		}
	}
	function redireccion($privilegio){
			if(!empty($privilegio)){
				if($privilegio[0]->al=="1"){ 
					redirect(base_url().'almacen');					
				}else{
					if($privilegio[0]->pr=="1"){ 
						redirect(base_url().'produccion');					
					}else{
						if($privilegio[0]->mo=="1"){ 
							redirect(base_url().'pedidos_ventas');					
						}else{
							if($privilegio[0]->ca=="1"){ 
								redirect(base_url().'capital_humano');					
							}else{
								if($privilegio[0]->cl=="1"){ 
									redirect(base_url().'cliente_proveedor');					
								}else{
									if($privilegio[0]->ac=="1"){ 
										redirect(base_url().'activos_fijos');					
									}else{
										if($privilegio[0]->ot=="1"){ 
											redirect(base_url().'insumo');					
										}else{
											if($privilegio[0]->co=="1"){ 
												redirect(base_url().'contabilidad');					
											}else{
												if($privilegio[0]->ad=="1"){ 
													redirect(base_url().'administrador');					
												}else{
													echo "<h1>No se le asigno ningun privilegio, consulte el administracion por favor.</h1>";
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}else{
				echo "<h3>Error en la obtención de privilegios</h3>";
			}
	}
}
/* End of file val.php */
/* Location: ./application/libraries/val.php*/