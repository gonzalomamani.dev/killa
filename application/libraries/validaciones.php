<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Validaciones{

	function esParrafo($cad,$lmin,$lmax){//parafo de cadenas y numeros que puede tener espacio
		$patron="/^[[:alnum:]áÁéÉíÍóÓúÚñÑ+-.,:;=* ]{".$lmin.",".$lmax."}$/";
		if(preg_match($patron,$cad)){
			return true;
		}else{
			return false;
		}
	}
	function esCodigo($cad,$lmin,$lmax){//vaida codigo alfanumerico con espacion y solo con algunos simbolos
		$patron="/^[[:alnum:]áÁéÉíÍóÓúÚñÑ+-.,:;]{".$lmin.",".$lmax."}$/";
		if(preg_match($patron,$cad)){
			return true;
		}else{
			return false;
		}
	}
	function esPalabraSinEspacio($cad,$lmin,$lmax){//solo letras sin espacion y sin simbolos
		$patron="/^[[:alnum:]áÁéÉíÍóÓúÚñÑ+]{".$lmin.",".$lmax."}$/";
		if(preg_match($patron,$cad)){
			return true;
		}else{
			return false;
		}
	}
	function esPalabraConEspacio($cad,$lmin,$lmax){//solo letras con espacion y sin simbolos
		$patron="/^[[:alnum:]áÁéÉíÍóÓúÚñÑ+-.,:; ]{".$lmin.",".$lmax."}$/";
		if(preg_match($patron,$cad)){
			return true;
		}else{
			return false;
		}
	}
	function esCedula($ci){//valida numero de carnet
		$patron="/^[0-9]{5,8}$/";
		if(preg_match($patron, $ci)){
			return true;
		}else{
			return false;
		}
	}
	function esNroDecimal($nro,$entero,$decimal){
		$patron="/^[0-9]{1,".$entero."}+([.]?[0-9]{0,".$decimal."})$/";
		if(preg_match($patron, $nro)){
			return true;
		}else{
			return false;
		}
	}
	function esNroEntero($nro,$entero){
		$patron="/^[0-9]{0,".$entero."}$/";
		if(preg_match($patron, $nro)){
			return true;
		}else{
			return false;
		}
	}
	function esEntero($nro,$minimo,$maximo){
		$patron="/^[0-9]{".$minimo.",".$maximo."}$/";
		if(preg_match($patron, $nro)){
			return true;
		}else{
			return false;
		}
	}
	function esFecha($fecha){//2000-01-31
		if($fecha!="" & strpos($fecha, "-")!== false){
			$vf=explode("-", $fecha);
			if(count($vf) == 3){
				if($vf[0]>0 & $vf[1]>0 & $vf[1]<=12 & $vf[2]>0 & $vf[2]<=31){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	function mayuscula($cad){
		return strtoupper($cad); 
	}
	function minuscula($cad){
		return utf8_decode(mb_convert_case($cad,MB_CASE_LOWER,"UTF-8"));
	}
	function may_cadena($cad){//primera letra en mayuscula
		return ucfirst($this->minuscula($cad));
	}
	function may_palabra($cad){//convierte en mayuscula el primer caracter de cada palabra
		return ucwords($cad);
	}
	function convierteMes($mes){
		if($mes>0 & $mes<10){
			$mes="0".$mes;
		}else{
			$mes="".$mes;
		}
		return $mes;
	}
	function dosDigitos($num){
		$num=$num*1;
		if($num>=0 & $num<10){
			$num="0".$num;
		}else{
			$num="".$num;
		}
		return $num;
	}
	function primerDiaMes($anio,$mes){
		$fecha=$anio."-".$mes."-01";
		return $fecha;
	}
	function ultimoDiaMes($anio,$mes){
		$fecha=$anio."-".$mes."-".$this->convierteMes($this->ultimo_dia($mes,$anio));
		return $fecha;
	}
	function ultimo_dia($mes,$anio){
		$final=date("d",(mktime(0,0,0,$mes+1,1,$anio)-1));
		return $final;
	}
	function mesLiteral($mes){
		$m="";
		switch ($mes) {
			case '01':  $m="ENERO"; break;
			case '02':  $m="FEBRERO"; break;
			case '03':  $m="MARZO"; break;
			case '04':  $m="ABRIL"; break;
			case '05':  $m="MAYO"; break;
			case '06':  $m="JUNIO"; break;
			case '07':  $m="JULIO"; break;
			case '08':  $m="AGOSTO"; break;
			case '09':  $m="SEPTIEMBRE"; break;
			case '10':  $m="OCTUBRE"; break;
			case '11':  $m="NOVIEMBRE"; break;
			case '12':  $m="DICIEMBRE"; break;
		}
		return $m;
	}
	function que_dia_es($dia,$mes,$anio){
		$dia=date('w', mktime(0,0,0,$mes,$dia,$anio));
		return $dia;
	}
	function nombre_de_dia($date){
		$dia="";
		switch ($date) {
				case 0: $dia="Domingo"; break;
				case 1: $dia="Lunes"; break;
				case 2: $dia="Martes"; break;
				case 3: $dia="Miercoles"; break;
				case 4: $dia="Jueves"; break;
				case 5: $dia="Viernes"; break;
				case 6: $dia="Sábado"; break;
			}
		return $dia;
	}

	function formato_fecha($fecha,$cond){
		if($fecha!="" && $fecha!=NULL){
			$f=explode("-", $fecha);
			$anio=$f[0];
			$mes=$f[1];
			$dia=$f[2];
			switch ($cond) {
				case 'Y-m-d': $resp=$anio."-".$this->may_cadena($this->mesLiteral($mes))."-".$dia; break;
				case 'd-m-Y': $resp=$dia." - ".$this->may_cadena($this->mesLiteral($mes))." - ".$anio; break;
				case 'Y/m/d': $resp=$anio."/".$this->may_cadena($this->mesLiteral($mes))."/".$dia; break;
				case 'Y,m,d': $resp=$dia.", ".$this->may_cadena($this->mesLiteral($mes)).", ".$anio; break;
				case 'd,m,Y': $resp=$dia." de ".$this->may_cadena($this->mesLiteral($mes))." del ".$anio; break;
				case 'd/m/Y': $resp=$dia." / ".$this->may_cadena($this->mesLiteral($mes))." / ".$anio; break;
				case 'li,d,m,Y': $resp=$this->nombre_de_dia($this->que_dia_es($dia,$mes,$anio)).", ".$dia." de ".$this->may_cadena($this->mesLiteral($mes))." de ".$anio; break;
				default: $resp=$anio."-".$mes."-".$dia; break;
			}
		}else{
			$resp="";
		}
		return $resp;
	}
	function calcula_edad($fecha){
		$edad=0;
		if($fecha!=""){
			$dat=explode("|",$this->diferencia_fecha($fecha,date('Y-m-d')));
			$edad=$dat[0];
		}
	    return $edad;
	}
	function diferencia_fecha($fecha1,$fecha2){
		$f1=new dateTime($fecha1);
		$f2=new dateTime($fecha2);
		$diff=$f1->diff($f2);
		return $diff->y."|".$diff->m."|".$diff->d;
	}
	function conbierte_segundos($d,$h,$m,$s){
		$seg=0;
		if($this->esNroEntero($d,50) & $this->esNroEntero($h,23) & $this->esNroEntero($m,59) & $this->esNroEntero($s,59)){
			$seg=(($d*24)*60*60)+($h*60*60)+($m*60)+$s;
		}
		return $seg;
	}
	function formato_hora($num){
		if($num<=9){
			return '0'.$num;
		}else{
			return $num;
		}
	}
	function segundos_a_hms($segundos){
		$s=0;
		$h=0;
		$m=0;
		if($segundos!=0){
			$s+=$segundos%60;
			$aux=floor($segundos/60);//minutos
			$m+=$aux%60;
			$h+=floor($aux/60);
		}
		return $this->formato_hora($h).':'.$this->formato_hora($m).':'.$this->formato_hora($s);
	}
	function formato_antiguedad($fecha){
		$msj_y="";
		$msj_m="";
		$msj_d="";
		if($fecha!="" & $fecha!=null){
			$dat=explode("|",$this->diferencia_fecha($fecha,date('Y-m-d')));
			if($dat[0]>0){ if($dat[0]==1){ $msj_y=$dat[0]." año";}else{$msj_y=$dat[0]." años";} }
			if($dat[1]>0){ if($dat[1]==1){$msj_m=$dat[1]." mes";}else{$msj_m=$dat[1]." meses";}}
			//if($dat[2]>0 & $dat[1]==0 & $dat[0]==0){ if($dat[2]==1){$msj_d=$dat[2]." dia";}else{$msj_d=$dat[2]." dias";}}
			//if($dat[2]==0 & $dat[1]==0 & $dat[0]==0){$msj_d="0 dias";}
		}
		$sol="";
		if($msj_y==""){
			$sol=$msj_m;
		}else{
			$sol=$msj_y;
		}
		//return 	$msj_y." ".$msj_m." ".$msj_d;	
		return $sol;
	}
	function subir_imagen_miniatura($FILES,$ruta,$pos,$resize,$cod){
		$img=NULL;
		if(count($FILES)>0){
			$rutaMiniatura=$ruta.'miniatura/';
			$key=$FILES['archivo'.$pos];
			$nameImg=$this->subir_imagen($key,$cod,$ruta);
			if($nameImg!="error_type" && $nameImg!="error"){
				if($this->crear_miniatura($resize,$ruta.$nameImg,$rutaMiniatura.$nameImg)){
					//subio y creo miniatura
					$img=$nameImg;
				}else{//no subio 
					if($this->eliminar_archivo($nameImg,$ruta)){}
					$img="error";
				}
			}else{
				$img=$nameImg;
			}
		}
		return $img;
	}
	function cambiar_imagen_miniatura($FILES,$ruta,$pos,$resize,$origen,$id){
		$img=$origen;
		if(count($FILES)>0){
			$rutaMiniatura=$ruta.'miniatura/';
			$key=$FILES['archivo'.$pos];
			$nameImg=$this->subir_imagen($key,$id,$ruta);
			if($nameImg!="error_type" && $nameImg!="error"){
				if($this->crear_miniatura($resize,$ruta.$nameImg,$rutaMiniatura.$nameImg)){
					//subio y creo miniatura
					//eliminado archivo
					if($this->eliminar_archivo($img,$ruta) && $this->eliminar_archivo($img,$rutaMiniatura)){}
					$img=$nameImg;
				}else{//no subio 
					if($this->eliminar_archivo($nameImg,$ruta)){}
				}
			}else{
				$img=$nameImg;
			}
		}
		return $img;
	}

	function subir_imagen($file,$nombre,$ubicacion){
		$key=$file;
		$name="";
		if($this->es_imagen_valida($key)){
			if(!file_exists($ubicacion)){ mkdir($ubicacion);}
			if($key['error'] == UPLOAD_ERR_OK ){
				//Si el archivo se paso correctamente Continuamos
				$atrib=explode(".", $key['name']); 
				if($nombre==NULL){$nombre=rand(0,9999999);}
				$nombre=$nombre."-".rand(0,9999999).".".$atrib[count($atrib)-1];
				$temporal = $key['tmp_name']; //Obtenemos la ruta Original del archivo
				$Destino = $ubicacion.$nombre;	//Creamos una ruta de destino con la variable ruta y el nombre original del archivo	
				if(file_exists($Destino)){ unlink($Destino);}
				move_uploaded_file($temporal, $Destino); //Movemos el archivo temporal a la ruta especificada
				$name=$nombre;
			}
			if($key['error']!=''){
				$name="error";
			}
		}else{
			$name="error_type";
		}
		return $name;
	}
	function es_imagen_valida($file){
		if($file['type'] =='image/jpeg' || $file['type'] =='image/jpg' || $file['type'] =='image/gif' || $file['type'] =='image/png'){
			return true;
		}else{
			return false;
		}
	}

	function son_imagenes_validas($FILES,$nombre){
		if(count($FILES)>0){
			for ($i=0; $i < count($FILES) ; $i++) { 
				$key=$_FILES[$nombre.$i];
				if(!$this->es_imagen_valida($key)){
					return false;
				}
			}
			return true;
		}else{
			return true;
		}
	}
	function crear_miniatura($funcion,$origen,$destino){
		if(file_exists($origen)){
			$min=$funcion;
			$min->inicio($origen);
			$min->resizeImage(140, 140, 'crop');
			if(!$min->saveImage($destino, 100)){
				return true;
			}else{//error en la creacion, eliminar archivo
				return false;
			}
		}else{
			return false;
		}
	}

	function eliminar_archivo($namefile,$dir){
		$namef=str_replace(' ', '', $namefile);
		if($namef!="" & $namef!=NULL){
			$dir_file=$dir.$namefile;
			if(file_exists($dir_file)){
				if(unlink($dir_file)){
					return true;
				}else{
					return false;
				}
			}else{
				return true;// no exitste archivo
			}
		}else{
			return true;// no exite imagen
		}
		
	}

	function eliminar_imagen($file,$ruta){
		if($file!="" && $file!=NULL){
			$rutaMiniatura=$ruta.'miniatura/';
			if($this->eliminar_archivo($file,$ruta) && $this->eliminar_archivo($file,$rutaMiniatura)){
				return true;
			}else{
				return false;
			}
		}else{
			return true;
		}
	}
	
	function eliminar_directorio($carpeta,$ruta_empleados){
		$dir_final=$ruta_empleados."/".$carpeta;
		//echo "DIR: ".$dir_final."            ";
		if(is_dir($dir_final)){ 
				if($opendir=opendir($dir_final)){
					//echo "directorio: ".$dir_final;
			 		 while(($file=readdir($opendir))!==false) { 
			            //esta línea la utilizaríamos si queremos listar todo lo que hay en el directorio 
			            //mostraría tanto archivos como directorios 
			            //echo "<br>Nombre de archivo: $file : Es un: " . filetype($ruta . $file);
			            if($file!="." && $file!=".."){
			            	if(is_dir($dir_final."/".$file)){
				            	//echo "   <br>!!!directorio: ".$dir_final."/".$file."<br>";
				            	$this->eliminar_directorio($file,$dir_final);
				            }else{
				            	unlink($dir_final."/".$file);
								//echo "<br>Archivo: ".$dir_final."/".$file."<br>";
				            }
			            }
			         } 
			      	closedir($opendir); 
			      	rmdir($dir_final);
			      	return true;	
				}else{
					return false;
				}
		}else{
			return true;
		}
		
	}

/*--- MANEJO DE CAPITAL HUMANO ---*/

	function subir_excel_2003($file,$nombre,$ubicacion){
		if($this->es_excel_2003($file)){
			if(!file_exists($ubicacion)){ mkdir($ubicacion);}
			if($file['error'] == UPLOAD_ERR_OK ){
				$atrib=explode(".", $file['name']);
				$nombreArch=$nombre.".".$atrib[count($atrib)-1];
				$rutaArchivo=$ubicacion."/".$nombreArch;
				if(file_exists($rutaArchivo)){ unlink($rutaArchivo);}
				if(move_uploaded_file($file['tmp_name'],$rutaArchivo)){
					return $nombreArch;
				}else{
					return "error";
				}
			}else{
				return "error";
			}
		}else{
			return "error_type";
		}
	}
	function es_excel_2003($file){
		if($file['type'] =='application/vnd.ms-excel'){
			return true;
		}else{
			return false;
		}
	}
	/*evaluando archivo biometrico*/
	function datos_biometrico_empleado($cod,$ci,$anio,$mes,$file){
		$resp="";//resultado de la horas de cada dia
		$textoFinal="";//resultado final
		$sep="|";
		$sep2="{#}";
		$filaEAnt="";//para la comparacion entre la hora actual y anterior
		$contFecha=0;//contador de marcados en un dia, la cantidad almacenada debe ser un numero par
		foreach($file as $row) : 
			$fecha_hora=explode(" ", $row[4]);//dividimos en fecha y hora, 01/12/2000 08:00
			$filaE="";
			if($row[1]==$cod){
				//validando si pertenece al mes y anio
				$fec=explode("/", $fecha_hora[0]);
				if($fec[1]==($mes*1) && $fec[2]==$anio){//perteneces entonces lo guardamos
					$fecha=$fec[2].'-'.$this->dosDigitos($fec[1]).'-'.$this->dosDigitos($fec[0]);
					$filaE=$row[1].$sep.$row[2].$sep.$row[3].$sep.$fecha.$sep.$fecha_hora[1];
				}else{//no pertenece entonces no lo guardamos
					$filaE="";
				}	
			}
			if($filaE!=""){
				if($filaEAnt==""){//no existe fecha anterior
					//verificamos si es un marcado de entrada o salida o nulo (E/S/N) y calculamos su hora redondeada
					$contFecha++;
					$filaE=$this->verifica_ES_y_redondeoHora($filaE,"",$contFecha,$sep);
					$filaEAnt=$filaE;
					$resp=$resp.$filaE.$sep2;//guardardando Horas
				}else{
					$hora1=explode("|", $filaEAnt);
					$hora2=explode("|", $filaE);
					if($hora1[3]==$hora2[3]){//comparando si son del mismo dia
						if(!$this->hora_iguales($hora1[4],$hora2[4])){//si no son horas iguales se guarda.. si son iguales, caso contrario no es guardado
							$contFecha++;
							$filaE=$this->verifica_ES_y_redondeoHora($filaE,$filaEAnt,$contFecha,$sep);
							$resp=$resp.$filaE.$sep2;//guardardando datos del mes
							$filaEAnt=$filaE;
						}
					}else{//no es el mismos dia
						//verificando que el nro de marcados este correcto
						if($contFecha%2==0){// datos del mes valido
							$textoFinal=$textoFinal.$resp;
						}else{// datos del mes invalido, el usuario se olvido marcar
							$resp=str_replace("N", "Fail", $resp);
							$resp=str_replace("E", "Fail", $resp);
							$resp=str_replace("S", "Fail", $resp);
							$textoFinal=$textoFinal.$resp;
						}
						$contFecha=1;
						$filaE=$this->verifica_ES_y_redondeoHora($filaE,"",$contFecha,$sep);
						$filaEAnt=$filaE;
						$resp=$filaE.$sep2;
					}
				}
			}
		endforeach;
		if($resp!=""){
			if($contFecha%2==0){// datos del mes valido
				$textoFinal=$textoFinal.$resp;
			}else{// datos del mes invalido, el usuario se olvido marcar
				$resp=str_replace("N", "Fail", $resp);
				$resp=str_replace("E", "Fail", $resp);
				$resp=str_replace("S", "Fail", $resp);
				$textoFinal=$textoFinal.$resp;
			}
		}
		return $textoFinal;
	}
	function verifica_ES_y_redondeoHora($filaDAct,$filaDAnt,$cont,$sep){
		//calculo de Estado E/S, redondeo de hora
		if($filaDAnt!=""){//caso de existir fecha anterior
			$dAct=explode($sep, $filaDAct);
			$dAnt=explode($sep, $filaDAnt);
			$resultado="";
			$estado="";
			if($this->esHoraValida($dAct[4],$dAnt[4])){//hora valida
				if($cont%2!=0){
					$estado="E";
				}else{
					$estado="S";
				}
			}else{
				$estado="N";
			}
		}else{//caso de no existir fecha anterior
			$estado="E";
			$dAct=explode("|", $filaDAct);
		}
		$resultado=$dAct[0].$sep.$dAct[1].$sep.$dAct[2].$sep.$dAct[3].$sep.$dAct[4].$sep.$this->redondeo_hora($dAct[4],$estado).$sep.$estado;
		return $resultado;
	}

	function detalle_salario_empleado($horas,$emp_cod,$ci,$emp_tc,$emp_hora_trabajo,$feriados){

	}
	function calcula_horas_trabajo($url_file,$tc,$hora_trabajo,$feriados){
		$json='';//almacen la cadena json
		$hraAnt="";//almacena la hora anterior
		$fechaAnt="";//almacen la fecha anterior
		$horaDia=0;//almacen las horas trabajadas por el dia
		if(file_exists($url_file)){
			$file=fopen($url_file, "r");
			while($line=fgetcsv($file,0,"|")){
				if($hraAnt==""){//no existe datos anterior
					$hraAnt=$line[5];
					$fechaAnt=$line[3];

				}else{//existe hora anterior
					if($line[6]!="Fail" & $line[6]!="N"){//si los datos de las horas son validas
						$dia0=explode("/", $fechaAnt);
						$dia1=explode("/", $line[3]);
						if($dia0[0]==$dia1[0]){//seguimos en el mismo dia
							if($line[6]=="S"){//es hora de salida, restamos con la hora de entrada
								$hra0=new DateTime($hraAnt);//hora de entrada
								$hra1=new DateTime($line[5]);//hora de salida
								$diff=new DateTime();//diferencia entre ambas horas

								$diff=$hra0->diff($hra1);
								$horas=$diff->h+($diff->i/60)+0;//calculamos el tiempo trabajado
								$horaDia=$horaDia+$horas;//sumamos al tiempo trabajado en el dia
							}else{//la hora es de Entrada E
								$hraAnt=$line[5];//guardando hora de entrada
								$fechaAnt=$line[3];//guardando fecha de entrada
							}
						}else{//es otro dia
							$vf=explode("/", $fechaAnt);
							$hrad=$this->hora_descuento($tc,$hora_trabajo,$horaDia,$vf[0],$vf[1],$vf[2],$feriados);//calculando las horas adeudadas
							$json=$this->concatena_json($json,$fechaAnt,$horaDia,$hrad,'add');//guardamos en jornal de trabajo
							$horaDia=0;
							$hraAnt=$line[5];//guardando hora de entrada
							$fechaAnt=$line[3];//guardando fecha de entrada
						}// end if($dia0[0]==$dia1[0])
					}//end if($line[6]!="Fail" & $line[6]!="N")
				}//end else
			}//end while
			if($horaDia!=0 & $hraAnt!="" & $fechaAnt!=""){
				$vf=explode("/", $fechaAnt);
				$hrad=$this->hora_descuento($tc,$hora_trabajo,$horaDia,$vf[0],$vf[1],$vf[2],$feriados);//calculando las horas adeudadas
				$json=$this->concatena_json($json,$fechaAnt,$horaDia,$hrad,'add');//guardamos en jornal de trabajo
			}
			$json=$this->concatena_json($json,"","CLOSED","","final");
			//echo "Json fuinal guardado. ".$json." .<br>";
		}//end if(file_exists($url_file))
		else{
			$json="No existe Archivo";
			//mkdir($url_file);
		}
		return $json;
	}
	function total_salarios($horarios,$empleados){

	}

	/*end evaluando archivo biometrico*/


	function fecha_hora_bio($fecha_hora){
		$res="";
		$fecha_h=explode(" ", $fecha_hora);//dividimos en fecha y hora, 01/12/2000 08:00
		if(count($fecha_h)==2){
			$aux=explode("/",$fecha_h[0]);
			$aux2=explode(":",$fecha_h[1]);
			if(count($aux)==3 && count($aux2)>=2){
				$fecha=$this->dosDigitos($aux[2])."-".$this->dosDigitos($aux[1])."-".$this->dosDigitos($aux[0]);
				$hora=$this->dosDigitos($aux2[0]).":".$this->dosDigitos($aux2[1]).":00";
				$res=$fecha."|".$hora;
			}
		}
		return $res;
	}
	function buscar_fecha_bio($hora_biometrico,$ide,$fecha_hora){
		$res="";
		$v=explode("|", $fecha_hora);
		if(count($v)==2){
			//echo "comparando: ".count($hora_biometrico);
			for($k=0; $k < count($hora_biometrico); $k++){ $hb=$hora_biometrico[$k];
				//echo "comparando: ".$hb->ide."=".$ide." ".$hb->fecha."=".$v[0]." ".$hb->hora."=".$v[1]." </br>";
				if($hb->ide==$ide && $hb->fecha==$v[0]  && $hb->hora==$v[1]){
					$res="exist";
					return $res;
				}
			}
		}
		return $res;
	}

/*--- END MANEJO DE CAPITAL HUMANO ---*/

	function crearArchivos($cod,$ide,$mes,$anio,$url,$file){
		$filaEAnt="";//para la comparacion entre la hora actual y anterior
		$textoFinal="";
		$sep="|";//separador de csv
		$contFecha=0;//contador de marcados en un dia, la cantidad almacenada debe ser un numero par
		$dMes="";//guarda solo los datos del mes
		$estado="";//guarda el estado del horario, Entrada o Salida (E o S)
		//echo " <br> ".$cod." ".$ide." ".$mes." ".$anio." ".$url;
		foreach($file as $row) : 
			$fecha_hora=explode(" ", $row[4]);//dividimos en fecha y hora, 01/12/2000 08:00
			$filaE="";
				if($row[1]==$cod){
					//echo " Row1".$row[1];
					//validando si pertenece al mes y anio
					$fec=explode("/", $fecha_hora[0]);
					if($fec[1]==$mes & $fec[2]==$anio){//perteneces entonces lo guardamos
						$filaE=$row[1].$sep.$row[2].$sep.$row[3].$sep.$fecha_hora[0].$sep.$fecha_hora[1];
					}else{//no pertenece entonces no lo guardamos
						$filaE="";
					}
					
				}
		//	}
			if($filaE!=""){
				if($filaEAnt==""){//no existe fecha anterior
					//verificamos si es un marcado de entrada o salida o nulo (E/S/N) y calculamos su hora redondeada
					$contFecha++;
					$filaE=$this->verificaES_RedondeHora($filaE,"",$contFecha,$sep);
					$filaEAnt=$filaE;
					$dMes=$dMes.$filaE."\n";//guardardando Horas
				}else{
					$hora1=explode("|", $filaEAnt);
					$hora2=explode("|", $filaE);
					
					if($hora1[3]==$hora2[3]){//comparando si son del mismo dia
						if(!$this->hora_iguales($hora1[4],$hora2[4])){//si no son horas iguales se guarda.. si son iguales, caso contrario no es guardado
							$contFecha++;
							$filaE=$this->verificaES_RedondeHora($filaE,$filaEAnt,$contFecha,$sep);
							$dMes=$dMes.$filaE."\n";//guardardando datos del mes
							$filaEAnt=$filaE;
						}
					}else{//no es el mismos dia
						//verificando que el nro de marcados este correcto
						if($contFecha%2==0){// datos del mes valido
							$textoFinal=$textoFinal.$dMes;
						}else{// datos del mes invalido, el usuario se olvido marcar
							$dMes=str_replace("N", "Fail", $dMes);
							$dMes=str_replace("E", "Fail", $dMes);
							$dMes=str_replace("S", "Fail", $dMes);
							
							$textoFinal=$textoFinal.$dMes;
						}
						$contFecha=1;
						$filaE=$this->verificaES_RedondeHora($filaE,"",$contFecha,$sep);
						$filaEAnt=$filaE;
						$dMes=$filaE."\n";
					}
				}
				
			}
		endforeach;
		if($dMes!=""){
			if($contFecha%2==0){// datos del mes valido
				$textoFinal=$textoFinal.$dMes;
			}else{// datos del mes invalido, el usuario se olvido marcar
				$dMes=str_replace("N", "Fail", $dMes);
				$dMes=str_replace("E", "Fail", $dMes);
				$dMes=str_replace("S", "Fail", $dMes);
				
				$textoFinal=$textoFinal.$dMes;
			}
		}
		//echo "TXT. ".$textoFinal;
		if($textoFinal!=""){//si existe texo para escribir
			$destino=$url.$ide."/";
			if(!file_exists($destino)){
				mkdir($destino); 
			}
			//creamos archivo del empledo en el mes actual
			$archivo=$destino.$ide."-".$mes."-".$anio.".csv";
			//$contenido="lo que quieras escribir en el archivo";
			if(file_exists($archivo)){ unlink($archivo);}//eliminamos si existe el archivo
			$fdestino=fopen($archivo,"x");
			fwrite($fdestino, $textoFinal); 
			fclose($fdestino);
		}
		return true;
	}

	function crearArchivosDeJSON($cod,$ci,$nombreEmpleado,$urlFile,$json){
		$filaEAnt="";//para la comparacion entre la hora actual y anterior
		$textoFinal="";
		$sep="|";//separador de csv
		$contFecha=0;//contador de marcados en un dia, la cantidad almacenada debe ser un numero par
		$dMes="";//guarda solo los datos del mes
		$estado="";//guarda el estado del horario, Entrada o Salida (E o S)
		$array = json_decode($json);
		foreach($array as $row) : 
			if($this->hora_valida($row->hora)){// si es hora valida y diferente que 0:00
				$filaE=$cod.$sep.$ci.$sep.$nombreEmpleado.$sep.$row->fecha.$sep.$row->hora;
				if($filaEAnt==""){//no existe fecha anterior
					//verificamos si es un marcado de entrada o salida o nulo (E/S/N) y calculamos su hora redondeada
					$contFecha++;
					$filaE=$this->verificaES_RedondeHora($filaE,"",$contFecha,$sep);
					$filaEAnt=$filaE;
					$dMes=$dMes.$filaE."\n";//guardardando Horas
				}else{
						$hora1=explode("|", $filaEAnt);
						$hora2=explode("|", $filaE);
						if($hora1[3]==$hora2[3]){//comparando si son del mismo dia
							if(!$this->hora_iguales($hora1[4],$hora2[4])){//si no son horas iguales se guarda.. si son iguales, caso contrario no es guardado
								$contFecha++;
								$filaE=$this->verificaES_RedondeHora($filaE,$filaEAnt,$contFecha,$sep);
								$dMes=$dMes.$filaE."\n";//guardardando datos del mes
								$filaEAnt=$filaE;
							}
						}else{//no es el mismos dia
							//verificando que el nro de marcados este correcto debe ser un numero par E/S
							if($contFecha%2==0){// datos del mes valido
								$textoFinal=$textoFinal.$dMes;
							}else{// datos del mes invalido, el usuario se olvido marcar
								$dMes=str_replace("N", "Fail", $dMes);
								$dMes=str_replace("E", "Fail", $dMes);
								$dMes=str_replace("S", "Fail", $dMes);
								
								$textoFinal=$textoFinal.$dMes;
							}
							$contFecha=1;
							$filaE=$this->verificaES_RedondeHora($filaE,"",$contFecha,$sep);
							$filaEAnt=$filaE;
							$dMes=$filaE."\n";
							//$listar['asd']=$this->M_hora_biometrico->get();
						}
				}
			}// end if hora_valida($row->hora)
		endforeach;
		if($dMes!=""){
			if($contFecha%2==0){// datos del mes valido
				$textoFinal=$textoFinal.$dMes;
			}else{// datos del mes invalido, el usuario se olvido marcar
				$dMes=str_replace("N", "Fail", $dMes);
				$dMes=str_replace("E", "Fail", $dMes);
				$dMes=str_replace("S", "Fail", $dMes);
				$textoFinal=$textoFinal.$dMes;
			}
		}
		if($textoFinal!=""){//si existe texo para escribir
			if(file_exists($urlFile)){ unlink($urlFile);}//eliminamos si existe el archivo
			$fdestino=fopen($urlFile,"x");
			fwrite($fdestino, $textoFinal); 
			fclose($fdestino);
		}
		return true;
	}
	function hora_iguales($hora1,$hora2){//comprar si dos horas son iguales segun su rango de error de 2min
		$hra1=new DateTime($hora1);
		$hra2=new DateTime($hora2);
		$diff=new DateTime();
		$diff=$hra1->diff($hra2);
		$minutos=(($diff->h)*60)+$diff->i+(($diff->s)/60);
		if($minutos>5){ return false; }else{ return true; }
	}

	/*function verificaES_RedondeHora($filaDAct,$filaDAnt,$cont,$sep){
		//calculo de Estado E/S, redondeo de hora
		if($filaDAnt!=""){//caso de existir fecha anterior
			$dAct=explode("|", $filaDAct);
			$dAnt=explode("|", $filaDAnt);
			$resultado="";
			$estado="";
			if($this->esHoraValida($dAct[4],$dAnt[4])){//hora valida
				if($cont%2!=0){
					$estado="E";
				}else{
					$estado="S";
				}
			}else{
				$estado="N";
			}
		}else{//caso de no existir fecha anterior
			$estado="E";
			$dAct=explode("|", $filaDAct);
		}
		
		$resultado=$dAct[0].$sep.$dAct[1].$sep.$dAct[2].$sep.$dAct[3].$sep.$dAct[4].$sep.$this->redondeo_hora($dAct[4],$estado).$sep.$estado;
		return $resultado;
	}*/

	function esHoraValida($hora_act,$hora_ant){//verificando que la hora anterior sea menor a la actual
		$h1=new DateTime($hora_ant);
		$h2=new DateTime($hora_act);
		if($h2>$h1){
			return true;
		}else{
			return false;
		}
	}
	function hora_valida($hora){
		if(strpos($hora, ':') !== false){
			$vh=explode(":", $hora);
			if(count($vh)>1){
				if($vh[0]>0 && $vh[0]<=23 && $vh[1]>=0 && $vh[1]<=59){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	
	function redondeo_hora($hora,$tipo){//redondeando la hora
		$horaFinal="00:00";
		switch ($tipo) {
			case 'E':
				$hra=explode(":", $hora);
				if($hra[1]>=0 & $hra[1]<=10){ $horaFinal=$hra[0].":00";}
				if($hra[1]>=11 & $hra[1]<=40){ $horaFinal=$hra[0].":30";}
				if($hra[1]>=41 & $hra[1]<=59){ $horaFinal=($hra[0]+1).":00";}
				break;
			
			case 'S':
				$hra=explode(":", $hora);
				if($hra[1]>=0 & $hra[1]<=29){ $horaFinal=$hra[0].":00";}
				if($hra[1]>=30 & $hra[1]<=59){ $horaFinal=$hra[0].":30";}
				break;
			case 'N':
				# code...
				break;
		}
		return $horaFinal;
	}
	

/*	function calcula_horas_trabajo($url_file,$tc,$hora_trabajo,$feriados){
		$json='';//almacen la cadena json
		$hraAnt="";//almacena la hora anterior
		$fechaAnt="";//almacen la fecha anterior
		$horaDia=0;//almacen las horas trabajadas por el dia
		if(file_exists($url_file)){
			$file=fopen($url_file, "r");
			while($line=fgetcsv($file,0,"|")){
				if($hraAnt==""){//no existe datos anterior
					$hraAnt=$line[5];
					$fechaAnt=$line[3];

				}else{//existe hora anterior
					if($line[6]!="Fail" & $line[6]!="N"){//si los datos de las horas son validas
						$dia0=explode("/", $fechaAnt);
						$dia1=explode("/", $line[3]);
						if($dia0[0]==$dia1[0]){//seguimos en el mismo dia
							if($line[6]=="S"){//es hora de salida, restamos con la hora de entrada
								$hra0=new DateTime($hraAnt);//hora de entrada
								$hra1=new DateTime($line[5]);//hora de salida
								$diff=new DateTime();//diferencia entre ambas horas

								$diff=$hra0->diff($hra1);
								$horas=$diff->h+($diff->i/60)+0;//calculamos el tiempo trabajado
								$horaDia=$horaDia+$horas;//sumamos al tiempo trabajado en el dia
							}else{//la hora es de Entrada E
								$hraAnt=$line[5];//guardando hora de entrada
								$fechaAnt=$line[3];//guardando fecha de entrada
							}
						}else{//es otro dia
							$vf=explode("/", $fechaAnt);
							$hrad=$this->hora_descuento($tc,$hora_trabajo,$horaDia,$vf[0],$vf[1],$vf[2],$feriados);//calculando las horas adeudadas
							$json=$this->concatena_json($json,$fechaAnt,$horaDia,$hrad,'add');//guardamos en jornal de trabajo
							$horaDia=0;
							$hraAnt=$line[5];//guardando hora de entrada
							$fechaAnt=$line[3];//guardando fecha de entrada
						}// end if($dia0[0]==$dia1[0])
					}//end if($line[6]!="Fail" & $line[6]!="N")
				}//end else
			}//end while
			if($horaDia!=0 & $hraAnt!="" & $fechaAnt!=""){
				$vf=explode("/", $fechaAnt);
				$hrad=$this->hora_descuento($tc,$hora_trabajo,$horaDia,$vf[0],$vf[1],$vf[2],$feriados);//calculando las horas adeudadas
				$json=$this->concatena_json($json,$fechaAnt,$horaDia,$hrad,'add');//guardamos en jornal de trabajo
			}
			$json=$this->concatena_json($json,"","CLOSED","","final");
			//echo "Json fuinal guardado. ".$json." .<br>";
		}//end if(file_exists($url_file))
		else{
			$json="No existe Archivo";
			//mkdir($url_file);
		}
		return $json;
	}*/

	function hora_descuento($tc,$hora_trabajo,$hora_trabajadas,$dia,$mes,$anio,$feriados){
		//calcula las horas que debe por dia el valor numerico es negativo si debe horas, si es positivo entonces son horas extra
		$des=0;
		//hallamos si es dia domingo 
		$diaN=$this->que_dia_es($dia,$mes,$anio);
		
		if($diaN==0){//si es domingo
			$des=$hora_trabajadas-0;// domingos son horas extra
		}else{
			if(!$this->exite_fecha($feriados,$anio."-".$mes."-".$dia)){//en caso de no ser feriado
				if($diaN==6){//si es sabado
					switch ($tc){
						case 0: $des=$hora_trabajadas-($hora_trabajo/2); break;// tiempo completo
						case 1: $des=$hora_trabajadas-$hora_trabajo; break;// medio tiempo
					}
				}else{// dia normal lunes a viernes;
					$des=$hora_trabajadas-$hora_trabajo;
				}
			}else{
				$des=$hora_trabajadas-0;//en caso de ser feriado la se cuenta con horas extra
			}
		}
		return $des;
	}

	function exite_fecha($array_fechas,$fecha){// busca si existe la fecha en el array asociativo: 2000-12-31
		if (count($array_fechas)>0) {
			for ($i=0; $i < count($array_fechas) ; $i++) { 
				$resp=$array_fechas[$i];
				if($fecha==$resp->fecha){
					return true;
				}
			}
			return false;
		}else{
			return false;
		}
	}

	

	function es_habil_domingo_feriado($dia,$mes,$anio,$feriados){//calcula si es dia habil feriado o domingo
		$resp="";
		if(!$this->exite_fecha($feriados,$anio."-".$mes."-".$dia)){//en caso de no ser feriado
			$diaN=$this->que_dia_es($dia,$mes,$anio);
				if($diaN==0){//si es domingo
					$resp="D";
				}else{// dia normal lunes a viernes;
					if($diaN==6){
						$resp="S";
					}else{
						$resp="H";
					}
				}
		}else{
				$resp="F";
		}
		return $resp;
	}

	function dias_habiles_domingo_feriado_rango($fe1,$fe2,$feriados){
		$fecha1=strtotime($fe1);
		$fecha2=strtotime($fe2);
		$cont_hab=0;
		$cont_dom=0;
		$cont_fer=0;
		while($fecha1<=$fecha2){
			$vf=explode("-", date('Y-m-d',$fecha1));
			$dia=$this->es_habil_domingo_feriado($vf[2],$vf[1],$vf[0],$feriados);
			switch ($dia) {
				case 'H': $cont_hab++; break;
				case 'D': $cont_dom++; break;
				case 'F': $cont_fer++; break;
			}
			$fecha1=strtotime('+1 day',$fecha1);
		}
		return $cont_hab."|".$cont_dom."|".$cont_fer;
	}
	function dias_habiles($feriados,$anio,$mes){
		$diaA=0;
		$max=$this->ultimo_dia($mes,$anio);
		for ($i=1; $i <=$max ; $i++) { 
			$dia=$this->que_dia_es($i,$mes,$anio);
			$fecha=$anio."-".$mes."-".$this->convierteMes($i);
			if($dia!=0 & !$this->exite_fecha($feriados,$fecha)){
				$diaA++;
			}
		}
		return $diaA;
	}

	function concatena_json($json,$fecha,$hora,$hora_debe,$action){
		//echo "Guardando: ".$json." ".$fecha." ".$hora." ".$action."<br>";
		$result="";
		if($json==''){
			if($hora!="CLOSED"){
				$result='[{"fecha":"'.$fecha.'","hora":"'.$hora.'","descuento":"'.$hora_debe.'"}';
			}
		}else{
			switch($action){
				case 'add':	$result=$json.',{"fecha":"'.$fecha.'","hora":"'.$hora.'","descuento":"'.$hora_debe.'"}'; break;
				case 'final':	$result=$json.']'; break;
			}
		}
		return $result;
	}
	function completa_fechas($tc,$hora_trabajo,$feriados,$json_horas,$mes,$anio){//calculamos el salario del mes, sueldo por dia,por mes, total descuento
		//hallamos ultimo dia del mes
		$ultimoDia=$this->ultimo_dia($mes,$anio);
		$json_sol='';
		$hraDes=0;//almacen las horas de descuento
		$hraTra=0;//almacen las horas de trabajo
		for ($i=1; $i <=$ultimoDia ; $i++) {
			$sw=0;
			$fecha=$this->convierteMes($i)."/".$mes."/".$anio;
			//recorremos el json de horarios y buscamos si existe la fecha
			//echo "<H4>JSON: ".$json_horas."</H4>";
			if($json_horas!=""){
				$array = json_decode($json_horas);
				foreach($array as $obj){
			        if($obj->fecha==$fecha){//existe la fecha
			        	$sw=1;
			        	$json_sol=$this->concatena_json($json_sol,$obj->fecha,$obj->hora,$obj->descuento,"add");
			        	$hraDes+=$obj->descuento;
			        	$hraTra+=$obj->hora;
			        	break;
			        }
				}
			}
			
			if($sw==0){//la fecha no existe
				$des=$this->hora_descuento($tc,$hora_trabajo,0,$this->convierteMes($i),$mes,$anio,$feriados);
				$json_sol=$this->concatena_json($json_sol,$fecha,"0",$des,"add");
				$hraDes+=$des;
			}
		}
		$json_sol=$this->concatena_json($json_sol,"","CLOSED","","final");
		return $json_sol."|".$hraTra."|".$hraDes;
	}
	

	function calcula_salario($salario,$hras_trabajo,$hra_deuda,$diasHabiles){
		//calculamos los dias habiiles
		$porDia=number_format($salario/$diasHabiles,2,'.','');
		$porHora=number_format($porDia/$hras_trabajo,2,'.','');
		$descuento=number_format($hra_deuda*$porHora,2,'.','');;
		$liquido=number_format($salario+$descuento,2,'.','');
		return $porDia."|".$porHora."|".$descuento."|".$liquido;
	}
	
	/*function copia_file_a_json($dir){//copia el archivo de emeplado a un json
		$json="";
		if(is_file($dir)){
			if(($fichero = fopen($dir, "r")) !== FALSE) {
				while (($datos = fgetcsv($fichero, 0, "|")) !== FALSE) {
					//echo "<h1>".$datos[0]."</h1>";
					$row='"fecha":"'.$datos[3].'","hora":"'.$datos[4].'","redondo":"'.$datos[5].'","tipo":"'.$datos[6].'"';
					$json=$this->concatena_json_row($json,$row,"add");
				}
				$json=$this->concatena_json_row($json,"CLOSED","final");
			}
		}
		return $json;
	}*/

	function concatena_json_row($json,$row,$action){
		$result="";
		if($json==''){
			if($row!="CLOSED"){
				$result='[{'.$row.'}';
			}
		}else{
			switch ($action) {
				case 'add':	$result=$json.',{'.$row.'}'; break;
				case 'final':	$result=$json.']'; break;
			}
		}
		return $result;
	}

	function es_E_S($tipo){
		$resp="";
		switch ($tipo) {
			case 'E':$resp="Entrada"; break;
			case 'S':$resp="Salida"; break;
			case 'N':$resp="No Válido"; break;
			case 'Fail':$resp="Error"; break;
		}
		return $resp;
	}

	function busca_error_file($dir){
		$resp="";
		if(file_exists($dir)){
			$file=fopen($dir, "r");
			while (($row = fgetcsv($file, 0, "|")) !== FALSE) {
				if($row[6]=="Fail" || $row[6]=="N"){
					$resp="E";
					break;
				}else{
					$resp="T";
				}
			}
			fclose($file);
			
		}else{
			$resp="NE";
		}
		return $resp;
	}


	/*Copias a JSON*/
	function copia_json_material($materiales){
		$json="";
		for($i=0; $i<count($materiales); $i++) {$m=$materiales[$i];
		  //$row='"idi":"'.$m->idi.'","idusu":"'.$m->idusu.'","ida":"'.$m->ida.'","cod":"'.$m->cod.'","nombre_i":"'.$m->nombre_i.'","c_u":"'.$m->c_u.'","cantidad":"'.$m->cantidad.'","descripcion_i":"'.$m->descripcion_i.'","fecha_creacion":"'.$m->fecha_creacion.'","fotografia":"'.$m->fotografia.'","idu":"'.$m->idu.'","medida":"'.$m->medida.'","abreviatura":"'.$m->abreviatura.'","idg":"'.$m->idg.'","nombre_g":"'.$m->nombre_g.'","descripcion_g":"'.$m->descripcion_g.'","idco":"'.$m->idco.'","nombre_c":"'.$m->nombre_c.'","codigo_c":"'.$m->codigo_c.'"';
			$row='"idi":"'.$m->idm.'","ida":"'.$m->ida.'","cod":"'.$m->codigo.'","nombre_i":"'.$m->nombre.'","c_u":"'.$m->costo_unitario.'","cantidad":"'.$m->cantidad.'","descripcion_i":"'.$m->observaciones.'","fotografia":"'.$m->fotografia.'","idu":"'.$m->idu.'","medida":"'.$m->medida.'","abreviatura":"'.$m->abreviatura.'","idg":"'.$m->idg.'","nombre_g":"'.$m->nombre_g.'","idco":"'.$m->idco.'","nombre_c":"'.$m->nombre_c.'","codigo_c":"'.$m->codigo_c.'"';
			$json=$this->concatena_json_row($json,$row,"add");
		}
		$json=$this->concatena_json_row($json,"CLOSED","final");
		return $json;
	}










	/*momnetaneo*/
	function crearArchivos_de_csv($cod,$ide,$mes,$anio,$url,$file_csv){
		$filaEAnt="";//para la comparacion entre la hora actual y anterior
		$textoFinal="";
		$sep="|";//separador de csv
		$contFecha=0;//contador de marcados en un dia, la cantidad almacenada debe ser un numero par
		$dMes="";//guarda solo los datos del mes
		$estado="";//guarda el estado del horario, Entrada o Salida (E o S)
		$file=fopen($file_csv, "r");

		while (($row = fgetcsv($file, 0, "|")) !== FALSE) {
			$fecha_hora=explode(" ", $row[3]);//dividimos en fecha y hora, 01/12/2000 08:00
			$filaE="";
			
				if($row[0]==$cod){

					//validando si pertenece al mes y anio
					$fec=explode("/", $fecha_hora[0]);
					if($fec[1]==$mes & $fec[2]==$anio){//perteneces entonces lo guardamos
						$filaE=$row[0].$sep.$row[1].$sep.$row[2].$sep.$fecha_hora[0].$sep.$fecha_hora[1];
					}else{//no pertenece entonces no lo guardamos
						$filaE="";
					}
					
				}

		//	}
			if($filaE!=""){
				if($filaEAnt==""){//no existe fecha anterior
					//verificamos si es un marcado de entrada o salida o nulo (E/S/N) y calculamos su hora redondeada
					$contFecha++;
					$filaE=$this->verificaES_RedondeHora($filaE,"",$contFecha,$sep);
					$filaEAnt=$filaE;
					$dMes=$dMes.$filaE."\n";//guardardando Horas
				}else{
					
					$hora1=explode($sep, $filaEAnt);
					$hora2=explode($sep, $filaE);
					
					if($hora1[3]==$hora2[3]){//comparando si son del mismo dia
						if(!$this->hora_iguales($hora1[4],$hora2[4])){//si no son horas iguales se guarda.. si son iguales, caso contrario no es guardado
							$contFecha++;
							$filaE=$this->verificaES_RedondeHora($filaE,$filaEAnt,$contFecha,$sep);
							$dMes=$dMes.$filaE."\n";//guardardando datos del mes
							$filaEAnt=$filaE;
						}
					}else{//no es el mismos dia
						//verificando que el nro de marcados este correcto
						if($contFecha%2==0){// datos del mes valido
							$textoFinal=$textoFinal.$dMes;
						}else{// datos del mes invalido, el usuario se olvido marcar
							$dMes=str_replace("N", "Fail", $dMes);
							$dMes=str_replace("E", "Fail", $dMes);
							$dMes=str_replace("S", "Fail", $dMes);
							
							$textoFinal=$textoFinal.$dMes;
						}
						$contFecha=1;
						$filaE=$this->verificaES_RedondeHora($filaE,"",$contFecha,$sep);
						$filaEAnt=$filaE;
						$dMes=$filaE."\n";
					}
				}
				
			}
		}//end while
		fclose($file);
		if($dMes!=""){
			$textoFinal=$textoFinal.$dMes;
		}
		if($textoFinal!=""){//si existe texo para escribir
			$destino=$url.$ide."/";
			if(!file_exists($destino)){
				mkdir($destino); 
			}
			//creamos archivo del empledo en el mes actual
			$archivo=$destino.$ide."-".$mes."-".$anio.".csv";
			//$contenido="lo que quieras escribir en el archivo";
			if(file_exists($archivo)){ unlink($archivo);}//eliminamos si existe el archivo
			$fdestino=fopen($archivo,"x");
			fwrite($fdestino, $textoFinal); 
			fclose($fdestino);
			return true;
		}else{
			return false;
		}
		
	}

/*FUNCIONES DE MANEJO DE PEDIDOS*/
	function array_unico($unico,$sort){
		$duplicado=$unico;
		$vector=array_values(array_unique($unico));
		$aux=array();
		$resultado=array();
		foreach ($vector as $valor){ 
			if($valor!="" && $valor!=false){
				$aux[count($aux)]=$valor;
			}
		}
		if($sort){
			natcasesort($aux);
		}
		//var_dump($aux);
		foreach ($aux as $val) {
			$ele=explode("[|]", $val);
			$resultado[]=$ele[1];	
		}
		return $resultado;
	}
/*END FUNCIONES DE MANEJO DE PEDIDOS*/


	function hora_costo_proceso($fecha_hora_inicio,$fecha_hora_fin,$horas_biometrico,$salario){
		$f1=explode(" ", $fecha_hora_inicio);
		$f2=explode(" ", $fecha_hora_fin);
		$hra0=new DateTime($f1[1]);//hora de entrada
		$hra1=new DateTime($f2[1]);//hora de salida
		$diff=new DateTime();//diferencia entre ambas horas
		$diff=$hra0->diff($hra1);
		$horas=($diff->h*60*60)+($diff->i*60)+$diff->s;//calculamos el tiempo trabajado
		$costo=($horas*$salario)/633600;
		return $horas."|".$costo;
	}
	
}

/* End of file validaciones.php */
/* Location: ./application/libraries/validaciones.php*/