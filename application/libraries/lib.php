<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lib{
/*---- MANEJO DE ARCHIVO BIOMETRICO---- */
function para_insertar_biometrico($empleado,$tipo,$datos){//preparamos los datos del archivo para insertar a la BD hora_biometrico
	$resp=array();//resultado final
	$horaAct="";
	$horaAnt="";
	$fechaAct="";
	$fechaAnt="";
	foreach($datos as $row):
		if($this->hora_empleado($empleado,$row,$tipo)){
			$save=false;
			$fecha_hora=explode(" ", $row[4]);//dividimos en fecha y hora, 01/12/2000 08:00
			$vf=explode("/", $fecha_hora[0]);
			$fecha=$vf[2].'-'.$this->dosDigitos($vf[1]).'-'.$this->dosDigitos($vf[0]);
			if($this->hora_iguales($fecha_hora[1],$horaAnt)){
				if($fecha!=$fechaAnt){
					$save=true;
				}
			}else{
				$save=true;
			}
			if($save){ $resp[]=array('fecha' => $fecha,'hora' => $fecha_hora[1]);}
			$horaAnt=$fecha_hora[1];
			$fechaAnt=$fecha;
		}
	endforeach;
	return json_encode($resp);
}
function hora_empleado($empleado,$fila,$tipo){//tipo de busqueda biometrico, por codigo(0) o cedula de identidad(1)
	$result=false;
	if($tipo==0){//Por codigo
		if($empleado->codigo==$fila[1]){
			$result=true;
		}
	}else{
		if($tipo==1){//Por cedula de identidad
			if($empleado->ci==$fila[2]){
				$result=true;
			}
		}
	}
	return $result;
}
function hora_iguales($hora1,$hora2){//comprar si dos horas son iguales segun su rango de error de 2min
	if($hora2!=NULL){
		$hra1=new DateTime($hora1);
		$hra2=new DateTime($hora2);
		$diff=new DateTime();
		$diff=$hra1->diff($hra2);
		$minutos=(($diff->h)*60)+$diff->i+(($diff->s)/60);
		if($minutos>5){
			return false; 
		}else{
			return true; 
		}
	}else{
		return false;
	}
}
/*---- END MANEJO DE ARCHIVO BIOMETRICO---- */
/*--- Manejo de costo de produccion ---*/
function costo_materiales($accesorios,$materiales,$materiales_liquidos,$indirectos,$idpim){
	$resultado = array();$cantidad=0;
	for($i=0; $i < count($accesorios) ; $i++){ $material=$accesorios[$i]; //calculando costo de materiales como argollas,cierres, etc;
		$resultado[count($resultado)]=array('nombre' => $material->nombre,'cantidad' => $material->cantidad,'costo_unitario' => $material->costo_unitario,'unidad' => $material->nombre_u,'abreviatura' => $material->abreviatura,'codigo' => $material->codigo,'color' => $material->nombre_color,'codigo_color' => $material->codigo_color,'fotografia' => $material->fotografia,'idmi' => $material->idmi);
	}
	for($i=0; $i < count($materiales_liquidos) ; $i++){ $material=$materiales_liquidos[$i]; //calculando costo de materiales que estans relacionados a las piezas, comos clefa, hilo, pintura, etc;
		if($material->equivalencia>0){
			$cantidad=($material->area*$material->variable)/$material->equivalencia;
		}
		$resultado[count($resultado)]=array('nombre' => $material->nombre,'cantidad' => $cantidad,'costo_unitario' => $material->costo_unitario,'unidad' => $material->nombre_u,'abreviatura' => $material->abreviatura,'codigo' => $material->codigo,'color' => $material->nombre_color,'codigo_color' => $material->codigo_color,'fotografia' => $material->fotografia,'idmi' => $material->idmi);
	}
	for($i=0; $i < count($materiales) ; $i++){ $material=$materiales[$i]; //calculando costo de materiales que estans relacionados con el grupo de piezas como: aguayo, cuero, tela, carton, etc.;
		if($material->color_producto!=1){
			if($material->equivalencia>0){
				$cantidad=$material->area/$material->equivalencia;
			}
			$resultado[count($resultado)]=array('nombre' => $material->nombre,'cantidad' => $cantidad,'costo_unitario' => $material->costo_unitario,'unidad' => $material->nombre_u,'abreviatura' => $material->abreviatura,'codigo' => $material->codigo,'color' => $material->nombre_color,'codigo_color' => $material->codigo_color,'fotografia' => $material->fotografia,'idmi' => $material->idmi);
		}
	}
	if(isset($idpim)){
		if($idpim!="" && $idpim!=NULL){
			for($i=0; $i < count($materiales) ; $i++){ $material=$materiales[$i];//calculando costo de materiales que estans relacionados con el grupo de piezas como: aguayo, cuero, tela, carton, etc.;
				if($material->idpim==$idpim){
					if($material->equivalencia>0){
						$cantidad=$material->area/$material->equivalencia;
					}
					
					$resultado[count($resultado)]=array('nombre' => $material->nombre,'cantidad' => $cantidad,'costo_unitario' => $material->costo_unitario,'unidad' => $material->nombre_u,'abreviatura' => $material->abreviatura,'codigo' => $material->codigo,'color' => $material->nombre_color,'codigo_color' => $material->codigo_color,'fotografia' => $material->fotografia,'idmi' => $material->idmi);
				}
			}
		}
	}
	for($i=0; $i < count($indirectos) ; $i++) { $material=$indirectos[$i]; //calculando costo de materiales que estans relacionados a las piezas, comos clefa, hilo, pintura, etc;
		$resultado[count($resultado)]=array('nombre' => $material->nombre,'cantidad' => "vacio",'costo_unitario' => $material->costo,'unidad' => $material->nombre_u,'abreviatura' => $material->abreviatura,'codigo' => $material->codigo,'color' => '','codigo_color' =>'','fotografia' => $material->fotografia,'idmi' => $material->idmi);
	}
	return json_encode($resultado);
}
function costo_materiales_color($materiales){
	$resultado = array();$cantidad=0;
	for($i=0; $i < count($materiales) ; $i++){ $material=$materiales[$i]; //calculando costo de materiales que estans relacionados con el grupo de piezas como: aguayo, cuero, tela, carton, etc.;
		if($material->color_producto==1){
			if($material->equivalencia>0){
				$cantidad=$material->area/$material->equivalencia;
			}
			$resultado[count($resultado)]=array('nombre' => $material->nombre,'cantidad' => $cantidad,'costo_unitario' => $material->costo_unitario,'unidad' => $material->nombre_u,'abreviatura' => $material->abreviatura,'codigo' => $material->codigo,'color' => $material->nombre_color,'codigo_color' => $material->codigo_color,'fotografia' => $material->fotografia,'idmi' => $material->idmi);
		}
	}
	return json_encode($resultado);
}
function costo_procesos($procesos,$procesos_pieza){
	$resultado = array();
	for($i=0; $i < count($procesos) ; $i++){ $proceso=$procesos[$i]; //calculando costo de materiales como argollas,cierres, etc;
		$resultado[count($resultado)]=array('nombre' => $proceso->nombre.": ".$proceso->sub_proceso,'cantidad' => '','segundos' => $proceso->tiempo_estimado,'costo' => $proceso->costo);
	}
	for($i=0; $i < count($procesos_pieza) ; $i++){ $proceso=$procesos_pieza[$i]; //calculando costo de materiales como argollas,cierres, etc;
		$resultado[count($resultado)]=array('nombre' => $proceso->nombre.": ".$proceso->nombre_pi,'cantidad' => $proceso->unidades,'segundos' => $proceso->tiempo_estimado,'costo' => $proceso->costo);
	}
	return json_encode($resultado);
}

function costo_total_produccion($accesorios,$materiales,$idpim,$materiales_liquidos,$indirectos,$procesos,$procesos_pieza){//CALCULANDO EL COSTO DE PRODUCCION DE UNA PRODUCTO SEGUN SU COLOR
	$costo_material=json_decode($this->costo_materiales($accesorios,$materiales,$materiales_liquidos,$indirectos,NULL));
	$costo_total=0;
	foreach ($costo_material as $clave => $valor){
		if($valor->cantidad=="vacio"){
			$costo_total+=$valor->costo_unitario;//solo para el caso de materiales indirectos
		}else{
			$costo_total+=($valor->cantidad*$valor->costo_unitario);
		}
	}
	$costo_total+=$this->costo_material_color($materiales,$idpim);
	$procesos=json_decode($this->costo_procesos($procesos,$procesos_pieza));
	foreach ($procesos as $clave => $valor){
		$costo_total+=$valor->costo;
	}
	return $costo_total;
}
function costo_material_color($materiales,$idpim){
	$costo=0;$cantidad=0;
	for($i=0; $i < count($materiales) ; $i++) { $material=$materiales[$i]; //calculando costo de materiales que esta relacionados con el color delproducto;
		if($material->idpim==$idpim){
			if($material->equivalencia>0){
				$cantidad=$material->area/$material->equivalencia;
			}
			$costo=$cantidad*$material->costo_unitario;
			break;
		}
	}
	return $costo;
}
function costo_total_produccion_sc($accesorios,$materiales,$materiales_liquidos,$indirectos,$procesos,$procesos_pieza){//CALCULANDO EL COSTO DE PRODUCCION DE UNA PRODUCTO SEGUN TODOS SUS COLORES
	$costo_material=json_decode($this->costo_materiales($accesorios,$materiales,$materiales_liquidos,$indirectos,NULL));
	$costo_total=0;
	foreach ($costo_material as $clave => $valor){
		if($valor->cantidad=="vacio"){
			$costo_total+=$valor->costo_unitario;//solo para el caso de materiales indirectos
		}else{
			$costo_total+=($valor->cantidad*$valor->costo_unitario);
		}
	}
	$procesos=json_decode($this->costo_procesos($procesos,$procesos_pieza));
	foreach ($procesos as $clave => $valor){
		$costo_total+=$valor->costo;
	}
	$colores_producto=json_decode($this->costo_materiales_color($materiales));
	$resultado=array();
	foreach ($colores_producto as $valor => $material) {
		$total=$costo_total+($material->costo_unitario*$material->cantidad);
		$resultado[count($resultado)]=array('nombre' => $material->nombre,'cantidad' => $material->cantidad,'costo_unitario' => $material->costo_unitario,'unidad' => $material->unidad,'abreviatura' => $material->abreviatura,'codigo' => $material->codigo,'color' => $material->color,'codigo_color' => $material->codigo_color,'costo_total' => $total);
	}
	return json_encode($resultado);
}
/*--- End Manejo de costo de produccion ---*/
/*--- Manejo de horas por empleado ---*/
	function horas_empleado($ide,$j_horas){
		$horas=json_decode($j_horas);
		$resultado = array();
		foreach ($horas as $clave => $valor){
			if($valor->ide==$ide){
				$resultado[]=array('idhb' => $valor->idhb, 'fecha' => $valor->fecha,'hora' => $valor->hora);
			}
		}
		return $resultado;
	}
	function horas_dia($j_horas,$empleado,$j_feriados,$fecha1,$fecha2,$horas_sabados_db){//calculo de las horas trabajadas por dia
		//$horas_trabajo,$tipo_contrato,$c_feriado
		//echo($empleado->nombre);
		$inicio=strtotime($fecha1);
		$horas=array();
		while($inicio<=strtotime($fecha2)){
			$fecha=date('Y-m-d',$inicio);
			$hras_dia=$this->search_horas($j_horas,$fecha);
			$horas_trabajadas=$this->horas_trabajo_dia(json_encode($hras_dia),$empleado,$j_feriados,$fecha,$horas_sabados_db);//calculando las horas trabajadas en el dia
			$horas[]=array('fecha'=>$fecha,'horas'=>$hras_dia,'horas_trabajadas'=>$horas_trabajadas);
			$inicio=strtotime('+1 day',$inicio);
		}
		return json_encode($horas);
	}
	function search_horas($j_horas,$fecha){
		$j_horas=json_decode($j_horas);
		$horas= array();
		$cont=0;
		foreach($j_horas as $clave => $valor){
			if($valor->fecha==$fecha){
				if($cont%2==0){ $tipo='Entrada'; $sigla="E"; }else{ $tipo='Salida'; $sigla="S"; }
				$hora_redondo=$this->redondeo_hora($valor->hora,$sigla);
				$horas[]=array('idhb'=>$valor->idhb,'tipo'=>$tipo,'sigla'=>$sigla,'hora' => $valor->hora,'hora_redondo' => $hora_redondo);
				$cont++;
			}
		}
		return $horas;
	}
	function horas_trabajo_dia($j_horas_dia,$empleado,$j_feriados,$fecha,$horas_sabados_db){
		//$horas_trabajo,$tipo_contrato,$c_feriado
		$vf=explode("-", $fecha);
		$horas_trabajadas=json_decode($this->tiempo_trabajo_dia($j_horas_dia,$empleado,$j_feriados,$fecha,$horas_sabados_db));
		$resp=array('horas_trabajadas' => $horas_trabajadas->horas,'horas_adeudadas' => $horas_trabajadas->descuento,'horas_trabajadas_no_redondo' => $horas_trabajadas->horas_original);
		return $resp;
	}
	function redondeo_hora($hora,$tipo){//redondeando la hora
		$horaFinal="00:00";
		switch ($tipo){
			case 'E':
				$hra=explode(":", $hora);
				if($hra[1]>=0 & $hra[1]<=10){ $horaFinal=$hra[0].":00";}
				if($hra[1]>=11 & $hra[1]<=40){ $horaFinal=$hra[0].":30";}
				if($hra[1]>=41 & $hra[1]<=59){ $horaFinal=($hra[0]+1).":00";}
				break;
			case 'S':
				$hra=explode(":", $hora);
				if($hra[1]>=0 & $hra[1]<=29){ $horaFinal=$hra[0].":00";}
				if($hra[1]>=30 & $hra[1]<=59){ $horaFinal=$hra[0].":30";}
				break;
		}
		return $horaFinal;
	}
	function tiempo_trabajo_dia($j_horas_dia,$empleado,$j_feriados,$fecha,$horas_sabados_db){
		//$horas_trabajo,$tipo_contrato,$c_feriado
		//verificando si es sabado
		$vf=explode("-", $fecha);
		$dia=$this->es_habil_domingo_sabado_feriado($fecha,$j_feriados);
		if($empleado->c_feriado==0){//se calculara con feriados como dia libre
			if($dia=="D" || $dia=="F"){
				$horas_descuento=0;
			}else{
				if($dia=="MF"){//es sabado o medio feriado
					if($empleado->tipo_contrato==0){//tiempo completo
						$horas_descuento=$empleado->horas/2;	
					}else{
						$horas_descuento=$empleado->horas;	
					}
				}else{
					if($dia=='S'){
						$horas_descuento=$horas_sabados_db*1;
					}else{
						$horas_descuento=$empleado->horas;
					}
				}
			}
		}else{//se calculara los feriados como dia de trabajo normal
			if($dia=="D"){
				$horas_descuento=0;
			}else{
				if($dia=='S'){//es sabado
					if($empleado->tipo_contrato==0){//tiempo completo
						$horas_descuento=$empleado->horas/2;	
					}else{
						$horas_descuento=$empleado->horas;	
					}
				}else{
					$horas_descuento=$empleado->horas;
				}
			}
		}
		$horas=json_decode($j_horas_dia);
		$hora1="";$hora1_org="";
		$hora2="";$hora2_org="";
		$total_hras=0;$total_hras_org=0;
		foreach ($horas as $key => $hra) {
			$hora2=$hra->hora_redondo;
			$hora2_org=$hra->hora;
			if($hora1==""){
				$hora1=$hora2;
				$hora1_org=$hora2_org;
			}else{
				$hra0=new DateTime($hora1);//hora de entrada
				$hra1=new DateTime($hora2);//hora de salida
				$diff=new DateTime();//diferencia entre ambas horas
				$diff=$hra0->diff($hra1);
				$diferencia=$diff->h+($diff->i/60)+0;//calculamos el tiempo trabajado
				$total_hras+=$diferencia;//sumamos al tiempo trabajado en el dia
				// para el caso de hora original
				$hra0=new DateTime($hora1_org);//hora de entrada
				$hra1=new DateTime($hora2_org);//hora de salida
				$diff=new DateTime();//diferencia entre ambas horas
				$diff=$hra0->diff($hra1);
				$diferencia=$diff->h+($diff->i/60)+0;//calculamos el tiempo trabajado
				$total_hras_org+=$diferencia;//sumamos al tiempo trabajado en el dia
				$hora1="";$hora1_org="";
			}
		}
		$descuento=$horas_descuento-$total_hras;
		$result = array('horas' => $total_hras, 'descuento' => $descuento,'horas_original' => $total_hras_org);
		return json_encode($result);
	}
	function sueldo_dia_hora($j_cant_dias,$tipo_contrato,$horas_trabajo,$salario_mes){//calculamos dos tipos de horas, para sueldos y para calculo de tiempo de proceso (con tiempo real)
		$cant_dias=json_decode($j_cant_dias);
		if($tipo_contrato==0){//es empleado a tiempo completo
			$horas=($cant_dias->habiles-$cant_dias->sabados)*$horas_trabajo;//solo horas de lunes a viernes
			$horas+=$cant_dias->sabados*($horas_trabajo/2);//solo horas de sabados
		}else{
			$horas=$cant_dias->habiles*$horas_trabajo;
		}
		$por_hora=$salario_mes/($cant_dias->habiles*$horas_trabajo);
		$por_dia=$salario_mes/$cant_dias->habiles;
		$por_hora_real=$salario_mes/$horas;
		$result = array('por_hora' => $por_hora,'por_hora_real' => $por_hora_real,'por_dia' => $por_dia);
		return $result;
	}
	function total_horas_trabajo($j_horas,$empleado,$j_feriados,$fecha1,$fecha2,$sueldo,$dias_habiles_db,$horas_sabados_db){//calculando el total de las horas trabajadas segun horas biometrico(EN USO VER PLANILLA)
		//$horas_trabajo,$tipo_contrato,$c_feriado
		$horas_dia=json_decode($this->horas_dia($j_horas,$empleado,$j_feriados,$fecha1,$fecha2,$horas_sabados_db));
		$h_trabajadas=0;
		$h_adeudadas=0;
		$h_no_redondo=0;
		$control=true;//control aque los marcados en el dia sega de catidad par
		$dias_habiles_periodico=$dias_habiles_db;
		foreach($horas_dia as $clave => $dia){
			$horas_trabajadas=json_decode(json_encode($dia->horas_trabajadas));
			$h_trabajadas+=($horas_trabajadas->horas_trabajadas);
			$h_adeudadas+=$horas_trabajadas->horas_adeudadas;
			$h_no_redondo+=$horas_trabajadas->horas_trabajadas_no_redondo;
			if(count($dia->horas)%2!=0){$control=false;}
		}
		//if($dias_habiles==NULL || $dias_habiles==""){ $dias_habiles=$this->dias_habiles_2($fecha1,$fecha2,$c_feriado,$j_feriados); }
		$meses=$this->meses($fecha1,$fecha2);
		$sueldo_total=$sueldo*$meses;
		$dias_habiles=$dias_habiles_periodico*$meses;
		$por_dia=$sueldo_total/$dias_habiles;
		$por_dia=$por_dia;
		$por_hora=$por_dia/$empleado->horas;
		//obteniendo la penalizacion por faltas mayores a una semana
		$penalizacion=json_decode($this->penalizacion($empleado,$h_adeudadas));
		$descuento=($h_adeudadas+$penalizacion->penalizacion)*$por_hora;//sumando penalizacion 
		$pagar=$sueldo_total-$descuento;
		$pagar_no_redondo=$h_no_redondo*$por_hora;
		if($h_trabajadas<=0){ $pagar=0;$pagar_no_redondo=0; }
		$resultado = array('sueldo_total' => $sueldo_total,'dias_habiles' => $dias_habiles,'por_dia' => $por_dia,'horas_trabajo' => $empleado->horas,'por_hora' => $por_hora,'descuento' => $descuento,'hra_trabajada' => $h_trabajadas,'hra_adeudadas' => $h_adeudadas,'penalizacion' => $penalizacion->penalizacion,'semanas_faltadas' => $penalizacion->semana,'sueldo' => $pagar,'hra_trabajada_no_redondo' => $h_no_redondo,'sueldo_no_redondo'=>$pagar_no_redondo,'control'=>$control);
		return json_encode($resultado);
	}
	function penalizacion($empleado,$h_adeudadas){//calcaula la penalizaicion por faltas
		//$tipo_contrato,$horas_trabajo
		$penalizacion=0;
		$semana=0;
		if($empleado->c_descuento==0){
			if($empleado->tipo_contrato==0){ $semana=($empleado->horas*5)+($empleado->horas/2);}else{$semana=$empleado->horas*6;}
			if($h_adeudadas>$semana){ $penalizacion=floor($h_adeudadas/$semana)*($empleado->horas/2);}
		}
		$result = array('penalizacion' => $penalizacion, 'semana' => $semana);
		return json_encode($result);		
	}
	function meses($fecha1,$fecha2){
		$meses=0;
		$inicio=strtotime($fecha1);
		while($inicio<=strtotime($fecha2)){
			$meses++;
			$inicio=strtotime("+1 month",$inicio);
		}
		return$meses;
	}
	/*function calcula_sueldo($sueldo_mes,$fecha1,$fecha2){
		$inicio=strtotime($fecha1);
		$meses=0;
		while($inicio<=strtotime($fecha2)){
			$meses++;
			$inicio=strtotime("+1 month",$inicio);
		}
		$sueldo=0;
		if(strtotime($fecha1)<=strtotime($fecha2)){
			//$vf1=explode("-", $fecha1);
			//$f1=$vf1[0]."-".$vf1[1]."-01";
			//$vf2=explode("-", $fecha2);
			//$f2=$vf2[0]."-".$vf2[1]."-".$this->ultimo_dia($vf2[1],$vf2[0]);
			//$dias_habiles_complet=$this->dias_habiles_2($f1,$f2,$c_feriado,$j_feriados);
			//regla de tres
			//$sueldo=($dias_habiles*($sueldo_mes*$meses))/$dias_habiles_complet;
			//echo $dias_habiles;
			$sueldo=$sueldo_mes*$meses;
		}
		return $sueldo;
	}*///
	function add_json_horas($j_horas,$ide,$fecha,$hora,$pos){
		$horas=json_decode($j_horas);
		$result=array();
		$sw=true;
		foreach($horas as $key => $h){
			if($pos=="ini"){
				if($sw){ $result[] = array('idhb' => '0','ide' => $ide,'fecha' => $fecha, 'hora' => $hora); $sw=false; }
			}
			$result[] = array('idhb' => $h->idhb,'ide' => $h->ide,'fecha' => $h->fecha, 'hora' => $h->hora);
		}
		if($pos=="fin"){ $result[] = array('idhb' => '0','ide' => $ide,'fecha' => $fecha, 'hora' => $hora); }
		return json_encode($result);
	}
	function horas_trabajo_modificado($j_horas,$ide,$fecha,$hora,$pos,$horas_trabajo,$tipo_contrato,$j_feriados){//calcula la horas trabajadas anterior o posterior. EN USO TAREA->CALCULAR TIEMPO DE TRABAJO
		$j_horas_antes=$this->add_json_horas($j_horas,$ide,$fecha,$hora,$pos);
		$j_horas_antes=json_encode($this->search_horas($j_horas_antes,$fecha));
		$res_horas=$this->tiempo_trabajo_dia($j_horas_antes,$horas_trabajo,$tipo_contrato,$j_feriados,$fecha);// devuelde las horas antes o despues de la fecha de inicio de trabajo y finalizacion
		return $res_horas;
	}
		function que_dia_es($dia,$mes,$anio){
		$dia=date('w', mktime(0,0,0,$mes,$dia,$anio));
		return $dia;
	}
	function dias_habiles($anio,$mes,$feriados){
		$habil=0;
		$sabado=0;
		$max=$this->ultimo_dia($mes,$anio);
		for($i=1; $i <= $max; $i++){ 
			$dia=$this->que_dia_es($i,$mes,$anio);
			$fecha=$anio."-".$mes."-".$this->dosDigitos($i);
			if(!$this->existe_fecha($feriados,$fecha)){
				if($dia!=0){
					if($dia==6){
						$sabado++;
					}
					$habil++;
				}
			}
		}
		$dia = array('sabados' => $sabado, 'habiles' => $habil);
		return $dia;
	}
	function dias_habiles_2($fecha1,$fecha2,$c_feriado,$j_feriados){
		$inicio=strtotime($fecha1);
		$habil=0;
		while ($inicio<=strtotime($fecha2)){
			$fecha=date('Y-m-d',$inicio);
			if($c_feriado!=1){
				if(!$this->existe_fecha($j_feriados,$fecha)){
					$vf=explode("-", $fecha);
					$dia=$this->que_dia_es($vf[2],$vf[1],$vf[0]);
					if($dia!=0){
						$habil++;
					}
				}
			}else{
				$vf=explode("-", $fecha);
				$dia=$this->que_dia_es($vf[2],$vf[1],$vf[0]);
				if($dia!=0){
					$habil++;
				}
			}
			$inicio=strtotime('+1 day',$inicio);
		}
		return $habil;
	}
















	
	function horas_fecha($ide,$horas,$fecha){//sacamos las horas de los registros de la BD $horas en formato array bidimencional
		$resp = array();
		$cont=0;
		$tipo="";
		$sigla="";
		foreach (json_decode($horas) as $key => $hora) {
			if($hora->fecha==$fecha && $hora->ide==$ide){
				if($cont%2==0){ $tipo='Entrada'; $sigla="E"; }else{ $tipo='Salida'; $sigla="S"; }
				$redondo=$this->redondeo_hora($hora->hora,$sigla);
				$resp[$cont]=array('tipo' => $tipo,'sigla' => $sigla,'hora' => $hora->hora,'redondo' => $redondo,'fecha' => $fecha);
				$cont++;
			}
		}
		return $resp;
	}
	function horas_fecha_adicionar($ide,$horas,$fecha,$hora_adicionar,$antes_despues){//sacamos las horas de los registros de la BD $horas en formato array bidimencional
		$resp = array();
		$cont=0;
		$tipo="";
		$sigla="";
		if($antes_despues=='despues'){
			$redondo=$this->redondeo_hora($hora_adicionar,"E");
			$resp[$cont]=array('tipo' => 'Entrada','sigla' => "E",'hora' => $hora_adicionar,'redondo' => $redondo,'fecha' => $fecha);
			$cont++;
		}
		foreach (json_decode($horas) as $key => $hora) {
			if($hora->fecha==$fecha && $hora->ide==$ide){
				if($cont%2==0){ $tipo='Entrada'; $sigla="E"; }else{ $tipo='Salida'; $sigla="S"; }
				$redondo=$this->redondeo_hora($hora->hora,$sigla);
				$resp[$cont]=array('tipo' => $tipo,'sigla' => $sigla,'hora' => $hora->hora,'redondo' => $redondo,'fecha' => $fecha);
				$cont++;
			}
		}
		if($antes_despues=='antes'){
			if($cont%2==0){ $tipo='Entrada'; $sigla="E"; }else{ $tipo='Salida'; $sigla="S"; }
			$redondo=$this->redondeo_hora($hora_adicionar,$sigla);
			$resp[$cont]=array('tipo' => $tipo,'sigla' => $sigla,'hora' => $hora_adicionar,'redondo' => $redondo,'fecha' => $fecha);
			$cont++;
		}
		return $resp;
	}

	function calcula_horas_trabajo($v_horas,$tipo_contrato,$horas_trabajo,$salario_mes,$fecha,$j_feriados){ //tipo de dias H,S,D,F
		$resp = array();
		$horas_trabajadas="";
		$horas_trabajadas_redondo="";
		$horas_adeudadas="";
		$horas_adeudadas_redondo="";
		$vf=explode("-", $fecha);
		$dias=json_encode($this->dias_habiles($vf[0],$vf[1],$j_feriados));
		$sueldo_hora=json_encode($this->sueldo_por_hora_dia($dias,$tipo_contrato,$horas_trabajo,$salario_mes));
		$horas_trabajadas=json_decode(json_encode($this->horas_trabajo($v_horas,$tipo_contrato,$horas_trabajo,$sueldo_hora,$fecha)));
		$resp = array('horas_trabajadas' => $horas_trabajadas->horas,'horas_adeudadas' => $horas_trabajadas->descuento,'por_hora' => $horas_trabajadas->por_hora,'por_dia' => $horas_trabajadas->por_dia);
		return $resp;
	}
	function sueldo_por_hora_dia($j_cant_dias,$tipo_contrato,$horas_trabajo,$salario_mes){//calculamos dos tipos de horas, para sueldos y para calculo de tiempo de proceso (con tiempo real)
		$cant_dias=json_decode($j_cant_dias);
		if($tipo_contrato==0){//es empleado a tiempo completo
			$horas=($cant_dias->habiles-$cant_dias->sabados)*$horas_trabajo;//solo horas de lunes a viernes
			$horas+=$cant_dias->sabados*($horas_trabajo/2);//solo horas de sabados
		}else{
			$horas=$cant_dias->habiles*$horas_trabajo;
		}
		$por_hora=$salario_mes/($cant_dias->habiles*$horas_trabajo);
		$por_dia=$salario_mes/$cant_dias->habiles;
		$por_hora_real=$salario_mes/$horas;
		$result = array('por_hora' => $por_hora,'por_hora_real' => $por_hora_real,'por_dia' => $por_dia);
		return $result;
	}
	function horas_trabajo($v_horas,$tipo_contrato,$horas_trabajo,$j_sueldo_hora,$fecha){
		$sueldo_hora=json_decode($j_sueldo_hora);
		//verificando si es sabado
		$vf=explode("-", $fecha);
		$dia=$this->que_dia_es($vf[2],$vf[1],$vf[0]);
		if($dia==6){//es sabado
			if($tipo_contrato==0){//tiempo completo
				$horas_descuento=$horas_trabajo/2;	
			}else{
				$horas_descuento=$horas_trabajo;	
			}
		}else{
			$horas_descuento=$horas_trabajo;
		}
		if(count($v_horas)>1 && count($v_horas)%2==0){
			$horas=json_decode(json_encode($v_horas));
			$hora1="";
			$hora2="";
			$total_hras=0;
			foreach ($horas as $key => $hra) {
				$hora2=$hra->redondo;
				if($hora1==""){
					$hora1=$hora2;
				}else{
					$hra0=new DateTime($hora1);//hora de entrada
					$hra1=new DateTime($hora2);//hora de salida
					$diff=new DateTime();//diferencia entre ambas horas
					$diff=$hra0->diff($hra1);
					$diferencia=$diff->h+($diff->i/60)+0;//calculamos el tiempo trabajado
					$total_hras+=$diferencia;//sumamos al tiempo trabajado en el dia
					$hora1="";
				}
			}
			$result = array('horas' => $total_hras, 'descuento' => $horas_descuento-$total_hras,'por_hora' => $sueldo_hora->por_hora,'por_dia' => $sueldo_hora->por_dia);
		}else{
			$result = array('horas' => '0', 'descuento' => $horas_descuento,'por_hora' => $sueldo_hora->por_hora,'por_dia' => $sueldo_hora->por_dia);
		}
		return $result;
	}
/*--- End manejo de horas por empleado ---*/
/*--- Manejo de horas por empleado produccion ---*/
	function calculo_total_horas_trabajo($j_horas,$feriados,$empleado,$fini,$ffin,$horas_antes,$horas_despues){
		$feriados=json_encode($feriados);
		$vf1=explode(" ", $fini);$fecha1=$vf1[0];
		$vf2=explode(" ", $ffin);$fecha2=$vf2[0];
		if(!empty($empleado)){
			$empleado=$empleado[0];
			$hora_antes=0;
			$tota_antes=0;
			$hora_despues=0;
			$total_despues=0;
			if(count($horas_antes)>0){
				$j_horas_antes=json_encode($horas_antes);
				if(count($horas_antes)%2==0){
					$v_hora_empleado=$this->horas_fecha($empleado->ide,$j_horas_antes,$fecha1);//horas en el dia
				}else{
					$v_hora_empleado=$this->horas_fecha_adicionar($empleado->ide,$j_horas_antes,$fecha1,$vf1[1],'antes');//horas en el dia
				}
				$antes=json_decode(json_encode($this->calcula_horas_trabajo($v_hora_empleado,$empleado->tipo_contrato,$empleado->horas,$empleado->salario,$fecha1,$feriados)));//totales por dia
				$hora_antes=$antes->horas_trabajadas;
				$total_antes=($antes->horas_adeudadas*-1*$antes->por_hora)+$antes->por_dia;
			}
			if(count($horas_despues)>0){
				$j_horas_despues=json_encode($horas_despues);
				if(count($horas_despues)%2==0){
					$v_hora_empleado=$this->horas_fecha($empleado->ide,$j_horas_despues,$fecha2);//horas en el dia
				}else{
					$v_hora_empleado=$this->horas_fecha_adicionar($empleado->ide,$j_horas_despues,$fecha2,$vf2[1],'despues');//horas en el dia
				}
				$despues=json_decode(json_encode($this->calcula_horas_trabajo($v_hora_empleado,$empleado->tipo_contrato,$empleado->horas,$empleado->salario,$fecha2,$feriados)));//totales por dia
				$hora_despues=$despues->horas_trabajadas;
				$total_despues=($despues->horas_adeudadas*-1*$despues->por_hora)+$despues->por_dia;
			}
			$inicio=strtotime($fecha1);
			$total_horas_trabajadas=0;
			$total_sueldo=0;
			while ($inicio<=strtotime($fecha2)){
				$fecha=date('Y-m-d',$inicio);
				$v_hora_empleado=$this->horas_fecha($empleado->ide,$j_horas,$fecha);//horas en el dia
				$result=json_decode(json_encode($this->calcula_horas_trabajo($v_hora_empleado,$empleado->tipo_contrato,$empleado->horas,$empleado->salario,$fecha,$feriados)));//totales por dia
				$total_horas_trabajadas+=$result->horas_trabajadas;
				//SELECT * FROM `hora_biometrico` WHERE fecha='2015-05-04' and ide='11' and hora<='14:15:46'
				//$horas_adeudadas=$result->horas_adeudadas;
				$total_sueldo+=($result->horas_adeudadas*-1*$result->por_hora)+$result->por_dia;
				$inicio=strtotime('+1 day',$inicio);
			}
		}
		$result = array('horas_trabajadas' => ($total_horas_trabajadas-$hora_antes-$hora_despues),'sueldo' => ($total_sueldo-$total_antes-$total_despues));
		return $result;
	}
/*--- End manejo de horas por empleado produccion ---*/
/*--- Manejo de fechas ---*/
	function es_habil_domingo_sabado_feriado($fecha,$j_feriados){//calcula si es dia habil feriado o domingo
		$resp="";
		$v=explode("-", $fecha);
		$fer=$this->search_feriado($j_feriados,$fecha);
		if($fer==""){//en caso de no ser feriado
			$diaN=$this->que_dia_es($v[2],$v[1],$v[0]);
				if($diaN==0){//si es domingo
					$resp="D";
				}else{// dia normal lunes a viernes;
					if($diaN==6){
						$resp="S";
					}else{
						$resp="H";
					}
				}
		}else{
			$resp=$fer;
		}
		return $resp;
	}
	function search_feriado($j_fechas,$fecha){// busca si existe la fecha en los feriados F(Feriado completo), MF(Medio feriado)
		$result="";
		$fechas=json_decode($j_fechas);
		foreach ($fechas as $key => $fila) {
			if($fecha==$fila->fecha){
				if($fila->tipo==0){
					$result="F";
				}else{
					$result="MF";
				}
				break;
			}
		}
		return $result;
	}
	function existe_fecha($j_fechas,$fecha){// busca si existe la fecha en el array asociativo: 2000-12-31
		$fechas=json_decode($j_fechas);
		foreach ($fechas as $key => $fila) {
			if($fecha==$fila->fecha && $fila->tipo!=1){//si exite la fecha y no es medio feriado
				return true;
			}
		}
		return false;
	}

	function ultimo_dia($mes,$anio){
		$final=date("d",(mktime(0,0,0,$mes+1,1,$anio)-1));
		return $final;
	}
	function obtiene_meses($fecha1,$fecha2){
		$resp=array();
		$inicio=strtotime($fecha1);
		while ($inicio<=strtotime($fecha2)) {
			$resp[count($resp)]=date('Y-m',$inicio);
			$inicio=strtotime('+1 day',$inicio);
		}
		$resp=array_unique($resp);
		return $resp;
	}
	function mes_literal($mes){
		$mes=$this->dosDigitos($mes);
		$m="";
		switch ($mes) {
			case '01':  $m="ENERO"; break;
			case '02':  $m="FEBRERO"; break;
			case '03':  $m="MARZO"; break;
			case '04':  $m="ABRIL"; break;
			case '05':  $m="MAYO"; break;
			case '06':  $m="JUNIO"; break;
			case '07':  $m="JULIO"; break;
			case '08':  $m="AGOSTO"; break;
			case '09':  $m="SEPTIEMBRE"; break;
			case '10':  $m="OCTUBRE"; break;
			case '11':  $m="NOVIEMBRE"; break;
			case '12':  $m="DICIEMBRE"; break;
		}
		return $m;
	}
	function format_date($fecha,$cond){
		if($fecha!="" && $fecha!=NULL){
			$f=explode("-", $fecha);
			if(count($f)>=3){
				$anio=$f[0];
				$mes=$f[1];
				$dia=$f[2];
				switch ($cond) {
					case 'Y-m-d': $resp=$anio."-".$mes."-".$dia; break;
					case 'Y-ml-d': $resp=$anio."-".$this->ini_mayuscula($this->mes_literal($mes))."-".$dia; break;
					case 'd-m-Y': $resp=$dia." - ".$mes." - ".$anio; break;
					case 'd-ml-Y': $resp=$dia." - ".$this->ini_mayuscula($this->mes_literal($mes))." - ".$anio; break;
					case 'Y/m/d': $resp=$anio."/".$mes."/".$dia; break;
					case 'Y/ml/d': $resp=$anio."/".$this->ini_mayuscula($this->mes_literal($mes))."/".$dia; break;
					case 'd/m/Y': $resp=$dia."/".$mes."/".$anio; break;
					case 'd/ml/Y': $resp=$dia." / ".$this->ini_mayuscula($this->mes_literal($mes))." / ".$anio; break;
					case 'd ml Y': $resp=$dia." de ".$this->ini_mayuscula($this->mes_literal($mes))." de ".$anio; break;
					case 'dl ml Y': $resp=$this->nombre_de_dia($this->que_dia_es($dia,$mes,$anio)).", ".($dia*1)." de ".$this->ini_mayuscula($this->mes_literal($mes))." de ".$anio; break;
					case 'dl ml2 Y': $resp=$this->nombre_de_dia($this->que_dia_es($dia,$mes,$anio)).", ".$dia." de ".$this->ini_mayuscula($this->mes_literal($mes))." de ".$anio; break;
					default: $resp=$anio."-".$mes."-".$dia; break;
				}
			}
		}else{
			$resp="";
		}
		return $resp;
	}
	function nombre_de_dia($date){
		$dia="";
		switch($date){
			case 0: $dia="Domingo"; break;
			case 1: $dia="Lunes"; break;
			case 2: $dia="Martes"; break;
			case 3: $dia="Miercoles"; break;
			case 4: $dia="Jueves"; break;
			case 5: $dia="Viernes"; break;
			case 6: $dia="Sábado"; break;
		}
		return $dia;
	}
/*--- end manejo de fechas ---*/
/*--- Manejo de cadenas ---*/
	function ini_mayuscula($cad){//primera letra en mayuscula
		return ucfirst($this->all_minuscula($cad));
	}
	function all_mayuscula($cad){
		return strtoupper($cad); 
	}
	function all_minuscula($cad){
		return utf8_decode(mb_convert_case($cad,MB_CASE_LOWER,"UTF-8"));
	}
	function may_palabra($cad){//convierte en mayuscula el primer caracter de cada palabra
		return ucwords($cad);
	}
	function may_cadena($cad){//primera letra en mayuscula
		return ucfirst($this->all_minuscula($cad));
	}
/*--- Manejo de cadenas ---*/
/*--- Manejo de numeros ---*/
	function dosDigitos($num){
		$num=$num*1;
		if($num>=0 & $num<10){
			$num="0".$num;
		}else{
			$num="".$num;
		}
		return $num;
	}
/*--- End Manejo de numeros ---*/	
/*--- Manejo de imagenes ---*/
	function subir_imagen_miniatura($FILES,$ruta,$pos,$resize,$cod){
		$img=NULL;
		if(count($FILES)>0){
			$rutaMiniatura=$ruta.'miniatura/';
			$key=$FILES['archivo'.$pos];
			$nameImg=$this->subir_imagen($key,$cod,$ruta);
			if($nameImg!="error_type" && $nameImg!="error" && $nameImg!="error_size_img"){
				if($this->crear_miniatura($resize,$ruta.$nameImg,$rutaMiniatura.$nameImg)){
					//subio y creo miniatura
					$img=$nameImg;
				}else{//no subio 
					if($this->eliminar_archivo($nameImg,$ruta)){}
					$img="error";
				}
			}else{
				$img=$nameImg;
			}
		}
		return $img;
	}
	function subir_imagen($file,$nombre,$ubicacion){
		$key=$file;
		$name="";
		if($this->size_valido($key,1)){//verificando que el tamaño de la imagen sea menor o igual a 1 MB
			if($this->es_imagen_valida($key)){
				if(!file_exists($ubicacion)){ mkdir($ubicacion);}
				if($key['error'] == UPLOAD_ERR_OK ){
					//Si el archivo se paso correctamente Continuamos
					$atrib=explode(".", $key['name']); 
					if($nombre==NULL || $nombre==""){$nombre=rand(0,9999999);}
					$nombre=$nombre."-".rand(0,9999999).".".$atrib[count($atrib)-1];
					$temporal = $key['tmp_name']; //Obtenemos la ruta Original del archivo
					$Destino = $ubicacion.$nombre;	//Creamos una ruta de destino con la variable ruta y el nombre original del archivo	
					if(file_exists($Destino)){ unlink($Destino);}
					move_uploaded_file($temporal, $Destino); //Movemos el archivo temporal a la ruta especificada
					$name=$nombre;
				}
				if($key['error']!=''){
					$name="error";
				}
			}else{
				$name="error_type";
			}
		}else{
			$name="error_size_img";
		}
		return $name;
	}
	function size_valido($file,$max_size){
		if(($file['size']*1) <= ($max_size*1024*1024)){
			return true;
		}else{
			return false;
		}
	}
	function es_imagen_valida($file){
		if($file['type'] =='image/jpeg' || $file['type'] =='image/jpg' || $file['type'] =='image/gif' || $file['type'] =='image/png'){
			return true;
		}else{
			return false;
		}
	}
	function subir_excel($file,$ubicacion,$nombre,$atributo){
		if($this->excel_valido($file,$atributo)){
			if(!file_exists($ubicacion)){ mkdir($ubicacion);}
			if($file['error'] == UPLOAD_ERR_OK ){
				//Si el archivo se paso correctamente Continuamos
				$atrib=explode(".", $file['name']); 
				$nombre=str_replace(".xls", ' '.$nombre,$file['name']);
				$nombre=$nombre.".".$atrib[count($atrib)-1];
				$temporal = $file['tmp_name']; //Obtenemos la ruta Original del archivo
				$destino = $ubicacion.$nombre;	//Creamos una ruta de destino con la variable ruta y el nombre original del archivo	
				if(file_exists($destino)){ unlink($destino);}
				move_uploaded_file($temporal, $destino); //Movemos el archivo temporal a la ruta especificada
				$name=$nombre;
			}
			if($file['error']!=''){
				$name="error";
			}
		}else{
			$name="error_type";
		}
		return $name;
	}
	function excel_valido($file,$extencion){
		$v_nom=explode(".", $file['name']);
		if($file['type']=='application/vnd.ms-excel' && $extencion==$v_nom[count($v_nom)-1]){
			return true;
		}else{
			return false;
		}
	}
	function crear_miniatura($funcion,$origen,$destino){
		if(file_exists($origen)){
			$min=$funcion;
			$min->inicio($origen);
			$min->resizeImage(140, 140, 'crop');
			if(!$min->saveImage($destino, 100)){
				return true;
			}else{//error en la creacion, eliminar archivo
				return false;
			}
		}else{
			return false;
		}
	}
	function eliminar_imagen($file,$ruta){
		if($file!="" && $file!=NULL){
			$rutaMiniatura=$ruta.'miniatura/';
			if($this->eliminar_archivo($file,$ruta) && $this->eliminar_archivo($file,$rutaMiniatura)){
				return true;
			}else{
				return false;
			}
		}else{
			return true;
		}
	}
	function eliminar_archivo($namefile,$dir){
		$namef=str_replace(' ', '', $namefile);
		if($namef!="" & $namef!=NULL){
			$dir_file=$dir.$namefile;
			if(file_exists($dir_file)){
				if(unlink($dir_file)){
					return true;
				}else{
					return false;
				}
			}else{
				return true;// no exitste archivo
			}
		}else{
			return true;// no exite imagen
		}
		
	}
	function cambiar_imagen($FILES,$ruta,$pos,$resize,$origen,$id){
		$img=$origen;
		if(count($FILES)>0){
			$rutaMiniatura=$ruta.'miniatura/';
			$key=$FILES['archivo'.$pos];
			$nameImg=$this->subir_imagen($key,$id,$ruta);
			if($nameImg!="error_type" && $nameImg!="error" && $nameImg!="error_size_img"){
				if($this->crear_miniatura($resize,$ruta.$nameImg,$rutaMiniatura.$nameImg)){
					//subio y creo miniatura
					//eliminado archivo
					if($this->eliminar_archivo($img,$ruta) && $this->eliminar_archivo($img,$rutaMiniatura)){}
					$img=$nameImg;
				}else{//no subio 
					if($this->eliminar_archivo($nameImg,$ruta)){}
				}
			}else{
				$img=$nameImg;
			}
		}
		return $img;
	}
/*--- End manejo de imagenes ---*/
/*--- CALCULO DE COSTO DE DEPRECIACION ---*/
	function costo_depreciacion($monto,$anioDeVida,$fechaInicial){
		$costo_actual=0;
		//calculando depreciacion
		$dep_anual=$monto/$anioDeVida;//depreciacion anual
		//calculando dias existente en un año
		$fecha_final=strtotime('+1 year',strtotime($fechaInicial));
		$fecha_final=date('Y-m-j',$fecha_final);
		$segundos=strtotime($fecha_final)-strtotime($fechaInicial);
		$dias=intval($segundos/60/60/24);
		$depreciacion_por_dia=$dep_anual/$dias;
		//dias transcurridos
		$segundos=strtotime('now')-strtotime($fechaInicial);
		$dias=intval($segundos/60/60/24);
		$costo_actual=$monto-($depreciacion_por_dia*$dias);		
		return $costo_actual;
	}
/*--- END CALCULO DE COSTO DE DEPRECIACION ---*/
/*--- MANEJO DE HORAS ---*/
	function hms($segundos){
		$s=0;
		$h=0;
		$m=0;
		if($segundos!=0 && $segundos!=NULL && $segundos!=""){
			$s+=$segundos%60;
			$aux=floor($segundos/60);//minutos
			$m+=$aux%60;
			$h+=floor($aux/60);
		}
		return $this->dosDigitos($h).':'.$this->dosDigitos($m).':'.$this->dosDigitos($s);
	}
/*--- MANEJO DE VECTORES ---*/
	function array_unico($unico,$sort){//ELIMINA REPETIDOS, en uso REPORTE->PEDIDOS->MATERIALES
		$duplicado=$unico;
		$vector=array_values(array_unique($unico));
		$aux=array();
		$resultado=array();
		foreach ($vector as $valor){ 
			if($valor!="" && $valor!=false){
				$aux[count($aux)]=$valor;
			}
		}
		if($sort){
			natcasesort($aux);
		}
		//var_dump($aux);
		foreach ($aux as $val) {
			$ele=explode("[|]", $val);
			$resultado[]=$ele[1];	
		}
		return $resultado;
	}
/*--- END MANEJO DE VECTORES ---*/	
/*--- MANEJO DE CAPITAL HUMANO ---*/
	function biometrico_valido($file){
		$cont=0;
		$sw=true;
		foreach($file as $row) : 
			if($cont==2){
				if(isset($row[1]) && isset($row[2]) && isset($row[3]) && isset($row[4])){
					if(($row[1]!="" || $row[2]!="") && $row[3]!="" && $row[4]!=""){
					}else{
						$sw=false;
					}
				}else{
					$sw=false;
				}
				break;
			}
			$cont++;			
		endforeach;
		if($cont>0 && $sw){
			return true;
		}else{
			return false;
		}
	}
	function min_max_fecha_biometrico($file){//en uso CAPITAL HUMANO,
		//usamos para hallar la minima o maxima fecha del archivo biometrico
		$merge=array();
		$max=strtotime('1970-01-01');
		$min=strtotime('2035-12-31');
		foreach($file as $row) : 
			$fecha_hora=explode(" ", $row[4]);//dividimos en fecha y hora, 01/12/2000 08:00
			$vf=explode("/", $fecha_hora[0]);
			if(count($vf)==3){
				$fecha=strtotime($vf[2].'-'.$vf[1].'-'.$vf[0]);
				if($fecha<=$min){ $min=$fecha; }
				if($fecha>=$max){ $max=$fecha; }
			}
		endforeach;
		$res = array('fecha_min' => date('Y-m-d',$min), 'fecha_max' => date('Y-m-d',$max));
		return json_encode($res);
	}
	/*--- para pedidos ---*/
	function es_feriado($fecha,$feriados){
		$sw=false;
		for ($i=0; $i < count($feriados) ; $i++) { 
			if($feriados[$i]->fecha==$fecha){
				$sw=true;
				break;
			}
		}
		return $sw;
	}

	function siguiente_dia_habil($fecha,$feriados){
		$fecha_habil="0000-00-00";
		$fin = new DateTime($fecha);
		while (true) {
			$fin->modify('+1 days');
			if(!$this->es_feriado($fin->format('Y-m-d'),$feriados) && $fin->format('l')!= 'Sunday'){//no es feriado y domingo
				$fecha_habil=$fin->format('Y-m-d');
				return $fecha_habil;
			}
		}
		return $fecha_habil;
	}
	function fecha_de_entrega_pedido($fini,$segundos,$feriados,$carga_horaria){
		$fecha_entrega=$fini;
		if(!empty($carga_horaria)){
			$segundos_por_dia=$carga_horaria[0]->horas*60*60;
			$seg=$segundos;
			$fin = new DateTime($fini);
			while ($seg>($segundos_por_dia/2)){
				if($fin->format('l')== 'Saturday'){
					$seg-=($segundos_por_dia/2);//tomamos los segundos trabajados en un sabado(medio )
				}else{
					$seg-=$segundos_por_dia;//tomamos los segundos trabajados en un jornal
				}
				if($seg>0){
					$fecha_entrega=$this->siguiente_dia_habil($fin->format('Y-m-d'),$feriados);
					$v=explode("-", $fecha_entrega);
					$fin->setDate($v[0],$v[1],$v[2]);
				}
			}
		}
		return $fecha_entrega;
	}
/*--- end para pedidos ---*/
/*---- ----*/
	function password($pass){
		$salt="$2a$10$".sha1($pass)."$";
		$password=crypt($pass,$salt);
		return $password;
	}
/*---- ----*/
}
/* End of file lib.php */
/* Location: ./application/libraries/lib.php*/