<?php
	$cargo=$this->session->userdata("cargo");
	if($this->session->userdata("nombre2")!=""){
		$usuario=$this->session->userdata("nombre")." ".$this->session->userdata("nombre2")." ".$this->session->userdata("paterno");
	}else{
		$usuario=$this->session->userdata("nombre")." ".$this->session->userdata("paterno");
	}
    $ci=$this->session->userdata("ci");
?>
<!DOCTYPE html>
<html lang="es" class="no-js">
	<head><?php $this->load->view('estructura/head',['title'=>'Produccion','css'=>'']);?></head>
	<body>
		<?php $this->load->view('estructura/modal');?>
		<div class="contenedor">
			<?php $this->load->view('estructura/menu_izq',['usuario'=>$usuario,'cargo'=>$cargo,'ventana'=>'produccion','privilegio'=>$privilegio[0]]);?>
			<?php $v_menu="";
				if($privilegio[0]->pr1r==1){ $v_menu.="Productos/producto/icon-suitcase|"; }
				if($privilegio[0]->pr2r==1){ $v_menu.="Orden de producción/orden/icon-clipboard2|";}
				if($privilegio[0]->pr3r==1){ $v_menu.="Configuración/config/glyphicon glyphicon-cog";}
			?>
			<?php $this->load->view('estructura/menu_top',['usuario'=>$usuario,'cargo'=>$cargo,'menu'=>$v_menu]);?>
			<div id="search"></div>
			<div id="contenido"></div>
		</div>
		<?php $this->load->view('estructura/js',['js'=>'produccion/produccion.js']);?>
	</body>
	<?php 
		switch ($pestania) {
			case '1': if($privilegio[0]->pr1r==1){ $title="Producto";$activo='producto'; $search="produccion/search_producto"; $view="produccion/view_producto";} break;
			case '2': if($privilegio[0]->pr2r==1){ $title="Orden de producción";$activo='orden'; $search="produccion/search_orden"; $view="produccion/view_orden";} break;
			case '5': if($privilegio[0]->pr3r==1){ $title="Configuración de producción";$activo='config'; $search=""; $view="produccion/view_config";} break;
			//default: $title="Error";$activo='';$search="NULL";$view="NULL"; break;
		}
	?>
	<script type="text/javascript">activar('<?php echo $activo;?>','<?php echo $title;?>','produccion?p=<?php echo $pestania;?>'); get_2n('<?php echo $search; ?>',{},'search',false,'<?php echo $view; ?>',{},'contenido',true); </script>
</html>