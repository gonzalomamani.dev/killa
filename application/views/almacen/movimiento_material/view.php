<?php  $j_movimientos=json_encode($movimientos); ?>
<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
	    	<th colspan="10"></th> 
			<th>
			<?php if($privilegio[0]->al2d=="1"){ ?>
				<button id="bh" class="col-xs-12 btn btn-danger btn-sm">Borrar Historial</button>
			<?php }?>
			</th>
	    </tr>
	    <tr>
	    	<th width='3%'>#</th>
			<th width='12%;'>Nombre de Material</th>
			<th width='13%'>Almacen</th>
			<th width='12%'>Usuario</th>
			<th width='12%'>Solicitante</th>
			<th width='12%'>fecha</th>  
			<th width='5%;'>Stock</th>
			<th width='5%;'>Ingreso</th>
			<th width='5%;'>Salida</th>
			<th width='5%;'>Saldo</th>
			<th width='16%;'>Observaciones</th>
	    </tr>
    </thead>
<tbody>
<?php
if(count($movimientos)<=0){ 
	echo "<tr><td colspan='11'><h3>0 registros encontrados...</h3></td></tr>";
}else{?>
    <tbody>
<?php for ($i=0; $i < count($movimientos) ; $i++){ $movimiento=$movimientos[$i]; ?>
    	<tr 	>
    		<td><?php echo $i+1;?></td>
			<td><?php echo $movimiento->nombre;?></td>
			<td><?php echo $movimiento->almacen;?></td>
			<td><?php echo $movimiento->usuario;?></td>
			<td><?php echo $movimiento->solicitante;?></td>
			<td><?php echo $this->validaciones->formato_fecha($movimiento->fecha,'Y,m,d')." ".$movimiento->hora;?></td> 
			<td><?php echo $movimiento->cantidad;?></td>
			<td><?php echo $movimiento->ingreso;?></td>
			<td><?php echo $movimiento->salida;?></td>
			<td><?php echo ($movimiento->cantidad+$movimiento->ingreso-$movimiento->salida);?></td>
			<td><?php echo $movimiento->observaciones;?></td>
    	</tr>
    	<?php
    	}//end else
    }//end ifelse
    ?>
    </tbody>
</table>
<script language="javascript">
	$("#print").removeAttr("onclick");
	$("#print").unbind("click");
	$("#print").click(function(){ imprimir_almacen_hm('<?php echo $j_movimientos;?>'); }); 
	$("#bh").click(function(){ confirmar_hitorial_material(); }); 
</script>