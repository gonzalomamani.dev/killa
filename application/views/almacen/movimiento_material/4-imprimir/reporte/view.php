<?php 
//print_r($movimientos);
	$movimientos=json_decode($movimientos);
	$contPagina=1;
	$contReg=0;
?>
<div class="pagina">
	<?php $this->load->view('estructura/print/encabezado',['titulo'=>'MOVIMIENTO DE INGRESOS Y SALIDAS','pagina'=>$contPagina,'sub_titulo'=> 'ALMACENES DE MATERIALES']);?>
	<div class="tabla tabla-border-true">
		<div class="fila">
				<?php if(!isset($v1)){?><div class="celda th" style="width:3% ">#</div><?php } ?>
				<?php if(!isset($v2)){?><div class="celda th" style="width:10%">Nombre Material</div><?php } ?>
				<?php if(!isset($v3)){?><div class="celda th" style="width:16%">Almacen</div><?php } ?>
				<?php if(!isset($v4)){?><div class="celda th" style="width:15%">Usuario</div><?php } ?>
				<?php if(!isset($v5)){?><div class="celda th" style="width:15%">Solicitante</div><?php } ?>
				<?php if(!isset($v6)){?><div class="celda th" style="width:5% ">Hora</div><?php } ?>
				<?php if(!isset($v7)){?><div class="celda th" style="width:8% ">Fecha</div><?php } ?>
				<?php if(!isset($v8)){?><div class="celda th" style="width:4% ">Stock</div><?php } ?>
				<?php if(!isset($v9)){?><div class="celda th" style="width:4% ">Ingreso</div><?php } ?>
				<?php if(!isset($v10)){?><div class="celda th" style="width:4% ">Salida</div><?php } ?>
				<?php if(!isset($v11)){?><div class="celda th" style="width:4% ">Saldo</div><?php } ?>
				<?php if(!isset($v12)){?><div class="celda th" style="width:12% ">Observaciónes</div><?php } ?>
		</div>
	<?php $cont=1;  foreach ($movimientos as $key => $movimiento) { 
			if($contReg>=$nro){
			$contReg=0;
			$contPagina++;
		?>
			</div><!--cerramos tabla-->
		</div><!--cerramos pagina-->
	<div class="pagina">
	<?php $this->load->view('estructura/print/encabezado',['titulo'=>'MOVIMIENTO DE INGRESOS Y SALIDAS','pagina'=>$contPagina,'sub_titulo'=> 'ALMACENES DE MATERIALES']);?>
	<div class="tabla tabla-border-true">
		<div class="fila">
				<?php if(!isset($v1)){?><div class="celda th" style="width:3% ">#</div><?php } ?>
				<?php if(!isset($v2)){?><div class="celda th" style="width:10%">Nombre Material</div><?php } ?>
				<?php if(!isset($v3)){?><div class="celda th" style="width:16%">Almacen</div><?php } ?>
				<?php if(!isset($v4)){?><div class="celda th" style="width:15%">Usuario</div><?php } ?>
				<?php if(!isset($v5)){?><div class="celda th" style="width:15%">Solicitante</div><?php } ?>
				<?php if(!isset($v6)){?><div class="celda th" style="width:5% ">Hora</div><?php } ?>
				<?php if(!isset($v7)){?><div class="celda th" style="width:8% ">Fecha</div><?php } ?>
				<?php if(!isset($v8)){?><div class="celda th" style="width:4% ">Stock</div><?php } ?>
				<?php if(!isset($v9)){?><div class="celda th" style="width:4% ">Ingreso</div><?php } ?>
				<?php if(!isset($v10)){?><div class="celda th" style="width:4% ">Salida</div><?php } ?>
				<?php if(!isset($v11)){?><div class="celda th" style="width:4% ">Saldo</div><?php } ?>
				<?php if(!isset($v12)){?><div class="celda th" style="width:12% ">Observaciónes</div><?php } ?>
		</div>
		<?php } ?>
		<div class="fila">
			<?php if(!isset($v1)){?><div class="celda td"><?php echo $cont++;?></div><?php } ?>
			<?php if(!isset($v3)){?><div class="celda td"><?php echo $movimiento->nombre;?></div><?php } ?>
			<?php if(!isset($v3)){?><div class="celda td"><?php echo $movimiento->almacen;?></div><?php } ?>
			<?php if(!isset($v4)){?><div class="celda td"><?php echo $movimiento->usuario;?></div><?php } ?>
			<?php if(!isset($v5)){?><div class="celda td"><?php echo $movimiento->solicitante;?></div><?php } ?>
			<?php if(!isset($v6)){?><div class="celda td"><?php echo $movimiento->hora;?></div><?php } ?>
			<?php if(!isset($v7)){?><div class="celda td"><?php echo $movimiento->fecha;?></div><?php } ?>
			<?php if(!isset($v8)){?><div class="celda td"><?php echo $movimiento->cantidad;?></div><?php } ?>
			<?php if(!isset($v9)){?><div class="celda td"><?php echo $movimiento->ingreso;?></div><?php } ?>
			<?php if(!isset($v10)){?><div class="celda td"><?php echo $movimiento->salida;?></div><?php } ?>
			<?php if(!isset($v11)){?><div class="celda td"><?php echo $movimiento->cantidad+$movimiento->ingreso-$movimiento->salida;?></div><?php } ?>
			<?php if(!isset($v12)){?><div class="celda td"><?php echo $movimiento->observaciones;?></div><?php } ?>
		</div>
	<?php  $contReg++; } ?>
	</div>
</div>
<script type="text/javascript"> $(".nroPagina").html(<?php echo $contPagina;?>); </script>