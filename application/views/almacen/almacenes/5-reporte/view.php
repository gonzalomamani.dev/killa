<?php $almacen=$almacen[0];
	$url=base_url().'libraries/img/almacenes/miniatura/';
	$img='default.png';
	if($almacen->fotografia!='' && $almacen->fotografia!=NULL){$img=$almacen->fotografia;}
?>
<div class="row">
	<div class="col-sm-3 col-xs-12 text-center"><img src="<?php echo $url.$img;?>" class="img-thumbnail" alt=""><hr></div>
	<div class="col-sm-9 cols-xs-12 table-responsive">
		<table border="0" class="tabla tabla-border-true">
			<tr class="fila">
				<th class="celda th">Código:</th>
				<td class="celda td" colspan="3"><?php echo $almacen->codigo;?></td>
			</tr>
			<tr class="fila">
				<th class="celda th">Nombre:</th><td class="celda td" colspan="3"><?php echo $almacen->nombre;?></td>
			</tr>
			<tr class="fila">
				<th class="celda th">Tipo de Almacen:</th><td class="celda td" colspan="3">
					<?php if($almacen->tipo=='material'){?>
						Almacen de Materiales
					<?php }else{?>
						Almacen de Productos Acabados
					<?php }?>
				</td>
			</tr>
			<tr class="fila">
				<th class="celda th">Descripción:</th><td class="celda td" colspan="3"><?php echo $almacen->descripcion;?></td>
			</tr>
		</table>
	</div>
</div>