<?php $url=base_url().'libraries/img/almacenes/';
$j_almacen=json_encode($almacenes); ?>
<table class="table table-bordered table-hover table-striped">
	<thead>
		<tr>
			<th class="g-thumbnail">#Item</th>
			<th class="celda-sm-10">Código</th>
			<th class="celda-sm-30">Nombre</th>
			<th style="width:15%" class="celda-sm">Tipo</th>
			<th style="width:12%" class="celda-sm">Cantidad</th>
			<th style="width:25%" class="celda-sm">Descripción</th>
			<th style="width:8%"></th>
		</tr>
	</thead>
<?php if (count($almacenes)>0) { ?>
	<tbody>
	<?php
	 for($i=0;$i<count($almacenes);$i++){ $almacen=$almacenes[$i]; $img="default.png"; if($almacen->fotografia!=NULL && $almacen->fotografia!=""){$img=$almacen->fotografia;} ?>
		<tr>
			<td class="g-thumbnail"><div id="item"><?php echo $i+1;?></div><div class="g-img"><a href="javascript:" onclick="fotografia('<?php echo $url.$img;?>')"><img src="<?php echo $url.'miniatura/'.$img;?>" width="100%" class="img-thumbnail"></a></div></td>
			<td><?php echo $almacen->codigo;?></td>
			<td><a href="<?php echo base_url().'almacen/'.$almacen->tipo.'/'.$almacen->ida?>"><?php echo $almacen->nombre;?></a></td>
			<td class="celda-sm"><?php echo "Almacen de ".$almacen->tipo;?></td>
			<td class="celda-sm">
			<?php if($almacen->tipo=="material"){ $aux=$this->M_almacen_material->get_row('ida',$almacen->ida);$text="material(es)";}
					else{ $aux=$this->M_almacen_producto->get_row('ida',$almacen->ida);$text="producto(s)";}
			?>
			<span class="label <?php if(count($aux)>0){ echo 'label-success';}else{ echo 'label-danger';}?> label-lg"><?php echo count($aux)." $text registrado(s)"; ?></span></td>
			<td class="celda-sm"><?php echo $almacen->descripcion;?></td>
			<td class="text-right">
				<?php $mod="configuracion_almacen('".$almacen->ida."')"; if($privilegio[0]->al1u!="1"){ $mod="";}?>
				<?php $del="confirmar('".$almacen->ida."')"; if($privilegio[0]->al1d!="1"){ $del="";}?>
				<?php $this->load->view("estructura/botones/botones_registros",["reportes"=>"reportes_almacen('".$almacen->ida."')",'configuracion'=>$mod,'eliminar'=>$del]);?>
			</td>
		</tr>				
	<?php }?>
	</tbody>
<?php }else{ echo "<tr><td colspan='6'><h2>0 registros encontrados...</h2></td></tr>"; } ?>
</table>
<script language="javascript">
	$("#print").removeAttr("onclick");
	$("#print").unbind("click");
	$("#print").click(function(){ imprimir_almacen('<?php echo $j_almacen;?>'); });
</script><br>
