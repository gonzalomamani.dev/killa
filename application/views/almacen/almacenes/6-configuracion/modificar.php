<?php 
	$help1="Seleccione una fotografía, preferiblemente una imagen que no sobrepase las dimenciones de <strong>700x700[px]</strong>, para evitar sobre cargar al sistema";
	$popover1='data-toggle="popover" data-placement="left" data-trigger="hover" title="<h4>Subir Fotografía</h4>" data-content="'.$help1.'"';
	$help2="Ingrese un Código alfanumerico de 2 a 10 caracteres <b>sin espacios</b>, ademas el codigo solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>";
	$popover2='data-toggle="popover" data-placement="left" data-trigger="hover" title="<h4>Ingresar codigo de almacen<h4>" data-content="'.$help2.'"';
	$help3="Ingrese un Nombre alfanumerico de 3 a 100 caracteres puede incluir espacios, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>";
	$popover3='data-toggle="popover" data-placement="left" data-trigger="hover" title="<h4>Ingresar nombre de almacen<h4>" data-content="'.$help3.'"';
	$help4="Imposible cambiar el tipo una vez creado no pude ser modificado por motivos de seguridad.";
	$popover4='data-toggle="popover" data-placement="left" data-trigger="hover" title="<h4>Seleccione un tipo de almacen<h4>" data-content="'.$help4.'"';
	$help5="Ingrese un descripcion de almacen alfanumerica de 0 a 400 caracteres puede incluir espacios <b>sin saltos de linea</b>, ademas la descripcion solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº)<b>";
	$popover5='data-toggle="popover" data-placement="left" data-trigger="hover" title="<h4>Descripcion de almacen<h4>" data-content="'.$help5.'"';
	$almacen=$almacen[0];
?>
<div class="row">
	<div class="col-sm-3 col-sm-offset-9 col-xs-12"><strong><span class='text-danger'>(*)</span> Campo obligatorio</strong></div>
	<div class="col-sm-2 col-xs-12"><strong>Fotografía: </strong></div>
	<div class="col-sm-10 col-xs-12">
		<div class="input-group input-group-xs">
			<input type="file" name="img" id="archivo" class="form-control input-xs">
			<span class="input-group-addon input-sm" <?php echo $popover1;?>><span class='glyphicon glyphicon-info-sign'></span></span>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Código:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<form onsubmit="return update_almacen('<?php echo $id;?>')">
			<div class="input-group input-group-xs">
				<input type="text" placeholder='Código' maxlength="10" id="cod" class="form-control input-xs" value="<?php echo $almacen->codigo;?>">
				<span class="input-group-addon input-sm" <?php echo $popover2;?>><span class='glyphicon glyphicon-info-sign'></span></span>
			</div>
		</form>
	</div>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Nombre:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<form onsubmit="return update_almacen('<?php echo $id;?>')">
			<div class="input-group input-group-xs">
				<input type="text" placeholder='Nombre de Almacen' id="nom" maxlength="100" class="form-control input-xs" value="<?php echo $almacen->nombre;?>">
				<span class="input-group-addon input-sm" <?php echo $popover3;?>><span class='glyphicon glyphicon-info-sign'></span></span>
			</div>
		</form>
	</div>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Tipo: </strong></div>
	<div class="col-sm-10 col-xs-12">
		<div class="input-group input-group-xs">
			<select id="tip" class="form-control input-xs" disabled="">
				<option value="">Seleccione...</option>
				<option value="material" <?php if($almacen->tipo=='material'){ echo "selected";}?>>Almacen de Materiales</option>
				<option value="producto" <?php if($almacen->tipo=='producto'){ echo "selected";}?>>Almacen de Productos Acabados</option>
			</select>
			<span class="input-group-addon input-sm" <?php echo $popover4;?>><span class='glyphicon glyphicon-info-sign'></span></span>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12"><strong>Descripción</strong></div>
	<div class="col-sm-10 col-xs-12">
		<div class="input-group input-group-xs">
			<textarea id="des" placeholder='Descripción del almacen' maxlength="300" class="form-control input-xs"><?php echo $almacen->descripcion;?></textarea>
			<span class="input-group-addon input-sm" <?php echo $popover5;?>><span class='glyphicon glyphicon-info-sign'></span></span>
		</div>
	</div>	
</div>
<script language='javascript'>Onfocus("cod");$('[data-toggle="popover"]').popover({html:true});</script>