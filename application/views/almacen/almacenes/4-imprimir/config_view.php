<div class="row-border table-responsive">
	<table class="table table-bordered"><thead>
		<tr>
			
			<th width="15%">Nro. Reg. por hojas</th>
			<th width="20%">
				<select id="nro" class="form-control input-sm" style="width:100%">
				<?php for ($i=1; $i <= 80 ; $i++) { ?>
					<option value="<?php echo $i;?>" <?php if($i==35){echo "selected";}?>><?php echo $i;?></option>
				<?php } ?>
				</select>
			</th>
			<th width="65%"></th>
		</tr>
	</thead></table>
	<div id="config_area">
		<table class="tabla tabla-border-true">
			<tr class="fila">
				<th class="celda th">#Item</th>
				<th class="celda th">Fotografía</th>
				<th class="celda th">Código</th>
				<th class="celda th">Nombre</th>
				<th class="celda th">Tipo</th>
				<th class="celda th">Observaciónes</th>
			</tr>
			<tr class="fila">
				<th class="celda th" width="7%"><input type="checkbox" id="1" checked="checked"></th>
				<th class="celda th" width="7%"><input type="checkbox" id="2" checked="checked"></th>
				<th class="celda th" width="10%"><input type="checkbox" id="3" checked="checked"></th>
				<th class="celda th" width="36%"><input type="checkbox" id="4" checked="checked"></th>
				<th class="celda th" width="10%"><input type="checkbox" id="5" checked="checked"></th>
				<th class="celda th" width="30%"><input type="checkbox" id="6" checked="checked"></th>
			</tr>
		</table>
	</div>
</div>
<div class="row-border" style="overflow:auto">
	<div id="area"></div>
</div>
<script language="javascript">
	$("#1").unbind("click");$("#1").change(function(){ arma_informe('<?php echo $almacenes;?>'); }); 
	$("#2").unbind("click");$("#2").change(function(){ arma_informe('<?php echo $almacenes;?>'); }); 
	$("#3").unbind("click");$("#3").change(function(){ arma_informe('<?php echo $almacenes;?>'); }); 
	$("#4").unbind("click");$("#4").change(function(){ arma_informe('<?php echo $almacenes;?>'); }); 
	$("#5").unbind("click");$("#5").change(function(){ arma_informe('<?php echo $almacenes;?>'); }); 
	$("#6").unbind("click");$("#6").change(function(){ arma_informe('<?php echo $almacenes;?>'); });
	$("#nro").unbind("click");$("#nro").change(function(){ arma_informe('<?php echo $almacenes;?>'); }); 
</script>