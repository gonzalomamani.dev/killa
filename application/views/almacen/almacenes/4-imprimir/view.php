<?php 
	$almacenes=json_decode($almacenes);
	$contPagina=1;
	$contReg=0;
	$url=base_url().'libraries/img/almacenes/miniatura/';
?>
<div class="pagina">
	<?php $this->load->view('estructura/print/encabezado',['titulo'=>'REPORTE DE ALMACENES','pagina'=>$contPagina,'sub_titulo'=> '']);?>
	<div class="tabla tabla-border-true">
		<div class="fila">
			<?php if(!isset($ite)){?><div class="celda th" style='width: 7%' >#Item</div><?php } ?>
			<?php if(!isset($fot)){?><div class="celda th" style='width: 7%' >Fotografía</div><?php } ?>
			<?php if(!isset($cod)){?><div class="celda th" style='width: 10%'>Código</div><?php } ?>
			<?php if(!isset($nom)){?><div class="celda th" style='width: 36%'>Nombre</div><?php } ?>
			<?php if(!isset($tip)){?><div class="celda th" style='width: 10%'>Tipo de almacen</div><?php } ?>
			<?php if(!isset($des)){?><div class="celda th" style='width: 30%'>Descripción</div><?php } ?>
		</div>
	<?php $cont=0; 
			foreach ($almacenes as $key => $almacen) { 
				$img='default.png';
				if($almacen->fotografia!=NULL && $almacen->fotografia!=""){ $img=$almacen->fotografia;}
		?>
			<?php 
				if($contReg>=$nro){
					$contReg=0;
					$contPagina++;

			?>
		</div><!--cerramos tabla-->
	</div><!--cerramos pagina-->
<div class="pagina">
	<?php $this->load->view('estructura/print/encabezado',['titulo'=>'REPORTE DE ALMACENES','pagina'=>$contPagina,'sub_titulo'=> '']);?>
	<div class="tabla tabla-border-true">
			<div class="fila">
			<?php if(!isset($ite)){?><div class="celda th" style='width: 7%' >#Item</div><?php } ?>
			<?php if(!isset($fot)){?><div class="celda th" style='width: 7%' >Fotografía</div><?php } ?>
			<?php if(!isset($cod)){?><div class="celda th" style='width: 10%'>Código</div><?php } ?>
			<?php if(!isset($nom)){?><div class="celda th" style='width: 36%'>Nombre</div><?php } ?>
			<?php if(!isset($tip)){?><div class="celda th" style='width: 10%'>Tipo de almacen</div><?php } ?>
			<?php if(!isset($des)){?><div class="celda th" style='width: 30%'>Descripción</div><?php } ?>
		</div>
		<?php } ?>
		<div class="fila">
			<?php if(!isset($ite)){?><div class="celda td"><?php echo $cont+1;$cont++;?></div><?php } ?>
			<?php if(!isset($fot)){?><div class="celda td"><img src="<?php echo $url.$img;?>" style='width:100%'></div><?php } ?>
			<?php if(!isset($cod)){?><div class="celda td"><?php echo $almacen->codigo;?></div><?php } ?>
			<?php if(!isset($nom)){?><div class="celda td"><?php echo $almacen->nombre;?></div><?php } ?>
			<?php if(!isset($tip)){?><div class="celda td"><?php echo $almacen->tipo;?></div><?php } ?>
			<?php if(!isset($des)){?><div class="celda td"><?php echo $almacen->descripcion;?></div><?php } ?>
		</div>
	<?php $contReg++; }// end for ?>
	</div>
</div>
<script type="text/javascript"> $(".nroPagina").html(<?php echo $contPagina;?>); </script>