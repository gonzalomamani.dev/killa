<table class="tabla tabla-border-false">
	<tr class="fila">
		<td class='celda g-thumbnail'><div class="g-img"></div></td>
		<td class='celda celda-sm-10'><form onsubmit="return view_almacen()"><input type="search" id="search_cod" class="form-control input-sm" maxlength="10" placeholder='Código' onkeyup="reset_all_view(this.id)" ></form></td>
		<td class='celda celda-sm-30'><form onsubmit="return view_almacen()"><input type="search" id="search_nom" class="form-control input-sm" maxlength="100" placeholder='Nombre de Almacen' onkeyup="reset_all_view(this.id)"/></form></td>
		<td class='celda-sm' style='width:15%'>
			<select id="search_tipo" class="form-control input-sm" onchange="reset_all_view(this.id); view_almacen()">
				<option value=''>Seleccionar...</option>	
				<option value='material'>Materia Prima</option>
				<option value='producto'>Productos Acabados</option>
			</select>
		</td>
		<td class='celda-sm' style='width:33%'><div style=""></div></td>
		<td class='celda text-right' style='width:12%'>
			<?php $new="view_form_new_almacen()"; if($privilegio[0]->al1c!="1"){ $new="";}?>
			<?php $print="print()"; if($privilegio[0]->al1p!="1"){ $print="";}?>
			<?php $this->load->view('estructura/botones/buscador',['f_buscar'=>"view_almacen()",'f_ver'=>"all_almacen()",'nuevo'=>'Nuevo','f_nuevo'=>$new,'f_imprimir'=>$print]);?>
		</td>		
	</tr>
</table>
<script type="text/javascript">Onfocus('search_cod');</script>