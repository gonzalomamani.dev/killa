<?php 
	$help1='title="<h4>Posición<h4>" data-content="Representa al orden en el que se generar los comprobantes, de las compras o egresos."';
	$help2='title="<h4>Cuenta<h4>" data-content="Seleccione las cuentas que seran usadas en el comprobante, la opcion <b>CUENTA DE EGRESO</b> es fijo pues toma la cuenta seleccionada en el registro de compra o egreso antes de generar el comprobante."';
	$popover='data-toggle="popover" data-placement="bottom" data-trigger="hover"';
	$popover2='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$popover3='data-toggle="popover" data-placement="top" data-trigger="hover"';
	if (count($usuarios)>0) {  $url=base_url().'libraries/img/personas/miniatura/'; 
?>
<table width="100%" class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th class="g-thumbnail">#Item</th>
			<th class="-10">NIT/CI</th>
			<th class="-30">Nombre completo</th>
			<th width="6.666666667%">Alm.</th>
			<th width="6.666666667%">Prod.</th>
			<th width="6.666666667%">Mov.</th>
			<th width="6.666666667%">Cap. Hum.</th>
			<th width="6.666666667%">Clie. Prov.</th>
			<th width="6.666666667%">Act. Fijo</th>
			<th width="6.666666667%">Otr. Mat. Ins.</th>
			<th width="6.666666667%">Contab.</th>
			<th width="6.666666667%">Admin.</th>
		</tr>
	</thead>
	<tbody>
	<?php for ($j=0; $j < count($usuarios); $j++) { $usuario=$usuarios[$j]; 
			$img="default.png";
			if($usuario->fotografia!='' && $usuario->fotografia!=NULL){ $img=$usuario->fotografia; } ?>
		<tr>
		<td class="g-thumbnail"><div id="item"><?php echo $j+1;?></div><div class="g-img"><img src="<?php echo $url.$img;?>" class="img-thumbnail g-thumbnail"></div></td>
			<td><?php echo $usuario->ci.' '.$usuario->abreviatura;?></td>
			<td><?php echo $usuario->nombre." ".$usuario->nombre2." ".$usuario->paterno." ".$usuario->materno; ?></td>
	<?php $privilegio=$this->M_privilegio->get_row('ide',$usuario->ide); 
			if(empty($privilegio)){
				if($this->M_privilegio->insertar($usuario->ide)){
					$privilegio=$this->M_privilegio->get_row('ide',$usuario->ide);
				}
			}
	?>
			<td><center>
				<?php if($privilegio[0]->al==1){ ?>
					<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio[0]->idpri;?>','al',this,'info','30');"></div>
				<?php }else{ ?>
					<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio[0]->idpri;?>','al',this,'info','30');"></div>
				<?php } ?>
				<a href="javascript:" onclick="detail_modal('<?php echo $privilegio[0]->idpri;?>','al')">Detalle</a>
			</center></td>
			<td><center>
				<?php if($privilegio[0]->pr==1){ ?>
					<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio[0]->idpri;?>','pr',this,'success','30');"></div>
				<?php }else{ ?>
					<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio[0]->idpri;?>','pr',this,'success','30');"></div>
				<?php } ?>
			<a href="javascript:" onclick="detail_modal('<?php echo $privilegio[0]->idpri;?>','pr')">Detalle</a>
			</center></td>
			<td><center>
				<?php if($privilegio[0]->mo==1){ ?>
					<div class="btn-circle btn-circle-red btn-circle-30" onclick="update_privilegio('<?php echo $privilegio[0]->idpri;?>','mo',this,'red','30');"></div>
				<?php }else{ ?>
					<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio[0]->idpri;?>','mo',this,'red','30');"></div>
				<?php } ?>
			<a href="javascript:" onclick="detail_modal('<?php echo $privilegio[0]->idpri;?>','mo')">Detalle</a>
			</center></td>
			<td><center>
				<?php if($privilegio[0]->ca==1){ ?>
					<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio[0]->idpri;?>','ca',this,'info','30');"></div>
				<?php }else{ ?>
					<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio[0]->idpri;?>','ca',this,'info','30');"></div>
				<?php } ?>
			<a href="javascript:" onclick="detail_modal('<?php echo $privilegio[0]->idpri;?>','ca')">Detalle</a>
			</center></td>
			<td><center>
				<?php if($privilegio[0]->cl==1){ ?>
					<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio[0]->idpri;?>','cl',this,'success','30');"></div>
				<?php }else{ ?>
					<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio[0]->idpri;?>','cl',this,'success','30');"></div>
				<?php } ?>
			<a href="javascript:" onclick="detail_modal('<?php echo $privilegio[0]->idpri;?>','cl')">Detalle</a>
			</center></td>
			<td><center>
				<?php if($privilegio[0]->ac==1){ ?>
					<div class="btn-circle btn-circle-red btn-circle-30" onclick="update_privilegio('<?php echo $privilegio[0]->idpri;?>','ac',this,'red','30');"></div>
				<?php }else{ ?>
					<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio[0]->idpri;?>','ac',this,'red','30');"></div>
				<?php } ?>
			<a href="javascript:" onclick="detail_modal('<?php echo $privilegio[0]->idpri;?>','ac')">Detalle</a>
			</center></td>
			<td><center>
				<?php if($privilegio[0]->ot==1){ ?>
					<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio[0]->idpri;?>','ot',this,'info','30');"></div>
				<?php }else{ ?>
					<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio[0]->idpri;?>','ot',this,'info','30');"></div>
				<?php } ?>
			<a href="javascript:" onclick="detail_modal('<?php echo $privilegio[0]->idpri;?>','ot')">Detalle</a>
			</center></td>
			<td><center>
				<?php if($privilegio[0]->co==1){ ?>
					<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio[0]->idpri;?>','co',this,'success','30');"></div>
				<?php }else{ ?>
					<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio[0]->idpri;?>','co',this,'success','30');"></div>
				<?php } ?>
			<a href="javascript:" onclick="detail_modal('<?php echo $privilegio[0]->idpri;?>','co')">Detalle</a>
			</center></td>
			<td><center>
				<?php if($privilegio[0]->ad==1){ ?>
					<div class="btn-circle btn-circle-red btn-circle-30" onclick="update_privilegio('<?php echo $privilegio[0]->idpri;?>','ad',this,'red','30');"></div>
				<?php }else{ ?>
					<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio[0]->idpri;?>','ad',this,'red','30');"></div>
				<?php } ?>
			<a href="javascript:" onclick="detail_modal('<?php echo $privilegio[0]->idpri;?>','ad')">Detalle</a>
			</center></td>
		</tr>
	<?php }?>
	</tbody>
</table>
<?php } else{ ?>
<h3>No existen registros</h3>
<?php }?>
<script language='javascript'>$('[data-toggle="popover"]').popover({html:true});</script>