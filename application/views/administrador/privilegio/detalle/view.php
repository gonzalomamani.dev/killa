<?php switch ($col) {
	case 'al': 
?>
<div class="row">
	<div class="col-sm-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="5" class="text-center"><ins>ALMACENES</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->al1r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','al1r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','al1r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al1c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','al1c',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','al1c',this,'success','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al1p==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','al1p',this,'red','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','al1p',this,'red','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al1u==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','al1u',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','al1u',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al1d==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','al1d',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','al1d',this,'success','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-sm-3 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="3" class="text-center"><ins>MOV. DE MATERIALES</ins></th></tr>
					<tr><th>Ver</th><th>Imprimir</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->al2r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','al2r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','al2r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al2p==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','al2p',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','al2p',this,'success','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al2d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','al2d',this,'red','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','al2d',this,'red','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-3 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="3" class="text-center"><ins>MOV. DE PRODUCTOS</ins></th></tr>
					<tr><th>Ver</th><th>Imprimir</th><th>Eliminar</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->al3r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','al3r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','al3r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al3p==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','al3p',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','al3p',this,'success','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al3d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','al3d',this,'red','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','al3d',this,'red','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
		break;
	case 'pr':
?>
<div class="row">
	<div class="col-sm-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="5" class="text-center"><ins>PRODUCTOS</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->pr1r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','pr1r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','pr1r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->pr1c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','pr1c',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','pr1c',this,'success','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->pr1p==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','pr1p',this,'red','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','pr1p',this,'red','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->pr1u==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','pr1u',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','pr1u',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->pr1d==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','pr1d',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','pr1d',this,'success','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-sm-3 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="3" class="text-center"><ins>ORDEN DE PRODUCCIÓN</ins></th></tr>
					<tr><th>Ver</th><th>Modificar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->pr2r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','pr2r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','pr2r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->pr2u==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','pr2u',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','pr2u',this,'success','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-3 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="3" class="text-center"><ins>CONFIGURACIÓN</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->pr3r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','pr3r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','pr3r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
		break;
	case 'mo':
?>
<div class="row">
	<div class="col-sm-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="6" class="text-center"><ins>PEDIDOS</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th><th>Adelanto</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->mo1r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo1r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo1r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo1c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo1c',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo1c',this,'success','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo1p==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo1p',this,'red','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo1p',this,'red','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo1u==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo1u',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo1u',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo1d==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo1d',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo1d',this,'success','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo1a==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo1a',this,'red','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo1a',this,'red','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-sm-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="6" class="text-center"><ins>VENTAS</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->mo2r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo2r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo2r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo2c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo2c',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo2c',this,'success','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo2p==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo2p',this,'red','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo2p',this,'red','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo2u==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo2u',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo2u',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo2d==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo2d',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo2d',this,'success','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="6" class="text-center"><ins>COMPRAS Y/O EGRESOS</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->mo3r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo3r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo3r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo3c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo3c',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo3c',this,'success','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo3p==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo3p',this,'red','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo3p',this,'red','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo3u==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo3u',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo3u',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo3d==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo3d',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','mo3d',this,'success','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
		break;
	case 'ca':
?>
<div class="row">
	<div class="col-sm-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="5" class="text-center"><ins>CAPITAL HUMANO</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->ca1r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ca1r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ca1r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ca1c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ca1c',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ca1c',this,'success','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ca1p==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ca1p',this,'red','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ca1p',this,'red','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ca1u==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ca1u',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ca1u',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ca1d==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ca1d',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ca1d',this,'success','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-sm-4 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="4" class="text-center"><ins>ORDEN DE PRODUCCIÓN</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->ca2r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ca2r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ca2r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ca2c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ca2c',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ca2c',this,'success','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ca2p==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ca2p',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ca2p',this,'success','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ca2u==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ca2u',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ca2u',this,'info','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th class="text-center"><ins>CONFIG</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->ca3r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ca3r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ca3r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
		break;
	case 'cl':
?>
<div class="row">
	<div class="col-sm-5 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="5" class="text-center"><ins>CLIENTES</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->cl1r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','cl1r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','cl1r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->cl1c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','cl1c',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','cl1c',this,'success','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->cl1p==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','cl1p',this,'red','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','cl1p',this,'red','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->cl1u==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','cl1u',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','cl1u',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->cl1d==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','cl1d',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','cl1d',this,'success','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-sm-5 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="5" class="text-center"><ins>PROVEEDORES</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->cl2r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','cl2r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','cl2r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->cl2c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','cl2c',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','cl2c',this,'success','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->cl2p==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','cl2p',this,'red','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','cl2p',this,'red','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->cl2u==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','cl2u',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','cl2u',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->cl2d==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','cl2d',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','cl2d',this,'success','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-sm-2 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th class="text-center"><ins>CONFIG</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->cl3r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','cl3r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','cl3r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
		break;
	case 'ac':
?>
<div class="row">
	<div class="col-sm-5 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="5" class="text-center"><ins>ACTIVOS FIJOS</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->ac1r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ac1r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ac1r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ac1c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ac1c',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ac1c',this,'success','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ac1p==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ac1p',this,'red','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ac1p',this,'red','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ac1u==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ac1u',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ac1u',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ac1d==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ac1d',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ac1d',this,'success','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-sm-3 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th class="text-center"><ins>CONFIGURACION</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->ac2r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ac2r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ac2r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
		break;
	case 'ot':
?>
<div class="row">
	<div class="col-sm-5 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="5" class="text-center"><ins>MATERIALES ADICIONALES</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->ot1r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ot1r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ot1r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ot1c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ot1c',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ot1c',this,'success','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ot1p==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ot1p',this,'red','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ot1p',this,'red','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ot1u==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ot1u',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ot1u',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ot1d==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ot1d',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ot1d',this,'success','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-sm-5 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="6" class="text-center"><ins>OTROS MATERIALES E INSUMOS</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->ot2r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ot2r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ot2r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ot2c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ot2c',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ot2c',this,'success','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ot2p==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ot2p',this,'red','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ot2p',this,'red','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ot2u==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ot2u',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ot2u',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ot2d==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ot2d',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ot2d',this,'success','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th class="text-center"><ins>CONFIG</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->ot3r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ot3r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ot3r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
		break;
	case 'co':
?>
<div class="row">
	<div class="col-sm-5 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="5" class="text-center"><ins>COMPROBANTES</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->co1r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co1r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co1r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->co1c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co1c',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co1c',this,'success','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->co1p==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co1p',this,'red','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co1p',this,'red','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->co1u==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co1u',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co1u',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->co1d==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co1d',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co1d',this,'success','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-sm-2 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th class="text-center"><ins>L. MAYOR</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->co2r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co2r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co2r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-3 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th class="text-center"><ins>SUMAS SALDOS</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->co3r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co3r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co3r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th class="text-center"><ins>E. CPV.</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->co4r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co4r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co4r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th class="text-center"><ins>E. RESUL.</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->co5r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co5r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co5r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th class="text-center"><ins>BALANCE</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->co6r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co6r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co6r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-5 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="5" class="text-center"><ins>PLAN DE CUENTAS</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->co7r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co7r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co7r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->co7c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co7c',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co7c',this,'success','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->co7p==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co7p',this,'red','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co7p',this,'red','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->co7u==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co7u',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co7u',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->co7d==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co7d',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co7d',this,'success','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-sm-2 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th class="text-center"><ins>CONFIG</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->co8r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co8r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','co8r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
		break;
	case 'ad':
?>
<div class="row">
	<div class="col-sm-5 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="5" class="text-center"><ins>USUARIOS</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->ad1r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ad1r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ad1r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ad1c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ad1c',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ad1c',this,'success','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ad1p==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ad1p',this,'red','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ad1p',this,'red','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ad1u==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ad1u',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ad1u',this,'info','30');"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ad1d==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ad1d',this,'success','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ad1d',this,'success','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-3 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th class="text-center"><ins>PRIVILEGIOS</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->ad2r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ad2r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ad2r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th class="text-center"><ins>CONFIG</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->ad3r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ad3r',this,'info','30');"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="update_privilegio('<?php echo $privilegio->idpri;?>','ad3r',this,'info','30');"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
		break;
}?>