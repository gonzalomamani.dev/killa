<?php $v = array('1' => 'Administrador', '0' => 'Usuarios Simple' );?>
<table class="tabla">
	<tr class="fila">
		<td class='celda celda-sm g-thumbnail'><div class="g-img"></div></td>
		<td class='celda celda-sm-10'><form onsubmit="return view_usuarios()"><input type="text" class="form-control input-sm" placeholder="CI" id="s_ci" value="" onkeyup="reset_all_view(this.id)"></form></td>
		<td class='celda celda-sm-30'><form onsubmit="return view_usuarios()"><input type="text" class="form-control input-sm" placeholder="Nombre completo" id="s_nom" value="" onkeyup="reset_all_view(this.id)"></form></td>
		<td class='celda celda-sm' width="15%">
		<select class="form-control input-sm" id="s_tip" onchange="reset_all_view(this.id); view_usuarios()" onclick="reset_all_view(this.id)">
			<option value="">Seleccionar...</option>
		<?php foreach ($v as $key => $value) { ?>
			<option value="<?php echo $key;?>"><?php echo $value; ?></option>
		<?php } ?>
		</select>
		<td class='celda celda-sm' width="15%"><form onsubmit="return view_usuarios()"><input type="text" class="form-control input-sm" placeholder="Cargo" id="s_car" value="" onkeyup="reset_all_view(this.id)"></form></td>
		<td class='celda celda-sm' width="17%"><form onsubmit="return view_usuarios()"><input type="text" class="form-control input-sm" placeholder="Nombre de usuario" id="s_usu" value="" onkeyup="reset_all_view(this.id)"></form></td>
		<td class='celda' width="13%">
			<?php $new='new_usuario()'; if($privilegio[0]->ad1c!="1"){ $new="";}?>
			<?php $print="print()"; if($privilegio[0]->ad1p!="1"){ $print="";}?>
			<?php $this->load->view('estructura/botones/buscador',['f_buscar'=>"view_usuarios()",'f_ver'=>"all_usuarios()",'nuevo'=>'Nuevo','f_nuevo'=>$new,'f_imprimir'=>$print]);?>
		</td>
	</tr>
</table>