<?php 
	$usuarios=json_decode($usuarios);
	$contPagina=1;
	$contReg=0;
	$url=base_url().'libraries/img/personas/miniatura/';
?>
<div class="pagina table-responsive">
	<?php $this->load->view('estructura/print/encabezado',['titulo'=>'USUARIOS DE SISTEMA','pagina'=>$contPagina]); ?>
	
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php if(!isset($v1)){?><th class="celda th" width="5%" >#Item</th><?php } ?>
			<?php if(!isset($v2)){?><th class="celda th" width="6%" >Fotografía</th><?php } ?>
			<?php if(!isset($v3)){?><th class="celda th" width="15%">CI</th><?php } ?>
			<?php if(!isset($v4)){?><th class="celda th" width="34%">Nombre completo</th><?php } ?>
			<?php if(!isset($v5)){?><th class="celda th" width="10%">Tipo de usuario</th><?php } ?>
			<?php if(!isset($v6)){?><th class="celda th" width="20%">cargo</th><?php } ?>
			<?php if(!isset($v7)){?><th class="celda th" width="10%">usuario</th><?php } ?>
		</tr>
		<?php $cont=0; $sub_total=0; $total=0;
			foreach ($usuarios as $key => $usuario){ 
				$img='default.png';
				if($usuario->fotografia!=NULL && $usuario->fotografia!=""){ $img=$usuario->fotografia;} ?>
			<?php if($contReg>=$nro){
					$contReg=0;
					$contPagina++;
			?>
		</table><!--cerramos tabla-->
	</div><!--cerramos pagina-->
<div class="pagina table-responsive">
	<?php $this->load->view('estructura/print/encabezado',['titulo'=>'USUARIOS DE SISTEMA','pagina'=>$contPagina]); ?>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php if(!isset($v1)){?><th class="celda th" width="5%" >#Item</th><?php } ?>
			<?php if(!isset($v2)){?><th class="celda th" width="6%" >Fotografía</th><?php } ?>
			<?php if(!isset($v3)){?><th class="celda th" width="15%">CI</th><?php } ?>
			<?php if(!isset($v4)){?><th class="celda th" width="34%">Nombre completo</th><?php } ?>
			<?php if(!isset($v5)){?><th class="celda th" width="10%">Tipo de usuario</th><?php } ?>
			<?php if(!isset($v6)){?><th class="celda th" width="20%">cargo</th><?php } ?>
			<?php if(!isset($v7)){?><th class="celda th" width="10%">usuario</th><?php } ?>
		</tr>
		<?php $sub_total=0;}//end if ?>
		<tr class="fila">
			<?php if(!isset($v1)){?><td class="celda td"><?php echo $cont+1;$cont++;?></td><?php } ?>
			<?php if(!isset($v2)){?><td class="celda td"><img src="<?php echo $url.$img;?>" style='width:100%'></td><?php } ?>
			<?php if(!isset($v3)){?><td class="celda td"><?php echo $usuario->ci;?></td><?php } ?>
			<?php if(!isset($v4)){?><td class="celda td"><?php echo $usuario->nombre.' '.$usuario->nombre2.' '.$usuario->paterno.' '.$usuario->materno;?></td><?php } ?>
			<?php if(!isset($v5)){?><td class="celda td"><?php if($usuario->admin_sistema==1){ echo "Administrador";}else{ echo "Usuario simple";}?></td><?php } ?>
			<?php if(!isset($v6)){?><td class="celda td"><?php echo $usuario->cargo;?></td><?php } ?>
			<?php if(!isset($v7)){?><td class="celda td"><?php echo $usuario->usuario;?></td><?php } ?>
		</tr>
	<?php $contReg++;}// end for ?>
	</table>
</div>
<script type="text/javascript"> $(".nroPagina").html(<?php echo $contPagina;?>); </script>