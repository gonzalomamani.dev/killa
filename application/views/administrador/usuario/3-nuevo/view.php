<?php 
	$help1='title="<h4>Subir Fotografía</h4>" data-content="Seleccione una fotografía, preferiblemente una imagen que no sobrepase las dimenciones de <strong>700x700[px]</strong>, para evitar sobre cargar al sistema"';
	$help2='title="<h4>Nº Cédula<h4>" data-content="Ingrese un numero cédula de identidad con valores numericos <b>sin espacios</b>, de 6 a 8 digitos"';
	$help3='title="<h4>Ciudad<h4>" data-content="Seleccione la ciudad donde se difundio la cédula de identidad. Si desea adicionar una nueva ciudad puede hacerlo en la sección de configuración en el menu superior, o puede dar click el el boton <b>+</b>"';
	$help4='title="<h4>Primer nombre<h4>" data-content="Ingrese un nombre alfanumerico de 2 a 20 caracteres <b>puede incluir espacios</b>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help5='title="<h4>Segundo nombre<h4>" data-content="Ingrese un segundo nombre (si tuviera) alfanumérico de 2 a 20 caracteres <b>puede incluir espacios</b>, ademas el segundo nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help6='title="<h4>Apellido Paterno<h4>" data-content="Ingrese un apellido paterno alfanumérico de 2 a 20 caracteres <b>puede incluir espacios</b>, ademas el apellido solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help7='title="<h4>Apellido Materno<h4>" data-content="Ingrese un apellido materno alfanumérico de 2 a 20 caracteres <b>puede incluir espacios</b>, ademas el apellido solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help8='title="<h4>Tipo de usuario<h4>" data-content="Seleccione un tipo de usuario, si el usuario simpleo o un usuario que tendra privilegios de administrador del sistema."';
	$help9='title="<h4>Nombre de usuario<h4>" data-content="Ingrese una nombre de usuario de sistema, este al ingresar debe tomar muy en cuenta que el sistema no distingue mayusculas y minusculas. Solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)</b>, de <b>4 de 30 caractereres sin espacios</b>."';
	$help10='title="<h4>Cargo<h4>" data-content="Ingrese el cargo o cargos que realiza. Ej. Encargado de cortado de piezas. Solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)</b>, con un <b>máximo de 100 caractereres</b>."';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$v = array('0' => 'Usuario simple','1' => 'Administrador de sistema',);
?>
<div class="row">
	<div class="col-sm-3 col-sm-offset-9 col-xs-12"><strong><span class='text-danger'>(*)</span> Campo obligatorio</strong></div><hr>
	<div class="col-sm-2 col-xs-12"><strong>Fotografía:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<div class="input-group">
			<input class="form-control input-xs" id="n_fot" type="file" placeholder='Seleccione fotografia'>
			<span class="input-group-addon input-sm input-sm" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Nº Cédula:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<form onsubmit="return save_usuario()">
			<div class="input-group">
				<input class="form-control input-xs" id="n_ci" type="number" placeholder='Número de cedula de identidad' onkeyup="search_ci();" onclick="search_ci();" min='100000' max='99999999' step='any'>
				<span class="input-group-addon input-sm" <?php echo $popover.$help2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Ciudad:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<div class="input-group">
			<select class="form-control input-xs" id="n_ciu">
				<option value="">Seleccionar...</option>
		<?php for($i=0; $i<count($ciudades); $i++){ $ciudad=$ciudades[$i]; ?>
				<option value="<?php echo $ciudad->idci;?>"><?php echo $ciudad->ciudad.'('.$ciudad->abreviatura.')';?></option>
		<?php } ?>
			</select>
			<a href="<?php echo base_url();?>capital_humano?p=5" target="_blank" title="Ver configuraciónes" class="input-group-addon input-sm"><i class="glyphicon glyphicon-plus"></i></a>
			<span class="input-group-addon input-sm" <?php echo $popover.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Primer nombre:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<form onsubmit="return save_usuario()">
			<div class="input-group col-xs-12">
				<input class="form-control input-xs" id="n_nom1" type="text" placeholder='Primero nombre del empleado' maxlength="20">
				<span class="input-group-addon input-sm" <?php echo $popover.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong>Segundo nombre:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<form onsubmit="return save_usuario()">
			<div class="input-group col-xs-12">
				<input class="form-control input-xs" id="n_nom2" type="text" placeholder='Segundo nombre del empleado' maxlength="20">
				<span class="input-group-addon input-sm" <?php echo $popover.$help5;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Apellido paterno:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<form onsubmit="return save_usuario()">
			<div class="input-group col-xs-12">
				<input class="form-control input-xs" id="n_pat" type="text" placeholder='Apellido paterno del empleado' maxlength="20">
				<span class="input-group-addon input-sm" <?php echo $popover.$help6;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong>Apellido materno:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<form onsubmit="return save_usuario()">
			<div class="input-group col-xs-12">
				<input class="form-control input-xs" id="n_mat" type="text" placeholder='Apellido materno del empleado' maxlength="20">
				<span class="input-group-addon input-sm" <?php echo $popover.$help7;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Tipo de usuario:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<div class="input-group">
			<select id="n_tus" class="form-control input-xs">
				<option value="">Seleccionar</option>
			<?php foreach ($v as $key => $value) { ?>
				<option value="<?php echo $key;?>"><?php echo $value;?></option>
			<?php }?>
			</select>
			<span class="input-group-addon input-sm" <?php echo $popover.$help8;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Nombre de usuario:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<form onsubmit="return save_usuario()">
			<div class="input-group">
				<input class="form-control input-xs" type="text" id="n_usu" placeholder='Nombre de usuario de sistema' maxlength="30">
				<span class="input-group-addon input-sm" <?php echo $popover.$help9;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong>Cargo:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<form onsubmit="return save_usuario()">
			<div class="input-group col-xs-12">
				<input class="form-control input-xs" id="n_car" type="text" placeholder='Cargo del empleado' maxlength="100">
				<span class="input-group-addon input-sm" <?php echo $popover.$help10;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div><i class="clearfix"></i>
</div>
<script language='javascript'>Onfocus("n_ci");$('[data-toggle="popover"]').popover({html:true});</script>