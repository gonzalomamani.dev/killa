<?php 
	$url=base_url().'libraries/img/personas/miniatura/';
	$img="default.png";
	if($usuario->fotografia!=NULL && $usuario->fotografia!=''){$img=$usuario->fotografia;}
?>
<div class="row">
	<div class="col-sm-3 col-xs-12 text-center"><img src="<?php echo $url.$img;?>" class="img-thumbnail" alt=""><hr></div>
	<div class="col-sm-9 cols-xs-12 table-responsive">
	<table border="0" class="tabla tabla-border-true">
		<tr class="fila">
			<th class="celda th">CI</th>
			<td class="celda td" colspan="3"><?php echo $usuario->ci;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Nombre completo:</th><td class="celda td" colspan="3"><?php echo $usuario->nombre;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Tipo de usuario:</th><td class="celda td" colspan="3"><?php if($usuario->admin_sistema==1){ echo "Administrador";}else{ echo "Usuario simple";}?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Cargo:</th><td class="celda td" colspan="3"><?php echo $usuario->cargo;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Usuario:</th><td class="celda td" colspan="3"><?php echo $usuario->usuario;?></td>
		</tr>
	</table>
	</div>
</div>