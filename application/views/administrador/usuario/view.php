<?php 
$j_usuarios=json_encode($usuarios);
if (count($usuarios)>0) {  $url=base_url().'libraries/img/personas/miniatura/'; ?>
<table width="100%" class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th class="g-thumbnail">#Item</th>
			<th class="celda-sm-10">NIT/CI</th>
			<th class="celda-sm-30">Nombre completo</th>
			<th class="celda-sm" width="15%">Tipo de usuario</th>
			<th class="celda-sm" width="15%">Cargo</th>
			<th class="celda-sm" width="19%">Nombre de usuario</th>
			<th width="11%"></th>
		</tr>
	</thead>
	<tbody>
	<?php for ($j=0; $j < count($usuarios); $j++) { $res=$usuarios[$j]; 
			$img="default.png";
			if($res->fotografia!='' && $res->fotografia!=NULL){ $img=$res->fotografia; } ?>
		<tr>
		<td class="g-thumbnail"><div id="item"><?php echo $j+1;?></div><div class="g-img"><img src="<?php echo $url.$img;?>" class="img-thumbnail g-thumbnail"></div></td>
			<td><?php echo $res->ci.' '.$res->abreviatura;?></td>
			<td><?php echo $res->nombre." ".$res->nombre2." ".$res->paterno." ".$res->materno; ?></td>
			<td class="celda-sm"><?php if($res->admin_sistema==1){ echo "Administrador";}else{ echo "Usuario simple";}?></td>
			<td class="celda-sm"><?php echo $res->cargo;?></td>
			<td class="celda-sm"><?php echo $res->usuario;?></td>
			<td>
				<?php $mod="config_usuario('".$res->ide."')"; if($privilegio[0]->ad1u!="1"){ $mod="";}?>
				<?php $del="confirmar_usuario('".$res->ide."')"; if($privilegio[0]->ad1d!="1"){ $del="";}?>
				<?php $this->load->view('estructura/botones/botones_registros',['reportes'=>"detalle_usuario('".$res->ide."')",'configuracion'=>$mod,'eliminar'=>$del]);?>
			</td>
		</tr>
	<?php }?>
	</tbody>
</table>
<?php } else{ ?>
<h3>No existen registros</h3>
<?php }?>
<script language="javascript" type="text/javascript"> 
	$("#print").removeAttr("onclick"); 
	$("#print").unbind("click"); 
	$("#print").click(function(){ imprimir_usuario('<?php echo $j_usuarios;?>'); }); 
</script>