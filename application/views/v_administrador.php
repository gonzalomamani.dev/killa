<?php
    $cargo=$this->session->userdata("cargo");
    if($this->session->userdata("nombre2")!=""){
		$usuario=$this->session->userdata("nombre")." ".$this->session->userdata("nombre2")." ".$this->session->userdata("paterno");
	}else{
		$usuario=$this->session->userdata("nombre")." ".$this->session->userdata("paterno");
	}
    $ci=$this->session->userdata("ci");
?>						
<!DOCTYPE html>
<html lang="es" class="no-js">
	<head><?php $this->load->view('estructura/head',['title'=>'Administrador','css'=>'']);?></head>
	<body>
		<?php $this->load->view('estructura/modal');?>
		<div class="contenedor">
			<?php $this->load->view('estructura/menu_izq',['usuario'=>$usuario,'cargo'=>$cargo,'ventana'=>'administrador','privilegio'=>$privilegio[0]]);?>
			<?php $v_menu="";
				if($privilegio[0]->ad1r==1){ $v_menu.="Usuarios de sistema/usuario/icon-user|"; }
				if($privilegio[0]->ad2r==1){ $v_menu.="Privilegios/privilegio/icon-unlocked|";}
				if($privilegio[0]->ad3r==1){ $v_menu.="Configuración/config/glyphicon glyphicon-cog";}
			?>
			<?php $this->load->view('estructura/menu_top',['usuario'=>$usuario,'cargo'=>$cargo,'menu'=>$v_menu]);?>
			<div id="search"></div>
			<div id="contenido"></div>
		</div>	
		<?php $this->load->view('estructura/js',['js'=>'admin/administrador.js']);?>
	</body>
	<?php 
	switch($pestania){
		case '1': $title="Usuarios de sistema"; $activo='usuario'; $search="administrador/search_usuario"; $view="administrador/view_usuario"; break;
		case '2': $title="Privilegios"; $activo='privilegio'; $search="administrador/search_privilegio"; $view="administrador/view_privilegio"; break;
		case '7': $title="Configuración contabilidad"; $activo='config'; $search=""; $view="administrador/view_config"; break;
		default: $title="Error!";$activo='';$search="NULL";$view="NULL"; break;
	} ?>
	<script type="text/javascript">activar('<?php echo $activo;?>','<?php echo $title;?>','administrador?p=<?php echo $pestania;?>'); get_2n('<?php echo $search; ?>',{},'search',false,'<?php echo $view; ?>',{},'contenido',true); </script>	
</html>