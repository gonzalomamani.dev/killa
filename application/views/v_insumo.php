<?php
	$cargo=$this->session->userdata("cargo");
	if($this->session->userdata("nombre2")!=""){
		$usuario=$this->session->userdata("nombre")." ".$this->session->userdata("nombre2")." ".$this->session->userdata("paterno");
	}else{
		$usuario=$this->session->userdata("nombre")." ".$this->session->userdata("paterno");
	}
    $ci=$this->session->userdata("ci");
?>					
<!DOCTYPE html>
<html lang="es" class="no-js">
	<head><?php $this->load->view('estructura/head',['title'=>'Insumos','css'=>'']);?></head>
	<body>
		<?php $this->load->view('estructura/modal');?>
		<div class="contenedor">
			<?php $this->load->view('estructura/menu_izq',['usuario'=>$usuario,'cargo'=>$cargo,'ventana'=>'insumo','privilegio'=>$privilegio[0]]);?>
			<?php $v_menu="";
				if($privilegio[0]->ot1r==1){ $v_menu.="Materiales adicionales/material/icon-box2|"; }
				if($privilegio[0]->ot2r==1){ $v_menu.="Otros materiales e insumos/insumo/icon-cabinet2|";}
				if($privilegio[0]->ot3r==1){ $v_menu.="Configuración/config/glyphicon glyphicon-cog";}
			?>
			<?php $this->load->view('estructura/menu_top',['usuario'=>$usuario,'cargo'=>$cargo,'menu'=>$v_menu]);?>
			<div id="search"></div>
			<div id="contenido"></div>
		</div>
		<?php $this->load->view('estructura/js',['js'=>'insumo/insumo.js']);?>
	</body>
	<?php switch($pestania){
			case '1': $activo='material'; $search="insumo/search_material"; $view="insumo/view_material"; break;
			case '2': $activo='insumo'; $search="insumo/search_insumo"; $view="insumo/view_insumo"; break;
			case '3': $activo='config'; $search=""; $view="insumo/view_config"; break;
	} ?>
	<script type="text/javascript">activar('<?php echo $activo;?>'); get_2n('<?php echo $search; ?>',{},'search',false,'<?php echo $view; ?>',{},'contenido',true); </script>
</html>