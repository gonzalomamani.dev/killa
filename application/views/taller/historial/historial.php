<?php 
	$nombre="";
	$ide="";
	if(count($empleado)>0){
		if($empleado[0]->nombre2!=null && $empleado[0]->nombre2!=''){
			$nombre=$empleado[0]->nombre.' '.$empleado[0]->nombre2.' '.$empleado[0]->paterno.' '.$empleado[0]->materno;
		}else{
			$nombre=$empleado[0]->nombre.' '.$empleado[0]->paterno.' '.$empleado[0]->materno;
		}
		$ide=$empleado[0]->ide;
	}
	//echo count($pedido_tareas);
?>
<div class="text-right text-md"><a href="javascript:" title="Actualizar..." onclick="view_historial('<?php echo $empleado[0]->ide;?>')"><span class='glyphicon glyphicon-refresh'></span></a></div>
<div>
	<h3><?php echo $nombre;?></h3>
</div>
<div class="table-responsive">
	<table class="table table-bordered table-hover table-striped">  
		<thead>
	        <tr>
	        	<th style='width: 3%'>Nº<br>Ped.</th>  
		        <th style='width: 3%'>Nº<br>Prod.</th>  
		  		<th style='width: 3%'>Nº<br>Trab.</th>
		  		<th style='width: 13%'>Nombre de Producto</th>
		  		<th style='width: 19%'>Tarea</th>
		  		<th style='width: 5%'>Cant. de Pedido</th>
		  		<th style='width: 10%'>Fecha asignación<br>de tarea</th>
				<th style='width: 10%'>Fecha de inicio</th>
				<th style='width: 12%'>Observacion de inicio</th>
				<th style='width: 10%'>Fecha de finalizacion</th>
				<th style='width: 12%'>Observacion de finalizacion</th>
	        </tr>
		</thead>
		<tbody>
		<?php 
	        if(count($pedido_tareas)>0){
		        for($i=0; $i < count($pedido_tareas) ; $i++) { $tarea=$pedido_tareas[$i];
		        	if($tarea->estado_ot==1){
			        	$pedido=$this->M_pedido->get($tarea->idpe);
			        	$producto=$this->M_categoria_producto->get_row_producto('cp.idpim',$tarea->idpim);
			        	$material=$this->M_categoria_producto->get_material($tarea->idpim);
			        	if($tarea->idpipr=="" || $tarea->idpipr==NULL){
							if($tarea->idppr!=NULL && $tarea->idppr!=""){
								$proceso=$this->M_producto_proceso->get_proceso($tarea->idppr);$tipo="prp";
							}else{
								$proceso=NULL;$tipo=""; 
							}
						}else{//es una tareas de pieza proceso
							$proceso=$this->M_pieza_proceso->get_proceso($tarea->idpipr);$tipo="pip";
						}
			        	if($tarea->fecha_fin!=NULL && $tarea->fecha_fin!=''){ ?>
			        		<tr>
					        	<td><?php echo $pedido[0]->numero;?></td>  
						        <td><?php echo $tarea->op;?></td>  
						  		<td><?php echo $tarea->ot;?></td>
						  		<td><strong><?php echo $producto[0]->nombre." - ".$material[0]->nombre_c;?></strong></td>
						  		<td>
						  		<?php 	if(!empty($proceso)){
									if($tipo=="pip"){ 
										$grupo=$this->M_pieza_grupo->get($proceso[0]->idpig);
										$material=$this->M_pieza->get_material($proceso[0]->idpi);
								 		echo $proceso[0]->nombre_proceso.": ".$grupo[0]->nombre."<strong> de ".$material[0]->nombre."</strong> (".$proceso[0]->nombre_pieza.")";;
								 		$cantidad=$tarea->cantidad*$proceso[0]->unidades;
									} 
									if($tipo=="prp"){ 
										echo $proceso[0]->nombre.": ".$proceso[0]->sub_proceso;
										$cantidad=$tarea->cantidad;
									}
								}?>
						  		</td>
						  		<td><?php echo $cantidad.' u.';?></td>
						  		<td><?php echo $this->validaciones->formato_fecha($tarea->fecha_asignacion,'d,m,Y');?></td>
								<td><?php echo $tarea->fecha_inicio;?></td>
								<td><?php echo $tarea->observacion_inicio;?></td>
								<td><?php echo $tarea->fecha_fin;?></td>
								<td><?php echo $tarea->observacion_fin;?></td>
					        </tr>
		        	<?php
		        		}
		        	}
		      	}// end for
	    	}else{?>
				   <tr><th colspan="11"><h3>0 tareas en el historial...</h3></th></tr>   
	    <?php } ?>
	    </tbody>
	</table> 
</div>