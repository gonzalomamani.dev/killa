<?php
	$producto=$producto[0];
	$ruta=base_url().'libraries/img/pieza_productos/';
	$insumo="";
	$imagen1="no.png";
	$imagen2="no.png";
	$imagen3="no.png";
	//$swap=$this->M_categoria_producto->get_portada_producto($producto->idp);
	
	$im=$this->M_imagen_producto->get_pieza_material($idpim);
	for($i=0; $i<count($im); $i++){$img_producto=$im[$i];
		switch ($i) {
			case 0: $imagen1=$img_producto->nombre; break;
			case 1: $imagen2=$img_producto->nombre; break;
			case 2: $imagen3=$img_producto->nombre; break;
		}
	}
	$categoria_pieza=$this->M_categoria_pieza->get_categoria_pieza($producto->idp);
?>
<ul class="nav nav-tabs">
  <li role="presentation" class="active"><a href="javascript:" onclick="ficha_tecnica('<?php echo $producto->idp; ?>','<?php echo $idpim;?>','<?php echo $iddp;?>')">Ficha Técnica</a></li>
  <li role="presentation"><a href="javascript:" onclick="materiales('<?php echo $producto->idp; ?>','<?php echo $idpim;?>','<?php echo $iddp;?>')">Materiales requeridos</a></li>
</ul><br>
<div id="table-responsive">
	<table class="tabla tabla-border-true">
		<tr class='fila'>
			<th class='celda th' colspan="4" width="60%"><h2><?php echo $producto->nombre;?></h2></th>
			<td class='celda td' colspan="2" rowspan="9" width="40%"><img src="<?php echo $ruta.$imagen1;?>" alt="" width='100%'></td>
		</tr>
		<tr class='fila'>
			<th class='celda th' colspan="2">Código</th>
			<th class='celda th' colspan="2">Fecha de Creación</th>
		</tr>
		<tr class='fila'>
			<td class='celda td' colspan="2"><?php echo $producto->cod;?></td>
			<td class='celda td' colspan="2"><?php echo $producto->fecha_creacion;?></td>
			
		</tr>
		<tr class='fila'>
			<th class='celda th' colspan="4">Diseñador</th>
		</tr>
		<tr class='fila'>
			<td class='celda td' colspan="4"><?php echo $producto->disenador;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th" colspan="4">Descripción</th></tr>
		<tr class='fila'>
			<td class='celda td' colspan="4"><?php echo $producto->descripcion;?></td>
		</tr>
		<tr class='fila'>
			<th class='celda th' width="15%">Logo Etiqueta de Cuero</th>
			<th class='celda th' width="15%">Logo Plaqueta</th>
			<th class='celda th' width="15%">Logo Repujado</th>
			<th class='celda th' width="15%">Repujado Decorado</th>
		</tr>
		<tr class='fila'>
			<td class="celda td"><?php echo $producto->etiquetaLogo;?></td>
			<td class="celda td"><?php echo $producto->plaquetaLogo;?></td>
			<td class="celda td"><?php echo $producto->repujadoLogo;?></td>
			<td class="celda td"><?php echo $producto->repujado;?></td>
		</tr>
		<tr class='fila'>
			<th class='celda th' colspan="2">Correa</th>
			<th class='celda th' colspan="2">Cierre Dentro</th>
						<?php 
				$medida="";
				if($producto->largo!=0 & $producto->largo!=null){
					$medida.='Largo '.$producto->largo.'cm. ';
				}
				if($producto->alto!=0 & $producto->alto!=null){
					$medida.=' x Alto '.$producto->alto.'cm. ';
				}
				if($producto->ancho!=0 & $producto->ancho!=null){
					$medida.='x Ancho '.$producto->ancho.'cm. ';
				}
			?>
			<th class='celda th' rowspan="2" width="20%">Medidas</th>
			<td class='celda td' rowspan="2" width="20%"><?php echo $medida;?></td>
		</tr>
		<tr class='fila'>
			<td class='celda td' colspan="2"><?php echo $producto->correa?></td>
			<td class='celda td' colspan="2"><?php echo $producto->cierreDentro;?></td>
		</tr>
		<tr class='fila'>
			<th class='celda th' colspan="2">Cierre Delante</th>
			<th class='celda th' colspan="2">Cierre Detras</th>
			<th class="celda th" rowspan="4"><img src="<?php echo $ruta.$imagen2;?>" alt="" width='100%'> </th>
			<th class="celda th" rowspan="4"><img src="<?php echo $ruta.$imagen3;?>" alt="" width='100%'> </th>
		</tr>
		<tr class='fila'>
			<td class='celda td' colspan="2"><?php echo $producto->cierreDelante;?></td>
			<td class='celda td' colspan="2"><?php echo $producto->cierreDetras;?></td>
		</tr>
		<tr class='fila'>
			<th class="celda th" colspan="2">Colores Disponibles</th>
			<th class="celda th" colspan="2"></th>
		</tr>
		<tr class="fila">
			<td class="celda td" colspan="2">
				<?php 
				$ca_pr=$this->M_categoria_producto->get_categoria_producto_colores($producto->idp);
					for ($i=0; $i < count($ca_pr) ; $i++) {
						$mat=$this->M_material->get($ca_pr[$i]->idm);
						$color=$this->M_color->get($mat[0]->idco); 
					?>
					<div class="g-caja-color">
						<span class="g-color" style="background: <?php echo $color[0]->codigo?>">&#160;&#160;&#160;&#160;</span>
						<span class="g-text-color"><?php echo $color[0]->nombre;?></span>
					</div>
						<?php
					}
				?>
			</td>
			<td class="celda td" colspan="2"></td>
		</tr>
		<tr class='fila'>
			<th class='celda th' colspan="6">Otras caracteristicas tecnicas</th>
		</tr>
		<tr class='fila'>
			<td class="celda td" colspan="6"><?php echo $producto->otra_caracteristica;?></td>
		</tr>
	</table>
</div>