<?php $producto=$producto[0]; 
	$url=base_url().'libraries/img/materiales/miniatura/';
?>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="ficha_tecnica('<?php echo $producto->idp; ?>','<?php echo $idpim;?>','<?php echo $iddp;?>')">Ficha Técnica</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="materiales('<?php echo $producto->idp; ?>','<?php echo $idpim;?>','<?php echo $iddp;?>')">Materiales requeridos</a></li>
</ul><br>
<div class="table-responsive g-table-responsive">
<table class="table table-bordered table-hover table-striped">
	<thead>
		<tr class="fila">
			<th class='celda th' colspan="5"><h2><?php echo $producto->nombre;?></h2></th>
		</tr>
		<tr class="fila">
			<th class="celda th" colspan="5">DETALLE DE PRODUCCIÓN</th>
		</tr>
		<tr class="fila">
			<th class="celda th" width="11%"><div class="g-thumbnail-modal"></div></th>
			<th class="celda th" width="15%">Código</th>
			<th class='celda th' width="50%">Material</th>
			<th class='celda th' width="12%">c/uni.</th>
			<th class='celda th' width="12%">c/req.</th>
		</tr>
	</thead>
	<tbody>
<?php 
	$materiales=json_decode($materiales); $i=1;
	$total_material=0;
	foreach ($materiales as $clave => $material) { 
		$img="default.png";
		if($material->fotografia!=NULL || $material->fotografia!=""){ $img=$material->fotografia;}
		if($material->cantidad!="vacio"){
?>
		<tr class="fila">
			<td class="celda td"><div id="item"><?php echo $i++;?></div><img src="<?php echo $url.$img;?>" width="100%" class="img-thumbnail"></td>
			<td class="celda td"><?php echo $material->codigo;?></td>
			<td class="celda td"><?php echo $material->nombre.' - '.$material->color;?></td>
			<td class="celda td"><?php if($material->cantidad>0){ echo $material->cantidad.' ['.$material->abreviatura.']';} ?></td>
			<td class="celda td"><?php if(($material->cantidad*$detalle_pedido->cantidad)>0){ echo ($material->cantidad*$detalle_pedido->cantidad).' ['.$material->abreviatura.']';} ?></td>
		</tr>
<?php  }
	}// end for 
	$materiales_color_producto=json_decode($materiales_color_producto);
	$material_actual=$this->M_categoria_producto->get_material($idpim);//saacando el material del producto actual de trabajo
	if(!empty($material_actual)){
		foreach ($materiales_color_producto as $clave => $material) { 
			if($material->idmi==$material_actual[0]->idmi){
				$img="default.png";
				if($material->fotografia!=NULL || $material->fotografia!=""){ $img=$material->fotografia;}
?>
		<tr class="fila">
			<td class="celda td"><div id="item"><?php echo $i++;?></div><img src="<?php echo $url.$img;?>" width="100%" class="img-thumbnail"></td>
			<td class="celda td"><?php echo $material->codigo;?></td>
			<td class="celda td"><?php echo $material->nombre.' - '.$material->color;?></td>
			<td class="celda td"><?php if($material->cantidad>0){ echo $material->cantidad.' ['.$material->abreviatura.']';}?></td>
			<td class="celda td"><?php if($material->cantidad>0){ echo $material->cantidad.' ['.$material->abreviatura.']';}?></td>
		</tr>
<?php
				break;
			}
		}
	}
?>


	</tbody>
</table>
</div>

