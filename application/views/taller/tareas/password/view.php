<?php 
	$help1="Ingrese su <b>contraseña actual</b> alfanumérica evite usar letras mayusculas y espacios";
	$help2="Ingrese su <b>nueva contraseña</b> alfanumérica evite usar letras mayusculas y espacios, ademas la contraseña debe poseer de entre 4 y 25 caracteres";
	$popover1='data-toggle="popover" data-placement="left" data-trigger="hover" title="<h4>Ingresar Contraseña<h4>" data-content="'.$help1.'"';
	$popover2='data-toggle="popover" data-placement="left" data-trigger="hover" title="<h4>Ingresar Contraseña<h4>" data-content="'.$help2.'"';
  	$url="./libraries/img/personas/miniatura/";
  	$img="default.png";
  	if($empleado->fotografia!=NULL && $empleado->fotografia!=""){ $img=$empleado->fotografia;}
?>
<div class="text-center"><img class="img-resposive img-thumbnail" src="<?php echo $url.$img;?>"></div>
<center><strong><ins>INGRESE SU CONTRASEÑA</ins></strong></center>
	<div class="form-group">
		<div class="input-group input-group-xs">
			<span class="input-group-addon" style='background-color: #fff;'><i class='glyphicon glyphicon-lock'></i></span>
			<input type="password" id="pass" placeholder='Contraseña' class="form-control input-md" maxlength="25">
			<span class="input-group-addon input-sm" title="Ver Contraseña" style="cursor:pointer;" id="pointer"><i class='glyphicon glyphicon-eye-open'></i></span>
			<span class="input-group-addon input-sm" <?php echo $popover1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div><br>
		<center><strong><ins>NUEVA CONTRASEÑA</ins></strong></center>
		<div class="input-group input-group-xs">
			<span class="input-group-addon" style='background-color: #fff;'><i class='glyphicon glyphicon-lock'></i></span>
			<input type="password" id="n_pass" placeholder='Nueva contraseña' class="form-control input-md" maxlength="25">
			<span class="input-group-addon input-sm" title="Ver Contraseña" style="cursor:pointer;" id="pointer2"><i class='glyphicon glyphicon-eye-open'></i></span>
			<span class="input-group-addon input-sm" <?php echo $popover2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div><br>
		<div class="input-group input-group-xs">
			<span class="input-group-addon" style='background-color: #fff;'><i class='glyphicon glyphicon-lock'></i></span>
			<input type="password" id="n_pass2" placeholder='Repita contraseña' class="form-control input-md" maxlength="25">
			<span class="input-group-addon input-sm" title="Ver Contraseña" style="cursor:pointer;" id="pointer3"><i class='glyphicon glyphicon-eye-open'></i></span>
			<span class="input-group-addon input-sm" <?php echo $popover2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div>
	<script language='javascript'>
		Onfocus("e_password");$('[data-toggle="popover"]').popover({html:true});
		$("#pointer").hover(function(){ $("#pass").attr("type","text");});
		$("#pointer").mouseout(function(){ $("#pass").attr("type","password");});
		$("#pointer2").hover(function(){ $("#n_pass").attr("type","text");});
		$("#pointer2").mouseout(function(){ $("#n_pass").attr("type","password");});
		$("#pointer3").hover(function(){ $("#n_pass2").attr("type","text");});
		$("#pointer3").mouseout(function(){ $("#n_pass2").attr("type","password");});
	</script>	