<?php 
	$help1='title="<h4>Obs. de inicio<h4>" data-content="Ingrese unas Observaciónes antes del inicio de tarea, Ej. si no le dieron el material necesarios para su trabajo. Ingrese una contenido alfanumerico <strong>de 0 a 400 caracteres con espacios y sin saltos de linea</strong>, ademas la observación solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº)<b>"';
	$help2='title="<h4>Obs. de terminado<h4>" data-content="Ingrese unas Observaciónes una vez terminada la tarea, Ej. algunas piezas defectuosas. Ingrese un contenido alfanumerico <strong>de 0 a 400 caracteres con espacios y sin saltos de linea</strong>, ademas la observación solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$nombre="";
	$ide="";
	if(count($empleado)>0){
		if($empleado[0]->nombre2!=null && $empleado[0]->nombre2!=''){
			$nombre=$empleado[0]->nombre.' '.$empleado[0]->nombre2.' '.$empleado[0]->paterno.' '.$empleado[0]->materno;
		}else{
			$nombre=$empleado[0]->nombre.' '.$empleado[0]->paterno.' '.$empleado[0]->materno;
		}
		$ide=$empleado[0]->ide;
	}
	//echo count($pedido_tareas);
?>
<div class="text-right text-md"><a href="javascript:" title="Actualizar..." onclick="view_tareas('<?php echo $empleado[0]->ide;?>')"><span class='glyphicon glyphicon-refresh'></span></a> | <a href="javascript:" title="Configuración" onclick="config_pass('<?php echo $ide;?>')"><span class='glyphicon glyphicon-cog'></span></a></div>
<div>
	<h3><?php echo $nombre;?></h3>
</div>
<div class="table-responsive">
	<table class="table table-bordered table-hover table-striped">  
		<thead>
	        <tr>
	        	<th style='width: 3%'>Nº<br>Ped.</th>  
		        <th style='width: 3%'>Nº<br>Prod.</th>  
		  		<th style='width: 3%'>Nº<br>Trab.</th>
		  		<th style='width: 13%'>Nombre de Producto</th>
		  		<th style='width: 22%'>Tarea</th>
		  		<th style='width: 5%'>Cant. de Pedido</th>
		  		<th style='width: 12%'>Fecha asignación<br>de tarea</th>
				<th style='width: 34%'>Observacion de Tarea</th>
				<th style='width: 5%'></th>
	        </tr>
		</thead>
		<tbody>
		<?php 
	        if(count($pedido_tareas)>0){
		        for($i=0; $i < count($pedido_tareas) ; $i++) { $tarea=$pedido_tareas[$i];
		        	if($tarea->estado_ot==1){
			        	$pedido=$this->M_pedido->get($tarea->idpe);
			        	$producto=$this->M_categoria_producto->get_row_producto('cp.idpim',$tarea->idpim);
			        	$material=$this->M_categoria_producto->get_material($tarea->idpim);
			        	if($tarea->idpipr=="" || $tarea->idpipr==NULL){
							if($tarea->idppr!=NULL && $tarea->idppr!=""){
								$proceso=$this->M_producto_proceso->get_proceso($tarea->idppr);$tipo="prp";
							}else{
								$proceso=NULL;$tipo=""; 
							}
						}else{//es una tareas de pieza proceso
							$proceso=$this->M_pieza_proceso->get_proceso($tarea->idpipr);$tipo="pip";
						}
			        	if($tarea->fecha_fin==NULL || $tarea->fecha_fin==''){ ?>
			        		<tr>
					        	<td><?php echo $pedido[0]->numero;?></td>  
						        <td><?php echo $tarea->op;?></td>  
						  		<td><?php echo $tarea->ot;?></td>
						  		<td><ins><strong><a href="javascript:" onclick="ficha_tecnica('<?php echo $producto[0]->idp;?>','<?php echo $tarea->idpim;?>','<?php echo $tarea->iddp;?>')"><?php echo $producto[0]->nombre." - ".$material[0]->nombre_c;?></a></strong></ins></td>
						  		<td>
						  		<?php 	if(!empty($proceso)){
									if($tipo=="pip"){ 
										$grupo=$this->M_pieza_grupo->get($proceso[0]->idpig);
										$material=$this->M_pieza->get_material($proceso[0]->idpi);
								 		echo $proceso[0]->nombre_proceso.": ".$grupo[0]->nombre."<strong> de ".$material[0]->nombre."</strong> (".$proceso[0]->nombre_pieza.")";;
								 		$cantidad=$tarea->cantidad*$proceso[0]->unidades;
									} 
									if($tipo=="prp"){ 
										echo $proceso[0]->nombre.": ".$proceso[0]->sub_proceso;
										$cantidad=$tarea->cantidad;
									}
								}?>
						  		</td>
						  		<td><?php echo $cantidad.' u.';?></td>
						  		<td><?php echo $this->validaciones->formato_fecha($tarea->fecha_asignacion,'d,m,Y');?></td>
								<td>
									<div class="input-group col-xs-12">
										<textarea class="form-control input-sm" id="obs<?php echo $tarea->idpt;?>" placeholder='Observación de <?php if($tarea->fecha_inicio==NULL || $tarea->fecha_inicio==''){echo 'inicio';}else{ echo 'finalización';}?> de Tarea' maxlength="400"></textarea>
										<span class="input-group-addon input-sm" <?php if($tarea->fecha_inicio==NULL || $tarea->fecha_inicio==''){ echo $popover.$help1;}else{ echo $popover.$help2;}?>><i class='glyphicon glyphicon-info-sign'></i></span>
									</div>
								</td>
								<td>
								<?php if($tarea->fecha_inicio==NULL || $tarea->fecha_inicio==''){?>
								<button type="button" class="btn btn-success btn-md" onclick="alert_inicio('<?php echo $tarea->idpt;?>','<?php echo $ide;?>','')" title="Iniar trabajo"><i class='glyphicon glyphicon-play'></i><span class="visible-md visible-lg visible-sm">INICIAR</span></button>
								<?php }else{?>
								<button type="button" class="btn btn-danger btn-md" onclick="alert_fin('<?php echo $tarea->idpt;?>','<?php echo $ide;?>','')"><i class='icon-clock2'></i><span class="visible-md visible-lg visible-sm">FINALIZAR</span></button>
								<?php }?>
								</td>
					        </tr>
		        	<?php
		        		}
		        	}
		      	}// end for
	    	}else{?>
				   <tr><th colspan="9"><h3>0 tareas asignadas...</h3></th></tr>   
	    <?php } ?>
	    </tbody>
	</table> 
</div>
<script language='javascript'>$('[data-toggle="popover"]').popover({html:true});</script>