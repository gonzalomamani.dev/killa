<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th style='width: 10%'>Fotografía</th>
			<th style='width: 55%'>Nombre Completo</th>
			<th class="celda-sm" style='width: 15%'>Cargo</th>
			<th style='width: 10%'></th>
			<th style='width: 10%'></th>
		</tr>
	</thead>
	<tbody>
<?php 
if(!empty($empleados)){
	for ($i=0; $i < count($empleados); $i++) { $empleado=$empleados[$i];
		if($empleado->tipo==0){ 
			$ruta=base_url().'libraries/img/personas/miniatura/';
			$nombre=$empleado->nombre." ".$empleado->nombre2." ".$empleado->paterno." ".$empleado->materno;
			$fotografia='default.png';
			if($empleado->fotografia!=null && $empleado->fotografia!=''){$fotografia=$empleado->fotografia;}
			$cargo=$empleado->cargo;
			$ide=$empleado->ide; 
	?>
		<tr>
	        <td><div id="item"><?php echo $i+1;?></div><img src="<?php echo $ruta.$fotografia;?>" width='100%' class='img-thumbnail'></td>
	        <td><H3><?php echo $nombre;?></H3></td>  
		  	<td class="celda-sm"><?php echo $cargo;?></td>
		  	<td> <button type="button" class="btn btn-danger btn-lg" onclick="confirmar(<?php echo $empleado->ide;?>)"><span class="icon-tools3 text-md"></span> <span class="visible-md visible-lg visible-sm">Trabajos</span></button></td>
		  	<td> <button type="button" class="btn btn-celeste btn-lg" onclick="view_historial(<?php echo $empleado->ide;?>)"><span class="icon-clipboard2 text-md"></span> <span class="visible-md visible-lg visible-sm">Historial</span></button></td>
	    </tr> 
	<?php 
		}
	}//end for
}else{// end if 
	echo "<tr><td colspan='5'><h2>0 registros encontrados...</h2></td></tr>";
}?>
	</tbody>
</table>  
