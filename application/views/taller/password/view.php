<?php 
	$help2="Ingrese su contraseña evitando usar letras mayusculas y espacios ademas la contraseña debe tener entre 4 y 25 caracteres alfanuméricos..";
	$popover2='data-toggle="popover" data-placement="left" data-trigger="hover" title="<h4>Ingresar Contraseña<h4>" data-content="'.$help2.'"';
  	$url="./libraries/img/personas/miniatura/";
  	$img="default.png";
  	if($empleado->fotografia!=NULL && $empleado->fotografia!=""){ $img=$empleado->fotografia;}
?>
<div class="text-center"><img class="img-resposive img-thumbnail" src="<?php echo $url.$img;?>"></div>
<center><strong><ins>INGRESE SU CONTRASEÑA</ins></strong></center>
	<div class="form-group">
		<div class="input-group input-group-xs">
		<span class="input-group-addon" style='background-color: #fff;'><i class='glyphicon glyphicon-lock'></i></span>
		<input type="password" id="e_password" placeholder='Contraseña' class="form-control input-md" maxlength="25">
		<span class="input-group-addon input-sm" title="Ver Contraseña" style="cursor:pointer;" id="pointer"><i class='glyphicon glyphicon-eye-open'></i></span>
		<span class="input-group-addon input-sm" <?php echo $popover2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
	</div>	
	</div>
	<script language='javascript'>
		Onfocus("e_password");$('[data-toggle="popover"]').popover({html:true});
		$("#pointer").hover(function(){ $("#e_password").attr("type","text"); });
		$("#pointer").mouseout(function(){ $("#e_password").attr("type","password"); });
	</script>	