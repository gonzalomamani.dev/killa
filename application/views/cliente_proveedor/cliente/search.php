<table class="tabla tabla-border-false">
	<tr class="fila">
		<td class='celda g-thumbnail'><div class="g-img"></div></td>
		<td class='celda celda-sm-10'><form onsubmit="return view_cliente()"><input type="number" id="s_nit" placeholder='Nit/CI' class="form-control input-sm" onkeyup="reset_all_view(this.id)" min='0'></form></td>
		<td class='celda celda-sm-30'><form onsubmit="return view_cliente()"><input type="search" id="s_razon" placeholder='Nombre o Razon Social' class="form-control input-sm" onkeyup="reset_all_view(this.id)"></form></td>
		<td style="width:13.5%" class=" celda celda-sm"><form onsubmit="return view_cliente()"><input type="search" id="s_encargado" placeholder='Nombre de Responsable' class="form-control input-sm" onkeyup="reset_all_view(this.id)"></form></td>
		<td style="width:15.5%" class=" celda celda-sm"><form onsubmit="return view_cliente()"><input type="number" id="s_telefono" placeholder='Telf./Cel.' class="form-control input-sm" onkeyup="reset_all_view(this.id)" min='0'></form></td>
		<td style="width:17%" class=" celda celda-sm"><form onsubmit="return view_cliente()"><input type="search" id="s_direccion" placeholder='Direccion' class="form-control input-sm" onkeyup="reset_all_view(this.id)"></form></td>
		<td style="width:14%" class="celda">
			<?php $new="new_cliente()"; if($privilegio[0]->cl1c!="1"){ $new="";}?>
			<?php $print="imprimir()"; if($privilegio[0]->cl1p!="1"){ $print="";}?>
			<?php $this->load->view('estructura/botones/buscador',['f_buscar'=>"view_cliente()",'f_ver'=>"all_cliente()",'nuevo'=>'Nuevo','f_nuevo'=>$new,'f_imprimir'=>$print]);?>
		</td>
	</tr>
</table>
<script type="text/javascript">Onfocus('s_nit');</script>