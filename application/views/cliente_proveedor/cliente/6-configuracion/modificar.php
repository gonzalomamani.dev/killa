<?php
	$cliente=$cliente[0];
	$help1='title="<h4>Subir Fotografía</h4>" data-content="Seleccione una fotografía, preferiblemente una imagen que no sobrepase las dimenciones de <strong>700x700[px]</strong>, para evitar sobre cargar al sistema"';
	$help2='title="<h4>Número de NIT o CI<h4>" data-content="Ingrese un numero de NIT o CI con valores numericos <b>sin espacios</b>, de 6 a 25 digitos"';
	$help3='title="<h4>Ingresar nombre o razon social<h4>" data-content="Ingrese un Nombre o Razon social alfanumerico de 2 a 100 caracteres <b>puede incluir espacios</b>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help4='title="<h4>Ingresar nombre de resonsable<h4>" data-content="Ingrese un Nombre alfanumerico de 2 a 150 caracteres <b>puede incluir espacios</b>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	
	$help5='title="<h4>teléfono o celular<h4>" data-content="Ingrese un Número de telefono o celular de 7 a 15 digitos <b>sin espacios</b>"';
	$help6='title="<h4>Email<h4>" data-content="Ingrese un Dirección de correo electronico con el siguientes formato ejemplo@dominio.com, <b>sin espacios</b>, el correo debe tener un maximo de 60 caracteres"';
	$help7='title="<h4>Pais<h4>" data-content="Seleccione el pais de domicilio de la empresa, si desea adicionar mas paises, puede hacerlo en la opcion de configuracion en el menu superior"';
	$help8='title="<h4>Ciudad<h4>" data-content="Seleccione la Ciudad de domicilio de la empresa, si la ciudad no aparece en las opciones puede adicionar mas cuidades en la opcion de configuracion en el menu superior"';
	$help9='title="<h4>Dirección<h4>" data-content="Ingrese una de la empresa en formato alfanumerico de 5 a 200 caracteres <b>puede incluir espacios</b>, ademas la direccion solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ/º,-.:)<b>"';
	$help10='title="<h4>Sitio Web<h4>" data-content="Ingrese una url de la pagina de la empresa con el formato http://wwww.ejemplo.com.bo, se acepta una url con una maximo de 150 caracteres"';
	$help11='title="<h4>Observaciónes<h4>" data-content="la observacion puede poseer un formato alfanumerico de 0 a 900 caracteres <b>puede incluir espacios</b>, ademas la observacion solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<ul class="nav nav-tabs">
  <li role="presentation" class="active"><a href="javascript:" onclick="configuracion_cliente('<?php echo $cliente->idcl; ?>')">Modificar</a></li>
  <li role="presentation"><a href="javascript:" onclick="productos_cliente('<?php echo $cliente->idcl; ?>')">Productos</a></li>
</ul>
<div class="row">
	<div class="col-sm-3 col-sm-offset-9 col-xs-12"><strong><span class='text-danger'>(*)</span> Campo obligatorio</strong></div><hr>
	<div class="col-sm-2 col-xs-12"><strong>Fotografía:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<div class="input-group">
			<input class="form-control input-xs" id="up_file" type="file" placeholder='Seleccione fotografia'>
			<span class="input-group-addon" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> NIT/CI:</strong></div>
	<div class="col-sm-10 col-xs-12">
	<form onsubmit="return update_cliente('<?php echo $idcl;?>')">
		<div class="input-group">
			<input class="form-control input-xs" id="up_nit" type="number" placeholder='NIT/CI' min="0" value="<?php echo $cliente->nit;?>">
			<span class="input-group-addon" <?php echo $popover.$help2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</form>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Nombre o Razón Social:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<form onsubmit="return update_cliente('<?php echo $idcl;?>')">
			<div class="input-group col-xs-12">
				<input class="form-control input-xs" id="up_raz" type="text" placeholder='Nombre o Razón Social'  value="<?php echo $cliente->nombre;?>" maxlength="90">
				<span class="input-group-addon" <?php echo $popover.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong>Nombre de Responsable:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<form onsubmit="return update_cliente('<?php echo $idcl;?>')">
			<div class="input-group col-xs-12">
				<input class="form-control input-xs" id="up_res" type="text" placeholder='Nombre del responsable' value="<?php echo $cliente->encargado;?>" maxlength="150">
				<span class="input-group-addon" <?php echo $popover.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong>Telf./Cel.:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<form onsubmit="return update_cliente('<?php echo $idcl;?>')">
			<div class="input-group">
				<input class="form-control input-xs" id="up_tel" type="number" placeholder='Número de teléfono o celular' value="<?php echo $cliente->telefono;?>" min='0'>
				<span class="input-group-addon" <?php echo $popover.$help5;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div>
	<div class="col-sm-2 col-xs-12"><strong>Email:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<form onsubmit="return update_cliente('<?php echo $idcl;?>')">
		<div class="input-group">
			<input class="form-control input-xs" id="up_ema" type="email" placeholder='example@dominio.com' value="<?php echo $cliente->email;?>" maxlength="60">
			<span class="input-group-addon" <?php echo $popover.$help6;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
		</form>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Pais:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<div class="input-group">
			<select class="form-control input-xs" id="up_pai" onchange="view_ciudades('up_ciu',this.value)">
				<option value="">Seleccionar...</option>
			<?php for($i=0; $i<count($paises); $i++){ ?>
				<option value="<?php echo $paises[$i]->idpa;?>"<?php if($cliente->idpa==$paises[$i]->idpa){ echo "selected";}?>><?php echo $paises[$i]->nombre;?></option>
			<?php } ?>
			</select>
			<span class="input-group-addon" <?php echo $popover.$help7;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Ciudad:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<div class="input-group">
			<select class="form-control input-xs" id="up_ciu">
				<option value="">Seleccionar...</option>
				<?php for($i=0; $i<count($ciudades); $i++){ ?>
					<option value="<?php echo $ciudades[$i]->idci;?>"<?php if($cliente->idci==$ciudades[$i]->idci){ echo "selected";}?>><?php echo $ciudades[$i]->nombre;?></option>
				<?php } ?>
				</select>
			</select>
			<span class="input-group-addon" <?php echo $popover.$help8;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong>Dirección:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<form onsubmit="return update_cliente('<?php echo $idcl;?>')">
			<div class="input-group col-xs-12">
				<input class="form-control input-xs" id="up_dir" type="text" placeholder='Z/Villa Fatima C/Saturnino Porcel' value="<?php echo $cliente->direccion;?>" maxlength="200">
				<span class="input-group-addon" <?php echo $popover.$help9;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong>Sitio Web:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<form onsubmit="return update_cliente('<?php echo $idcl;?>')">
			<div class="input-group col-xs-12">
				<input class="form-control input-xs" id="up_web" type="text" placeholder='http://wwww.ejemplo.com.bo' value="<?php echo $cliente->url;?>" maxlength="150">
				<span class="input-group-addon" <?php echo $popover.$help10;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong>Observaciónes:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<div class="input-group col-xs-12">
			<textarea class="form-control input-xs" id="up_obs" placeholder='Observaciónes de Cliente' maxlength="900"><?php echo $cliente->caracteristicas;?></textarea>
			<span class="input-group-addon" <?php echo $popover.$help11;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div><i class="clearfix"></i>
<script language='javascript'>Onfocus("up_nit");$('[data-toggle="popover"]').popover({html:true});</script>