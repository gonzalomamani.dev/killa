<?php $cliente=$cliente[0];
	$url=base_url().'libraries/img/pieza_productos/miniatura/';
?>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="configuracion_cliente('<?php echo $cliente->idcl; ?>')">Modificar</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="productos_cliente('<?php echo $cliente->idcl; ?>')">Productos</a></li>
</ul>
<table class="table table-bordered">
	<tr><thead><th width="92.7%"></th>
		<th width="7.3%"><?php $this->load->view('estructura/botones/buscador',['nuevo'=>'Adicionar Producto al cliente','f_nuevo'=>"form_adicionar_producto_cliente('".$cliente->idcl."')"]);?></th>
	</tr></thead>
</table>
<div class="table-responsive g-table-responsive">
<table class="table table-bordered table-striped table-hover">
<?php if(!empty($productos)){?>
<thead><tr>
		<th class='celda g-thumbnail-modal'><div class="g-img-modal">#Item</div></th>
		<th class="celda-sm-10">Código</th>
		<th class="celda-sm-30">Nombre</th>
		<th style="width:8%" class="celda-sm">Color</th>
		<th style="width:15%">% Utilidad</th>
		<th style="width:10%">Costo de<br>producción<span class="sm-cel"><br></span>(Bs.)</th>
		<th style="width:10%">Presion<br>de venta<br>sugerido<span class="sm-cel"><br></span>(Bs.)</th>
		<th style="width:10%">Presio (Bs.)</th>
		<th style="width:8%"></th>
	</tr>
</thead>
	<?php

		for($i=0; $i < count($productos); $i++) { $producto=$productos[$i];
			$imagen=$this->M_imagen_producto->get_pieza_material($producto->idpim);
			$img="default.png";
			if(count($imagen)>0){ $img=$imagen[0]->nombre; }
			$accesorios=$this->M_producto->get_accesorios($producto->idp);
			$materiales_liquidos=$this->M_producto->get_materiales_liquidos($producto->idp);
			$materiales=$this->M_producto->get_materiales($producto->idp);
			$indirectos=$this->M_producto->get_materiales_indirectos($producto->idp);
			$procesos=$this->M_producto->get_procesos($producto->idp);
			$procesos_pieza=$this->M_producto->get_procesos_pieza($producto->idp);
			$costo_produccion=number_format($this->lib->costo_total_produccion($accesorios,$materiales,$producto->idpim,$materiales_liquidos,$indirectos,$procesos,$procesos_pieza),1,'.','');
		?>
		<tbody>
			<tr>
				<th><div id="item"><?php echo $i+1;?></div><img src="<?php echo $url.$img;?>" width='100%' class='img-thumbnail g-thumbnail-modal'></th>
				<td><?php echo $producto->codigo?></td>
				<td><?php echo $producto->nombre;?><span class="sm-cel" style="font-weight:bold; color: <?php echo $producto->codigo_color;?>"><?php echo "<br>(".$producto->nombre_color.")";?></span></td>
				<td class="celda-sm"><ul class="list-group" style="font-size:10px; padding: 1px">
						<li class="list-group-item" style="padding-top: 5px;padding-bottom: 5px;color:white; background:<?php echo $producto->codigo_color;?>"><?php echo $producto->nombre_color;?></li>
					</ul></td>
				<td>
					<select id="uv<?php echo $producto->idpc;?>" class='form-control input-sm' onchange="calcular_costo('<?php echo $producto->idpc;?>')">
					<?php 
						$por_ganancia=0;
						for ($u=0; $u < count($utilidad_ventas) ; $u++) { $uv=$utilidad_ventas[$u]; ?>
						<option value="<?php echo $uv->iduv.'|'.$uv->porcentaje;?>" <?php if($uv->iduv==$producto->iduv){ echo "selected"; $por_ganancia=$uv->porcentaje;}?>><?php echo ($uv->porcentaje*100).'% '.$uv->nombre;?></option>
					<?php }?>
					</select>
				</td>
				<td><div type="number" class="form-control input-sm text-right" id="cos_pro<?php echo $producto->idpc;?>" disabled><?php echo $costo_produccion;?></div></td>
				<td><div type="number" class="form-control input-sm text-right" id="cos_ven_sug<?php echo $producto->idpc;?>" disabled><?php $sugerido=number_format(($costo_produccion+($costo_produccion*$por_ganancia)),1,'.',''); echo $sugerido;?></div></td>
				<td><form onsubmit="return update_presio_venta('<?php echo $cliente->idcl;?>','<?php echo $producto->idpc;?>')"><input type="number" id="cos_ven<?php echo $producto->idpc;?>" placeholder='0.00' value="<?php echo $producto->costo_unitario;?>" class='form-control input-sm' min='0'></form></td>
				<th>
				<?php $this->load->view("estructura/botones/botones_registros",['guardar'=>"update_presio_venta('".$cliente->idcl."','".$producto->idpc."')",'eliminar'=>"confirmar_producto_cliente('".$cliente->idcl."','".$producto->idpc."','".$producto->nombre."','".$img."')"]);?></th>
			</tr>
		</tbody>
		<?php }//end for ?>
	
</table>
</div>
<table class="table table-bordered">
	<tr><thead><th width="92.7%"></th>
		<th width="7.3%"><?php $this->load->view('estructura/botones/buscador',['nuevo'=>'Adicionar Producto al cliente','f_nuevo'=>"form_adicionar_producto_cliente('".$cliente->idcl."')"]);?></th>
	</tr></thead>
</table>
<?php }//end if ?>