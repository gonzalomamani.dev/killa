
<?php
	$url=base_url().'libraries/img/pieza_productos/miniatura/';
?>


<div class="panel-group" id="accordion">
<?php 
if(count($productos)>0){
  $contador=0;
	for ($i=0; $i < count($productos) ; $i++) { $producto=$productos[$i];
		$img="default.png";
?>
  <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title" style="text-align:left;">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $producto->idp; ?>">
        <?php $swap=$this->M_categoria_producto->get_portada_producto($producto->idp);
          if(count($swap)>0){
            $im=$this->M_imagen_producto->get_pieza_material($swap[0]->idpim);
            $img="default.png";
            if(count($im)>0){ $img=$im[0]->nombre; }
        ?>
              <img src="<?php echo $url.$img;?>" class='img-thumbnail g-thumbnail-modal'>
            <?php
          }//end if?>
           <?php echo $producto->codigo." - <strong>".$producto->nombre."</strong>";?>
        </a>
      </h4>
    </div>
    <div id="collapse<?php echo $producto->idp; ?>" class="panel-collapse collapse">
      <div class="panel-body">    
      <?php $categorias=$this->M_categoria_producto->get_row('idcp',$producto->idcp); //echo count($categorias).' '.$producto->idcp;?>
      <?php if(count($categorias)>0){?>
      <table class="table table-bordered table-striped">
 	<?php for ($ca=0; $ca < count($categorias); $ca++) { $categoria=$categorias[$ca];
        //color
    $mat=$this->M_material->get($categoria->idm);
      $color=$this->M_color->get($mat[0]->idco);
 			$imagenes=$this->M_imagen_producto->get_pieza_material($categoria->idpim);
 			$resto=3-count($imagenes);
      	?>
      	<tr>
      	<?php $mostrado=false;//controla que sea mostrado por lomenos una imagen
      		for ($img=0; $img < count($imagenes) ; $img++){$imagene=$imagenes[$img]; 
      	?>
      		<td width="10%" <?php if($img>0){ echo "class='celda-sm'";}else{ $mostrado=true;}?>><img src="<?php echo $url.$imagene->nombre;?>" width='30px'></td>
      	<?php
      		}
      		for ($img=0; $img < $resto ; $img++) { 
      	?>
      		<td width="10%" <?php if($mostrado){ echo "class='celda-sm'"; }else{ $mostrado=true;}?>><img src="<?php echo $url.'default.png';?>" width='30px'></td>
      	<?php
      		}//end for
      	?>
      		<td width="45%" style="color:<?php echo $color[0]->codigo;?>; font-weight:bold;"><?php echo $producto->nombre,' - '.$color[0]->nombre;?></td>
          <?php $detalle_pedido=$this->M_producto_cliente->get_row_2n('idcl',$idcl,'idpim',$categoria->idpim); 
                $contador++;
          ?>
          <td width="20%">
            <select id="uv<?php echo $contador;?>" class='form-control input-sm'>
            <?php for ($u=0; $u < count($utilidad_ventas) ; $u++) { $uv=$utilidad_ventas[$u] ?>
              <option value="<?php echo $uv->iduv;?>" <?php if(!empty($detalle_pedido)){ if($detalle_pedido[0]->iduv==$uv->iduv){echo "selected";}} ?>><?php echo ($uv->porcentaje*100).'% '.$uv->nombre; ?></option>
            <?php }?>
            </select>
          </td>
      		<th>
      		<?php if(count($detalle_pedido)>0){ $dp=$detalle_pedido[0];?>
      			<div title='Quitar Producto'onclick="save_producto_cliente(this,'default','25','<?php echo $idcl;?>','<?php echo $categoria->idpim;?>','<?php echo $contador;?>')" class='btn-circle btn-circle-red btn-circle-25'></div>
			<?php }else{?>
        <div title='Seleccionar Producto'onclick="save_producto_cliente(this,'red','25','<?php echo $idcl;?>','<?php echo $categoria->idpim;?>','<?php echo $contador;?>')" class='btn-circle btn-circle-default btn-circle-25'></div>
			<?php }?>
      		</th>
      	</tr>
      	<?php
    }?>
      </table>
    <?php }?>
      </div>
    </div>
  </div>
 <?php }//en for
  }else{// end if
    echo "<h3>0 Registros encontrados...</h3>";
  }
    ?>
</div>
