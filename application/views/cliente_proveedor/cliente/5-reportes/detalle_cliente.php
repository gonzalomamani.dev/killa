<?php 
	$cliente=$cliente[0];
	$url=base_url().'libraries/img/personas/miniatura/';
	$img="default.png";
	if($cliente->fotografia!=NULL && $cliente->fotografia!=''){$img=$cliente->fotografia;}
?>
<div class="row">
	<div class="col-sm-3 col-xs-12 text-center"><img src="<?php echo $url.$img;?>" class="img-thumbnail" alt=""><hr></div>
	<div class="col-sm-9 cols-xs-12 table-responsive">
	<table border="0" class="tabla tabla-border-true">
		<tr class="fila">
			<th class="celda th">NIT/CI</th>
			<td class="celda td" colspan="3"><?php echo $cliente->nit;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Nombre/Razon Social:</th><td class="celda td" colspan="3"><?php echo $cliente->nombre;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Responsable:</th><td class="celda td" colspan="3"><?php echo $cliente->encargado;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th" width="17.5%">Pais:</th><td class="celda td" width="17.5%"><?php echo $cliente->pais;?></td>
			<th class="celda th" width="17.5%">Ciudad:</th><td class="celda td" width="17.5%"><?php echo $cliente->ciudad;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Telf/Cel.:</th><td class="celda td" colspan="3"><?php echo $cliente->telefono;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Correo electrónico:</th><td class="celda td" colspan="3"><?php echo $cliente->email;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Sitio Web:</th><td class="celda td" colspan="3"><?php echo $cliente->url;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Dirección:</th><td class="celda td" colspan="3"><?php echo $cliente->direccion;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Observaciones:</th><td class="celda td" colspan="3"><?php echo $cliente->caracteristicas;?></td>
		</tr>
	</table>
	</div>
</div>