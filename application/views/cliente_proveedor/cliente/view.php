<?php $url=base_url().'libraries/img/personas/miniatura/';
	$j_clientes=json_encode($clientes);
?>
<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th class="g-thumbnail">#Item</th>
			<th class="celda-sm-10">NIT/CI</th>
			<th class="celda-sm-30">Nombre o Razon Social</th>
			<th style="width:14%" class="celda-sm">Nombre de Responsable</th>
			<th style="width:15%" class="celda-sm">Teléfono/Celular</th>
			<th style="width:20%" class="celda-sm">Dirección</th>
			<th style="width:11%"></th>
		</tr>
	</thead>
	<tbody>
	<?php 
	if(count($clientes)>0){
	for ($i=0; $i < count($clientes) ; $i++) { $cliente=$clientes[$i];
			$img="default.png";
			if($cliente->fotografia!=NULL && $cliente->fotografia!=''){$img=$cliente->fotografia;}
	?>
		<tr>
			<td class="g-thumbnail"><div id="item"><?php echo $i+1;?></div><div class="g-img"><img src="<?php echo $url.$img;?>" class="img-thumbnail g-thumbnail"></div></td>
			<td><?php echo $cliente->nit;?></td>
			<td><?php echo $cliente->nombre;?></td>
			<td class="celda-sm"><?php echo $cliente->encargado;?></td>
			<td class="celda-sm"><?php echo $cliente->telefono;?></td>
			<td class="celda-sm"><?php echo $cliente->direccion;?></td>
			<td>
				<?php $mod="configuracion_cliente('".$cliente->idcl."')"; if($privilegio[0]->cl1u!="1"){ $mod="";}?>
				<?php $del="confirmar_cliente('".$cliente->idcl."','".$cliente->nombre."','".$img."')"; if($privilegio[0]->cl1d!="1"){ $del="";}?>
				<?php $this->load->view('estructura/botones/botones_registros',['reportes'=>"reportes_cliente('".$cliente->idcl."')",'configuracion'=>$mod,'eliminar'=>$del]);?>
			</td>
		</tr>
	<?php
	}}else{
		echo "<tr><td colspan='7'><h2>0 registros encontrados...</h2></td></tr>";
	}?>
	</tbody>
</table>
<script language="javascript" type="text/javascript"> 
	$("#print").removeAttr("onclick"); 
	$("#print").unbind("click"); 
	$("#print").click(function(){ imprimir_clientes('<?php echo $j_clientes;?>'); }); 
</script>