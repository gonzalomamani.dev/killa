<?php 
	$clientes=json_decode($clientes);
	$contPagina=1;
	$contReg=0;
	$url=base_url().'libraries/img/personas/miniatura/';
?>
<div class="pagina table-responsive">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="88%"><div class='encabezado_titulo'>REGISTRO DE CLIENTES</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'></span></div>
				</td></tr>
			</table>
		</div>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php if(!isset($v1)){?><th class="celda th" width="5%" >#Item</th><?php } ?>
			<?php if(!isset($v2)){?><th class="celda th" width="6%" >Fotografía</th><?php } ?>
			<?php if(!isset($v3)){?><th class="celda th" width="8%">NIT/CI</th><?php } ?>
			<?php if(!isset($v4)){?><th class="celda th" width="12%">Nombre o Razon Social</th><?php } ?>
			<?php if(!isset($v5)){?><th class="celda th" width="13%">Nombre de responsable</th><?php } ?>
			<?php if(!isset($v6)){?><th class="celda th" width="7%">Pais</th><?php } ?>
			<?php if(!isset($v7)){?><th class="celda th" width="7%">Ciudad</th><?php } ?>
			<?php if(!isset($v8)){?><th class="celda th" width="7%">Telf./Cel.</th><?php } ?>
			<?php if(!isset($v9)){?><th class="celda th" width="8%" >Email</th><?php } ?>
			<?php if(!isset($v10)){?><th class="celda th" width="10%" >Dirección</th><?php } ?>
			<?php if(!isset($v11)){?><th class="celda th" width="7%" >Sitio Web</th><?php } ?>
			<?php if(!isset($v12)){?><th class="celda th" width="10%">Observaciónes</th><?php } ?>
		</tr>
		<?php $cont=0; $sub_total=0; $total=0;
			foreach ($clientes as $key => $cliente){ 
				$img='default.png';
				if($cliente->fotografia!=NULL && $cliente->fotografia!=""){ $img=$cliente->fotografia;} ?>
			<?php if($contReg>=$nro){
					$contReg=0;
					$contPagina++;
			?>
		</table><!--cerramos tabla-->
	</div><!--cerramos pagina-->
<div class="pagina table-responsive">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="90%"><div class='encabezado_titulo'>REGISTRO DE CLIENTES</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'></span></div>
				</td></tr>
			</table>
		</div>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php if(!isset($v1)){?><th class="celda th" width="5%" >#Item</th><?php } ?>
			<?php if(!isset($v2)){?><th class="celda th" width="6%" >Fotografía</th><?php } ?>
			<?php if(!isset($v3)){?><th class="celda th" width="8%">NIT/CI</th><?php } ?>
			<?php if(!isset($v4)){?><th class="celda th" width="12%">Nombre o Razon Social</th><?php } ?>
			<?php if(!isset($v5)){?><th class="celda th" width="13%">Nombre de responsable</th><?php } ?>
			<?php if(!isset($v6)){?><th class="celda th" width="7%">Pais</th><?php } ?>
			<?php if(!isset($v7)){?><th class="celda th" width="7%">Ciudad</th><?php } ?>
			<?php if(!isset($v8)){?><th class="celda th" width="7%">Telf./Cel.</th><?php } ?>
			<?php if(!isset($v9)){?><th class="celda th" width="8%" >Email</th><?php } ?>
			<?php if(!isset($v10)){?><th class="celda th" width="10%" >Dirección</th><?php } ?>
			<?php if(!isset($v11)){?><th class="celda th" width="7%" >Sitio Web</th><?php } ?>
			<?php if(!isset($v12)){?><th class="celda th" width="10%">Observaciónes</th><?php } ?>
		</tr>
		<?php $sub_total=0;}//end if ?>
		<tr class="fila">
			<?php if(!isset($v1)){?><td class="celda td"><?php echo $cont+1;$cont++;?></td><?php } ?>
			<?php if(!isset($v2)){?><td class="celda td"><img src="<?php echo $url.$img;?>" style='width:100%'></td><?php } ?>
			<?php if(!isset($v3)){?><td class="celda td"><?php echo $cliente->nit;?></td><?php } ?>
			<?php if(!isset($v4)){?><td class="celda td"><?php echo $cliente->nombre;?></td><?php } ?>
			<?php if(!isset($v5)){?><td class="celda td"><?php echo $cliente->encargado;?></td><?php } ?>
			<?php if(!isset($v6)){?><td class="celda td"><?php echo $cliente->pais;?></td><?php } ?>
			<?php if(!isset($v7)){?><td class="celda td"><?php echo $cliente->ciudad;?></td><?php } ?>
			<?php if(!isset($v8)){?><td class="celda td"><?php echo $cliente->telefono;?></td><?php } ?>
			<?php if(!isset($v9)){?><td class="celda td"><?php echo $cliente->email;?></td><?php } ?>
			<?php if(!isset($v10)){?><td class="celda td"><?php echo $cliente->direccion;?></td><?php } ?>
			<?php if(!isset($v11)){?><td class="celda td"><?php echo $cliente->url;?></td><?php } ?>
			<?php if(!isset($v12)){?><td class="celda td"><?php echo $cliente->caracteristicas;?></td><?php } ?>
		</tr>
	<?php $contReg++;}// end for ?>
	</table>
</div>
<script type="text/javascript"> $(".nroPagina").html(<?php echo $contPagina;?>); </script>