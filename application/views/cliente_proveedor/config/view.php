<?php
	$help1='title="<h4>Nombre de pais<h4>" data-content="Ingrese un nombre alfanumerico <strong>de 2 a 100 caracteres con espacios</strong>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help2='title="<h4>Nombre de ciudad<h4>" data-content="Ingrese un nombre de ciudad alfanumerico <strong>de 2 a 100 caracteres con espacios</strong>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help3='title="<h4>Nombre de pais<h4>" data-content="Seleccione el pais donde pertenece la ciudad, el valor vacio no es aceptado."';
	$help4='title="<h4>Nombre de porcentaje de utilidad<h4>" data-content="Ingrese un nombre alfanumerico <strong>de 2 a 150 caracteres con espacios</strong>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help5='title="<h4>Porcentaje de utilidad<h4>" data-content="Ingrese un porcentaje numerico <strong>de 0% a 100%</strong>, se acepta numeros decimales con un maximo de dos decimales."';
	$popover='data-toggle="popover" data-placement="right" data-trigger="hover"';
	$popover2='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$popover3='data-toggle="popover" data-placement="top" data-trigger="hover"';
?>

<div class="col-sm-6 col-xs-12">
	<h3>Paises</h3>
	<table class="table table-striped table-hover table-bordered">
		<thead>
			<tr>
				<th width="5%">#</th>
				<th width="85%">
				<div class="input-group col-xs-12">
					<span class="input-group-addon" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					<div class="form-control input-sm" disabled>Nombre</div>
				</div></th>
				<th width="10%"></th>
			</tr>
		</thead>
		<tbody>
	<?php for ($i=0; $i < count($paises) ; $i++) { $pais=$paises[$i]; $c=$this->M_ciudad->get_row('idpa',$pais->idpa);?>
		<tr>
			<td><?php echo $i+1;?></td>
			<td>
			<form onsubmit="return update_pais('<?php echo $pais->idpa;?>')"><input type="text" class="form-control input-sm" id="p<?php echo $pais->idpa;?>" placeholder='Nombre de pais' value="<?php echo $pais->nombre;?>" maxlength="100"></form></td>
			<?php $str="";
				if(count($c)>0){
					$str="<hr><strong class='text-danger'>¡Imposible Eliminar el color, esta siendo usado por ".count($c)." ciudad(es)!</strong>";
					$fun="disabled";
				}else{
					$fun="alerta_pais('".$pais->idpa."')";
				}
				$help='title="<h4>Detalles de pais<h4>" data-content="<b>Nombre: </b>'.$pais->nombre.'<br>'.$str.'"';
			?>
			<td><?php $this->load->view('estructura/botones/botones_registros',['detalle'=>$popover3.$help,'guardar'=>"update_pais('".$pais->idpa."')",'eliminar'=>$fun]);?></td>
		</tr>
	<?php } ?>
		</tbody>
		<thead>
			<tr>
				<th colspan="3" class="text-center">NUEVO</th>
			</tr>
			<tr>
				<td colspan="2"><form onsubmit="return save_pais()">
					<div class="input-group col-xs-12">
						<input type="text" id="p" class="form-control input-sm" placeholder='Nombre de pais'>
						<span class="input-group-addon" <?php echo $popover3.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					</div>
				</form></td>
				<td><?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"save_pais()",'detalle'=>"",'eliminar'=>""]);?></td>
			</tr>
		</thead>
	</table>
	<h3>Porcentajes de utilidad de venta</h3>
	<table class="table table-bordered table-striped table-hover">
		<thead><tr>
			<th width="5%">#</th>
			<th width="40%"><div class="input-group col-xs-12">
				<span class="input-group-addon" <?php echo $popover.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
				<div class="form-control input-sm" disabled>Detalle</div>
			</div></th>
			<th width="25%"><div class="input-group col-xs-12">
				<span class="input-group-addon" <?php echo $popover3.$help5;?>><i class='glyphicon glyphicon-info-sign'></i></span>
				<div class="form-control input-sm" disabled>% Porcent.</div>
			</div></th>
			<th width="10%"></th>
		</tr></thead>
		<tbody>
	<?php for ($i=0; $i<count($utilidad_ventas); $i++){ $uv=$utilidad_ventas[$i]; $control=$this->M_producto_cliente->get_row('iduv',$uv->iduv);?>
		<tr>
			<td><?php echo $i+1;?></td>
			<td><textarea class='form-control input-sm' id='pu_des<?php echo $uv->iduv;?>' maxlength="150"><?php echo $uv->nombre;?></textarea></td>
			<?php $porcentaje=$uv->porcentaje*100; ?>
			<td><form onsubmit="return update_porcentaje('<?php echo $uv->iduv;?>')"><input type="number" class="form-control input-sm" placeholder='100%' id="pu_por<?php echo $uv->iduv;?>" value='<?php echo $porcentaje;?>' min="0" max="100"></form></td>
			<?php $str="";
				if(count($control)>0){
					$str="<hr><strong class='text-danger'>¡Imposible Eliminar, el porcentaje de utilidad esta siendo usado por ".count($control)." material(es) en el(los) cliente(s)!</strong>";
					$fun="disabled";
				}else{
					$fun="alerta_porcentaje('".$uv->iduv."')";
				}
				$help='title="<h4>Detalles de unilidad de venta<h4>" data-content="<b>'.$uv->nombre.'</b><br> representa a un '.($uv->porcentaje*100).'% de utilidad'.$str.'"';
			?>
			<td><?php $this->load->view('estructura/botones/botones_registros',['detalle'=>$popover3.$help,'guardar'=>"update_porcentaje('".$uv->iduv."')",'eliminar'=>$fun]);?></td>
		</tr>
	<?php } ?>
		</tbody>
		<thead><tr><th colspan="4" class="text-center">NUEVO</th></tr>
		<tr>
			<td colspan="2">
			<div class="input-group col-xs-12">
				<textarea class="form-control input-sm" placeholder='Detalle de Utilidad' id="pu_des" maxlength="150"></textarea>
				<span class="input-group-addon" <?php echo $popover3.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div></td>
			<td><form onsubmit="return save_porcentaje()">
			<div class="input-group col-xs-12"><input type="number" class="form-control input-sm" placeholder='100%' id="pu_por" min="0" max="100">
				<span class="input-group-addon" <?php echo $popover3.$help5;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div></form></td>

			<td><?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"save_porcentaje()",'detalle'=>"",'eliminar'=>""]);?></td>
		</tr></thead>
	</table>
</div>
<div class="col-sm-6 col-xs-12">
	<h3>Ciudades</h3>
	<table class="table table-striped table-hover table-bordered">
		<thead>
			<tr>
				<th width="5%">#</th>
				<th width="45%">
				<div class="input-group col-xs-12">
					<span class="input-group-addon" <?php echo $popover.$help2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					<div class="form-control input-sm" disabled>Nombre</div>
				</div></th>
				<th width="40%">
				<div class="input-group col-xs-12">
					<span class="input-group-addon" <?php echo $popover2.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					<div class="form-control input-sm" disabled>Pais</div>
				</div></th>
				<th width="10%"></th>
			</tr>
		</thead>
		<tbody>
	<?php for ($i=0; $i < count($ciudades) ; $i++) { $ciudad=$ciudades[$i]; $p=$this->M_persona->get_row('idci',$ciudad->idci);?>
		<tr>
			<td><?php echo $i+1;?></td>
			<td><input type="text" class="form-control input-sm" id="c<?php echo $ciudad->idci;?>" placeholder='Nombre de ciudad' value="<?php echo $ciudad->ciudad;?>" maxlength="100"></td>
			<td>
				<select id="cp<?php echo $ciudad->idci;?>" class="form-control input-sm">
					<option value="">Seleccionar...</option>
				<?php for ($j=0; $j < count($paises) ; $j++) { $pais=$paises[$j]; ?>
					<option value="<?php echo $pais->idpa;?>" <?php if($pais->idpa==$ciudad->idpa){ echo "selected";}?>><?php echo $pais->nombre;?></option>
				<?php }?>
				</select>
			</td>
			<?php $str="";
				if(count($p)>0){
					$str="<hr><strong class='text-danger'>¡Imposible Eliminar el la ciudad, esta siendo usada por ".count($p)." persona(s)!</strong>";
					$fun="disabled";
				}else{
					$fun="alerta_ciudad('".$ciudad->idci."')";
				}
				$help='title="<h4>Detalles de ciudad<h4>" data-content="<b>Nombre: </b>'.$ciudad->ciudad.'<br>'.$str.'"';
			?>
			<td><?php $this->load->view('estructura/botones/botones_registros',['detalle'=>$popover3.$help,'guardar'=>"update_ciudad('".$ciudad->idci."')",'eliminar'=>$fun]);?></td>
		</tr>
	<?php } ?>
		</tbody>
		<thead>
			<tr>
				<th colspan="3" class="text-center">NUEVO</th>
			</tr>
			<tr>
				<td colspan="2"><form onsubmit="return save_ciudad()">
					<div class="input-group col-xs-12">
						<input type="text" id="c" class="form-control input-sm" placeholder='Nombre de ciudad'>
						<span class="input-group-addon" <?php echo $popover3.$help2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					</div>
				</form></td>
				<td>
				<div class="input-group col-xs-12">
					<select id="cp" class="form-control input-sm">
						<option value="">Seleccionar...</option>
					<?php for ($i=0; $i < count($paises) ; $i++) { $pais=$paises[$i]; ?>
						<option value="<?php echo $pais->idpa;?>"><?php echo $pais->nombre;?></option>
					<?php }?>
					</select>
					<span class="input-group-addon" <?php echo $popover3.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
				</div>
				</td>
				<td><?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"save_ciudad()",'detalle'=>"",'eliminar'=>""]);?></td>
			</tr>
		</thead>
	</table>
</div>
<script language='javascript'>$('[data-toggle="popover"]').popover({html:true});</script>