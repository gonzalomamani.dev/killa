<?php 
	$url=base_url().'libraries/img/personas/miniatura/';
	$j_proveedores=json_encode($proveedores);
?>
<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th class="g-thumbnail">#Item</th>
			<th class="celda-sm-10">NIT/CI</th>
			<th class="celda-sm-30">Nombre o Razon Social</th>
			<th style="width:14%" class="celda-sm">Nombre de Responsable</th>
			<th style="width:15%" class="celda-sm">Teléfono/Celular</th>
			<th style="width:20%" class="celda-sm">Dirección</th>
			<th style="width:11%"></th>
		</tr>
	</thead>
	<tbody>
	<?php if(count($proveedores)>0){ for ($i=0; $i < count($proveedores) ; $i++) { $proveedor=$proveedores[$i];
			$img="default.png";
			if($proveedor->fotografia!=NULL && $proveedor->fotografia!=''){ $img=$proveedor->fotografia;}
	?>
		<tr>
			<td class="g-thumbnail"><div id="item"><?php echo $i+1;?></div><div class="g-img"><img src="<?php echo $url.$img;?>" class="img-thumbnail g-thumbnail"></div></td>
			<td><?php echo $proveedor->nit;?></td>
			<td><?php echo $proveedor->nombre;?></td>
			<td class="celda-sm"><?php echo $proveedor->encargado;?></td>
			<td class="celda-sm"><?php echo $proveedor->telefono;?></td>
			<td class="celda-sm"><?php echo $proveedor->direccion;?></td>
			<td>
				<?php $mod="configuracion_proveedor('".$proveedor->idpro."')"; if($privilegio[0]->cl2u!="1"){ $mod="";}?>
				<?php $del="confirmar_proveedor('".$proveedor->idpro."')"; if($privilegio[0]->cl2d!="1"){ $del="";}?>
				<?php $this->load->view('estructura/botones/botones_registros',['reportes'=>"reportes_proveedor('".$proveedor->idpro."')",'configuracion'=>$mod,'eliminar'=>$del]);?>
			</td>
		</tr>
	<?php }//end for
	}else{
		echo "<tr><td colspan='7'><h2>0 registros encontrados...</h2></td></tr>";
	}?>
	</tbody>
</table>
<script language="javascript" type="text/javascript"> 
	$("#print").removeAttr("onclick"); 
	$("#print").unbind("click"); 
	$("#print").click(function(){ imprimir_proveedor('<?php echo $j_proveedores;?>'); }); 
</script>