<?php
	$url=base_url().'libraries/img/materiales_varios/miniatura/';
?>
<table class="table table-bordered table-striped">
  <thead><tr><th width="9%">#Item</th><th width="12%">Código</th><th width="79%">Nombre</th><th></th></tr></thead>
  <tbody>
  <?php 
  if(count($materiales)>0){
  for ($i=0; $i < count($materiales) ; $i++) { $material=$materiales[$i];
    $img='default.png';
    if($material->fotografia!=NULL && $material->fotografia!=''){$img=$material->fotografia;}
  ?>
     <tr>
       <th><div id="item"><?php echo $i+1;?></div><img src="<?php echo $url.$img;?>" width="100%" class='img-thumbnail'></th>
       <th><?php echo $material->codigo;?></th>
       <th><?php echo $material->nombre;?></th>
       <?php $control=$this->M_material_proveedor->get_row_2n('idpro',$idpro,'idmi',$material->idmi);?>
       <th>
        <?php if(count($control)>0){ ?>
            <div class="btn-circle btn-circle-red btn-circle-25" title='Quitar Material'onclick="save_material_proveedor_2('<?php echo $idpro;?>','<?php echo $material->idmi;?>',this,'red','25')"></div>
          <?php  
        }else{ ?>
            <div class="btn-circle btn-circle-default btn-circle-25" title="Seleccionar Material" onclick="save_material_proveedor_2('<?php echo $idpro;?>','<?php echo $material->idmi;?>',this,'red','25')"></div>
        <?php }?>
       </th>
     </tr>
  <?php }// end for 
  }else{// end if
    echo "<tr><td colspan='4'><h3>0 Registros encontrados...</h3></td></tr>";
  }?>
</tbody>
</table>
