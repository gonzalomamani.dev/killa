<?php $url=base_url().'libraries/img/materiales/miniatura/'; ?>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="configuracion_proveedor('<?php echo $idpro; ?>')">Modificar</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="materiales_proveedor('<?php echo $idpro; ?>')">Materiales</a></li>
</ul>
<table class="table table-bordered table-striped table-hover">
<thead>
	<tr><th colspan="5" width="90%"></th><th width="10%">
	<?php $this->load->view('estructura/botones/buscador',['nuevo'=>'Adicionar material al proveedor','f_nuevo'=>"form_adicionar_material_proveedor('".$idpro."')"]);?>
	</th></tr>
</thead>
</table>
<div class="table-responsive g-table-responsive">
<table class="table table-bordered table-striped table-hover">
<?php $url=base_url().'libraries/img/materiales/miniatura/'; ?>
<?php if(count($materiales)>0){?>
<thead><tr>
	<th class='celda g-thumbnail-modal'><div class="g-img-modal">#Item</div></th>
	<th class="celda-sm-10">Código</th>
	<th class="celda-sm-50">Nombre</th>
	<th style="width:10%">Color</th>
	<th style="width:20%">Presio de<br>compra(Bs.)</th>
	<th style="width:10%"></th>
</tr></thead>
<tbody><?php for($i=0; $i < count($materiales); $i++) { $material=$materiales[$i];
			$img="default.png"; if($material->fotografia!='' && $material->fotografia!=NULL){$img=$material->fotografia;} ?>
			<tr>
				<th><div id="item"><?php echo $i+1;?></div><img src="<?php echo $url.$img;?>" width='100%' class='img-thumbnail g-thumbnail-modal'></th>
				<td><?php echo $material->codigo;?></td>
				<td><?php echo $material->nombre;?></td>
				<td><ul class="list-group" style="font-size:10px; padding: 1px"><li class="list-group-item" style="padding-top: 5px;padding-bottom: 5px;color:white; background:<?php echo $material->codigo_color;?>"><?php echo $material->nombre_color;?></li></ul></td>
				<td><form onsubmit="return update_presio_compra('<?php echo $idpro;?>','<?php echo $material->idmp;?>')"><input type="number" id="presio<?php echo $material->idmp;?>" placeholder='costo por unidad' value="<?php echo $material->costo_unitario;?>" class='form-control input-sm'></form></td>
				<th><?php $this->load->view("estructura/botones/botones_registros",['guardar'=>"update_presio_compra('".$idpro."','".$material->idmp."')",'eliminar'=>"confirmar_material_proveedor('".$idpro."','".$material->idmp."','almacen')"]);?></th>
			</tr>
		<?php } // en for?>
</tbody>
<?php }//enf if ?>
<?php 	
	$help1='title="<h4>Presio por unidad<h4>" data-content="Imposible asignar un presio por unidad a este grupo de materiales."';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$url=base_url().'libraries/img/materiales_adicionales/miniatura/'; 
?>
<?php if(count($materiales_adicionales)>0){?>
<thead><tr>
	<th class='celda g-thumbnail-modal'><div class="g-img-modal">#Item</div></th>
	<th class="celda-sm-10">Código</th>
	<th colspan="2" class="celda-sm-60">Materiales Indirectos</th>
	<th style="width:20%">Presio por uso<br>o compra(Bs.)</th>
	<th style="width:10%"></th>
</tr></thead>
<tbody><?php for($i=0; $i < count($materiales_adicionales); $i++) { $material=$materiales_adicionales[$i];
			$img="default.png"; if($material->fotografia!='' && $material->fotografia!=NULL){$img=$material->fotografia;} ?>
			<tr>
				<th><div id="item"><?php echo $i+1;?></div><img src="<?php echo $url.$img;?>" width='100%' class='img-thumbnail g-thumbnail-modal'></th>
				<td><?php echo $material->codigo;?></td>
				<td colspan="2"><?php echo $material->nombre;?></td>
				<td><div class="input-group">
					<input type="number" class='form-control input-sm' disabled="">
				<span class="input-group-addon" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
				</div></td>
				<th><?php $this->load->view("estructura/botones/botones_registros",['guardar'=>"",'eliminar'=>"confirmar_material_proveedor('".$idpro."','".$material->idmp."','material_adicional')"]);?></th>
			</tr>
		<?php } // en for?>
</tbody>
<?php }//enf if?>

<?php 
	$url=base_url().'libraries/img/materiales_varios/miniatura/'; 
if(count($materiales_varios)>0){?>
<thead><tr>
	<th class='celda g-thumbnail-modal'><div class="g-img-modal">#Item</div></th>
	<th class="celda-sm-10">Código</th>
	<th colspan="2" class="celda-sm-60">Otro tipo de<br>Materiales o insumos</th>
	<th style="width:20%">Presio de compra<br>por unidad(Bs.)</th>
	<th style="width:10%"></th>
</tr></thead>
<tbody><?php for($i=0; $i < count($materiales_varios); $i++) { $material=$materiales_varios[$i];
			$img="default.png"; if($material->fotografia!='' && $material->fotografia!=NULL){$img=$material->fotografia;} ?>
			<tr>
				<th><div id="item"><?php echo $i+1;?></div><img src="<?php echo $url.$img;?>" width='100%' class='img-thumbnail g-thumbnail-modal'></th>
				<td><?php echo $material->codigo;?></td>
				<td colspan="2"><?php echo $material->nombre;?></td>
				<td>
				<div class="input-group">
					<input type="number" class='form-control input-sm' disabled="">
				<span class="input-group-addon" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
				</div>
				</td>
				<th><?php $this->load->view("estructura/botones/botones_registros",['guardar'=>"",'eliminar'=>"confirmar_material_proveedor('".$idpro."','".$material->idmp."','materiales_varios')"]);?></th>
			</tr>
		<?php } // en for?>
</tbody>
<?php }//enf if?>

<?php 
$url=base_url().'libraries/img/activos_fijos/miniatura/'; 
if(count($activos_fijos)>0){?>
<thead><tr>
	<th class='celda g-thumbnail-modal'><div class="g-img-modal">#Item</div></th>
	<th class="celda-sm-10">Código</th>
	<th colspan="2" class="celda-sm-60">Activo Fijo</th>
	<th style="width:20%">Presio de compra<br>por unidad(Bs.)</th>
	<th style="width:10%"></th>
</tr></thead>
<tbody><?php for($i=0; $i < count($activos_fijos); $i++) { $material=$activos_fijos[$i];
			$img="default.png"; if($material->fotografia!='' && $material->fotografia!=NULL){$img=$material->fotografia;} ?>
			<tr>
				<th><div id="item"><?php echo $i+1;?></div><img src="<?php echo $url.$img;?>" width='100%' class='img-thumbnail g-thumbnail-modal'></th>
				<td><?php echo $material->codigo;?></td>
				<td colspan="2"><?php echo $material->nombre;?></td>
				<td><form onsubmit="return update_presio_compra('<?php echo $idpro;?>','<?php echo $material->idmp;?>')"><input type="number" id="presio<?php echo $material->idmp;?>" placeholder='costo por unidad' value="<?php echo $material->costo_unitario;?>" class='form-control input-sm'></form></td>
				<th><?php $this->load->view("estructura/botones/botones_registros",['guardar'=>"update_presio_compra('".$idpro."','".$material->idmp."')",'eliminar'=>"confirmar_material_proveedor('".$idpro."','".$material->idmp."','activos_fijos')"]);?></th>
			<!--<th><?php $this->load->view("estructura/botones/botones_registros",['guardar'=>"",'eliminar'=>"confirmar_material_proveedor('".$idpro."','".$material->idmp."','activos_fijos')"]);?></th>-->
			</tr>
		<?php } // en for?>
</tbody>
</table>
</div>
<table class="table table-bordered table-striped table-hover">
<thead>
	<tr><th colspan="5" width="90%"></th><th width="10%">
	<?php $this->load->view('estructura/botones/buscador',['nuevo'=>'Adicionar material al proveedor','f_nuevo'=>"form_adicionar_material_proveedor('".$idpro."')"]);?>
	</th></tr>
</thead>
</table>
<?php }//enf if?>
<script language='javascript'>$('[data-toggle="popover"]').popover({html:true});</script>