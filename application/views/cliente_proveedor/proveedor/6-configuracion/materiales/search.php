<?php
  $help1='title="<h4>Almacen<h4>" data-content="Seleccione el almacen donde desea buscar el material, si no selecciona ningun almacen se mostrara por defectos todos los materiales de todos los almacenes"';
  $popover='data-toggle="popover" data-placement="left" data-trigger="hover" ';
?>
<ul class="nav nav-tabs">
  <li role="presentation" class="active"><a href="javascript:" onclick="form_adicionar_material_proveedor('<?php echo $idpro; ?>')">Materiales</a></li>
  <li role="presentation"><a href="javascript:" onclick="form_adicionar_proveedor_indirectos('<?php echo $idpro; ?>')">Materiales indirectos</a></li>
  <li role="presentation"><a href="javascript:" onclick="form_adicionar_proveedor_insumos('<?php echo $idpro; ?>')">Otros Insumos</a></li>
  <li role="presentation"><a href="javascript:" onclick="form_adicionar_proveedor_activos('<?php echo $idpro; ?>')">Activos fijos</a></li>
</ul>


<table class="tabla tabla-border-false">
  <tr class="fila">
    <td class="celda" width="7%"></td>
    <td class="celda" width="13%"><form onsubmit="return search_proveedor_material('<?php echo $idpro;?>')"> <input type="search" class="form-control input-sm" id="s_cod2" onkeyup="reset_input_2(this.id)" placeholder='Código'></form></td>
    <td class="celda" width="40%"><form onsubmit="return search_proveedor_material('<?php echo $idpro;?>')"> <input type="search" class="form-control input-sm" id="s_nom2" onkeyup="reset_input_2(this.id)" placeholder='Nombre de material'></form></td>
    <td class="celda" width="33%">
      <div class="input-group col-xs-12">
        <select class="form-control input-sm" id="s_alm2" onchange="search_proveedor_material('<?php echo $idpro?>')">
          <option value="">Seleccione...</option>
            <?php for ($i=0; $i < count($almacenes) ; $i++) { $almacen=$almacenes[$i]; ?>
              <option value="<?php echo $almacen->ida;?>"><?php echo $almacen->nombre;?></option>
            <?php } ?>
        </select>
        <span class="input-group-addon" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
      </div>
    </td>
    <td class="celda" width="7%">
      <?php $this->load->view('estructura/botones/buscador',['f_buscar'=>"search_proveedor_material('".$idpro."')"]);?>
    </td>
  </tr>
</table>
<div class="row-border" id="contenido_2"></div>
<script language='javascript'>Onfocus("s_cod_3");$('[data-toggle="popover"]').popover({html:true});</script>