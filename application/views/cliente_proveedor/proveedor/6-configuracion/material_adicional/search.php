
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="form_adicionar_material_proveedor('<?php echo $idpro; ?>')">Materiales</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="form_adicionar_proveedor_indirectos('<?php echo $idpro; ?>')">Materiales indirectos</a></li>
  <li role="presentation"><a href="javascript:" onclick="form_adicionar_proveedor_insumos('<?php echo $idpro; ?>')">Otros Insumos</a></li>
  <li role="presentation"><a href="javascript:" onclick="form_adicionar_proveedor_activos('<?php echo $idpro; ?>')">Activos fijos</a></li>
</ul>

<table class="tabla tabla-border-false">
  <tr class="fila">
    <td class="celda" width="10%"></td>
    <td class="celda" width="13%"><form onsubmit="return search_proveedor_indirectos('<?php echo $idpro;?>')"> <input type="search" class="form-control input-sm" id="s_cod2" onkeyup="reset_input_2(this.id)" placeholder='Código'></form></td>
    <td class="celda" width="70%"><form onsubmit="return search_proveedor_indirectos('<?php echo $idpro;?>')"> <input type="search" class="form-control input-sm" id="s_nom2" onkeyup="reset_input_2(this.id)" placeholder='Nombre de material'></form></td>
    <td class="celda" width="7%">
      <?php $this->load->view('estructura/botones/buscador',['f_buscar'=>"search_proveedor_indirectos('".$idpro."')"]);?>
    </td>
  </tr>
</table>
<div class="row-border" id="contenido_2"></div>
<script language='javascript'>Onfocus("s_cod_3");</script>