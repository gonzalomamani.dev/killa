<?php 
	$contPagina=1;
	$contReg=0;
?>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="detalle_empleado('<?php echo $empleado->ide;?>')">Detalle</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="ficha_control('<?php echo $empleado->ide;?>',false)">Ficha de control</a></li>
</ul>
<div class="row-border table-responsive">
	<div class="col-sm-2 col-sm-offset-5 col-xs-12">
		<select id="max" class="form-control input-sm" onchange="ficha_control('<?php echo $empleado->ide;?>',true)">
			<?php for ($i=1; $i <= 800 ; $i++) { ?>
				<option value="<?php echo $i;?>" <?php if($i==$max){echo "selected";}?>><?php echo $i;?></option>
			<?php } ?>
		</select>
	</div>
	<div class="col-sm-2 col-xs-12">
		<select id="nro" class="form-control input-sm" onchange="ficha_control('<?php echo $empleado->ide;?>',true)">
			<?php for ($i=1; $i <= 80 ; $i++) { ?>
				<option value="<?php echo $i;?>" <?php if($i==$nro){echo "selected";}?>><?php echo $i;?></option>
			<?php } ?>
		</select>
	</div>
	<div class="col-sm-3 col-xs-12">
		<select id="ges" class="form-control input-sm" onchange="ficha_control('<?php echo $empleado->ide;?>',true)">
			<?php for ($i=2015; $i <= 2030 ; $i++) { ?>
				<option value="<?php echo $i;?>" <?php if($i==$ges){echo "selected";}?>><?php echo $i;?></option>
			<?php } ?>
		</select>
	</div>
</div>
<div id="area">
	<div class="pagina table-responsive">
		<?php $this->load->view('estructura/print/encabezado',['titulo'=>'HOJA DE PRODUCCIÓN - MAESTRO DE '.$this->lib->all_mayuscula($empleado->nombre_proceso)." - ".$ges,'pagina'=>$contPagina,'sub_titulo'=>'<strong>NOMBRE: </strong>'.$empleado->nombre." ".$empleado->paterno." ".$empleado->materno.". <strong>AYUDANTE:</strong>....................................................................","nro" => false]);?>
		<table class="tabla tabla-border-true">
			<tr class="fila">
				<th class="celda th" rowspan="2" width="3%">N°</th>
				<th class="celda th" rowspan="2" width="32">NOMBRE DE PRODUCTO</th>
				<th class="celda th" rowspan="2" width="5%">CANT.</th>
				<th class="celda th" colspan="3" width="18%">FECHA DE INICIO</th>
				<th class="celda th" colspan="3" width="18%">FECHA DE ENTREGA</th>
				<th class="celda th" rowspan="2" width="24%">OBS. SUGERENCIAS</th>
			</tr>
			<tr class="fila">
				<th class="celda th">Mes</th>
				<th class="celda th" width="4%">Dia</th>
				<th class="celda th" width="5%">Hora</th>
				<th class="celda th">Mes</th>
				<th class="celda th" width="4%">Dia</th>
				<th class="celda th" width="5%">Hora</th>
			</tr>
		<?php $cont=0; 
				for($i=0; $i<$max; $i++){ 
				if($nro<=$cont){
					$contPagina++;
					$cont=0;
		?>
				</table>
				</div>
			<div class="pagina table-responsive">
		<?php $this->load->view('estructura/print/encabezado',['titulo'=>'HOJA DE PRODUCCIÓN - MAESTRO DE '.$this->lib->all_mayuscula($empleado->nombre_proceso)." ".$ges,'pagina'=>$contPagina,'sub_titulo'=>'<strong>NOMBRE: </strong>'.$empleado->nombre." ".$empleado->paterno." ".$empleado->materno.". <strong>AYUDANTE:</strong>...................................................................."]);?>
		<table class="tabla tabla-border-true">
			<tr class="fila">
				<th class="celda th" rowspan="2" width="3%">N°</th>
				<th class="celda th" rowspan="2" width="32">NOMBRE DE PRODUCTO</th>
				<th class="celda th" rowspan="2" width="5%">CANT.</th>
				<th class="celda th" colspan="3" width="18%">FECHA DE INICIO</th>
				<th class="celda th" colspan="3" width="18%">FECHA DE ENTREGA</th>
				<th class="celda th" rowspan="2" width="24%">OBS. SUGERENCIAS</th>
			</tr>
			<tr class="fila">
				<th class="celda th">Mes</th>
				<th class="celda th" width="4%">Dia</th>
				<th class="celda th" width="5%">Hora</th>
				<th class="celda th">Mes</th>
				<th class="celda th" width="4%">Dia</th>
				<th class="celda th" width="5%">Hora</th>
			</tr>
		<?php
				}//end if
		?>
			<tr class="fila">
				<td class="celda td" style="font-size: .736em;"><?php echo $i+1;?></td>
				<td class="celda td"></td>
				<td class="celda td"></td>
				<td class="celda td"></td>
				<td class="celda td"></td>
				<td class="celda td"></td>
				<td class="celda td"></td>
				<td class="celda td"></td>
				<td class="celda td"></td>
				<td class="celda td"></td>

			</tr>
		<?php
			$cont++;
			} ?>

		</table>
	</div>
</div>
<script type="text/javascript"> $(".nroPagina").html(<?php echo $contPagina;?>); </script>
