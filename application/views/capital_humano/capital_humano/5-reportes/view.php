<?php 
	$url=base_url().'libraries/img/personas/miniatura/';
	$img='default.png';
	$contPagina=1;
?>
<ul class="nav nav-tabs">
  <li role="presentation" class="active"><a href="javascript:" onclick="detalle_empleado('<?php echo $empleado->ide;?>')">Detalle</a></li>
  <li role="presentation"><a href="javascript:" onclick="ficha_control('<?php echo $empleado->ide;?>',false)">Ficha de control</a></li>
</ul>
<div class="detalle" id="area">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="88%"><div class='encabezado_titulo'>DETALLE DE EMPLEADOS</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'>1</span></div>
				</td></tr>
			</table>
		</div>
	<span class='titulo'>DATOS PERSONALES</span><hr>
	<table border="0" class="" width="100%">
		<tr>
		<?php if($empleado->fotografia!="" & $empleado->fotografia!=NULL){ $img=$empleado->fotografia; } ?>
			<td rowspan="9" width="20%"><img src="<?php echo $url.$img; ?>" width='100%'></td>
		</tr>
		<tr><td><span class='negrilla'>Nombre: </span><?php echo $empleado->nombre;?><?php if($empleado->nombre2!=""){ echo " ".$empleado->nombre2; } ?><?php echo " ".$empleado->paterno." ".$empleado->materno; ?></td></tr>
		<tr><td><span class='negrilla'>Cédula de Identidad: </span><?php echo $empleado->ci." ".$empleado->ciudad;?></td></tr>
		<tr><td><span class='negrilla'>Código: </span><?php echo $empleado->codigo;?></td></tr>
		<tr><td><span class='negrilla'>Fecha de Nacimiento: </span><?php echo $this->validaciones->formato_fecha($empleado->fecha_nacimiento,'d,m,Y')?></td></tr>
		<tr><td><span class='negrilla'>Teléfono: </span><?php if($empleado->telefono!=0 && $empleado->telefono!=''){ echo $empleado->telefono;}?></td></tr>
		<tr><td><span class='negrilla'>Correo Electrónico: </span><?php if($empleado->email!=null && $empleado->email!=''){ echo $empleado->email;}?></td></tr>
		<tr><td><span class='negrilla'>Dirección: </span><?php if($empleado->direccion!=null && $empleado->direccion!=''){ echo $empleado->direccion;}?></td></tr>
		<tr><td><span class='negrilla'>Edad: </span><?php echo $this->validaciones->calcula_edad($empleado->fecha_nacimiento)." años";?></td></tr>
		
	</table>
	<span class='titulo'>DATOS LABORALES EN LA EMPRESA</span><hr>
	<table border="0" class="" width="100%">
	
	<tr><td><span class='negrilla'>Fecha de ingreso: </span><?php echo $this->validaciones->formato_fecha($empleado->fecha_ingreso,'li,d,m,Y')?></td></tr>
	<tr><td><span class='negrilla'>Antiguedad: </span><?php echo $this->validaciones->formato_antiguedad($empleado->fecha_ingreso);?></td></tr>
	<tr><td><span class='negrilla'>Tipo de Contrato: </span><?php if($empleado->tipo_contrato==0){ echo "Tiempo completo";}else{ echo "Medio Tiempo";}?></td></tr>
	<tr><td><span class='negrilla'>Carga Horaria: </span><?php echo $empleado->horas;?> Horas/dia</td></tr>
	<tr><td><span class='negrilla'>Suedo: </span><?php echo $empleado->salario;?> Bs.</td></tr>
	<tr><td><span class='negrilla'>Area de Trabajo: </span><?php echo $empleado->cargo;?></td></tr>
	</table>
	<span class='titulo'>DATOS ADICIONALES</span><hr>
	<table border="0" class="" width="100%">
	<tr><td><span class='negrilla'>Grado académico: </span><?php echo $empleado->formacion;?></td></tr>
	<tr><td><span class='negrilla'>Caracteristicas Personales: </span><?php echo $empleado->caracteristicas;?></td></tr>
	</table>
</div>
</body>

	