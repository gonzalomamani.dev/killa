<?php 
	$empleados=json_decode($empleados);
	$contPagina=1;
	$contReg=0;
	$url=base_url().'libraries/img/personas/miniatura/';
?>
<div class="pagina table-responsive">
	<?php $this->load->view('estructura/print/encabezado',['titulo'=>'REGISTRO DE EMPLEADOS','pagina'=>$contPagina,'sub_titulo'=> '']);?>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php if(!isset($v1)){?><th class="celda th" width="3%" >#Item</th><?php } ?>
			<?php if(!isset($v2)){?><th class="celda th" width="5%" >Fotografía</th><?php } ?>
			<?php if(!isset($v3)){?><th class="celda th" width="8%" >Nº de Cédula</th><?php } ?>
			<?php if(!isset($v4)){?><th class="celda th" width="3%" >Cod. Bio.</th><?php } ?>
			<?php if(!isset($v5)){?><th class="celda th" width="11%">Nombre completo</th><?php } ?>
			<?php if(!isset($v6)){?><th class="celda th" width="9%">Cargo en la empresa</th><?php } ?>
			<?php if(!isset($v7)){?><th class="celda th" width="7%">Procesos que realiza</th><?php } ?>
			<?php if(!isset($v8)){?><th class="celda th" width="7%" >Tipo de contrato</th><?php } ?>
			<?php if(!isset($v9)){?><th class="celda th" width="4%" >Salario (Bs.)</th><?php } ?>
			<?php if(!isset($v10)){?><th class="celda th" width="5%" >Telf./Cel.</th><?php } ?>
			<?php if(!isset($v11)){?><th class="celda th" width="8%" >Email</th><?php } ?>
			<?php if(!isset($v12)){?><th class="celda th" width="12%">Dirección</th><?php } ?>
			<?php if(!isset($v13)){?><th class="celda th" width="18%">Caracteristicas</th><?php } ?>
		</tr>
		<?php $cont=0; $sub_total=0; $total=0;
			foreach ($empleados as $key => $empleado){ 
				$img='default.png';
				if($empleado->fotografia!=NULL && $empleado->fotografia!=""){ $img=$empleado->fotografia;} ?>
			<?php if($contReg>=$nro){
					$contReg=0;
					$contPagina++;
			?>
		</table><!--cerramos tabla-->
	</div><!--cerramos pagina-->
<div class="pagina table-responsive">
	<?php $this->load->view('estructura/print/encabezado',['titulo'=>'REGISTRO DE EMPLEADOS','pagina'=>$contPagina,'sub_titulo'=> '']);?>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php if(!isset($v1)){?><th class="celda th" width="3%" >#Item</th><?php } ?>
			<?php if(!isset($v2)){?><th class="celda th" width="5%" >Fotografía</th><?php } ?>
			<?php if(!isset($v3)){?><th class="celda th" width="8%" >Nº de Cédula</th><?php } ?>
			<?php if(!isset($v4)){?><th class="celda th" width="3%" >Cod. Bio.</th><?php } ?>
			<?php if(!isset($v5)){?><th class="celda th" width="11%">Nombre completo</th><?php } ?>
			<?php if(!isset($v6)){?><th class="celda th" width="9%">Cargo en la empresa</th><?php } ?>
			<?php if(!isset($v7)){?><th class="celda th" width="7%">Procesos que realiza</th><?php } ?>
			<?php if(!isset($v8)){?><th class="celda th" width="7%" >Tipo de contrato</th><?php } ?>
			<?php if(!isset($v9)){?><th class="celda th" width="4%" >Salario (Bs.)</th><?php } ?>
			<?php if(!isset($v10)){?><th class="celda th" width="5%" >Telf./Cel.</th><?php } ?>
			<?php if(!isset($v11)){?><th class="celda th" width="8%" >Email</th><?php } ?>
			<?php if(!isset($v12)){?><th class="celda th" width="12%">Dirección</th><?php } ?>
			<?php if(!isset($v13)){?><th class="celda th" width="18%">Caracteristicas</th><?php } ?>
		</tr>
		<?php $sub_total=0;}//end if ?>
		<tr class="fila">
			<?php if(!isset($v1)){?><td class="celda td"><?php echo $cont+1;$cont++;?></td><?php } ?>
			<?php if(!isset($v2)){?><td class="celda td"><img src="<?php echo $url.$img;?>" style='width:100%'></td><?php } ?>
			<?php if(!isset($v3)){?><td class="celda td"><?php echo $empleado->ci.' '.$empleado->abreviatura;?></td><?php } ?>
			<?php if(!isset($v4)){?><td class="celda td"><?php echo $empleado->codigo;?></td><?php } ?>
			<?php if(!isset($v5)){?><td class="celda td"><?php echo $empleado->nombre.' '. $empleado->nombre2.' '. $empleado->paterno.' '. $empleado->materno;?></td><?php } ?>
			<?php if(!isset($v6)){?><td class="celda td"><?php echo $empleado->cargo;?></td><?php } ?>
			<?php $procesos=$this->M_empleado_proceso->get_proceso($empleado->ide); ?>
			<?php if(!isset($v7)){?><td class="celda td">
				<?php  if(!empty($procesos)){ 
					for ($p=0; $p < count($procesos); $p++) { $proceso=$procesos[$p];
						if($proceso->tipo==0){ $tipo="Ayudante";}else{ $tipo="Maestro";}
			?>
						- <?php echo $proceso->nombre." (".$tipo.")<br>";?>
			<?php 	}// end for
				}//end if
			?>
			</td><?php } ?>
			<?php $contrato=$this->M_tipo_contrato->get($empleado->idtc);?>
			<?php if(!isset($v8)){?><td class="celda td"><?php if(!empty($contrato)){ if($contrato[0]->tipo==0){ echo "Tiempo completo";}else{ echo "Medio Tiempo";} echo " (".$contrato[0]->horas."hrs.)";}?></td><?php } ?>
			<?php if(!isset($v9)){?><td class="celda td"><?php echo number_format($empleado->salario,2,'.',',');?></td><?php } ?>
			<?php if(!isset($v10)){?><td class="celda td"><?php echo $empleado->telefono;?></td><?php } ?>
			<?php if(!isset($v11)){?><td class="celda td"><?php echo $empleado->email;?></td><?php } ?>
			<?php if(!isset($v12)){?><td class="celda td"><?php echo $empleado->direccion;?></td><?php } ?>
			<?php if(!isset($v13)){?><td class="celda td"><?php echo $empleado->caracteristicas;?></td><?php } ?>
		</tr>
	<?php $contReg++;}// end for ?>
	</table>
</div>
<script type="text/javascript"> $(".nroPagina").html(<?php echo $contPagina;?>); </script>