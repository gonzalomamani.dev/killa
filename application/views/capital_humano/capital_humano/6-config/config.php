<?php 
	$help1='title="<h4>Año de los anticipos<h4>" data-content="Ingrese la gestión donde desea ver los anticipos."';
	$help2='title="<h4>Fecha de anticipo<h4>" data-content="Representa la fecha en donde se realiza el prestamo, el monto del adelantado de descontara en la fecha que se registre"';
	$help3='title="<h4>Monto de anticipo<h4>" data-content="Representa al monto adelantado en bolivianos, el sistema acepta montos comprendidos <b>entre 0 y 99999.9</b> con un <b>máximo de una decimal</b>."';
	$help4='title="<h4>Descripción de anticipo<h4>" data-content="Representa la descripcion del anticipo, contenido debe tener un formato alfanumerico, <b>puede incluir espacios y sin saltos de linea</b>, ademas la descripcion solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑº+-ª.,:;)</b>, con un <b>máximo de 300 caractereres</b>."';
	$help5='title="<h4>Contraseña de empleado<h4>" data-content="Representa la contraseña de ingreso al sistema del taller o ingreso al sistema como usuario, inicialmente por defecto es el número de ci, si desea restablecer la contraseña al número de ci puede dar click en el boton <b>Restablecer</b>."';
	$help6='title="<h4>Configuración de feriados<h4>" data-content="Configura si el calculo de las horas trabajadas toma de los dias feriados libres (no se realiza el descuento por el dia no trabajado) o feriados de trabajo (el dia feriado se toma como una jornal de trabajo normal)."';
	$popover='data-toggle="popover" data-placement="top" data-trigger="hover"';
	$popover2='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$empleado=$empleado[0];
	$v = array('0' => 'Ayudante','1' => 'Maestro',);
	$vf = array('0' => 'Feriados libres','1' => 'Feriados como dia de trabajo');
	$vd = array('0' => 'Con descuentos','1' => 'Sin descuentos');
?>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="change_empleado('<?php echo $empleado->ide; ?>')">Capital Humano</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="config_empleado('<?php echo $empleado->ide; ?>')">Configuración</a></li>
</ul><br>
<div class="row">
	<div class="col-md-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="4" class="text-center"><ins>AYUDANTES</ins></th></tr>
					<tr>
						<th width="g-thumbnail-modal"><div class="g-img-modal">#Item</div></th>
						<th width="15%">CI</th>
						<th width="70%">Nombre</th>
						<th width="15%"></th>
					</tr>
				</thead>
				<tbody>
		<?php $url="./libraries/img/personas/miniatura/";
		for ($i=0; $i < count($ayudantes) ; $i++) { $ayudante=$ayudantes[$i]; 
			$img="default.png";
			if($ayudante->fotografia!="" && $ayudante->fotografia!=NULL){ $img=$ayudante->fotografia;}
		?>
				<tr>
					<td>
						<div id="item"><?php echo $i+1;?></div>
						<img src="<?php echo $url.$img;?>" width="100%" class="img-thumbnail g-thumbnail-modal">
					</td>
					<td><?php echo $ayudante->ci.' '.$ayudante->abreviatura;?></td>
					<td><?php $nombre=$ayudante->nombre.' '.$ayudante->nombre2.' '.$ayudante->paterno.' '.$ayudante->materno; echo $nombre; ?></td>
					<td><?php $this->load->view("estructura/botones/botones_registros",['eliminar'=>"alerta_ayudante('".$empleado->ide."','".$ayudante->iday."','".$nombre."','".$url.$img."')"]);?></td>
				</tr>
		<?php }?>
				</tbody>
				<thead>
					<tr><th colspan="4" class="text-center"><ins>NUEVO</ins></th></tr>
					<tr><th colspan="3"></th><th><?php $this->load->view('estructura/botones/buscador',['nuevo'=>'Adicionar nuevo ayudante','f_nuevo'=>"view_ayudantes('".$empleado->ide."')"]);?></th></tr>
				</thead>
			</table>			
		</div>
	</div>
	<div class="col-md-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="4" class="text-center"><ins>PROCESOS</ins></th></tr>
					<tr>
						<th width="5%">#</th>
						<th width="55%">Nombre proceso</th>
						<th width="30%">Tipo</th>
						<th width="10%"></th>
					</tr>
				</thead>
				<tbody>
		<?php for ($i=0; $i < count($empleado_procesos) ; $i++) { $empleado_proceso=$empleado_procesos[$i];?>
				<tr>
					<td><?php echo $i+1;?></td>
					<td>
						<select id="pro_e<?php echo $empleado_proceso->idep;?>" class="form-control input-sm">
								<option value="">Seleccione...</option>
						<?php for ($p=0; $p < count($procesos) ; $p++) { $proceso=$procesos[$p]; ?>
								<option value="<?php echo $proceso->idpr;?>" <?php if($empleado_proceso->idpr==$proceso->idpr){ echo "selected";}?>><?php echo $proceso->nombre;?></option>
						<?php }?>
						</select>
					</td>
					<td>
						<select class="form-control input-sm" id="tip_e<?php echo $empleado_proceso->idep;?>">
							<option value="">Seleccione...</option>
							<?php foreach ($v as $key => $value) { ?>
								<option value="<?php echo $key;?>" <?php if($empleado_proceso->tipo==$key){ echo "selected";}?>><?php echo $value;?></option>
							<?php }?>
						</select>
					</td>
					<td><?php $this->load->view("estructura/botones/botones_registros",['guardar'=>"update_proceso('".$empleado->ide."','".$empleado_proceso->idep."')",'eliminar'=>"alerta_proceso('".$empleado->ide."','".$empleado_proceso->idep."','".$empleado_proceso->nombre."')"]);?></td>
				</tr>
		<?php }?>
				</tbody>
				<thead>
					<tr><th colspan="4" class="text-center"><ins>NUEVO</ins></th></tr>
					<tr>
						<td colspan="2">
							<select id="pro_e" class="form-control input-sm">
								<option value="">Seleccione...</option>
						<?php for ($i=0; $i < count($procesos) ; $i++){ $proceso=$procesos[$i]; ?>
								<option value="<?php echo $proceso->idpr;?>"><?php echo $proceso->nombre;?></option>
						<?php }// end for ?>
							</select>
						</td>
						<td>
							<select class="form-control input-sm" id="tip_e">
								<option value="">Seleccione...</option>
								<?php foreach ($v as $key => $value) { ?>
									<option value="<?php echo $key;?>"><?php echo $value;?></option>
								<?php }?>
							</select>
						</td>
						<td><?php $this->load->view("estructura/botones/botones_registros",['guardar'=>"save_proceso('".$empleado->ide."')",'eliminar'=>""]);?></td></tr>
				</thead>
			</table>			
		</div>
	</div><i class="clearfix"></i>
	<div class="col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="5" class="text-center"><ins>ANTICIPOS</ins></th></tr>
					<tr>
						<th colspan="3"></th>
						<th>
							<form onsubmit="return search_anticipo('<?php echo $empleado->ide;?>')">
								<div class="input-group col-xs-12">
									<input type="number" class="form-control input-sm" id="anio" value="<?php if(isset($_POST['anio'])){echo $_POST['anio'];}else{echo date('Y');}?>">
									<span class="input-group-addon input-sm" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								</div>
							</form>
						</th>
						<th><?php $this->load->view('estructura/botones/buscador',['f_buscar'=>"search_anticipo('".$empleado->ide."')",'f_nuevo'=>""]);?></th>
					</tr>
					<tr>
						<th width="5%">#</th>
						<th width="25%">Fecha de anticipos</th>
						<th width="20%">Monto<br>anticipos (Bs.)</th>
						<th width="40%">Descripción de anticipos</th>
						<th width="10%"></th>
					</tr>
				</thead>
				<tbody>
		<?php for ($i=0; $i < count($anticipos) ; $i++) { $anticipo=$anticipos[$i]; ?>
				<tr>
					<td><?php echo $i+1;?></td>
					<td>
						<div class="input-group col-xs-12">
							<input type="date" class="form-control input-sm" id="fec_e<?php echo $anticipo->idan;?>" value="<?php echo $anticipo->fecha_anticipo;?>">
							<span class="input-group-addon input-sm" <?php echo $popover.$help2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
						</div>
					</td>
					<td>
					<form onsubmit="return update_anticipo('<?php echo $empleado->ide;?>','<?php echo $anticipo->idan;?>')">
						<div class="input-group col-xs-12">
							<input type="number" class="form-control input-sm" id="mon_e<?php echo $anticipo->idan;?>" value="<?php echo $anticipo->monto;?>" placeholder="Monto anticipado">
							<span class="input-group-addon input-sm" <?php echo $popover.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
						</div>
					</form>
					</td>
					<td>
					<div class="input-group col-xs-12">
						<textarea class="form-control input-sm" id="des_e<?php echo $anticipo->idan;?>" placeholder="Descripcion anticipado"><?php echo $anticipo->descripcion ;?></textarea>
						<span class="input-group-addon input-sm" <?php echo $popover.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					</div>
					</td>
					<td><?php $this->load->view("estructura/botones/botones_registros",['guardar'=>"update_anticipo('".$empleado->ide."','".$anticipo->idan."')",'eliminar'=>"alerta_anticipo('".$empleado->ide."','".$anticipo->idan."')"]);?></td>
				</tr>
		<?php }?>
				</tbody>
				<thead>
					<tr>
						<th class="text-center" colspan="5"><ins>NUEVO</ins></th>
					</tr>
					<tr>
						<td></td>
						<td>
						<div class="input-group col-xs-12">
							<input type="date" class="form-control input-sm" id="fec_e" value="<?php echo date('Y-m-d');?>">
							<span class="input-group-addon input-sm" <?php echo $popover.$help2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
						</div>
						</td>
						<td>
						<form onsubmit="return save_anticipo('<?php echo $empleado->ide;?>')">
							<div class="input-group col-xs-12">
								<input type="number" class="form-control input-sm" id="mon_e" placeholder="Monto anticipado" style="any" min="0" max="99999.9">
								<span class="input-group-addon input-sm" <?php echo $popover.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
							</div>
						</form>
						</td>
						<td>
							<div class="input-group col-xs-12">
								<textarea class="form-control input-sm" id="des_e" placeholder="Descripcion anticipado" maxlength="300"></textarea>
								<span class="input-group-addon input-sm" <?php echo $popover.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
							</div>
						</td>
						<td><?php $this->load->view("estructura/botones/botones_registros",['guardar'=>"save_anticipo('".$empleado->ide."')",'eliminar'=>""]);?></td>
					</tr>
				</thead>
			</table>			
		</div>
	</div>
	<div class="col-md-6 col-xs-12">
		<table class="table table-bordered table-hover">
			<thead>
				<tr><th class="text-center"><ins>CONTRASEÑA EN EL SISTEMA</ins></th></tr>
			</thead>
			<tbody>
				<tr><td>
					<div class="input-group col-xs-12">
						<input type="text" class="form-control input-xs" value="Contraseña encriptada..." disabled="disabled">
						<a href="javascript:" onclick="confirmar_password('<?php echo $empleado->ide;?>')" title="Restablecer contraseña" class="input-group-addon input-xs"><i class="glyphicon glyphicon-repeat"></i></a>
						<span class="input-group-addon input-sm" <?php echo $popover2.$help5;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					</div>
				</td></tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-6 col-xs-12">
		<table class="table table-bordered table-hover">
			<thead>
				<tr><th class="text-center"><ins>CONTROL DE FERIADOS</ins></th></tr>
			</thead>
			<tbody>
				<tr><td>
					<div class="input-group col-xs-12">
						<select class="form-control input-sm" id="c_fec" onchange="update_c_feriado('<?php echo $empleado->ide;?>')">
							<option value="">Seleccione...</option>
							<?php foreach ($vf as $key => $value) { ?>
								<option value="<?php echo $key;?>" <?php if($empleado->c_feriado==$key){ echo "selected";}?>><?php echo $value;?></option>
							<?php }?>
						</select>						
						<span class="input-group-addon input-sm" <?php echo $popover2.$help6;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					</div>
				</td></tr>
			</tbody>
		</table>
	</div><i class="clearfix"></i>
	<div class="col-md-6 col-xs-12">
		<table class="table table-bordered table-hover">
			<thead><tr><th class="text-center"><ins>CONTROL DE DESCUENTOS</ins></th></tr></thead>
			<tbody>
				<tr><td>
					<div class="input-group col-xs-12">
						<select class="form-control input-sm" id="c_dec" onchange="update_c_descuento('<?php echo $empleado->ide;?>')">
							<option value="">Seleccione...</option>
							<?php foreach ($vd as $key => $value) { ?>
								<option value="<?php echo $key;?>" <?php if($empleado->c_descuento==$key){ echo "selected";}?>><?php echo $value;?></option>
							<?php }?>
						</select>						
						<span class="input-group-addon input-sm" <?php echo $popover2.$help6;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					</div>
				</td></tr>
			</tbody>
		</table>
	</div>
</div>
<script language='javascript'>$('[data-toggle="popover"]').popover({html:true});</script>