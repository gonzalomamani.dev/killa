<?php 
	if(count($ayudantes)>0){ $url='./libraries/img/personas/miniatura/'; ?>
		<table class="table table-bordered table-striped">
			<thead>
				<th class="g-thumbnail-modal"><div class="g-img-modal">#Item</div></th>
				<th class="celda-sm-15">CI</th>
				<th class="celda-sm-80">Nombre</th>
				<th style="width:5%"></th>
			</thead>
			<?php for($i=0; $i < count($ayudantes) ; $i++) { $ayudante=$ayudantes[$i];
					$img="default.png";
					if($ayudante->fotografia!="" & $ayudante->fotografia!=null){ $img=$ayudante->fotografia; }
			?>
					<tr>
						<td><div id="item"><?php echo $i+1;?></div><img src="<?php echo $url.$img;?>" width='100%' class='img-thumbnail g-thumbnail-modal'></td>
						<td><?php echo $ayudante->ci;?></td>
						<td><?php echo $ayudante->nombre.' '.$ayudante->nombre2.' '.$ayudante->paterno.' '.$ayudante->materno;?></td>
						<td>
				<?php $control=$this->M_ayudante->get_row_2n('maestro',$ide,'ayudante',$ayudante->ide);
					if(!empty($control)){
				?>
						<div class="btn-circle btn-circle-red btn-circle-25" onclick="select_ayudante('<?php echo $ide;?>','<?php echo $ayudante->ide;?>',this,'red','25');"></div>
					<?php }else{ ?>
						<div class="btn-circle btn-circle-default btn-circle-25" onclick="select_ayudante('<?php echo $ide;?>','<?php echo $ayudante->ide;?>',this,'red','25');"></div>
					<?php } ?>
						</td>
					</tr>
				<?php }// end for ?>
		</table>
		<?php
	}else{
		echo "<h3>0 registros encontrados...</h3>";
	}
?>
