<?php $empleado=$empleado[0]; 
		$help1='title="<h4>Subir Fotografía</h4>" data-content="Seleccione una fotografía, preferiblemente una imagen que no sobrepase las dimenciones de <strong>700x700[px]</strong>, para evitar sobre cargar al sistema"';
	$help2='title="<h4>Nº Cédula<h4>" data-content="Ingrese un numero cédula de identidad con valores numericos <b>sin espacios</b>, de 6 a 8 digitos"';
	$help3='title="<h4>Ciudad<h4>" data-content="Seleccione la ciudad donde se difundio la cédula de identidad. Si desea adicionar una nueva ciudad puede hacerlo en la sección de configuración en el menu superior, o puede dar click el el boton <b>+</b>"';
	$help4='title="<h4>Primer nombre<h4>" data-content="Ingrese un nombre alfanumerico de 2 a 20 caracteres <b>puede incluir espacios</b>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help5='title="<h4>Segundo nombre<h4>" data-content="Ingrese un segundo nombre (si tuviera) alfanumérico de 2 a 20 caracteres <b>puede incluir espacios</b>, ademas el segundo nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help6='title="<h4>Apellido Paterno<h4>" data-content="Ingrese un apellido paterno alfanumérico de 2 a 20 caracteres <b>puede incluir espacios</b>, ademas el apellido solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help7='title="<h4>Apellido Materno<h4>" data-content="Ingrese un apellido materno alfanumérico de 2 a 20 caracteres <b>puede incluir espacios</b>, ademas el apellido solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help8='title="<h4>Código<h4>" data-content="Este código representa principalmente al codigo usado en el registro biométrico de 0 a 10 digitos."';
	$help9='title="<h4>Tipo de empleado<h4>" data-content="Seleccione un tipo de empleado, si el empleado trabaja en la planta de producción o es un empleado dedicado a actividades administrativas."';
	$help10='title="<h4>Cargo<h4>" data-content="Ingrese el cargo o cargos que realiza. Ej. Encargado de cortado de piezas. Solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)</b>, con un <b>máximo de 100 caractereres</b>."';
	$help11='title="<h4>Grado académico<h4>" data-content="Representa al mayor grado académico alcanzado por el empleado. Ej. (Primaria, Secundaria, Técnico superior). El contenido solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)</b>, con un <b>máximo de 100 caractereres</b>."';
	$help12='title="<h4>Tipo de contrato<h4>" data-content="Seleccione un tipo de contrato, esto va según la cargar horaria del empleado, si desea adicionar una nueva carga horaria puede realizarlo en la sección de configuración en el menu superior o pude dar click en el botón <b>+</b>."';
	$help13='title="<h4>Salario por mes<h4>" data-content="Representa al pago que recibira el empleado por cada mes trabajado, segun la carga horaria asignada, el valor acptado es de <b>0 a 9999.9</b> con una sola decimal."';
	$help14='title="<h4>Telf./Celular<h4>" data-content="Ingrese el número de teléfono o celular de referencia del empleado de 6 a 15 digitos numéricos."';
	$help15='title="<h4>Email<h4>" data-content="Representa al correo electrónico del empleado si tuviera, el formato del correo electrónico aceptado es <b>example@dominio.com</b>."';
	$help16='title="<h4>Fecha de nacimiento<h4>" data-content="Representa a la fecha de nacimiento del empleado, esta fecha es referencial para uso apropiado dentro la empresa."';
	$help17='title="<h4>Fecha de ingreso<h4>" data-content="Representa a la fecha de inicio de contrato en la empresa, esta fecha es referencial para uso apropiado dentro la empresa"';
	$help18='title="<h4>Dirección de domicilio<h4>" data-content="Representa a la dirección de domicilio del empleado, esta dirección tiene com fin como referencia y garantía para la empresa. El contenido solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑº+-ª/.,:;)</b>, con un <b>máximo de 200 caractereres</b>."';
	$help19='title="<h4>Característica<h4>" data-content="Representa a las característica propias del empleado si tuviera el contenido debe tener un formato alfanumerico de 0 a 900 caracteres <b>puede incluir espacios y sin saltos de linea</b>, ademas la observacion solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑº+-ª.,:;)</b>, con un <b>máximo de 900 caractereres</b>."';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$v = array('0' => 'Empleado en planta','1' => 'Empleado administrativo (Oficina)');
?>
<ul class="nav nav-tabs">
  <li role="presentation" class="active"><a href="javascript:" onclick="change_empleado('<?php echo $empleado->ide; ?>')">Capital Humano</a></li>
  <li role="presentation"><a href="javascript:" onclick="config_empleado('<?php echo $empleado->ide; ?>')">Configuración</a></li>
</ul><br>
<div class="row">
	<div class="col-sm-3 col-sm-offset-9 col-xs-12"><strong><span class='text-danger'>(*)</span> Campo obligatorio</strong></div>
	<div class="col-sm-2 col-xs-12"><strong>Fotografía:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<div class="input-group">
			<input class="form-control input-xs" id="n_fot" type="file" placeholder='Seleccione fotografia'>
			<span class="input-group-addon input-sm input-sm" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Nº Cédula:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<form onsubmit="return update_empleado('<?php echo $empleado->ide;?>')">
			<div class="input-group">
				<input class="form-control input-xs" id="n_ci" type="number" placeholder='Número de cedula de identidad' min='100000' max='99999999' value="<?php echo $empleado->ci;?>">
				<span class="input-group-addon input-sm" <?php echo $popover.$help2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Ciudad:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<div class="input-group">
			<select class="form-control input-xs" id="n_ciu">
				<option value="">Seleccionar...</option>
		<?php for($i=0; $i<count($ciudades); $i++){ $ciudad=$ciudades[$i]; ?>
				<option value="<?php echo $ciudad->idci;?>" <?php if($empleado->idci==$ciudad->idci){ echo "selected";}?>><?php echo $ciudad->ciudad.'('.$ciudad->abreviatura.')';?></option>
		<?php } ?>
			</select>
			<a href="<?php echo base_url();?>capital_humano?p=5" target="_blank" title="Ver configuraciónes" class="input-group-addon input-sm"><i class="glyphicon glyphicon-plus"></i></a>
			<span class="input-group-addon input-sm" <?php echo $popover.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Primer nombre:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<form onsubmit="return update_empleado('<?php echo $empleado->ide;?>')">
			<div class="input-group col-xs-12">
				<input class="form-control input-xs" id="n_nom1" type="text" placeholder='Primero nombre del empleado' maxlength="20" value="<?php echo $empleado->nombre;?>">
				<span class="input-group-addon input-sm" <?php echo $popover.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong>Segundo nombre:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<form onsubmit="return update_empleado('<?php echo $empleado->ide;?>')">
			<div class="input-group col-xs-12">
				<input class="form-control input-xs" id="n_nom2" type="text" placeholder='Segundo nombre del empleado' maxlength="20" value="<?php echo $empleado->nombre2;?>">
				<span class="input-group-addon input-sm" <?php echo $popover.$help5;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Apellido paterno:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<form onsubmit="return update_empleado('<?php echo $empleado->ide;?>')">
			<div class="input-group col-xs-12">
				<input class="form-control input-xs" id="n_pat" type="text" placeholder='Apellido paterno del empleado' maxlength="20" value="<?php echo $empleado->paterno;?>">
				<span class="input-group-addon input-sm" <?php echo $popover.$help6;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong>Apellido materno:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<form onsubmit="return update_empleado('<?php echo $empleado->ide;?>')">
			<div class="input-group col-xs-12">
				<input class="form-control input-xs" id="n_mat" type="text" placeholder='Apellido materno del empleado' maxlength="20" value="<?php echo $empleado->materno;?>">
				<span class="input-group-addon input-sm" <?php echo $popover.$help7;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong>Código:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<form onsubmit="return update_empleado('<?php echo $empleado->ide;?>')">
			<div class="input-group">
				<input class="form-control input-xs" type="number" id="n_cod" placeholder='Código biométrico' min='0' max='9999999999' value="<?php echo $empleado->codigo;?>">
				<span class="input-group-addon input-sm" <?php echo $popover.$help8;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Tipo de empleado:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<form onsubmit="return update_empleado('<?php echo $empleado->ide;?>')">
		<div class="input-group">
			<select id="n_tem" class="form-control input-xs">
				<option value="">Seleccionar</option>
			<?php foreach ($v as $key => $value) { ?>
				<option value="<?php echo $key;?>" <?php if($empleado->tipo==$key){ echo "selected";}?>><?php echo $value;?></option>
			<?php }?>
			</select>
			<span class="input-group-addon input-sm" <?php echo $popover.$help9;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
		</form>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong>Cargo:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<form onsubmit="return update_empleado('<?php echo $empleado->ide;?>')">
			<div class="input-group col-xs-12">
				<input class="form-control input-xs" id="n_car" type="text" placeholder='Cargo del empleado' maxlength="100" value="<?php echo $empleado->cargo;?>">
				<span class="input-group-addon input-sm" <?php echo $popover.$help10;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong>Grado académico:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<form onsubmit="return update_empleado('<?php echo $empleado->ide;?>')">
			<div class="input-group col-xs-12">
				<input class="form-control input-xs" id="n_ga" type="text" placeholder='Máximo grado académico alcanzado' maxlength="100" value="<?php echo $empleado->formacion;?>">
				<span class="input-group-addon input-sm" <?php echo $popover.$help11;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Tipo de contrato:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<div class="input-group">
			<select class="form-control input-xs" id="n_tc">
				<option value="">Seleccionar...</option>
		<?php for ($i=0; $i < count($contrato); $i++) { $resp=$contrato[$i]; if($resp->tipo==0){ $nom_tip="Tiempo completo";}else{ $nom_tip="Medio tiempo";}
				?>
				<option value="<?php echo $resp->idtc;?>" <?php if($empleado->idtc==$resp->idtc){ echo "selected";}?>><?php echo $nom_tip." (".$resp->horas."hrs.)";?></option>
		<?php } ?>
			</select>
			<a href="<?php echo base_url();?>capital_humano?p=5" target="_blank" title="Ver configuraciónes" class="input-group-addon input-sm"><i class="glyphicon glyphicon-plus"></i></a>
			<span class="input-group-addon input-sm" <?php echo $popover.$help12;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Salario/Mes (Bs.):</strong></div>
	<div class="col-sm-4 col-xs-12">
	<form onsubmit="return update_empleado('<?php echo $empleado->ide;?>')">
		<div class="input-group">
			<input class='form-control input-xs' type="number" id="n_sal" placeholder='Salario por mes en bolivianos' min='0' max="9999.9" step="any" value="<?php echo $empleado->salario;?>">		
			<span class="input-group-addon input-sm" <?php echo $popover.$help13;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</form>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong>Telf./Celular:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<form onsubmit="return update_empleado('<?php echo $empleado->ide;?>')">
			<div class="input-group">
				<input class="form-control input-xs" id="n_tel" type="number" placeholder='Número de teléfono o celular' min='100000' max="999999999999999" value="<?php echo $empleado->telefono;?>">
				<span class="input-group-addon input-sm" <?php echo $popover.$help14;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div>
	<div class="col-sm-2 col-xs-12"><strong>Email:</strong></div>
	<div class="col-sm-4 col-xs-12">
	<form onsubmit="return update_empleado('<?php echo $empleado->ide;?>')">
		<div class="input-group">
			<input class="form-control input-xs" id="n_ema" type="email" placeholder='example@dominio.com' maxlength="60" value="<?php echo $empleado->email;?>">
			<span class="input-group-addon input-sm" <?php echo $popover.$help15;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</form>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong>Fecha de nacimiento:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<div class="input-group">
			<input class="form-control input-xs" type="date"  id="n_fi" value="<?php echo $empleado->fecha_nacimiento;?>">
			<span class="input-group-addon input-sm" <?php echo $popover.$help16;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12"><strong>Fecha de ingreso:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<div class="input-group">
			<input class="form-control input-xs" type="date"  id="n_fn" value="<?php echo $empleado->fecha_ingreso;?>">
			<span class="input-group-addon input-sm" <?php echo $popover.$help17;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong>Dirección de domicilio:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<form onsubmit="return update_empleado('<?php echo $empleado->ide;?>')">
			<div class="input-group col-xs-12">
				<input class="form-control input-xs" id="n_dir" type="text" placeholder='Z/Villa Fatima C/Saturnino Porcel' maxlength="200" value="<?php echo $empleado->direccion;?>">
				<span class="input-group-addon input-sm" <?php echo $popover.$help18;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong>Característica:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<div class="input-group col-xs-12">
			<textarea class="form-control input-xs" id="n_obs" placeholder='Caracteristicas personales' maxlength="900"><?php echo $empleado->caracteristicas;?></textarea>
			<span class="input-group-addon input-sm" <?php echo $popover.$help19;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div><i class="clearfix"></i>
</div>
<script language='javascript'>Onfocus("n_ci");$('[data-toggle="popover"]').popover({html:true});</script>
