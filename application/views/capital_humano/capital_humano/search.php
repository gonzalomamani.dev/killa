<?php $v = array('0' => 'Empleado en planta','1' => 'Empleado administrativo (Oficina)',);?>
<table class="tabla tabla-border-false">
	<tr class="fila">
		<td class='celda g-thumbnail'><div class="g-img"></div></td>
		<td class='celda celda-sm-6'><form onsubmit="return view_empleados()"><input type="text"class="form-control input-sm" id="s_cod" placeholder='Código' onkeyup="reset_input(this.id)"></form></td>
		<td class='celda celda-sm-25'><form onsubmit="return view_empleados()"><input type="text"class="form-control input-sm" id="s_nom" placeholder='Nombre Paterno Materno' onkeyup="reset_input(this.id)"/></form></td>
		<td class='celda celda-sm celda-sm-10'><form onsubmit="return view_empleados()"><input type="number"class="form-control input-sm" id="s_ci" maxlength="8" placeholder='CI' onkeyup="reset_input(this.id)" onclick="reset_input(this.id)"></form></td>
		<td class='celda celda-sm' style="width:10%">
			<select onchange="view_empleados();reset_input(this.id);" id="s_con" class="form-control input-sm">
				<option value="">Seleccionar</option>
			<?php for ($i=0; $i < count($tipo_contratos) ; $i++) { ?>
				<option value="<?php echo $tipo_contratos[$i]->idtc?>"><?php if($tipo_contratos[$i]->tipo==0){ echo "Tiempo completo";}else{ echo "Medio Tiempo";} echo " (".$tipo_contratos[$i]->horas."hrs.)";?></option>
			<?php }?>
			</select>
		</td>
		<td class='celda celda-sm' style="width:10%"><form onsubmit="return view_empleados()"><input type="number"class="form-control input-sm" id="s_sal" placeholder='Salario' onkeyup="reset_input(this.id)" onclick="reset_input(this.id)" step="any"></form></td>
		<td class='celda celda-sm-15'><select onchange="view_empleados();reset_input(this.id);" id="s_tip" class="form-control input-sm">
			<option value="">Seleccionar</option>
		<?php foreach ($v as $key => $value) { ?>
			<option value="<?php echo $key;?>"><?php echo $value;?></option>
		<?php }?>
		</select></td>
		<td class='celda celda-sm' style="width:11%"></td>
		<td class='celda text-right' style="width:13%">
			<?php $new="new_empleado()"; if($privilegio[0]->ca1c!="1"){ $new="";}?>
			<?php $print="print()"; if($privilegio[0]->ca1p!="1"){ $print="";}?>
			<?php $this->load->view('estructura/botones/buscador',['f_buscar'=>"view_empleados()",'f_ver'=>"all_empleado()",'nuevo'=>'Nuevo Empleado','f_nuevo'=>$new,'f_imprimir'=>$print]);?>
		</td>
	</tr>
</table>
<script language="javascript" type="text/javascript"> Onfocus('s_cod'); </script>