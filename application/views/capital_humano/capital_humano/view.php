<?php 
	$url=base_url().'libraries/img/personas/';
	$j_empleados=json_encode($empleados);
?>
	<table class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<td class='g-thumbnail'><div class="g-img"></div></td>
				<th class='celda-sm-6'>Código</th>
				<th class='celda-sm-25'>Nombre</th>
				<th class='celda-sm-10'>CI</th>
				<th class='celda-sm' width="12%">Tipo de contrato</th>
				<th class='celda-sm' width="9%">Salario(Bs.)</th>
				<th class='celda-sm' width="13%">Tipo de empleado</th>
				<th class='celda-sm' width="10%">Procesos que realiza</th>
				<th class="celda-sm" width="4%">Estado</th>
				<th width="11%"></th>
			</tr>
		</thead>
		<tbody>
<?php if (count($empleados)>0) { 
		for($i=0; $i < count($empleados); $i++){ $res=$empleados[$i]; $img="default.png";
			if($res->fotografia!="" && $res->fotografia!=NULL){
				$img=$res->fotografia;
			} ?>
			<tr>
			<td><div id="item"><?php echo $i+1;?></div><a href="javascript:" onclick="fotografia('<?php echo $url.$img;?>')"><img src="<?php echo $url.'miniatura/'.$img;?>" width="100%" class="img-thumbnail"></a></td>
				<td><?php echo $res->codigo;?></td>
				<td><?php echo $res->nombre." ".$res->nombre2." ".$res->paterno." ".$res->materno; ?></td>
				<td><?php echo $res->ci." ".$res->abreviatura;?></td>
				<td class='celda-sm'><?php if($res->tipo_contrato==0){ echo "Tiempo completo";}else{ echo "Medio Tiempo";} echo " (".$res->horas."hrs.)";?></td>
				<td class='celda-sm'><?php echo number_format($res->salario,2,'.',',');?></td>
				<td class='celda-sm'><?php if($res->tipo==0){ echo "Empleado en planta"; }if($res->tipo==1){ echo "Empleado administrativo (Oficina)";}?></td>
			<?php $procesos=$this->M_empleado_proceso->get_proceso($res->ide); ?>
				<td class='celda-sm'>
			<?php  if(!empty($procesos)){ 
					for ($p=0; $p < count($procesos); $p++) { $proceso=$procesos[$p];
						if($proceso->tipo==0){ $tipo="Ayudante";}else{ $tipo="Maestro";}
			?>
						- <?php echo $proceso->nombre." (".$tipo.")<br>";?>
			<?php 	}// end for
				}//end if
			?>
				</td>
				<td><center>
					<div class="btn-circle btn-circle-<?php if($res->estado==1){ echo 'success';}else{ echo 'default';}?> btn-circle-30 " onclick="cambiar_estado('<?php echo $res->ide;?>')"></div>
					<span class="label <?php if($res->estado==1){ echo 'label-success';}else{ echo 'label-danger';}?> label-lg"><?php if($res->estado==1){ echo "Activo";}else{ echo "inactivo";} ?></span>
				</center></td>
				<?php $mod="change_empleado('".$res->ide."')"; if($privilegio[0]->ca1u!="1"){ $mod="";}?>
				<?php $del="confirmar_empleado('".$res->ide."','".$res->nombre."','".$img."')"; if($privilegio[0]->ca1d!="1"){ $del="";}?>
				<td class="text-right"><?php $this->load->view('estructura/botones/botones_registros',['reportes'=>"detalle_empleado('".$res->ide."')",'configuracion'=>$mod,'eliminar'=>$del]);?></td>
			</tr>
		<?php }// end for
		}else{//end if
			echo "<tr><td colspan='9'><h2>0 registros encontrados...</h2></td></tr>";
		}?>
		</tbody>
	</table>
<script language="javascript" type="text/javascript"> 
	$("#print").removeAttr("onclick"); 
	$("#print").unbind("click"); 
	$("#print").click(function(){ imprimir_empleados('<?php echo $j_empleados;?>'); }); 
</script>