<?php
	$help1='title="<h4>Fecha<h4>" data-content="Representa a la fecha de en el que se realizo el anticipo. <b>el formado de la fecha debe ser 2000-01-31</b>."';
	$help2='title="<h4>Monto<h4>" data-content="Representa al monto adelantado, el valor debe ser <b>mayor a 0 y menor o igual a 99999.9</b>"';
	$help3='title="<h4>Detalle<h4>" data-content="Representa al detalle del anticipo si tuviera,el contenido debe tener un formato alfanumerico <b>de 0 a 300</b> caracteres <b>puede incluir espacios y sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑº+-ª.,:;)</b>."';
	$popover='data-toggle="popover" data-placement="right" data-trigger="hover"';
?>
<div class="table-responsive">
<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th width="5%">#</th>
			<th width="25%">
				<div class="input-group col-xs-12">
					<span class="input-group-addon input-sm" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					<div class="form-control input-sm" disabled>Fecha</div>
				</div>
			</th>
			<th width="15%">
				<div class="input-group col-xs-12">
					<span class="input-group-addon input-sm" <?php echo $popover.$help2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					<div class="form-control input-sm" disabled>Monto</div>
				</div>
			</th>
			<th width="50%">
				<div class="input-group col-xs-12">
					<span class="input-group-addon input-sm" <?php echo $popover.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					<div class="form-control input-sm" disabled>Detalle de anticipo</div>
				</div>
			</th>
			<th width="5%"></th>
		</tr>
	</thead>
	<tbody>
<?php $total=0; 
	for ($i=0; $i < count($anticipos); $i++) { $anticipo=$anticipos[$i] ?>
		<tr>
			<td><?php echo $i+1;?></td>
			<td><input type="date" id="fec_e<?php echo $anticipo->idan;?>" class="form-control input-sm" placeholder="2000-12-31" value="<?php echo $anticipo->fecha_anticipo;?>"></td>
			<td><form onsubmit="return update_planilla_anticipo('<?php echo $empleado->ide;?>','<?php echo $anticipo->idan;?>','<?php echo $f1;?>','<?php echo $f2;?>');">
				<input type="number" id="mon_e<?php echo $anticipo->idan;?>" class="form-control input-sm" placeholder="0.0" step="any" value="<?php echo $anticipo->monto;?>" min="0" max="99999.9"><?php $total+=$anticipo->monto;?></form></td>
			<td><textarea id="des_e<?php echo $anticipo->idan;?>" class="form-control input-sm" placeholder="Detalle de anticipo"><?php echo $anticipo->descripcion;?></textarea></td>
			<td><?php $this->load->view("estructura/botones/botones_registros",['guardar'=>"update_planilla_anticipo('".$empleado->ide."','".$anticipo->idan."','".$f1."','".$f2."')",'eliminar'=>"alerta_planilla_anticipo('".$empleado->ide."','".$anticipo->idan."','".$f1."','".$f2."')"]);?></td>
		</tr>
<?php }?>
	</tbody>
	<thead>
		<tr>
			<th colspan="2" class="text-rigth">TOTAL (Bs.):</th>
			<th class="text-right"><?php echo number_format($total,2,'.',',');?></th>
			<th colspan="2"></th>
		</tr>
		<tr>
			<th colspan="5" class="text-center"><ins>NUEVO</ins></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan="2"><input type="date" id="fec_e" class="form-control input-sm" placeholder="2000-12-31" value="<?php echo $f1;?>"></td>
			<td><form onsubmit="return save_planilla_anticipo('<?php echo $empleado->ide;?>','<?php echo $f1;?>','<?php echo $f2;?>');">
				<input type="number" id="mon_e" class="form-control input-sm" placeholder="0.0" step="any" min="0" max="99999.9">
			</form></td>
			<td><textarea id="des_e" class="form-control input-sm" placeholder="Detalle de anticipo"></textarea></td>
			<td><?php $this->load->view("estructura/botones/botones_registros",['guardar'=>"save_planilla_anticipo('".$empleado->ide."','".$f1."','".$f2."')",'eliminar'=>""]);?></td>
		</tr>
	</tbody>
</table>
</div>
<script language='javascript'>$('[data-toggle="popover"]').popover({html:true});</script>