<?php 
	$help6='title="<h4>Configuración de feriados<h4>" data-content="Configura si el calculo de las horas trabajadas toma de los dias <b>feriados libres</b> (no se realiza el descuento por el dia no trabajado) o <b>feriados de trabajo</b> (el dia feriado se toma como una jornal de trabajo normal)."';
	$help7='title="<h4>Descuento<h4>" data-content="Configura si el si el calculo de las horas adeudadas se hara con la penalizacion por faltas mayores a una semana o no."';
	$popover2='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$horas_sabados=0;
	$dias_habiles=0;
	for ($i=0; $i < count($configuraciones); $i++) { $configuracion=$configuraciones[$i];
		if($configuracion->clave=="sueldo_hora_sabados"){
			$horas_sabados=$configuracion->valor*1;
		}
		if($configuracion->clave=="sueldo_dias_habiles"){
			$dias_habiles=$configuracion->valor*1;
		}
	}




	$vf = array('0' => 'Feriados libres','1' => 'Feriados como dia de trabajo');
	$vd = array('0' => 'Con descuentos','1' => 'Sin descuentos');
	$j_horas=json_encode($horas);
	$j_feriados=json_encode($feriados);
	//echo "Emp: ".$empleado->nombre;
	$horas_dia=json_decode($this->lib->horas_dia($j_horas,$empleado,$j_feriados,$f1,$f2,$horas_sabados));
	$total_trabajadas=0;
	$total_adeudada=0;
	$t_anticipos=0;
	for($i=0; $i < count($anticipos) ; $i++){ $t_anticipos+=$anticipos[$i]->monto;}
		$horas=json_encode($horas); 
		$feriados=json_encode($feriados);

?>
<?php $total_horas=json_decode($this->lib->total_horas_trabajo($j_horas,$empleado,$j_feriados,$f1,$f2,$empleado->salario,$dias_habiles,$horas_sabados)); ?>
<div class="row">
	<div class="col-md-2 col-xs-6"><span class="g-color dia_habil">&nbsp;&nbsp;&nbsp;&nbsp;</span> Día habil</div>
	<div class="col-md-2 col-xs-6"><span class="g-color dia_domingo">&nbsp;&nbsp;&nbsp;&nbsp;</span> Domingo</div>
	<div class="col-md-2 col-xs-6"><span class="g-color dia_feriado">&nbsp;&nbsp;&nbsp;&nbsp;</span> Feriado</div>
	<div class="col-md-2 col-xs-6"><span class="g-color medio_feriado">&nbsp;&nbsp;&nbsp;&nbsp;</span> Medio feriado</div>
	<div class="col-md-2 col-xs-6"><span class="g-color dia_error">&nbsp;&nbsp;&nbsp;&nbsp;</span> Error</div>
</div>
	<div id="area">
		<div class="table-responsive">
			<table class="tabla tabla-border-true">
				<tr class="fila">
					<th class="celda th" colspan="2">Nombre empleado:</th>
					<td class="celda td" colspan="2"><?php echo $empleado->nombre.' '.$empleado->nombre2.' '.$empleado->paterno.' '.$empleado->materno;?></td>
					<th class="celda th">Nº CI</th>
					<td class="celda td"><?php echo $empleado->ci;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th" width="8%">Año</th>
					<th class="celda th" width="8%">Mes</th>
					<th class="celda th" width="12%">Día</th>
					<th class="celda th" width="42%">Detalle de horas</th>
					<th class="celda th" width="15%">Horas Trabajadas</th>
					<th class="celda th" width="15%">Descuento</th>
				</tr>
		<?php foreach($horas_dia as $clave => $hora){ $v=explode("-", $hora->fecha); 
			$es_dia=$this->lib->es_habil_domingo_sabado_feriado($hora->fecha,$feriados);
			$clase="";
			switch($es_dia){ 
				case 'H': $clase="dia_habil"; break;
				case 'S': $clase="dia_habil"; break;
				case 'F': $clase="dia_feriado"; break;
				case 'MF': $clase="medio_feriado"; break;
				break;case 'D': $clase="dia_domingo";break;
			}
			if($clase=="dia_habil"){
				if(count($hora->horas)<=0 || count($hora->horas)%2!=0){
					$clase="dia_error";
				}
			}else{
				if(count($hora->horas)%2!=0){
					$clase="dia_error";
				}
			}
		?>
				<tr class="fila <?php echo $clase;?>">
					<td class="celda td"><?php echo $v[0];?></td>
					<td class="celda td"><?php echo $this->lib->mes_literal($v[1]);?></td>
					<td class="celda td"><?php echo $this->lib->nombre_de_dia($this->lib->que_dia_es($v[2],$v[1],$v[0]))." ".$v[2];?></td>
					<?php $hrs=json_decode(json_encode($hora->horas));?>
					<td class="celda td">
				<?php $trabajadas=json_decode(json_encode($hora->horas_trabajadas));?>
						<table class="table table-bordered table-hover" style="background:rgba(255,255,255,.25);"><thead></thead>
				<?php foreach ($hrs as $key => $hr) { ?>
							<tr>
								<td width="45%"><?php echo $hr->tipo;?></td>
								<td width="25%"><?php echo $hr->hora;?></td>
								<td width="25%"><?php echo $hr->hora_redondo;?></td>
								<td width="5%" class="text-center"><button class="btn btn-danger btn-xs" title="Eliminar registro..." onclick="alerta_hora('<?php echo $empleado->ide;?>','<?php echo $hr->idhb;?>','<?php echo $f1;?>','<?php echo $f2;?>')"><span class="glyphicon glyphicon-remove"></span></button></td>
							</tr>
				<?php }// end forearch ?>
							<thead>
							<tr>
								<td width="95%" colspan="3"></td>
								<td width="5%"><button class="btn btn-success btn-xs" title="Adicionar Hora..." onclick="add_hora('<?php echo $empleado->ide;?>','<?php echo $hora->fecha;?>','<?php echo $f1;?>','<?php echo $f2;?>')"><span class="glyphicon glyphicon-plus"></span></button></td>
							</tr></thead>
					</table>
					</td>
					<td class="celda td"><?php echo $trabajadas->horas_trabajadas; $total_trabajadas+=$trabajadas->horas_trabajadas; ?></td>
					<td class="celda td"><?php echo $trabajadas->horas_adeudadas*-1; $total_adeudada+=$trabajadas->horas_adeudadas; ?></td>
				</tr>
			<?php } ?>
			<tr class="fila">
				<th class="celda td text-right" colspan="4">TOTAL:</th>
				<th class="celda th"><?php echo $total_trabajadas;?></th>
				<th class="celda th"><?php echo $total_adeudada*-1;?></th>
			</tr>
			<?php if($total_horas->penalizacion>0){?>
				<tr class="fila">
					<th class="celda td text-right" colspan="4">PENALIZACIÓN POR MAYOR O IGUAL A <?php echo $total_horas->semanas_faltadas;?>Hras. DE FALTA:</th>
					<th class="celda th"></th>
					<th class="celda th"><?php echo $total_horas->penalizacion*-1;?></th>
				</tr>
				<tr class="fila">
					<th class="celda td text-right" colspan="4">TOTAL:</th>
					<th class="celda th"><?php echo $total_trabajadas;?></th>
					<th class="celda th"><?php echo ($total_adeudada+$total_horas->penalizacion)*-1;?></th>
				</tr>
			<?php } ?>
		</table>
		</div>	
	</div><br>
		<div class="row">
			<div class="col-md-4 col-md-offset-4 col-xs-12">
					<div class="input-group col-xs-12">
						<select class="form-control input-sm" id="c_fec" onchange="update_planilla_c_feriado('<?php echo $empleado->ide;?>','<?php echo $f1;?>','<?php echo $f2;?>')">
							<option value="">Seleccione...</option>
							<?php foreach ($vf as $key => $value) { ?>
								<option value="<?php echo $key;?>" <?php if($empleado->c_feriado==$key){ echo "selected";}?>><?php echo $value;?></option>
							<?php }?>
						</select>						
						<span class="input-group-addon input-sm" <?php echo $popover2.$help6;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					</div>
			</div>
			<div class="col-md-4 col-xs-12">
					<div class="input-group col-xs-12">
						<select class="form-control input-sm" id="c_dec" onchange="update_planilla_c_descuento('<?php echo $empleado->ide;?>','<?php echo $f1;?>','<?php echo $f2;?>')">
							<option value="">Seleccione...</option>
							<?php foreach ($vd as $key => $value) { ?>
								<option value="<?php echo $key;?>" <?php if($empleado->c_descuento==$key){ echo "selected";}?>><?php echo $value;?></option>
							<?php }?>
						</select>						
						<span class="input-group-addon input-sm" <?php echo $popover2.$help7;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					</div>
			</div>
		</div>
			<div class="table-responsive">	
				<table class="tabla tabla-border-true">
					<tr class="fila">
						<th class="celda td text-right" width="14%">Salario(Bs.):</th>
						<td class="celda td" width="6%"><?php echo $total_horas->sueldo_total;?></td>
						<th class="celda td text-right" width="14%">Días hábiles:</th>
						<td class="celda td" width="6%"><?php echo $total_horas->dias_habiles;?></td>
						<th class="celda td text-right" width="14%">Salario p/dia(Bs.):</th>
						<td class="celda td" width="6%"><?php echo $total_horas->por_dia;?></td>
						<th class="celda td text-right" width="14%">Jornal (horas):</th>
						<td class="celda td" width="6%"><?php echo $total_horas->horas_trabajo.' h.';?></td>
						<th class="celda td text-right" width="14%">Salario p/hora(Bs.):</th>
						<td class="celda td" width="6%"><?php echo $total_horas->por_hora;?></td>
					</tr>
					<tr class="fila">
						<th class="celda td text-right">Descuento(Bs.):</th>
						<td class="celda td"><?php echo $total_horas->descuento;?></td>
						<th class="celda td text-right">Sueldo a pagar(Bs.):</th>
						<td class="celda td"><?php echo number_format(number_format($total_horas->sueldo,0,'.',''),2,'.',',');?></td>
						<th class="celda td text-right">Anticipos(Bs.):</th>
						<td class="celda td"><?php echo number_format($t_anticipos,2,'.','');?></td>
						<th class="celda td text-right" colspan="2">TOTAL A PAGAR(Bs.):</th>
						<th class="celda th" colspan="2"><h3><?php echo number_format(number_format(($total_horas->sueldo-$t_anticipos),0,'.',''),2,'.',',');?></h3></th>
					</tr>
				</table>
			</div>

<script language='javascript'>$('[data-toggle="popover"]').popover({html:true});</script>