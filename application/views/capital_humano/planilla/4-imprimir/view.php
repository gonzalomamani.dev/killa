<?php 
	$j_horas=json_encode($horas);
	$j_feriados=json_encode($feriados);
	$empleados=json_decode($empleados);
	$contPagina=1;
	$contReg=0;
	$url=base_url().'libraries/img/personas/miniatura/';
	$hras_empleado=array();
	$cols=0;
	foreach ($empleados as $key => $empleado){ $hras_empleado[$empleado->ide.'']=$this->lib->horas_empleado($empleado->ide,$j_horas);}

	$horas_sabados=0;
	$dias_habiles=0;
	for ($i=0; $i < count($configuraciones); $i++) { $configuracion=$configuraciones[$i];
		if($configuracion->clave=="sueldo_hora_sabados"){
			$horas_sabados=$configuracion->valor*1;
		}
		if($configuracion->clave=="sueldo_dias_habiles"){
			$dias_habiles=$configuracion->valor*1;
		}
	}
?>
<div class="pagina table-responsive">
	<div class="encabezado_pagina">
		<?php $this->load->view('estructura/print/encabezado',['titulo'=>'PLANILLA DE SUELDOS Y SALARIOS','pagina'=>$contPagina,'sub_titulo'=> 'Del '.$f1." al ".$f2]);?>
	</div>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php if(!isset($v1)){?><th class="celda th" width="3%" >#Item</th><?php  $cols++;}?>
			<?php if(!isset($v2)){?><th class="celda th" width="5%" >Fotografía</th><?php  $cols++;}?>
			<?php if(!isset($v3)){?><th class="celda th" width="9%" >Nº de CI</th><?php  $cols++;}?>
			<?php if(!isset($v4)){?><th class="celda th" width="6%" >Cod. Bio.</th><?php  $cols++;}?>
			<?php if(!isset($v5)){?><th class="celda th" width="26%">Nombre de empleado</th><?php  $cols++;}?>
			<?php if(!isset($v6)){?><th class="celda th" width="7%" >Sueldo Básico (Bs.)</th><?php  $cols++;}?>
			<?php if(!isset($v7)){?><th class="celda th" width="7%" >Total Ganado (Bs.)</th><?php  $cols++;}?>
			<?php if(!isset($v8)){?><th class="celda th" width="7%" >Total anticipos (Bs.)</th><?php }?>
			<?php if(!isset($v9)){?><th class="celda th" width="7%" >Liquido pagable (Bs.)</th><?php }?>
			<?php if(!isset($v10)){?><th class="celda th" width="23%">Recibio conforme</th><?php }?>
		</tr>
		<?php $cont=0;$total_anticipo=0;$sub_total_anticipo=0;$total_sueldo=0;$sub_total_sueldo=0;
			foreach($empleados as $key => $empleado){
				$anticipos=$this->M_anticipo->get_empleado_fecha($empleado->ide,$f1,$f2);
				$total_horas=$this->lib->total_horas_trabajo(json_encode($hras_empleado[$empleado->ide]),$empleado,$j_feriados,$f1,$f2,$empleado->salario,$dias_habiles,$horas_sabados);
				$total_hrs=json_decode($total_horas);
				$t_ant=0;
				for($a=0;$a<count($anticipos);$a++){ $t_ant+=$anticipos[$a]->monto;}
				$img='default.png';
				if($empleado->fotografia!=NULL && $empleado->fotografia!=""){ $img=$empleado->fotografia;}
				if($contReg>=$nro){
					$contReg=0;
					$contPagina++;
		?>
		</table><!--cerramos tabla-->
	</div><!--cerramos pagina-->
<div class="pagina table-responsive">
	<?php $this->load->view('estructura/print/encabezado',['titulo'=>'PLANILLA DE SUELDOS Y SALARIOS','pagina'=>$contPagina,'sub_titulo'=> 'Del '.$f1." al ".$f2]);?>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php $cols=0;?>
			<?php if(!isset($v1)){?><th class="celda th" width="3%" >#Item</th><?php $cols++;}?>
			<?php if(!isset($v2)){?><th class="celda th" width="5%" >Fotografía</th><?php $cols++;}?>
			<?php if(!isset($v3)){?><th class="celda th" width="9%" >Nº de CI</th><?php $cols++;}?>
			<?php if(!isset($v4)){?><th class="celda th" width="6%" >Cod. Bio.</th><?php $cols++;}?>
			<?php if(!isset($v5)){?><th class="celda th" width="26%">Nombre de empleado</th><?php $cols++;}?>
			<?php if(!isset($v6)){?><th class="celda th" width="7%" >Sueldo Básico (Bs.)</th><?php $cols++;}?>
			<?php if(!isset($v7)){?><th class="celda th" width="7%" >Total Ganado (Bs.)</th><?php $cols++;}?>
			<?php if(!isset($v8)){?><th class="celda th" width="7%" >Total anticipos (Bs.)</th><?php }?>
			<?php if(!isset($v9)){?><th class="celda th" width="7%" >Liquido pagable (Bs.)</th><?php }?>
			<?php if(!isset($v10)){?><th class="celda th" width="23%">Recibio conforme</th><?php }?>
		</tr>
		<?php $sub_total=0;}//end if ?>
		<tr class="fila">
			<?php if(!isset($v1)){?><td class="celda td"><?php echo $cont+1;$cont++;?></td><?php } ?>
			<?php if(!isset($v2)){?><td class="celda td"><img src="<?php echo $url.$img;?>" style='width:100%'></td><?php } ?>
			<?php if(!isset($v3)){?><td class="celda td"><?php echo $empleado->ci.' '.$empleado->abreviatura;?></td><?php } ?>
			<?php if(!isset($v4)){?><td class="celda td"><?php echo $empleado->codigo;?></td><?php } ?>
			<?php if(!isset($v5)){?><td class="celda td"><?php echo $empleado->nombre.' '. $empleado->nombre2.' '. $empleado->paterno.' '. $empleado->materno;?></td><?php } ?>
			<?php if(!isset($v6)){?><td class="celda td"><?php echo number_format($empleado->salario,2,'.',',');?></td><?php } ?>
			<?php if(!isset($v7)){?>
			<?php $sueldo_ganado=0; if(isset($total_hrs->sueldo)){ if($total_hrs->sueldo>=0){ $sueldo_ganado=number_format($total_hrs->sueldo,0,'.',''); }}?>
			<td class="celda td"><?php echo number_format($sueldo_ganado,2,'.',',');?></td><?php } ?>
			<?php if(!isset($v8)){?><td class="celda td"><?php echo number_format($t_ant,2,'.',','); $total_anticipo+=$t_ant;?></td><?php } ?>
			<?php $pagable=number_format($total_hrs->sueldo-$t_ant,0,'.','');  ?>
			<?php if(!isset($v9)){?><td class="celda td"><?php echo number_format($pagable,2,'.',','); $total_sueldo+=$pagable;?></td><?php } ?>
			<?php if(!isset($v10)){?><td class="celda td"></td><?php } ?>
		</tr>
	<?php $contReg++;}// end for ?>
		<tr class="fila">
			<?php if($cols!=0){?><th class="celda th" colspan="<?php echo $cols;?>">Total Bs.: </th><?php }?>
			<?php if(!isset($v8)){?><th class="celda th"><?php echo number_format($total_anticipo,2,'.',',');?></th><?php }?>
			<?php if(!isset($v9)){?><th class="celda th"><?php echo number_format($total_sueldo,2,'.',',');?></th><?php }?>
			<?php if(!isset($v10)){?><th class="celda th"></th><?php }?>
		</tr>
	</table>
</div>
<script type="text/javascript"> $(".nroPagina").html(<?php echo $contPagina;?>); </script>