<?php 
	$j_horas=json_encode($horas);
	$j_feriados=json_encode($feriados);
	$j_empleados=json_encode($empleados);
	$total_anticipos=0;
	$total_sueldo=0;

	//
	$horas_sabados=0;
	$dias_habiles=0;
	for ($i=0; $i < count($configuraciones); $i++) { $configuracion=$configuraciones[$i];
		if($configuracion->clave=="sueldo_hora_sabados"){
			$horas_sabados=$configuracion->valor*1;
		}
		if($configuracion->clave=="sueldo_dias_habiles"){
			$dias_habiles=$configuracion->valor*1;
		}
	}
?>
	<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr><th colspan="13" style="text-align:center;"><h2>PLANILLA DE SUELDOS</h2>Del <?php echo $this->validaciones->formato_fecha($f1,'d,m,Y');?> Al <?php echo $this->validaciones->formato_fecha($f2,'d,m,Y');?></th></tr>
		<tr>
			<td class='g-thumbnail'><div class="g-img"></div></td>
			<th width="10%">CI <?php echo $dias_habiles." ".$horas_sabados;?></th>
			<th width="45%">Nombre</th>
			<th width="10%">Sueldo<br>Básico (Bs)</th>
			<th width="10%">Total<br>Ganado (Bs.)</th>
			<th width="10%">Total Anticipos (Bs.)</th>
			<th width="10%">Liquido<br>pagable (Bs.)</th>
			<th width="5%"></th>
		</tr>
	</thead>
		<tbody>
		<?php if (count($empleados)>0) {
			$url=base_url().'libraries/img/personas/';
			$hras_empleado=array();
			for ($i=0; $i < count($empleados) ; $i++) { 
				$hras_empleado[$empleados[$i]->ide.'']=$this->lib->horas_empleado($empleados[$i]->ide,$j_horas);
			}
		?>
		<?php 
			$total_anticipos=0;
			$total_sueldo=0;
			for($i=0; $i < count($empleados); $i++){ $empleado=$empleados[$i]; $img="default.png";
				$anticipos=$this->M_anticipo->get_empleado_fecha($empleado->ide,$f1,$f2);
				$total_horas=$this->lib->total_horas_trabajo(json_encode($hras_empleado[$empleado->ide]),$empleado,$j_feriados,$f1,$f2,$empleado->salario,$dias_habiles,$horas_sabados);
				$total_hrs=json_decode($total_horas);
			if($empleado->fotografia!="" && $empleado->fotografia!=NULL){ $img=$empleado->fotografia;} ?>
			<tr>
			<td><div id="item"><?php echo $i+1;?></div><a href="javascript:" onclick="fotografia('<?php echo $url.$img;?>')"><img src="<?php echo $url.'miniatura/'.$img;?>" width="100%" class="img-thumbnail"></a></td>
				<td><?php echo $empleado->ci;?></td>
				<td><?php echo $empleado->nombre." ".$empleado->nombre2." ".$empleado->paterno." ".$empleado->materno; ?></td>
				<td class="text-right"><?php echo number_format(number_format($total_hrs->sueldo_total,1,'.',''),2,'.',',');?></td>
				<?php $sueldo_ganado=0; if(isset($total_hrs->sueldo)){ if($total_hrs->sueldo>=0){ $sueldo_ganado=number_format($total_hrs->sueldo,0,'.',''); }}?>
				<td class="text-right"><?php echo number_format($sueldo_ganado,2,'.',',');?></td>
				<?php $t_ant=0; for($a=0; $a <count($anticipos); $a++){ $t_ant+=$anticipos[$a]->monto; } ?>
				<td class="text-right"><a href="javascript:" onclick="detalle_anticipos('<?php echo $empleado->ide;?>','<?php echo $f1;?>','<?php echo $f2;?>')" title="Ver detalle de anticipos"><?php echo number_format($t_ant,2,'.',','); $total_anticipos+=$t_ant;?></a></td>
				<?php $pagable=number_format($total_hrs->sueldo-$t_ant,0,'.','');  ?>
				<td class="text-right"><?php echo number_format($pagable,2,'.',','); $total_sueldo+=$pagable; ?>
					<?php if(count($hras_empleado[$empleados[$i]->ide.''])>0){
								if($total_hrs->control){
									$img="ok_min.png";
								}else{
									$img="alert_min.png";
								}
							}else{
							 	$img="error_min.png";
							}
					?>
					<img src="<?php echo base_url().'libraries/img/sistema/'.$img;?>">
				</td>
				<?php $mod="config_planilla('".$empleado->ide."','".$f1."','".$f2."')"; if($privilegio[0]->ca2u!="1"){ $mod="";}?>
				<td class="text-right"><?php $this->load->view('estructura/botones/botones_registros',['reportes'=>"reporte_planilla('".$empleado->ide."','".$f1."','".$f2."')",'configuracion'=>$mod]);?></td>
			</tr>
		<?php }//end for ?>
		<?php } else{ echo "<tr><td colspan='9'><h2>0 registros encontrados...</h2></td></tr>"; } ?>
			<tr>
				<th colspan="5" class="text-right">TOTAL (Bs.): </th>
				<th class="text-right"><?php echo number_format($total_anticipos,2,'.',',');?></th>
				<th class="text-right"><?php echo number_format($total_sueldo,2,'.',',');?></th>
			</tr>
		</tbody>
	</table>
<script language="javascript" type="text/javascript"> 
	Onfocus('s_cod');
	$("#print").removeAttr("onclick"); 
	$("#print").unbind("click"); 
	$("#print").click(function(){ imprimir_planilla('<?php echo $j_empleados;?>','<?php echo $f1?>','<?php echo $f2;?>'); }); 
</script>