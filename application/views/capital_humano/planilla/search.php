<?php 
	
	$aux=strtotime('-1 month' ,strtotime(date('Y-m').'-01'));
	$f1=date('Y-m',$aux);
	$f2=date('Y')."-".$this->lib->dosDigitos(date("m")-1);
?>
<table class="tabla">
	<tr>
		<td class='celda-sm g-thumbnail'><div class="g-img"></div></td>
		<td class="celda-sm" width="12%"><form onsubmit="return view_planilla();"><input type="text" id="s_ci" class="form-control input-sm" placeholder="N° de carnet" onkeyup="reset_input(this.id)"></form></td>
		<td class="celda-sm" width="44%"><form onsubmit="return view_planilla();"><input type="text" id="s_nom" class="form-control input-sm" placeholder="Nombre completo" onkeyup="reset_input(this.id)"></form></td>
		<td class="celda td" width="17%"><input type="month" id="f1" class="form-control input-sm" value="<?php echo $f1;?>" onchange=""></td>
		<td class="celda td" width="17%"><input type="month" id="f2" class="form-control input-sm" value="<?php echo $f2;?>" onchange=""></td>
		<td class="celda td text-right" width="10%">
			<?php $new="new_biometrico()"; if($privilegio[0]->ca2c!="1"){ $new="";}?>
			<?php $print="print()"; if($privilegio[0]->ca2p!="1"){ $print="";}?>
			<?php $this->load->view('estructura/botones/buscador',['f_buscar'=>"view_planilla()",'f_ver'=>"",'nuevo'=>'Importar archivo biometrico','f_nuevo'=>$new,'f_imprimir'=>$print]);?>
		</td>		
	</tr>
</table>
