<table class="tabla tabla-border-false">
	<tr class="fila">
		<th class="celda th">Seleccione Archivo Biometrico, en formato Excel 97-2003(.xls) </th>
	</tr>
	<tr class="fila">
		<td class="celda td">
			<input type="file" id="archivo" class="form-control input-xs">
		</td>
	</tr>
	<tr class="fila">
		<td class="celda td">
			Buscar por: 
			<select id="tipo" class="form-control input-sm">
				<option value="0">Código de registro biométrico</option>
				<option value="1">Nº de cédula de empleado</option>
			</select>
		</td>
	</tr>
</table>