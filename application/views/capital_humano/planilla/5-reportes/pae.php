<?php 	
	$j_feriados=json_encode($feriados);
		if($empleado->nombre2!="" & $empleado->nombre2!=NULL){
			$nombre=$empleado->nombre." ".$empleado->nombre2." ".$empleado->paterno." ".$empleado->materno;
			$nombreCompleto=$this->lib->may_palabra($nombre);
		}else{
			$nombre=$empleado->nombre." ".$empleado->paterno." ".$empleado->materno;
			$nombreCompleto=$this->lib->may_palabra($nombre);
		}
		$vfec1=explode("-",$fe1);
		$vfec2=explode("-",$fe2);
		$mesLiteral=$this->lib->mes_literal($vfec1[1])."-".$this->lib->mes_literal($vfec2[1]);
		if($vfec1[1]==$vfec2[1]){
			$mesLiteral=$this->lib->mes_literal($vfec1[1]);
		}
		$dias_habiles=$this->lib->dias_habiles_2($fe1,$fe2,$empleado->c_feriado,$j_feriados);
?>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="reporte_planilla('<?php echo $empleado->ide; ?>','<?php echo $fe1;?>','<?php echo $fe2;?>')">Sueldos</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="pae('<?php echo $empleado->ide; ?>','<?php echo $fe1;?>','<?php echo $fe2;?>')">PAE</a></li>
</ul>
<div class="table-responsive">
	<div id="area">
		<h3>&nbsp;</h3>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>libraries/css/print.css" media="print">
			<div id="print_planilla">
				<h3>PROGRAMA DE APOYO AL EMPLEO</h3>
				<h4>PLANILLA DE ASISTENCIA - PAE</h4>
				<table width="100%" border="1">
					<tr>
						<th colspan="2" class="t1">NOMBRE: </th>
						<td colspan="6" class="t2"><?php echo $nombreCompleto; ?></td>
						<th class="t1">CI: </th>
						<td colspan="2" class="t2"><?php echo $empleado->ci." ".$empleado->abreviatura; ?></td>
						<th colspan="2" class="t1">PERIODO: </th>
						<td colspan="8" class="t2"><?php echo $mesLiteral;?></td>
					</tr>
					<tr>
						<th class='t1' colspan="2">EMPRESA:</th>
						<td class='t2' colspan="9">K'illa</td>
						<th class='t1' colspan="3">SUPERVISOR:</th>
						<td class='t2' colspan="7">Julio Reas Patzi</td>
						
					</tr>
					<tr>
						<th class='t1' colspan="4">DIAS LABORALES:</th>
						<td class='t2' colspan="7"><?php echo $dias_habiles;?></td>
						<th class='t1' colspan="4">DIAS TRABAJADOS:</th>
						<td class='t2' colspan="6"><?php echo $dias_habiles;?></td>
					</tr>
			<?php $hras="";$cont_habil=0;
					$fecha1=strtotime($fe1);
					$fecha2=strtotime($fe2);
				while($fecha1<=$fecha2){
					$feriados="";
				?>
					<tr><th colspan="21" class='t0'></th></tr>
					<tr>
					<?php for ($i=1; $i <=6 ; $i++){
						$f1=date('Y-m-d',$fecha1); $vf1=explode("-", $f1); $num_dia=$this->lib->que_dia_es($vf1[2],$vf1[1],$vf1[0]);
						$tipo_dia=$this->lib->es_habil_domingo_sabado_feriado($f1,$j_feriados);
						switch ($tipo_dia) {
							case 'F': $feriados.="|".$i;break;
						}
						?>
						<th class='t0 fecha' colspan="3"><?php if($num_dia==$i && $fecha1<=$fecha2){?> <?php echo $this->lib->format_date($f1,'dl ml2 Y');$fecha1=strtotime('+1 day',$fecha1); $hras.="|true";}else{ $hras.="|false";}?></th>
						<?php
					}?>
						<?php $f1=date('Y-m-d',$fecha1); $vf1=explode("-", $f1); $num_dia=$this->lib->que_dia_es($vf1[2],$vf1[1],$vf1[0]);?>
						<th class='t0 fecha' colspan="3"><?php  if($num_dia==0 && $fecha1<=$fecha2){ echo $this->lib->format_date($f1,'dl ml2 Y');$fecha1=strtotime('+1 day',$fecha1); $hras.="|true";}else{ $hras.="|false";} ?></th>
					</tr>
					
					<?php
						$cont=0;
						//echo "<h3>".$hras."</h3>";
						$vfer=explode("|", $feriados);
						$vh=explode("|", $hras);
							?>
							<tr>
						<?php for ($i=1; $i <=7 ; $i++) { ?>
									<th class='t0 celda' width='4.5%'>Ingreso</th>
									<th class='t0 celda' width='4.5%'>Salida</th>
									<th class='t0 celda' width='5,1%'>Firma</th>
								
								<?php
							}// end for?>
							</tr>
							<tr>
							<?php for ($i=1; $i <=7 ; $i++) {//hora de la mañana
								$es="";
								for($k=1;$k<count($vfer);$k++){
									if($vfer[$k]==$i){
										$es="si";
										break;
									}
								}
								if($vh[$i]=="true" & $i!=7 & $es!="si"){ 

									if($i==6){
										?>
									<td class='t0 celda'>08:<?php echo rand(55,59);?></td>
									<td class='t0 celda'>12:<?php $min=rand(0,10); if($min<10){$min="0".$min;} echo $min;?></td>
									<td class='t0 celda'></td>
									<?php
									}else{
									?>
									<td class='t0 celda'>07:<?php echo rand(55,59);?></td>
									<td class='t0 celda'>12:<?php echo rand(30,40);?></td>
									<td class='t0'></td>
									<?php }?>
								<?php
								}else{
									?>
									<td class='t0'>&nbsp;</td>
									<td class='t0'>&nbsp;</td>
									<td class='t0'>&nbsp;</td>
									<?php
								}
							}// end for?>
							</tr>
							<tr>
							<?php for ($i=1; $i <=7 ; $i++) {//hora de la tarde
								$es="";
								for($k=1;$k<count($vfer);$k++){
									if($vfer[$k]==$i){
										$es="si";
										break;
									}
								}
								if($vh[$i]=="true" & $i!=7 & $i!=6 & $es!="si"){ ?>

									<td class='t0  celda'>13:<?php echo rand(55,59);?></td>
									<td class='t0  celda'>18:<?php echo rand(30,40);?></td>
									<td class='t0'></td>

								<?php
								}else{
									?>
									<td class='t0'>&nbsp;</td>
									<td class='t0'>&nbsp;</td>
									<td class='t0'>&nbsp;</td>
									<?php
								}
							}// end for?>
							</tr>
							<?php
						$hras="";
					?>
					
			<?php }?>
			<tr>
				<th class='t0 celda pie' colspan="21">*No se permiten enmiendas, tachaduras ni borrones en la planilla</th>
			</tr>
			<tr>
				<th colspan="9" class='t0 celda'><h3>&nbsp;</h3><h3>&nbsp;</h3><h3>&nbsp;</h3><h3>&nbsp;</h3><h3>&nbsp;</h3>FIRMA/SELLO EMPRESA</th>
				<th colspan="12" class='t0 celda'><h3>&nbsp;</h3><h3>&nbsp;</h3><h3>&nbsp;</h3><h3>&nbsp;</h3><h3>&nbsp;</h3>FIRMA/SELLO OFICIAL OPERATIVO</th>
			</tr>
			
				</table>
				<div class='pequeno' colspan="21">EACH./</div>
			</div>
	</div>
</div>