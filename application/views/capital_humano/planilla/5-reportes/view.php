<?php
	$horas_sabados=0;
	$dias_habiles=0;
	for ($i=0; $i < count($configuraciones); $i++) { $configuracion=$configuraciones[$i];
		if($configuracion->clave=="sueldo_hora_sabados"){
			$horas_sabados=$configuracion->valor*1;
		}
		if($configuracion->clave=="sueldo_dias_habiles"){
			$dias_habiles=$configuracion->valor*1;
		}
	}



	$j_horas=json_encode($horas);
	$j_feriados=json_encode($feriados);
	$horas_dia=json_decode($this->lib->horas_dia($j_horas,$empleado,$j_feriados,$f1,$f2,$horas_sabados));
	$contPagina=1;
	$contReg=0;
	$total_trabajadas=0;
	$total_adeudada=0;
	$t_anticipos=0;
	for($i=0; $i<count($anticipos) ; $i++){ $t_anticipos+=$anticipos[$i]->monto; }



?>
<?php $total_horas=json_decode($this->lib->total_horas_trabajo($j_horas,$empleado,$j_feriados,$f1,$f2,$empleado->salario,$dias_habiles,$horas_sabados)); ?>
<ul class="nav nav-tabs">
  <li role="presentation" class="active"><a href="javascript:" onclick="reporte_planilla('<?php echo $empleado->ide; ?>','<?php echo $f1;?>','<?php echo $f2;?>')">Sueldos</a></li>
  <li role="presentation"><a href="javascript:" onclick="pae('<?php echo $empleado->ide; ?>','<?php echo $f1;?>','<?php echo $f2;?>')">PAE</a></li>
</ul><hr>
<div class="row">
<div class="col-md-3 col-md-offset-6 col-xs-12 text-right"><b>Nº de reg. por hoja:</b></div>
<div class="col-md-3 col-xs-12">
	<select id="nro" class="form-control input-sm">
		<?php for($i=1; $i <= 70 ; $i++) { ?>
			<option value="<?php echo $i;?>" <?php if(isset($nro)){ if($i==$nro){echo "selected";}}else{ if($i==57){echo "selected";}}?>><?php echo $i;?></option>
		<?php } ?>
	</select>
</div></div>
<div class="table-responsive">
	<div id="area">
		<div class="pagina">
			<?php $this->load->view('estructura/print/encabezado',['titulo'=>'DETALLE DE HORAS BIOMETRICO','pagina'=>$contPagina,'sub_titulo'=>'']);?>
			<table class="tabla tabla-border-true">
				<tr class="fila">
					<th class="celda th" colspan="2">Nombre empleado:</th>
					<td class="celda td" colspan="2"><?php echo $empleado->nombre.' '.$empleado->nombre2.' '.$empleado->paterno.' '.$empleado->materno;?></td>
					<th class="celda th">Nº CI</th>
					<td class="celda td"><?php echo $empleado->ci;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th" width="8%">Año</th>
					<th class="celda th" width="15%">Mes</th>
					<th class="celda th" width="17%">Día</th>
					<th class="celda th" width="30%">Detalle de horas</th>
					<th class="celda th" width="15%">Horas Trabajadas</th>
					<th class="celda th" width="15%">Descuento</th>
				</tr>
<?php foreach($horas_dia as $clave => $hora){ $v=explode("-", $hora->fecha);//sacando los dias
		$trabajadas=json_decode(json_encode($hora->horas_trabajadas));
		if($trabajadas->horas_trabajadas!=0 || $trabajadas->horas_adeudadas!=0){//no se mostrara
				if($contReg>=$nro){
					$contReg=0;
					$contPagina++;
			?>
			</table>
		</div>
		<div class="pagina">
			<?php $this->load->view('estructura/print/encabezado',['titulo'=>'DETALLE DE HORAS BIOMETRICO','pagina'=>$contPagina,'sub_titulo'=>'']);?>
			<table class="tabla tabla-border-true">
				<tr class="fila">
					<th class="celda th" colspan="2">Nombre empleado:</th>
					<td class="celda td" colspan="2"><?php echo $empleado->nombre.' '.$empleado->nombre2.' '.$empleado->paterno.' '.$empleado->materno;?></td>
					<th class="celda th">Nº CI</th>
					<td class="celda td"><?php echo $empleado->ci;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th" width="8%">Año</th>
					<th class="celda th" width="15%">Mes</th>
					<th class="celda th" width="17%">Día</th>
					<th class="celda th" width="30%">Detalle de horas</th>
					<th class="celda th" width="15%">Horas Trabajadas</th>
					<th class="celda th" width="15%">Descuento</th>
				</tr>
		<?php }// end if ?>
				<tr class="fila">
					<td class="celda td" style="font-size: .9em;"><?php echo $v[0];?></td>
					<td class="celda td" style="font-size: .9em;"><?php echo $this->lib->may_cadena($this->lib->mes_literal($v[1]));?></td>
					<td class="celda td" style="font-size: .9em;"><?php echo $this->lib->nombre_de_dia($this->lib->que_dia_es($v[2],$v[1],$v[0]))." ".$v[2];?></td>
					<?php $hrs=json_decode(json_encode($hora->horas));?>
					<td class="celda no-padding td">
				<?php if(count($hora->horas)>0){ ?>
						<table class="tabla tabla-border-true">
			<?php foreach($hrs as $key => $hr){//sacando detalle del dia
						if($contReg >= $nro){
							$contReg=0;
							$contPagina++;
			?>
						</table>
					</td>
					<td class="celda td" style="font-size: .9em;"><?php echo $trabajadas->horas_trabajadas;?></td>
					<td class="celda td" style="font-size: .9em;"><?php echo $trabajadas->horas_adeudadas*-1;?></td>
				</tr>
				</table>
			</div>
			<div class="pagina">
				<?php $this->load->view('estructura/print/encabezado',['titulo'=>'DETALLE DE HORAS BIOMETRICO','pagina'=>$contPagina,'sub_titulo'=>'']);?>
				<table class="tabla tabla-border-true">
					<tr class="fila">
						<th class="celda th" colspan="2">Nombre empleado:</th>
						<td class="celda td" colspan="2"><?php echo $empleado->nombre.' '.$empleado->nombre2.' '.$empleado->paterno.' '.$empleado->materno;?></td>
						<th class="celda th">Nº CI</th>
						<td class="celda td"><?php echo $empleado->ci;?></td>
					</tr>
					<tr class="fila">
						<th class="celda th" width="8%">Año</th>
						<th class="celda th" width="15%">Mes</th>
						<th class="celda th" width="17%">Día</th>
						<th class="celda th" width="30%">Detalle de horas</th>
						<th class="celda th" width="15%">Horas Trabajadas</th>
						<th class="celda th" width="15%">Descuentoooo</th>
					</tr>
					<tr class="fila">
						<td class="celda td" style="font-size: .9em;"><?php echo $v[0];?></td>
						<td class="celda td" style="font-size: .9em;"><?php echo $this->lib->may_cadena($this->lib->mes_literal($v[1]));?></td>
						<td class="celda td" style="font-size: .9em;"><?php echo $this->lib->nombre_de_dia($this->lib->que_dia_es($v[2],$v[1],$v[0]))." ".$v[2];?></td>
						<?php $hrs=json_decode(json_encode($hora->horas));?>
						<td class="celda no-padding td">
							<table class="tabla tabla-border-true">
						<?php }// end if ?>
								<tr class="fila">
									<td class="celda no-padding td"><?php echo $hr->tipo;?></td>
									<td class="celda no-padding td"><?php echo $hr->hora;?></td>
									<td class="celda no-padding td"><?php echo $hr->hora_redondo;?></td>
								</tr>
			<?php $contReg++; }// end forearch ?>
							</table>
						<?php }// end if ?>
						</td>
						<td class="celda td" style="font-size: .9em;"><?php echo $trabajadas->horas_trabajadas; $total_trabajadas+=$trabajadas->horas_trabajadas; ?></td>
						<td class="celda td" style="font-size: .9em;"><?php echo $trabajadas->horas_adeudadas*-1; $total_adeudada+=$trabajadas->horas_adeudadas; ?></td>
					</tr>
<?php $contReg++; 
		}// end if
	}// end forearch  ?>
			<tr class="fila">
				<th class="celda td text-right" colspan="4">TOTAL:</th>
				<th class="celda th"><?php echo $total_trabajadas;?></th>
				<th class="celda th"><?php echo $total_adeudada*-1;?></th>
			</tr>
			<?php if($total_horas->penalizacion>0){?>
				<tr class="fila">
					<th class="celda td text-right" colspan="4">PENALIZACIÓN POR MAYOR O IGUAL A <?php echo $total_horas->semanas_faltadas;?>Hras. DE FALTA:</th>
					<th class="celda th"></th>
					<th class="celda th"><?php echo $total_horas->penalizacion*-1;?></th>
				</tr>
				<tr class="fila">
					<th class="celda td text-right" colspan="4">TOTAL:</th>
					<th class="celda th"><?php echo $total_trabajadas;?></th>
					<th class="celda th"><?php echo ($total_adeudada+$total_horas->penalizacion)*-1;?></th>
				</tr>
			<?php } ?>
		</table>

		<table class="tabla tabla-border-true">
			<tr class="fila">
				<th class="celda td text-right" width="14%">Salario(Bs.):</th>
				<td class="celda td" width="6%"><?php echo number_format(number_format($total_horas->sueldo_total,1,'.',''),2,'.',',');?></td>
				<th class="celda td text-right" width="14%">Días hábiles:</th>
				<td class="celda td" width="6%"><?php echo $total_horas->dias_habiles;?></td>
				<th class="celda td text-right" width="14%">Jornal (horas):</th>
				<td class="celda td" width="6%"><?php echo $total_horas->horas_trabajo.' h.';?></td>
				<th class="celda td text-right" width="14%">Salario p/hora(Bs.):</th>
				<td class="celda td" width="6%"><?php echo number_format(number_format($total_horas->por_hora,1,'.',''),2,'.',',');?></td>
				<th class="celda td text-right" width="14%">Descuento(Bs.):</th>
				<td class="celda td"><?php echo number_format(number_format($total_horas->descuento,1,'.',''),2,'.',',');?></td>
			</tr>
			<tr class="fila">
				<th class="celda td text-right">Sueldo a pagar(Bs.):</th>
				<td class="celda td"><?php echo number_format(number_format($total_horas->sueldo,1,'.',''),2,'.',',');?></td>
				<th class="celda td text-right">Anticipos(Bs.):</th>
				<td class="celda td"><?php echo number_format($t_anticipos,2,'.','');?></td>
				<th class="celda td text-right">Total sueldo(Bs.):</th>
				<td class="celda td"><?php echo number_format(number_format(($total_horas->sueldo-$t_anticipos),1,'.',''),2,'.',',');?></td>
				<th class="celda td text-right" colspan="2">TOTAL A PAGAR(Bs.):</th>
				<th class="celda th" colspan="2" style="font-size: 1em;"><?php echo number_format(number_format(($total_horas->sueldo-$t_anticipos),0,'.',''),2,'.',',');?></th>
			</tr>
		</table>

		</div>	
	</div>
</div>
<script language="javascript">
	$("#nro").change(function(){ nro_planilla('<?php echo $empleado->ide;?>','<?php echo $f1; ?>','<?php echo $f2; ?>'); });
	$(".nroPagina").html(<?php echo $contPagina;?>); 
</script>