<?php
	$help1='title="<h4>Nombre de pais<h4>" data-content="Ingrese un nombre alfanumerico <strong>de 2 a 100 caracteres con espacios</strong>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help2='title="<h4>Nombre de ciudad<h4>" data-content="Ingrese un nombre de ciudad alfanumerico <strong>de 2 a 100 caracteres con espacios</strong>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help3='title="<h4>Nombre de pais<h4>" data-content="Seleccione el pais donde pertenece la ciudad, el valor vacio no es aceptado."';
	$help4='title="<h4>Fecha<h4>" data-content="Representa a la fecha de feriado, esta fecha es usada principalmente el las horas de trabajo en planilla de sueldo y en pedido para proyectar el dia de entrega. <b>el formado de la fecha debe ser 2000-01-31</b>."';
	$help45='title="<h4>Tipo de jornal<h4>" data-content="Representa al tipo de jornal que tomara el sistema, es decir si en jornal completo el sistema tomara todo el dia libre para los trabajadores y si es medio jornal el sistema tomara como medio dia libre a los empleados."';
	$help5='title="<h4>Descripción<h4>" data-content="Ingrese un detalle de la fecha, a que tipo de feriado representa la fecha ingresada, el contenido debe tener un formato alfanumerico de 5 a 150 caracteres <b>puede incluir espacios y sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑº+-ª.,:;)</b>."';
	$help6='title="<h4>Tipo<h4>" data-content="Tipo de contrato, este tipo debe ser real, pues esta carga horaria es usada en el calculo de horas trabajas según el registro biométrico, una vez asignado al empleado."';
	$help7='title="<h4>Horas<h4>" data-content="Esta hora representa a las horas de trabajo por día según el tipo de contrato seleccionado, esta carga es <b>usada en el calculo de horas trabajadas según el registro biométrico</b>, una vez asignado al empleado. es sistema acepta <b>valores a 0 a 99</b>"';
	$help8='title="<h4>Descripción<h4>" data-content="Ingrese un comentario a la carga horaria su tuviera, el contenido debe tener un formato alfanumerico <b>de 0 a 150</b> caracteres <b>puede incluir espacios y sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑº+-ª.,:;)</b>."';
	$help9='title="<h4>Hora principal<h4>" data-content="Representa la hora principal que sera tomado en cuenta por el sistema, esta hora es usada en la seecion de pedidos <b>para proyectar la fecha de entrega del pedidos</b> tomando la hora seleccionada como jornal de trabajo diario por los empleados."';
	$popover='data-toggle="popover" data-placement="right" data-trigger="hover"';
	$popover2='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$popover3='data-toggle="popover" data-placement="top" data-trigger="hover"';
	$v = array('0' => 'Tiempo completo','1' => 'Medio Tiempo');
	$vf = array('0' => 'Jornada completa','1' => 'Media jornada');
?>
<div class="col-md-6 col-xs-12">
	<h3>PAISES</h3>
	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<th width="5%">#</th>
				<th width="85%">
				<div class="input-group col-xs-12">
					<span class="input-group-addon input-sm" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					<div class="form-control input-sm" disabled>Nombre</div>
				</div></th>
				<th width="10%"></th>
			</tr>
		</thead>
		<tbody>
	<?php for($i=0; $i<count($paises); $i++){ $pais=$paises[$i]; $c=$this->M_ciudad->get_row('idpa',$pais->idpa);?>
		<tr>
			<td><?php echo $i+1;?></td>
			<td>
			<form onsubmit="return update_pais('<?php echo $pais->idpa;?>')"><input type="text" class="form-control input-sm" id="p<?php echo $pais->idpa;?>" placeholder='Nombre de pais' value="<?php echo $pais->nombre;?>" maxlength="100"></form></td>
			<?php $str="";
				if(count($c)>0){
					$str="<hr><strong class='text-danger'>¡Imposible Eliminar el color, esta siendo usado por ".count($c)." ciudad(es)!</strong>";
					$fun="disabled";
				}else{
					$fun="alerta_pais('".$pais->idpa."')";
				}
				$help='title="<h4>Detalles de pais<h4>" data-content="<b>Nombre: </b>'.$pais->nombre.'<br>'.$str.'"';
			?>
			<td><?php $this->load->view('estructura/botones/botones_registros',['detalle'=>$popover3.$help,'guardar'=>"update_pais('".$pais->idpa."')",'eliminar'=>$fun]);?></td>
		</tr>
	<?php } ?>
		</tbody>
		<thead>
			<tr>
				<th colspan="3" class="text-center">NUEVO</th>
			</tr>
			<tr>
				<td colspan="2"><form onsubmit="return save_pais()">
					<div class="input-group col-xs-12">
						<input type="text" id="p" class="form-control input-sm" placeholder='Nombre de pais'>
						<span class="input-group-addon input-sm" <?php echo $popover3.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					</div>
				</form></td>
				<td><?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"save_pais()",'detalle'=>"",'eliminar'=>""]);?></td>
			</tr>
		</thead>
	</table>
	
	<h3>FERIADOS</h3>
	<div class="table-responsive">
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
					<th width="5%">#</th>
					<th width="15%">
						<div class="input-group col-xs-12">
							<span class="input-group-addon input-sm" <?php echo $popover.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
							<div class="form-control input-sm" disabled>Fecha</div>
						</div>
					</th>
					<th width="20%">
						<div class="input-group col-xs-12">
							<span class="input-group-addon input-sm" <?php echo $popover.$help45;?>><i class='glyphicon glyphicon-info-sign'></i></span>
							<div class="form-control input-sm" disabled>Tipo</div>
						</div>
					</th>
					<th width="50%">
					<div class="input-group col-xs-12">
						<span class="input-group-addon input-sm" <?php echo $popover3.$help5;?>><i class='glyphicon glyphicon-info-sign'></i></span>
						<div class="form-control input-sm" disabled>Descripción</div>
					</div></th>
					<th width="10%"></th>
				</tr>
			</thead>
			<tbody>
		<?php for($i=0;$i<count($feriados);$i++){ $feriado=$feriados[$i];?>
			<tr>
				<td><?php echo $i+1;?></td>
				<td><input type="date" class="form-control input-sm" id="f_fec<?php echo $feriado->idf;?>" placeholder='2000-01-31' value="<?php echo $feriado->fecha;?>"></td>
				<td>
					<select id="f_tip<?php echo $feriado->idf;?>" class="form-control input-sm">
						<option value="">Seleccionar...</option>
					<?php foreach ($vf as $clave => $valor) {?>
						<option value="<?php echo $clave;?>" <?php if($feriado->tipo==$clave){ echo "selected";}?>><?php echo $valor;?></option>
					<?php }?>
					</select>
				</td>
				<td><textarea class="form-control input-sm" id="f_des<?php echo $feriado->idf;?>" placeholder="Descripción del feriado"><?php echo $feriado->descripcion;?></textarea></td>
				<?php $help='title="<h4>Detalles de feriado<h4>" data-content="<b>Fecha: </b>'.$feriado->fecha.'<br><b>Descripción: </b>'.$feriado->descripcion.'"'; ?>
				<td><?php $this->load->view('estructura/botones/botones_registros',['detalle'=>$popover3.$help,'guardar'=>"update_feriado('".$feriado->idf."')",'eliminar'=>"alerta_feriado('".$feriado->idf."','".$feriado->fecha."','".$feriado->descripcion."')"]);?></td>
			</tr>
		<?php } ?>
			</tbody>
			<thead>
				<tr><th colspan="3" class="text-center">NUEVO</th></tr>
				<tr>
					<td colspan="2"><form onsubmit="return save_feriado()">
						<div class="input-group col-xs-12">
							<input type="date" class="form-control input-sm" id="f_fec" placeholder='2000-01-31'>
							<span class="input-group-addon input-sm" <?php echo $popover3.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
						</div>
					</form></td>
					<td>
						<select id="f_tip" class="form-control input-sm">
							<option value="">Seleccionar...</option>
						<?php foreach ($vf as $clave => $valor) {?>
							<option value="<?php echo $clave;?>"><?php echo $valor;?></option>
						<?php }?>
						</select>
					</td>
					<td>
						<textarea class="form-control input-sm" id="f_des"  placeholder="Descripción del feriado"></textarea>
					</td>
					<td><?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"save_feriado()",'detalle'=>"",'eliminar'=>""]);?></td>
				</tr>
			</thead>
		</table>
	</div>
</div>
<div class="col-md-6 col-xs-12">
	<h3>CIUDADES</h3>
	<div class="table-responsive">
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
					<th width="5%">#</th>
					<th width="45%">
					<div class="input-group col-xs-12">
						<span class="input-group-addon input-sm" <?php echo $popover.$help2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
						<div class="form-control input-sm" disabled>Nombre</div>
					</div></th>
					<th width="40%">
					<div class="input-group col-xs-12">
						<span class="input-group-addon input-sm" <?php echo $popover2.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
						<div class="form-control input-sm" disabled>Pais</div>
					</div></th>
					<th width="10%"></th>
				</tr>
			</thead>
			<tbody>
		<?php for ($i=0; $i < count($ciudades) ; $i++) { $ciudad=$ciudades[$i]; $p=$this->M_persona->get_row('idci',$ciudad->idci);?>
			<tr>
				<td><?php echo $i+1;?></td>
				<td><input type="text" class="form-control input-sm" id="c<?php echo $ciudad->idci;?>" placeholder='Nombre de ciudad' value="<?php echo $ciudad->ciudad;?>" maxlength="100"></td>
				<td>
					<select id="cp<?php echo $ciudad->idci;?>" class="form-control input-sm">
						<option value="">Seleccionar...</option>
					<?php for ($j=0; $j < count($paises) ; $j++) { $pais=$paises[$j]; ?>
						<option value="<?php echo $pais->idpa;?>" <?php if($pais->idpa==$ciudad->idpa){ echo "selected";}?>><?php echo $pais->nombre;?></option>
					<?php }?>
					</select>
				</td>
				<?php $str="";
					if(count($p)>0){
						$str="<hr><strong class='text-danger'>¡Imposible Eliminar el la ciudad, esta siendo usada por ".count($p)." persona(s)!</strong>";
						$fun="disabled";
					}else{
						$fun="alerta_ciudad('".$ciudad->idci."')";
					}
					$help='title="<h4>Detalles de ciudad<h4>" data-content="<b>Nombre: </b>'.$ciudad->ciudad.'<br>'.$str.'"';
				?>
				<td><?php $this->load->view('estructura/botones/botones_registros',['detalle'=>$popover3.$help,'guardar'=>"update_ciudad('".$ciudad->idci."')",'eliminar'=>$fun]);?></td>
			</tr>
		<?php } ?>
			</tbody>
			<thead>
				<tr><th colspan="3" class="text-center">NUEVO</th></tr>
				<tr>
					<td colspan="2"><form onsubmit="return save_ciudad()">
						<div class="input-group col-xs-12">
							<input type="text" id="c" class="form-control input-sm" placeholder='Nombre de ciudad'>
							<span class="input-group-addon input-sm" <?php echo $popover3.$help2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
						</div>
					</form></td>
					<td>
					<div class="input-group col-xs-12">
						<select id="cp" class="form-control input-sm">
							<option value="">Seleccionar...</option>
						<?php for ($i=0; $i < count($paises) ; $i++) { $pais=$paises[$i]; ?>
							<option value="<?php echo $pais->idpa;?>"><?php echo $pais->nombre;?></option>
						<?php }?>
						</select>
						<span class="input-group-addon input-sm" <?php echo $popover3.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					</div>
					</td>
					<td><?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"save_ciudad()",'detalle'=>"",'eliminar'=>""]);?></td>
				</tr>
			</thead>
		</table>
	</div>
	<h3>TIPO DE CONTRATO</h3>
	<div class="table-responsive">
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
					<th width="5%">#</th>
					<th width="25%">
						<div class="input-group col-xs-12">
							<span class="input-group-addon input-sm" <?php echo $popover3.$help6;?>><i class='glyphicon glyphicon-info-sign'></i></span>
							<div class="form-control input-sm" disabled>Tipo</div>
						</div>
					</th>
					<th width="15%">
						<div class="input-group col-xs-12">
							<span class="input-group-addon input-sm" <?php echo $popover3.$help7;?>><i class='glyphicon glyphicon-info-sign'></i></span>
							<div class="form-control input-sm" disabled>Hrs</div>
						</div>
					</th>
					<th width="35%">
						<div class="input-group col-xs-12">
							<span class="input-group-addon input-sm" <?php echo $popover3.$help8;?>><i class='glyphicon glyphicon-info-sign'></i></span>
							<div class="form-control input-sm" disabled>Descripción</div>
						</div>
					</th>
					<th width="10%">
						<div class="input-group col-xs-12">
							<span class="input-group-addon input-sm" <?php echo $popover3.$help9;?>><i class='glyphicon glyphicon-info-sign'></i></span>
							<div class="form-control input-sm" disabled></div>
						</div>
					</th>
					<th width="10%"></th>
				</tr>
			</thead>
			<tbody>
		<?php 
		for ($i=0; $i < count($tipo_contratos) ; $i++) { $tipo_contrato=$tipo_contratos[$i]; $control=$this->M_empleado->get_row('idtc',$tipo_contrato->idtc);?>
			<tr>
				<td><?php echo $i+1;?></td>
				<td>
					<select class="form-control input-sm" id="t_tip<?php echo $tipo_contrato->idtc;?>">
						<option value="">Seleccionar...</option>
						<?php foreach ($v as $clave => $tipo) { ?>
						<option value="<?php echo $clave;?>" <?php if($clave==$tipo_contrato->tipo){ echo "selected";}?>><?php echo $tipo;?></option>
						<?php }?>
					</select>
				</td>
				<td> <form onsubmit="return update_tipo('<?php echo $tipo_contrato->idtc;?>')">
					<input type="number" class="form-control input-sm" id="t_hor<?php echo $tipo_contrato->idtc;?>" placeholder='Horas por dia' value="<?php echo $tipo_contrato->horas;?>" min='0' max='99'>
				</form></td>
				<td><textarea class="form-control input-sm" id="t_des<?php echo $tipo_contrato->idtc;?>"  placeholder="Descripción del jornal de trabajo" maxlength="150"><?php echo $tipo_contrato->descripcion;?></textarea></td>
				<td>
			<?php if($tipo_contrato->principal==1){?>
					<div class="btn-circle btn-circle-success btn-circle-30" onclick="select_principal('<?php echo $tipo_contrato->idtc;?>')"></div>
			<?php }else{?>
					<div class="btn-circle btn-circle-default btn-circle-30" onclick="select_principal('<?php echo $tipo_contrato->idtc;?>')"></div>
			<?php } ?>
				</td>
				<?php $str="";
					if(count($control)>0){
						$str="<strong class='text-danger'>¡Imposible Eliminar el tipo de contrato, pues esta siendo usada por ".count($control)." empleado(s)!</strong>";
						$fun="disabled";
					}else{
						$fun="alerta_tipo('".$tipo_contrato->idtc."','".$v[$tipo_contrato->tipo*1]."','".$tipo_contrato->horas."')";
					}
					$help='title="<h4>Detalles de tipo de contrato<h4>" data-content="<b>Tipo: </b>'.$v[$tipo_contrato->tipo*1].'<br><b>Carga horaria: </b>'.$tipo_contrato->horas.'h.<br><b>Descripción: </b>'.$tipo_contrato->descripcion.'<br>'.$str.'"';
				?>
				<td><?php $this->load->view('estructura/botones/botones_registros',['detalle'=>$popover3.$help,'guardar'=>"update_tipo('".$tipo_contrato->idtc."')",'eliminar'=>$fun]);?></td>
			</tr>
		<?php } ?>
			</tbody>
			<thead>
				<tr>
					<th colspan="6" class="text-center">NUEVO</th>
				</tr>
				<tr>
					<td colspan="2">
						<div class="input-group col-xs-12">
							<select class="form-control input-sm" id="t_tip">
								<option value="">Seleccionar...</option>
								<?php foreach ($v as $clave => $tipo) { ?>
								<option value="<?php echo $clave;?>"><?php echo $tipo;?></option>
								<?php }?>
							</select>
							<span class="input-group-addon input-sm" <?php echo $popover3.$help6;?>><i class='glyphicon glyphicon-info-sign'></i></span>
						</div>
					</td>
					<td><form onsubmit="return save_tipo()">
						<div class="input-group col-xs-12">
							<input type="number" class="form-control input-sm" id="t_hor" placeholder='Horas por dia' value="" min='0' max='99'>
							<span class="input-group-addon input-sm" <?php echo $popover3.$help7;?>><i class='glyphicon glyphicon-info-sign'></i></span>
						</div>
					</form></td>
					<td colspan="2">
						<div class="input-group col-xs-12">
							<textarea class="form-control input-sm" id="t_des"  placeholder="Descripción del jornal de trabajo" maxlength="150"></textarea>
							<span class="input-group-addon input-sm" <?php echo $popover3.$help8;?>><i class='glyphicon glyphicon-info-sign'></i></span>
						</div>
					</td>
					<td><?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"save_tipo()",'detalle'=>"",'eliminar'=>""]);?></td>
				</tr>
			</thead>
		</table>
	</div>
	<h3>CONFIGURACIÓN DE SISTEMA</h3>
	<div class="table-responsive">
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
					<th width="5%">#</th>
					<th width="55%">
						Detalle
					</th>
					<th width="35%">
						Valor
					</th>
					<th width="5%">Mod.</th>
				</tr>
			</thead>
			<tbody>
		<?php for ($i=0; $i < count($configuraciones) ; $i++) { $configuracion=$configuraciones[$i];?>
			<tr>
				<td><?php echo $i+1;?></td>
				<td><?php echo $configuracion->detalle?></td>
				<td>
					<div class="input-group col-xs-12">
						<input type="text" class="form-control input-sm" id="configuracion<?php echo $configuracion->idconf;?>" value="<?php echo $configuracion->valor;?>" maxlength="20" data-type="number">
						<span class="input-group-addon input-sm">
							<?php if($configuracion->clave=="sueldo_hora_sabados"){echo "Horas";}?>
							<?php if($configuracion->clave=="sueldo_dias_habiles"){echo "Días";}?>
						</span>
					</div>
				</td>
				<td><?php $this->load->view('estructura/botones/botones_registros',['detalle'=>'','guardar'=>"update_configuracion('".$configuracion->idconf."')",'eliminar'=>'']);?></td>
			</tr>
		<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<script language='javascript'>$('[data-toggle="popover"]').popover({html:true});</script>