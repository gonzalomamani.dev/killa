<?php
    $cargo=$this->session->userdata("cargo");
    if($this->session->userdata("nombre2")!=""){
		$usuario=$this->session->userdata("nombre")." ".$this->session->userdata("nombre2")." ".$this->session->userdata("paterno");
	}else{
		$usuario=$this->session->userdata("nombre")." ".$this->session->userdata("paterno");
	}
    $ci=$this->session->userdata("ci");
?>						
<!DOCTYPE html>
<html lang="es" class="no-js">
	<head><?php $this->load->view('estructura/head',['title'=>'Comprobantes','css'=>'']);?></head>
	<body>
		<?php $this->load->view('estructura/modal');?>
		<div class="contenedor">
			<?php $this->load->view('estructura/menu_izq',['usuario'=>$usuario,'cargo'=>$cargo,'ventana'=>'contabilidad','privilegio'=>$privilegio[0]]);?>
			<?php $v_menu="";
				if($privilegio[0]->co1r==1){ $v_menu.="Comprobantes/comprobante/icon-documents|"; }
				if($privilegio[0]->co2r==1){ $v_menu.="Libro mayor/libro_mayor/glyphicon glyphicon-book|";}
				if($privilegio[0]->co3r==1){ $v_menu.="Sumas y saldos/sumas_saldos/icon-layout|";}
				if($privilegio[0]->co4r==1){ $v_menu.="Estado de costo P. y V./costo_pv/icon-clipboard2|";}
				if($privilegio[0]->co5r==1){ $v_menu.="Estado de R./estado/icon-book|";}
				if($privilegio[0]->co6r==1){ $v_menu.="Balance G./balance/icon-paste|";}
				if($privilegio[0]->co7r==1){ $v_menu.="Plan de cuentas/plan/icon-file-text2|";}
				if($privilegio[0]->co8r==1){ $v_menu.="Configuración/config/glyphicon glyphicon-cog";}
			?>
			<?php $this->load->view('estructura/menu_top',['usuario'=>$usuario,'cargo'=>$cargo,'menu'=>$v_menu]);?>
			<div id="search"></div>
			<div id="contenido"></div>
		</div>
		<?php $this->load->view('estructura/js',['js'=>'contabilidad/contabilidad.js']);?>
	</body>
	<?php $nro=NULL;
	switch($pestania){
		case '1': $title="Comprobantes"; $activo='comprobante'; $search="contabilidad/search_comprobante"; $view="contabilidad/view_comprobante"; break;
		case '2': $title="Libro Mayor"; $activo='libro_mayor'; $search="contabilidad/search_mayor"; $view="contabilidad/view_mayor"; break;
		case '3': $title="Sumas y saldos"; $activo='sumas_saldos'; $search="contabilidad/search_sumas"; $view="contabilidad/view_sumas"; break;
		case '4': $title="Estado de costos de producción de lo vendido"; $activo='costo_pv'; $search="contabilidad/search_costo"; $view="contabilidad/view_estado"; $nro="0";break;
		case '5': $title="Estado de resultados"; $activo='estado'; $search="contabilidad/search_estado"; $view="contabilidad/view_estado"; $nro="1";break;
		case '6': $title="Balance general"; $activo='balance'; $search="contabilidad/search_balance"; $view="contabilidad/view_estado"; $nro="2";break;
		case '7': $title="Plan de cuentas"; $activo='plan'; $search="contabilidad/search_plan"; $view="contabilidad/view_plan"; break;
		case '10': $title="Configuración contabilidad"; $activo='config'; $search=""; $view="contabilidad/view_config"; break;
		default: $title="Error!";$activo='';$search="NULL";$view="NULL"; break;
	} ?>
	<script type="text/javascript">var atrib=new FormData(); atrib.append('nro','<?php echo $nro?>'); activar('<?php echo $activo;?>','<?php echo $title;?>','contabilidad?p=<?php echo $pestania;?>'); get_2n('<?php echo $search; ?>',{},'search',false,'<?php echo $view; ?>',atrib,'contenido',true); </script>
</html> 