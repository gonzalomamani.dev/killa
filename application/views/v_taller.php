<?php
?>						
<!DOCTYPE html>
<html lang="es" class="no-js">
	<head><?php $this->load->view('estructura/head',['title'=>'Taller de producción','css'=>'taller.css']);?></head>
	<body>
		<?php $this->load->view('estructura/modal');?>
		<div class="contenedor">
			<?php $this->load->view('estructura/menu_top',['usuario'=>'nada_taller','cargo'=>'nada_taller','menu'=>""]);?>
			<div id="search"></div>
			<div id="contenido"></div>
		</div><!-- /contenedor -->
		<script type="text/javascript" src="<?php echo base_url(); ?>libraries/js/jquery.min.js"></script>
		<!--<script type="text/javascript" src="<?php echo base_url(); ?>libraries/js/killa.js"></script>-->
		<script type="text/javascript" src="<?php echo base_url(); ?>libraries/js/taller/empleados.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>libraries/js/bootstrap.min.js"></script>
		<script type="text/javascript">init('<?php echo base_url();?>');</script>
	</body>
</html>