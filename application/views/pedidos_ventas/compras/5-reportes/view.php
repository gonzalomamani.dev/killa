<?php $compra=$compra[0];
	$contPagina=1;
?>
<div class="table-responsive" id="area">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="88%"><div class='encabezado_titulo'>DETALLE DE COMPRA</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Página: </b><?php echo $contPagina.' de 1'?></div>
				</td></tr>
			</table>
		</div>
	<table border="0" class="tabla tabla-border-true">
		<tr class="fila">
			<th class="celda td" style="font-size:.9em;">FECHA DE VENTA:</th>
			<td class="celda td" colspan="3" style="font-size:.9em;"><?php echo $compra->fecha;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th" colspan="4">DATOS DE CLIENTE</th>
		</tr>
		<tr class="fila">
			<th class="celda th" width="20%">Nombre/Razon Social:</th><td class="celda td" width="30%"><?php echo $compra->nombre;?></td>
			<th class="celda th" width="17%">NIT/CI:</th><td class="celda td" width="33%"><?php echo $compra->nit;?></td>
		</tr>
		<?php $ciudad=$this->M_ciudad->get_pais($compra->idci);?>
		<tr class="fila">
			<th class="celda th">Pais: </th><td class="celda td"><?php echo $ciudad[0]->pais;?></td>
			<th class="celda th">Ciudad: </th><td class="celda td"><?php echo $ciudad[0]->ciudad;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Telf/Cel: </th><td class="celda td"><?php echo $compra->telefono;?></td>
			<th class="celda th">Email: </th><td class="celda td"><?php echo $compra->email;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Dirección: </th><td class="celda td"><?php echo $compra->direccion;?></td>
			<th class="celda th">Sitio Web: </th><td class="celda td"><?php echo $compra->url;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Encargado: </th><td class="celda td"><?php echo $compra->encargado;?></td>
			<th class="celda th">Observaciónes: </th><td class="celda td"><?php echo $compra->caracteristicas;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th" colspan="4">DATOS DE COMPRA O EGRESO</th>
		</tr>
		<tr class="fila">
			<th class="celda th">Nº Orden: </th><td class="celda td"><?php echo $compra->orden_gasto;?></td>
			<?php $nom="NULL"; $empleado=$this->M_empleado->get_complet($compra->ide); if(!empty($empleado)){ $nom=$empleado[0]->nombre." ".$empleado[0]->nombre2." ".$empleado[0]->paterno." ".$empleado[0]->materno;}?>
			<th class="celda th">Nombre del encargado de registro de movimiento: </th><td class="celda td"><?php echo $nom;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Importe de compra o egreso(Bs.): </th><td class="celda td"><?php echo number_format($compra->importe,2,'.',',');?></td>
			<th class="celda th">Descuento (Bs.): </th><td class="celda td"><?php echo number_format($compra->descuento,2,'.',',');?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Neto de compra o egreso(Bs.): </th><td class="celda td"><?php echo number_format($compra->neto,2,'.',',');?></td>
			<th class="celda th">Nº de Doc./Fac.: </th><td class="celda td"><?php echo $compra->numero_factura;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Material/Producto adquirido: </th><td class="celda td"><?php echo $compra->nombre_material;?></td>
			<th class="celda th">Cantidad adquirida: </th><td class="celda td"><?php echo $compra->cantidad;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Observaciónes: </th><td class="celda td" colspan="3"><?php echo $compra->observaciones;?></td>
		</tr>
	</table>
</div>