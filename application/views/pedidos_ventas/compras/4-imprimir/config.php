<div class="row-border table-responsive">
	<table class="table table-bordered"><thead>
		<tr>
			<th width="15%">Nro. Reg. por hojas</th>
			<th width="20%">
				<select id="nro" class="form-control input-sm">
				<?php for ($i=1; $i <= 50 ; $i++) { ?>
					<option value="<?php echo $i;?>" <?php if($i==34){echo "selected";}?>><?php echo $i;?></option>
				<?php } ?>
				</select>
			</th>
			<th width="65%"></th>
		</tr>
	</thead></table>
	<div id="config_area">
		<table class="tabla tabla-border-true">
			<tr class="fila">
				<th class="celda th" width="5%" >#</th>
				<th class="celda th" width="5%">Fecha</th>
				<th class="celda th" width="10%">Nº doc./fac.</th>
				<th class="celda th" width="10%" >NIT/CI de proveedor</th>
				<th class="celda th" width="15%">Nombre/Razon de proveedor</th>
				<th class="celda th" width="8%">Nº Orden</th>
				<th class="celda th" width="15%">Material y/o producto</th>
				<th class="celda th" width="5%">Cantidad</th>
				<th class="celda th" width="7%">Total (Bs.)</th>
				<th class="celda th" width="20%">Observaciónes</th>
			</tr>
			<tr class="fila">
			<tr class="fila">
				<th class="celda th"><input type="checkbox" id="1" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="2" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="3" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="4" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="5" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="6" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="7" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="8" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="9" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="10" checked="checked"></th>
			</tr>
			</tr>
		</table>
	</div>
</div>
<div class="row-border">
	<div id="area"></div>
</div>
<script language="javascript">
	$("#1").unbind("click");$("#1").change(function(){ arma_compras('<?php echo $compras;?>'); }); 
	$("#2").unbind("click");$("#2").change(function(){ arma_compras('<?php echo $compras;?>'); }); 
	$("#3").unbind("click");$("#3").change(function(){ arma_compras('<?php echo $compras;?>'); }); 
	$("#4").unbind("click");$("#4").change(function(){ arma_compras('<?php echo $compras;?>'); }); 
	$("#5").unbind("click");$("#5").change(function(){ arma_compras('<?php echo $compras;?>'); }); 
	$("#6").unbind("click");$("#6").change(function(){ arma_compras('<?php echo $compras;?>'); });
	$("#7").unbind("click");$("#7").change(function(){ arma_compras('<?php echo $compras;?>'); }); 
	$("#8").unbind("click");$("#8").change(function(){ arma_compras('<?php echo $compras;?>'); }); 
	$("#9").unbind("click");$("#9").change(function(){ arma_compras('<?php echo $compras;?>'); });
	$("#10").unbind("click");$("#10").change(function(){ arma_compras('<?php echo $compras;?>'); });
	$("#nro").unbind("click");$("#nro").change(function(){ arma_compras('<?php echo $compras;?>'); });
	Onfocus("nro");
</script>