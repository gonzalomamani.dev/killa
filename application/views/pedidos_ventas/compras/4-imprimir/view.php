<?php 
	$compras=json_decode($compras);
	$contPagina=1;
	$contReg=0;
?>
<div class="pagina table-responsive">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="88%"><div class='encabezado_titulo'>REGISTRO DE COMPRAS</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'></span></div>
				</td></tr>
			</table>
		</div>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php if(!isset($v1)){?><th class="celda th" width="5%" >#</th><?php }?>
			<?php if(!isset($v2)){?><th class="celda th" width="5%">Fecha</th><?php }?>
			<?php if(!isset($v3)){?><th class="celda th" width="10%">Nº doc./fac.</th><?php }?>
			<?php if(!isset($v4)){?><th class="celda th" width="10%" >NIT/CI de proveedor</th><?php }?>
			<?php if(!isset($v5)){?><th class="celda th" width="15%">Nombre/Razon de proveedor</th><?php }?>
			<?php if(!isset($v6)){?><th class="celda th" width="8%">Nº Orden</th><?php }?>
			<?php if(!isset($v7)){?><th class="celda th" width="15%">Material y/o producto</th><?php }?>
			<?php if(!isset($v8)){?><th class="celda th" width="5%">Cantidad</th><?php }?>
			<?php if(!isset($v9)){?><th class="celda th" width="7%">Total (Bs.)</th><?php }?>
			<?php if(!isset($v10)){?><th class="celda th" width="20%">Observaciónes</th><?php }?>
		</tr>
		<?php $cont=0; $sub_total=0; $total=0;
			foreach ($compras as $key => $compra){ ?>
			<?php if($contReg>=$nro){
					$contReg=0;
					$contPagina++;
			?>
		</table><!--cerramos tabla-->
		</div><!--cerramos pagina-->
<div class="pagina table-responsive">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="90%"><div class='encabezado_titulo'>REGISTRO DE COMPRAS</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'></span></div>
				</td></tr>
			</table>
		</div>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php if(!isset($v1)){?><th class="celda th" width="5%" >#</th><?php }?>
			<?php if(!isset($v2)){?><th class="celda th" width="5%">Fecha</th><?php }?>
			<?php if(!isset($v3)){?><th class="celda th" width="10%">Nº doc./fac.</th><?php }?>
			<?php if(!isset($v4)){?><th class="celda th" width="10%" >NIT/CI de proveedor</th><?php }?>
			<?php if(!isset($v5)){?><th class="celda th" width="15%">Nombre/Razon de proveedor</th><?php }?>
			<?php if(!isset($v6)){?><th class="celda th" width="8%">Nº Orden</th><?php }?>
			<?php if(!isset($v7)){?><th class="celda th" width="15%">Material y/o producto</th><?php }?>
			<?php if(!isset($v8)){?><th class="celda th" width="5%">Cantidad</th><?php }?>
			<?php if(!isset($v9)){?><th class="celda th" width="7%">Total (Bs.)</th><?php }?>
			<?php if(!isset($v10)){?><th class="celda th" width="20%">Observaciónes</th><?php }?>
		</tr>
		<?php }//end if 
		?>
		<tr class="fila">
			<?php if(!isset($v1)){?><td class="celda td"><?php echo $cont+1;$cont++;?></td><?php } ?>
			<?php if(!isset($v2)){?><td class="celda td"><?php echo $this->lib->format_date($compra->fecha,'Y/m/d');?></td><?php } ?>
			<?php if(!isset($v3)){?><td class="celda td"><?php echo $compra->numero_factura;?></td><?php } ?>
			<?php if(!isset($v4)){?><td class="celda td"><?php echo $compra->nit; ?></td><?php } ?>
			<?php if(!isset($v5)){?><td class="celda td"><?php echo $compra->nombre; ?></td><?php } ?>
			<?php if(!isset($v6)){?><td class="celda td"><?php echo $compra->orden_gasto;  ?></td><?php } ?>
			<?php if(!isset($v7)){?><td class="celda td"><?php echo $compra->nombre_material; ?></td><?php } ?>
			<?php if(!isset($v8)){?><td class="celda td"><?php echo $compra->cantidad; ?></td><?php } ?>
			<?php if(!isset($v9)){?><td class="celda td"><?php echo $compra->neto;?></td><?php } ?>
			<?php if(!isset($v10)){?><td class="celda td"><?php echo $compra->observaciones; ?></td><?php } ?>
		</tr>
	<?php $contReg++;}// end for ?>
	</table>
</div>
<script type="text/javascript"> $(".nroPagina").html(<?php echo $contPagina;?>); </script>