<table class="tabla tabla-border-false">
	<tr class="fila">
		<td class='celda celda-sm' style="width:3%"></td>
		<td class='celda celda-sm' style="width:10%">
			<form onsubmit="return view_compra();">
				<input type="number" id="sc_nit" placeholder='NIT/CI' class="form-control input-sm" onkeyup="reset_input(this.id)">
			</form>
		</td>
		<td class='celda celda-sm' style="width:15%">
			<select id="sc_pro" class="form-control input-sm" onchange="reset_input(this.id); view_compra();">
				<option value="">Seleccionar... Nombre o Razon Social</option>
		<?php for ($i=0; $i < count($proveedores); $i++) { $proveedor=$proveedores[$i]; ?>
				<option value="<?php echo $proveedor->idpro;?>"><?php echo $proveedor->nombre;?></option>
		<?php }//end for ?>
			</select>
		</td>
		<td class='celda celda-sm-15'>
		<form onsubmit="return view_compra();">
			<input type="date" class="form-control input-sm" id="sc_fe1" onchange="reset_input(this.id)">
		</form>
		</td>
		<td class='celda celda-sm-15'>
		<form onsubmit="return view_compra();">
			<input type="date" class="form-control input-sm" id="sc_fe2" onchange="reset_input(this.id)">
		</form>
		</td>
		<td class='celda celda-sm' width="15%"><input type="month" id="sc_am" placeholder='2000-12' class="form-control input-sm" onchange="reset_input(this.id);view_compra();"></td>
		<td class="celda celda-sm" style="width:14%;"></td>
		<td class="celda td text-right" width="13%">
			<?php $new="con_orden()"; if($privilegio[0]->mo3c!="1"){ $new="";}?>
			<?php $print="print()"; if($privilegio[0]->mo3p!="1"){ $print="";}?>
			<?php $this->load->view('estructura/botones/buscador',['f_buscar'=>"view_compra()",'f_ver'=>"all_compra()",'nuevo'=>'Nuevo','f_nuevo'=>$new,'f_imprimir'=>$print]);?>
		</td>
	</tr>
</table>