<?php 
	$help1='title="<h4>Número de orden<h4>" data-content="Representa al número de referencia del movimiento, este numero es unico y generado automaticamente por el sistema, si fuese el caso de que el numero fuera ya registrado puede <b>pulsar el boton actualizar el número.</b>"';
	$help2='title="<h4>Tipo de movimiento<h4>" data-content="Este tipo de movimiento es referencial, representa el tipo de movimiento del presente orden"';
	$help3='title="<h4>Fecha de movimiento<h4>" data-content="Ingrese la fecha de registro del movimiento."';
	$help4='title="<h4>Observaciónes del movimiento<h4>" data-content="Ingrese algunas observaciónes del movimiento si tuviera. El contenido de las observaciones debe ser en formato alfanumérico hasta 700 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help5='title="<h4>Tipo de material a ser adquirido<h4>" data-content="Representa al tipo de materiales que se adquirira, estos tipos se encuentran asignados en la configuración de proveedores."';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$popover2='data-toggle="popover" data-placement="top" data-trigger="hover"';

	$tipo_gasto=array(1=>'Gasto',2=>'Compra', 3=>'Pago');
	$categorias=array('Materiales','Materiales indirectos','Activo Fijo','Otro tipo de materiales y/o productos');
?>
<ul class="nav nav-tabs">
  <li role="presentation" class="active"><a href="javascript:" onclick="con_orden()">Con Orden</a></li>
  <li role="presentation"><a href="javascript:" onclick="sin_orden()">Sin Orden</a></li>
</ul>
<div class="row"><div class="col-sm-2 col-sm-offset-10 col-xs-12"><strong><span class='text-danger'>(*)</span>Campos obligatorio</strong></div></div>
		<div class="list-group">
		  <div class="list-group-item active">
		    <h5 class="list-group-item-heading">ORDEN DE <span class="g_text">GASTO</span></h5>
		  </div>
		  <div class="list-group-item">
		  	<div class="row">
		  		<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="input-group">
						<div class="form-control text-right" style="height:60px; font-size:30px;" id="nro_ord"><?php echo $orden;?></div>
						<span class="btn input-group-addon"  title="Actualizar número de pedido" onclick="refresh_orden_gasto();"><i class="glyphicon glyphicon-refresh"></i></span>
						<span class="input-group-addon" <?php echo $popover.$help1;?>><i class="glyphicon glyphicon-info-sign"></i></span>
					</div>
		  		</div>
		  		<div class="col-md-3 col-sm-6 col-xs-12">
		  			<h5 class="list-group-item-heading"><strong><span class='text-danger'>(*)</span>Tipo de movimiento: </strong></h5>
		  			<div class="input-group col-xs-12">
						<select id="tg" class="form-control input-sm" onchange="orden()">
						<?php for ($i=1; $i<=count($tipo_gasto) ; $i++) { ?>
							<option value="<?php echo $i;?>"><?php echo $tipo_gasto[$i];?></option>
						<?php }//en for ?>
						</select>
						<span class="input-group-btn" <?php echo $popover.$help2;?>><div class="btn btn-default btn-sm" type="button"><span class="glyphicon glyphicon-info-sign"></span></div></span>
				   	</div>
		  		</div><i class='clearfix visible-sm-block'></i>
		  		<div class="col-md-3 col-sm-6 col-xs-12">
		  			<h5 class="list-group-item-heading"><strong><span class='text-danger'>(*)</span>Fecha de <span id="g_text2">gasto</span>: </strong></h5>
		  			<div class="input-group">
						<input type="date" class="form-control input-sm" value="<?php echo date('Y-m-d');?>" id="fecha">
						<span class="input-group-addon" <?php echo $popover.$help3;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
		  		</div>
		  		<div class="col-md-3 col-sm-6 col-xs-12">
		  			<h5 class="list-group-item-heading"><strong>Observaciónes: </strong></h5>
			      	<div class="input-group">
			      		<textarea class="form-control input-sm" id="obs" placeholder="Observaciónes en la compra, gasto o pago"></textarea>
			      		<span class="input-group-addon" <?php echo $popover.$help4;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
		  		</div>
		  	</div>
		  </div>
		</div>
	<div class="list-group">
		  <div class="list-group-item active">
		    <h5 class="list-group-item-heading" id="g_text">DETALLE DE ELEMENTOS EN EL <span class='g_text'>GASTO</span></h5>
		  </div>
		<div class="list-group-item">
		  	<div class="row">
		  		<div class="col-xs-12">
					<div class="table-responsive">
						<table id="table-np" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="4%">#</th>
									<th width="12%">NIT/CI</th>
									<th width="20%">Proveedor</th>
									<th width="23%">Material o producto</th>
									<th width="6%">C/U (Bs.)</th>
									<th width="10%">Cantidad</th>
									<th width="10%">Importe<br>s/sistema(Bs.)</th>
									<th width="10%">Importe(Bs.)</th>
									<th width="5%"></th>
								</tr>
							</thead>
							<tbody id="content_protuct"></tbody>
							<thead>
								<tr>
									<th colspan="6" class="text-right">TOTAL(Bs.):</th>
									<th id="tot_sis" class="text-right">0</th>
									<th id="tot" class="text-right">0</th>
									<th></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				<div class="col-md-3 col-md-offset-7 col-sm-4 col-sm-offset-5 col-xs-12">
					<div class="input-group">
						<select id="cat" class="form-control input-md" onchange="">
				<?php for ($i=1; $i<=count($categorias) ; $i++) { ?>
							<option value="<?php echo $i;?>"><?php echo $categorias[$i-1];?></option>
				<?php }//en for ?>
						</select>
					  	<span class="input-group-btn" <?php echo $popover.$help5;?>><div class="btn btn-default btn-md" type="button"><span class="glyphicon glyphicon-info-sign"></span></div></span>
					</div>
				</div>
				<div class="col-md-2 col-sm-3 col-xs-12">
					<button class="btn btn-info col-xs-12" onclick="add_row_con_orden($(this));"><span class='glyphicon glyphicon-plus'></span> Adicionar material</button>
				</div>
			</div>
		</div>
	</div>
<script language='javascript'>Onfocus("cli");$('[data-toggle="popover"]').popover({html:true});</script>