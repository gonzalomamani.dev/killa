<?php
	$help1='title="<h4>Seleccione el proveedor<h4>" data-content="Seleccione el proveedor de la compra o gasto, si desea adicionar un nuevo proveedor puede hacerlo en la sección cliente proveedor o dar click en el boton <b>+</b>"';
	switch ($cat){
		case '1': $help2='title="<h4>Material para almacen<h4>" data-content="Seleccione el material a ser adquirido, este material es principalmente materiales que se encuentran en almacen, si no aparece puede no haber seleccionado el proveedor o el proveedor no tiene asignado ningun material,  si desea adicionar un nuevo material puede hacerlo en la sección cliente-proveedor o dar click en el boton <b>+</b>"'; break;
		case '2': $help2='title="<h4>Materiales indirectos<h4>" data-content="Seleccione el material indirecto a ser adquirido, este material es principalmente materiales que son usados como materiales indirectos en el producto, si no aparece materiales puede no haber seleccionado el proveedor o el proveedor no tiene asignado ningun material,  si desea adicionar un nuevo material puede hacerlo en la sección cliente-proveedor o dar click en el boton <b>+</b>"'; break;
		case '3': $help2='title="<h4>Activo Fijo<h4>" data-content="Seleccione el activo fijo a ser adquirido, si el activo fijo no aparece puede no haber seleccionado el proveedor o el proveedor no tiene asignado ningun material,  si desea adicionar un nuevo activo fijo puede hacerlo en la sección cliente-proveedor o dar click en el boton <b>+</b>"'; break;
		case '4': $help2='title="<h4>Otro tipo de materiales y/o productos<h4>" data-content="Seleccione el material a ser adquirido, este material es principalmente materiales varios que no se encuentran en las opciones anteriores, si el material no aparece puede no haber seleccionado el proveedor o el proveedor no tiene asignado ningun material,  si desea adicionar un nuevo material puede hacerlo en la sección cliente-proveedor o dar click en el boton <b>+</b>."'; break;
	}
	$help3='title="<h4>Costo por unidad<h4>" data-content="Representa al costo por unidad, asignado en al proveedor"';
	$help4='title="<h4>Importe sistema<h4>" data-content="Representa al costo total segun el <b>costo unitario</b> y la <b>cantidad</b> a ser adquirida."';
	$help5='title="<h4>Importe<h4>" data-content="Representa al costo total a ser tomado como costo final del gasto, inicialmente es el costo sugerio por el sistema, este costo puede ser modificado."';
	$popover='data-toggle="popover" data-placement="top" data-trigger="hover"';
?>
<tr>
	<td id="auto-num"></td>
	<td>
		<input type="hidden" id="pos<?php echo $row;?>" value="<?php echo $row;?>">
		<div class="input-group input-140">
			<select id="nit<?php echo $row;?>" class="form-control input-sm" onchange="selected_proveedor('<?php echo $row;?>','<?php echo $cat?>');">
				<option value="">Seleccionar...</option>
		<?php for ($i=0; $i < count($materiales_proveedores) ; $i++) { ?>
				<option value="<?php echo $materiales_proveedores[$i]->idpro;?>"><?php echo $materiales_proveedores[$i]->nit;?></option>
		<?php }?>
			</select>
		</div>
	</td>
	<td>
		<input type="hidden" id="cat<?php echo $row;?>" value="<?php echo $cat;?>">
		<div class="input-group input-220">
		  	<select id="prov<?php echo $row;?>" class="form-control input-sm" onchange="selected_nit('<?php echo $row;?>','<?php echo $cat?>');">
				<option value="">Seleccionar...</option>
	<?php for ($i=0; $i < count($materiales_proveedores) ; $i++) { ?>
			<option value="<?php echo $materiales_proveedores[$i]->idpro;?>"><?php echo $materiales_proveedores[$i]->nombre_proveedor;?></option>
	<?php }?>
			</select>
			<a href="<?php echo base_url();?>cliente_proveedor?p=2" target="_blank" title="Ver proveedores" class="input-group-addon input-sm"><i class="glyphicon glyphicon-plus"></i></a>
		  	<span class="input-group-addon input-sm" <?php echo $popover.$help1;?>><i class="glyphicon glyphicon-info-sign"></i></span>
		</div>
	</td>
	<td>
		<div class="input-group input-220">
		  	<select id="mat<?php echo $row;?>" class="form-control input-sm" onchange="monto_material('<?php echo $row;?>','<?php echo $cat?>');">
				<option value=" ">Seleccionar...</option>
			</select>
		  	<a href="<?php echo base_url();?>cliente_proveedor?p=2" target="_blank" title="Ver proveedores" class="input-group-addon input-sm"><i class="glyphicon glyphicon-plus"></i></a>
		  	<span class="input-group-addon input-sm" <?php echo $popover.$help2;?>><i class="glyphicon glyphicon-info-sign"></i></span>
		</div>
	</td>
	<td>
		<div class="input-group input-120">
			<input type="number" class="form-control input-sm input-120" id="cu<?php echo $row;?>" disabled>
			<span class="input-group-addon input-sm" <?php echo $popover.$help3;?>><i class="glyphicon glyphicon-info-sign"></i></span>
		</div>
	</td>
	<td>
		<div class="input-group input-120">
			<input type="number" class="form-control input-sm" id="can<?php echo $row;?>" <?php if($cat==1 || $cat==3){ ?> onkeyup="calcula_monto_material('<?php echo $row;?>','<?php echo $cat?>');" onclick="calcula_monto_material('<?php echo $row;?>','<?php echo $cat?>');"<?php }?>>
		  	<span class="input-group-addon input-sm" id="med<?php echo $row;?>">...</span>
		</div>
	</td>
	<td>
		<div class="input-group input-120">
			<input type="number" class="form-control input-sm input-120" id="i_sis<?php echo $row;?>"  disabled>
			<span class="input-group-addon input-sm" <?php echo $popover.$help4;?>><i class="glyphicon glyphicon-info-sign"></i></span>
		</div>
	</td>
	<td>
		<div class="input-group input-120">
			<input type="number" id="imp<?php echo $row;?>" class="form-control input-sm input-120" onkeyup="refresh_totales_compras()" onclick="refresh_totales_compras();">
			<span class="input-group-addon input-sm" <?php echo $popover.$help5;?>><i class="glyphicon glyphicon-info-sign"></i></span>
		</div>
	</td>
	<td>
		<?php $this->load->view('estructura/botones/botones_registros',['eliminar'=>"drop_fila($(this));"]);?>
	</td>
</tr>
<script language='javascript'>Onfocus("nit<?php echo $row;?>");$('[data-toggle="popover"]').popover({html:true});</script>