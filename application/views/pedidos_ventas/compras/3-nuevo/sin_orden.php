<?php 
	$help5='title="<h4>Tipo de material a ser registrados<h4>" data-content="Representa al tipo de materiales adquirido, estos tipos se encuentran asignados en la configuración de proveedores."';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$categorias=array('Materiales','Materiales indirectos','Activo Fijo','Otro tipo de materiales y/o productos');
?>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="con_orden()">Con Orden</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="sin_orden()">Sin Orden</a></li>
</ul>
<div class="list-group">
		  <div class="list-group-item active">
		    <h5 class="list-group-item-heading" id="g_text">DETALLE DE ELEMENTOS EN EL <span class='g_text'>GASTO</span></h5>
		  </div>
		<div class="list-group-item">
		  	<div class="row">
		  		<div class="col-xs-12">
					<div class="table-responsive">
						<table id="table-np" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="4%">#</th>
									<th width="10%">Fecha</th>
									<th width="12%">NIT/CI</th>
									<th width="20%">Proveedor</th>
									<th width="23%">Material o producto</th>
									<th width="10%">Nº de<br>Documento</th>
									<th width="6%">C/U (Bs.)</th>
									<th width="10%">Cantidad</th>
									<th width="10%">Importe<br>s/sistema(Bs.)</th>
									<th width="10%">Importe(Bs.)</th>
									<th width="5%"></th>
								</tr>
							</thead>
							<tbody id="content_protuct"></tbody>
							<thead>
								<tr>
									<th colspan="8" class="text-right">TOTAL(Bs.):</th>
									<th id="tot_sis" class="text-right">0</th>
									<th id="tot" class="text-right">0</th>
									<th></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				<div class="col-md-3 col-md-offset-7 col-sm-4 col-sm-offset-5 col-xs-12">
					<div class="input-group">
						<select id="cat" class="form-control input-md" onchange="">
				<?php for ($i=1; $i<=count($categorias) ; $i++) { ?>
							<option value="<?php echo $i;?>"><?php echo $categorias[$i-1];?></option>
				<?php }//en for ?>
						</select>
					  	<span class="input-group-btn" <?php echo $popover.$help5;?>><div class="btn btn-default btn-md" type="button"><span class="glyphicon glyphicon-info-sign"></span></div></span>
					</div>
				</div>
				<div class="col-md-2 col-sm-3 col-xs-12">
					<button class="btn btn-info col-xs-12" onclick="add_row_sin_orden($(this));"><span class='glyphicon glyphicon-plus'></span> Adicionar fila</button>
				</div>
			</div>
		</div>
	</div>
<script language='javascript'>Onfocus("cli");$('[data-toggle="popover"]').popover({html:true});</script>