<?php
	$tot=0;
	$ice=0;
	$exc=0;
	$net=0;
	$desc=0;
	$j_compras=json_encode($gastos);
?>
<div class="table-responsive">
	<table class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th width="3%">#</th>
				<th width="10%" class="celda-sm">NIT/CI<br>proveedor</th>
				<th width="15%">Razon Social<br>del proveedor</th>
				<th width="15%">Fecha de compra</th>
				<th width="5%">Nº Orden</th>
				<th width="15%">Producto</th>
				<th width="7%">Total<br>(Bs.)</th>
				<th width="7%">descuento<br>(Bs.)</th>
				<th width="7%">Neto<br>(Bs.)</th>
				<th width="6%" class="celda-sm"></th>
				<th width="10%"></th>
			</tr>
		</thead>
		<tbody>
<?php if(count($gastos)>0){
		for ($i=0; $i < count($gastos) ; $i++) { $gasto=$gastos[$i];?>
		<tr>
			<td><?php echo $i+1;?></td>
			<td class="celda-sm"><?php echo $gasto->nit;?></td>
			<td><?php echo $gasto->nombre;?></td>
			<td><?php echo $gasto->fecha?></td>
			<td><?php echo $gasto->orden_gasto;?></td>
			<td><?php echo $gasto->nombre_material;?></td>
			<td><?php echo number_format($gasto->importe,2,'.',',');?></td>
			<td><?php echo number_format($gasto->descuento,2,'.',',');?></td>
			<td><?php echo number_format($gasto->neto,2,'.',',');?></td>
			<td class="celda-sm">
				<select id="" class="form-control input-sm" onchange="/*actualiza_cuenta(<?php echo $detalle_gasto->iddga;?>,this.value)*/">
					<option value="">Seleccionar...</option>
			<?php for ($c=0; $c < count($cuentas) ; $c++){ $cuenta=$cuentas[$c]; ?>
					<option value="<?php echo $cuenta->idpl;?>" <?php if($gasto->idpl==$cuenta->idpl){ echo "selected";}?>><?php echo $cuenta->cuenta_text;?></option>
			<?php }// end for ?>
				</select>
			</td>
		<?php if($gasto->orden_gasto!="" || $gasto->orden_gasto!=NULL){ $fun="config_con_orden('".$gasto->idga."')";}else{ $fun="config_sin_orden('".$gasto->idga."','".$gasto->iddga."')";}?>
		<?php if($privilegio[0]->mo3u!="1"){ $fun="";}?>
		<?php $eli="confirmar_compra('".$gasto->iddga."')"; if($privilegio[0]->mo3d!="1"){ $eli="";}?>
			<td class="text-right"><?php $this->load->view('estructura/botones/botones_registros',['reportes'=>"reportes_compra('".$gasto->idga."')",'configuracion'=>$fun,'eliminar'=>$eli]);?></td>
		</tr>	
<?php	}//end for
	}else{// en if

	}
?>
		</tbody>
	</table>
</div>
<script language="javascript" type="text/javascript"> 
	$("#print").removeAttr("onclick"); 
	$("#print").unbind("click"); 
	$("#print").click(function(){ imprimir_compras('<?php echo $j_compras;?>'); }); 
</script>