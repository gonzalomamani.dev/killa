<?php 

	$help0='title="<h4>Seleccione la fecha de movimiento<h4>" data-content="Seleccione la fecha de compra o gasto"';
	$help1='title="<h4>Seleccione el proveedor<h4>" data-content="Seleccione el proveedor de la compra o gasto, si desea adicionar un nuevo proveedor puede hacerlo en la sección cliente proveedor o dar click en el boton <b>+</b>"';
	switch ($compras[0]->categoria){
		case '1': $help2='title="<h4>Material para almacen<h4>" data-content="Seleccione el material adquirido, este material es principalmente materiales que se encuentran en almacen, si no aparece puede no haber seleccionado el proveedor o el proveedor no tiene asignado ningun material,  si desea adicionar un nuevo material puede hacerlo en la sección cliente-proveedor o dar click en el boton <b>+</b>"'; break;
		case '2': $help2='title="<h4>Materiales indirectos<h4>" data-content="Seleccione el material indirecto adquirido, este material es principalmente materiales que son usados como materiales indirectos en el producto, si no aparece materiales puede no haber seleccionado el proveedor o el proveedor no tiene asignado ningun material,  si desea adicionar un nuevo material puede hacerlo en la sección cliente-proveedor o dar click en el boton <b>+</b>"'; break;
		case '3': $help2='title="<h4>Activo Fijo<h4>" data-content="Seleccione el activo fijo adquirido, si el activo fijo no aparece puede no haber seleccionado el proveedor o el proveedor no tiene asignado ningun material,  si desea adicionar un nuevo activo fijo puede hacerlo en la sección cliente-proveedor o dar click en el boton <b>+</b>"'; break;
		case '4': $help2='title="<h4>Otro tipo de materiales y/o productos<h4>" data-content="Seleccione el material adquirido, este material es principalmente materiales varios que no se encuentran en las opciones anteriores, si el material no aparece puede no haber seleccionado el proveedor o el proveedor no tiene asignado ningun material,  si desea adicionar un nuevo material puede hacerlo en la sección cliente-proveedor o dar click en el boton <b>+</b>."'; break;
	}
	$help25='title="<h4>Nº de Documento<h4>" data-content="Representa al numero de factura o recibo si ubiera."';
	$help3='title="<h4>Costo por unidad<h4>" data-content="Representa al costo por unidad, asignado en al proveedor"';
	$help4='title="<h4>Importe sistema<h4>" data-content="Representa al costo total segun el <b>costo unitario</b> y la <b>cantidad</b> a ser adquirida."';
	$help5='title="<h4>Importe<h4>" data-content="Representa al costo total a ser tomado como costo final del gasto, inicialmente es el costo sugerio por el sistema, este costo puede ser modificado."';
	$popover='data-toggle="popover" data-placement="top" data-trigger="hover"';

	$categorias=array('Materiales','Materiales indirectos','Activo Fijo','Otro tipo de materiales y/o productos');
?>
<div class="list-group">
		  <div class="list-group-item active">
		    <h5 class="list-group-item-heading" id="g_text">DETALLE DE ELEMENTOS EN EL <span class='g_text'>GASTO</span></h5>
		  </div>
		<div class="list-group-item">
		  	<div class="row">
		  		<div class="col-xs-12">
					<div class="table-responsive">
						<table id="table-np" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="4%">#</th>
									<th width="10%">Fecha</th>
									<th width="12%">NIT/CI</th>
									<th width="20%">Proveedor</th>
									<th width="23%">Material o producto</th>
									<th width="10%">Nº de<br>Documento</th>
									<th width="6%">C/U (Bs.)</th>
									<th width="10%">Cantidad</th>
									<th width="10%">Importe<br>s/sistema(Bs.)</th>
									<th width="10%">Importe(Bs.)</th>
									<th width="5%"></th>
								</tr>
							</thead>
							<tbody id="content_protuct">
					<?php for ($c=0; $c < count($compras) ; $c++) { $compra=$compras[$c];
								//cargando nit proveedor
								if($compra->categoria==1){ $materiales_proveedores=$this->M_material->material_proveedor();}
								if($compra->categoria==2){ $materiales_proveedores=$this->M_material_adicional->material_proveedor();}
								if($compra->categoria==3){ $materiales_proveedores=$this->M_activo_fijo->material_proveedor();}
								if($compra->categoria==4){ $materiales_proveedores=$this->M_material_varios->material_proveedor();}
								//cargando materiales
								if($compra->categoria==1){ $materiales=$this->M_material_proveedor->get_material($compra->idpro,'material'); }
								if($compra->categoria==2){ $materiales=$this->M_material_proveedor->get_material($compra->idpro,'material_adicional'); }
								if($compra->categoria==3){ $materiales=$this->M_material_proveedor->get_material($compra->idpro,'activo_fijo'); }
								if($compra->categoria==4){ $materiales=$this->M_material_proveedor->get_material($compra->idpro,'material_vario'); }
						?>
								<tr>
									<td id="auto-num"></td>
									<td>
										<div class="input-group input-date">
											<input type="date" class="form-control input-sm input-120" id="fech<?php echo $c+1;?>" value="<?php echo $compra->fecha;?>">
											<span class="input-group-addon input-sm" <?php echo $popover.$help0;?>><i class="glyphicon glyphicon-info-sign"></i></span>
										</div>
									</td>
									<td>
										<input type="hidden" id="pos<?php echo $c+1;?>" value="<?php echo $c+1;?>">
										<div class="input-group input-140">
											<select id="nit<?php echo $c+1;?>" class="form-control input-sm" onchange="selected_proveedor('<?php echo $c+1;?>','<?php echo $compra->categoria?>');">
												<option value="">Seleccionar...</option>
										<?php for ($i=0; $i < count($materiales_proveedores) ; $i++) { ?>
												<option value="<?php echo $materiales_proveedores[$i]->idpro;?>" <?php if($compra->idpro==$materiales_proveedores[$i]->idpro){ echo "selected";}?>><?php echo $materiales_proveedores[$i]->nit;?></option>
										<?php }?>
											</select>
										</div>
									</td>
									<td>
										<input type="hidden" id="cat<?php echo $c+1;?>" value="<?php echo $compra->categoria;?>">
										<div class="input-group input-220">
										  	<select id="prov<?php echo $c+1;?>" class="form-control input-sm" onchange="selected_nit('<?php echo $c+1;?>','<?php echo $compra->categoria?>');">
												<option value="">Seleccionar...</option>
									<?php for ($i=0; $i < count($materiales_proveedores) ; $i++) { ?>
											<option value="<?php echo $materiales_proveedores[$i]->idpro;?>" <?php if($compra->idpro==$materiales_proveedores[$i]->idpro){ echo "selected";}?>><?php echo $materiales_proveedores[$i]->nombre_proveedor;?></option>
									<?php }?>
											</select>
											<a href="<?php echo base_url();?>cliente_proveedor?p=2" target="_blank" title="Ver proveedores" class="input-group-addon input-sm"><i class="glyphicon glyphicon-plus"></i></a>
										  	<span class="input-group-addon input-sm" <?php echo $popover.$help1;?>><i class="glyphicon glyphicon-info-sign"></i></span>
										</div>
									</td>
									<td>
										<div class="input-group input-220">
										  	<select id="mat<?php echo $c+1;?>" class="form-control input-sm" onchange="monto_material('<?php echo $c+1;?>','<?php echo $compra->categoria?>');">
												<option value=" ">Seleccionar...</option>
											<?php for ($j=0; $j < count($materiales) ; $j++) { $material=$materiales[$j]; ?>
												<option value="<?php echo $material->idmp;?>" <?php if($compra->idmp==$material->idmp){ echo "selected";}?>><?php echo $material->nombre;?></option>
											<?php }?>
											</select>
										  	<a href="<?php echo base_url();?>cliente_proveedor?p=2" target="_blank" title="Ver proveedores" class="input-group-addon input-sm"><i class="glyphicon glyphicon-plus"></i></a>
										  	<span class="input-group-addon input-sm" <?php echo $popover.$help2;?>><i class="glyphicon glyphicon-info-sign"></i></span>
										</div>
									</td>
									<td>
										<div class="input-group input-150">
											<input type="text" class="form-control input-sm input-120" id="fac<?php echo $c+1;?>" value="<?php echo $compra->numero_factura;?>">
											<span class="input-group-addon input-sm" <?php echo $popover.$help25;?>><i class="glyphicon glyphicon-info-sign"></i></span>
										</div>
									</td>
									<td>
										<div class="input-group input-120">
											<input type="number" class="form-control input-sm input-120" id="cu<?php echo $c+1;?>" value="<?php if($compra->costo_unitario>0){ echo $compra->costo_unitario;}?>" disabled>
											<span class="input-group-addon input-sm" <?php echo $popover.$help3;?>><i class="glyphicon glyphicon-info-sign"></i></span>
										</div>
									</td>
									<td>
										<div class="input-group input-120">
											<input type="number" class="form-control input-sm" id="can<?php echo $c+1;?>" value="<?php if($compra->cantidad>0){ echo $compra->cantidad;}?>" <?php if($compra->categoria==1 || $compra->categoria==3){ ?> onkeyup="calcula_monto_material('<?php echo $c+1;?>','<?php echo $compra->categoria;?>');" onclick="calcula_monto_material('<?php echo $c+1;?>','<?php echo $compra->categoria;?>');"<?php }?>>
										  	<span class="input-group-addon input-sm" id="med<?php echo $c+1;?>">...</span>
										</div>
									</td>
									<td>
										<div class="input-group input-120">
											<input type="number" class="form-control input-sm input-120" id="i_sis<?php echo $c+1;?>" value="<?php if(($compra->cantidad*$compra->costo_unitario)>0){ echo $compra->cantidad*$compra->costo_unitario;}?>" disabled>
											<span class="input-group-addon input-sm" <?php echo $popover.$help4;?>><i class="glyphicon glyphicon-info-sign"></i></span>
										</div>
									</td>
									<td>
										<div class="input-group input-120">
											<input type="number" id="imp<?php echo $c+1;?>" class="form-control input-sm input-120" onkeyup="refresh_totales_compras()" onclick="refresh_totales_compras();" value="<?php echo $compra->neto;?>">
											<span class="input-group-addon input-sm" <?php echo $popover.$help5;?>><i class="glyphicon glyphicon-info-sign"></i></span>
										</div>
									</td>
									<td>
										
									</td>
								</tr>
					<?php }?>

							</tbody>
							<thead>
								<tr>
									<th colspan="8" class="text-right">TOTAL(Bs.):</th>
									<th id="tot_sis" class="text-right">0</th>
									<th id="tot" class="text-right">0</th>
									<th></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
<script language='javascript'>Onfocus("cli");$('[data-toggle="popover"]').popover({html:true});</script>