<?php 
	
	$help1='title="<h4>Número de orden<h4>" data-content="Representa al número de referencia del movimiento, este numero es unico y generado automaticamente por el sistema, si fuese el caso de que el numero fuera ya registrado puede <b>pulsar el boton actualizar el número.</b>"';
	$help2='title="<h4>Tipo de movimiento<h4>" data-content="Este tipo de movimiento es referencial, representa el tipo de movimiento del presente orden"';
	$help3='title="<h4>Fecha de movimiento<h4>" data-content="Ingrese la fecha de registro del movimiento."';
	$help4='title="<h4>Observaciónes del movimiento<h4>" data-content="Ingrese algunas observaciónes del movimiento si tuviera. El contenido de las observaciones debe ser en formato alfanumérico hasta 700 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help5='title="<h4>Tipo de material a ser adquirido<h4>" data-content="Representa al tipo de materiales que se adquirira, estos tipos se encuentran asignados en la configuración de proveedores."';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$popover2='data-toggle="popover" data-placement="top" data-trigger="hover"';

	$help11='title="<h4>Seleccione el proveedor<h4>" data-content="Seleccione el proveedor de la compra o gasto, si desea adicionar un nuevo proveedor puede hacerlo en la sección cliente proveedor o dar click en el boton <b>+</b>"';
	
		$help121='title="<h4>Material para almacen<h4>" data-content="Seleccione el material a ser adquirido, este material es principalmente materiales que se encuentran en almacen, si no aparece puede no haber seleccionado el proveedor o el proveedor no tiene asignado ningun material,  si desea adicionar un nuevo material puede hacerlo en la sección cliente-proveedor o dar click en el boton <b>+</b>"';
		$help122='title="<h4>Materiales indirectos<h4>" data-content="Seleccione el material indirecto a ser adquirido, este material es principalmente materiales que son usados como materiales indirectos en el producto, si no aparece materiales puede no haber seleccionado el proveedor o el proveedor no tiene asignado ningun material,  si desea adicionar un nuevo material puede hacerlo en la sección cliente-proveedor o dar click en el boton <b>+</b>"';
		$help123='title="<h4>Activo Fijo<h4>" data-content="Seleccione el activo fijo a ser adquirido, si el activo fijo no aparece puede no haber seleccionado el proveedor o el proveedor no tiene asignado ningun material,  si desea adicionar un nuevo activo fijo puede hacerlo en la sección cliente-proveedor o dar click en el boton <b>+</b>"';
		$help124='title="<h4>Otro tipo de materiales y/o productos<h4>" data-content="Seleccione el material a ser adquirido, este material es principalmente materiales varios que no se encuentran en las opciones anteriores, si el material no aparece puede no haber seleccionado el proveedor o el proveedor no tiene asignado ningun material,  si desea adicionar un nuevo material puede hacerlo en la sección cliente-proveedor o dar click en el boton <b>+</b>."';
	
	$help13='title="<h4>Costo por unidad<h4>" data-content="Representa al costo por unidad, asignado en al proveedor"';
	$help14='title="<h4>Importe sistema<h4>" data-content="Representa al costo total segun el <b>costo unitario</b> y la <b>cantidad</b> a ser adquirida."';
	$help15='title="<h4>Importe<h4>" data-content="Representa al costo total a ser tomado como costo final del gasto, inicialmente es el costo sugerio por el sistema, este costo puede ser modificado."';
	$popover='data-toggle="popover" data-placement="top" data-trigger="hover"';

	$tipo_gasto=array(1=>'Gasto',2=>'Compra', 3=>'Pago');
	$categorias=array('Materiales','Materiales indirectos','Activo Fijo','Otro tipo de materiales y/o productos');
?>
<div class="row"><div class="col-sm-2 col-sm-offset-10 col-xs-12"><strong><span class='text-danger'>(*)</span>Campos obligatorio</strong></div></div>
		<div class="list-group">
		  <div class="list-group-item active">
		    <h5 class="list-group-item-heading">ORDEN DE <span class="g_text">GASTO</span></h5>
		  </div>
		  <div class="list-group-item">
		  	<div class="row">
		  		<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="input-group">
						<div class="form-control text-right" style="height:60px; font-size:30px;" id="nro_ord"><?php echo $compras[0]->orden_gasto;?></div>
						<span class="btn input-group-addon" disabled><i class="glyphicon glyphicon-refresh"></i></span>
						<span class="input-group-addon" <?php echo $popover.$help1;?>><i class="glyphicon glyphicon-info-sign"></i></span>
					</div>
		  		</div>
		  		<div class="col-md-3 col-sm-6 col-xs-12">
		  			<h5 class="list-group-item-heading"><strong><span class='text-danger'>(*)</span>Tipo de movimiento: </strong></h5>
		  			<div class="input-group col-xs-12">
						<select id="tg" class="form-control input-sm" onchange="orden()">
						<?php for ($i=1; $i<=count($tipo_gasto) ; $i++) { ?>
							<option value="<?php echo $i;?>"<?php if($compras[0]->tipo==$i){ echo "selected";}?>><?php echo $tipo_gasto[$i];?></option>
						<?php }//en for ?>
						</select>
						<span class="input-group-btn" <?php echo $popover.$help2;?>><div class="btn btn-default btn-sm" type="button"><span class="glyphicon glyphicon-info-sign"></span></div></span>
				   	</div>
		  		</div><i class='clearfix visible-sm-block'></i>
		  		<div class="col-md-3 col-sm-6 col-xs-12">
		  			<h5 class="list-group-item-heading"><strong><span class='text-danger'>(*)</span>Fecha de <span id="g_text2">gasto</span>: </strong></h5>
		  			<div class="input-group">
						<input type="date" class="form-control input-sm" value="<?php echo $compras[0]->fecha;?>" id="fecha">
						<span class="input-group-addon" <?php echo $popover.$help3;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
		  		</div>
		  		<div class="col-md-3 col-sm-6 col-xs-12">
		  			<h5 class="list-group-item-heading"><strong>Observaciónes: </strong></h5>
			      	<div class="input-group">
			      		<textarea class="form-control input-sm" id="obs" placeholder="Observaciónes en la compra, gasto o pago"><?php echo $compras[0]->observaciones;?></textarea>
			      		<span class="input-group-addon" <?php echo $popover.$help4;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
		  		</div>
		  	</div>
		  </div>
		</div>
	<div class="list-group">
		  <div class="list-group-item active">
		    <h5 class="list-group-item-heading" id="g_text">DETALLE DE ELEMENTOS EN EL <span class='g_text'>GASTO</span></h5>
		  </div>
		<div class="list-group-item">
		  	<div class="row">
		  		<div class="col-xs-12">
					<div class="table-responsive">
						<table id="table-np" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="4%">#</th>
									<th width="12%">NIT/CI</th>
									<th width="20%">Proveedor</th>
									<th width="23%">Material o producto</th>
									<th width="6%">C/U (Bs.)</th>
									<th width="10%">Cantidad</th>
									<th width="10%">Importe<br>s/sistema(Bs.)</th>
									<th width="10%">Importe(Bs.)</th>
									<th width="5%"></th>
								</tr>
							</thead>
						<?php ?>
							<tbody id="content_protuct">
						<?php for($i=0; $i<count($compras); $i++){ $compra=$compras[$i];
								//cargando nit proveedor
								if($compra->categoria==1){ $materiales_proveedores=$this->M_material->material_proveedor();}
								if($compra->categoria==2){ $materiales_proveedores=$this->M_material_adicional->material_proveedor();}
								if($compra->categoria==3){ $materiales_proveedores=$this->M_activo_fijo->material_proveedor();}
								if($compra->categoria==4){ $materiales_proveedores=$this->M_material_varios->material_proveedor();}
								//cargando materiales
								if($compra->categoria==1){ $materiales=$this->M_material_proveedor->get_material($compra->idpro,'material'); }
								if($compra->categoria==2){ $materiales=$this->M_material_proveedor->get_material($compra->idpro,'material_adicional'); }
								if($compra->categoria==3){ $materiales=$this->M_material_proveedor->get_material($compra->idpro,'activo_fijo'); }
								if($compra->categoria==4){ $materiales=$this->M_material_proveedor->get_material($compra->idpro,'material_vario'); }
						?>
							<tr>
								<td id="auto-num"></td>
								<td>
									<input type="hidden" id="pos<?php echo $i+1;?>" value="<?php echo $i+1;?>">
									<div class="input-group input-140">
										<select id="nit<?php echo $i+1;?>" class="form-control input-sm" onchange="selected_proveedor('<?php echo $i+1;?>','<?php echo $compra->categoria?>');">
											<option value="">Seleccionar...</option>
									<?php for ($j=0; $j < count($materiales_proveedores) ; $j++) { ?>
											<option value="<?php echo $materiales_proveedores[$j]->idpro;?>" <?php if($compra->idpro==$materiales_proveedores[$j]->idpro){ echo "selected";}?>><?php echo $materiales_proveedores[$j]->nit;?></option>
									<?php }?>
										</select>
									</div>
								</td>
								<td>
									<input type="hidden" id="cat<?php echo $i+1;?>" value="<?php echo $compra->categoria;?>">
									<div class="input-group input-220">
									  	<select id="prov<?php echo $i+1;?>" class="form-control input-sm" onchange="selected_nit('<?php echo $i+1;?>','<?php echo $compra->categoria?>');">
											<option value="">Seleccionar...</option>
								<?php for ($j=0; $j < count($materiales_proveedores) ; $j++) { ?>
											<option value="<?php echo $materiales_proveedores[$j]->idpro;?>" <?php if($compra->idpro==$materiales_proveedores[$j]->idpro){ echo "selected";}?>><?php echo $materiales_proveedores[$j]->nombre_proveedor;?></option>
								<?php }?>
										</select>
										<a href="<?php echo base_url();?>cliente_proveedor?p=2" target="_blank" title="Ver proveedores" class="input-group-addon input-sm"><i class="glyphicon glyphicon-plus"></i></a>
									  	<span class="input-group-addon input-sm" <?php echo $popover.$help11;?>><i class="glyphicon glyphicon-info-sign"></i></span>
									</div>
								</td>
								<td>
									<div class="input-group input-220">
									  	<select id="mat<?php echo $i+1;?>" class="form-control input-sm" onchange="monto_material('<?php echo $i+1;?>','<?php echo $compra->categoria?>');">
											<option value=" ">Seleccionar...</option>
								<?php for ($j=0; $j < count($materiales) ; $j++) { $material=$materiales[$j]; ?>
											<option value="<?php echo $material->idmp;?>" <?php if($compra->idmp==$material->idmp){ echo "selected";}?>><?php echo $material->nombre;?></option>
								<?php }?>
										</select>
									  	<a href="<?php echo base_url();?>cliente_proveedor?p=2" target="_blank" title="Ver proveedores" class="input-group-addon input-sm"><i class="glyphicon glyphicon-plus"></i></a>
									  	<?php $msj="";
									  		if($compra->categoria==1){ $msj=$popover.$help121;}
									  		if($compra->categoria==2){ $msj=$popover.$help122;}
									  		if($compra->categoria==3){ $msj=$popover.$help123;}
									  		if($compra->categoria==4){ $msj=$popover.$help124;}
									  	?>
									  	<span class="input-group-addon input-sm" <?php echo $msj;?>><i class="glyphicon glyphicon-info-sign"></i></span>
									</div>
								</td>
								<td>
									<div class="input-group input-120">
										<input type="number" class="form-control input-sm input-120" id="cu<?php echo $i+1;?>" value="<?php if($compra->costo_unitario>0){ echo $compra->costo_unitario;}?>" disabled>
										<span class="input-group-addon input-sm" <?php echo $popover.$help13;?>><i class="glyphicon glyphicon-info-sign"></i></span>
									</div>
								</td>
								<td>
									<div class="input-group input-120">
										<input type="number" class="form-control input-sm" id="can<?php echo $i+1;?>" value="<?php if($compra->cantidad>0){ echo $compra->cantidad;}?>" <?php if($compra->categoria==1 || $compra->categoria==3){ ?> onkeyup="calcula_monto_material('<?php echo $i+1;?>','<?php echo $compra->categoria?>');" onclick="calcula_monto_material('<?php echo $i+1;?>','<?php echo $compra->categoria?>');"<?php }?>>
									  	<span class="input-group-addon input-sm" id="med<?php echo $i+1;?>">...</span>
									</div>
								</td>
								<td>
									<div class="input-group input-120">
										<input type="number" class="form-control input-sm input-120" id="i_sis<?php echo $i+1;?>" value="<?php if(($compra->cantidad*$compra->costo_unitario)>0){ echo $compra->cantidad*$compra->costo_unitario;}?>" disabled>
										<span class="input-group-addon input-sm" <?php echo $popover.$help14;?>><i class="glyphicon glyphicon-info-sign"></i></span>
									</div>
								</td>
								<td>
									<div class="input-group input-120">
										<input type="number" id="imp<?php echo $i+1;?>" class="form-control input-sm input-120" onkeyup="refresh_totales_compras()" onclick="refresh_totales_compras();" value="<?php echo $compra->neto;?>">
										<span class="input-group-addon input-sm" <?php echo $popover.$help15;?>><i class="glyphicon glyphicon-info-sign"></i></span>
									</div>
								</td>
								<td>
									<?php $this->load->view('estructura/botones/botones_registros',['eliminar'=>"drop_fila($(this));"]);?>
								</td>
							</tr>
						<?php }//end for?>

							</tbody>
							<thead>
								<tr>
									<th colspan="6" class="text-right">TOTAL(Bs.):</th>
									<th id="tot_sis" class="text-right">0</th>
									<th id="tot" class="text-right">0</th>
									<th></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				<div class="col-md-3 col-md-offset-7 col-sm-4 col-sm-offset-5 col-xs-12">
					<div class="input-group">
						<select id="cat" class="form-control input-md" onchange="">
				<?php for($i=1; $i<=count($categorias) ; $i++){ ?>
						<option value="<?php echo $i;?>"><?php echo $categorias[$i-1];?></option>
				<?php }//en for ?>
						</select>
					  	<span class="input-group-btn" <?php echo $popover.$help5;?>><div class="btn btn-default btn-md" type="button"><span class="glyphicon glyphicon-info-sign"></span></div></span>
					</div>
				</div>
				<div class="col-md-2 col-sm-3 col-xs-12">
					<button class="btn btn-info col-xs-12" onclick="add_row_con_orden($(this));"><span class='glyphicon glyphicon-plus'></span> Adicionar material</button>
				</div>
			</div>
		</div>
	</div>
<script language='javascript'>Onfocus("cli");$('[data-toggle="popover"]').popover({html:true});</script>