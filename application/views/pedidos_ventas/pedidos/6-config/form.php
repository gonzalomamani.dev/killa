<?php
	$help1='title="<h4>Número de pedido<h4>" data-content="Representa al número de referencia del pedido, este numero es unico y generado automaticamente por el sistema."';
	$help15='title="<h4>Nombre de pedido<h4>" data-content="Representa al nombre del pedido, el contenido debe ser en formato alfanumérico <b>de 3 a 150 caracteres <b>puede incluir espacios</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
	$help2='title="<h4>Cliente<h4>" data-content="Representa al cliente que realiza el pedido"';
	$help3='title="<h4>Fecha de pedido<h4>" data-content="Ingrese la fecha de registro de este pedido."';
	$help4='title="<h4>Observaciónes del pedido<h4>" data-content="Ingrese algunas observaciónes del pedido si tuviera. El contenido de las observaciones debe ser en formato alfanumérico <b>hasta 900 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<ul class="nav nav-tabs">
  	<li role="presentation" class="active"><a href="javascript:" onclick="config_pedido('<?php echo $pedido->idpe;?>')">Pedido</a></li>
 <?php for($i=0; $i < count($sub_pedidos); $i++) { ?>
 	<li role="presentation"><a href="javascript:" onclick="config_sub_pedido('<?php echo $pedido->idpe;?>','<?php echo $sub_pedidos[$i]->idsp;?>')">Parte <?php echo $sub_pedidos[$i]->numero; ?></a></li>
 <?php } ?>
 	<li role="presentation"><a href="javascript:" onclick="config_pedido('<?php echo $pedido->idpe;?>')">+ Nuevo</a></li>
</ul>
		<div class="row"><div class="col-sm-2 col-sm-offset-10 col-xs-12"><strong><span class='text-danger'>(*)</span>Campos obligatorio</strong></div></div>
		<div class="list-group">
		  <div class="list-group-item active">
		    <h5 class="list-group-item-heading" id="g_text">DETALLE DEL PEDIDO</h5>
		  </div>
		  <div class="list-group-item">
		  	<div class="row">
		  		<div class="col-md-1 col-sm-2 col-xs-12 text-left"><h5 class="list-group-item-heading"><strong><span class='text-danger'>(*)</span>N° de pedido:</strong></h5></div>
		  		<div class="col-md-3 col-sm-4 col-xs-12">
					<div class="input-group">
						<div class="form-control text-right" disabled><?php echo $pedido->numero;?></div>
						<span class="input-group-addon input-sm" <?php echo $popover.$help1;?>><i class="glyphicon glyphicon-info-sign"></i></span>
					</div>
		  		</div>
		  		<div class="col-md-1 col-sm-2 col-xs-12 text-left"><h5 class="list-group-item-heading"><strong><span class='text-danger'>(*)</span>Nombre de pedido:</strong></h5></div>
		  		<div class="col-md-3 col-sm-4 col-xs-12">
		  			<div class="input-group">
						<input type="text" class="form-control input-sm" id="p_nom" placeholder="Nombre de pedido" value="<?php echo $pedido->nombre;?>">
						<span class="input-group-addon input-sm" <?php echo $popover.$help15;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
		  		</div><i class='visible-sm-block clearfix'></i>
		  		<div class="col-md-1 col-sm-2 col-xs-12 text-left"><h5 class="list-group-item-heading"><strong><span class='text-danger'>(*)</span>Cliente:</strong></h5></div>
		  		<div class="col-md-3 col-sm-4 col-xs-12">
		  			<div class="input-group">
						<select class="form-control input-sm" disabled>
							<option value="">Seleccionar...</option>
						<?php 
						for ($i=0; $i < count($clientes) ; $i++) { $cliente=$clientes[$i]; ?>
							<option value="<?php echo $cliente->idcl; ?>" <?php if($cliente->idcl==$pedido->idcl){ echo "selected";}?>><?php echo $cliente->nombre;?></option>
						<?php } ?>
						</select>
						<span class="input-group-addon input-sm" <?php echo $popover.$help2;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
		  		</div><i class='visible-lg-block visible-md-block clearfix'></i>
		  		<div class="col-md-1 col-sm-2 col-xs-12 text-left"><h5 class="list-group-item-heading"><strong>Observaciónes:</strong></h5></div>
		  		<div class="col-md-11 col-sm-10 col-xs-12">
			      	<div class="input-group">
			      		<textarea class="form-control input-sm" id="p_obs" placeholder="Observaciónes de pedido"><?php echo $pedido->observaciones;?></textarea>
			      		<span class="input-group-addon input-sm" <?php echo $popover.$help4;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
		  		</div>
		  	</div>
		  </div>	  
		</div>
<script language='javascript'>Onfocus("p_nom");$('[data-toggle="popover"]').popover({html:true});</script>