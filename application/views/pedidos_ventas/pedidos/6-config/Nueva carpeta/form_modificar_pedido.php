<?php
	$pedido=$pedido[0];
?>

<ul class="nav nav-tabs">
  <li role="presentation" class="active"><a href="javascript:" onclick="form_modificar_pedido('<?php echo $pedido->idpe; ?>')">Modificar</a></li>
  <li role="presentation"><a href="javascript:" onclick="productos_pedido('<?php echo $pedido->idpe; ?>')">Productos</a></li>
</ul>

<div class="row row-bordered">
		<div class="list-group">
	  <div class="list-group-item active">
	    <h5 class="list-group-item-heading" id="g_text">PEDIDO</h5>
	  </div>
	  <div class="list-group-item">
	  <table class="tabla tabla-border-false">
	  	<tr class="fila" >
	  		<td class="celda td" width='20%'>
				<h5 class="list-group-item-heading"><strong>Cliente</strong></h5>
			   	<p class="list-group-item-text">
					<select id="cliente" class="form-control input-sm">
						<option value="">Seleccionar...</option>
					<?php 
					for ($i=0; $i < count($clientes) ; $i++) { $cliente=$clientes[$i]; ?>
						<option value="<?php echo $cliente->idcl; ?>" <?php if($pedido->idcl==$cliente->idcl){ echo "selected";}?>><?php echo $cliente->nombre;?></option>
					<?php } ?>
					</select>
			   	</p>
			</td>
			<td class="celda td">
				<h5 class="list-group-item-heading"><strong>Fecha de Pedido</strong></h5>
			   	<p class="list-group-item-text">
					<input type="date" class="form-control input-sm" value="<?php echo $pedido->fecha_pedido;?>" id="f1">
			   	</p>
			</td>
			<td class="celda td" width="20%">
				<h5 class="list-group-item-heading"><strong>Fecha estimada de entrega</strong></h5>
	      		<p class="list-group-item-text">
					<input type="date" class="form-control input-sm" value="<?php echo $pedido->fecha_entrega;?>" id="f2">
	      		</p>
			</td>

			<td class="celda td" width="40%" rowspan="2">
				<b class="list-group-item-text">
					<div class="input-group">
					  <span class="input-group-addon" style="height:45px; font-size:20px; ">Nº DE PEDIDO:</span>  
					</div>
					<div class="input-group">
						<div class="form-control" style="height:60px; font-size:30px;"><?php echo $pedido->idpe;?></div>
						  <span class="input-group-addon"></span>
						</div>
					</div>
				</b>
			</td>
	  	</tr>
	  	<tr class="fila">
	  		<td class="celda td">
				<h5 class="list-group-item-heading"><strong>Monto Total del Pedido</strong></h5>
	      		<p class="list-group-item-text">
					<input type="number" class="form-control input-sm" value="<?php echo $pedido->monto;?>" id="monto">
	      		</p>
			</td>
	  		<td class="celda td">
	  			<h5 class="list-group-item-heading"><strong>Monto adelantado</strong></h5>
	      		<p class="list-group-item-text">
	      			<input type="number" class="form-control input-sm" value="<?php echo $pedido->adelanto;?>" id="adelanto">
	      		</p>
	  		</td>
	  		<td class="celda td">
	  			<h5 class="list-group-item-heading"><strong>Observaciónes</strong></h5>
	      		<p class="list-group-item-text">
	      			<textarea class="form-control input-sm" id="observaciones"><?php echo $pedido->observaciones;?></textarea>
	      		</p>
	  		</td>

	  	</tr>
	  </table>
	  </div>
	</div>
</div>
<div class="row row-border">
	<div class="col-xs-12">
		<div class="table-responsive">
			<table id="table-np" class="table table-bordered table-striped">
				<thead>
					<th width="10%">#Item</th>
					<th width="35%">Produto</th>
					<th width="15%">Costo por Unid.(Bs)</th>
					<th width="15%">Cantidad</th>
					<th width="18%">Costo Total (Bs)</th>
					<th width="7%"></th>
				</thead>
				<tbody id="content_protuct">
				<?php for ($i=0; $i < count($detalle_pedidos) ; $i++) { $dp=$detalle_pedidos[$i]; 
						$costo_unitario=0;
					?>
					<tr>
						<td id="auto-num"></td>
						<td><select class="form-control input-sm" id="prod<?php echo $i+1;?>" onchange="costo_producto('<?php echo $i+1;?>')">
							<option value="">Seleccione...</option>
						<?php
						for ($j=0; $j < count($productos) ; $j++) { $producto=$productos[$j];
							$prod=$this->M_producto->get($producto->idp); 
							$material=$this->M_material->get($producto->idm);
							$color=$this->M_color->get($material[0]->idco);?>
							<option value="<?php echo $producto->idpim;?>" <?php if($producto->idpim==$dp->idpim){echo "selected"; $costo_unitario=$producto->costo_unitario;}?>><?php echo $prod[0]->nombre.'-'.$color[0]->nombre; ?></option>
						<?php } ?>
						</select>
						</td>
						<td id="con_can<?php echo $i+1;?>"><input type="number" class="form-control input-sm" disabled="disabled" value="<?php echo $costo_unitario;?>"></div></td>
						<td><input type="number" class="form-control input-sm" id="can<?php echo $i+1; ?>" onkeyup="costo_total_producto('<?php echo $i+1;?>')" value='<?php echo $dp->cantidad;?>'></td>
						<td><input type="number" class="form-control input-sm" id="ct<?php echo $i+1; ?>" disabled='disabled' value="<?php echo $costo_unitario*$dp->cantidad;?>"></td>
						<td><button class="btn btn-danger btn-sm" title="Eliminar..." onclick="confirm_update_pedido($(this),'<?php echo $dp->iddp;?>');"><span class="glyphicon glyphicon-remove"></span></button></td>
					</tr>
				<?php }?>
				</tbody>
			</table>
		</div>
		<button class="btn btn-default col-xs-2 col-xs-offset-10" onclick="adicionar_row_pedido_producto();"><span class='glyphicon glyphicon-plus'></span> Adicionar</button>
	</div>
</div>

<script type="text/javascript">button_modal('modal_ok_1','modificar_pedido',"'<?php echo $pedido->idpe;?>'",'modal_print_1','','','modal_closed_1');cerrar_modal('content_modal','modal');</script>