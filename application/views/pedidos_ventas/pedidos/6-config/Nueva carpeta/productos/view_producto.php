
<?php
	$url=base_url().'libraries/img/pieza_productos/miniatura/';
?>


<div class="panel-group" id="accordion">
<?php 
	for ($i=0; $i < count($productos) ; $i++) { $producto=$productos[$i];
		$img="default.png";
?>
  <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title" style="text-align:left;">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $producto->idp; ?>">
        <?php $swap=$this->M_categoria_producto->get_portada_producto($producto->idp);
          $resto=3;
          if(count($swap)>0){
            $im=$this->M_imagen_producto->get_pieza_material($swap[0]->idpim);
            $img="default.png";
            $resto-=count($im);
             for ($ip=0; $ip < count($im); $ip++) {$img_producto=$im[$ip];?>
              <img src="<?php echo $url.$img_producto->nombre;?>" width='40px'>
            <?php
            }//end for
          }//end if
          for($j=0; $j < $resto; $j++){ ?>
            <img src="<?php echo $url.'default.png';?>" width='40px'>
            <?php
          }
        ?>
           <?php echo $producto->nombre;?>
        </a>
      </h4>
    </div>
    <div id="collapse<?php echo $producto->idp; ?>" class="panel-collapse collapse">
      <div class="panel-body">    
      <?php $categorias=$this->M_categoria_producto->get_row('idcp',$producto->idcp); //echo count($categorias).' '.$producto->idcp;?>
      <?php if(count($categorias)>0){?>
      <table class="tabla tabla-border-true">
 	<?php for ($ca=0; $ca < count($categorias); $ca++) { $categoria=$categorias[$ca];
        //color
    $mat=$this->M_material->get($categoria->idma);
      $color=$this->M_color->get($mat[0]->idco);
 			$imagenes=$this->M_imagen_producto->get_pieza_material($categoria->idpim);
 			$resto=3-count($imagenes);
      	?>
      	<tr class="fila">
      	<?php
      		for ($img=0; $img < count($imagenes) ; $img++){$imagene=$imagenes[$img]; 
      	?>
      		<td class="celda td" width="10%"><img src="<?php echo $url.$imagene->nombre;?>" width='30px'></td>
      	<?php
      		}
      		for ($img=0; $img < $resto ; $img++) { 
      	?>
      		<td class="celda td" width="10%"><img src="<?php echo $url.'default.png';?>" width='30px'></td>
      	<?php
      		}//end for
      	?>
      		<td class="celda td" width="65%" style="color:<?php echo $color[0]->codigo;?>; font-weight:bold;"><?php echo $producto->nombre,' - '.$color[0]->nombre;?></td>
      	<?php
      		$detalle_pedido=$this->M_detalle_pedido->get_row_2n('idpe',$idpe,'idpim',$categoria->idpim);
      	?>
      		<th class="celda th">
      		<?php if(count($detalle_pedido)>0){ $dp=$detalle_pedido[0];?>
      			<a href="javascript:" title='Quitar Producto'onclick="save_producto(this,'rojo','<?php echo $idpe;?>','<?php echo $categoria->idpim;?>')" class='g-boton-circle g-boton-circle-25'>
            <div class="g-boton-circle-child g-boton-color-rojo"></div>
				</a>
			<?php }else{?>
				<a href="javascript:" title="Seleccionar Producto" onclick="save_producto(this,'rojo','<?php echo $idpe;?>','<?php echo $categoria->idpim;?>')" class='g-boton-circle g-boton-circle-25'>
					<div class="g-boton-circle-child g-boton-color-default"></div>
				</a>
			<?php }?>
      		</th>
      	</tr>
      	<?php
    }?>
      </table>
    <?php }?>
      </div>
    </div>
  </div>
 <?php } ?>
</div>
