<?php
	$resp=$pedido[0];
	$url=base_url().'libraries/img/pieza_productos/miniatura/'; 
?>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="form_modificar_pedido('<?php echo $resp->idpe; ?>')">Modificar</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="productos_pedido('<?php echo $resp->idpe; ?>')">Productos</a></li>
</ul>
<table class="tabla tabla-border-true">
	<tr class="fila"><th class="celda th" colspan="3"></th><th class="celda th" colspan="2"><a href="javascript:" onclick="form_adicionar_producto('<?php echo $resp->idpe;?>')">Adicionar Producto</a></th></tr>
	<tr class="fila">
		<th class="celda th" width="7%"></th>
		<th class="celda th" width="48%">Producto</th>
		<th class="celda th" width="15%">Cantidad</th>
		<th class="celda th" width="10%">Guardar</th>
		<th class="celda th" width="10%">Eliminar</th>
	</tr>
	<?php
		for($i=0; $i < count($detalle_pedido); $i++) { $dep=$detalle_pedido[$i];
			$pieza=$this->M_categoria_pieza->get($dep->idcp);
			$producto=$this->M_producto->get_col($pieza[0]->idp,'nombre');
			//color del producto
			$categoria=$this->M_categoria_producto->get($dep->idpim);
			$mat=$this->M_material->get($categoria[0]->idma);
			$color=$this->M_color->get($mat[0]->idco);
			//imagen producto
			$imagen=$this->M_imagen_producto->get_pieza_material($dep->idpim);
			$img="default.png";
			if(count($imagen)>0){$img=$imagen[0]->nombre;}
		?>
			<tr class="fila">
				<th class="celda th"><img src="<?php echo $url.$img;?>" width='100%'></th>
				<td class="celda td" ><?php echo $producto[0]->nombre.' - '.$color[0]->nombre;?></td>
				<td class="celda td" ><input type="number" id="can<?php echo $dep->iddp;?>" placeholder='cantidad solicitada' value="<?php echo $dep->cantidad;?>"></td>
				<th class="celda th" ><a href="javascript:" onclick="save_cantidad_pedido('<?php echo $dep->idpe;?>','<?php echo $dep->iddp;?>')"> Guardar</a></th>
				<th class="celda th" ><a href="javascript:" onclick="drop_producto_pedido('<?php echo $dep->idpe;?>','<?php echo $dep->iddp;?>')"> Eliminar</a></th>
			</tr>
		<?php
		}
	?>
	<tr class="fila"><th class="celda th" colspan="3"></th><th class="celda th" colspan="2"><a href="javascript:" onclick="form_adicionar_producto('<?php echo $resp->idpe;?>')">Adicionar Producto</a></th></tr>
</table>