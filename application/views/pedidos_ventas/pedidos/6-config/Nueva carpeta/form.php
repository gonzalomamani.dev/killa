<?php
	$pedido=$pedido[0];
	$help1='title="<h4>Número de pedido<h4>" data-content="Representa al número de referencia del pedido, este numero es unico y generado automaticamente por el sistema en el momento de registro del pedido, una vez generado es <b>imposible modificar el numero</b>"';
	$help2='title="<h4>Cliente<h4>" data-content="Seleccione el cliente que realiza el pedido, si desea adicionar un nuevo cliente lo puede hacer accediendo por el menu principal izquierdo <b>Cliente/Proveedor</b>, o dando click en el boton <b>+</b>"';
	$help3='title="<h4>Fecha de pedido<h4>" data-content="Ingrese la fecha de registro de este pedido."';
	$help4='title="<h4>Observaciónes del pedido<h4>" data-content="Ingrese algunas observaciónes del pedido si tuviera. El contenido de las observaciones debe ser en formato alfanumérico hasta 900 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help5='title="<h4>Fecha de creación<h4>" data-content="Seleccione la fecha cuando se creo o finalizo el diseño del producto. en formato 2000-01-31"';
	$help6='title="<h4>Costo de pedido según sistema<h4>" data-content="Costo total del pedido según las cantidades y el costo del producto según el cliente, <b>Para poder actualizar el costo debe dar click en el boton Calcular</b>"';
	$help7='title="<h4>Costo del pedido<h4>" data-content="Costo del pedido, el valor por defecto es el dado por el sistema, este valor esta sujeto a modificación."';
	$help8='title="<h4>Descuento<h4>" data-content="Monto de descuento, representa el descuento realizado en el pedido."';
	$help9='title="<h4>Costo final del pedido<h4>" data-content="Representa al coso final del pedido, como resultado de la diferencia del costo del pedido y el descuento."';
	$help10='title="<h4>Tiempo estimado de producción<h4>" data-content="Representa a la suma total del tiempo de producción de los productos asignados al producto."';
	$help11='title="<h4>Fecha de inicio<h4>" data-content="Representa a la fecha de inicio cuando se iniciara con la producción del pedido."';
	$help12='title="<h4>Fecha estimada de entrega<h4>" data-content="Representa a la fecha de entrega según el sistema, el calculo de fecha de entrega es calculado haciendo uso de la carga horaria estandar dasiganda y los dias registrados como no trabajados."';
	$help13='title="<h4>Fecha de entrega del pedido<h4>" data-content="Inicialmente tiene el valor de lafecha estomada de entrega, el valor de la fecha en modificable, <b>preferiblemente la fecha debe ser mayor a la fecha estimada de entrega.</b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
		<div class="row"><div class="col-sm-2 col-sm-offset-10 col-xs-12"><strong><span class='text-danger'>(*)</span>Campos obligatorio</strong></div></div>
		<div class="list-group">
		  <div class="list-group-item active">
		    <h5 class="list-group-item-heading" id="g_text">DETALLE DEL PEDIDO</h5>
		  </div>
		  <div class="list-group-item">
		  	<div class="row">
		  		<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="input-group">
						<div class="form-control text-right" style="height:60px; font-size:30px;"><?php echo $pedido->numero;?></div>
						<span class="btn input-group-addon" disabled><i class="glyphicon glyphicon-refresh"></i></span>
						<span class="input-group-addon" <?php echo $popover.$help1;?>><i class="glyphicon glyphicon-info-sign"></i></span>
					</div>
		  		</div>
		  		<div class="col-md-3 col-sm-6 col-xs-12">
		  			<h5 class="list-group-item-heading"><strong><span class='text-danger'>(*)</span>Cliente:</strong></h5>
				   	<div class="input-group">
						<select id="cli" class="form-control input-sm" <?php if(count($detalle_pedido)>0){ echo "disabled";}?>>
							<option value="">Seleccionar...</option>
						<?php 
						for ($i=0; $i < count($clientes) ; $i++) { $cliente=$clientes[$i]; ?>
							<option value="<?php echo $cliente->idcl; ?>" <?php if($pedido->idcl==$cliente->idcl){ echo "selected";} ?>><?php echo $cliente->nombre;?></option>
						<?php } ?>
						</select>
						<a href="<?php echo base_url();?>cliente_proveedor" target="_blank" title="Ver Clientes" class="input-group-addon"><i class="glyphicon glyphicon-plus"></i></a>
						<span class="input-group-addon" <?php echo $popover.$help2;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
		  		</div><i class='clearfix visible-sm-block'></i>
		  		<div class="col-md-3 col-sm-6 col-xs-12">
		  			<h5 class="list-group-item-heading"><strong><span class='text-danger'>(*)</span>Fecha de pedído: </strong></h5>
		  			<div class="input-group">
						<input type="date" class="form-control input-sm" value="<?php echo $pedido->fecha_pedido; ?>" id="fped">
						<span class="input-group-addon" <?php echo $popover.$help3;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
		  		</div>
		  		<div class="col-md-3 col-sm-6 col-xs-12">
		  			<h5 class="list-group-item-heading"><strong>Observaciónes: </strong></h5>
			      	<div class="input-group">
			      		<textarea class="form-control input-sm" id="obs" placeholder="Observaciónes de pedido"><?php echo $pedido->observaciones;?></textarea>
			      		<span class="input-group-addon" <?php echo $popover.$help4;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
		  		</div>
		  	</div>
		  </div>
		</div>
	<div class="list-group">
		  <div class="list-group-item active">
		    <h5 class="list-group-item-heading" id="g_text">PRODUCTOS EN EL PEDIDO</h5>
		  </div>
		<div class="list-group-item">
		  	<div class="row">
		  		<div class="col-xs-12">
					<div class="table-responsive">
						<table id="table-np" class="table table-bordered table-hover">
							<thead>
								<th width="4%">#</th>
								<th width="47%">Nombre del produto asignado</th>
								<th width="13%">Costo por<br>unid.(Bs)</th>
								<th width="13%">Cantidad</th>
								<th width="18%">Costo<br>Total (Bs)</th>
								<th width="5%"></th>
							</thead>
							<tbody id="content_protuct">
								<?php for ($dp=0; $dp < count($detalle_pedido) ; $dp++) { 
										$prod_cliente=$this->M_producto_cliente->get_row_2n('idcl',$pedido->idcl,'idpim',$detalle_pedido[$dp]->idpim);
									?>
									<tr>
										<td id="auto-num"></td>
										<td>
											<input type="hidden" id="pos<?php echo $dp+1;?>" value="<?php echo $dp+1;?>">
											<div class="input-group">
												<select class="form-control input-sm" id="prod<?php echo $dp+1;?>" onchange="calcular_fila('<?php echo $dp+1;?>','<?php echo $pedido->idcl;?>')">
													<option value="">Seleccione...</option>
											<?php for ($i=0; $i < count($productos_cliente) ; $i++) { $producto=$productos_cliente[$i];
													$prod=$this->M_producto->get($producto->idp); 
													$material=$this->M_material->get_material_color($producto->idm);	
											?>
													<option value="<?php echo $producto->idpim;?>" <?php if($detalle_pedido[$dp]->idpim==$producto->idpim){ echo "selected";}?>><?php echo $prod[0]->nombre.'-'.$material[0]->nombre; ?></option>
											<?php } ?>
												</select>
												<a href="<?php echo base_url();?>cliente_proveedor" target="_blank" title="Ver Clientes" class="input-group-addon"><i class="glyphicon glyphicon-plus"></i></a>
												<span class="input-group-addon" <?php echo $popover.$help1;?>><i class="glyphicon glyphicon-info-sign"></i></span>
											</div>
										</td>
										<td id="con_can<?php echo "";?>"><div class="form-control input-sm" disabled="disabled" id="cu<?php echo $dp+1;?>"><?php echo number_format($prod_cliente[0]->costo_unitario,2,'.',',');?></div></td>
										<td><input type="number" class="form-control input-sm" id="can<?php echo $dp+1; ?>" onkeyup="calcular_fila('<?php echo $dp+1;?>','<?php echo $pedido->idcl;?>')" onchange="calcular_fila('<?php echo $dp+1;?>','<?php echo $pedido->idcl;?>')" value='<?php echo $detalle_pedido[$dp]->cantidad; ?>'></td>
										<td><input type="number" class="form-control input-sm" id="ct<?php echo $dp+1; ?>" disabled='disabled' value='<?php echo number_format(($prod_cliente[0]->costo_unitario*$detalle_pedido[$dp]->cantidad),2,'.','');?>'></td>
										<td>
											<?php $this->load->view('estructura/botones/botones_registros',['eliminar'=>"drop_fila($(this));"]);?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-md-2 col-md-offset-10 col-sm-3 col-sm-offset-9 col-xs-12">
					<button class="btn btn-info col-xs-12" onclick="add_row_producto();"><span class='glyphicon glyphicon-plus'></span> Adicionar Producto</button>
				</div>
			</div>
		</div>
	</div>
	<div class="list-group">
		  <div class="list-group-item active">
		    <h5 class="list-group-item-heading" id="g_text">DETALLE DE COSTO Y TIEMPO</h5>
		  </div>
		<div class="list-group-item">
		  	<div class="row">
				<div class="col-md-3 col-sm-6 col-xs-12">
					<h5 class="list-group-item-heading"><strong>Costo del pedido de según sistema (Bs.): </strong></h5>
		      		<div class="input-group">
						<input type="number" class="form-control input-sm" id="c_est" value="<?php echo number_format($pedido->monto_sistema,2,'.','');?>" disabled>
						<span class="input-group-addon" <?php echo $popover.$help6;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<h5 class="list-group-item-heading"><strong>Costo de pedido (Bs.)</strong></h5>
		      		<div class="input-group">
		      			<input type="number" class="form-control input-sm" value="<?php echo $pedido->monto_parcial;?>" id="c_cos" onkeyup="actualizar_costo_tiempo()" onchange="actualizar_costo_tiempo()">
		      			<span class="input-group-addon" <?php echo $popover.$help7;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<h5 class="list-group-item-heading"><strong>Descuento (Bs.)</strong></h5>
		      		<div class="input-group">
		      			<input type="number" class="form-control input-sm" value="<?php echo $pedido->descuento;?>" id="c_des" onkeyup="actualizar_costo_tiempo()" onchange="actualizar_costo_tiempo()">
		      			<span class="input-group-addon" <?php echo $popover.$help8;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<h5 class="list-group-item-heading"><strong>Costo final del pedido (Bs.)</strong></h5>
		      		<div class="input-group">
		      			<input type="number" class="form-control input-sm" value="<?php echo $pedido->monto_total;?>" id="c_tot" disabled>
		      			<span class="input-group-addon" <?php echo $popover.$help9;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
				</div>
		  	</div>
		  	<div class="row">
				<div class="col-md-3 col-sm-6 col-xs-12">
				<?php 
						$t=explode(":", $this->lib->hms($pedido->tiempo));
						$hora="";
						if($t[0]!=0 & $t[0]!=''){$hora.=$t[0].'h ';}
						if($t[1]!=0 & $t[1]!=''){$hora.=$t[1].'m ';}
						if($t[2]!=0 & $t[2]!=''){$hora.=$t[2].'s';}
				?>
					<h5 class="list-group-item-heading"><strong>Tiempo de Producción estimado</strong></h5>
		      		<div class="input-group">
		      			<input type="text" class="form-control input-sm" value="<?php echo $hora;?>" id="tiempo" disabled>
		      			<input type="hidden" id="segundos" value="<?php echo $pedido->tiempo;?>">
		      			<span class="input-group-addon" <?php echo $popover.$help10;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<h5 class="list-group-item-heading"><strong>Fecha de inicio</strong></h5>
		      		<div class="input-group">
						<input type="date" class="form-control input-sm" value="<?php echo $pedido->fecha_inicio;?>" id="fini" onchange="calcular_costo_tiempo()">
		      			<span class="input-group-addon" <?php echo $popover.$help11;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<h5 class="list-group-item-heading"><strong>Fecha estimada de entrega</strong></h5>
		      		<div class="input-group">
						<input type="date" class="form-control input-sm" value="<?php echo $pedido->fecha_estimada;;?>" id="fest" disabled>
		      			<span class="input-group-addon" <?php echo $popover.$help12;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<h5 class="list-group-item-heading"><strong>Fecha de Entrega</strong></h5>
		      		<div class="input-group">
						<input type="date" class="form-control input-sm" value="<?php echo $pedido->fecha_entrega;?>" id="fent">
		      			<span class="input-group-addon" <?php echo $popover.$help13;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
				</div>
		  	</div>
		  	<div class="row">
		  		<div class="col-md-2 col-md-offset-10 col-sm-3 col-sm-offset-9 col-xs-12">
					<button class="btn btn-info col-xs-12" onclick="calcular_costo_tiempo();"><span class='icon-calculator2'></span> Calcular</button>
				</div>
		  	</div>
		</div>
	</div>
<script language='javascript'>Onfocus("cli");$('[data-toggle="popover"]').popover({html:true});</script>