<?php
	$help1='title="<h4>Número de parte<h4>" data-content="Representa al número de la parte del pedido, este numero en generado automaticamente por el sistema."';
	$help2='title="<h4>Fecha de pedido<h4>" data-content="Ingrese la fecha de registro de este pedido."';
	$help3='title="<h4>Cliente<h4>" data-content="Nombre de cliente que realiza el pedido."';
	$help4='title="<h4>Observaciónes del pedido<h4>" data-content="Ingrese algunas observaciónes del sub pedido si tuviera. El contenido de las observaciones debe ser en formato alfanumérico <b>hasta 500 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
	$help5='title="<h4>Productos asigandos al cliente<h4>" data-content="Seleccione el producto a ser asigando al pedido, si desea adicionar un nuevo producto al cliente lo puede hacer accediendo por el menu principal izquierdo <b>Cliente/Proveedor</b>, o dando click en el boton <b>+</b>, y dirigirse a la parte de configuración de cliente."';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$url=base_url().'libraries/img/pieza_productos/';
?>
<ul class="nav nav-tabs">
  	<li role="presentation"><a href="javascript:" onclick="config_pedido('<?php echo $sub_pedido->idpe;?>')">Pedido</a></li>
 <?php for($i=0; $i < count($sub_pedidos); $i++) { ?>
 	<li role="presentation" class="<?php if($sub_pedidos[$i]->idsp==$sub_pedido->idsp){ echo "active";} ?>"><a href="javascript:" onclick="config_sub_pedido('<?php echo $sub_pedido->idpe;?>','<?php echo $sub_pedidos[$i]->idsp;?>')">Parte <?php echo $sub_pedidos[$i]->numero; ?></a></li>
 <?php } ?>
 	<li role="presentation"><a href="javascript:" onclick="">+ Nuevo</a></li>
</ul>
		<div class="row"><div class="col-sm-2 col-sm-offset-10 col-xs-12"><strong><span class='text-danger'>(*)</span>Campos obligatorio</strong></div></div>
		<div class="list-group">
		  <div class="list-group-item active">
		    <h5 class="list-group-item-heading" id="g_text">PEDIDO <?php echo $pedido->numero.": ".$pedido->nombre." parte ".$sub_pedido->numero;?></h5>
		  </div>
		  <div class="list-group-item">
		  	<div class="row">
		  		<div class="col-md-1 col-sm-2 col-xs-12 text-left"><h5 class="list-group-item-heading"><strong><span class='text-danger'>(*)</span>N° de parte:</strong></h5></div>
		  		<div class="col-md-3 col-sm-4 col-xs-12">
					<div class="input-group">
						<div class="form-control text-right" disabled><?php echo $sub_pedido->numero;?></div>
						<span class="input-group-addon input-sm" <?php echo $popover.$help1;?>><i class="glyphicon glyphicon-info-sign"></i></span>
					</div>
		  		</div>
		  		<div class="col-md-1 col-sm-2 col-xs-12 text-left"><h5 class="list-group-item-heading"><strong><span class='text-danger'>(*)</span>Fecha de pedido:</strong></h5></div>
		  		<div class="col-md-3 col-sm-4 col-xs-12">
		  			<div class="input-group">
						<input type="date" class="form-control input-sm" id="p_fec" placeholder="Nombre de pedido" value="<?php echo $sub_pedido->fecha;?>">
						<span class="input-group-addon input-sm" <?php echo $popover.$help2;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
		  		</div><i class='visible-sm-block clearfix'></i>
		  		<div class="col-md-1 col-sm-2 col-xs-12 text-left"><h5 class="list-group-item-heading"><strong><span class='text-danger'>(*)</span>Cliente:</strong></h5></div>
		  		<div class="col-md-3 col-sm-4 col-xs-12">
		  			<div class="input-group">
						<select class="form-control input-sm" disabled="disabled">
							<option value="">Seleccionar...</option>
						<?php 
						for ($i=0; $i < count($clientes) ; $i++) { $cliente=$clientes[$i]; ?>
							<option value="<?php echo $cliente->idcl; ?>" <?php if($pedido->idcl==$cliente->idcl){ echo "selected";} ?>><?php echo $cliente->nombre;?></option>
						<?php } ?>
						</select>
						<span class="input-group-addon input-sm" <?php echo $popover.$help3;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
		  		</div><i class='visible-lg-block visible-md-block clearfix'></i>
		  		<div class="col-md-1 col-sm-2 col-xs-12 text-left"><h5 class="list-group-item-heading"><strong>Observaciónes:</strong></h5></div>
		  		<div class="col-md-11 col-sm-10 col-xs-12">
			      	<div class="input-group">
			      		<textarea class="form-control input-sm" id="p_obs" placeholder="Observaciónes de pedido"><?php echo $sub_pedido->observaciones;?></textarea>
			      		<span class="input-group-addon input-sm" <?php echo $popover.$help4;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
		  		</div>
		  	</div>
		  </div>
			<div class="list-group-item">
				<div class="table-responsive">
					<table id="table-np" class="tabla tabla-border-true">
						<thead>
							<tr class="fila">
								<th class="celda th" width="5%">#Item</th>
								<th class="celda th" width="93%">Detalle de producto</th>
								<th class="celda th" width="2%"></th>
							</tr>
						</thead>
						<tbody id="content_protuct">
						<?php $rows=0;
							$products= array();
							for($i=0; $i < count($productos_pedido) ; $i++){ $products[]=$productos_pedido[$i]->codigo; }
							$products=array_unique($products);
							foreach ($products as $key => $codigo) {
							$rows++; ?>
							<tr id="auto-num" class="fila" stack="<?php echo $rows;?>">
								<td class="celda td" style="font-size:1.6em; font-weight:bold; text-align:center;"></td>
								<td class="celda td">
									<div class="input-group">
										<?php $idp="";?>
										<select class="form-control input-sm" id="prod<?php echo $rows;?>" cli="<?php echo $pedido->idcl;?>" posicion="<?php echo $rows;?>" >
											<option value="">Seleccione...</option>
										<?php 
										for ($j=0; $j < count($productos) ; $j++) { $producto=$productos[$j]; ?>
											<option value="<?php echo $producto->idpim;?>" <?php if($codigo==$producto->codigo){ echo "selected"; $idp=$producto->idp;} ?>><?php echo $producto->codigo; ?></option>
										<?php } ?>
										</select>
										<span class="input-group-addon input-sm" <?php echo $popover.$help5;?>><i class="glyphicon glyphicon-search"></i></span>
										<a href="<?php echo base_url();?>cliente_proveedor" target="_blank" title="Ver Clientes" class="input-group-addon input-sm"><i class="glyphicon glyphicon-plus"></i></a>
										<span class="input-group-addon input-sm" <?php echo $popover.$help5;?>><i class="glyphicon glyphicon-info-sign"></i></span>
									</div>
									<table class="table table-bordered">
										<?php $productos_all=$this->M_producto->get_search("codigo",$codigo);?>
										<thead id="categorias<?php echo $rows;?>">
									<?php for ($p=0; $p < count($productos_all) ; $p++) { $producto_all=$productos_all[$p]; 
											$existe=false;
											for($i=0; $i < count($productos_pedido); $i++){ if($productos_pedido[$i]->idp==$producto_all->idp){ $existe=true; break;} }
											if($existe){
												$img="default.png";
												$categoria_productos=$this->M_categoria_producto->get_colores_producto_cliente($producto_all->idp,$pedido->idcl);
												if(count($categoria_productos)>0){
													for ($j=0; $j < count($categoria_productos); $j++) { 
														if($categoria_productos[$j]->portada=="1"){
															$image=$this->M_imagen_producto->get_pieza_material($categoria_productos[$j]->idpim); 
															if(count($image)>0){
																$img=$image[0]->nombre;
															}
														}
													}
												}
									?>
										<tr>
											<td class="g-thumbnail-modal"><div class="g-thumbnail-modal"></div>
												<a href="javascript:" onclick="fotografia('<?php echo $url.$img;?>')"><img src="<?php echo $url.'miniatura/'.$img;?>" width='100%' class='img-thumbnail g-thumbnail-modal'></a>
											</td>
											<td width="20%"><?php echo $producto_all->nombre." - <b>".$producto_all->nombre_grupo."</b>";?></td>
											<td width="77%">
												<table class="table table-bordered table-striped table-hover">
													<tbody>
												<?php for ($c=0; $c < count($categoria_productos) ; $c++){
														$existe=false;
														$cantidad="";
														for($i=0; $i < count($productos_pedido); $i++){ if($productos_pedido[$i]->idpim==$categoria_productos[$c]->idpim){ $existe=true; $cantidad=$productos_pedido[$i]->cantidad; break;} }
														if($existe){
														$color=$this->M_color->get($categoria_productos[$c]->idco);
												?>
														<tr>
															<td width="26%"><?php echo $color[0]->nombre;?><input type="hidden" value="<?php echo $categoria_productos[$c]->idpim;?>"></td>
															<td width="23%"><input type="number" class="form-control input-sm" id="cu<?php echo $categoria_productos[$c]->idpim;?>" placeholder="Costo por unidad" step="any" value="<?php echo $categoria_productos[$c]->costo_unitario;?>" disabled></td>
															<td width="23%"><input type="number" class="form-control input-sm" id="cant<?php echo $categoria_productos[$c]->idpim;?>" placeholder="Cantidad" name="cantidad" step="any" onclick="calcular_total_fila('<?php echo $categoria_productos[$c]->idpim;?>')" onkeyup="calcular_total_fila('<?php echo $categoria_productos[$c]->idpim;?>')" producto="<?php echo $categoria_productos[$c]->idpim;?>" value="<?php echo $cantidad;?>" min='0' max='9999999'></td>
															<td width="23%"><input type="number" class="form-control input-sm" id="tot<?php echo $categoria_productos[$c]->idpim;?>" placeholder="Costo total" step="any" disabled value="0.0"></td>
															<td width="5%"><?php $this->load->view('estructura/botones/botones_registros',['eliminar'=>"drop_fila_2n($(this));"]);?></td>
														</tr>
													<?php }// end if?>
												<?php }// end for ?>
													</tbody>
												</table>
											</td>
											<td width="3%"><?php $this->load->view('estructura/botones/botones_registros',['eliminar'=>"drop_fila($(this));"]);?></td>
										</tr>
										<?php }// end if ?>
										
									<?php }// end for ?>
										</thead>
									</table>
								</td>
								<td class="celda td"><?php $this->load->view('estructura/botones/botones_registros',['eliminar'=>"drop_fila($(this));"]);?></td>
							</tr>
						<?php } ?>
						</tbody>
						<thead><tr class="fila"><th class="celda text-right" colspan="3" class="text-right"><button class="btn btn-info" id="btn_add" onclick="add_row_producto($(this),'<?php echo $pedido->idcl;?>');"><span class='glyphicon glyphicon-plus'></span> Adicionar producto</button></th></tr></thead>
					</table>
				</div>
			</div>	 
		</div>
<script language='javascript'>Onfocus("p_nom");$('[data-toggle="popover"]').popover({html:true}); $("select[id*=prod]").change(function(){$(this).add_categorias()});</script>