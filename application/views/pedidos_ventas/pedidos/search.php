<table class="tabla tabla-border-false">
	<tr class="fila">
		<td class='celda celda-sm-6'>
			<form onsubmit="return view_pedido();"><input type="number" id="sp_num" placeholder='Numero de pedido' class="form-control input-sm" onkeyup="reset_input(this.id)"></form>
		</td>
		<td class='celda celda-sm' style="width:14%">
			<form onsubmit="return view_pedido();"><input type="number" id="sp_nom" placeholder='Nombre de pedido' class="form-control input-sm" onkeyup="reset_input(this.id)"></form>
		</td>
		<td class='celda celda-sm-20'>
			<select id="sp_cli" class="form-control input-sm" onchange="reset_input(this.id); view_pedido();">
				<option value="">Seleccionar...</option>
		<?php for ($i=0; $i < count($clientes); $i++) { $cliente=$clientes[$i]; ?>
				<option value="<?php echo $cliente->idcl;?>"><?php echo $cliente->nombre;?></option>
		<?php }//end for ?>
			</select>
		</td>
		<td class='celda celda-sm' style="width:45%"></td>
		<?php $new="new_pedido()"; if($privilegio[0]->mo1c!="1"){ $new="";}?>
		<?php $print="print()"; if($privilegio[0]->mo1p!="1"){ $print="";}?>
		<td class='celda text-right' style="width:13%"><?php $this->load->view('estructura/botones/buscador',['f_buscar'=>"view_pedido()",'f_ver'=>"all_pedido()",'nuevo'=>'Nuevo Pedido','f_nuevo'=>$new,'f_imprimir'=>$print]);?></td>
	</tr>
</table>