<?php
	$help1='title="<h4>Productos asigandos al cliente<h4>" data-content="Seleccione el producto a ser asigando al pedido, si desea adicionar un nuevo producto al cliente lo puede hacer accediendo por el menu principal izquierdo <b>Cliente/Proveedor</b>, o dando click en el boton <b>+</b>, y dirigirse a la parte de configuración de cliente."';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<tr class="fila" id="auto-num" stack="<?php echo $rows;?>">
	<td class="celda td" style="font-size:1.6em; font-weight:bold; text-align:center;"></td>
	<td class="celda td">
		<div class="input-group">
			<select class="form-control input-sm" id="prod<?php echo $rows;?>" cli="<?php echo $idcl;?>" posicion="<?php echo $rows;?>" >
				<option value="">Seleccione...</option>
			<?php 
			for ($i=0; $i < count($productos) ; $i++) { $producto=$productos[$i]; ?>
				<option value="<?php echo $producto->idpim;?>"><?php echo $producto->codigo; ?></option>
			<?php } ?>
			</select>
			<span class="input-group-addon input-sm" <?php echo $popover.$help1;?>><i class="glyphicon glyphicon-search"></i></span>
			<a href="<?php echo base_url();?>cliente_proveedor" target="_blank" title="Ver Clientes" class="input-group-addon input-sm"><i class="glyphicon glyphicon-plus"></i></a>
			<span class="input-group-addon input-sm" <?php echo $popover.$help1;?>><i class="glyphicon glyphicon-info-sign"></i></span>
		</div>
		<table class="table table-bordered">
			<thead id="categorias<?php echo $rows;?>"></thead>
		</table>
	</td>
	<td class="celda td">
		<?php $this->load->view('estructura/botones/botones_registros',['eliminar'=>"drop_fila($(this));"]);?>
		<script language='javascript'>Onfocus("prod<?php echo $rows;?>");$('[data-toggle="popover"]').popover({html:true}); $("#prod<?php echo $rows;?>").change(function(){$(this).add_categorias()});</script>
	</td>
</tr>