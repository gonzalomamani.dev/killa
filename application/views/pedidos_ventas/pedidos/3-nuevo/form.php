<?php
	$help1='title="<h4>Número de pedido<h4>" data-content="Representa al número de referencia del pedido, este numero es unico y generado automaticamente por el sistema, si fuese el caso de que el numero fuera ya registrado puede pulsar el boton actualizar el número."';
	$help15='title="<h4>Nombre de pedido<h4>" data-content="Representa al nombre del pedido, el contenido debe ser en formato alfanumérico <b>de 3 a 150 caracteres <b>puede incluir espacios</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
	$help2='title="<h4>Cliente<h4>" data-content="Seleccione el cliente que realiza el pedido, si desea adicionar un nuevo cliente lo puede hacer accediendo por el menu principal izquierdo <b>Cliente/Proveedor</b>, o dando click en el boton <b>+</b>"';
	$help3='title="<h4>Fecha de pedido<h4>" data-content="Ingrese la fecha de registro de este pedido."';
	$help4='title="<h4>Observaciónes del pedido<h4>" data-content="Ingrese algunas observaciónes del pedido si tuviera. El contenido de las observaciones debe ser en formato alfanumérico <b>hasta 900 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
		<div class="row"><div class="col-sm-2 col-sm-offset-10 col-xs-12"><strong><span class='text-danger'>(*)</span>Campos obligatorio</strong></div></div>
		<div class="list-group">
		  <div class="list-group-item active">
		    <h5 class="list-group-item-heading" id="g_text">DETALLE DEL PEDIDO</h5>
		  </div>
		  <div class="list-group-item">
		  	<div class="row">
		  		<div class="col-md-1 col-sm-2 col-xs-12 text-left"><h5 class="list-group-item-heading"><strong><span class='text-danger'>(*)</span>N° de pedido:</strong></h5></div>
		  		<div class="col-md-3 col-sm-4 col-xs-12">
					<div class="input-group">
						<div class="form-control text-right" id="p_nro" disabled><?php echo $nro_pedido;?></div>
						<span class="btn input-group-addon input-sm" id="p_act"  title="Actualizar número de pedido" onclick="refresh_nro_orden();"><i class="glyphicon glyphicon-refresh"></i></span>
						<span class="input-group-addon input-sm" <?php echo $popover.$help1;?>><i class="glyphicon glyphicon-info-sign"></i></span>
					</div>
		  		</div>
		  		<div class="col-md-1 col-sm-2 col-xs-12 text-left"><h5 class="list-group-item-heading"><strong><span class='text-danger'>(*)</span>Nombre de pedido:</strong></h5></div>
		  		<div class="col-md-3 col-sm-4 col-xs-12">
		  			<div class="input-group">
						<input type="text" class="form-control input-sm" id="p_nom" placeholder="Nombre de pedido">
						<span class="input-group-addon input-sm" <?php echo $popover.$help15;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
		  		</div><i class='visible-sm-block clearfix'></i>
		  		<div class="col-md-1 col-sm-2 col-xs-12 text-left"><h5 class="list-group-item-heading"><strong><span class='text-danger'>(*)</span>Cliente:</strong></h5></div>
		  		<div class="col-md-3 col-sm-4 col-xs-12">
		  			<div class="input-group">
						<select id="p_cli" class="form-control input-sm">
							<option value="">Seleccionar...</option>
						<?php 
						for ($i=0; $i < count($clientes) ; $i++) { $cliente=$clientes[$i]; ?>
							<option value="<?php echo $cliente->idcl; ?>"><?php echo $cliente->nombre;?></option>
						<?php } ?>
						</select>
						<a href="<?php echo base_url();?>cliente_proveedor" target="_blank" title="Ver Clientes" class="input-group-addon input-sm"><i class="glyphicon glyphicon-plus"></i></a>
						<span class="input-group-addon input-sm" <?php echo $popover.$help2;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
		  		</div><i class='visible-lg-block visible-md-block clearfix'></i>
		  		<div class="col-md-1 col-sm-2 col-xs-12 text-left"><h5 class="list-group-item-heading"><strong><span class='text-danger'>(*)</span>Fecha pedido:</strong></h5></div>
		  		<div class="col-md-3 col-sm-4 col-xs-12">
		  			<div class="input-group">
						<input type="date" class="form-control input-sm" value="<?php echo date('Y-m-d');?>" id="p_fec">
						<span class="input-group-addon input-sm" <?php echo $popover.$help3;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>		  			
		  		</div><i class='visible-sm-block clearfix'></i>
		  		<div class="col-md-1 col-sm-2 col-xs-12 text-left"><h5 class="list-group-item-heading"><strong>Observaciónes:</strong></h5></div>
		  		<div class="col-md-7 col-sm-10 col-xs-12">
			      	<div class="input-group">
			      		<textarea class="form-control input-sm" id="p_obs" placeholder="Observaciónes de pedido"></textarea>
			      		<span class="input-group-addon input-sm" <?php echo $popover.$help4;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
		  		</div>
		  	</div>
		  </div>
			<div class="list-group-item">
				<div class="table-responsive">
					<table id="table-np" class="tabla tabla-border-true">
						<thead>
							<tr class="fila">
								<th class="celda th" width="5%">#Item</th>
								<th class="celda th" width="93%">Detalle de producto</th>
								<th class="celda th" width="2%"></th>
							</tr>
						</thead>
						<tbody id="content_protuct"></tbody>
						<thead><tr class="fila"><th class="celda th" colspan="3" class="text-right"><button class="btn btn-info" id="btn_add" onclick="add_row_producto($(this),'');"><span class='glyphicon glyphicon-plus'></span> Adicionar producto</button></th></tr></thead>
					</table>
				</div>
			</div>	  
		</div>
<!--<div class="list-group">
		  <div class="list-group-item active">
		    <h5 class="list-group-item-heading" id="g_text">DETALLE DE COSTO Y TIEMPO</h5>
		  </div>
		<div class="list-group-item">
		  	<div class="row">
				<div class="col-md-3 col-sm-6 col-xs-12">
					<h5 class="list-group-item-heading"><strong>Costo del pedido de según sistema (Bs.): </strong></h5>
		      		<div class="input-group">
						<input type="number" class="form-control input-sm" value="0" id="c_est" disabled>
						<span class="input-group-addon input-sm" <?php echo $popover.$help6;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<h5 class="list-group-item-heading"><strong>Costo de pedido (Bs.)</strong></h5>
		      		<div class="input-group">
		      			<input type="number" class="form-control input-sm" value="0" id="c_cos" onkeyup="actualizar_costo_tiempo()" onchange="actualizar_costo_tiempo()">
		      			<span class="input-group-addon input-sm" <?php echo $popover.$help7;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<h5 class="list-group-item-heading"><strong>Descuento (Bs.)</strong></h5>
		      		<div class="input-group">
		      			<input type="number" class="form-control input-sm" value="0" id="c_des" onkeyup="actualizar_costo_tiempo()" onchange="actualizar_costo_tiempo()">
		      			<span class="input-group-addon input-sm" <?php echo $popover.$help8;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<h5 class="list-group-item-heading"><strong>Costo final del pedido (Bs.)</strong></h5>
		      		<div class="input-group">
		      			<input type="number" class="form-control input-sm" value="0" id="c_tot" disabled>
		      			<span class="input-group-addon input-sm" <?php echo $popover.$help9;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
				</div>
		  	</div>
		  	<div class="row">
				<div class="col-md-3 col-sm-6 col-xs-12">
					<h5 class="list-group-item-heading"><strong>Tiempo de Producción estimado</strong></h5>
		      		<div class="input-group">
		      			<input type="text" class="form-control input-sm" value="0" id="tiempo" disabled>
		      			<input type="hidden" id="segundos">
		      			<span class="input-group-addon input-sm" <?php echo $popover.$help10;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<h5 class="list-group-item-heading"><strong>Fecha de inicio</strong></h5>
		      		<div class="input-group">
						<input type="date" class="form-control input-sm" value="<?php echo date('Y-m-d');?>" id="fini" onchange="calcular_costo_tiempo()">
		      			<span class="input-group-addon input-sm" <?php echo $popover.$help11;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<h5 class="list-group-item-heading"><strong>Fecha estimada de entrega</strong></h5>
		      		<div class="input-group">
						<input type="date" class="form-control input-sm" value="<?php echo date('Y-m-d');?>" id="fest" disabled>
		      			<span class="input-group-addon input-sm" <?php echo $popover.$help12;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<h5 class="list-group-item-heading"><strong>Fecha de Entrega</strong></h5>
		      		<div class="input-group">
						<input type="date" class="form-control input-sm" value="<?php echo date('Y-m-d');?>" id="fent">
		      			<span class="input-group-addon input-sm" <?php echo $popover.$help13;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
				</div>
		  	</div>
		  	<div class="row">
		  		<div class="col-md-2 col-md-offset-10 col-sm-3 col-sm-offset-9 col-xs-12">
					<button class="btn btn-info col-xs-12" onclick="calcular_costo_tiempo();"><span class='icon-calculator2'></span> Calcular</button>
				</div>
		  	</div>
		</div>
	</div>-->
<script language='javascript'>Onfocus("p_nom");$('[data-toggle="popover"]').popover({html:true});</script>