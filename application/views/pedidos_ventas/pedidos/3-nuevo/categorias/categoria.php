<?php if(count($productos)>0){ 
		$url=base_url().'libraries/img/pieza_productos/';
?>
<table class="table table-bordered table-striped table-hover">
	<tbody>
<?php for($i=0; $i < count($productos) ; $i++) { $producto=$productos[$i];
		$img="default.png";
		$categoria_productos=$this->M_categoria_producto->get_colores_producto_cliente($producto->idp,$idcl);
		if(count($categoria_productos)>0){
			for ($j=0; $j < count($categoria_productos); $j++) { 
				if($categoria_productos[$j]->portada=="1"){
					$image=$this->M_imagen_producto->get_pieza_material($categoria_productos[$j]->idpim); 
					if(count($image)>0){
						$img=$image[0]->nombre;
					}
				}
			}
		}
?>
		<tr>
			<td class="g-thumbnail-modal"><div class="g-thumbnail-modal"></div>
				<a href="javascript:" onclick="fotografia('<?php echo $url.$img;?>')"><img src="<?php echo $url.'miniatura/'.$img;?>" width='100%' class='img-thumbnail g-thumbnail-modal'></a>
			</td>
			<td width="20%"><?php echo $producto->nombre." - <b>".$producto->nombre_grupo."</b>";?></td>
			<td width="77%">
				<table class="table table-bordered table-striped table-hover">
					<tbody>
				<?php for ($c=0; $c < count($categoria_productos) ; $c++) { 
					$color=$this->M_color->get($categoria_productos[$c]->idco);
				?>
						<tr>
							<td width="26%"><?php echo $color[0]->nombre;?><input type="hidden" value="<?php echo $categoria_productos[$c]->idpim;?>"></td>
							<td width="23%"><input type="number" class="form-control input-sm" id="cu<?php echo $categoria_productos[$c]->idpim;?>" placeholder="Costo por unidad" step="any" value="<?php echo $categoria_productos[$c]->costo_unitario;?>" disabled></td>
							<td width="23%"><input type="number" class="form-control input-sm" id="cant<?php echo $categoria_productos[$c]->idpim;?>" placeholder="Cantidad" name="cantidad" step="any" onclick="calcular_total_fila('<?php echo $categoria_productos[$c]->idpim;?>')" onkeyup="calcular_total_fila('<?php echo $categoria_productos[$c]->idpim;?>')" producto="<?php echo $categoria_productos[$c]->idpim;?>"></td>
							<td width="23%"><input type="number" class="form-control input-sm" id="tot<?php echo $categoria_productos[$c]->idpim;?>" placeholder="Costo total" step="any" disabled value="0.0"></td>
							<td width="5%"><?php $this->load->view('estructura/botones/botones_registros',['eliminar'=>"drop_fila_2n($(this));"]);?></td>
						</tr>
			<?php }?>
					</tbody>
				</table>
			</td>
			<td width="3%"><?php $this->load->view('estructura/botones/botones_registros',['eliminar'=>"drop_fila($(this));"]);?></td>
		</tr>
<?php }?>
	</tbody>
</table>
<?php } ?>