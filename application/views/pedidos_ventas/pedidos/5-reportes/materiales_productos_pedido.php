<?php $pedido=$pedido[0];
	$contPagina=1;
	$contReg=0;?>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="reportes_pedido('<?php echo $pedido->idpe; ?>')">Detalle</a></li>
  <li role="presentation"><a href="javascript:" onclick="productos_pedido('<?php echo $pedido->idpe; ?>')">Productos</a></li>
  <li role="presentation"><a href="javascript:" onclick="materiales_pedido('<?php echo $pedido->idpe; ?>')">Materiales</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="materiales_productos_pedido('<?php echo $pedido->idpe; ?>')">Materiales por producto</a></li>
</ul>
<div class="detalle" id="area">

	<div class="encabezado_pagina">
		<table class="tabla tabla-border-false"><tr class="fila">
			<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
			<td class="celda td" width="88%"><div class='encabezado_titulo'>MATERIALES POR PRODUCTO EN EL PEDIDO</div></td>
			<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
				<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
				<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
				<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'>1</span>
			</td></tr>
		</table>
	</div>
<table class="tabla tabla-border-true">
		<tr class="fila">
			<th class="celda td" style="font-size:.9em;" colspan="1">Nº DE PEDIDO:</th>
			<td class="celda td" colspan="5" style="font-size:.9em;"><?php echo $pedido->numero;?></td>
		</tr>
	<?php 
		for($i=0; $i < count($detalle_pedidos); $i++) { $categoria_producto=$detalle_pedidos[$i];
			$prod=$this->M_categoria_pieza->get_categoria_pieza_producto($categoria_producto->idcp);
			$producto=$prod[0];
			$categoria=$this->M_categoria_producto->get($categoria_producto->idpim);
			$mat=$this->M_material->get($categoria[0]->idm);
			$color=$this->M_color->get($mat[0]->idco);
	?>
	<tr class='fila'><th class='celda th' colspan="6"><?php echo $producto->nombre.' - '.$color[0]->nombre;?></th></tr>
	<tr class='fila'>
		<th class='celda th' width="5%">#Item</th>
		<th class='celda th' width="43%">Material/Accesorio</th>
		<th class='celda th' width="13%">Cantidad Por Unidad</th>
		<th class='celda th' width="13%">Costo por Unidad (Bs.)</th>
		<th class='celda th' width="13%">Cantidad por <?php echo $categoria_producto->cantidad;?> Unidades</th>
		<th class='celda th' width="13%">Costo por <?php echo $categoria_producto->cantidad;?> Unidades (Bs.)</th>
	</tr>
	<?php
		$item=1;
		$suma_costo_unidad=0;
		$suma_costo_pedido=0;
		$materiales=$this->M_producto->get_material($producto->idp);
		for($m=0; $m < count($materiales) ; $m++){ $material_cuero=$materiales[$m];
			if($material_cuero->idpim==$categoria_producto->idpim || $material_cuero->color_producto==0){//para sacar los cueros y el color del producto	
				$ma=$this->M_material->get($material_cuero->idm);
				$material=$ma[0];
				$unidad=$this->M_unidad->get($material->idu);
				$color=$this->M_color->get($material->idco);
				/*Por Unidad*/
				$cantidad_unidad=$material_cuero->area/$unidad[0]->equivalencia;
				$costo_unidad=$cantidad_unidad*$material->costo_unitario;
				/*Por la cantidad pedida*/
				$cantidad_pedido=$cantidad_unidad*$categoria_producto->cantidad;
				$costo_pedido=$cantidad_pedido*$material->costo_unitario;

				$suma_costo_unidad+=number_format($costo_unidad,1,'.','');
				$suma_costo_pedido+=number_format($costo_pedido,1,'.','');
				?>
				<tr class='fila'>
					<td class='celda td'><?php echo $item++;?></td>
					<td class='celda td'><?php echo $material->nombre.' - '.$color[0]->nombre;?></td>
					<td class='celda td'><?php echo number_format($cantidad_unidad,4,'.',',').' '.$unidad[0]->abreviatura;?></td>
					<td class='celda td'><?php echo number_format($costo_unidad,1,'.',',');?></td>
					<td class='celda td'><?php echo number_format($cantidad_pedido,4,'.',',').' '.$unidad[0]->abreviatura; ?></td>
					<td class='celda td'><?php echo number_format($costo_pedido,1,'.',',');?></td>
				</tr>
	<?php
			}
		}
	?>
	<?php 
		$accesorios=$this->M_producto_material->get_where_producto_materiales($producto->idp);
		for($m=0; $m<count($accesorios) ; $m++) { $accesorio=$accesorios[$m]; 
			$unidad=$this->M_unidad->get($accesorio->idu);
			$color=$this->M_color->get($accesorio->idco);
			/*Por Unidad*/
			$cantidad_unidad=$accesorio->cantidad;
			$costo_unidad=$cantidad_unidad*$accesorio->costo_unitario;
			/*Por la cantidad pedida*/
			$cantidad_pedido=$cantidad_unidad*$categoria_producto->cantidad;
			$costo_pedido=$cantidad_pedido*$accesorio->costo_unitario;
			
			$suma_costo_unidad+=number_format($costo_unidad,1,'.','');
			$suma_costo_pedido+=number_format($costo_pedido,1,'.','');
			?>
			<tr class='fila'>
				<td class='celda td'><?php echo $item++;?></td>
				<td class='celda td'><?php echo $accesorio->nombre.' - '.$color[0]->nombre;?></td>
				<td class='celda td'><?php echo number_format($cantidad_unidad,4,'.',',').' '.$unidad[0]->abreviatura;?></td>
				<td class='celda td'><?php echo number_format($costo_unidad,1,'.',',');?></td>
				<td class='celda td'><?php echo number_format($cantidad_pedido,4,'.',',').' '.$unidad[0]->abreviatura; ?></td>
				<td class='celda td'><?php echo number_format($costo_pedido,1,'.',',');?></td>
			</tr>
			<?php
		} 
	?>
	<?php 
		$materiales_liquidos=$this->M_producto->get_material_liquido($producto->idp);
		for($m=0; $m < count($materiales_liquidos) ; $m++){ $material_adicional=$materiales_liquidos[$m];
			$ma=$this->M_material->get($material_adicional->idm);
			$material=$ma[0];
			$unidad=$this->M_unidad->get($material->idu);
			/*Por Unidad*/
			$cantidad_unidad=$material_adicional->area*$material_adicional->variable/$unidad[0]->equivalencia;
			$costo_unidad=$cantidad_unidad*$material->costo_unitario;
			/*Por la cantidad pedida*/
			$cantidad_pedido=$cantidad_unidad*$categoria_producto->cantidad;
			$costo_pedido=$cantidad_pedido*$material->costo_unitario;

			$suma_costo_unidad+=number_format($costo_unidad,1,'.','');
			$suma_costo_pedido+=number_format($costo_pedido,1,'.','');
			?>
			<tr class='fila'>
				<td class='celda td'><?php echo $item++;?></td>
				<td class='celda td'><?php echo $material->nombre.' - '.$color[0]->nombre;?></td>
				<td class='celda td'><?php echo number_format($cantidad_unidad,4,'.',',').' '.$unidad[0]->abreviatura;?></td>
				<td class='celda td'><?php echo number_format($costo_unidad,1,'.',',');?></td>
				<td class='celda td'><?php echo number_format($cantidad_pedido,4,'.',',').' '.$unidad[0]->abreviatura; ?></td>
				<td class='celda td'><?php echo number_format($costo_pedido,1,'.',',');?></td>
			</tr>
	<?php
		}
	?>
	<tr class="fila">
		<th class="celda th" colspan="3">Costo Total Materiales/Accesorios (Bs.)</th>
		<th class="celda th"><?php echo $suma_costo_unidad;?></th>
		<th class="celda th"></th>
		<th class="celda th"><?php echo $suma_costo_pedido;?></th>
	</tr>
	<?php }?>
</table>
</div>