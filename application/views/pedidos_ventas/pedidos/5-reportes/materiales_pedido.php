<?php $pedido=$pedido[0];
	$contPagina=1;
	$contReg=0;
	$url=base_url().'libraries/img/materiales/miniatura/';
	//CALCULANDO EL MATERIAL NECESARIO
			$v_material=array();//Guarda los materiales necesarios por producto
			$v_cantidad=array();//Guardar la cantidad de producto en el pedido
			$ctas_material=array();//Guarda el nombre y el id para agrupar en unicos y ordenar por nombre
			for($i=0; $i < count($detalle_pedidos); $i++) { $producto=$detalle_pedidos[$i];
				$accesorios=$this->M_producto->get_accesorios($producto->idp);
				$materiales=$this->M_producto->get_materiales($producto->idp);
				$materiales_liquidos=$this->M_producto->get_materiales_liquidos($producto->idp);
				$j_materiales=$this->lib->costo_materiales($accesorios,$materiales,$materiales_liquidos,NULL,$producto->idpim);
				$v_material[count($v_material)]=$j_materiales;
				$auxiliar=json_decode($j_materiales);
				foreach($auxiliar as $valor => $material){
					$ctas_material[count($ctas_material)]=trim($material->nombre)."[|]".trim($material->idmi);//[|]=>separador de elementos
				}
				$v_cantidad[count($v_cantidad)]=$producto->cantidad;
			}//end for

			//hallado totales producto
			$ctas_m=$this->lib->array_unico($ctas_material,true);
			$materiales_total= array();
			for($i=0;$i<count($ctas_m);$i++){
				$idmi="";$img="";$cod="";$nom="";$col="";$uni="";$can="";$cos=0;
				for($m=0;$m<count($v_material);$m++){
					$j_materiales=json_decode($v_material[$m]);
					$can_pro=0;$cos_pro=0;
					foreach ($j_materiales as $clave => $material){
						if($material->idmi==$ctas_m[$i]){
							$idmi=$material->idmi;
							$img=$material->fotografia;
							$cod=$material->codigo;
							$nom=$material->nombre;
							$col=$material->color;
							$uni=$material->abreviatura;
							$can_pro+=$material->cantidad;
							$cos_pro+=($material->costo_unitario*$material->cantidad);
						}
					}
					$can+=($can_pro*$v_cantidad[$m]);
					$cos+=($cos_pro*$v_cantidad[$m]);
				}
				$materiales_total[count($materiales_total)]=array('idmi' => $idmi,'imagen' => $img,'codigo' => $cod,'nombre' => $nom,'color' => $col,'unidad' => $uni,'cantidad' => $can,'costo' => $cos);
			}
	//CALCULANDO MATERIAL FALTANTE
	$j_materiales=json_decode(json_encode($materiales_total));
	$v_faltante=Array();
	foreach ($j_materiales as $clave => $material) {
		for ($i=0; $i < count($materiales_all) ; $i++) {
			if($materiales_all[$i]->idmi==$material->idmi){
				if($materiales_all[$i]->cantidad<$material->cantidad){
					$cantidad=$material->cantidad-$materiales_all[$i]->cantidad;
					$costo=number_format((($cantidad*$material->costo)/$material->cantidad),1,'.','');
					$v_faltante[count($v_faltante)]=array('idmi' => $material->idmi,'imagen' => $material->imagen,'codigo' => $material->codigo,'nombre' => $material->nombre,'color' => $material->color,'unidad' => $material->unidad,'cantidad' => $cantidad,'costo' => $costo);
				}
				break;
			}
		}
	}
?>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="reportes_pedido('<?php echo $pedido->idpe; ?>')">Detalle</a></li>
  <li role="presentation"><a href="javascript:" onclick="productos_pedido('<?php echo $pedido->idpe; ?>')">Productos</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="materiales_pedido('<?php echo $pedido->idpe; ?>')">Materiales</a></li>
  <li role="presentation"><a href="javascript:" onclick="materiales_productos_pedido('<?php echo $pedido->idpe; ?>')">Materiales por producto</a></li>
</ul>
<table class="tabla tabla-border-true">
	<tr class="fila">
		<th class="celda th" width="5%">imagen</th>
		<th class="celda th" width="8%"><input type="checkbox" id="1" <?php if(!isset($v1)){ echo "checked";}?>></th>
		<th class="celda th" width="12%">Nº Reg.</th>
		<th class="celda th" width="20%">
			<select id="nro" class="form-control input-sm">
				<?php for ($i=1; $i <= 50 ; $i++) { ?>
					<option value="<?php echo $i;?>" <?php if($i==$nro){echo "selected";}?>><?php echo $i;?></option>
				<?php } ?>
				</select>
		</th>
		<th class="celda th celda-sm" width="55%"></th>
	</tr>
</table><hr>
<div id="area">
<div class="table-responsive pagina">
	<div class="encabezado_pagina">
		<table class="tabla tabla-border-false"><tr class="fila">
			<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
			<td class="celda td" width="88%"><div class='encabezado_titulo'>MATERIALES EN EL PEDIDO</div></td>
			<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
				<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
				<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
				<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'></span>
			</td></tr>
		</table>
	</div>
	<table border="0" class="tabla tabla-border-true">
		<tr class="fila">
			<th class="celda td" style="font-size:.9em;" colspan="3">Nº DE PEDIDO:</th>
			<td class="celda td" colspan="3" style="font-size:.9em;"><?php echo $pedido->numero;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th" colspan="6">MATERIALES REQUERIDOS</th>
		</tr>
		<tr class="fila">
			<th class="celda th" width="5%">#</th>
			<?php if(!isset($v1)){ ?><th class="celda th" width="7%"><div class="g-thumbnail"></div></th><?php } ?>
			<th class="celda th" width="13%">Código</th>
			<th class="celda th" width="50%">Materiales y Accesorios</th>
			<th class="celda th" width="15%">Cantidad</th>
			<th class="celda th" width="10%">Costo (Bs.)</th>
		</tr>
		<?php
			$suma_total=0;
			$i=1;
			$j_materiales=json_decode(json_encode($materiales_total));
			foreach ($j_materiales as $clave => $material){
				$costo=number_format($material->costo,1,'.','');
				$suma_total+=$costo;
				$cantidad=number_format($material->cantidad,5,'.','');
				$img="default.png";
				if($material->imagen!="" && $material->imagen!=NULL){ $img=$material->imagen;}
		?>
		<?php if($contReg>=$nro){
					$contReg=0;
					$contPagina++;
			?>
			</table>
			</div>
		<div class="table-responsive pagina">
			<div class="encabezado_pagina">
				<table class="tabla tabla-border-false"><tr class="fila">
					<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
					<td class="celda td" width="88%"><div class='encabezado_titulo'>MATERIALES EN EL PEDIDO</div></td>
					<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
						<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
						<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
						<b>Página: </b><?php echo $contPagina.' de ';?><span class='nroPagina'></span>
					</td></tr>
				</table>
			</div>
			<table border="0" class="tabla tabla-border-true">
				<tr class="fila">
					<th class="celda td" style="font-size:.9em;" colspan="3">Nº DE PEDIDO:</th>
					<td class="celda td" colspan="3" style="font-size:.9em;"><?php echo $pedido->numero;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th" colspan="6">MATERIALES REQUERIDOS</th>
				</tr>
				<tr class="fila">
					<th class="celda th" width="5%">#</th>
					<?php if(!isset($v1)){ ?><th class="celda th" width="7%"><div class="g-thumbnail"></div></th><?php } ?>
					<th class="celda th" width="13%">Código</th>
					<th class="celda th" width="50%">Materiales y Accesorios</th>
					<th class="celda th" width="15%">Cantidad</th>
					<th class="celda th" width="10%">Costo (Bs.)</th>
				</tr>
			<?php } // end if?>
			<tr class="fila">
				<td class="celda td"><?php echo $i++;?></td>
				<?php if(!isset($v1)){ ?><td class="celda td"><img src="<?php echo $url.$img;?>" width="100%" class="g-thumbnail"></td><?php } ?>
				<td class="celda td"><?php echo $material->codigo;?></td>
				<td class="celda td"><?php echo $material->nombre.' - '.$material->color;?></td>
				<td class="celda td"><?php echo number_format($cantidad,5,'.',',').' '.$material->unidad;?></td>
				<td class="celda td"><?php echo number_format($costo,2,'.',',');?></td>
			</tr>
		<?php $contReg++;}//end for ?>
		<tr class="fila">
			<th class="celda th" colspan="<?php if(!isset($v1)){ echo "5";}else{ echo "4";} ?>">COSTO TOTAL DE MATERIALES REQUERIDOS (Bs.) </th>
			<th class="celda th"><?php echo number_format($suma_total,1,'.',',');?></th>
		</tr>
	</table>
</div>
<?php $contReg=0;
	if(!empty($v_faltante)){
		$contPagina++;
?>
<div class="table-responsive pagina">
	<div class="encabezado_pagina">
		<table class="tabla tabla-border-false"><tr class="fila">
			<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
			<td class="celda td" width="88%"><div class='encabezado_titulo'>MATERIALES EN EL PEDIDO</div></td>
			<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
				<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
				<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
				<b>Página: </b><?php echo $contPagina.' de ';?><span class='nroPagina'></span></div>
			</td></tr>
		</table>
	</div>
	<table border="0" class="tabla tabla-border-true">
		<tr class="fila">
			<th class="celda th" colspan="6">MATERIALES INSUFICIENTES SEGÚN ALMACEN</th>
		</tr>
		<tr class="fila">
			<th class="celda th" width="5%">#</th>
			<?php if(!isset($v1)){ ?><th class="celda th" width="7%"><div class="g-thumbnail"></div></th><?php } ?>
			<th class="celda th" width="13%">Código</th>
			<th class="celda th" width="50%">Materiales y Accesorios</th>
			<th class="celda th" width="15%">Cantidad</th>
			<th class="celda th" width="10%">Costo (Bs.)</th>
		</tr>
	<?php
		$suma_total=0;
		$j_faltante=json_decode(json_encode($v_faltante));
		$i=1;
		foreach ($j_faltante as $valor => $material){
			$costo=number_format($material->costo,1,'.','');
			$cantidad=number_format($material->cantidad,5,'.','');
			$img="default.png";
			if($material->imagen!="" && $material->imagen!=NULL){ $img=$material->imagen;}
	?>
	<?php if($contReg>=$nro){
			$contReg=0;
			$contPagina++;
	?>
	</table>
</div>
<div class="table-responsive pagina">
	<div class="encabezado_pagina">
		<table class="tabla tabla-border-false"><tr class="fila">
			<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
			<td class="celda td" width="88%"><div class='encabezado_titulo'>MATERIALES EN EL PEDIDO</div></td>
			<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
				<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
				<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
				<b>Página: </b><?php echo $contPagina.' de ';?><span class='nroPagina'></span></div>
			</td></tr>
		</table>
	</div>
	<table border="0" class="tabla tabla-border-true">
		<tr class="fila">
			<th class="celda th" colspan="6">MATERIALES INSUFICIENTES SEGÚN ALMACEN</th>
		</tr>
		<tr class="fila">
			<th class="celda th" width="5%">#</th>
			<?php if(!isset($v1)){ ?><th class="celda th" width="7%"><div class="g-thumbnail"></div></th><?php } ?>
			<th class="celda th" width="13%">Código</th>
			<th class="celda th" width="50%">Materiales y Accesorios</th>
			<th class="celda th" width="15%">Cantidad</th>
			<th class="celda th" width="10%">Costo (Bs.)</th>
		</tr>
	<?php } // end if?>
			<tr class="fila">
				<td class="celda td"><?php echo $i++;?></td>
				<?php if(!isset($v1)){ ?><td class="celda td"><img src="<?php echo $url.$img;?>" width="100%" class="g-thumbnail"></td><?php } ?>
				<td class="celda td"><?php echo $material->codigo;?></td>
				<td class="celda td"><?php echo $material->nombre.' - '.$material->color;?></td>
				<td class="celda td"><?php echo number_format($cantidad,5,'.',',').' '.$material->unidad;?></td>
				<td class="celda td"><?php echo number_format($costo,2,'.',',');?></td>
			</tr>
			<?php
			$suma_total+=$costo;
	$contReg++;}// end for ?>
		<tr class="fila">
			<th class="celda th" colspan="<?php if(!isset($v1)){ echo "5";}else{ echo "4";} ?>">COSTO TOTAL MATERIALES INSUFICIENTES (Bs.)</th>
			<th class="celda th"><?php echo number_format($suma_total,1,'.',',')  ;?></th>
		</tr>
	</table>
</div>
<?php } // end if?>
</div>
<script language="javascript">
	$("#1").unbind("click");$("#1").change(function(){ materiales_pedido_imagen('<?php echo $pedido->idpe;?>'); });
	$("#nro").unbind("click");$("#nro").change(function(){ materiales_pedido_imagen('<?php echo $pedido->idpe;?>'); });
	$(".nroPagina").html(<?php echo $contPagina;?>);
</script>