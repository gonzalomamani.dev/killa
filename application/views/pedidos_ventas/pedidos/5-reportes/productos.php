<ul class="nav nav-tabs">
  	<li role="presentation"><a href="javascript:" onclick="reportes_pedido('<?php echo $idpe;?>')">Materiales</a></li>
 	<li role="presentation" class="active"><a href="javascript:" onclick="productos_pedido('<?php echo $idpe;?>')">Productos</a></li>
</ul>
<div id="area">
	<?php $this->load->view('estructura/print/encabezado',['titulo'=>'PRODUCTOS','pagina'=>'1','sub_titulo'=> '']);?>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<th class="celda th" width="10%">#Item</th>
			<th class="celda th" width="17%">Código</th>
			<th class="celda th" width="43%">Nombre de producto</th>
			<th class="celda th" width="17%">Color</th>
			<th class="celda th" width="13%">Cantidad[u.]</th>
		</tr>
	<?php for($i=0; $i < count($productos); $i++){ $producto=$productos[$i]; ?>
		<tr class="fila">
			<td class="celda td"><?php echo $i+1;?></td>
			<td class="celda td"><?php echo $producto->codigo;?></td>
			<td class="celda td"><?php echo $producto->nombre." - ".$producto->nombre_grupo;?></td>
			<td class="celda td"><?php echo $producto->nombre_color;?></td>
			<td class="celda td"><?php echo $producto->cantidad_pedida;?></td>
		</tr>
	<?php } ?>
	</table>
</div>