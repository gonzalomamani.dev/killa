<ul class="nav nav-tabs">
  	<li role="presentation" class="active"><a href="javascript:" onclick="reportes_pedido('<?php echo $idpe;?>')">Materiales</a></li>
 	<li role="presentation"><a href="javascript:" onclick="productos_pedido('<?php echo $idpe;?>')">Productos</a></li>
</ul>
<div id="area">
	<?php $this->load->view('estructura/print/encabezado',['titulo'=>'MATERIAL REQUERIDO','pagina'=>'1','sub_titulo'=> '']);?>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<th class="celda th" width="10%">#Item</th>
			<th class="celda th" width="57%">Material</th>
			<th class="celda th" width="20%">Color</th>
			<th class="celda th" width="13%">Cantidad[u.]</th>
		</tr>
	<?php 
	$vidm=array();
		for ($i=0; $i < count($materiales) ; $i++) { $vidm[]=$materiales[$i]->idm; }
		$vidm=array_unique($vidm);
		$productos=array();
		foreach ($vidm as $key => $idm) {
			$cantidad=0;
			$idco="";
			$idu="";
			$nombre="";
			for ($i=0; $i < count($materiales) ; $i++) {
				if($idm==$materiales[$i]->idm){
					$cantidad+=($materiales[$i]->cantidad_requerida*$materiales[$i]->cantidad_pedida);
					$idco=$materiales[$i]->idco;
					$idu=$materiales[$i]->idu;
					$nombre=$materiales[$i]->nombre_material;
				}
			}
			if($idco!="" && $idu!="" && $nombre!=""){
				$color=$this->M_color->get($idco);
				$unidad=$this->M_unidad->get($idu);
				if(!empty($color) && !empty($unidad)){
					$productos[]=array("idm" => $idm,"nombre_material" => $nombre,"nombre_color" => $color[0]->nombre,"cantidad" => $cantidad,"unidad" => $unidad[0]->nombre,"abreviatura" => $unidad[0]->abreviatura);
				}
			}
		}
		$productos=json_decode(json_encode($productos));
	?>
	<?php $i=1; foreach ($productos as $key => $producto) {?>
		<tr class="fila">
			<td class="celda td"><?php echo $i++;?></td>
			<td class="celda td"><?php echo $producto->nombre_material;?></td>
			<td class="celda td"><?php echo $producto->nombre_color;?></td>
			<td class="celda td"><?php echo $producto->cantidad." ".$producto->abreviatura.".";?></td>
		</tr>
	<?php }?>
	</table>
</div>