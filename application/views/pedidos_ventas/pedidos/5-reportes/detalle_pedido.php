<?php $pedido=$pedido[0];
	$contPagina=1;
?>
<ul class="nav nav-tabs">
  <li role="presentation" class="active"><a href="javascript:" onclick="reportes_pedido('<?php echo $pedido->idpe; ?>')">Detalle</a></li>
  <li role="presentation"><a href="javascript:" onclick="productos_pedido('<?php echo $pedido->idpe; ?>')">Productos</a></li>
  <li role="presentation"><a href="javascript:" onclick="materiales_pedido('<?php echo $pedido->idpe; ?>')">Materiales</a></li>
  <li role="presentation"><a href="javascript:" onclick="materiales_productos_pedido('<?php echo $pedido->idpe; ?>')">Materiales por producto</a></li>
</ul>
<div class="table-responsive" id="area">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="88%"><div class='encabezado_titulo'>DETALLE DE PEDIDO</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Página: </b><?php echo $contPagina.' de 1'?></div>
				</td></tr>
			</table>
		</div>
	<table border="0" class="tabla tabla-border-true">
		<tr class="fila">
			<th class="celda td" style="font-size:.9em;">Nº DE PEDIDO:</th>
			<td class="celda td" colspan="3" style="font-size:.9em;"><?php echo $pedido->numero;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th" colspan="4">DATOS DE CLIENTE</th>
		</tr>
		<tr class="fila">
			<th class="celda th" width="20%">Nombre/Razon Social:</th><td class="celda td" width="30%"><?php echo $pedido->nombre;?></td>
			<th class="celda th" width="17%">NIT/CI:</th><td class="celda td" width="33%"><?php echo $pedido->nit;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Pais: </th><td class="celda td"><?php echo $pedido->pais;?></td>
			<th class="celda th">Ciudad: </th><td class="celda td"><?php echo $pedido->ciudad;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Telf/Cel: </th><td class="celda td"><?php echo $pedido->telefono;?></td>
			<th class="celda th">Email: </th><td class="celda td"><?php echo $pedido->email;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Dirección: </th><td class="celda td"><?php echo $pedido->direccion;?></td>
			<th class="celda th">Sitio Web: </th><td class="celda td"><?php echo $pedido->url;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Encargado: </th><td class="celda td"><?php echo $pedido->encargado;?></td>
			<th class="celda th">Observaciónes: </th><td class="celda td"><?php echo $pedido->caracteristicas;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th" colspan="4">DATOS DE PEDIDO</th>
		</tr>
		<tr class="fila">
			<th class="celda th">Fecha de Pedido: </th><td class="celda td"><?php echo $pedido->fecha_pedido;?></td>
			<th class="celda th">Fecha de Entrega: </th><td class="celda td"><?php echo $pedido->fecha_entrega;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Monto de pedido (Bs.): </th><td class="celda td"><?php echo number_format($pedido->monto_parcial,2,'.',',');?></td>
			<th class="celda th">Descuento (Bs.): </th><td class="celda td"><?php echo number_format($pedido->descuento,2,'.',',');?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Total Pedido (Bs.): </th><td class="celda td"><?php echo number_format($pedido->monto_total,2,'.',',');?></td>
			<?php 
				$c_productos=0;
				for($dp=0; $dp < count($detalle_pedidos); $dp++){ $c_productos+=($detalle_pedidos[$dp]->cantidad);}
			?>
			<th class="celda th">Cantidad de productos: </th><td class="celda td"><?php echo $c_productos.' Prod.';?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Estado de producción: </th><td class="celda td"><?php if($pedido->estado==0){echo "Pendiente";}
				if($pedido->estado==1){echo "En producción";}
				if($pedido->estado==2){echo "Entregado";}?></td>
			<?php 
				$t_pagos=0;
				for($pa=0; $pa < count($pagos); $pa++){ $t_pagos+=($pagos[$pa]->monto);}
			?>
			<th class="celda th">Estado de pagos: </th><td class="celda td"><?php 
				if($pedido->monto_total<$t_pagos){
					 echo "Cancelado +";
				}else{
					if($pedido->monto_total==$t_pagos){ 
						echo "Cancelado";
					 }else{
						if($t_pagos==0){
							echo "No cancelado";
						}else{
							echo "En proceso";
						}
					}
				}?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Observaciónes: </th><td class="celda td" colspan="3"><?php echo $pedido->observaciones;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th" colspan="4">DATOS DE PAGOS EN EL PEDIDO</th>
		</tr>
		<tr class="fila">
			<th class="celda th">#Pago</th>
			<th class="celda th">Fecha de pago</th>
			<th class="celda th">Monto cancelado</th>
			<th class="celda th">Observaciónes</th>
		</tr>
		<?php for ($i=0; $i < count($pagos) ; $i++) { $pago=$pagos[$i]; ?>
		<tr class="fila">
			<td class="celda td"><?php echo "Pago ".($i+1);?></td>
			<td class="celda td"><?php echo $pago->fecha;?></td>
			<td class="celda td"><?php echo number_format($pago->monto,2,'.',',');?></td>
			<td class="celda td"><?php echo $pago->observaciones;?></td>
		</tr>
		<?php } ?>
		<tr class="fila">
			<th class="celda th" colspan="2">Total Pagos(Bs.)</th>
			<th class="celda th"><?php echo number_format($t_pagos,2,'.',',');?></th>
			<th class="celda th"></th>
		</tr>
		<tr class="fila">
			<th class="celda th" colspan="4">DETALLE ECONÓMICO</th>
		</tr>
		<tr class="fila">
			<th class="celda th">Total Pedido (Bs.): </th><td class="celda td"><?php echo number_format($pedido->monto_total,2,'.',',');?></td>
			<th class="celda th">Total Pagos (Bs.): </th><td class="celda td"><?php echo number_format($t_pagos,2,'.',',');?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Saldo del Pedido (Bs.): </th><td class="celda td"><?php echo number_format($pedido->monto_total-$t_pagos,2,'.',',');?></td>
			<th class="celda th"></th><td class="celda td"></td>
		</tr>
	<!--<tr class="fila">
			<th class="celda th" colspan="6">DETALLE DE PRODUCTOS</td>
		</tr>
		<tr class="fila">
			<th class="celda th" width="5%">#Item</th>
			<th class="celda th" width="83%" colspan="4" >Producto</th>
			<th class="celda th" width="12%">Unidades Solicitada</th>
		</tr>
		<?php 
			for($i=0; $i < count($detalle_pedidos); $i++) { $categoria_producto=$detalle_pedidos[$i];
				$prod=$this->M_categoria_pieza->get_categoria_pieza_producto($categoria_producto->idcp);
				$producto=$prod[0];
				$categoria=$this->M_categoria_producto->get($categoria_producto->idpim);
				$mat=$this->M_material->get($categoria[0]->idm);
				$color=$this->M_color->get($mat[0]->idco);
				?>
				<tr class="fila">
					<td class="celda td"><?php echo $i+1;?></td>
					<td class="celda td" colspan="4"><?php echo $producto->nombre." - ".$color[0]->nombre;?></td>
					<td class="celda td"><?php echo  $categoria_producto->cantidad;?></td>
				</tr>
				<?php
			}//end for ?>
		<?php //CALCULANDO EL MATERIAL NECESARIO
			$v_material=array();
			$ctas_material=array();
			
		for($i=0; $i < count($detalle_pedidos); $i++) { $categoria_producto=$detalle_pedidos[$i];
			$prod=$this->M_categoria_pieza->get_categoria_pieza_producto($categoria_producto->idcp);
			$producto=$prod[0];
			$materiales=$this->M_producto->get_material($producto->idp);
			for ($m=0; $m < count($materiales) ; $m++){ $material_cuero=$materiales[$m];
				if($material_cuero->idpim==$categoria_producto->idpim || $material_cuero->color_producto==0){//para sacar los cueros y el color del producto	
					$ma=$this->M_material->get($material_cuero->idm);
					$material=$ma[0];
					$unidad=$this->M_unidad->get($material->idu);
					$color=$this->M_color->get($material->idco);
					$cantidad_material=($material_cuero->area/$unidad[0]->equivalencia)*$categoria_producto->cantidad;
					$ctas_material[count($ctas_material)]=trim($material->nombre)."[|]".trim($material->idm);//[|]=>separador de elementos
					$v_material[count($v_material)]=array(0=>$material->nombre.' - '.$color[0]->nombre,1=>''.$material->idm,2=>''.$cantidad_material,3=>$unidad[0]->nombre,4=>number_format($cantidad_material,4,'.','')*$material->costo_unitario);
				}// END IF
			}// END FOR
			$accesorios=$this->M_producto_material->get_where_producto_materiales($producto->idp);
			for($m=0; $m<count($accesorios) ; $m++) { $accesorio=$accesorios[$m]; $unidad=$this->M_unidad->get_col($accesorio->idu,'nombre');
				$color=$this->M_color->get($accesorio->idco);
				$cantidad_material=($accesorio->cantidad)*$categoria_producto->cantidad;
				$ctas_material[]=trim($accesorio->nombre)."[|]".trim($accesorio->idm);
				$v_material[]=array(0=>$accesorio->nombre.' - '.$color[0]->nombre,1=>''.$accesorio->idm,2=>''.($accesorio->cantidad)*$categoria_producto->cantidad,3=>$unidad[0]->nombre,4=>number_format($cantidad_material,4,'.','')*$material->costo_unitario);	
			}// end for
			$materiales_liquidos=$this->M_producto->get_material_liquido($producto->idp);
			for($m=0; $m < count($materiales_liquidos) ; $m++){ $material_adicional=$materiales_liquidos[$m];
				$ma=$this->M_material->get($material_adicional->idm);
				$material=$ma[0];
				$unidad=$this->M_unidad->get($material->idu);
				$cantidad_material=(($material_adicional->area*$material_adicional->variable)/$unidad[0]->equivalencia)*$categoria_producto->cantidad;
				$ctas_material[]=trim($material->nombre)."[|]".trim($material->idm);
				$v_material[]=array(0=>$material->nombre.' - '.$color[0]->nombre,1=>''.$material->idm,2=>''.$cantidad_material,3=>$unidad[0]->nombre,4=>number_format($cantidad_material,4,'.','')*$material->costo_unitario);	
			}// end for
		}//end for
		?>
		<tr class="fila">
			<th class="celda th" colspan="6">MATERIALES REQUERIDOS</td>
		</tr>
		<tr class="fila">
			<th class="celda th">#Item</th>
			<th class="celda th"colspan="3">Materiales y Accesorios</th>
			<th class="celda th">Cantidad</th>
			<th class="celda th">Costo (Bs.)</th>
		</tr>
		<?php 
			$ctas_m=$this->validaciones->array_unico($ctas_material,true);
			$v_faltante=Array();
			$suma_total=0;
		for($i=0;$i<count($ctas_m);$i++){
			$nombre="";
			$cantidad=0;
			$unidad="";
			$costo=0;
			for($j=0;$j<count($v_material);$j++){ $material=$v_material[$j];
			 	if($material[1]==$ctas_m[$i]){
			 		$nombre=$material[0];
			 		$cantidad+=$material[2];
			 		$costo+=$material[4];
			 		$unidad=$material[3];
			 	}
			 }
			
			$mat=$this->M_almacen_material->get_material_idm($ctas_m[$i]);
			$diferencia=number_format(($mat[0]->cantidad-$cantidad),4,'.',',');
			if($diferencia<0){ $diferencia*=-1;
				$v_faltante[]=array(0=>$nombre,1=>$diferencia,2=>$unidad,3=>($diferencia*$mat[0]->costo_unitario));
			}
			$costo=number_format($costo,1,'.','');
			$suma_total+=$costo;
		?>
			<tr class="fila">
				<td class="celda td"><?php echo $i+1;?></td>
				<td class="celda td" colspan="3"><?php echo $nombre;?></td>
				<td class="celda td"><?php echo number_format($cantidad,4,'.',',').' '.$unidad;?></td>
				<td class="celda td"><?php echo number_format($costo,1,'.',',');?></td>
			</tr>
		<?php
		}//end for
		?>
		<tr class="fila">
			<th class="celda th" colspan="5">COSTO TOTAL DE MATERIALES REQUERIDOS (Bs.) </th>
			<th class="celda th"><?php echo number_format($suma_total,1,'.',',');?></th>
		</tr>
		<tr class="fila">
			<th class="celda th" colspan="6">MATERIALES INSUFICIENTES SEGÚN ALMACEN</td>
		</tr>
		<tr class="fila">
			<th class="celda th">#Item</th>
			<th class="celda th"colspan="3">Materiales y Accesorios</th>
			<th class="celda th">Cantidad</th>
			<th class="celda th">Costo (Bs.)</th>
		</tr>
		<?php
		$suma_total=0;
		for ($m=0; $m < count($v_faltante) ; $m++) { $material=$v_faltante[$m]; 
			$costo=number_format($material[3],1,'.','');
			?>
			<tr class="fila">
				<td class="celda td"><?php echo $m+1;?></td>
				<td class="celda td" colspan="3"><?php echo $material[0];?></td>
				<td class="celda td"><?php echo number_format($material[1],4,'.',',').' '.$material[2];?></td>
				<td class="celda td"><?php echo number_format($costo,1,'.',','); ?></td>
			</tr>
			<?php
			$suma_total+=$costo;
		}
		?>
		<tr class="fila">
			<th class="celda th" colspan="5">COSTO TOTAL MATERIALES INSUFICIENTES (Bs.)</th>
			<th class="celda th"><?php echo number_format($suma_total,1,'.',',')  ;?></th>
		</tr>-->
	</table>
</div>