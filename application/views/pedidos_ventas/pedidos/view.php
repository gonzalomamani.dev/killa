<?php $j_pedidos=json_encode($pedidos);?>
<div class="table-responsive">
<table class="table table-striped table-bordered table-hover">
<tr><thead>
	<th width="5%">#Item</th>
	<th width="6%">Nº de<br>pedido</th>
	<th width="11%">Pedido</th>
	<th width="15%">Cliente</th>
	<th width="13%">Fecha de Pedido</th>
	<th width="9%">Total<br>costo (Bs.)</th>
	<th width="9%">Descuento<br>(Bs.)</th>
	<th width="9%">Pagos<br>(Bs.)</th>
	<th width="9%">Saldo<br>(Bs.)</th>
	<th width="4%">Estado<br>prod.</th>
	<th width="10%"></th>
</thead></tr>
<tbody>
<?php if(count($pedidos)>0){ ?>
		<?php for($i=0;$i<count($pedidos); $i++) { $pedido=$pedidos[$i]; ?>
			<tr>
			<td><?php echo $i+1;?></td>
			<td><?php echo $pedido->numero;?></td>
			<td><?php echo $pedido->nombre;?></td>
			<td><?php echo $pedido->nombre_cliente;?></td>
			<td><?php ?></td>
			<td class="text-right"><?php echo "0.0";?></td>
			<td class="text-right"><?php echo "0.0";?></td>
			<td class="text-right"><?php echo "0.0";?></td>
			<td class="text-right"><?php echo "0.0";?></td>
			<td>
				<?php if(true){?><span class="label label-danger label-lg">Pendiente</span><?php }?>
				<?php if(false){?><span class="label label-warning label-lg">En producción</span><?php }?>
				<?php if(false){?><span class="label label-success label-lg">Entregado</span><?php }?>
			</td>
			<td class="celda td text-right">
				<?php $mod="config_pedido('".$pedido->idpe."')"; if($privilegio[0]->mo1u!="1"){ $mod="";}?>
				<?php $eli="confirmar_pedido('".$pedido->idpe."')"; if($privilegio[0]->mo1d!="1"){ $eli="";}?>
				<?php $this->load->view('estructura/botones/botones_registros',['reportes'=>"reportes_pedido('".$pedido->idpe."')",'configuracion'=>$mod,'eliminar'=>$eli]);?>
			</td>
		</tr>
		<?php }?>
<?php }else{
		echo "<tr><td colspan='11'><h2>0 Registros encontrados...</h2></td></tr>";
	} ?>
	</tbody>
</table>
</div>
<script language="javascript" type="text/javascript"> 
	$("#print").removeAttr("onclick"); 
	$("#print").unbind("click"); 
	$("#print").click(function(){ imprimir_pedidos('<?php echo $j_pedidos;?>'); }); 
</script>