<?php 
	$pedidos=json_decode($pedidos);
	$contPagina=1;
	$contReg=0;
?>
<div class="pagina table-responsive">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="88%"><div class='encabezado_titulo'>REGISTRO DE PEDIDOS</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'></span></div>
				</td></tr>
			</table>
		</div>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php if(!isset($v1)){?><th class="celda th" width="3%" >#Item</th><?php } ?>
			<?php if(!isset($v2)){?><th class="celda th" width="3%" >Nº de pedido</th><?php } ?>
			<?php if(!isset($v3)){?><th class="celda th" width="7%">Fecha de pedido</th><?php } ?>
			<?php if(!isset($v4)){?><th class="celda th" width="7%" >Fecha en entrega</th><?php } ?>
			<?php if(!isset($v5)){?><th class="celda th" width="8%">Monto del pedido(Bs.)</th><?php } ?>
			<?php if(!isset($v6)){?><th class="celda th" width="7%">Descuento (Bs.)</th><?php } ?>
			<?php if(!isset($v7)){?><th class="celda th" width="8%">Monto total del pedido(Bs.)</th><?php } ?>
			<?php if(!isset($v8)){?><th class="celda th" width="8%">Monto cancelado (Bs.)</th><?php } ?>
			<?php if(!isset($v9)){?><th class="celda th" width="8%">Saldo del pedido (Bs.)</th><?php } ?>
			<?php if(!isset($v10)){?><th class="celda th" width="7%">Cantidad de productos</th><?php } ?>
			<?php if(!isset($v11)){?><th class="celda th" width="7%">Estado de producción</th><?php } ?>
			<?php if(!isset($v12)){?><th class="celda th" width="9%">Estado de pago</th><?php } ?>
			<?php if(!isset($v13)){?><th class="celda th" width="18%">Observaciónes</th><?php } ?>

		</tr>
		<?php $cont=0; $sub_total=0; $total=0;
			foreach ($pedidos as $key => $pedido){ ?>
			<?php if($contReg>=$nro){
					$contReg=0;
					$contPagina++;
			?>
		</table><!--cerramos tabla-->
		</div><!--cerramos pagina-->
<div class="pagina table-responsive">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="90%"><div class='encabezado_titulo'>REGISTRO DE PEDIDOS</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'></span></div>
				</td></tr>
			</table>
		</div>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php if(!isset($v1)){?><th class="celda th" width="3%" >#Item</th><?php } ?>
			<?php if(!isset($v2)){?><th class="celda th" width="3%" >Nº de pedido</th><?php } ?>
			<?php if(!isset($v3)){?><th class="celda th" width="7%">Fecha de pedido</th><?php } ?>
			<?php if(!isset($v4)){?><th class="celda th" width="7%" >Fecha en entrega</th><?php } ?>
			<?php if(!isset($v5)){?><th class="celda th" width="8%">Monto del pedido(Bs.)</th><?php } ?>
			<?php if(!isset($v6)){?><th class="celda th" width="7%">Descuento (Bs.)</th><?php } ?>
			<?php if(!isset($v7)){?><th class="celda th" width="8%">Monto total del pedido(Bs.)</th><?php } ?>
			<?php if(!isset($v8)){?><th class="celda th" width="8%">Monto cancelado (Bs.)</th><?php } ?>
			<?php if(!isset($v9)){?><th class="celda th" width="8%">Saldo del pedido (Bs.)</th><?php } ?>
			<?php if(!isset($v10)){?><th class="celda th" width="7%">Cantidad de productos</th><?php } ?>
			<?php if(!isset($v11)){?><th class="celda th" width="7%">Estado de producción</th><?php } ?>
			<?php if(!isset($v12)){?><th class="celda th" width="9%">Estado de pago</th><?php } ?>
			<?php if(!isset($v13)){?><th class="celda th" width="18%">Observaciónes</th><?php } ?>
		</tr>
		<?php }//end if 
		?>
		<tr class="fila">
			<?php if(!isset($v1)){?><td class="celda td"><?php echo $cont+1;$cont++;?></td><?php } ?>
			<?php if(!isset($v2)){?><td class="celda td"><?php echo $pedido->numero;?></td><?php } ?>
			<?php if(!isset($v3)){?><td class="celda td"><?php echo $this->lib->format_date($pedido->fecha_pedido,'Y/m/d'); ?></td><?php } ?>
			<?php if(!isset($v4)){?><td class="celda td"><?php echo $this->lib->format_date($pedido->fecha_entrega,'Y/m/d'); ?></td><?php } ?>
			<?php if(!isset($v5)){?><td class="celda td"><?php echo number_format($pedido->monto_parcial,2,'.',','); ?></td><?php } ?>
			<?php if(!isset($v6)){?><td class="celda td"><?php echo number_format($pedido->descuento,2,'.',','); ?></td><?php } ?>
			<?php if(!isset($v7)){?><td class="celda td"><?php echo number_format($pedido->monto_total,2,'.',','); ?></td><?php } ?>
			<?php $pagos=$this->M_pago->get_row('idpe',$pedido->idpe); $t_pagos=0;
				for($pa=0; $pa < count($pagos); $pa++){ $t_pagos+=($pagos[$pa]->monto);}
			?>
			<?php if(!isset($v8)){?><td class="celda td"><?php echo number_format($t_pagos,2,'.',',');?></td><?php } ?>
			<?php if(!isset($v9)){?><td class="celda td"><?php echo number_format(($pedido->monto_total-$t_pagos),2,'.',',');?></td><?php } ?>
			<?php $detalle_pedido=$this->M_detalle_pedido->get_row('idpe',$pedido->idpe);
				$c_productos=0;
				for($dp=0; $dp < count($detalle_pedido); $dp++){ $c_productos+=($detalle_pedido[$dp]->cantidad);}
			?>
			<?php if(!isset($v10)){?><td class="celda td"><?php echo $c_productos;?></td><?php } ?>
			<?php if(!isset($v11)){?><td class="celda td"><?php 
				if($pedido->estado==0){echo "Pendiente";}
				if($pedido->estado==1){echo "En producción";}
				if($pedido->estado==2){echo "Entregado";}
			?></td><?php } ?>
			<?php if(!isset($v12)){ ?><td class="celda td"><?php 
				if($pedido->monto_total<$t_pagos){
					 echo "Cancelado +";
				}else{
					if($pedido->monto_total==$t_pagos){ 
						echo "Cancelado";
					 }else{
						if($t_pagos==0){
							echo "No cancelado";
						}else{
							echo "En proceso";
						}
					}
				}?></td><?php } ?>
			<?php if(!isset($v13)){?><td class="celda td"><?php echo $pedido->observaciones;?></td><?php } ?>
		</tr>
	<?php $contReg++;}// end for ?>
	</table>
</div>
<script type="text/javascript"> $(".nroPagina").html(<?php echo $contPagina;?>); </script>