<div class="row-border table-responsive">
	<table class="table table-bordered"><thead>
		<tr>
			<th width="15%">Nro. Reg. por hojas</th>
			<th width="20%">
				<select id="nro" class="form-control input-sm">
				<?php for ($i=1; $i <= 50 ; $i++) { ?>
					<option value="<?php echo $i;?>" <?php if($i==34){echo "selected";}?>><?php echo $i;?></option>
				<?php } ?>
				</select>
			</th>
			<th width="65%"></th>
		</tr>
	</thead></table>
	<div id="config_area">
		<table class="tabla tabla-border-true">
			<tr class="fila">
				<th class="celda th" width="3%" >#Item</th>
				<th class="celda th" width="3%" >Nº de pedido</th>
				<th class="celda th" width="7%">Fecha de pedido</th>
				<th class="celda th" width="7%" >Fecha en entrega</th>
				<th class="celda th" width="8%">Monto del pedido(Bs.)</th>
				<th class="celda th" width="7%">Descuento (Bs.)</th>
				<th class="celda th" width="8%">Monto total del pedido(Bs.)</th>
				<th class="celda th" width="8%">Monto cancelado (Bs.)</th>
				<th class="celda th" width="8%">Saldo del pedido (Bs.)</th>
				<th class="celda th" width="7%">Cantidad de productos</th>
				<th class="celda th" width="7%">Estado de producción</th>
				<th class="celda th" width="9%">Estado de pago</th>
				<th class="celda th" width="18%">Observaciónes</th>
			</tr>
			<tr class="fila">
			<tr class="fila">
				<th class="celda th"><input type="checkbox" id="1" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="2" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="3" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="4" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="5" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="6" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="7" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="8" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="9" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="10" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="11" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="12" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="13" checked="checked"></th>
			</tr>
			</tr>
		</table>
	</div>
</div>
<div class="row-border">
	<div id="area"></div>
</div>
<script language="javascript">
	$("#1").unbind("click");$("#1").change(function(){ arma_informe_pedido('<?php echo $pedidos;?>'); }); 
	$("#2").unbind("click");$("#2").change(function(){ arma_informe_pedido('<?php echo $pedidos;?>'); }); 
	$("#3").unbind("click");$("#3").change(function(){ arma_informe_pedido('<?php echo $pedidos;?>'); }); 
	$("#4").unbind("click");$("#4").change(function(){ arma_informe_pedido('<?php echo $pedidos;?>'); }); 
	$("#5").unbind("click");$("#5").change(function(){ arma_informe_pedido('<?php echo $pedidos;?>'); }); 
	$("#6").unbind("click");$("#6").change(function(){ arma_informe_pedido('<?php echo $pedidos;?>'); });
	$("#7").unbind("click");$("#7").change(function(){ arma_informe_pedido('<?php echo $pedidos;?>'); }); 
	$("#8").unbind("click");$("#8").change(function(){ arma_informe_pedido('<?php echo $pedidos;?>'); }); 
	$("#9").unbind("click");$("#9").change(function(){ arma_informe_pedido('<?php echo $pedidos;?>'); }); 
	$("#10").unbind("click");$("#10").change(function(){ arma_informe_pedido('<?php echo $pedidos;?>'); }); 
	$("#11").unbind("click");$("#11").change(function(){ arma_informe_pedido('<?php echo $pedidos;?>'); }); 
	$("#12").unbind("click");$("#12").change(function(){ arma_informe_pedido('<?php echo $pedidos;?>'); });
	$("#13").unbind("click");$("#13").change(function(){ arma_informe_pedido('<?php echo $pedidos;?>'); });
	$("#nro").unbind("click");$("#nro").change(function(){ arma_informe_pedido('<?php echo $pedidos;?>'); });
	Onfocus("nro");
</script>