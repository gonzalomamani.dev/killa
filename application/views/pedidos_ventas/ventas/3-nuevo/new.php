<?php
	$help1='title="<h4>Número de pedido<h4>" data-content="Representa al número de referencia del pedido que va a ser vendído, <b>si el número no aparece en la lista, entonces el pedido ya fue vendido</b>"';
	$help2='title="<h4>Fecha de venta<h4>" data-content="Representa a la fecha de venta del pedido"';
	$help3='title="<h4>Monto parcial<h4>" data-content="Representa al monto de pedido ingresado deacuerdo con el monto sugerido por el sistema, para mas detalle vaya a pedido en la sección del configuración del pedido."';
	$help4='title="<h4>Descuento<h4>" data-content="Representa al descuento registrado en el pedido, para mas detalle vaya a la sección de pedido a la parte de configuración de pedido."';
	$help5='title="<h4>Monto total<h4>" data-content="Representa el la diferencia entre <b>monto parcial y descuento</b>, este valor se asigno en el pedido, para mas detalle vaya a la sección de pedido a la parte de configuración de pedido."';
	$help6='title="<h4>Número de pagos<h4>" data-content="Representa a la cantidad de pagos efectuados en el pedido, para mayor detalle dirijase a la sección de pedidos y la opción pagos."';
	$help7='title="<h4>Total pagos<h4>" data-content="Representa a la cantidad total en terminos monetarios de los pagos efectuados en el pedido, para mayor detalle dirijase a la sección de pedidos y la opción pagos."';
	$help8='title="<h4>Saldo de pedido<h4>" data-content="Representa a el saldo adeudado del cliente en el pedido, el origen del valor es a la diferencia entre el monto total el pedido y monto total pagado,"';
	$help9='title="<h4>Monto de venta s/sistema<h4>" data-content="Representa al monto de venta sugerido por el sistema, este monto varia segun la cantidad de productos vendidos."';
	$help10='title="<h4>Monto de venta<h4>" data-content="Representa al monto de venta total del pedido, este monto inicialmente toma el valor sugerido por el sistema, si desea puede mondificarlo teniendo el <b>cuidado que el valor se aproxime al valor siguerido por el sistema</b>."';
	$help11='title="<h4>Descuento<h4>" data-content="Representa al descuento en la venta si hubiera, este descuento es a sugerencia del cliente."';
	$help12='title="<h4>Monto total de venta<h4>" data-content="Representa al <b>monto final de la venta</b>, este monto representa a la diferencia en monto de venta y el descuento de venta."';
	$help13='title="<h4>Total Cancelado<h4>" data-content="Representa a la cantidad total en terminos monetarios de los pagos efectuados en el pedido, para mayor detalle dirijase a la sección de pedidos y la opción pagos."';
	$help14='title="<h4>Saldo de venta<h4>" data-content="Representa al saldo en terminos monetarios de la venta, este saldo se obtiene de la diferencia entre el monto total de venta y el total cancelado."';
	$help15='title="<h4>Observaciónes<h4>" data-content="la observacion en la venta puede poseer un formato alfanumerico de 0 a 900 caracteres <b>puede incluir espacios</b>, ademas la observacion solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	//armando los pedidos no vendidos
	$v_pedidos=Array();
	for ($i=0; $i < count($pedidos) ; $i++) { 
		$control=true;
		for ($j=0; $j < count($ventas) ; $j++) { 
			if($pedidos[$i]->idpe==$ventas[$j]->idpe){
				$control=false;
				break;
			}
		}
		if($control){
			$v_pedidos[]=$pedidos[$i]->idpe;
		}
	}
	$pedidos=json_decode(json_encode($v_pedidos));
?>
<div id="row">
	<div class="col-md-3 col-md-offset-9 col-sm-5 col-sm-offset-7 col-xs-12 text-left">
		<div class="input-group">
			<select id="cod_ped" onchange="search_venta(this.value)" class="form-control input-md">
				<option value="">Seleccione...</option>
			<?php foreach ($pedidos as $clave => $ped) { ?>
				<option value="<?php echo $ped;?>" <?php if(isset($_POST['idpe'])){if($_POST['idpe']==$ped){ echo "selected";}}?>><?php echo $ped;?></option>
			<?php }?>
			</select>
			<span class="input-group-addon" <?php echo $popover.$help1;?>><i class="glyphicon glyphicon-info-sign"></i></span>
		</div>
	</div>
</div>
<hr>
<div id="modal_contenido">
<?php if(isset($_POST['idpe'])){
	$pedido=$pedido[0];
	$url=base_url().'libraries/img/pieza_productos/miniatura/';
	$cliente=$this->M_cliente->get($pedido->idcl);
?>
<b>NIT/CI: </b><?php echo $cliente[0]->nit;?><br>
<b>Nombre o Razon Social: </b><?php echo $cliente[0]->nombre;?>
<div class="row"><div class="col-sm-2 col-sm-offset-10 col-xs-12"><strong><span class='text-danger'>(*)</span>Campos obligatorio</strong></div></div>
<div class="table-responsive">
<table class="table table-hover table-bordered">
<thead>
	<tr class="fila">
		<th class="celda th" colspan="4"></th>
		<th class="celda th text-right" colspan="2"><span class='text-danger'>(*)</span> Fecha de Venta:</th>
		<th class="celda th" colspan="2">
		<div class="input-group col-xs-12">
			<input type="date" class="form-control  input-sm" id="fecha" value="<?php echo date('Y-m-d'); ?>">
			<span class="input-group-addon" <?php echo $popover.$help2;?>><i class='glyphicon glyphicon-info-sign'></i></span>	
		</div>
		</th>
	</tr>
	<tr class="fila">
		<th class="celda th"><div class="g-thumbnail"></div></th>
		<th class="celda th" width="11%">Código</th>
		<th class="celda th" width="28%">Producto</th>
		<th class="celda th" width="9%">Cantidad<br>Pedida</th>
		<th class="celda th" width="9%">Cantidad<br>Vendida</th>
		<th class="celda th" width="9%">c/u de venta<br>(Bs.), segun<br>cliente</th>
	<!--<th class="celda th" width="9%">Costo Total<br>de venta (Bs.)</th>-->
	<!--<th class="celda th" width="9%">Descuento</th>-->
		<th class="celda th" width="9%">Costo Total<br>de venta(Bs.)</th>
		<th class="celda th" width="25%">Observaciónes</th>
	</tr>
</thead>
<tbody id="content_protuct">
	<?php
		$s_cant_ped=0;
		$s_cant_pro=0;
		$s_cant_ven=0;
		$s_cu=0;
		$s_ct=0;
		$s_des=0;
		$s_cf=0;
		$s_por=0;
		$limite=count($detalle_pedido);
		for($i=0; $i < $limite; $i++) { $dep=$detalle_pedido[$i];
			$pieza=$this->M_categoria_pieza->get($dep->idcp);
			$producto=$this->M_producto->get($pieza[0]->idp);
			//color del producto
			$categoria=$this->M_categoria_producto->get($dep->idpim);
			$mat=$this->M_material->get($categoria[0]->idm);
			$color=$this->M_color->get($mat[0]->idco);
			//imagen producto
			$imagen=$this->M_imagen_producto->get_pieza_material($dep->idpim);
			$img="default.png";
			if(count($imagen)>0){$img=$imagen[0]->nombre;}
			//costo del producto
			$producto_cliente=$this->M_producto_cliente->get_row_2n('idpim',$dep->idpim,'idcl',$cliente[0]->idcl);
			$costo=0;
			if(count($producto_cliente)>0){
				$costo=$producto_cliente[0]->costo_unitario;
			}
		?>
			<tr class="fila">
				<td class="celda td"><input type="hidden" id="iddp<?php echo $dep->iddp;?>" value='<?php echo $dep->iddp;?>'><div id="item"><?php echo $i+1;?></div><img src="<?php echo $url.$img;?>" width='100%'class='img-thumbnail g-thumbnail'></td>
				<td class="celda td" ><?php echo $producto[0]->cod;?></td>
				<td class="celda td" ><?php echo $producto[0]->nombre.' - '.$color[0]->nombre;?></td>
				<td class="celda td" ><input type="number" class="form-control  input-sm" value="<?php echo $dep->cantidad;?>" disabled="disabled" id="cp_<?php echo $dep->iddp;?>"><?php $s_cant_ped+=$dep->cantidad;?></td>
				<td class="celda td" ><input type="number" class="form-control input-sm" onkeyup="costo_venta_producto('<?php echo $dep->iddp;?>')" onclick="costo_venta_producto('<?php echo $dep->iddp;?>')" value="<?php echo $dep->cantidad;?>" id="cv_<?php echo $dep->iddp;?>" name='cv' min="0"><?php $s_cant_ven+=$dep->cantidad;?></td>
				<td class="celda td" ><input type="number" class="form-control input-sm" disabled="disabled" id="cu_<?php echo $dep->iddp;?>" value="<?php echo $costo;?>"><?php $s_cu+=$costo;?></td>
			<!--<td class="celda td" ><input type="number" class="form-control input-sm" id="ct<?php echo $i;?>" value="<?php echo $costo*$dep->cantidad;?>" disabled="disabled" name='ct'><?php $s_ct+=($costo*$dep->cantidad);?></td>-->
			<!--<td class="celda td" ><input type="number" class="form-control input-sm" onchange="descuento('<?php echo $i;?>')" onclick="descuento('<?php echo $i;?>')" value="0" id="des<?php echo $i?>" name='des' alt="<?php echo $dep->iddp;?>"><?php $s_des+=0;?></td>-->
				<td class="celda td" ><input type="number" class="form-control input-sm" value="<?php echo $costo*$dep->cantidad;?>" id="cf_<?php echo $dep->iddp;?>" disabled="disabled" name='cf'><?php $s_cf+=($costo*$dep->cantidad);?></td>
				<td class="celda"><textarea id="obs_<?php echo $dep->iddp;?>" class="form-control input-sm" placeholder="Observaciones en la venta del producto"></textarea></td>
			</tr>
		<?php
		}
	?>
</tbody>
<thead>
	<tr class="fila">
		<th class="celda th" colspan="3">Totales:</th>
		<th class="celda th"><?php echo $s_cant_ped;?></th>
		<th class="celda th" id="t_cv"><?php echo $s_cant_ven;?></th>
		<th class="celda th"><?php echo $s_cu;?></th>
	<!--<th class="celda th" id="t_ct"><?php echo $s_ct;?></th>-->
	<!--<th class="celda th" id="t_des"><?php echo $s_des;?></th>-->
		<th class="celda th" id="t_cf"><?php echo $s_cf;?></th>
		<th class="celda"></th>
	</tr>
	<tr class="fila">
		<th class="celda th" colspan="3"></th>
		<th class="celda th">Cantidad<br>Pedida</th>
		<th class="celda th">Cantidad<br>Vendida</th>
		<th class="celda th">c/u de venta<br>(Bs.), segun<br>cliente</th>
	<!--<th class="celda th">Costo Total<br>de venta (Bs.)</th>-->
	<!--<th class="celda th">Descuento</th>-->
		<th class="celda th">Costo Final<br>de venta</th>
		<th class="celda th" width="14%">Observaciónes</th>
	</tr>
</thead>
</table>
</div>
<hr>
<div class="row">
	<div class="col-xs-12"><p><strong><ins>DETALLE ECONÓMICO DE PEDIDO:</ins></strong></p></div>
	<div class="col-md-1 col-sm-2 col-xs-12"><strong>Monto parcial(Bs.): </strong></div>
	<div class="col-md-3 col-sm-4 col-xs-12">
		<div class="input-group col-xs-12">
			<div class='form-control input-md' disabled><?php echo number_format($pedido->monto_parcial,2,'.',','); ?></div>
			<span class="input-group-addon" <?php echo $popover.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div>
	<div class="col-md-1 col-sm-2 col-xs-12"><strong>Descuento(Bs.): </strong></div>
	<div class="col-md-3 col-sm-4 col-xs-12">
		<div class="input-group col-xs-12">
			<div class='form-control input-md' disabled><?php echo number_format($pedido->descuento,2,'.',','); ?></div>
			<span class="input-group-addon" <?php echo $popover.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>	
		</div>
	</div><i class='visible-sm-block clearfix'></i>
	<div class="col-md-1 col-sm-2 col-xs-12"><strong>Monto total s/pedido(Bs.): </strong></div>
	<div class="col-md-3 col-sm-4 col-xs-12">
		<div class="input-group col-xs-12">
			<div class='form-control input-md' disabled><strong><?php echo number_format($pedido->monto_total,2,'.',','); ?></strong></div>
			<span class="input-group-addon" <?php echo $popover.$help5;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div><i class='visible-lg-block visible-md-block clearfix'></i>
	<div class="col-xs-12">
		<p><strong><ins>DETALLE DE PAGOS EN EL PEDIDO:</ins></strong></p>
		<?php $pagos=$this->M_pago->get_row('idpe',$pedido->idpe);
			$t_pagos=0;
			for($pa=0; $pa < count($pagos); $pa++){ $t_pagos+=($pagos[$pa]->monto);}
		?>
	</div>
	<div class="col-md-1 col-sm-2 col-xs-12"><strong>Número de pagos: </strong></div>
	<div class="col-md-3 col-sm-4 col-xs-12">
		<div class="input-group col-xs-12">
			<div class='form-control input-md' disabled><?php echo number_format(count($pagos),0,'.',','); ?></div>
			<span class="input-group-addon" <?php echo $popover.$help6;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div>
	<div class="col-md-1 col-sm-2 col-xs-12"><strong>Total pagado (Bs.): </strong></div>
	<div class="col-md-3 col-sm-4 col-xs-12">
		<div class="input-group col-xs-12">
			<div class='form-control input-md' disabled><?php echo number_format($t_pagos,2,'.',','); ?></div>
			<span class="input-group-addon" <?php echo $popover.$help7;?>><i class='glyphicon glyphicon-info-sign'></i></span>	
		</div>
	</div><i class='visible-sm-block clearfix'></i>
	<div class="col-md-1 col-sm-2 col-xs-12"><strong>Saldo del pedido (Bs.):</strong></div>
	<div class="col-md-3 col-sm-4 col-xs-12">
		<div class="input-group col-xs-12">
			<div class='form-control input-md' disabled><strong><?php echo number_format($pedido->monto_total-$t_pagos,2,'.',','); ?></strong></div>
			<span class="input-group-addon" <?php echo $popover.$help8;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div><i class='visible-lg-block visible-md-block clearfix'></i>
	<div class="col-xs-12"><p><strong><ins>DETALLE ECONÓMICO DE VENTA:</ins></strong></p></div>
	<div class="col-md-1 col-sm-2 col-xs-12"><strong>Monto de venta s/Sistema(Bs.): </strong></div>
	<div class="col-md-3 col-sm-4 col-xs-12">
		<div class="input-group col-xs-12">
			<input type="number" class="form-control input-md" id="cos_vs" placeholder="Costo de venta" value="<?php echo $s_cf;?>" disabled>
			<span class="input-group-addon" <?php echo $popover.$help9;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div>
	<div class="col-md-1 col-sm-2 col-xs-12"><strong>Monto de venta (Bs.): </strong></div>
	<div class="col-md-3 col-sm-4 col-xs-12">
	<form onsubmit="return calcula_costo_venta()">
		<div class="input-group col-xs-12">
			<input type="number" class="form-control input-md" id="cos_v" placeholder="Descuento de venta" value="<?php echo $s_cf;?>" onchange='calcula_costo_venta();' onkeyup="calcula_costo_venta()">
			<span class="input-group-addon" <?php echo $popover.$help10;?>><i class='glyphicon glyphicon-info-sign'></i></span>	
		</div>
	</form>
	</div><i class='visible-sm-block clearfix'></i>
	<div class="col-md-1 col-sm-2 col-xs-12"><strong>Descuento (Bs.): </strong></div>
	<div class="col-md-3 col-sm-4 col-xs-12">
	<form onsubmit="return calcula_costo_venta()">
		<div class="input-group col-xs-12">
			<input type="number" class="form-control input-md" id="des_v" placeholder="Descuento de venta" value="0" onchange='calcula_costo_venta();' onkeyup="calcula_costo_venta()">
			<span class="input-group-addon" <?php echo $popover.$help11;?>><i class='glyphicon glyphicon-info-sign'></i></span>	
		</div>
	</form>
	</div><i class='visible-lg-block visible-md-block clearfix'></i>
	<div class="col-md-1 col-sm-2 col-xs-12"><strong>Monto total de venta (Bs.): </strong></div>
	<div class="col-md-3 col-sm-4 col-xs-12">
		<div class="input-group col-xs-12">
			<input type="number" class="form-control input-md" id="tot_v" placeholder="Costo total de venta" value="<?php echo $s_cf;?>" disabled>
			<span class="input-group-addon" <?php echo $popover.$help12;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div><i class='visible-sm-block clearfix'></i>
	<div class="col-md-1 col-sm-2 col-xs-12"><strong>Total Cancelado (Bs.): </strong></div>
	<div class="col-md-3 col-sm-4 col-xs-12">
		<div class="input-group col-xs-12">
			<input type="number" class="form-control input-md" id="pag_v" placeholder="Costo total de venta" value="<?php echo $t_pagos;?>" disabled>
			<span class="input-group-addon" <?php echo $popover.$help13;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div><i class='visible-md-block clearfix'></i>
	<div class="col-md-1 col-sm-2 col-xs-12"><strong id="t_saldo" class="<?php if(($s_cf-$t_pagos)==$pedido->monto_total){ echo 'text-danger';}else{ if(($s_cf-$t_pagos)<$pedido->monto_total){ echo 'text-warning';}else{}}?>">Saldo de venta (Bs.): </strong></div>
	<div class="col-md-3 col-sm-4 col-xs-12">
		<div class="input-group col-xs-12">
			<input type="number" class="form-control input-md" id="sal_v" placeholder="Costo total de venta" value="<?php echo ($s_cf-$t_pagos);?>" disabled>
			<span class="input-group-addon" <?php echo $popover.$help14;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div><i class='visible-lg-block visible-sm-block clearfix'></i>
	<div class="col-md-1 col-sm-2 col-xs-12"><strong>Observaciónes: </strong></div>
	<div class="col-md-11 col-sm-10 col-xs-12">
		<div class="input-group col-xs-12">
			<textarea id="obs" class="form-control input-sm" placeholder="Observaciónes en la venta"></textarea>
			<span class="input-group-addon" <?php echo $popover.$help15;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div>
</div>

<?php }// end if?>
</div>

<script language='javascript'>Onfocus("cod_ped");$('[data-toggle="popover"]').popover({html:true});</script>