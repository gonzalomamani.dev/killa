<?php $j_ventas=json_encode($ventas);?>
<div class="table-responsive">
<table class="table table-bordered table-striped table-hover">
<thead>
<tr>
	<th width="3%">#</th>
	<th width="10%" class="celda-sm">NIT/CI</th>
	<th width="15%">Razon Social<br>del Cliente</th>
	<th width="15%">Fecha de Venta</th>
	<th width="7%">Total<br>(Bs.)</th>
	<th width="7%">Pagado<br>(Bs.)</th>
	<th width="7%">Saldo<br>(Bs.)</th>
	<th width="5%">Nº Ped.</th>
	<th width="5%">% cumpl.</th>
	<th width="16%" class="celda-sm">Observaciónes</th>
	<th width="10%"></th>
</tr>
</thead>
<tbody>
<?php if(count($ventas)>0){?>
<?php for($i=0;$i<count($ventas); $i++) { $venta=$ventas[$i];
	$cliente=$this->M_cliente->get($venta->idcl);
?>
	<tr>
		<td><?php echo $i+1;?></td>
		<td class="celda-sm"><?php echo $venta->nit;?></td>
		<td><?php echo $venta->nombre;?></td>
		<td><?php echo $this->lib->format_date($venta->fecha,'d ml Y');?></td>
		<td><?php echo number_format($venta->monto_total,2,'.',',');?></td>
		<?php $pagos=$this->M_pago->get_row('idpe',$venta->idpe);
			$t_pagos=0;
			for($pa=0; $pa < count($pagos); $pa++){ $t_pagos+=($pagos[$pa]->monto);}
		?>
		<td><?php echo number_format($t_pagos,2,'.',',');?></td>
		<td><?php echo number_format($venta->monto_total-$t_pagos,2,'.',',');?></td>
		<td><?php echo $venta->numero;?></td>
			<?php $detalle_pedido=$this->M_detalle_pedido->get_row('idpe',$venta->idpe);
				$c_prod_ped=0;$c_prod_ven=0;
				for($dp=0; $dp < count($detalle_pedido); $dp++){ $c_prod_ped+=($detalle_pedido[$dp]->cantidad); $c_prod_ven+=($detalle_pedido[$dp]->cantidad_vendida);}
			?>
		<td><?php echo number_format(($c_prod_ven*100)/$c_prod_ped,2,'.',',')."%"; ?></td>
		<td class="celda-sm"><?php echo $venta->observaciones_venta;?></td>
		<?php $mod="config_venta('".$venta->idv."')"; if($privilegio[0]->mo2u!="1"){ $mod="";}?>
		<?php $eli="confirmar_venta('".$venta->idv."')"; if($privilegio[0]->mo2d!="1"){ $eli="";}?>
		<td class="text-right"><?php $this->load->view('estructura/botones/botones_registros',['reportes'=>"reportes_venta('".$venta->idv."')",'configuracion'=>$mod,'eliminar'=>$eli]);?></td>
	</tr>
<?php }// end for?>
<?php }else{
		echo "<tr><td colspan='11'><h2>0 Registros encontrados...</h2></td></tr>";
	} ?>
</tbody>
</table>
</div>
<script language="javascript" type="text/javascript"> 
	$("#print").removeAttr("onclick"); 
	$("#print").unbind("click"); 
	$("#print").click(function(){ imprimir_ventas('<?php echo $j_ventas;?>'); }); 
</script>