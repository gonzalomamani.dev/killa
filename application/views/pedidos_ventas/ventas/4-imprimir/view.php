<?php 
	$ventas=json_decode($ventas);
	$contPagina=1;
	$contReg=0;
?>
<div class="pagina table-responsive">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="88%"><div class='encabezado_titulo'>REGISTRO DE VENTAS</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'></span></div>
				</td></tr>
			</table>
		</div>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php if(!isset($v1)){?><th class="celda th" width="5%" >#</th><?php }?>
			<?php if(!isset($v2)){?><th class="celda th" width="5%">Fecha de venta</th><?php }?>
			<?php if(!isset($v3)){?><th class="celda th" width="7%" >NIT/CI de cliente</th><?php }?>
			<?php if(!isset($v4)){?><th class="celda th" width="15%">Nombre/Razon de cliente</th><?php }?>
			<?php if(!isset($v5)){?><th class="celda th" width="8%">Total venta (Bs.)</th><?php }?>
			<?php if(!isset($v6)){?><th class="celda th" width="8%">Cancelado (Bs.)</th><?php }?>
			<?php if(!isset($v7)){?><th class="celda th" width="8%">Saldo de venta(Bs.)</th><?php }?>
			<?php if(!isset($v8)){?><th class="celda th" width="4%">ID Ped.</th><?php }?>
			<?php if(!isset($v9)){?><th class="celda th" width="5%">Prod. pedidos</th><?php }?>
			<?php if(!isset($v10)){?><th class="celda th" width="5%">Prod. vendidos</th><?php }?>
			<?php if(!isset($v11)){?><th class="celda th" width="5%">% de cumpl. venta</th><?php }?>
			<?php if(!isset($v12)){?><th class="celda th" width="25%">Observaciónes</th><?php }?>
		</tr>
		<?php $cont=0; $sub_total=0; $total=0;
			foreach ($ventas as $key => $venta){ ?>
			<?php if($contReg>=$nro){
					$contReg=0;
					$contPagina++;
			?>
		</table><!--cerramos tabla-->
		</div><!--cerramos pagina-->
<div class="pagina table-responsive">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="90%"><div class='encabezado_titulo'>REGISTRO DE VENTAS</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'></span></div>
				</td></tr>
			</table>
		</div>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php if(!isset($v1)){?><th class="celda th" width="5%" >#</th><?php }?>
			<?php if(!isset($v2)){?><th class="celda th" width="5%">Fecha de venta</th><?php }?>
			<?php if(!isset($v3)){?><th class="celda th" width="7%" >NIT/CI de cliente</th><?php }?>
			<?php if(!isset($v4)){?><th class="celda th" width="15%">Nombre/Razon de cliente</th><?php }?>
			<?php if(!isset($v5)){?><th class="celda th" width="8%">Total venta (Bs.)</th><?php }?>
			<?php if(!isset($v6)){?><th class="celda th" width="8%">Cancelado (Bs.)</th><?php }?>
			<?php if(!isset($v7)){?><th class="celda th" width="8%">Saldo de venta(Bs.)</th><?php }?>
			<?php if(!isset($v8)){?><th class="celda th" width="4%">ID Ped.</th><?php }?>
			<?php if(!isset($v9)){?><th class="celda th" width="5%">Prod. pedidos</th><?php }?>
			<?php if(!isset($v10)){?><th class="celda th" width="5%">Prod. vendidos</th><?php }?>
			<?php if(!isset($v11)){?><th class="celda th" width="5%">% de cumpl. venta</th><?php }?>
			<?php if(!isset($v12)){?><th class="celda th" width="25%">Observaciónes</th><?php }?>
		</tr>
		<?php }//end if 
		?>
		<tr class="fila">
			<?php if(!isset($v1)){?><td class="celda td"><?php echo $cont+1;$cont++;?></td><?php } ?>
			<?php if(!isset($v2)){?><td class="celda td"><?php echo $this->lib->format_date($venta->fecha,'Y/m/d');?></td><?php } ?>
			<?php if(!isset($v3)){?><td class="celda td"><?php echo $venta->nit; ?></td><?php } ?>
			<?php if(!isset($v4)){?><td class="celda td"><?php echo $venta->nombre; ?></td><?php } ?>
			<?php if(!isset($v5)){?><td class="celda td"><?php echo number_format($venta->monto_total,2,'.',','); ?></td><?php } ?>
			<?php $pagos=$this->M_pago->get_row('idpe',$venta->idpe);
				$t_pagos=0;
				for($pa=0; $pa < count($pagos); $pa++){ $t_pagos+=($pagos[$pa]->monto);}
			?>
			<?php if(!isset($v6)){?><td class="celda td"><?php echo number_format($t_pagos,2,'.',','); ?></td><?php } ?>
			<?php if(!isset($v7)){?><td class="celda td"><?php echo number_format($venta->monto_total-$t_pagos,2,'.',','); ?></td><?php } ?>
			<?php if(!isset($v8)){?><td class="celda td"><?php echo $venta->numero;?></td><?php } ?>
			<?php $detalle_pedido=$this->M_detalle_pedido->get_row('idpe',$venta->idpe);
				$c_prod_ped=0;$c_prod_ven=0;
				for($dp=0; $dp < count($detalle_pedido); $dp++){ $c_prod_ped+=($detalle_pedido[$dp]->cantidad); $c_prod_ven+=($detalle_pedido[$dp]->cantidad_vendida);}
			?>
			<?php if(!isset($v9)){?><td class="celda td"><?php echo $c_prod_ped;?></td><?php } ?>
			<?php if(!isset($v10)){?><td class="celda td"><?php echo $c_prod_ven;?></td><?php } ?>
			<?php if(!isset($v11)){?><td class="celda td"><?php echo number_format(($c_prod_ven*100)/$c_prod_ped,2,'.',',')."%"; ?></td><?php } ?>
			<?php if(!isset($v12)){?><td class="celda td"><?php echo $venta->observaciones_venta; ?></td><?php } ?>
		</tr>
	<?php $contReg++;}// end for ?>
	</table>
</div>
<script type="text/javascript"> $(".nroPagina").html(<?php echo $contPagina;?>); </script>