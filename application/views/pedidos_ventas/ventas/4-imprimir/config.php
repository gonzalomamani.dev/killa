<div class="row-border table-responsive">
	<table class="table table-bordered"><thead>
		<tr>
			<th width="15%">Nro. Reg. por hojas</th>
			<th width="20%">
				<select id="nro" class="form-control input-sm">
				<?php for ($i=1; $i <= 50 ; $i++) { ?>
					<option value="<?php echo $i;?>" <?php if($i==34){echo "selected";}?>><?php echo $i;?></option>
				<?php } ?>
				</select>
			</th>
			<th width="65%"></th>
		</tr>
	</thead></table>
	<div id="config_area">
		<table class="tabla tabla-border-true">
			<tr class="fila">
				<th class="celda th" width="5%" >#</th>
				<th class="celda th" width="5%">Fecha de venta</th>
				<th class="celda th" width="7%" >NIT/CI de cliente</th>
				<th class="celda th" width="15%">Nombre/Razon de cliente</th>
				<th class="celda th" width="8%">Total venta (Bs.)</th>
				<th class="celda th" width="8%">Cancelado (Bs.)</th>
				<th class="celda th" width="8%">Saldo de venta(Bs.)</th>
				<th class="celda th" width="4%">ID Ped.</th>
				<th class="celda th" width="5%">Prod. pedidos</th>
				<th class="celda th" width="5%">Prod. vendidos</th>
				<th class="celda th" width="5%">% de cumpli. venta</th>
				<th class="celda th" width="25%">Observaciónes</th>
			</tr>
			<tr class="fila">
			<tr class="fila">
				<th class="celda th"><input type="checkbox" id="1" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="2" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="3" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="4" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="5" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="6" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="7" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="8" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="9" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="10" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="11" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="12" checked="checked"></th>
			</tr>
			</tr>
		</table>
	</div>
</div>
<div class="row-border">
	<div id="area"></div>
</div>
<script language="javascript">
	$("#1").unbind("click");$("#1").change(function(){ arma_ventas('<?php echo $ventas;?>'); }); 
	$("#2").unbind("click");$("#2").change(function(){ arma_ventas('<?php echo $ventas;?>'); }); 
	$("#3").unbind("click");$("#3").change(function(){ arma_ventas('<?php echo $ventas;?>'); }); 
	$("#4").unbind("click");$("#4").change(function(){ arma_ventas('<?php echo $ventas;?>'); }); 
	$("#5").unbind("click");$("#5").change(function(){ arma_ventas('<?php echo $ventas;?>'); }); 
	$("#6").unbind("click");$("#6").change(function(){ arma_ventas('<?php echo $ventas;?>'); });
	$("#7").unbind("click");$("#7").change(function(){ arma_ventas('<?php echo $ventas;?>'); }); 
	$("#8").unbind("click");$("#8").change(function(){ arma_ventas('<?php echo $ventas;?>'); }); 
	$("#9").unbind("click");$("#9").change(function(){ arma_ventas('<?php echo $ventas;?>'); });
	$("#10").unbind("click");$("#10").change(function(){ arma_ventas('<?php echo $ventas;?>'); });
	$("#11").unbind("click");$("#11").change(function(){ arma_ventas('<?php echo $ventas;?>'); });
	$("#12").unbind("click");$("#12").change(function(){ arma_ventas('<?php echo $ventas;?>'); });
	$("#nro").unbind("click");$("#nro").change(function(){ arma_ventas('<?php echo $ventas;?>'); });
	Onfocus("nro");
</script>