<?php $venta=$venta[0];
	$contPagina=1;
	$contReg=0;
	$url=base_url().'libraries/img/pieza_productos/miniatura/';
	if(!isset($nro)){ $nro=34;}
?>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="reportes_venta('<?php echo $venta->idv;?>')">Detalle</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="productos_venta('<?php echo $venta->idv;?>')">Productos</a></li>
</ul>
<table class="tabla tabla-border-true">
	<tr class="fila">
		<th class="celda th" width="5%">imagen</th>
		<th class="celda th" width="8%"><input type="checkbox" id="1" <?php if(!isset($v1)){ echo "checked";}?>></th>
		<th class="celda th" width="12%">Nº Reg.</th>
		<th class="celda th" width="20%">
			<select id="nro" class="form-control input-sm">
				<?php for ($i=1; $i <= 50 ; $i++) { ?>
					<option value="<?php echo $i;?>" <?php if($i==$nro){echo "selected";}?>><?php echo $i;?></option>
				<?php } ?>
				</select>
		</th>
		<th class="celda th celda-sm" width="55%"></th>
	</tr>
</table><hr>
<div id="area">
	<div class="table-responsive pagina">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="88%"><div class='encabezado_titulo'>DETALLE DE PRODUCTOS EN LA VENTA</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'></span></div>
				</td></tr>
			</table>
		</div>
	<table border="0" class="tabla tabla-border-true">
		<tr class="fila">
			<th class="celda td" colspan="<?php if(!isset($v1)){ echo '3';}else{ echo '2';}?>" style="font-size:.9em;">FECHA DE VENTA:</th>
			<td class="celda td" style="font-size:.9em;"><?php echo $venta->fecha;?></td>
			<th class="celda td" colspan="2" style="font-size:.9em;">Nº DE PEDIDO:</th>
			<th class="celda td" style="font-size:.9em;"><?php echo $venta->idpe;?></th>
		</tr>
		<tr class="fila">
			<th class="celda th" width="5%">#Item</th>
			<?php if(!isset($v1)){ ?><th class="celda th" width="8%"><div class="g-thumbnail"></div></th><?php } ?>
			<th class="celda th" width="12%">Código</th>
			<th class="celda th" width="45%">Producto</th>
			<th class="celda th" width="10%">Cantidad de productos pedidos</th>
			<th class="celda th" width="10%">Cantidad de productos vendidos</th>
			<th class="celda th" width="10%">% de cumplimiento</th>
		</tr>
		<?php 
			for($i=0; $i < count($detalle_pedidos); $i++) { $producto=$detalle_pedidos[$i];
				$categoria=$this->M_categoria_producto->get($producto->idpim);
				$material=$this->M_material->get_material_color($producto->idm);
				$color="Sin color";if(!empty($material)){ $color=$material[0]->nombre;}
				$imagenes=$this->M_imagen_producto->get_row('idpim',$producto->idpim);
				$img="default.png";
				if(!empty($imagenes)){ $img=$imagenes[0]->nombre;}
				?>
			<?php if($contReg>=$nro){
					$contReg=0;
					$contPagina++;
			?>
	</table>
			</div>
	<div class="table-responsive pagina">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="88%"><div class='encabezado_titulo'>DETALLE DE PRODUCTOS EN LA VENTA</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'></span></div>
				</td></tr>
			</table>
		</div>
	<table border="0" class="tabla tabla-border-true">
		<tr class="fila">
			<th class="celda td" colspan="<?php if(!isset($v1)){ echo '3';}else{ echo '2';}?>" style="font-size:.9em;">FECHA DE VENTA:</th>
			<td class="celda td" style="font-size:.9em;"><?php echo $venta->fecha;?></td>
			<th class="celda td" colspan="2" style="font-size:.9em;">Nº DE PEDIDO:</th>
			<th class="celda td" style="font-size:.9em;"><?php echo $venta->idpe;?></th>
		</tr>
		<tr class="fila">
			<th class="celda th" width="5%">#Item</th>
			<?php if(!isset($v1)){ ?><th class="celda th" width="8%"><div class="g-thumbnail"></div></th><?php } ?>
			<th class="celda th" width="12%">Código</th>
			<th class="celda th" width="45%">Producto</th>
			<th class="celda th" width="10%">Cantidad de productos pedidos</th>
			<th class="celda th" width="10%">Cantidad de productos vendidos</th>
			<th class="celda th" width="10%">% de cumplimiento</th>
		</tr>
			<?php } // end if?>
				<tr class="fila">
					<td class="celda td"><?php echo $i+1;?></td>
					<?php if(!isset($v1)){ ?><td class="celda td"><img src="<?php echo $url.$img;?>" width="100%" class="g-thumbnail"></td><?php } ?>
					<td class="celda td"><?php echo $producto->codigo;?></td>
					<td class="celda td"><?php echo $producto->nombre." - ".$color;?></td>
					<td class="celda td"><?php echo  $producto->cantidad;?></td>
					<td class="celda td"><?php echo  $producto->cantidad_vendida;?></td>
					<td class="celda td"><?php echo  number_format(($producto->cantidad_vendida*100)/$producto->cantidad,2,'.',',')."%";?></td>
				</tr>
				<?php
			$contReg++; }//end for ?>
	</table>
</div>
</div>
<script language="javascript">
	$("#1").unbind("click");$("#1").change(function(){ productos_venta_imagen('<?php echo $venta->idv;?>'); });
	$("#nro").unbind("click");$("#nro").change(function(){ productos_venta_imagen('<?php echo $venta->idv;?>'); });
	$(".nroPagina").html(<?php echo $contPagina;?>);
</script>