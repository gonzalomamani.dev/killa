<?php $venta=$venta[0];
	$contPagina=1;
?>
<ul class="nav nav-tabs">
  <li role="presentation" class="active"><a href="javascript:" onclick="reportes_venta('<?php echo $venta->idv;?>')">Detalle</a></li>
  <li role="presentation"><a href="javascript:" onclick="productos_venta('<?php echo $venta->idv;?>')">Productos</a></li>
</ul>
<div class="table-responsive" id="area">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="88%"><div class='encabezado_titulo'>DETALLE DE VENTA</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Página: </b><?php echo $contPagina.' de 1'?></div>
				</td></tr>
			</table>
		</div>
	<table border="0" class="tabla tabla-border-true">
		<tr class="fila">
			<th class="celda td" style="font-size:.9em;">FECHA DE VENTA:</th>
			<td class="celda td" colspan="3" style="font-size:.9em;"><?php echo $venta->fecha;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th" colspan="4">DATOS DE CLIENTE</th>
		</tr>
		<tr class="fila">
			<th class="celda th" width="20%">Nombre/Razon Social:</th><td class="celda td" width="30%"><?php echo $venta->nombre;?></td>
			<th class="celda th" width="17%">NIT/CI:</th><td class="celda td" width="33%"><?php echo $venta->nit;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Pais: </th><td class="celda td"><?php echo $venta->pais;?></td>
			<th class="celda th">Ciudad: </th><td class="celda td"><?php echo $venta->ciudad;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Telf/Cel: </th><td class="celda td"><?php echo $venta->telefono;?></td>
			<th class="celda th">Email: </th><td class="celda td"><?php echo $venta->email;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Dirección: </th><td class="celda td"><?php echo $venta->direccion;?></td>
			<th class="celda th">Sitio Web: </th><td class="celda td"><?php echo $venta->url;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Encargado: </th><td class="celda td"><?php echo $venta->encargado;?></td>
			<th class="celda th">Observaciónes: </th><td class="celda td"><?php echo $venta->caracteristicas;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th" colspan="4">DATOS DE VENTA</th>
		</tr>
		<tr class="fila">
			<th class="celda th">Nº Pedido: </th><td class="celda td"><?php echo $venta->numero;?></td>
			<?php $nom="NULL"; $empleado=$this->M_empleado->get_complet($venta->ide); if(!empty($empleado)){ $nom=$empleado[0]->nombre." ".$empleado[0]->nombre2." ".$empleado[0]->paterno." ".$empleado[0]->materno;}?>
			<th class="celda th">Nombre de vendedor: </th><td class="celda td"><?php echo $nom;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Monto de venta (Bs.): </th><td class="celda td"><?php echo number_format($venta->monto_parcial_venta,2,'.',',');?></td>
			<th class="celda th">Descuento (Bs.): </th><td class="celda td"><?php echo number_format($venta->descuento_venta,2,'.',',');?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Monto de venta (Bs.): </th><td class="celda td"><?php echo number_format($venta->monto_total,2,'.',',');?></td>
			<?php 
				$t_pagos=0;
				for($pa=0; $pa < count($pagos); $pa++){ $t_pagos+=($pagos[$pa]->monto);}
			?>
			<th class="celda th">Estado de pagos: </th><td class="celda td"><?php 
				if($venta->monto_total<$t_pagos){
					 echo "Cancelado +";
				}else{
					if($venta->monto_total==$t_pagos){ 
						echo "Cancelado";
					 }else{
						if($t_pagos==0){
							echo "No cancelado";
						}else{
							echo "En proceso";
						}
					}
				}?></td>
		</tr>
		<?php 
			$c_prod=0;$c_ven=0;
			for($dp=0; $dp < count($detalle_pedidos); $dp++){ $c_prod+=($detalle_pedidos[$dp]->cantidad);$c_ven+=($detalle_pedidos[$dp]->cantidad_vendida);}
		?>
		<tr class="fila">
			<th class="celda th">Cantidad de productos pedidos: </th><td class="celda td"><?php echo $c_prod;?></td>
			<th class="celda th">Cantidad de productos vendidos: </th><td class="celda td"><?php echo $c_ven;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">% de cumplimiento: </th><td class="celda td"><?php echo number_format((($c_ven*100)/$c_prod),2,'.',',')."%";?></td>
			<th class="celda th"></th><td class="celda td"></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Observaciónes: </th><td class="celda td" colspan="3"><?php echo $venta->observaciones_venta;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th" colspan="4">DATOS DE PAGOS EN EL PEDIDO</th>
		</tr>
		<tr class="fila">
			<th class="celda th">#Pago</th>
			<th class="celda th">Fecha de pago</th>
			<th class="celda th">Monto cancelado</th>
			<th class="celda th">Observaciónes</th>
		</tr>
		<?php for ($i=0; $i < count($pagos) ; $i++) { $pago=$pagos[$i]; ?>
		<tr class="fila">
			<td class="celda td"><?php echo "Pago ".($i+1);?></td>
			<td class="celda td"><?php echo $pago->fecha;?></td>
			<td class="celda td"><?php echo number_format($pago->monto,2,'.',',');?></td>
			<td class="celda td"><?php echo $pago->observaciones;?></td>
		</tr>
		<?php } ?>
		<tr class="fila">
			<th class="celda th" colspan="2">Total venta(Bs.)</th>
			<th class="celda th"><?php echo number_format($t_pagos,2,'.',',');?></th>
			<th class="celda th"></th>
		</tr>
		<tr class="fila">
			<th class="celda th" colspan="4">DETALLE ECONÓMICO</th>
		</tr>
		<tr class="fila">
			<th class="celda th">Total venta (Bs.): </th><td class="celda td"><?php echo number_format($venta->monto_total,2,'.',',');?></td>
			<th class="celda th">Total pagos (Bs.): </th><td class="celda td"><?php echo number_format($t_pagos,2,'.',',');?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Saldo de la venta (Bs.): </th><td class="celda td"><?php echo number_format($venta->monto_total-$t_pagos,2,'.',',');?></td>
			<th class="celda th"></th><td class="celda td"></td>
		</tr>
	</table>
</div>