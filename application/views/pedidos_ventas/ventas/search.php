<table class="tabla tabla-border-false">
	<tr class="fila">
		<td class='celda celda-sm' style="width:3%"></td>
		<td class='celda celda-sm' style="width:10%">
			<form onsubmit="return view_venta();">
				<input type="number" id="sv_nit" placeholder='NIT/CI' class="form-control input-sm" onkeyup="reset_input(this.id)">
			</form>
		</td>
		<td class='celda celda-sm' style="width:15%">
			<select id="sv_raz" class="form-control input-sm" onchange="reset_input(this.id); view_venta();">
				<option value="">Seleccionar... Nombre o Razon Social</option>
		<?php for ($i=0; $i < count($clientes); $i++) { $cliente=$clientes[$i]; ?>
				<option value="<?php echo $cliente->idcl;?>"><?php echo $cliente->nombre;?></option>
		<?php }//end for ?>
			</select>
		</td>
		<td class='celda celda-sm-15'>
			<input type="date" class="form-control input-sm" id="sv_fe1" onchange="reset_input(this.id)">
		</td>
		<td class='celda celda-sm-15'>
			<input type="date" class="form-control input-sm" id="sv_fe2" onchange="reset_input(this.id)">
		</td>
		<td class="celda celda-sm" style="width:29%;"></td>
		<?php $new="new_venta()"; if($privilegio[0]->mo2c!="1"){ $new="";}?>
		<?php $print="print()"; if($privilegio[0]->mo2p!="1"){ $print="";}?>
		<td class='celda text-right' style="width:13%"><?php $this->load->view('estructura/botones/buscador',['f_buscar'=>"view_venta()",'f_ver'=>"all_venta()",'nuevo'=>'Nuevo Pedido','f_nuevo'=>$new,'f_imprimir'=>$print]);?></td>
	</tr>
</table>