<?php 
	$help1='title="<h4>Nombre de porcentaje de depreciación<h4>" data-content="Ingrese un nombre nombre de porcentaje<strong> de 2 a 100 caracteres con espacios</strong>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help2='title="<h4>Años de vida útil<h4>" data-content="Ingrese una año <strong>de 1 a 999</strong>, se acepta numeros decimales con un maximo de una decimal Ej. 10,5. <b>Este valor es usado en el calculo de depreciación</b>"';
	$help3='title="<h4>Porcentaje de depreciación<h4>" data-content="Ingrese un porcentaje numerico <strong>de 0% a 999%</strong>, se acepta numeros decimales con un maximo de dos decimales Ej. 999,99. <b>Este valor no es usado en el calculo de depreciación, solo es un valor referencial</b>"';
	$popover='data-toggle="popover" data-placement="right" data-trigger="hover"';
	$popover2='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$popover3='data-toggle="popover" data-placement="top" data-trigger="hover"';
?>
<div class="col-md-6 col-xs-12">
<h3>Activo Fijo: Depreciación</h3>
<div class="table-responsive">
	<table class="table table-bordered table-striped table-hover">
		<thead>
		<tr>
			<th width="5%">#</th>
			<th width="45%">
				<div class="input-group col-xs-12">
					<span class="input-group-addon" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					<div class="form-control input-sm" disabled>Nombre</div>
				</div>
			</th>
			<th width="20%">
				<div class="input-group col-xs-12">
					<span class="input-group-addon" <?php echo $popover.$help2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					<div class="form-control input-sm" disabled>Años</div>
				</div>
			</th>
			<th width="20%">
				<div class="input-group col-xs-12">
					<span class="input-group-addon" <?php echo $popover2.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					<div class="form-control input-sm" disabled> %Porcent. </div>
				</div>
			</th>
			<th width="10%"></th>
		</tr>
		</thead>
		<tbody>
		<?php for ($i=0; $i<count($depreciaciones); $i++){$d=$depreciaciones[$i]; $activo=$this->M_activo_fijo->get_row('iddpre',$d->iddpre);
			?>
		<tr>
			<td><?php echo $i+1;?></td>
			<td><form onsubmit="return update_depreciacion('<?php echo $d->iddpre; ?>');"><input type='text' class="form-control input-sm" placeholder="Nombre del Activo Fijo" value='<?php echo $d->nombre;?>' id='nom_d<?php echo $d->iddpre;?>'></form></td>
			<td><form onsubmit="return update_depreciacion('<?php echo $d->iddpre; ?>');"><input type='number' class="form-control input-sm" placeholder="Años de vida útil" value='<?php echo $d->anio;?>' id='anio_d<?php echo $d->iddpre;?>'  step='any' min='0' max='999.9'></form></td>
			<td><form onsubmit="return update_depreciacion('<?php echo $d->iddpre; ?>');"><input type='number' class="form-control input-sm" placeholder="Coeficiente" value='<?php echo $d->porcentaje*100;?>' id='coe_d<?php echo $d->iddpre;?>'  step='any' min='0' max='999.99'></form></td>
			<td><?php $str="";
					if(count($activo)>0){
						$str="<hr><strong class='text-danger'>¡Imposible Eliminar el porcentaje de depreciación, esta siendo usado por ".count($activo)." activo(s) fijo(s).!</strong>";
						$fun="disabled";
					}else{
						$fun="alerta_depreciacion('".$d->iddpre."')";
					}
					$help='title="<h4>'.$d->nombre.'<h4>" data-content="<b>Año de vida útil: </b>'.$d->anio.' años.<br><b>Porcentaje de depreciación: </b>'.number_format(($d->porcentaje*100),2,'.',',').'% '.$str.'"'; 
			?>
				<?php $this->load->view('estructura/botones/botones_registros',['detalle'=>$popover2.$help,'guardar'=>"update_depreciacion('".$d->iddpre."')",'eliminar'=>$fun]);?>
			</td>
		</tr>
		<?php } ?>
		</tbody><thead>
		<tr><th colspan="6" class="text-center">NUEVO</th></tr>
		<tr>
			<td colspan="2">
			<form onsubmit="return save_depreciacion();">
				<div class="input-group col-xs-12">
					<input type='text' class="form-control input-sm" placeholder="Nombre del Activo Fijo" id='nom_d'>
					<span class="input-group-addon" <?php echo $popover3.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
				</div>
			</form>
			</td>
			<td>
			<form onsubmit="return save_depreciacion();">
				<div class="input-group col-xs-12">
					<input type='number' class="form-control input-sm" placeholder="Años de vida útil" id='anio_d' step='any' min='0' max='999.9'>
					<span class="input-group-addon" <?php echo $popover3.$help2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
				</div>
			</form>
			</td>
			<td>
			<form onsubmit="return save_depreciacion();">
				<div class="input-group col-xs-12">
					<input type='number' class="form-control input-sm" placeholder="Coeficiente" id='coe_d' step='any' min='0' max='999.99'>
					<span class="input-group-addon" <?php echo $popover3.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
				</div>
			</form>
			</td>
			<td><?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"save_depreciacion()",'detalle'=>"",'eliminar'=>""]);?></td>
		</tr></thead>
	</table>
</div>			
</div>
<div class="col-md-6 col-xs-12"></div>
<script language='javascript'>$('[data-toggle="popover"]').popover({html:true});</script>