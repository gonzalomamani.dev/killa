<?php 
	$activos=json_decode($activos);
	$contPagina=1;
	$contReg=0;
	$url=base_url().'libraries/img/activos_fijos/miniatura/';
?>
<div class="pagina table-responsive">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="88%"><div class='encabezado_titulo'>REGISTRO DE ACTIVOS FIJOS</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'></span></div>
				</td></tr>
			</table>
		</div>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php if(!isset($v1)){?><th class="celda th" width="5%" >#Item</th><?php } ?>
			<?php if(!isset($v2)){?><th class="celda th" width="6%" >Fotografía</th><?php } ?>
			<?php if(!isset($v3)){?><th class="celda th" width="10%">Código</th><?php } ?>
			<?php if(!isset($v4)){?><th class="celda th" width="16%">Nombre del activo fijo</th><?php } ?>
			<?php if(!isset($v5)){?><th class="celda th" width="8%" >Porcentaje de depreciación anual</th><?php } ?>
			<?php if(!isset($v6)){?><th class="celda th" width="9%" >Años de vida útil</th><?php } ?>
			<?php if(!isset($v7)){?><th class="celda th" width="13%">Fecha de inicio de trabajo</th><?php } ?>
			<?php if(!isset($v8)){?><th class="celda th" width="9%" >Costo inicial(Bs.)</th><?php } ?>
			<?php if(!isset($v9)){?><th class="celda th" width="9%" >Costo actual(Bs.)</th><?php } ?>
			<?php if(!isset($v10)){?><th class="celda th" width="15%">Observaciónes</th><?php } ?>
		</tr>
		<?php $cont=0; $sub_total=0; $total=0;
			foreach ($activos as $key => $activo){ 
				$img='default.png';
				if($activo->fotografia!=NULL && $activo->fotografia!=""){ $img=$activo->fotografia;} ?>
			<?php if($contReg>=$nro){
					$contReg=0;
					$contPagina++;
			?>
		</table><!--cerramos tabla-->
		</div><!--cerramos pagina-->
<div class="pagina table-responsive">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="90%"><div class='encabezado_titulo'>REGISTRO DE ACTIVOS FIJOS</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'></span></div>
				</td></tr>
			</table>
		</div>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php if(!isset($v1)){?><th class="celda th" width="5%" >#Item</th><?php } ?>
			<?php if(!isset($v2)){?><th class="celda th" width="6%" >Fotografía</th><?php } ?>
			<?php if(!isset($v3)){?><th class="celda th" width="10%" >Código</th><?php } ?>
			<?php if(!isset($v4)){?><th class="celda th" width="16%">Nombre del activo fijo</th><?php } ?>
			<?php if(!isset($v5)){?><th class="celda th" width="8%" >Porcentaje de depreciación anual</th><?php } ?>
			<?php if(!isset($v6)){?><th class="celda th" width="9%" >Años de vida útil</th><?php } ?>
			<?php if(!isset($v7)){?><th class="celda th" width="13%">Fecha de inicio de trabajo</th><?php } ?>
			<?php if(!isset($v8)){?><th class="celda th" width="9%" >Costo inicial(Bs.)</th><?php } ?>
			<?php if(!isset($v9)){?><th class="celda th" width="9%" >Costo actual(Bs.)</th><?php } ?>
			<?php if(!isset($v10)){?><th class="celda th" width="15%">Observaciónes</th><?php } ?>
		</tr>
		<?php $sub_total=0;}//end if 
				$costo_actual=$this->lib->costo_depreciacion($activo->costo,$activo->anio,$activo->fecha_inicio_trabajo);
		?>
		<tr class="fila">
			<?php if(!isset($v1)){?><td class="celda td"><?php echo $cont+1;$cont++;?></td><?php } ?>
			<?php if(!isset($v2)){?><td class="celda td"><img src="<?php echo $url.$img;?>" style='width:100%'></td><?php } ?>
			<?php if(!isset($v3)){?><td class="celda td"><?php echo $activo->codigo;?></td><?php } ?>
			<?php if(!isset($v4)){?><td class="celda td"><?php echo $activo->nombre;?></td><?php } ?>
			<?php if(!isset($v5)){?><td class="celda td"><?php echo number_format(($activo->porcentaje*100),2,'.',',')."%";?></td><?php } ?>
			<?php if(!isset($v6)){?><td class="celda td"><?php echo $activo->anio.' año(s)';?></td><?php } ?>
			<?php if(!isset($v7)){?><td class="celda td"><?php echo $activo->fecha_inicio_trabajo;?></td><?php } ?>
			<?php if(!isset($v8)){?><td class="celda td"><?php echo number_format($activo->costo,2,'.',',');?></td><?php } ?>
			<?php if(!isset($v9)){?><td class="celda td"><?php echo number_format($costo_actual,2,'.',',');?></td><?php } ?>
			<?php if(!isset($v10)){?><td class="celda td"><?php echo $activo->observaciones;?></td><?php } ?>
		</tr>
	<?php $contReg++;}// end for ?>
	</table>
</div>
<script type="text/javascript"> $(".nroPagina").html(<?php echo $contPagina;?>); </script>