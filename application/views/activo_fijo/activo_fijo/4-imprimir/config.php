<div class="row-border table-responsive">
	<table class="table table-bordered"><thead>
		<tr>
			<th width="15%">Nro. Reg. por hojas</th>
			<th width="20%">
				<select id="nro" class="form-control input-sm">
				<?php for ($i=1; $i <= 50 ; $i++) { ?>
					<option value="<?php echo $i;?>" <?php if($i==34){echo "selected";}?>><?php echo $i;?></option>
				<?php } ?>
				</select>
			</th>
			<th width="65%"></th>
		</tr>
	</thead></table>
	<div id="config_area">
		<table class="tabla tabla-border-true">
			<tr class="fila">
				<th class="celda th" width="5%" >#Item</th>
				<th class="celda th" width="6%" >Fotografía</th>
				<th class="celda th" width="10%" >Código</th>
				<th class="celda th" width="16%">Nombre del activo fijo</th>
				<th class="celda th" width="8%" >Porcentaje de depreciación</th>
				<th class="celda th" width="9%" >Años de vida útil</th>
				<th class="celda th" width="13%">Fecha de inicio de trabajo</th>
				<th class="celda th" width="9%" >Costo inicial(Bs.)</th>
				<th class="celda th" width="9%" >Costo actual(Bs.)</th>
				<th class="celda th" width="15%">Observaciónes</th>
			</tr>
			<tr class="fila">
			<tr class="fila">
				<th class="celda th"><input type="checkbox" id="1" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="2" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="3" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="4" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="5" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="6" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="7" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="8" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="9" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="10" checked="checked"></th>
			</tr>
			</tr>
		</table>
	</div>
</div>
<div class="row-border">
	<div id="area"></div>
</div>
<script language="javascript">
	$("#1").unbind("click");$("#1").change(function(){ arma_informe('<?php echo $activos;?>'); }); 
	$("#2").unbind("click");$("#2").change(function(){ arma_informe('<?php echo $activos;?>'); }); 
	$("#3").unbind("click");$("#3").change(function(){ arma_informe('<?php echo $activos;?>'); }); 
	$("#4").unbind("click");$("#4").change(function(){ arma_informe('<?php echo $activos;?>'); }); 
	$("#5").unbind("click");$("#5").change(function(){ arma_informe('<?php echo $activos;?>'); }); 
	$("#6").unbind("click");$("#6").change(function(){ arma_informe('<?php echo $activos;?>'); });
	$("#7").unbind("click");$("#7").change(function(){ arma_informe('<?php echo $activos;?>'); }); 
	$("#8").unbind("click");$("#8").change(function(){ arma_informe('<?php echo $activos;?>'); }); 
	$("#9").unbind("click");$("#9").change(function(){ arma_informe('<?php echo $activos;?>'); }); 
	$("#10").unbind("click");$("#10").change(function(){ arma_informe('<?php echo $activos;?>'); });
	$("#nro").unbind("click");$("#nro").change(function(){ arma_informe('<?php echo $activos;?>'); }); 
	Onfocus('nro');
</script>