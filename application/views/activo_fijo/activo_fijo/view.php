<?php 
	$url=base_url().'libraries/img/activos_fijos/miniatura/';
	$j_activos_fijos=json_encode($activos_fijos);
?>
<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th class="g-thumbnail">#Item</th>
			<th class="celda-sm-10">Código</th>
			<th class="celda-sm-20">Nombre</th>
			<th style="width:15%" class="celda-sm">Fecha de inicio de trabajo</th>
			<th style="width:14.5%" class="celda-sm">Costo de inicial(Bs.)</th>
			<th style="width:11.5%" class="celda-sm">Costo Actual(Bs.)</th>
			<th style="width:19%" class="celda-sm">Descripción</th>
			<th style="width:11%"></th>
		</tr>
	</thead>
	<tbody>
<?php 
if(count($activos_fijos)>0){
	for ($i=0; $i < count($activos_fijos); $i++) { $activo_fijo=$activos_fijos[$i];
	$img="default.png";
	if($activo_fijo->fotografia!=NULL && $activo_fijo->fotografia!=""){$img=$activo_fijo->fotografia;}
		$costo_actual=$this->lib->costo_depreciacion($activo_fijo->costo,$activo_fijo->anio,$activo_fijo->fecha_inicio_trabajo); ?>
		<tr>
			<td class="g-thumbnail"><div id="item"><?php echo $i+1;?></div><div class="g-img"><img src="<?php echo $url.$img;?>" class="img-thumbnail g-thumbnail"></div></td>
			<td><?php echo $activo_fijo->codigo;?></td>
			<td><?php echo $activo_fijo->nombre;?></td>
			<td class="celda-sm"><?php echo $activo_fijo->fecha_inicio_trabajo;?></td>
			<td class="celda-sm"><?php echo number_format($activo_fijo->costo,2,'.',',');;?></td>
			<td class="celda-sm"><?php echo number_format($costo_actual,2,'.',',');?></td>
			<td class="celda-sm"><?php echo $activo_fijo->observaciones;?></td>
			<td>
				<?php $mod="configuracion_activo('".$activo_fijo->idaf."')"; if($privilegio[0]->ac1u!="1"){ $mod="";}?>
				<?php $del="confirmar('".$activo_fijo->idaf."')"; if($privilegio[0]->ac1d!="1"){ $del="";}?>
				<?php $this->load->view('estructura/botones/botones_registros',['reportes'=>"reportes_activo('".$activo_fijo->idaf."')",'configuracion'=>$mod,'eliminar'=>$del]);?>
			</td>
		</tr>
<?php }// end for
}else{// end if
	echo "<tr><td colspan='8'><h2>0 registros encontrados...</h2></td></tr>";	
}?>
	</tbody>
</table>
<script language="javascript" type="text/javascript"> 
	$("#print").removeAttr("onclick"); 
	$("#print").unbind("click"); 
	$("#print").click(function(){ imprimir_activos('<?php echo $j_activos_fijos;?>'); }); 
</script>