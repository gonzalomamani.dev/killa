
<?php
	$help1='title="<h4>Subir Fotografía</h4>" data-content="Seleccione una fotografía, preferiblemente una imagen que no sobrepase las dimenciones de <strong>700x700[px]</strong>, para evitar sobre cargar al sistema, y solo es aceptado los formatos *.jpg, *.gif o *.png"';
	$help2='title="<h4>Ingresar nombre de activo fijo<h4>" data-content="Ingrese un Nombre alfanumerico de 3 a 200 caracteres <b>puede incluir espacios</b>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help3='title="<h4>Ingresar codigo de activo fijo<h4>" data-content="Ingrese un Código alfanumerico de 2 a 15 caracteres <b>sin espacios</b>, ademas el codigo solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help4='title="<h4>Costo de activo fijo<h4>" data-content="Ingrese el valor actual en bolivianos del activo fijo, un valor entre entre 0 y 99999,9"';
	$help5='title="<h4>Fecha de inicio de operación<h4>" data-content="Seleccione la fecha cuando el activo fijo inicia sus operaciónes, es decir la fecha cuando inicia a ser usado en la planta de trabajo. en formato 2000-01-31"';
	$help6='title="<h4>% de depreciación<h4>" data-content="Seleccione un porcentaje de depreciación anual del activo fijo, si desea adicionar una nuevo porcentaje de depreciación lo puede hacer en el menu superior (Configuracion)"';
	$help7='title="<h4>Descripcion del activo fijo<h4>" data-content="Ingrese un descripcion alfanumerica de 5 a 700 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas la descripcion solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>

<div class="row">
	<div class="col-sm-3 col-sm-offset-9 col-xs-12"><strong><span class='text-danger'>(*)</span> Campo obligatorio</strong></div>
	<div class="col-sm-2 col-xs-12"><strong>Fotografía: </strong></div>
	<div class="col-sm-10 col-xs-12">
		<div class="input-group col-xs-12">
			<input type="file" id="n_fot" class="form-control input-xs">
			<span class="input-group-addon" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div>

	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Nombre:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<form onsubmit="return save_activo()">
			<div class="input-group col-xs-12">
				<input type="text" placeholder='Nombre del activo fijo' class="form-control input-xs" id="n_nom">
				<span class="input-group-addon" <?php echo $popover.$help2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Código:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<form onsubmit="return save_activo()">
			<div class="input-group">
				<input type="text" placeholder='Código del Bien' class="form-control input-xs" id="n_cod">
				<span class="input-group-addon" <?php echo $popover.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Costo(Bs.)</strong></div>
	<div class="col-sm-4 col-xs-12">
		<div class="input-group">
			<input type="number" placeholder='Costo del activo fijo' class="form-control input-xs" id="n_cos">
			<span class="input-group-addon" <?php echo $popover.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Fecha de inicio de operacion:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<div class="input-group">
			<input type="date" placeholder='2000-12-01' class="form-control input-xs" id="n_fec">
			<span class="input-group-addon" <?php echo $popover.$help5;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> % de depresiación:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<div class="input-group">
			<select id="n_dep" class="form-control input-xs">
				<option value="">Seleccionar...</option>
			<?php
				for ($i=0; $i < count($depreciaciones); $i++) { $d=$depreciaciones[$i];
			?>
				<option value="<?php echo $d->iddpre?>"><?php echo $d->nombre;?></option>
			<?php
				}
			?>
			</select>
			<span class="input-group-addon" <?php echo $popover.$help6;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div><i class='clearfix'></i>
	<div class="col-sm-2 col-xs-12"><strong>Descripción</strong></div>
	<div class="col-sm-10 col-xs-12">
		<div class="input-group col-xs-12">
			<textarea id="n_des" class="form-control input-xs" placeholder='Observaciones del activo fijo'></textarea>
			<span class="input-group-addon" <?php echo $popover.$help7;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div>	
</div>
<script language='javascript'>Onfocus("n_nom");$('[data-toggle="popover"]').popover({html:true});</script>