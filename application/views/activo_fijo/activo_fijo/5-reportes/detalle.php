<?php 
	$url=base_url().'libraries/img/activos_fijos/miniatura/';
	$img="default.png";
	$activo_fijo=$activo_fijo[0];
	if($activo_fijo->fotografia!=NULL && $activo_fijo->fotografia!=""){$img=$activo_fijo->fotografia;}
	$costo_actual=$this->lib->costo_depreciacion($activo_fijo->costo,$activo_fijo->anio,$activo_fijo->fecha_inicio_trabajo);
?>
<div class="row">
	<div class="col-sm-3 col-xs-12 text-center"><img src="<?php echo $url.$img;?>" class="img-thumbnail" alt=""><hr></div>
	<div class="col-sm-9 cols-xs-12">
	<table border="0" class="tabla tabla-border-true">
		<tr class="fila">
			<th class="celda th">Código del activo fijo</th>
			<td class="celda td" colspan="3"><?php echo $activo_fijo->codigo;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Nombre del activo fijo:</th><td class="celda td" colspan="3"><?php echo $activo_fijo->nombre;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th" width="17.5%">Fecha de inicio de trabajo:</th><td class="celda td" width="17.5%"><?php echo $activo_fijo->fecha_inicio_trabajo;?></td>
			<th class="celda th" width="17.5%">Tipo de Depreciación:</th><td class="celda td" width="17.5%"><?php echo $activo_fijo->nombre_depreciacion;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Porcentaje de depreciación:</th><td class="celda td" width="17.5%"><?php echo number_format(($activo_fijo->porcentaje*100),2,'.',',').'%';?></td>
			<th class="celda th">Año de vida útil:</th><td class="celda td" width="17.5%"><?php echo $activo_fijo->anio." año(s)";?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Costo inicial(Bs.):</th><td class="celda td" width="17.5%"><?php echo number_format($activo_fijo->costo,2,'.',',').' Bs.';?></td>
			<th class="celda th">Costo actual(Bs.):</th><td class="celda td" width="17.5%"><?php echo number_format($costo_actual,2,'.',',');?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Observaciones:</th><td class="celda td" colspan="3"><?php echo $activo_fijo->observaciones;?></td>
		</tr>
	</table>
	</div>
</div>