<table class="tabla tabla-border-false">
	<tr class="fila">
		<td class='celda g-thumbnail'><div class="g-img"></div></td>
		<td class='celda celda-sm-10'><form onsubmit="return view_activo()"><input type="search" id="s_codigo" placeholder='Código de activo fijo' class="form-control input-sm" onkeyup="reset_all(this.id)"></form></td>
		<td class='celda celda-sm-20'><form onsubmit="return view_activo()"><input type="search" id="s_nombre" placeholder='Nombre de activo fijo' class="form-control input-sm" onkeyup="reset_all(this.id)"></form></td>
		<td class="celda-sm" style="width:14%"><input type="date" id="s_fecha" placeholder='2000-01-31' class="form-control input-sm" onchange="reset_all(this.id)"></td>
		<td class="celda-sm" style="width:14%"><form onsubmit="return view_activo()"><input type="search" id="s_costo" class="form-control input-sm" onkeyup="reset_all(this.id)" placeholder="Costo de inicial del activo fijo"></form></td>
		<td class="celda-sm" style="width:28%"></td>
		<td class="celda td" style="width:14%">
			<?php $new="new_activo()"; if($privilegio[0]->ac1c!="1"){ $new="";}?>
			<?php $print="print()"; if($privilegio[0]->ac1p!="1"){ $print="";}?>
			<?php $this->load->view('estructura/botones/buscador',['f_buscar'=>"view_activo()",'f_ver'=>"all_activo()",'nuevo'=>'Nuevo Activo Fijo','f_nuevo'=>$new,'f_imprimir'=>$print]);?>
		</td>
	</tr>
</table>
<script type="text/javascript">Onfocus('s_codigo');</script>