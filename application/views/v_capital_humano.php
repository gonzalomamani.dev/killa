<?php
    $cargo=$this->session->userdata("cargo");
    if($this->session->userdata("nombre2")!=""){
		$usuario=$this->session->userdata("nombre")." ".$this->session->userdata("nombre2")." ".$this->session->userdata("paterno");
	}else{
		$usuario=$this->session->userdata("nombre")." ".$this->session->userdata("paterno");
	}
    $ci=$this->session->userdata("ci");
?>						
<!DOCTYPE html>
<html lang="es" class="no-js">
	<head><?php $this->load->view('estructura/head',['title'=>'Capital Humano','css'=>'']);?></head>
	<body>
		<?php $this->load->view('estructura/modal');?>
		<div class="contenedor">
			<?php $this->load->view('estructura/menu_izq',['usuario'=>$usuario,'cargo'=>$cargo,'ventana'=>'capital_humano','privilegio'=>$privilegio[0]]);?>
			<?php $v_menu="";
				if($privilegio[0]->ca1r==1){ $v_menu.="Capital Humano/empleado/icon-assignment_ind|"; }
				if($privilegio[0]->ca2r==1){ $v_menu.="Planillas/planilla/icon-archive2|";}
				if($privilegio[0]->ca3r==1){ $v_menu.="Configuraciones/config/glyphicon glyphicon-cog";}
			?>
			<?php $this->load->view('estructura/menu_top',['usuario'=>$usuario,'cargo'=>$cargo,'menu'=>$v_menu]);?>
			<div id="search"></div>
			<div id="contenido"></div>
		</div>
		<?php $this->load->view('estructura/js',['js'=>'capital_humano/capital_humano.js']);?>
	</body>
<?php switch($pestania){
		case '1': if($privilegio[0]->ca1r==1){$title="Empleados"; $activo='empleado'; $search="capital_humano/search_empleado"; $view="capital_humano/view_empleado";} break;
		case '2': if($privilegio[0]->ca2r==1){$title="Planilla"; $activo='planilla'; $search="capital_humano/search_planilla"; $view="capital_humano/view_planilla";} break;
		case '5': if($privilegio[0]->ca3r==1){$title="Configuración capital humano"; $activo='config'; $search=""; $view="capital_humano/view_config";} break;
} ?>
	<script type="text/javascript">activar('<?php echo $activo;?>','<?php echo $title;?>','capital_humano?p=<?php echo $pestania;?>'); get_2n('<?php echo $search; ?>',{},'search',false,'<?php echo $view; ?>',{},'contenido',true); </script>
</html>