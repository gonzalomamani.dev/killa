<?php $opciones=explode("|", $menu);

  $url=base_url()."libraries/img/personas/miniatura/";
  $img="avatar.png"; if($this->session->userdata('fotografia')!=""){ $img=$this->session->userdata('fotografia');}
  $admin="";if($this->session->userdata('administrador')==1){$admin="Administrador";}
?>
<div id="logo-top"></div>
<nav class="navbar-fixed-top g-nav-blue" >
<div id="menu-izq-logo"></div>
      <ul id="menu-top" class="g-nav g-nav-left">
      <?php for ($i=0; $i < count($opciones) ; $i++) { $opcion=explode("/", $opciones[$i]);
     // echo "cou: ".count($opcion)." ".$opcion[0];
          if(count($opcion)>1){
            if(isset($opcion[2])){$icon=$opcion[2];}else{$icon="";}
      ?>
      	<li <?php if($i==0){?>class="activado"<?php }?>><a href="javascript:" id="<?php echo $opcion[1];?>"><i class="<?php echo $icon;?>"></i> <span id="txt_btn" <?php if($i!=0){?>class="g-btn-sm"<?php }?>><?php echo $opcion[0];?></span></a></li>
      <?php
          }
      }?>
      </ul>
<?php if($usuario!="nada_taller" && $cargo!="nada_taller"){ ?>   
	<div class="nav navbar-nav pull-right">
        <a href='javascript:' class="dropdown-toggle" data-toggle="dropdown">
            <img src="<?php echo $url.$img; ?>" class="img-circle img-responsive g-img-nav img-thumbnail" width="30px;">
        </a>
        <div id="view_session_user" class="dropdown-menu dropdown-user">
          <div class="list-group">
            <a href="#" class="list-group-item active">
              <?php echo $this->session->userdata('nombre').' '.$this->session->userdata('paterno');?><br><?php echo  $admin?>
            </a>
            <figure>
              <img src="<?php echo $url.$img?>" width='40%' class="img-circle img-responsive img-thumbnail">
              <figcaption>
                <?php echo $this->session->userdata('cargo');?>
              </figcaption>
            </figure>
            <button type="button" class="list-group-item" onclick="form_cambiar_password(<?php echo $this->session->userdata('id')?>)">Cambiar contraseña</button>
            <button type="button" class="list-group-item" onclick="window.location.href='<?php echo base_url().'login/logout';?>'" href="">Cerrar session</button>
          </div>
        </div>
	</div> 
<?php } ?>
  </nav>