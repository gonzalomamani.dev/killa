 <?php if($usuario!='nada_taller' & $cargo!='nada_taller'){
  $menu=array();
  $h=array();
  if(isset($privilegio)){
    if($privilegio->al==1){
      if($privilegio->al1r==1){$h[]=array('nombre' => 'Ver Almacenes', 'pestania' => 'almacen?p=1', 'icon' => 'icon-office');}
      if($privilegio->al2r==1){$h[]=array('nombre' => 'Movimientos de Material', 'pestania' => 'almacen?p=2', 'icon' => 'glyphicon glyphicon-stats');}
      if($privilegio->al3r==1){$h[]=array('nombre' => 'Movimientos de Producto', 'pestania' => 'almacen?p=3', 'icon' => 'icon-graph');}
      $menu[]=array('active' => $ventana,'nombre' => 'Almacen','pestania' => 'almacen','icon' => 'icon-office', 'hijos' => $h);
    }
    if($privilegio->pr==1){
      $h=array();
      if($privilegio->pr1r==1){$h[]=array('nombre' => 'Productos', 'pestania' => 'produccion', 'icon' => 'icon-suitcase');}
      if($privilegio->pr2r==1){$h[]=array('nombre' => 'Orden de producción', 'pestania' => 'produccion?p=2', 'icon' => 'icon-clipboard2');}
      if($privilegio->pr3r==1){$h[]=array('nombre' => 'Configuración', 'pestania' => 'produccion?p=5', 'icon' => 'glyphicon glyphicon-cog');}
      $menu[]=array('active' => $ventana,'nombre' => 'Producción','pestania' => 'produccion','icon' => 'icon-factory', 'hijos' => $h);
    }
    if($privilegio->mo==1){
      $h=array();
      if($privilegio->mo1r==1){$h[]=array('nombre' => 'Pedidos', 'pestania' => 'pedidos_ventas?p=1', 'icon' => 'icon-clipboard2');}
      if($privilegio->mo2r==1){$h[]=array('nombre' => 'Ventas', 'pestania' => 'pedidos_ventas?p=2', 'icon' => 'icon-clipboard');}
      if($privilegio->mo3r==1){$h[]=array('nombre' => 'Compras o egresos', 'pestania' => 'pedidos_ventas?p=3', 'icon' => 'icon-cart');}
      $menu[]=array('active' => $ventana,'nombre' => 'Movimientos','pestania' => 'pedidos_ventas','icon' => 'icon-stats2', 'hijos' => $h);
    }
    if($privilegio->ca==1){
      $h=array();
      if($privilegio->ca1r==1){$h[]=array('nombre' => 'Empleados', 'pestania' => 'capital_humano?p=1', 'icon' => 'icon-assignment_ind');}
      if($privilegio->ca2r==1){$h[]=array('nombre' => 'Planillas', 'pestania' => 'capital_humano?p=2', 'icon' => 'icon-archive2');}
      if($privilegio->ca3r==1){$h[]=array('nombre' => 'Configuración', 'pestania' => 'capital_humano?p=5', 'icon' => 'glyphicon glyphicon-cog');}
      $menu[]=array('active' => $ventana,'nombre' => 'Capital humano','pestania' => 'capital_humano','icon' => 'icon-assignment_ind','hijos' => $h);
    }
    if($privilegio->cl==1){
      $h=array();
      if($privilegio->cl1r==1){$h[]=array('nombre' => 'Clientes', 'pestania' => 'cliente_proveedor', 'icon' => 'icon-profile');}
      if($privilegio->cl2r==1){$h[]=array('nombre' => 'Proveedores', 'pestania' => 'cliente_proveedor?p=2', 'icon' => 'icon-addressbook');}
      if($privilegio->cl3r==1){$h[]=array('nombre' => 'Configuración', 'pestania' => 'cliente_proveedor?p=3', 'icon' => 'glyphicon glyphicon-cog');}
      $menu[]=array('active' => $ventana,'nombre' => 'Cliente/Proveedor','pestania' => 'cliente_proveedor','icon' => 'icon-book2','hijos' => $h);
    }
    if($privilegio->ac==1){
      $h=array();
      if($privilegio->ac1r==1){$h[]=array('nombre' => 'Ver activos fijos', 'pestania' => 'activos_fijos', 'icon' => 'icon-truck2');}
      if($privilegio->ac2r==1){$h[]=array('nombre' => 'Configuración', 'pestania' => 'activos_fijos?p=2', 'icon' => 'glyphicon glyphicon-cog');}
      $menu[]=array('active' => $ventana,'nombre' => 'Activos Fijos','pestania' => 'activos_fijos','icon' => 'icon-truck2','hijos' => $h);
    }
    if($privilegio->ot==1){
      $h=array();
      if($privilegio->ot1r==1){$h[]=array('nombre' => 'Materiales adicionales', 'pestania' => 'insumo', 'icon' => 'icon-box2');}
      if($privilegio->ot2r==1){$h[]=array('nombre' => 'Otros materiales e insumos', 'pestania' => 'insumo?p=2', 'icon' => 'icon-cabinet2');}
      if($privilegio->ot3r==1){$h[]=array('nombre' => 'Configuración', 'pestania' => 'insumo?p=3', 'icon' => 'glyphicon glyphicon-cog');}
      $menu[]=array('active' => $ventana,'nombre' => 'Otros materiales e insumos','pestania' => 'insumo','icon' => 'icon-box','hijos' => $h);
    }
    if($privilegio->co==1){
      $h=array();
      if($privilegio->co1r==1){$h[]=array('nombre' => 'Comprobantes', 'pestania' => 'contabilidad?p=1', 'icon' => 'icon-documents');}
      if($privilegio->co2r==1){$h[]=array('nombre' => 'Libro mayor', 'pestania' => 'contabilidad?p=2', 'icon' => 'glyphicon glyphicon-book');}
      if($privilegio->co3r==1){$h[]=array('nombre' => 'Sumas y saldos', 'pestania' => 'contabilidad?p=3', 'icon' => 'icon-layout');}
      if($privilegio->co4r==1){$h[]=array('nombre' => 'Estado de costo P. y V.', 'pestania' => 'contabilidad?p=4', 'icon' => 'icon-clipboard2');}
      if($privilegio->co5r==1){$h[]=array('nombre' => 'Estado de Resultados', 'pestania' => 'contabilidad?p=5', 'icon' => 'icon-book');}
      if($privilegio->co6r==1){$h[]=array('nombre' => 'Balance General', 'pestania' => 'contabilidad?p=6', 'icon' => 'icon-paste');}
      if($privilegio->co7r==1){$h[]=array('nombre' => 'Plan de cuentas', 'pestania' => 'contabilidad?p=7', 'icon' => 'icon-file-text2');}
      if($privilegio->co8r==1){$h[]=array('nombre' => 'Configuración', 'pestania' => 'contabilidad?p=10', 'icon' => 'glyphicon glyphicon-cog');}
      $menu[]=array('active' => $ventana,'nombre' => 'Contabilidad','pestania' => 'contabilidad','icon' => 'icon-calculator2','hijos' => $h);
    }
    if($privilegio->ad==1){
      $h=array();
      if($privilegio->ad1r==1){$h[]=array('nombre' => 'Usuarios de sistema', 'pestania' => 'administrador?p=1', 'icon' => 'icon-user');}
      if($privilegio->ad2r==1){$h[]=array('nombre' => 'Privilegios', 'pestania' => 'administrador?p=2', 'icon' => 'icon-unlocked');}
      if($privilegio->ad3r==1){$h[]=array('nombre' => 'Configuración', 'pestania' => 'administrador?p=7', 'icon' => 'glyphicon glyphicon-cog');}
      $menu[]=array('active' => $ventana,'nombre' => 'Administracion','pestania' => 'administrador','icon' => 'icon-lock','hijos' => $h);
    }
  }
  $menu=json_decode(json_encode($menu));
  /*$menu=json_decode(json_encode(array(array('active' => $ventana,'nombre' => 'Almacen','pestania' => 'almacen','icon' => 'icon-office',
                                                        'hijos' => array(array('nombre' => 'Ver Almacenes', 'pestania' => 'almacen?p=1', 'icon' => 'icon-office'),
                                                                        array('nombre' => 'Movimientos de Material', 'pestania' => 'almacen?p=2', 'icon' => 'glyphicon glyphicon-stats'),
                                                                        array('nombre' => 'Movimientos de Producto', 'pestania' => 'almacen?p=3', 'icon' => 'icon-graph')
                                                            )),
                                          array('active' => $ventana,'nombre' => 'Producción','pestania' => 'produccion','icon' => 'icon-factory',
                                                        'hijos' => array(array('nombre' => 'Productos', 'pestania' => 'produccion', 'icon' => 'icon-suitcase'),
                                                                  array('nombre' => 'Orden de producción', 'pestania' => 'produccion?p=2', 'icon' => 'icon-clipboard2'),
                                                                  array('nombre' => 'Configuración', 'pestania' => 'produccion?p=5', 'icon' => 'glyphicon glyphicon-cog'),
                                                          )),
                                          array('active' => $ventana,'nombre' => 'Movimientos','pestania' => 'pedidos_ventas','icon' => 'icon-stats2',
                                                    'hijos' => array(array('nombre' => 'Pedidos', 'pestania' => 'pedidos_ventas?p=1', 'icon' => 'icon-clipboard2'),
                                                                     array('nombre' => 'Ventas', 'pestania' => 'pedidos_ventas?p=2', 'icon' => 'icon-clipboard'),
                                                                     array('nombre' => 'Compras o egresos', 'pestania' => 'pedidos_ventas?p=3', 'icon' => 'icon-cart')
                                                            )),
                                          array('active' => $ventana,'nombre' => 'Capital humano','pestania' => 'capital_humano','icon' => 'icon-assignment_ind',
                                                        'hijos' => array(array('nombre' => 'Empleados', 'pestania' => 'capital_humano', 'icon' => 'icon-assignment_ind'),
                                                                        array('nombre' => 'Planillas', 'pestania' => 'capital_humano?p=2', 'icon' => 'icon-archive2'),
                                                                        array('nombre' => 'Configuración', 'pestania' => 'capital_humano?p=5', 'icon' => 'glyphicon glyphicon-cog')
                                                            )),
                                          array('active' => $ventana,'nombre' => 'Cliente/Proveedor','pestania' => 'cliente_proveedor','icon' => 'icon-book2',
                                                        'hijos' => array(array('nombre' => 'Clientes', 'pestania' => 'cliente_proveedor', 'icon' => 'icon-profile'),
                                                                        array('nombre' => 'Proveedores', 'pestania' => 'cliente_proveedor?p=2', 'icon' => 'icon-addressbook'),
                                                                        array('nombre' => 'Configuración', 'pestania' => 'cliente_proveedor?p=3', 'icon' => 'glyphicon glyphicon-cog')
                                                            )),
                                          array('active' => $ventana,'nombre' => 'Activos Fijos','pestania' => 'activos_fijos','icon' => 'icon-truck2',
                                                        'hijos' => array(array('nombre' => 'Ver activos fijos', 'pestania' => 'activos_fijos', 'icon' => 'icon-truck2'),
                                                                        array('nombre' => 'Configuración', 'pestania' => 'activos_fijos?p=2', 'icon' => 'glyphicon glyphicon-cog')
                                                            )),
                                          array('active' => $ventana,'nombre' => 'Otros materiales e insumos','pestania' => 'insumo','icon' => 'icon-box',
                                                        'hijos' => array(array('nombre' => 'Materiales adicionales', 'pestania' => 'insumo', 'icon' => 'icon-box2'),
                                                                        array('nombre' => 'Otros materiales e insumos', 'pestania' => 'insumo?p=2', 'icon' => 'icon-cabinet2'),
                                                                        array('nombre' => 'Configuración', 'pestania' => 'insumo?p=3', 'icon' => 'glyphicon glyphicon-cog')
                                                            )),
                                          array('active' => $ventana,'nombre' => 'Contabilidad','pestania' => 'contabilidad','icon' => 'icon-calculator2',
                                                        'hijos' => array(array('nombre' => 'Comprobantes', 'pestania' => 'contabilidad?p=1', 'icon' => 'icon-documents'),
                                                                        array('nombre' => 'Libro mayor', 'pestania' => 'contabilidad?p=2', 'icon' => 'glyphicon glyphicon-book'),
                                                                        array('nombre' => 'Sumas y saldos', 'pestania' => 'contabilidad?p=3', 'icon' => 'icon-layout'),
                                                                        array('nombre' => 'Estado de costo P. y V.', 'pestania' => 'contabilidad?p=4', 'icon' => 'icon-clipboard2'),
                                                                        array('nombre' => 'Estado de Resultados', 'pestania' => 'contabilidad?p=5', 'icon' => 'icon-book'),
                                                                        array('nombre' => 'Balance General', 'pestania' => 'contabilidad?p=6', 'icon' => 'icon-paste'),
                                                                        array('nombre' => 'Plan de cuentas', 'pestania' => 'contabilidad?p=7', 'icon' => 'icon-file-text2'),
                                                                        array('nombre' => 'Configuración', 'pestania' => 'contabilidad?p=10', 'icon' => 'glyphicon glyphicon-cog')
                                                            )),
                                          array('active' => $ventana,'nombre' => 'Administracion','pestania' => 'administrador','icon' => 'icon-lock',
                                                        'hijos' => array(array('nombre' => 'Usuarios de sistema', 'pestania' => 'administrador?p=1', 'icon' => 'icon-user'),
                                                                        array('nombre' => 'Privilegios', 'pestania' => 'administrador?p=2', 'icon' => 'icon-unlocked'),
                                                                        array('nombre' => 'Configuración', 'pestania' => 'administrador?p=7', 'icon' => 'glyphicon glyphicon-cog')
                                                            ))
                                    )));*/
?>
<div id="menu">
    <div id="logo-principal"></div>
            <div class="g-menu-izq">
                <div class="g-menu-izq-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                    <?php foreach($menu as $clave => $fila){  //echo count($valor->hijos); echo "<br>$c<br>";$c++;?>
                            <li>
                              <!--<a href="<?php echo base_url().$fila->pestania;?>" <?php if($fila->active==$fila->pestania){ echo "class='active'"; }?>><i class="fa fa-fw <?php echo $fila->icon;?>"></i><?php echo $fila->nombre;?> <?php if(!empty($fila->hijos)){?><span class="glyphicon fa arrow"></span><?php } ?>-->
                              <a href="javascript:" <?php if($fila->active==$fila->pestania){ echo "class='active'"; }?>><i class="fa fa-fw <?php echo $fila->icon;?>"></i><?php echo $fila->nombre;?> <?php if(!empty($fila->hijos)){?><span class="glyphicon fa arrow"></span><?php } ?>
                            </a>
                            <?php if(!empty($fila->hijos)){ ?>
                            <ul class="nav nav-second-level">
                            <?php $hijos=json_decode(json_encode($fila->hijos));
                                foreach ($hijos as $key => $fila_hijo) { ?>
                                    <li><a href="<?php echo base_url().$fila_hijo->pestania;?>"><i class="fa fa-fw <?php echo $fila_hijo->icon;?>"></i><?php echo $fila_hijo->nombre;?></a>
                                <?php } ?>
                            </ul>
                            <?php } ?>
                    <?php } ?>
                    </ul>
                </div>
            </div>
</div>
<?php }?>
