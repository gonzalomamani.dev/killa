
<div id="alerta" style="display:none;">
  <button type="button" class="close" onclick="close_alerta('alerta')">&times;</button>
  <img src="" id="alerta_img" style="width:30px;">
  <span id='alerta_text'></span>
</div>

<!-- Modal Bootstrap Nivel 1-->
<div class="modal fade" id="modal_1" role="dialog">
    <div id="dialog_1">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h3 class="modal-title titulo_1"></h3>
          </div>
          <div class="modal-body" id="content_1"></div>
          <div class="modal-footer">
          <div class="row">
            <div id="modal_ok_1" class="hidden"><button onclick="" class="btn btn-success col-xs-12">Guardar</button></div>
            <div id="modal_print_1" class="hidden"><button onclick="" class="btn btn-info col-xs-12">Imprimir</button></div>
            <div id="modal_closed_1" class="col-sm-3 col-sm-offset-9 col-xs-12"><button type="button" data-dismiss="modal" class="btn btn-danger col-xs-12">Cerrar</button></div>
          </div>           
          </div>
        </div>
    </div>
</div>
<!-- Modal Nivel 2-->
<div class="row" id="modal_2">
  <div id="dialog_2">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close closed_2" data-dismiss="modal">&times;</button>
          <h3 class="modal-title titulo_2"></h3>
        </div>
          <div class="modal-body" id="content_2"></div>
            <div class="modal-footer">
              <div class="row">
                <div id="modal_ok_2" class="hidden"><button onclick="" class="btn btn-success col-xs-12">Guardar</button></div>
                <div id="modal_print_2" class="hidden"><button onclick="" class="btn btn-info col-xs-12">Imprimir</button></div>
                <div id="modal_closed_2" class="col-sm-3 col-sm-offset-9 col-xs-12"><button type="button" data-dismiss="modal" class="btn btn-danger col-xs-12">Cerrar</button></div>
              </div>  
            </div>
        </div>
  </div>
</div>
<!-- Modal Nivel 3-->
<div class="row" id="modal_3">
     <div id="dialog_3">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close closed_3" data-dismiss="modal">&times;</button>
              <h3 class="modal-title titulo_3"></h3>
          </div>
          <div class="modal-body" id="content_3"></div>
            <div class="modal-footer">
              <div class="row">
                <div id="modal_ok_3" class="hidden"><button onclick="" class="btn btn-success col-xs-12">Guardar</button></div>
                <div id="modal_print_3" class="hidden"><button onclick="" class="btn btn-info col-xs-12">Imprimir</button></div>
                <div id="modal_closed_3" class="col-sm-3 col-sm-offset-9 col-xs-12"><button type="button" data-dismiss="modal" class="btn btn-danger col-xs-12">Cerrar</button></div>
              </div>  
            </div>
        </div>
    </div>
</div>
<!-- Modal Nivel 3-->
<div class="row" id="modal_4">
     <div id="dialog_4">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close closed_4" data-dismiss="modal">&times;</button>
              <h3 class="modal-title titulo_4"></h3>
          </div>
          <div class="modal-body" id="content_4"></div>
            <div class="modal-footer">
              <div class="row">
                <div id="modal_ok_4" class="hidden"><button onclick="" class="btn btn-success col-xs-12">Guardar</button></div>
                <div id="modal_print_4" class="hidden"><button onclick="" class="btn btn-info col-xs-12">Imprimir</button></div>
                <div id="modal_closed_4" class="col-sm-3 col-sm-offset-9 col-xs-12"><button type="button" data-dismiss="modal" class="btn btn-danger col-xs-12">Cerrar</button></div>
              </div>  
            </div>
        </div>
    </div>
</div>
<!--Modal Nivel 5 Validadcion-->
<div class="row" id="modal_5">
  <div id="dialog_5">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close closed_5" data-dismiss="modal">&times;</button>
        <h3 class="modal-title titulo_5"></h3>
      </div>
      <div class="modal-body" id="content_5"></div>
        <div class="modal-footer">
          <div class="row">
            <div id="modal_ok_5" class="col-sm-4 col-sm-offset-4 col-xs-12"><button onclick="" class="btn btn-success col-xs-12">Aceptar</button></div>
            <div id="modal_closed_5" class="col-sm-4 col-xs-12 btn-cerrar"><button type="button" data-dismiss="modal" class="btn btn-danger col-xs-12">Cancelar</button></div>
          </div>  
        </div>
        </div>
    </div>
</div>
<!--Modal Nivel 5 Validadcion-->
<div class="row" id="modal_6">
  <div id="dialog_6">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close closed_6" data-dismiss="modal">&times;</button>
        <h3 class="modal-title titulo_6"></h3>
      </div>
      <div class="modal-body" id="content_6"></div>
        <div class="modal-footer">
          <div class="row">
            <div id="modal_ok_6" class="col-sm-4 col-sm-offset-4 col-xs-12"><button onclick="" class="btn btn-success col-xs-12">Aceptar</button></div>
            <div id="modal_closed_6" class="col-sm-4 col-xs-12 btn-cerrar"><button type="button" data-dismiss="modal" class="btn btn-danger col-xs-12">Cancelar</button></div>
          </div>  
        </div>
        </div>
    </div>
</div>





<div class="alert alert-success" id="alerta_ok" style="display:none;">
  <button type="button" class="close" onclick="cerrar_alerta('alerta_ok')">&times;</button>
  <span id='alerta_ok_text'></span>
</div>
<div class="alert alert-danger" id="alerta_error" style="display:none;">
  <button type="button" class="close" onclick="cerrar_alerta('alerta_error')">&times;</button>
  <span id="alerta_error_text"></span>
</div>
<div class="alert alert-warning" id="alerta_fail" style="display:none;">
  <button type="button" class="close" onclick="cerrar_alerta('alerta_fail')">&times;</button>
  <span id="alerta_fail_text"></span>
</div>