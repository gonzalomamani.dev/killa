<?php
	$p_nuevo="";
	$btn="";
	if(isset($popover_n)){
		if($popover_n!=""){
			$p_nuevo=$popover_n;
		}else{
			$btn="btn-group";
		}
	}else{
		$btn="btn-group";
	}
?>
<div class="<?php echo $btn;?> btn-group-sm btn-group-justified">
	<?php if(isset($f_buscar)){ if($f_buscar!=""){?><a href="javascript:" class="btn btn-celeste" onclick="<?php echo $f_buscar;?>" title="Buscar"><span class="glyphicon glyphicon-search"></span></a><?php } }?>
	<?php if(isset($f_ver)){ if($f_ver!=""){?><a href="javascript:" class="btn btn-info g-btn-sm" onclick="<?php echo $f_ver;?>" title='Ver Todo'><span class="glyphicon glyphicon-list"></span></a><?php } }?>
	<?php if(isset($f_nuevo)){ if($f_nuevo!=""){?><a href="javascript:" class="btn btn-success2 " <?php echo $p_nuevo;?> onclick="<?php echo $f_nuevo;?>" title='<?php echo $nuevo;?>'><span class="glyphicon glyphicon-plus"></span></a><?php } }?>
	<?php if(isset($f_imprimir)){ if($f_imprimir!=""){?><a href="javascript:" id="print" class="btn btn-warning" onclick="<?php echo $f_imprimir;?>" title='Configuración de Impresión'><span class="glyphicon glyphicon-print"></span></a><?php } }?>
</div>