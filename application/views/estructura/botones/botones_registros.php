<?php 
	$btn="";
	$border_ini="";
	$border_center="";
	$border_fin="";
	if(isset($detalle)){ 
		if($detalle==""){
			$btn="btn-group";
		}else{
			$border_ini="border-ini";
			$border_center="border-center";
			$border_fin="border-fin";
		}
	}else{
		$btn="btn-group";
	}
?>
<div class="<?php echo $btn;?> btn-group-sm btn-group-justified">
	<?php if(isset($detalle)){ if($detalle!=""){?><a href="javascript:" class="btn btn-default <?php echo $border_ini;?>" <?php echo $detalle;?> title="Guardar"><i class='glyphicon glyphicon-info-sign'></i></a><?php }}?>
	<?php if(isset($reportes)){ if($reportes!=""){?><a href="javascript:" class="btn btn-primary <?php echo $border_center;?>" onclick="<?php echo $reportes;?>" title="Reportes"><i class="glyphicon glyphicon-list-alt"></i></a><?php }}?>
	<?php if(isset($reset)){ if($reset!=""){?><a href="javascript:" class="btn btn-success2 <?php echo $border_center;?>" onclick="<?php echo $reset;?>" title="Volver los datos a su estado original"><i class="glyphicon glyphicon-repeat"></i></a><?php }}?>
	<?php if(isset($guardar)){ if($guardar!=""){?><a href="javascript:" class="btn btn-celeste <?php echo $border_center;?>" onclick="<?php echo $guardar;?>" title="Guardar"><i class="glyphicon glyphicon-save"></i></a><?php }}?>
	<?php if(isset($configuracion)){ if($configuracion!=""){?><a href="javascript:" class="btn btn-celeste <?php echo $border_center;?>" onclick="<?php echo $configuracion;?>" title="Configuración"><i class="glyphicon glyphicon-cog"></i></a><?php }} ?>
	<?php if(isset($eliminar)){ if($eliminar!=""){?><a href="javascript:" class="btn btn-danger <?php if($eliminar=='disabled'){ echo "disabled";}?> <?php echo $border_fin;?>" onclick="<?php echo $eliminar;?>" title="Eliminar"><i class="glyphicon glyphicon-trash"></i></a><?php }} ?>
</div>
