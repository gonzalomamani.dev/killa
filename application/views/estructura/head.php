<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="mobile-web-app-capable" content="yes">
<title><?php echo $title;?></title>
<meta name="description" content="" />
<meta name="keywords" content="marroquineria, cuero, maletines, billeteras" />
<meta name="author" content="gmg.software.developer@gmail.com" />

<link rel="icon" type="image/png" href="<?php echo base_url(); ?>favicon.png" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css/icomon.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css/bootstrap.css">
<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>libraries/css/style.css" />-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>libraries/css/estilo.css" />
<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css/encabezado.css">-->

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css/print.css" media='print'>
<meta name="theme-color" content="#34679a">
<?php if ($css!="") {
	?>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css/<?php echo $css;?>">	
	<?php
	
}?>

