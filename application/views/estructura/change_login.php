<?php echo $empleado->usuario." ¿desea cambiar tu contraseña?";
  $help2="Ingrese su contraseña evitando usar letras mayusculas";
  $popover2='data-toggle="popover" data-placement="left" data-trigger="hover" title="<h4>Ingrese su contraseña actual<h4>" data-content="'.$help2.'"';
  $help3="Ingresa tu nueva contraseña evitando usar letras mayusculas de 4 a 25 caracteres alfanuméricos";
  $popover3='data-toggle="popover" data-placement="left" data-trigger="hover" title="<h4>Nueva contraseña<h4>" data-content="'.$help3.'"';
  $help4="Repite tu nueva contraseña evitando usar letras mayusculas";
  $popover4='data-toggle="popover" data-placement="left" data-trigger="hover" title="<h4>Repite tu contraseña<h4>" data-content="'.$help4.'"';
  $img="default.png";
  $url=base_url().'libraries/img/personas/miniatura/';
  if($empleado->fotografia!=NULL && $empleado->fotografia!=""){ $img=$empleado->fotografia;}
?>
<div class="text-center">
	<img class="img-resposive img-thumbnail" src="<?php echo $url.$img;?>">
</div>
<hr>
	<div class="form-group">
		<div class="input-group input-group-xs">
			<span class="input-group-addon" style='background-color: #fff;'><i class='glyphicon glyphicon-lock'></i></span>
			<input type="password" id="e_pass" placeholder='Ingresa tu contraseña actual' class="form-control input-xs">
			<span class="input-group-addon input-sm" <?php echo $popover2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div>
<hr>
	<div class="form-group">
		<div class="input-group input-group-xs">
			<span class="input-group-addon" style='background-color: #fff;'><i class='glyphicon glyphicon-lock'></i></span>
			<input type="password" id="new_pass" placeholder='Ingresa tu nueva contraseña' class="form-control input-xs">
			<span class="input-group-addon input-sm" <?php echo $popover3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div>
	<div class="form-group">
		<div class="input-group input-group-xs">
			<span class="input-group-addon" style='background-color: #fff;'><i class='glyphicon glyphicon-lock'></i></span>
			<input type="password" id="rep_pass" placeholder='Repite tu nueva contraseña' class="form-control input-xs">
			<span class="input-group-addon input-sm" <?php echo $popover4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div>

	<script language='javascript'>Onfocus("e_pass");$('[data-toggle="popover"]').popover({html:true});</script>
