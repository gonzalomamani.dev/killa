<?php echo "¿Desea ".$titulo."?";
  $help1="Ingrese nombre de usuario, Tenga en cuenta que el sistema de validacion no distingue entre mayuscula y minuscula Ej. (Juan != juan)";
  $popover1='data-toggle="popover" data-placement="left" data-trigger="hover" title="<h4>Nombre de Usuario</h4>" data-content="'.$help1.'"';
  $help2="Ingrese su contraseña evitando usar letras mayusculas";
  $popover2='data-toggle="popover" data-placement="left" data-trigger="hover" title="<h4>Ingresar Contraseña<h4>" data-content="'.$help2.'"';

?>
<div class="text-center">
<?php if(isset($img)){ ?>    
	<img class="img-resposive img-thumbnail" src="<?php echo $img;?>">
<?php } ?>
</div>
<?php
	echo "<strong>$desc</strong>";
	if(!isset($control)){?>
	<div class="form-group"><hr>
	<div class="input-group input-group-xs">
		<span class="input-group-addon" style='background-color: #fff;'><i class='glyphicon glyphicon-user'></i></span>
		<input type="text" id="e_user" placeholder='Ingresa tu nombre de usuario' class="form-control input-xs">
		<span class="input-group-addon input-sm" <?php echo $popover1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
	</div>
	</div>
	<div class="form-group">
		<div class="input-group input-group-xs">
		<span class="input-group-addon" style='background-color: #fff;'><i class='glyphicon glyphicon-lock'></i></span>
		<input type="password" id="e_password" placeholder='Ingresa tu contraseña' class="form-control input-xs">
		<span class="input-group-addon input-sm" <?php echo $popover2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
	</div>	
	</div>
	<script language='javascript'>Onfocus("e_user");$('[data-toggle="popover"]').popover({html:true});</script>	
<?php	}else{
	if($open_control=="true"){
		if(!empty($control)){
			for ($i=0; $i < count($control) ; $i++) { 
				echo "<br>- ".$control[$i]->nombre;
			}
		}
	}
}
?>
