<?php
	$cargo=$this->session->userdata("cargo");
	if($this->session->userdata("nombre2")!=""){
		$usuario=$this->session->userdata("nombre")." ".$this->session->userdata("nombre2")." ".$this->session->userdata("paterno");
	}else{
		$usuario=$this->session->userdata("nombre")." ".$this->session->userdata("paterno");
	}
    $ci=$this->session->userdata("ci");
?>					
<!DOCTYPE html>
<html lang="es" class="no-js">
	<head><?php $this->load->view('estructura/head',['title'=>'Activos Fijos','css'=>'']);?></head>
	<body>
		<?php $this->load->view('estructura/modal');?>
		<div class="contenedor">
			<?php $this->load->view('estructura/menu_izq',['usuario'=>$usuario,'cargo'=>$cargo,'ventana'=>'activos_fijos','privilegio'=>$privilegio[0]]);?>
			<?php $v_menu="|Configuración/configuracion/glyphicon glyphicon-cog"?>
			<?php $v_menu="";
				if($privilegio[0]->ac1r==1){ $v_menu.="Activos Fijos/activo_fijo/icon-truck2|"; }
				if($privilegio[0]->ac2r==1){ $v_menu.="Configuración/configuracion/glyphicon glyphicon-cog";}
			?>
			<?php $this->load->view('estructura/menu_top',['usuario'=>$usuario,'cargo'=>$cargo,'menu'=>$v_menu]);?>
			<div id="search"></div>
			<div id="contenido"></div>
		</div>
		<?php $this->load->view('estructura/js',['js'=>'activo_fijo/activo_fijo.js']);?>
	</body>
	<?php 
		switch ($pestania) {
			case '1': $activo='activo_fijo'; $search="activos_fijos/search_activo"; $view="activos_fijos/view_activo"; break;
			case '2': $activo='configuracion'; $search=""; $view="activos_fijos/view_config"; break;
		}
	?>
	<script type="text/javascript">activar('<?php echo $activo;?>'); get_2n('<?php echo $search; ?>',{},'search',false,'<?php echo $view; ?>',{},'contenido',true); </script>
</html>