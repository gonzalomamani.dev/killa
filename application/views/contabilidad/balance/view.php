<?php 
	$v=array();
	for($i=0; $i < count($estructura) ; $i++) { $e=$estructura[$i];
		
			$fin=0;
			$sum=0;
			$est_ctas=$this->M_estado_cuentas->get_cta_estructura($e->idee);
			if(!empty($est_ctas)){
				for($j=0; $j < count($cuentas) ; $j++) { $cuenta=$cuentas[$j];
					for ($k=0; $k < count($est_ctas) ; $k++) { 
						$fin=strlen($est_ctas[$k]->codigo);
						if($est_ctas[$k]->codigo==substr($cuenta->codigo, 0,$fin)){
							$sum+=abs($cuenta->debe-$cuenta->haber);
						}
					}
				}
			$v[$e->idee.'']=$sum;
			}
			
	}
?>
<div class="container">
	<div class="row row-border">
		<table class="tabla tabla-border-false">
			<thead>
				<tr class="fila">
					<th class="celda th" colspan="5">
						<h3><ins>BALANCE GENERAL</ins></h3>
							Del <?php echo $this->validaciones->formato_fecha($fecha1,'d,m,Y');?> al <?php echo $this->validaciones->formato_fecha($fecha2,'d,m,Y');?>
					</th>
				</tr>
			</thead>
			<tbody>
			<tr class="fila">
				<td class="celda td" width="10%"></td>
				<td class="celda td" width="45%"></td>
				<td class="celda td" width="15%"></td>
				<td class="celda td" width="15%"></td>
				<td class="celda td" width="15%"></td>
			</tr>
			<?php for ($i=0; $i < count($estructura) ; $i++) { $e=$estructura[$i]; 
						$est_ctas=$this->M_estado_cuentas->get_cta_estructura($e->idee);
						?>
				<tr class="fila"><td class="celda td" width="10%"></td>
				<td class="celda td" width="45%">
					<ins><?php if($e->t_visible==1){ echo $e->titulo;}?></ins>
				</td>
				<?php if(!empty($est_ctas)){ // en caso si el grupo tiene cuentas o no tiene cuentas ?>
				<td class="celda td" width="15%"></td>
				<td class="celda td" width="15%"></td>
				<td class="celda td" width="15%"></td>
				</tr>
					<?php
						$sum_col=0;
						for ($j=0; $j < count($cuentas); $j++) { $cuenta=$cuentas[$j];
							for ($k=0; $k < count($est_ctas) ; $k++) {
								$fin=strlen($est_ctas[$k]->codigo);
								if($est_ctas[$k]->codigo==substr($cuenta->codigo, 0,$fin)){?>
								<tr class="fila">
									<td class="celda td"><?php if($e->s_visible==1 && $k==0){ if($e->signo==0){ echo "mas:";}else{ echo "menos:";}}?></td>
									<td class="celda td"><?php echo $cuenta->cuenta_text; ?></td>
									<td class="celda td"><?php if($e->columna==0){ $monto=abs($cuenta->debe-$cuenta->haber); echo $monto; $sum_col+=$monto;} ?></td>
									<td class="celda td"><?php if($e->columna==1){ $monto=abs($cuenta->debe-$cuenta->haber); echo $monto; $sum_col+=$monto;} ?></td>
									<td class="celda td"><?php if($e->columna==2){ $monto=abs($cuenta->debe-$cuenta->haber); echo $monto; $sum_col+=$monto;} ?></td>
								</tr>
					<?php 		}//end if
						  	}//end for
						}// end for 
					?>
					<tr class="fila">
									<th class="celda td"></th>
									<th class="celda td"></th>
									<th class="celda td"><span class='g-subrayado-top'><?php if($e->columna==0){ echo $sum_col;} ?></span></th>
									<th class="celda td"><span class='g-subrayado-top'><?php if($e->columna==1){ echo $sum_col;} ?></span></th>
									<th class="celda td"><span class='g-subrayado-top'><?php if($e->columna==2){ echo $sum_col;} ?></span></th>
					</tr>
				<?php }else{ 
						$grus=$this->M_estado_operacion->get_row('idee1',$e->idee);
						$monto=0;
						for ($gr=0; $gr < count($grus); $gr++){ $gru=$grus[$gr];
							if($gru->signo==0){
								$monto+=$v[$gru->idee2];
							}else{
								$monto-=$v[$gru->idee2];
							}
						}
					?>
							<th class="celda td"><span class='g-subrayado-top'><?php if($e->columna==0){ echo $monto; $sum_col=$monto;} ?></span></th>
							<th class="celda td"><span class='g-subrayado-top'><?php if($e->columna==1){ echo $monto; $sum_col=$monto;} ?></span></th>
							<th class="celda td"><span class='g-subrayado-top'><?php if($e->columna==2){ echo $monto; $sum_col=$monto;} ?></span></th>
					</tr>
				<?php 
						$v[$e->idee.'']=$sum_col;
					}
				 ?>
			<?php 	
				}// end for 
			?>
			</tbody>
		</table>
	</div>
</div>
<script language="javascript" type="text/javascript"> 
	$("#print").removeAttr("onclick"); $("#print").unbind("click"); 
	$("#print").click(function(){ imprimir_estado('<?php echo $fecha1; ?>','<?php echo $fecha2; ?>','2'); }); 
</script>