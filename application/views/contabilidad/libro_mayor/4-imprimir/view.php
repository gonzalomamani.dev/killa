<?php 
	$cuentas=json_decode($cuentas);
	$contPagina=1;
	$contReg=0;
	$cols_cabecera=0; if(!isset($v1)){ $cols_cabecera++;} if(!isset($v2)){ $cols_cabecera++;} if(!isset($v3)){ $cols_cabecera++;} if(!isset($v4)){ $cols_cabecera++;} if(!isset($v5)){ $cols_cabecera++;}
	$cols_total=0; if(!isset($v1)){ $cols_total++;} if(!isset($v2)){ $cols_total++;} if(!isset($v3)){ $cols_total++;}
?>
<div class="pagina table-responsive">
	<?php $this->load->view('estructura/print/encabezado',['titulo'=>'LIBRO MAYOR','pagina'=>$contPagina,'sub_titulo'=>'Del '.$this->validaciones->formato_fecha($fecha1,'d,m,Y').' al '.$this->validaciones->formato_fecha($fecha2,'d,m,Y')]);?>
	<table class="tabla tabla-border-true">
<?php 
	$cont=0;
	foreach($cuentas as $key => $cuenta){ 
		$debe=0; $haber=0;
		$detalles=$this->M_detalle_comprobante->get_group_cuentas($fecha1,$fecha2,false,$cuenta->idpl);//sacamos las cuentas no agruapdas de una cuenta
		if($contReg>=$nro){
			$contReg=0;
			$contPagina++;
?>
	</table>
</div>
<div class="pagina table-responsive">
	<?php $this->load->view('estructura/print/encabezado',['titulo'=>'LIBRO MAYOR','pagina'=>$contPagina,'sub_titulo'=>'']);?>
	<table class="tabla tabla-border-true">
<?php } ?>
		<tr class="fila"><th class="celda td" colspan="<?php echo $cols_cabecera;?>"><?php echo $cuenta->codigo.': '.$this->lib->all_mayuscula($cuenta->cuenta_text);?></th>
		</tr>
<?php	for($d=0; $d<count($detalles); $d++){ $detalle=$detalles[$d]; 
			if($contReg>=$nro){
				$contReg=0;
				$contPagina++;
?>
	</table>
</div>
<div class="pagina table-responsive">
	<?php $this->load->view('estructura/print/encabezado',['titulo'=>'LIBRO MAYOR','pagina'=>$contPagina,'sub_titulo'=>'']);?>
	<table class="tabla tabla-border-true">
	<?php }//end if?>
		<tr class="fila">
			<?php if(!isset($v1)){?><td class="celda td" width="4%"><?php echo $d+1;?></td><?php }?>
			<?php if(!isset($v2)){?><td class="celda td" width="8%"><?php echo $this->lib->format_date($detalle->fecha,'d/m/Y');?></td><?php }?>
			<?php if(!isset($v3)){?><td class="celda td" width="70%"><?php echo $detalle->referencia;?></td><?php }?>
			<?php if(!isset($v4)){?><td class="celda td" width="9%"><?php echo number_format($detalle->debe,2,'.',','); $debe+=$detalle->debe;?></td><?php }?>
			<?php if(!isset($v5)){?><td class="celda td" width="9%"><?php echo number_format($detalle->haber,2,'.',','); $haber+=$detalle->haber;?></td><?php }?>
		</tr>
	<?php $contReg++;}//end for detalles 
		$dif=$this->conta->deudor_acreedor($cuenta->idpl,$debe,$haber);
	?>

		<tr class="fila">
			<?php if($cols_total>0){?><th class="celda td text-right" colspan="<?php echo $cols_total;?>">TOTAL (Bs.)</th><?php } ?>
			<?php if(!isset($v4)){?><th class="celda td text-right"><?php echo number_format($debe,2,'.',',');?></th><?php } ?>
			<?php if(!isset($v5)){?><th class="celda td text-right"><?php echo number_format($haber,2,'.',',');?></th><?php } ?>
		</tr>
		<tr class="fila">
			<?php if($cols_total>0){?><th class="celda td text-right" colspan="<?php echo $cols_total;?>">DIFERENCIA (Bs.)</th><?php } ?>
			<?php if(!isset($v4)){?><th class="celda td text-right"><?php if($dif>0){ echo number_format($dif,2,'.',',');}else{ echo '0.00';}?></th><?php } ?>
			<?php if(!isset($v5)){?><th class="celda td text-right"><?php if($dif<0){ echo number_format(abs($dif),2,'.',',');}else{ echo '0.00';}?></th><?php } ?>
		</tr>	
<?php }// end for cuentas?>
	</table>
</div>
<script type="text/javascript"> $(".nroPagina").html(<?php echo $contPagina;?>); </script>