<?php $j_cuentas=json_encode($cuentas);?>
<table class="table table-striped table-hover table-bordered">
	<thead>
		<tr><th colspan="13" style="text-align:center;"><h3><ins>LIBRO MAYOR</ins></h3>Del <?php echo $this->validaciones->formato_fecha($fecha1,'d,m,Y');?> al <?php echo $this->validaciones->formato_fecha($fecha2,'d,m,Y');?></th></tr>
	</thead>
<?php 
if(count($cuentas)>0){
	for ($i=0; $i < count($cuentas) ; $i++) { $cuenta=$cuentas[$i]; 
		//$cta=$this->M_plan_cuenta->get($cuenta->idpl); ?>
		<thead>
			<tr>
				<th colspan="4">
					<h3><?php echo $cuenta->codigo.': '.$cuenta->cuenta_text;?></h3>
				</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th width="15%">Fecha</th>
				<th width="65%">Referencia</th>
				<th width="10%">Debe (Bs.)</th>
				<th width="10%">Haber (Bs.)</th>
			</tr>
	<?php 
		$detalles=$this->M_detalle_comprobante->get_group_cuentas($fecha1,$fecha2,false,$cuenta->idpl);//sacamos las cuentas no agruapdas de una cuenta
		$debe=0;$haber=0;
		for($d=0; $d<count($detalles); $d++) { $detalle=$detalles[$d]; ?>
			<tr>
				<td><?php echo $this->lib->format_date($detalle->fecha,'d ml Y');?></td>
				<td><?php echo $detalle->referencia;?></td>
				<td><?php echo number_format($detalle->debe,2,'.',','); $debe+=$detalle->debe;?></td>
				<td><?php echo number_format($detalle->haber,2,'.',','); $haber+=$detalle->haber;?></td>
			</tr>
	<?php } ?>
			</tbody>
			<thead>
				<tr>
					<th colspan="2" class="text-right">Total (Bs.):</th>
					<th><?php echo $debe;?></th>
					<th><?php echo $haber;?></th>
				</tr>
				<?php $dif=$this->conta->deudor_acreedor($cuenta->idpl,$debe,$haber);?>
				<tr>
					<th colspan="2" class="text-right">Diferencia (Bs.):</th>
					<th><?php if($dif>0){ echo $dif;}else{ echo 0;}?></th>
					<th><?php if($dif<0){ echo abs($dif);}else{ echo 0;}?></th>
				</tr>
			</thead>
<?php } //en for cuentas
}else{
	echo "<tr><th colspan='4'><h3>0 registros encontrados...</h3></th></tr>";
}
?>
</table>
<script language="javascript" type="text/javascript"> 
	$("#print").removeAttr("onclick"); $("#print").unbind("click"); 
	$("#print").click(function(){ imprimir_mayor('<?php echo $j_cuentas;?>','<?php echo $fecha1; ?>','<?php echo $fecha2; ?>'); }); 
</script>
