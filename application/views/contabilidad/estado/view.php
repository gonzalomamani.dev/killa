<?php 
	$v=array();
	$ecpv=array();
	for($i=0; $i < count($estructura) ; $i++){ $e=$estructura[$i];
		$fin=0;
		$sum=0;
		$est_ctas=$this->M_estado_cuentas->get_cta_estructura($e->idee);
		if(!empty($est_ctas)){
			for ($j=0; $j < count($est_ctas) ; $j++) { //cuentas en el grupo
				$fin=strlen($est_ctas[$j]->codigo);
				for($k=0; $k < count($cuentas) ; $k++) { $cuenta=$cuentas[$k];
					if($est_ctas[$j]->codigo==substr($cuenta->codigo, 0,$fin)){
						$sum+=abs($cuenta->debe-$cuenta->haber);
					}
				}
			}
			switch ($e->tipo_estado) {
				case '0': $ecpv[$e->idee.'']=$sum; break;// para armar y buscar valor de Estado de costo de produccion de los vendido
				case '1': $v[$e->idee.'']=$sum; break;//para armar estado de resultados
			}
		}else{
			//no hay cuentas en el grupo
		}
	}
	$costo_ecpv=0;
	for($i=0; $i < count($estructura) ; $i++){ $e=$estructura[$i];
		$fin=0;
		$sum=0;
		//buscando cuentas en el grupo
		if($e->tipo_estado==0){
			$est_ctas=$this->M_estado_cuentas->get_cta_estructura($e->idee);
			for ($j=0; $j < count($est_ctas) ; $j++) { //cuentas en el grupo
				$fin=strlen($est_ctas[$j]->codigo);
				for($k=0; $k < count($cuentas) ; $k++) { $cuenta=$cuentas[$k];
					if($est_ctas[$j]->codigo==substr($cuenta->codigo, 0,$fin)){
						$sum+=abs($cuenta->debe-$cuenta->haber);
					}
				}
			}
			//buscando operaciones en el grupo
			$grus=$this->M_estado_operacion->get_row('idee1',$e->idee);
			if(!empty($grus)){
				for ($gr=0; $gr < count($grus); $gr++){ $gru=$grus[$gr];
					
					if($gru->signo==0){
						$sum+=$ecpv[$gru->idee2];
					}else{
						$sum-=$ecpv[$gru->idee2];
					}
				}
				$ecpv[$e->idee.'']=$sum;
			}
			$costo_ecpv=$sum;
		}
	}
?>
<div class="container">
	<div class="row row-border">
		<div class="col-xs-12">
			<table class="tabla tabla-border-false">
				<thead>
					<tr class="fila">
						<th class="celda th" colspan="4">
							<h3><ins>ESTADO DE RESULTADOS</ins></h3>
							Del <?php echo $this->validaciones->formato_fecha($fecha1,'d,m,Y');?> al <?php echo $this->validaciones->formato_fecha($fecha2,'d,m,Y');?>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr class="fila">
						<td class="celda td" width="10%"></td>
						<td class="celda td" width="50%"></td>
						<td class="celda td" width="20%"></td>
						<td class="celda td" width="20%"></td>
					</tr>
			<?php
				for($i=0; $i < count($estructura) ; $i++) { $e=$estructura[$i];
					$fin=0;
					if($e->tipo_estado==1){
					$est_ctas=$this->M_estado_cuentas->get_cta_estructura($e->idee);
			?>
				<tr class="fila"><td class="celda td" width="10%"></td>
					<td class="celda td" width="45%">
						<ins><?php if($e->t_visible==1){ echo $e->titulo;}?></ins>
					</td>

			<?php
					$sum=0;
					if(!empty($est_ctas)){?>
					<td class="celda td"></td>
					<td class="celda td"></td>
				</tr>
			<?php for($j=0; $j < count($cuentas) ; $j++) { $cuenta=$cuentas[$j];
							for ($k=0; $k < count($est_ctas) ; $k++) { 
								$fin=strlen($est_ctas[$k]->codigo);
								if($est_ctas[$k]->codigo==substr($cuenta->codigo, 0,$fin)){
									//$sum+=abs($cuenta->debe-$cuenta->haber);
								?>
								<tr class="fila">
									<td class="celda td"><?php if($e->s_visible==1 && $k==0){ if($e->signo==0){ echo "mas:";}else{ echo "menos:";}}?></td>
									<td class="celda td"><?php echo $cuenta->cuenta_text; ?></td>
									<td class="celda td"><?php if($e->columna==0){ $monto=abs($cuenta->debe-$cuenta->haber); echo $monto; $sum+=$monto;} ?></td>
									<td class="celda td"><?php if($e->columna==1){ $monto=abs($cuenta->debe-$cuenta->haber); echo $monto; $sum+=$monto;} ?></td>
									<td class="celda td"><?php if($e->columna==2){ $monto=abs($cuenta->debe-$cuenta->haber); echo $monto; $sum+=$monto;} ?></td>
								</tr>
						<?php	}
							}//end for
						}//end for
					?>
					<tr class="fila">
						<th class="celda td"></th>
						<th class="celda td"></th>
						<th class="celda td"><span class='g-subrayado-top'><?php if($e->columna==0){ echo $sum;} ?></span></th>
						<th class="celda td"><span class='g-subrayado-top'><?php if($e->columna==1){ echo $sum;} ?></span></th>
					</tr>
			<?php }//end if
					else{
						$grus=$this->M_estado_operacion->get_row('idee1',$e->idee);
						$monto=0;
						if(!empty($grus)){// no hay operaciones de en el grupo
							for ($gr=0; $gr < count($grus); $gr++){ $gru=$grus[$gr];
								if($gru->signo==0){
									$monto+=$v[$gru->idee2];
								}else{
									$monto-=$v[$gru->idee2];
								}
								
							}
						}else{//buscando si es valor de Costo de produccion de los vendido
							if($e->ecpv==1){ $monto=$costo_ecpv;}
						}
			?>
						<th class="celda td"><span class='g-subrayado-top'><?php if($e->columna==0){ echo $monto; $sum=$monto;} ?></span></th>
						<th class="celda td"><span class='g-subrayado-top'><?php if($e->columna==1){ echo $monto; $sum=$monto;} ?></span></th>
					</tr>
				<?php
						}
						$v[$e->idee.'']=$sum;
					}// end if
				}//end for
			?>
				</tbody>
			</table>
		</div>

	</div>
</div>
<script language="javascript" type="text/javascript"> 
	$("#print").removeAttr("onclick"); $("#print").unbind("click"); 
	$("#print").click(function(){ imprimir_estado('<?php echo $fecha1; ?>','<?php echo $fecha2; ?>','1'); }); 
</script>