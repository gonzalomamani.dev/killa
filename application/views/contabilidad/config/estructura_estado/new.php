<?php
	$help1='title="<h4>Número de pedido<h4>" data-content="Representa al número de referencia del pedido, este numero es unico y generado automaticamente por el sistema, si fuese el caso de que el numero fuera ya registrado puede pulsar el boton actualizar el número."';
	$help2='title="<h4>Cliente<h4>" data-content="Seleccione el cliente que realiza el pedido, si desea adicionar un nuevo cliente lo puede hacer accediendo por el menu principal izquierdo <b>Cliente/Proveedor</b>, o dando click en el boton <b>+</b>"';
	$help3='title="<h4>Fecha de pedido<h4>" data-content="Ingrese la fecha de registro de este pedido."';
	$help4='title="<h4>Observaciónes del pedido<h4>" data-content="Ingrese algunas observaciónes del pedido si tuviera. El contenido de las observaciones debe ser en formato alfanumérico hasta 900 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help5='title="<h4>Fecha de creación<h4>" data-content="Seleccione la fecha cuando se creo o finalizo el diseño del producto. en formato 2000-01-31"';
	$help6='title="<h4>Costo de pedido según sistema<h4>" data-content="Costo total del pedido según las cantidades y el costo del producto según el cliente, <b>Para poder actualizar el costo debe dar click en el boton Calcular</b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<div class="row">
	<div class="col-xs-12"><strong><ins>Detalles del titulo del grupo</ins></strong></div>
	<div class="col-md-3 col-xs-12">
		<div class="input-group">
			<select class="form-control input-sm" id="signo">
				<option value="0">mas:</option>
				<option value="1">menos:</option>
			</select>
			<span class="input-group-addon input-sm" <?php echo $popover.$help1;?>><i class="glyphicon glyphicon-info-sign"></i></span>
		</div>
	</div>
	<div class="col-md-1 col-xs-12">
		<input type="checkbox" id="sig_vis"> visible
	</div>
	<div class="col-md-7 col-xs-12">
		<div class="input-group">
			<input type="text" class="form-control input-sm" placeholder='Titulo de division (no obligatorio)' id="nom_cta_group">
			<span class="input-group-addon input-sm" <?php echo $popover.$help1;?>><i class="glyphicon glyphicon-info-sign"></i></span>
		</div>
	</div>
	<div class="col-md-1 col-xs-12">
		<input id="nom_vis" type="checkbox"> visible
	</div>
	<div class="col-xs-12"><hr></div>
	<div class="col-xs-12"><strong><ins>Detalles del cuentas en el grupo</ins></strong></div>
	<div class="col-sm-4 col-md-2 col-xs-12"><label>Columna 1:</label> <input type="radio" value="0" name="co" title="columna 1" checked=""></div>
	<div class="col-sm-4 col-md-2 col-xs-12"><label>Columna 2:</label> <input type="radio" value="1" name="co" title="columna 2"></div>
	<div class="col-xs-12"><input type="checkbox" id="ecpv"> Cuenta de Estado de costo de Producción de los Vendido</div>
	<div class="col-xs-12">
		<div class="table-responsive">
			<table id="table-np" class="table table-bordered table-striped table-hover">
				<thead>
					<th width="10%">#Item</th>
					<th width="85%">Cuenta</th>
					<td width="5%">
						<!--<button class="btn btn-celeste btn-sm col-xs-12" onclick="adicionar_row_cuenta();" title="Adicionar Fila"><span class='glyphicon glyphicon-save'></span></button>-->
					</td>
				</thead>
				<tbody id="content_cuentas"></tbody>
				<thead>
					<th colspan="4">
						<div class="col-md-3 col-md-offset-9 col-sm-4 col-sm-offset-8 col-xs-12">
							<button class="btn btn-info col-xs-12" onclick="add_row_cuenta_epv($(this));"><span class='glyphicon glyphicon-plus'></span> Adicionar Registro</button>
						</div>
					</th>
				</thead>
			</table>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>
	<div class="col-xs-12"><strong><ins>Detalles del operaciones con otros grupos</ins></strong></div>
	<div class="col-xs-12">
		<div class="table-responsive">
			<table id="table-np" class="table table-bordered table-striped">
				<thead>
					<th width="9%">#Item</th>
					<th width="15%">Operacion</th>
					<th width="70%">Grupo del estado</th>
					<th width="6%"></th>
				</thead>
				<tbody id="content_operacion"></tbody>
				<thead>
					<th colspan="4">
						<div class="col-md-3 col-md-offset-9 col-sm-4 col-sm-offset-8 col-xs-12">
							<button class="btn btn-info col-xs-12" onclick="add_row_grupo_epv('1',$(this));"><span class='glyphicon glyphicon-plus'></span> Adicionar grupo</button>
						</div>
					</th>
				</thead>
			</table>
		</div>
	</div>
</div>
<script language='javascript'>Onfocus("cli");$('[data-toggle="popover"]').popover({html:true});</script>


<!--
<div class="row row-border">
	<table class="tabla">
		<tr class="fila">
			<td class="celda td" colspan="2">
				<input type="text" class="form-control input-xs" placeholder='Titulo de division (no obligatorio)' id="nom_cta_group">
			</td>
			<td class="celda td" colspan="2">
				<input id="nom_vis" type="checkbox"> Texto visible
			</td>
		</tr>
		<tr class="fila">
			<td class="celda td" width="35%">
				<select class="form-control input-xs" id="signo">
					<option value="0">mas(+)</option>
					<option value="1">menos(-)</option>
				</select>
			</td>
			<td class="celda td" width="35%">
				<input type="checkbox" id="sig_vis"> Signo literal visible
			</td>
			<th width="10%"><input type="radio" class="form-control input-sm" value="0" name="co" title="columna 1" checked=""></th>
			<th width="10%"><input type="radio" class="form-control input-sm" value="1" name="co" title="columna 2"></th>
		</tr>
		<tr class="fila">
			<td class="celda td" colspan="1"></td>	
			<td class="celda td" colspan="3">
				<input type="checkbox" id="ecpv"> Cuenta de Estado de costo de Producción de los Vendido
			</td>
			
		</tr>
	</table>
</div>
<div class="row row-border">
	<div class="col-xs-12">
		<div class="table-responsive">
			<table id="table-np" class="table table-bordered table-striped">
				<thead>
					<th width="9%">#Item</th>
					<th width="25%">Código</th>
					<th width="55%">Cuenta</th>
					<td width="6%">
						<button class="btn btn-celeste btn-sm col-xs-12" onclick="adicionar_row_cuenta();" title="Adicionar Fila"><span class='glyphicon glyphicon-save'></span></button>
					</td>
				</thead>
				<tbody id="content_cuentas"></tbody>
				<thead>
					<th colspan="3"></th>
					<th>
						<button class="btn btn-danger btn-sm col-xs-12" onclick="drop_row('content_cuentas')" title="Eliminar fila"><span class='glyphicon glyphicon-trash'></span></button>
					</th>
				</thead>
			</table>
		</div>
		
	</div>
</div>
<div class="row row-border">
	<div class="col-xs-12">
		<div class="table-responsive">
			<table id="table-np" class="table table-bordered table-striped">
				<thead>
					<th width="9%">#Item</th>
					<th width="15%">Operacion</th>
					<th width="70%">Grupo del estado</th>
					<th width="6%">
						<button class="btn btn-celeste btn-sm col-xs-12" onclick="adicionar_row_grupo('1');" title="Adicionar Fila"><span class='glyphicon glyphicon-save'></span></button>
					</th>
				</thead>
				<tbody id="content_operacion"></tbody>
				<thead>
					<th colspan="3"></th>
					<th>
						<button class="btn btn-danger btn-sm col-xs-12" onclick="drop_row('content_operacion');" title="Eliminar ultima fila"><span class='glyphicon glyphicon-trash'></span></button>
					</th>
				</thead>
			</table>
		</div>
	</div>
</div>-->