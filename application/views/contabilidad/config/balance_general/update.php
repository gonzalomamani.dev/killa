<?php
	$estado=$estado[0];
	$help1='title="<h4>Número de pedido<h4>" data-content="Representa al número de referencia del pedido, este numero es unico y generado automaticamente por el sistema, si fuese el caso de que el numero fuera ya registrado puede pulsar el boton actualizar el número."';
	$help2='title="<h4>Cliente<h4>" data-content="Seleccione el cliente que realiza el pedido, si desea adicionar un nuevo cliente lo puede hacer accediendo por el menu principal izquierdo <b>Cliente/Proveedor</b>, o dando click en el boton <b>+</b>"';
	$help3='title="<h4>Fecha de pedido<h4>" data-content="Ingrese la fecha de registro de este pedido."';
	$help4='title="<h4>Observaciónes del pedido<h4>" data-content="Ingrese algunas observaciónes del pedido si tuviera. El contenido de las observaciones debe ser en formato alfanumérico hasta 900 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help5='title="<h4>Fecha de creación<h4>" data-content="Seleccione la fecha cuando se creo o finalizo el diseño del producto. en formato 2000-01-31"';
	$help6='title="<h4>Costo de pedido según sistema<h4>" data-content="Costo total del pedido según las cantidades y el costo del producto según el cliente, <b>Para poder actualizar el costo debe dar click en el boton Calcular</b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<div class="row">
	<div class="col-xs-12"><strong><ins>Detalles del titulo del grupo</ins></strong></div>
	<div class="col-md-3 col-xs-12">
		<div class="input-group">
			<select class="form-control input-sm" id="signo">
				<option value="0" <?php if($estado->signo==0){ echo "selected";}?>>mas:</option>
				<option value="1" <?php if($estado->signo==1){ echo "selected";}?>>menos:</option>
			</select>
			<span class="input-group-addon input-sm" <?php echo $popover.$help1;?>><i class="glyphicon glyphicon-info-sign"></i></span>
		</div>
	</div>
	<div class="col-md-1 col-xs-12">
		<input type="checkbox" id="sig_vis"  <?php if($estado->s_visible==1){ echo "checked";} ?>> visible
	</div>
	<div class="col-md-7 col-xs-12">
		<div class="input-group">
			<input type="text" class="form-control input-sm" placeholder='Titulo de division (no obligatorio)' id="nom_cta_group" value="<?php echo $estado->titulo;?>">
			<span class="input-group-addon input-sm" <?php echo $popover.$help1;?>><i class="glyphicon glyphicon-info-sign"></i></span>
		</div>
	</div>
	<div class="col-md-1 col-xs-12">
		<input id="nom_vis" type="checkbox" <?php if($estado->t_visible==1){ echo "checked";} ?>> visible
	</div>
	<div class="col-xs-12"><hr></div>
	<div class="col-xs-12"><strong><ins>Detalles del cuentas en el grupo</ins></strong></div>
	<div class="col-sm-4 col-md-2 col-xs-12"><label>Columna 1:</label> <input type="radio" value="0" name="co" title="columna 1" <?php if($estado->columna==0){ echo "checked";}?>></div>
	<div class="col-sm-4 col-md-2 col-xs-12"><label>Columna 2:</label> <input type="radio" value="1" name="co" title="columna 2" <?php if($estado->columna==1){ echo "checked";}?>></div>
	<div class="col-xs-12">
		<div class="table-responsive">
			<table id="table-np" class="table table-bordered table-striped table-hover">
				<thead><tr>
					<th width="10%">#Item</th>
					<th width="85%">Cuenta</th>
					<td width="5%"></td></tr>
				</thead>
				<?php $estado_cuentas=$this->M_estado_cuentas->get_cta_estructura($estado->idee);?>
				<tbody id="content_cuentas">
				<?php for ($i=0; $i < count($estado_cuentas); $i++) { $estado_cuenta=$estado_cuentas[$i];?>
					<tr>
						<td id="auto-num"><input type="hidden" id="pos<?php echo $i+1;?>" value="<?php echo $i+1;?>"></td>
						<td><select class="form-control input-sm" id="cta<?php echo $i+1;?>">
							<option value="">Seleccione...</option>
						<?php
						for ($c=0; $c < count($cuentas) ; $c++) { $cuenta=$cuentas[$c]; ?>
							<option value="<?php echo $cuenta->idpl;?>" <?php if($estado_cuenta->idpl==$cuenta->idpl){ echo "selected";}?> <?php if($cuenta->tipo!=1){?> style='font-weight: bold; text-decoration: underline;'<?php }?>><?php echo $cuenta->codigo.' '.$cuenta->cuenta_text; ?></option>
						<?php } ?>
						</select>
						</td>
						<td><?php $this->load->view('estructura/botones/botones_registros',['eliminar'=>"drop_fila($(this));"]);?></td>
					</tr>
				<?php } ?>
				</tbody>
				<thead><tr>
					<th colspan="4">
						<div class="col-md-3 col-md-offset-9 col-sm-4 col-sm-offset-8 col-xs-12">
							<button class="btn btn-info col-xs-12" onclick="add_row_cuenta_epv($(this));"><span class='glyphicon glyphicon-plus'></span> Adicionar Registro</button>
						</div>
					</th></tr>
				</thead>
			</table>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>
	<div class="col-xs-12"><strong><ins>Detalles del operaciones con otros grupos</ins></strong></div>
	<div class="col-xs-12">
		<div class="table-responsive">
			<table id="table-np" class="table table-bordered table-striped">
				<thead>
					<th width="9%">#Item</th>
					<th width="15%">Operacion</th>
					<th width="70%">Grupo del estado</th>
					<th width="6%"></th>
				</thead>
				<tbody id="content_operacion">
				<?php for ($i=0; $i < count($operaciones); $i++) { $operacion=$operaciones[$i];?>
					<tr>
						<td id="auto-num"><input type="hidden" id="pos<?php echo $i+1;?>" value="<?php echo $i+1;?>"></td>
						<td>
							<select class="form-control input-sm" id="gru_sig<?php echo $i+1;?>">
								<option value="0" <?php if($operacion->signo==0){ echo "selected";}?>>(+) Mas</option>
								<option value="1" <?php if($operacion->signo==1){ echo "selected";}?>>(-) Menos</option>
							</select>
						</td>
						<td><select class="form-control input-sm" id="grupo<?php echo $i+1;?>" onchange="">
							<option value="">Seleccione...</option>
						<?php
						for ($c=0; $c < count($grupos) ; $c++) { $grupo=$grupos[$c]; ?>
							<option value="<?php echo $grupo->idee;?>" <?php if($operacion->idee2==$grupo->idee){ echo "selected";}?>><?php echo $grupo->titulo; ?></option>
						<?php } ?>
						</select>
						</td>
						<td><?php $this->load->view('estructura/botones/botones_registros',['eliminar'=>"drop_fila($(this));"]);?></td>
					</tr>
				<?php } ?>
				</tbody>
				<thead>
					<th colspan="4">
						<div class="col-md-3 col-md-offset-9 col-sm-4 col-sm-offset-8 col-xs-12">
							<button class="btn btn-info col-xs-12" onclick="add_row_grupo_epv('2',$(this));"><span class='glyphicon glyphicon-plus'></span> Adicionar grupo</button>
						</div>
					</th>
				</thead>
			</table>
		</div>
	</div>
</div>
<script language='javascript'>Onfocus("cli");$('[data-toggle="popover"]').popover({html:true});</script>