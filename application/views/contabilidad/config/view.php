<?php 
	$help1='title="<h4>Posición<h4>" data-content="Representa al orden en el que se generar los comprobantes, de las compras o egresos."';
	$help2='title="<h4>Cuenta<h4>" data-content="Seleccione las cuentas que seran usadas en el comprobante, la opcion <b>CUENTA DE EGRESO</b> es fijo pues toma la cuenta seleccionada en el registro de compra o egreso antes de generar el comprobante."';
	$help3='title="<h4>Porcentaje %<h4>" data-content="Representa al porcentaje del valor total a ser tomado en cuenta, debe poseer la posibilidad de hacer cuadrar el comprobante, el porcentaje debe estar entre el rango <b>mayor a cero y menor o igual a 100, se acepta 2 decimales como máximo.</b>."';
	$help4='title="<h4>Tipo<h4>" data-content="Representa al tipo de movimiento de la cuenta, debe tener cuidado de que los porcentajes cuadren segun el porcentaje asignado."';
	$help5='title="<h4>Posición<h4>" data-content="Representa al orden en el que se generar los comprobantes, de las ventas o ingresos."';
	$help6='title="<h4>Cuenta<h4>" data-content="Seleccione las cuentas que seran usadas en el comprobante, la opcion <b>CUENTA DE INGRESO</b> es fijo pues toma la cuenta seleccionada en el registro de venta o ingreso antes de generar el comprobante."';
	$help7='title="<h4>Tipo de material<h4>" data-content="Existen dos tipo de materiales: <b>Liquido que representa principalmente a materiales liquidos necesarios detro la pieza como ser(clefa,pintura,etc),</b> se asume que el material liquido solo se usa en los contornos de la pieza, si usted cree que el valor de la clefa debe ser mayor solo debe incrementar el valor de la <b>variable</b> para poder apalear estos gastos. <b>Materiales de recorrido, son principalmente materiales que se usan en el controno de la pieza como ser (Hilo, etc.)</b>. De igual usted puede aumentar el valor de la variable si creee que debe de ingresar mas material por pieza."';
	$help8='title="<h4>Signo<h4>" data-content="Representa al signo referencia al titulo, este signo no influye en las operaciónes. Ej. <b>mas: <ins>INGRESO</ins></b>"';
	$help9='title="<h4>Título de grupo<h4>" data-content="Representa al título del grupo, el titulo puede ser nulo, este título puede ser visible o no, el sistema solo acepta texto de 0 a 100 caracteres alfanumerico y solo se acepta los siguientes caracteres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>."';
	$help10='title="<h4>Columna<h4>" data-content="Representa a la columna donde corresponde los valores del grupo."';
	$popover='data-toggle="popover" data-placement="right" data-trigger="hover"';
	$popover2='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$popover3='data-toggle="popover" data-placement="top" data-trigger="hover"';
	$tipo = array('H'=>'Haber' , 'D'=>'Debe');
?>

	<div class="col-md-6 col-xs-12">
		<h3>Egresos: Estructura Comprobante</h3>
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th width="11%">
							<div class="input-group"><span class="input-group-addon input-sm" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span><div class="form-control input-sm" disabled>Pos</div></div>
						</th>
						<th width="47%">
							<div class="input-group">
								<span class="input-group-addon input-sm" <?php echo $popover.$help2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								<div class="form-control input-sm" disabled>Cuenta</div>
							</div>
						</th>
						<th width="15%">
							<div class="input-group">
								<span class="input-group-addon input-sm" <?php echo $popover.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								<div class="form-control input-sm" disabled>%</div>
							</div></th>
						<th width="17%">
							<div class="input-group">
								<span class="input-group-addon input-sm" <?php echo $popover.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								<div class="form-control input-sm" disabled>Tipo</div>
							</div></th>
						<th width="10%"></th>
					</tr>
				</thead>
				<tbody>
			<?php for($i=0; $i < count($egresos) ; $i++){ $egreso=$egresos[$i]; ?>
					<tr>
						<td><select id="eg_pos<?php echo $egreso->idci;?>" class="form-control input-sm">
							<?php for ($e=0; $e < count($egresos) ; $e++) { ?>
								<option value="<?php echo $egresos[$e]->posicion;?>" <?php if($egresos[$e]->idci==$egreso->idci){ echo "selected";}?>><?php echo $egresos[$e]->posicion;?></option>
							<?php } ?>
							</select>
						</td>
						<td>
						<?php $cta=""; if($egreso->idpl!=NULL && $egreso->control!=0){?>
							<select id="eg_cue<?php echo $egreso->idci;?>" class="form-control input-sm">
								<option value="">Seleccionar...</option>
							<?php for ($c=0; $c < count($cuentas) ; $c++) { $cuenta=$cuentas[$c]; ?>
								<option value="<?php echo $cuenta->idpl?>" <?php if($cuenta->idpl==$egreso->idpl){echo "selected"; $cta=$cuenta->cuenta_text;}?>><?php echo $cuenta->cuenta_text;?></option>
							<?php } ?>
							</select>
						<?php }else{ ?>
							CUENTA DE EGRESO<input type="hidden" id="eg_cue<?php echo $egreso->idci;?>" value="block">
						<?php } ?>
						</td>
						<td>
							<form onsubmit="return update_cuenta_egreso('<?php echo $egreso->idci;?>')">
								<input type="number" id="eg_por<?php echo $egreso->idci;?>" class="form-control input-sm" value="<?php echo $egreso->porcentaje*100;?>" min='0' max='100' step="any">
							</form>
						</td>
						<td>
							<select id="eg_tip<?php echo $egreso->idci;?>" class="form-control input-sm">
								<option value="">Seleccionar...</option>
							<?php foreach ($tipo as $key => $value) {?>
								<option value="<?php echo $key; ?>" <?php if($key==$egreso->tipo){ echo "selected";}?>><?php echo $value; ?></option>
							<?php }?>
							</select>
						</td>
						<?php $fun="alerta_egreso('".$egreso->idci."','".$cta."','".($egreso->porcentaje*100)."')";
						if($egreso->idpl==NULL || $egreso->control==0){ $fun="";} ?>
						<td><?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"update_cuenta_egreso('".$egreso->idci."')",'eliminar'=>$fun]);?></td>
					</tr>
			<?php }//end for ?>
				</tbody>
				<thead>
					<tr><th class="celda td text-center" colspan="5"><ins>NUEVO</ins></th></tr>
					<tr>
						<td colspan="2">
							<select id="eg_cue" class="form-control input-sm">
								<option value="">Seleccionar...</option>
							<?php for ($i=0; $i < count($cuentas) ; $i++) { $cuenta=$cuentas[$i];
							?>
								<option value="<?php echo $cuenta->idpl?>"><?php echo $cuenta->cuenta_text;?></option>
							<?php
							}?>
							</select>
						</td>
						<td>
							<form onsubmit="return save_cuenta_egreso()">
								<input type="number" id="eg_por" placeholder="100" class="form-control input-sm" min='0' max='100' step="any">
							</form>
						</td>
						<td>
							<select id="eg_tip" class="form-control input-sm">
							<?php foreach ($tipo as $key => $value) { ?>
								<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php }?>
							</select>
						</td>
						<td><?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"save_cuenta_egreso()",'eliminar'=>""]);?></td>
					</tr>
				</thead>
			</table>
		</div>

		<h3>Ventas: Estructura Comprobante</h3>
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th width="11%">
							<div class="input-group">
								<span class="input-group-addon input-sm" <?php echo $popover.$help5;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								<div class="form-control input-sm" disabled>Pos</div>
							</div>
						</th>
						<th width="47%">
							<div class="input-group">
								<span class="input-group-addon input-sm" <?php echo $popover.$help6;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								<div class="form-control input-sm" disabled>Cuenta</div>
							</div>
						</th>
						<th width="15%">
							<div class="input-group">
								<span class="input-group-addon input-sm" <?php echo $popover.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								<div class="form-control input-sm" disabled>%</div>
							</div></th>
						<th width="17%">
							<div class="input-group">
								<span class="input-group-addon input-sm" <?php echo $popover.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								<div class="form-control input-sm" disabled>Tipo</div>
							</div></th>
						<th width="10%"></th>
					</tr>
				</thead>
				<tbody>
			<?php for($i=0; $i < count($ingresos) ; $i++){ $ingreso=$ingresos[$i]; ?>
					<tr>
						<td><select id="in_pos<?php echo $ingreso->idci;?>" class="form-control input-sm">
							<?php for ($e=0; $e < count($ingresos) ; $e++) { ?>
								<option value="<?php echo $ingresos[$e]->posicion;?>" <?php if($ingresos[$e]->idci==$ingreso->idci){ echo "selected";}?>><?php echo $ingresos[$e]->posicion;?></option>
							<?php } ?>
							</select>
						</td>
						<td>
						<?php $cta=""; if($ingreso->idpl!=NULL && $ingreso->control!=0){?>
							<select id="in_cue<?php echo $ingreso->idci;?>" class="form-control input-sm">
								<option value="">Seleccionar...</option>
							<?php for ($c=0; $c < count($cuentas) ; $c++) { $cuenta=$cuentas[$c]; ?>
								<option value="<?php echo $cuenta->idpl?>" <?php if($cuenta->idpl==$ingreso->idpl){echo "selected"; $cta=$cuenta->cuenta_text;}?>><?php echo $cuenta->cuenta_text;?></option>
							<?php } ?>
							</select>
						<?php }else{ ?>
							CUENTA DE INGRESO<input type="hidden" id="in_cue<?php echo $ingreso->idci;?>" value="block">
						<?php } ?>
						</td>
						<td>
							<form onsubmit="return update_cuenta_ingreso('<?php echo $ingreso->idci;?>')">
								<input type="number" id="in_por<?php echo $ingreso->idci;?>" class="form-control input-sm" value="<?php echo $ingreso->porcentaje*100;?>" min='0' max='100' step="any">
							</form>
						</td>
						<td>
							<select id="in_tip<?php echo $ingreso->idci;?>" class="form-control input-sm">
								<option value="">Seleccionar...</option>
							<?php foreach ($tipo as $key => $value) {?>
								<option value="<?php echo $key; ?>" <?php if($key==$ingreso->tipo){ echo "selected";}?>><?php echo $value; ?></option>
							<?php }?>
							</select>
						</td>
						<?php $fun="alerta_ingreso('".$ingreso->idci."','".$cta."','".($ingreso->porcentaje*100)."')";
						if($ingreso->idpl==NULL || $ingreso->control==0){ $fun="";} ?>
						<td><?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"update_cuenta_ingreso('".$ingreso->idci."')",'eliminar'=>$fun]);?></td>
					</tr>
			<?php }//end for ?>
				</tbody>
				<thead>
					<tr><th class="celda td text-center" colspan="5"><ins>NUEVO</ins></th></tr>
					<tr>
						<td colspan="2">
							<select id="in_cue" class="form-control input-sm">
								<option value="">Seleccionar...</option>
							<?php for ($i=0; $i < count($cuentas) ; $i++) { $cuenta=$cuentas[$i]; ?>
								<option value="<?php echo $cuenta->idpl?>"><?php echo $cuenta->cuenta_text;?></option>
							<?php }?>
							</select>
						</td>
						<td>
							<form onsubmit="return save_cuenta_ingreso()">
								<input type="number" id="in_por" placeholder="100" class="form-control input-sm" min='0' max='100' step="any">
							</form>
						</td>
						<td>
							<select id="in_tip" class="form-control input-sm">
							<?php foreach ($tipo as $key => $value) { ?>
								<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php }?>
							</select>
						</td>
						<td><?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"save_cuenta_ingreso()",'eliminar'=>""]);?></td>
					</tr>
				</thead>
			</table>
		</div>
	</div>
	<div class="col-md-6 col-xs-12">
		<h3>Estructura: Estado Costo Prod. y Prod. Vend. </h3>
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th width="10%">
							<!--<div class="input-group"><span class="input-group-addon input-sm" <?php echo $popover.$help5;?>><i class='glyphicon glyphicon-info-sign'></i></span><div class="form-control input-sm" disabled>Pos</div></div>-->
							#
						</th>
						<th width="10%">
							<div class="input-group">
								<span class="input-group-addon input-sm" <?php echo $popover.$help8;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								<div class="form-control input-sm" disabled>Sig</div>
							</div>
						</th>
						<th width="60%">
							<div class="input-group">
								<span class="input-group-addon input-sm" <?php echo $popover.$help9;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								<div class="form-control input-sm" disabled>Titulo</div>
							</div>
						</th>
						<th width="10%">
							<div class="input-group">
								<span class="input-group-addon input-sm" <?php echo $popover.$help10;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								<div class="form-control input-sm" disabled>Col</div>
							</div>
						</th>
						<th width="10%"></th>
					</tr>
				</thead>
				<tbody>
			<?php for ($i=0; $i < count($estructura0) ; $i++) { 
					$e=$estructura0[$i];
					if($e->tipo_estado==0){ ?>
					<tr>
						<td>
							<!--<select class="form-control input-sm" id="">
							<?php for ($j=0; $j < count($estructura0) ; $j++) { $pos=$estructura0[$j]; if($pos->tipo_estado==0){ ?>
								<option value="<?php echo $pos->posicion?>" <?php if($e->posicion==$pos->posicion){ echo "selected";}?>><?php echo $pos->posicion?></option>
							<?php }} ?>
							</select>-->
							<?php echo $i+1;?>
						</td>
						<td><?php if($e->signo==0){ echo "mas:";}else{ echo "menos:";}?>
						</td>
						<td><?php echo $e->titulo;?></td>
						<td><?php echo $e->columna+1; ?></td>
						<td><?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"",'configuracion'=>"mod_group_est('".$e->idee."','0')",'eliminar'=>"alerta_epv('".$e->idee."','".$e->titulo."')"]);?></td>
					</tr>
			<?php } 
				} ?>
				</tbody>
				<thead>
					<tr>
						<td colspan="4"></td>
						<td><?php $this->load->view('estructura/botones/buscador',['nuevo'=>'Nuevo registro','f_nuevo'=>"new_group_est('0')"]);?></td>
					</tr>
				</thead>
			</table>
		</div>
		<h3>Estructura: Estado de resultados</h3>
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th width="10%">#</th>
						<th width="10%">
							<div class="input-group">
								<span class="input-group-addon input-sm" <?php echo $popover.$help8;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								<div class="form-control input-sm" disabled>Sig</div>
							</div>
						</th>
						<th width="60%">
							<div class="input-group">
								<span class="input-group-addon input-sm" <?php echo $popover.$help9;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								<div class="form-control input-sm" disabled>Titulo</div>
							</div>
						</th>
						<th width="10%">
							<div class="input-group">
								<span class="input-group-addon input-sm" <?php echo $popover.$help10;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								<div class="form-control input-sm" disabled>Col</div>
							</div>
						</th>
						<th width="10%"></th>
					</tr>
				</thead>
				<tbody>
			<?php $cont=1; 
				for ($i=0; $i < count($estructura0) ; $i++) { 
					$e=$estructura0[$i];
					if($e->tipo_estado==1){ ?>
				<tr>
					<td>
						<!--<select class="form-control input-sm" id="">
						<?php for ($j=0; $j < count($estructura0) ; $j++) { $pos=$estructura0[$j]; if($pos->tipo_estado==1){ ?>
							<option value="<?php echo $pos->posicion?>" <?php if($e->posicion==$pos->posicion){ echo "selected";}?>><?php echo $pos->posicion?></option>
						<?php } }?>
						</select>-->
						<?php echo $cont++;?>
					</td>
					<td><?php if($e->signo==0){ echo "mas:";}else{ echo "menos:";}?>
					</td>
					<td><?php echo $e->titulo;?></td>
					<td><?php echo $e->columna+1; ?></td>
					<td><?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"",'configuracion'=>"mod_group_est('".$e->idee."','1')",'eliminar'=>"alerta_epv('".$e->idee."','".$e->titulo."')"]);?></td>
				</tr>
			<?php } 
				} ?>
				</tbody>
				<thead>
					<tr>
						<td colspan="4"></td>
						<td><?php $this->load->view('estructura/botones/buscador',['nuevo'=>'Nuevo registro','f_nuevo'=>"new_group_est('1')"]);?></td>
					</tr>
				</thead>
			</table>
		</div>
		<h3>Estructura: Balance General</h3>
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th width="10%">#</th>
						<th width="10%">
							<div class="input-group">
								<span class="input-group-addon input-sm" <?php echo $popover.$help8;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								<div class="form-control input-sm" disabled>Sig</div>
							</div>
						</th>
						<th width="60%">
							<div class="input-group">
								<span class="input-group-addon input-sm" <?php echo $popover.$help9;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								<div class="form-control input-sm" disabled>Titulo</div>
							</div>
						</th>
						<th width="10%">
							<div class="input-group">
								<span class="input-group-addon input-sm" <?php echo $popover.$help10;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								<div class="form-control input-sm" disabled>Col</div>
							</div>
						</th>
						<th width="10%"></th>
					</tr>
				</thead>
				<tbody>
			<?php $cont=1;
				for($i=0; $i < count($estructura0) ; $i++) { 
					$e=$estructura0[$i];
					if($e->tipo_estado==2){ ?>
					<tr>
						<td>
							<!--<select class="form-control input-sm" id="">
							<?php for($j=0; $j<count($estructura0) ;$j++){ $pos=$estructura0[$j]; 
									if($pos->tipo_estado==2){?>
								<option value="<?php echo $pos->posicion?>" <?php if($pos->posicion==$e->posicion){ echo "selected";} ?>><?php echo $pos->posicion?></option>
							<?php }
								} ?>
							</select>-->
							<?php echo $cont++;?>
						</td>
						<td><?php if($e->signo==0){ echo "mas:";}else{ echo "menos:";}?>
						</td>
						<td><?php echo $e->titulo;?></td>
						<td><?php echo $e->columna+1; ?></td>
						<td><?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"",'configuracion'=>"mod_group_est('".$e->idee."','2')",'eliminar'=>"alerta_epv('".$e->idee."','".$e->titulo."')"]);?></td>
					</tr>
			<?php } 
				} ?>
				</tbody>
				<thead>
					<tr>
						<td colspan="4"></td>
						<td><?php $this->load->view('estructura/botones/buscador',['nuevo'=>'Nuevo registro','f_nuevo'=>"new_group_est('2')"]);?></td>
					</tr>
				</thead>
			</table>
		</div>
	</div>

<script language='javascript'>$('[data-toggle="popover"]').popover({html:true});</script>