<?php
	$help1='title="<h4>Signo<h4>" data-content="Representa al signo referencia al titulo, este signo no influye en las operaciónes. Ej. <b>mas: <ins>INGRESO</ins></b>"';
	$help2='title="<h4>Título de grupo<h4>" data-content="Representa al título del grupo, el titulo puede ser nulo, este título puede ser visible o no, el sistema solo acepta texto de 0 a 100 caracteres alfanumerico y solo se acepta los siguientes caracteres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>."';
	$help3='title="<h4>Grupos de cuentas<h4>" data-content="Selecione las cuenta o grupos de cuentas que pertenecen al grupo del estado, si al cuenta que dea no aparece puede adicionar una nueva cuenta en el plan de cuenta, dirigiendose al menu superior Plan de cuenta o dar click en el boton <b>+</b>"';
	$popover='data-toggle="popover" data-placement="right" data-trigger="hover"';
	$popover2='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$popover3='data-toggle="popover" data-placement="top" data-trigger="hover"';
?>
<div class="row">
	<div class="col-xs-12"><strong><ins>Detalles del titulo del grupo</ins></strong></div>
	<div class="col-md-3 col-xs-12">
		<div class="input-group">
			<select class="form-control input-sm" id="signo">
				<option value="0">mas:</option>
				<option value="1">menos:</option>
			</select>
			<span class="input-group-addon input-sm" <?php echo $popover3.$help1;?>><i class="glyphicon glyphicon-info-sign"></i></span>
		</div>
	</div>
	<div class="col-md-1 col-xs-12">
		<input type="checkbox" id="sig_vis"> visible
	</div>
	<div class="col-md-7 col-xs-12">
		<div class="input-group">
			<input type="text" class="form-control input-sm" placeholder='Titulo de division (no obligatorio)' id="nom_cta_group">
			<span class="input-group-addon input-sm" <?php echo $popover2.$help2;?>><i class="glyphicon glyphicon-info-sign"></i></span>
		</div>
	</div>
	<div class="col-md-1 col-xs-12">
		<input id="nom_vis" type="checkbox"> visible
	</div>
	<div class="col-xs-12"><hr></div>
	<div class="col-xs-12"><strong><ins>Detalles del cuentas en el grupo</ins></strong></div>
	<div class="col-sm-4 col-md-2 col-xs-12"><label>Columna 1:</label> <input type="radio" value="0" name="co" title="columna 1" checked=""></div>
	<div class="col-sm-4 col-md-2 col-xs-12"><label>Columna 2:</label> <input type="radio" value="1" name="co" title="columna 2"></div>
	<div class="col-sm-4 col-md-2 col-xs-12"><label>Columna 3:</label> <input type="radio" value="2" name="co" title="columna 3"></div>
	<div class="col-xs-12">
		
		
		
		<div class="table-responsive">
			<table id="table-np" class="table table-bordered table-striped table-hover">
				<thead>
					<th width="10%">#Item</th>
					<th width="84%">
						<div class="input-group">
							<span class="input-group-addon input-sm" <?php echo $popover.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
							<div class="form-control input-sm" disabled>Cuenta</div>
							<a href="<?php echo base_url();?>contabilidad?p=7" target="_blank" title="Ver Configuraciónes" class="input-group-addon input-sm"><i class="glyphicon glyphicon-plus"></i></a>
						</div>
					</th>
					<td width="6%"></td>
				</thead>
				<tbody id="content_cuentas"></tbody>
				<thead>
					<th colspan="4">
						<div class="col-md-3 col-md-offset-9 col-sm-4 col-sm-offset-8 col-xs-12">
							<button class="btn btn-info col-xs-12" onclick="add_row_cuenta_epv($(this));"><span class='glyphicon glyphicon-plus'></span> Adicionar Registro</button>
						</div>
					</th>
				</thead>
			</table>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>
	<div class="col-xs-12"><strong><ins>Detalles del operaciones con otros grupos</ins></strong></div>
	<div class="col-xs-12">
		<div class="table-responsive">
			<table id="table-np" class="table table-bordered table-striped table-hover">
				<thead>
					<th width="9%">#Item</th>
					<th width="15%">Operacion</th>
					<th width="70%">Grupo del estado</th>
					<th width="6%">
						<!--<button class="btn btn-celeste btn-sm col-xs-12" onclick="adicionar_row_grupo('0');" title="Adicionar Fila"><span class='glyphicon glyphicon-save'></span></button>-->
					</th>
				</thead>
				<tbody id="content_operacion"></tbody>
				<thead>
					<th colspan="4">
						<div class="col-md-3 col-md-offset-9 col-sm-4 col-sm-offset-8 col-xs-12">
							<button class="btn btn-info col-xs-12" onclick="add_row_grupo_epv('0',$(this));"><span class='glyphicon glyphicon-plus'></span> Adicionar grupo</button>
						</div>
					</th>
					<!--<th>
						
						<button class="btn btn-danger btn-sm col-xs-12" onclick="drop_row('content_operacion');" title="Eliminar ultima fila"><span class='glyphicon glyphicon-trash'></span></button>
					</th>-->
				</thead>
			</table>
		</div>
	</div>
</div>

<script language='javascript'>Onfocus("cli");$('[data-toggle="popover"]').popover({html:true});</script>