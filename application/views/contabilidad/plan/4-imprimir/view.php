<?php 
	$cuentas=json_decode($cuentas);
	$contPagina=1;
	$contReg=0;
?>
<div class="pagina table-responsive">
	<?php $this->load->view('estructura/print/encabezado',['titulo'=>'PLAN DE CUENTAS','pagina'=>$contPagina,'sub_titulo'=>'']);?>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php if(!isset($v1)){?><th class="celda th" width="5%" >#Item</th><?php } ?>
			<?php if(!isset($v2)){?><th class="celda th" width="15%">Código</th><?php } ?>
			<?php if(!isset($v3)){?><th class="celda th" width="60%">Cuenta</th><?php } ?>
			<?php if(!isset($v4)){?><th class="celda th" width="10%" >Egreso</th><?php } ?>
			<?php if(!isset($v5)){?><th class="celda th" width="10%">Ingreso</th><?php } ?>
		</tr>
	<?php $cont=1;
		foreach($cuentas as $key => $cuenta){ ?>
	<?php if($contReg>=$nro){
				$contReg=0;
				$contPagina++;
	?>
		</table><!--cerramos tabla-->
	</div><!--cerramos pagina-->
<div class="pagina table-responsive">
	<?php $this->load->view('estructura/print/encabezado',['titulo'=>'PLAN DE CUENTAS','pagina'=>$contPagina,'sub_titulo'=>'']);?>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php if(!isset($v1)){?><th class="celda th" width="5%" >#Item</th><?php } ?>
			<?php if(!isset($v2)){?><th class="celda th" width="15%">Código</th><?php } ?>
			<?php if(!isset($v3)){?><th class="celda th" width="60%">Cuenta</th><?php } ?>
			<?php if(!isset($v4)){?><th class="celda th" width="10%" >Egreso</th><?php } ?>
			<?php if(!isset($v5)){?><th class="celda th" width="10%">Ingreso</th><?php } ?>
		</tr>
		<?php }//end if ?>
		<tr class="fila">
			<?php if(!isset($v1)){?><td class="celda td"><?php echo $cont++;?></td><?php } ?>
			<?php if(!isset($v2)){?><td class="celda td"><?php if($cuenta->tipo!=1){?><strong><?php }?><?php echo $cuenta->codigo;?><?php if($cuenta->tipo!=1){?></strong><?php }?></td><?php } ?>
			<?php if(!isset($v3)){?><td class="celda td"><?php if($cuenta->tipo!=1){?><ins><strong><?php }?><?php echo $cuenta->cuenta_text;?><?php if($cuenta->tipo!=1){?></ins></strong><?php }?></td><?php } ?>
			<?php if(!isset($v4)){?><td class="celda td"><?php if($cuenta->gasto==1){?><span class="glyphicon glyphicon-ok"></span><?php }?></td><?php } ?>
			<?php if(!isset($v5)){?><td class="celda td"><?php if($cuenta->venta==1){?><span class="glyphicon glyphicon-ok"></span><?php }?></td><?php } ?>
		</tr>
		<?php $contReg++;}// end for ?>
	</table>
</div>
<script type="text/javascript"> $(".nroPagina").html(<?php echo $contPagina;?>); </script>