<?php $j_cuentas=json_encode($cuentas); ?>
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th colspan="7" style="text-align:center;">
						<h3><ins>PLAN DE CUENTAS</ins></h3>
					</th>
				</tr>
				<tr>
					<th width="5%">#Item</th>
					<th width="10%">Código</th>
					<th width="60%">Cuenta</th>
					<th width="6%">Cta./No Cta.</th>
					<th width="6%">Cta. de Gasto</th>
					<th width="6%">Cta de Venta</th>
					<th width="7%"></th>
				</tr>
			</thead>
			<tbody>
			<?php for ($i=0; $i < count($cuentas) ; $i++) { $cuenta=$cuentas[$i];?>
				<tr>
					<td><?php echo $i+1;?></td>
					
					<td><?php echo $cuenta->codigo; ?></td>
					<td><?php if($cuenta->tipo!=1){?><ins><strong><?php }?><?php echo $cuenta->cuenta_text;?><?php if($cuenta->tipo!=1){?></ins></strong><?php }?></td>
					<td>
						<?php if($cuenta->tipo==1){ ?>
							<div class="btn-circle btn-circle-info btn-circle-30" onclick="change_estado('<?php echo $cuenta->idpl;?>','0');"></div>
						<?php }else{ ?>
							<div class="btn-circle btn-circle-default btn-circle-30" onclick="change_estado('<?php echo $cuenta->idpl;?>','0');"></div>
						<?php } ?>
					</td>
					<td>
					<?php if($cuenta->tipo==1){ ?>
						<?php if($cuenta->gasto==1){ ?>
							<div class="btn-circle btn-circle-red btn-circle-30" onclick="change_estado('<?php echo $cuenta->idpl;?>','1');"></div>
						<?php }else{ ?>
							<div class="btn-circle btn-circle-default btn-circle-30" onclick="change_estado('<?php echo $cuenta->idpl;?>','1');"></div>
						<?php } ?>
					<?php } ?>
					</td>
					<td>
					<?php if($cuenta->tipo==1){ ?>
						<?php if($cuenta->venta==1){ ?>
							<div class="btn-circle btn-circle-success btn-circle-30" onclick="change_estado('<?php echo $cuenta->idpl;?>','2');"></div>
						<?php }else{ ?>
							<div class="btn-circle btn-circle-default btn-circle-30" onclick="change_estado('<?php echo $cuenta->idpl;?>','2');"></div>
						<?php } ?>
					<?php } ?>
					</td>
					<td>
						<?php $mod="config_plan('".$cuenta->idpl."')"; if($privilegio[0]->co7u!="1"){ $mod="";}?>
						<?php $del="confirmar_plan('".$cuenta->idpl."','".$cuenta->codigo."','".$cuenta->cuenta_text."')"; if($privilegio[0]->co7d!="1"){ $del="";}?>
						<?php $this->load->view('estructura/botones/botones_registros',['reportes'=>"",'configuracion'=>$mod,'eliminar'=>$del]);?>
					</td>
				</tr>
			<?php
			} ?>
			</tbody>
		</table>
<script language="javascript" type="text/javascript"> 
	$("#print").removeAttr("onclick"); $("#print").unbind("click"); 
	$("#print").click(function(){ imprimir_plan('<?php echo $j_cuentas;?>'); }); 
</script>