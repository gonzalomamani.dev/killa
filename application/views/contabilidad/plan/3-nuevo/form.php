<?php
	$help0='title="<h4>Tipo<h4>" data-content="Representa al tipo de registro: si selecciona tipo <b>Cuenta</b> el sistema tomara al registro como una cuenta a ser usada, si selecciona tipo <b>Grupo</b>, el sistema no tomara al registro como cuenta si no al contrario solo sera como un texto referencial o un titulo."';
	$help1='title="<h4>Grupo<h4>" data-content="Representa al grupo de la cuenta, solo se aceptan <b>valores numéricos enteros entre 0 y 9.</b>"';
	$help2='title="<h4>Sub grupo<h4>" data-content="Representa al sub grupo de la cuenta si tuviera, solo se aceptan <b>valores numéricos enteros entre 0 y 9.</b>"';
	$help3='title="<h4>Rubro<h4>" data-content="Representa al rubro de la cuenta si tuviera, solo se aceptan <b>valores numéricos enteros entre 0 y 9.</b>"';
	$help4='title="<h4>Cuenta<h4>" data-content="Representa al número de la cuenta, solo se aceptan <b>valores numéricos enteros entre 0 y 9999.</b>"';
	$help5='title="<h4>Nombre de la cuenta<h4>" data-content="Ingrese el nombre de la cuenta de tipo alfanumérico de 5 a 150 caracteres <b>puede incluir espacios</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$popover3='data-toggle="popover" data-placement="top" data-trigger="hover"';
?>
<div class="row">
	<div class="col-xs-12 text-right text-danger">
		<strong class=''>Los campos con borde rojo son obligatorios</strong>
		<hr>
	</div>
	<div class="col-md-4 col-md-offset-8 col-xs-12">
		<div class="input-group">
			<select id="tip" class="form-control input-xs" style="border:solid 1px rgba(255,0,0,.4);">
				<option value="">Seleccionar...</option>
				<option value="0">Grupo en el plan</option>
				<option value="1">Cuenta en el plan</option>
			</select>
			<span class="input-group-addon input-sm" <?php echo $popover.$help0;?>><i class="glyphicon glyphicon-info-sign"></i></span>
		</div>
	</div><i class='clearfix'></i>
	<div class="col-md-2 col-xs-12">
		<div class="input-group">
			<form onsubmit="return save_plan()">
				<input type="number" class="form-control input-xs" id="gru" placeholder='Grupo' min="0" max="9" style="border:solid 1px rgba(255,0,0,.4);">
			</form>
			<span class="input-group-addon input-sm" <?php echo $popover3.$help1;?>><i class="glyphicon glyphicon-info-sign"></i></span>
		</div>
	</div>
	<div class="col-md-2 col-xs-12">
		<div class="input-group">
			<form onsubmit="return save_plan()">
				<input type="number" class="form-control input-xs" id="sub_gru" placeholder='Sub grupo' min="0" max="9">
			</form>
			<span class="input-group-addon input-sm" <?php echo $popover3.$help2;?>><i class="glyphicon glyphicon-info-sign"></i></span>
		</div>
	</div>
	<div class="col-md-2 col-xs-12">
		<div class="input-group">
			<form onsubmit="return save_plan()">
				<input type="number" class="form-control input-xs" id="rub" placeholder='rubro' min="0" max="9" value="">
			</form>
			<span class="input-group-addon input-sm" <?php echo $popover3.$help3;?>><i class="glyphicon glyphicon-info-sign"></i></span>
		</div>
	</div>
	<div class="col-md-2 col-xs-12">
		<div class="input-group">
			<form onsubmit="return save_plan()">
				<input type="number" class="form-control input-xs" id="cue" placeholder='Cuenta' min="0" max="9999">
			</form>
			<span class="input-group-addon input-sm" <?php echo $popover3.$help4;?>><i class="glyphicon glyphicon-info-sign"></i></span>
		</div>
	</div>
	<div class="col-md-4 col-xs-12">
		<div class="input-group">
			<form onsubmit="return save_plan()">
				<input type="text" class="form-control input-xs" id="cue_text" placeholder='Nombre de Cuenta' style="border:solid 1px rgba(255,0,0,.4);" maxlength="150">
			</form>
			<span class="input-group-addon input-sm" <?php echo $popover.$help5;?>><i class="glyphicon glyphicon-info-sign"></i></span>
		</div>
	</div>
</div>
<script language='javascript'>Onfocus("tip"); $('[data-toggle="popover"]').popover({html:true});</script>