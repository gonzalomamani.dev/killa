<?php 
	$cuentas=json_decode($cuentas);
	$contPagina=1;
	$contReg=0;
	$cols=0;
?>
<div class="pagina table-responsive">
	<?php $this->load->view('estructura/print/encabezado',['titulo'=>'DETALLE DE COMPROBANTES','pagina'=>$contPagina,'sub_titulo'=>'Del '.$this->validaciones->formato_fecha($fecha1,'d,m,Y').' al '.$this->validaciones->formato_fecha($fecha2,'d,m,Y')]);?>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php $cols=0;?>
			<?php if(!isset($v1)){?><th class="celda th" rowspan="2" width="5%" >#Item</th><?php $cols++;} ?>
			<?php if(!isset($v2)){?><th class="celda th" rowspan="2" width="10%">Código</th><?php $cols++;} ?>
			<?php if(!isset($v3)){?><th class="celda th" rowspan="2" width="45%">Cuenta</th><?php $cols++;} ?>
			<?php if(!isset($v4)){?><th class="celda th" colspan="2" width="20%" >Sumas</th><?php } ?>
			<?php if(!isset($v5)){?><th class="celda th" colspan="2" width="20%">Saldos</th><?php } ?>
		</tr>
		<tr class="fila">
			<?php if(!isset($v4)){?><th class="celda th" width="10%">DEBE</th><?php } ?>
			<?php if(!isset($v4)){?><th class="celda th" width="10%">HABER</th><?php } ?>
			<?php if(!isset($v5)){?><th class="celda th" width="10%">DEUDOR</th><?php } ?>
			<?php if(!isset($v5)){?><th class="celda th" width="10%">ACREEDOR</th><?php } ?>
		</tr>
		<?php $cont=0;
			  $deudor=0;$deudor_sub=0;
			  $acreedor=0;$acreedor_sub=0;
			  $debe=0;$debe_sub=0;
			  $haber=0;$haber_sub=0;
			foreach($cuentas as $key => $cuenta){ ?>
		<?php if($contReg>=$nro){
				$contReg=0;
				$contPagina++;
		?>
			<tr class="fila">
				<?php if($cols>0){?><th colspan="<?php echo $cols;?>" class="celda td text-right">SUB TOTAL(Bs.):</th><?php }?>
				<?php if(!isset($v4)){?><th class="celda td"><?php echo number_format($debe_sub,2,'.',','); $debe_sub=0;?></th><?php }?>
				<?php if(!isset($v4)){?><th class="celda td"><?php echo number_format($haber_sub,2,'.',','); $haber_sub=0;?></th><?php }?>
				<?php if(!isset($v5)){?><th class="celda td"><?php echo number_format($deudor_sub,2,'.',','); $deudor_sub=0;?></th><?php }?>
				<?php if(!isset($v5)){?><th class="celda td"><?php echo number_format($acreedor_sub,2,'.',','); $acreedor_sub=0;?></th><?php }?>
			</tr>
			<tr class="fila">
				<?php if($cols>0){?><th colspan="<?php echo $cols;?>" class="celda td text-right">TOTAL(Bs.):</th><?php }?>
				<?php if(!isset($v4)){?><th class="celda td"><?php echo number_format($debe,2,'.',',');?></th><?php }?>
				<?php if(!isset($v4)){?><th class="celda td"><?php echo number_format($haber,2,'.',',');?></th><?php }?>
				<?php if(!isset($v5)){?><th class="celda td"><?php echo number_format($deudor,2,'.',',');?></th><?php }?>
				<?php if(!isset($v5)){?><th class="celda td"><?php echo number_format($acreedor,2,'.',',');?></th><?php }?>
			</tr>
		</table><!--cerramos tabla-->
	</div><!--cerramos pagina-->
<div class="pagina table-responsive">
	<?php $this->load->view('estructura/print/encabezado',['titulo'=>'DETALLE DE COMPROBANTES','pagina'=>$contPagina,'sub_titulo'=>'']);?>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php $cols=0;?>
			<?php if(!isset($v1)){?><th class="celda th" rowspan="2" width="5%" >#Item</th><?php $cols++;} ?>
			<?php if(!isset($v2)){?><th class="celda th" rowspan="2" width="10%">Código</th><?php $cols++;} ?>
			<?php if(!isset($v3)){?><th class="celda th" rowspan="2" width="45%">Cuenta</th><?php $cols++;} ?>
			<?php if(!isset($v4)){?><th class="celda th" colspan="2" width="20%" >Sumas</th><?php } ?>
			<?php if(!isset($v5)){?><th class="celda th" colspan="2" width="20%">Saldos</th><?php } ?>
		</tr>
		<tr class="fila">
			<?php if(!isset($v4)){?><th class="celda th" width="10%">DEBE</th><?php } ?>
			<?php if(!isset($v4)){?><th class="celda th" width="10%">HABER</th><?php } ?>
			<?php if(!isset($v5)){?><th class="celda th" width="10%">DEUDOR</th><?php } ?>
			<?php if(!isset($v5)){?><th class="celda th" width="10%">ACREEDOR</th><?php } ?>
		</tr>
		<?php $sub_total=0;}//end if ?>
		<tr class="fila">
			<?php if(!isset($v1)){?><td class="celda td"><?php echo $cont+1;$cont++;?></td><?php } ?>
			<?php if(!isset($v2)){?><td class="celda td"><?php echo $cuenta->codigo;?></td><?php } ?>
			<?php if(!isset($v3)){?><td class="celda td"><?php echo $cuenta->cuenta_text;?></td><?php } ?>
			<?php if(!isset($v4)){?><td class="celda td"><?php echo number_format($cuenta->debe,2,'.',','); $debe+=$cuenta->debe;$debe_sub+=$cuenta->debe;?></td><?php } ?>
			<?php if(!isset($v4)){?><td class="celda td"><?php echo number_format($cuenta->haber,2,'.',','); $haber+=$cuenta->haber;$haber_sub+=$cuenta->haber;?></td><?php } ?>
			<?php $dif=$this->conta->deudor_acreedor($cuenta->idpl,$cuenta->debe,$cuenta->haber);?>
			<?php if(!isset($v5)){?><td class="celda td"><?php if($dif>0){ echo number_format($dif,2,'.',','); $deudor+=$dif;$deudor_sub+=$dif;}else{ echo '0.00'; }?></td><?php } ?>
			<?php if(!isset($v5)){?><td class="celda td"><?php if($dif<0){ echo number_format(abs($dif),2,'.',','); $acreedor+=abs($dif);$acreedor_sub+=abs($dif);}else{ echo '0.00'; }?></td><?php } ?>
		</tr>
		<?php $contReg++;}// end for ?>
		<tr class="fila">
			<?php if($cols>0){?><th colspan="<?php echo $cols;?>" class="celda td text-right">SUB TOTAL(Bs.):</th><?php }?>
			<?php if(!isset($v4)){?><th class="celda td"><?php echo number_format($debe_sub,2,'.',',');?></th><?php }?>
			<?php if(!isset($v4)){?><th class="celda td"><?php echo number_format($haber_sub,2,'.',',');?></th><?php }?>
			<?php if(!isset($v5)){?><th class="celda td"><?php echo number_format($deudor_sub,2,'.',',');?></th><?php }?>
			<?php if(!isset($v5)){?><th class="celda td"><?php echo number_format($acreedor_sub,2,'.',',');?></th><?php }?>
		</tr>
		<tr class="fila">
			<?php if($cols>0){?><th colspan="<?php echo $cols;?>" class="celda td text-right">TOTAL(Bs.):</th><?php }?>
			<?php if(!isset($v4)){?><th class="celda td"><?php echo number_format($debe,2,'.',',');?></th><?php }?>
			<?php if(!isset($v4)){?><th class="celda td"><?php echo number_format($haber,2,'.',',');?></th><?php }?>
			<?php if(!isset($v5)){?><th class="celda td"><?php echo number_format($deudor,2,'.',',');?></th><?php }?>
			<?php if(!isset($v5)){?><th class="celda td"><?php echo number_format($acreedor,2,'.',',');?></th><?php }?>
		</tr>
	</table>
</div>
<script type="text/javascript"> $(".nroPagina").html(<?php echo $contPagina;?>); </script>