<?php 
	$f1=date('Y-m').'-01';
	$f2=date('Y-m').'-'.$this->validaciones->convierteMes($this->validaciones->ultimo_dia(date('m'),date('Y')));
?>
<table class="tabla">
	<tr>
		<td class="celda td" width="30%"></td>
		<td class="celda td" width="20%"></td>
		<td class="celda td" width="20%"><input type="date" id="f1" class="form-control input-sm" value="<?php echo $f1;?>" onchange=""></td>
		<td class="celda td" width="20%"><input type="date" id="f2" class="form-control input-sm" value="<?php echo $f2;?>" onchange=""></td>
		<td class="celda td" width="10%">
			<?php $this->load->view('estructura/botones/buscador',['f_buscar'=>"view_sumas()",'f_ver'=>"all_sumas()",'f_imprimir'=>'imprimir()']);?>
		</td>		
	</tr>
</table>