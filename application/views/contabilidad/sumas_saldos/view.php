<?php 
	$deudor=0;$acreedor=0;
	$debe=0;$haber=0;
	$j_cuentas=json_encode($cuentas);
?>

		<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th colspan="6" style="text-align:center;">
						<h3><ins>BALANCE DE COMPROBANCIÓN DE SUMAS Y SALDOS</ins></h3>
						Del <?php echo $this->validaciones->formato_fecha($fecha1,'d,m,Y');?> al <?php echo $this->validaciones->formato_fecha($fecha2,'d,m,Y');?>
					</th>
				</tr>
				<tr>
					<th rowspan="2" width="10%">Código</th>
					<th rowspan="2" width="50%">Cuentas</th>
					<th colspan="2" class="text-center">SUMAS</th>
					<th colspan="2" class="text-center">SALDOS</th>
				</tr>
				<tr>
					<th width="10%" class="text-center">DEUDOR</th>
					<th width="10%" class="text-center">ACREEDOR</th>
					<th width="10%" class="text-center">DEBE</th>
					<th width="10%" class="text-center">HABER</th>
				</tr>
			</thead>
			<tbody>
		<?php for ($i=0; $i < count($cuentas); $i++){ $cuenta=$cuentas[$i]; ?>
				<tr>
					<td><?php echo $cuenta->codigo;?></td>
					<td><?php echo $cuenta->cuenta_text;?></td>
					<td><?php echo number_format($cuenta->debe,2,'.',','); $debe+=$cuenta->debe;?></td>
					<td><?php echo number_format($cuenta->haber,2,'.',','); $haber+=$cuenta->haber;?></td>
					<?php $dif=$this->conta->deudor_acreedor($cuenta->idpl,$cuenta->debe,$cuenta->haber);?>
					<td><?php if($dif>0){ echo number_format($dif,2,'.',','); $deudor+=$dif;}else{ echo '0.00'; }?></td>
					<td><?php if($dif<0){ echo number_format(abs($dif),2,'.',','); $acreedor+=abs($dif);}else{ echo '0.00'; }?></td>
				</tr>
		<?php } ?>
			</tbody>
			<thead>
				<tr>
					<th colspan="2" class="text-right">TOTAL(Bs.)</th>
					<th><?php echo number_format($debe,2,'.',',');;?></th>
					<th><?php echo number_format($haber,2,'.',',');;?></th>
					<th><?php echo number_format($deudor,2,'.',',');;?></th>
					<th><?php echo number_format($acreedor,2,'.',',');;?></th>
				</tr>
			</thead>	
		</table>
<script language="javascript" type="text/javascript"> 
	$("#print").removeAttr("onclick"); $("#print").unbind("click"); 
	$("#print").click(function(){ imprimir_sumas('<?php echo $j_cuentas;?>','<?php echo $fecha1; ?>','<?php echo $fecha2; ?>'); }); 
</script>
