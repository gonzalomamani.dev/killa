<?php
	$tipos= array('T' => 'Traspazo','E' => 'Egreso','I' => 'Ingreso');
	$fecha=date('Y-m-d');
	$vf=explode("-", $fecha);
	$fecha1=$vf[0]."-".$vf[1]."-01";
	$fecha2=$vf[0]."-".$vf[1]."-".$this->validaciones->ultimo_dia($vf[1],$vf[0]);
?>
<table class="tabla tabla-border-false">
	<thead>
		<tr class="fila">
			<td class="celda celda-sm" width="42%"></td>
			<td class="celda celda-sm" width="10%">
				<select id="s_tip" class="form-control input-sm" onchange="view_comprobante();">
					<option value="">Seleccionar...</option>
				<?php foreach ($tipos as $clave => $valor) { ?>
					<option value="<?php echo $clave; ?>"><?php echo $valor; ?></option>
				<?php }?>
				</select>
			</td>
			<td class="celda celda-sm" width="10%">
				<input type="number" id="s_fol" placeholder="Número de folio" class="form-control input-sm"></td>
			<td class="celda td" width="12%">
				<input type="date" class="form-control input-sm" id="s_f1" value="<?php echo $fecha1;?>">
			</td>
			<td class="celda td" width="12%">
				<input type="date" class="form-control input-sm" id="s_f2" value="<?php echo $fecha2;?>">
			</td>
			<td class="celda td" width="14%">
				<?php $new="new_comprobante()"; if($privilegio[0]->co1c!="1"){ $new="";}?>
				<?php $print="print()"; if($privilegio[0]->co1p!="1"){ $print="";}?>
				<?php $this->load->view('estructura/botones/buscador',['f_buscar'=>"view_comprobante()",'f_ver'=>"all_comprobante()",'nuevo'=>'Nuevo','f_nuevo'=>$new,'f_imprimir'=>$print]);?>
			</td>
		</tr>
	</thead>
</table>