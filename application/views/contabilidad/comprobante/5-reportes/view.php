<?php 
	$contPagina=1;
	$tipos= array('T' => 'Traspazo','E' => 'Egreso','I' => 'Ingreso');
	$t_debe=0;
	$t_haber=0;
?>
<div class="row">
	<div class="col-xs-12">
		<div id="area">
			<?php $this->load->view('estructura/print/encabezado',['titulo'=>'DETALLE DE COMPROBANTE','pagina'=>$contPagina,'sub_titulo'=>'']);?>
			<table class="tabla tabla-border-true">
				<tr class="fila">
					<th class="celda th" colspan="8">COMPROBANTE</th>
				</tr>
				<tr class="fila">
					<th class="celda th" width="5%">Fecha:</th>
					<td class="celda td" width="10%"><?php echo $this->lib->format_date($comprobante->fecha,'d/m/Y');?></td>
					<th class="celda th" width="5%">Tipo:</th>
					<td class="celda td" width="7%"><?php echo $tipos[$comprobante->tipo.""];?></td>
					<th class="celda th" width="5%">Folio:</th>
					<td class="celda td" width="5%"><?php echo $comprobante->folio;?></td>
					<th class="celda th" width="5%">Glosa:</th>
					<td class="celda td" width="58%"><?php echo $comprobante->glosa;?></td>
				</tr>
			</table><br>
			<table class="tabla tabla-border-true">
				<tr class="fila">
					<th class="celda th" colspan="8">DETALLE DE COMPROBANTE</th>
				</tr>
				<tr class="fila">
					<th class="celda th" width="5%">#Item</th>
					<th class="celda th" width="8%">Código</th>
					<th class="celda th" width="20%">Cuenta</th>
					<th class="celda th" width="47%">Referencia</th>
					<th class="celda th" width="10%">Debe(Bs.)</th>
					<th class="celda th" width="10%">Haber(Bs.)</th>
				</tr>
		<?php for($i=0; $i < count($detalle_comprobante) ; $i++){ $cuenta=$detalle_comprobante[$i]; ?>
				<tr class="fila">
					<td class="celda td"><?php echo $i+1;?></td>
					<td class="celda td"><?php echo $cuenta->grupo.$cuenta->sub_grupo.$cuenta->rubro.$cuenta->cuenta;?></td>
					<td class="celda td"><?php echo $cuenta->cuenta_text;?></td>
					<td class="celda td"><?php echo $cuenta->referencia;?></td>
					<td class="celda td"><?php echo number_format($cuenta->debe,2,'.',',');$t_debe+=$cuenta->debe;?></td>
					<td class="celda td"><?php echo number_format($cuenta->haber,2,'.',',');$t_haber+=$cuenta->haber;?></td>
				</tr>
		<?php }?>
				<tr class="fila">
					<th class="celda th text-rigth" colspan="4">TOTAL(Bs.)</th>
					<th class="celda th"><?php echo number_format($t_debe,2,'.',',');?></th>
					<th class="celda th"><?php echo number_format($t_haber,2,'.',',');?></th>
				</tr>
			</table>
		</div>
	</div>
</div>
<script type="text/javascript"> $(".nroPagina").html(<?php echo $contPagina;?>); </script>