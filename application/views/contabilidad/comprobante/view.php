<?php
	$debe=0;$haber=0;
	$j_comprobantes=json_encode($comprobantes);
	$tipos= array('T' => 'Traspazo','E' => 'Egreso','I' => 'Ingreso');
?>

		<table class="table table-bordered table-striped table-hover">
			<thead>
				<tr><th colspan="13" style="text-align:center;"><h3><ins>COMPROBATES</ins></h3>Del <?php echo $this->validaciones->formato_fecha($fecha1,'d,m,Y');?> al <?php echo $this->validaciones->formato_fecha($fecha2,'d,m,Y');?></th></tr>
				<tr><th width="5%">#Item</th>
				<th width="13%">Fecha</th>
				<th width="7%">Tipo</th>
				<th width="5%">Folio</th>
				<th width="40%">Glosa</th>
				<th width="10%">Debe (Bs.)</th>
				<th width="10%">Haber (Bs.)</th>
				<th width="10%"></th></tr>
			</thead>
			<tbody>
		<?php if(count($comprobantes)>0){
			for ($i=0; $i < count($comprobantes); $i++) { $comprobante=$comprobantes[$i];
			?>
			<tr><td><?php echo $i+1;?></td>
				<td><?php echo $this->lib->format_date($comprobante->fecha,'d ml Y');?></td>
				<td><?php echo $comprobante->tipo;?></td>
				<td><?php echo $comprobante->folio;?></td>
				<td><?php echo $comprobante->glosa;?></td>
				<td><?php echo number_format($comprobante->debe,2,'.',',');$debe+=$comprobante->debe;?></td>
				<td><?php echo number_format($comprobante->haber,2,'.',',');$haber+=$comprobante->haber;?></td>
				<td>
				<?php $mod="config_comprobante('".$comprobante->idcm."')"; if($privilegio[0]->co1u!="1"){ $mod="";}?>
				<?php $del="alerta_comprobante('".$comprobante->idcm."','".$tipos[$comprobante->tipo]."','".$comprobante->folio."')"; if($privilegio[0]->co1d!="1"){ $del="";}?>
				<?php $this->load->view('estructura/botones/botones_registros',['reportes'=>"reportes_comprobante('".$comprobante->idcm."')",'configuracion'=>$mod,'eliminar'=>$del]);?></td>
			</tr>
			<?php
			}// end for
		}else{ 
		?>
			<td colspan="8">No Existen Comprobantes en fecha <?php echo $fecha1.' al '.$fecha2; ?></td>
		<?php } ?>
			</tbody>
			<thead>
				<tr><th colspan="5">TOTAL (Bs.)</th>
				<th><?php echo number_format($debe,2,'.',',');?></th>
				<th><?php echo number_format($haber,2,'.',',');?></th>
				<th></th></tr>
			</thead>
		</table>
<script language="javascript" type="text/javascript"> 
	$("#print").removeAttr("onclick"); $("#print").unbind("click"); 
	$("#print").click(function(){ imprimir_comprobantes('<?php echo $j_comprobantes;?>','<?php echo $fecha1;?>','<?php echo $fecha2;?>'); }); 
</script>
