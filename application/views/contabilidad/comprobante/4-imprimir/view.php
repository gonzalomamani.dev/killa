<?php 
	$comprobantes=json_decode($comprobantes);
	$contPagina=1;
	$contReg=0;
	$tipos= array('T' => 'Traspazo','E' => 'Egreso','I' => 'Ingreso');
?>
<div class="pagina table-responsive">
	<?php $this->load->view('estructura/print/encabezado',['titulo'=>'DETALLE DE COMPROBANTES','pagina'=>$contPagina,'sub_titulo'=>'Del '.$this->validaciones->formato_fecha($fecha1,'d,m,Y').' al '.$this->validaciones->formato_fecha($fecha2,'d,m,Y')]);?>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php $cols=0;?>
			<?php if(!isset($v1)){?><th class="celda th" width="5%" >#Item</th><?php $cols++;} ?>
			<?php if(!isset($v2)){?><th class="celda th" width="8%">Fecha</th><?php $cols++;} ?>
			<?php if(!isset($v3)){?><th class="celda th" width="8%">Tipo</th><?php $cols++;} ?>
			<?php if(!isset($v4)){?><th class="celda th" width="5%" >Folio</th><?php $cols++;} ?>
			<?php if(!isset($v5)){?><th class="celda th" width="56%">Glosa</th><?php $cols++;} ?>
			<?php if(!isset($v6)){?><th class="celda th" width="9%" >Debe(Bs.)</th><?php } ?>
			<?php if(!isset($v7)){?><th class="celda th" width="9%" >Haber(Bs.)</th><?php } ?>
		</tr>
		<?php $cont=0; $total_debe=0;$total_haber=0; $sub_total_debe=0; $sub_total_haber=0;
			foreach($comprobantes as $key => $comprobante){ ?>
		<?php if($contReg>=$nro){
				$contReg=0;
				$contPagina++;
		?>
			<tr class="fila"><?php if($cols>0){?><th colspan="<?php echo $cols;?>" class="celda td text-right">SUB TOTAL(Bs.):</th><?php }?><?php if(!isset($v6)){?><th class="celda td"><?php echo number_format($sub_total_debe,2,'.',',');$sub_total_debe=0;?></th><?php }?><?php if(!isset($v7)){?><th class="celda td"><?php echo number_format($sub_total_haber,2,'.',',');$sub_total_haber=0;?></th><?php }?></tr>
			<tr class="fila"><?php if($cols>0){?><th colspan="<?php echo $cols;?>" class="celda td text-right">TOTAL(Bs.):</th><?php }?><?php if(!isset($v6)){?><th class="celda td"><?php echo number_format($total_debe,2,'.',',');?></th><?php }?><?php if(!isset($v7)){?><th class="celda td"><?php echo number_format($total_haber,2,'.',',');?></th><?php }?></tr>
		</table><!--cerramos tabla-->
	</div><!--cerramos pagina-->
<div class="pagina table-responsive">
	<?php $this->load->view('estructura/print/encabezado',['titulo'=>'DETALLE DE COMPROBANTES','pagina'=>$contPagina,'sub_titulo'=>'']);?>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php $cols=0;?>
			<?php if(!isset($v1)){?><th class="celda th" width="5%" >#Item</th><?php $cols++;} ?>
			<?php if(!isset($v2)){?><th class="celda th" width="8%">Fecha</th><?php $cols++;} ?>
			<?php if(!isset($v3)){?><th class="celda th" width="8%">Tipo</th><?php $cols++;} ?>
			<?php if(!isset($v4)){?><th class="celda th" width="5%" >Folio</th><?php $cols++;} ?>
			<?php if(!isset($v5)){?><th class="celda th" width="56%">Glosa</th><?php $cols++;} ?>
			<?php if(!isset($v6)){?><th class="celda th" width="9%" >Debe(Bs.)</th><?php } ?>
			<?php if(!isset($v7)){?><th class="celda th" width="9%" >Haber(Bs.)</th><?php } ?>
		</tr>
		<?php $sub_total=0;}//end if ?>
		<tr class="fila">
			<?php if(!isset($v1)){?><td class="celda td"><?php echo $cont+1;$cont++;?></td><?php } ?>
			<?php if(!isset($v2)){?><td class="celda td"><?php echo $this->lib->format_date($comprobante->fecha,'d/m/Y');?></td><?php } ?>
			<?php if(!isset($v3)){?><td class="celda td"><?php echo $tipos[$comprobante->tipo.""];?></td><?php } ?>
			<?php if(!isset($v4)){?><td class="celda td"><?php echo $comprobante->folio;?></td><?php } ?>
			<?php if(!isset($v5)){?><td class="celda td"><?php echo $comprobante->glosa;?></td><?php } ?>
			<?php if(!isset($v6)){?><td class="celda td"><?php echo number_format($comprobante->debe,2,'.',',');$total_debe+=$comprobante->debe;$sub_total_debe+=$comprobante->debe;?></td><?php } ?>
			<?php if(!isset($v7)){?><td class="celda td"><?php echo number_format($comprobante->haber,2,'.',',');$total_haber+=$comprobante->haber;$sub_total_haber+=$comprobante->haber;?></td><?php } ?>
		</tr>
		<?php $contReg++;}// end for ?>
		<tr class="fila"><?php if($cols>0){?><th colspan="<?php echo $cols;?>" class="celda td text-right">SUB TOTAL(Bs.):</th><?php }?><?php if(!isset($v6)){?><th class="celda td"><?php echo number_format($sub_total_debe,2,'.',',');$sub_total_debe=0;?></th><?php }?><?php if(!isset($v7)){?><th class="celda td"><?php echo number_format($sub_total_haber,2,'.',',');$sub_total_haber=0;?></th><?php }?></tr>
		<tr class="fila"><?php if($cols>0){?><th colspan="<?php echo $cols;?>" class="celda td text-right">TOTAL(Bs.):</th><?php }?><?php if(!isset($v6)){?><th class="celda td"><?php echo number_format($total_debe,2,'.',',');?></th><?php }?><?php if(!isset($v7)){?><th class="celda td"><?php echo number_format($total_haber,2,'.',',');?></th><?php }?></tr>
	</table>
</div>
<script type="text/javascript"> $(".nroPagina").html(<?php echo $contPagina;?>); </script>