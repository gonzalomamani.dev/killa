<?php 
	$tipos= array('T' => 'Traspazo','E' => 'Egreso','I' => 'Ingreso');
	$help1='title="<h4>Número de comprobante<h4>" data-content="Representa al número de comprobante, este numero es unico y generado automaticamente por el sistema según el tipo de comprobante, <b>el contador se reinicia cada mes</b>, si fuese el caso de que el numero fuera ya registrado puede pulsar el boton actualizar el número."';
	$help2='title="<h4>Tipo de comprobante<h4>" data-content="Representa el tipo de comprobante de Egreso, Ingreso o Traspazo, este campo es obligatorio"';
	$help3='title="<h4>Fecha de comprobante<h4>" data-content="Ingrese la fecha del comprobante el valor vacio no es aceptado."';
	$help4='title="<h4>Glosa<h4>" data-content="Ingrese la glosa del comprobante si tuviera. El contenido de la glosa debe ser en formato alfanumérico hasta 200 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº)<b>"';
	$help5='title="<h4>Cuenta<h4>" data-content="Seleccione la cuenta del movimiento, si no encuentra la cuenta puede adicionar una nueva cuenta en el menu superior <b>plan de cuentas</b> o dar click en el boton <b>+</b>"';
	$help6='title="<h4>Referencia<h4>" data-content="Ingrese la referencia de la cuenta si tuviera. El contenido de la referencia debe ser en formato alfanumérico hasta 200 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº)<b>"';
	$help7='title="<h4>Debe (Bs.)<h4>" data-content="Valor en el debe de la cuenta, el monto aceptado por el sistema es de 0 a 9999999.9, con un máximo de una decimal."';
	$help8='title="<h4>Haber (Bs.)<h4>" data-content="Valor en el haber de la cuenta, el monto aceptado por el sistema es de 0 a 9999999.9, con un máximo de una decimal."';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$popover3='data-toggle="popover" data-placement="top" data-trigger="hover"';
?>
<div class="row"><div class="col-sm-2 col-sm-offset-10 col-xs-12"><strong><span class='text-danger'>(*)</span>Campos obligatorio</strong></div></div>
	<div class="list-group">
		<div class="list-group-item active"><h5 class="list-group-item-heading" id="g_text">DETALLE DEL COMPROBANTE</h5></div>
		<div class="list-group-item">
		  	<div class="row">
		  		<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="input-group">
						<div class="form-control text-right" style="height:60px; font-size:30px;" id="c_nro"></div>
						<span class="btn input-group-addon input-sm"  title="Actualizar número de comprobante" onclick="refresh_nro_cmp();"><i class="glyphicon glyphicon-refresh"></i></span>
						<span class="input-group-addon input-sm" <?php echo $popover.$help1;?>><i class="glyphicon glyphicon-info-sign"></i></span>
					</div>
		  		</div>
		  		<div class="col-md-3 col-sm-6 col-xs-12">
		  			<h5 class="list-group-item-heading"><strong><span class='text-danger'>(*)</span>Tipo de comprobante:</strong></h5>
				   	<div class="input-group">
						<select id="c_tip" class="form-control input-sm" onchange="refresh_nro_cmp()">
							<option value="">Seleccionar...</option>
					<?php foreach ($tipos as $clave => $tipo) { ?>
							<option value="<?php echo $clave; ?>"><?php echo $tipo;?></option>
					<?php } ?>
						</select>
						<span class="input-group-addon input-sm" <?php echo $popover.$help2;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
		  		</div><i class='clearfix visible-sm-block'></i>
		  		<div class="col-md-3 col-sm-6 col-xs-12">
		  			<h5 class="list-group-item-heading"><strong><span class='text-danger'>(*)</span>Fecha de comprobante: </strong></h5>
		  			<div class="input-group">
						<input type="date" class="form-control input-sm" value="<?php echo date('Y-m-d');?>" onchange="refresh_nro_cmp()" id="c_fec">
						<span class="input-group-addon input-sm" <?php echo $popover.$help3;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
		  		</div>
		  		<div class="col-md-3 col-sm-6 col-xs-12">
		  			<h5 class="list-group-item-heading"><strong>Glosa: </strong></h5>
			      	<div class="input-group">
			      		<textarea class="form-control input-sm" id="c_glo" placeholder="Glosa del comprobante" maxlength='200'></textarea>
			      		<span class="input-group-addon input-sm" <?php echo $popover.$help4;?>><i class="glyphicon glyphicon-info-sign"></i></span>
				   	</div>
		  		</div>
		  	</div>
		</div>
	</div>
	<div class="list-group">
		  <div class="list-group-item active">
		    <h5 class="list-group-item-heading" id="g_text">CUENTA EN EL COMPROBANTE</h5>
		  </div>
		<div class="list-group-item">
		  	<div class="row">
		  		<div class="col-xs-12">
					<div class="table-responsive">
						<table id="table-np" class="table table-bordered table-hover">
							<thead>
								<th width="4%">#</th>
								<th width="27%">
									<div class="input-group col-xs-12">
										<span class="input-group-addon input-sm input-sm" <?php echo $popover3.$help5;?>><i class='glyphicon glyphicon-info-sign'></i></span>
										<div class="form-control input-sm" disabled>Nombre de cuenta</div>
										<a href="<?php echo base_url();?>contabilidad?p=7" target="_blank" title="Ir a Plan de cuentas." class="input-group-addon input-sm"><i class="glyphicon glyphicon-plus"></i></a>
									</div>
								</th>
								<th width="40%">
									<div class="input-group col-xs-12">
										<span class="input-group-addon input-sm input-sm" <?php echo $popover3.$help6;?>><i class='glyphicon glyphicon-info-sign'></i></span>
										<div class="form-control input-sm" disabled>Referencia</div>
									</div>
								</th>
								<th width="13%">
									<div class="input-group col-xs-12">
										<span class="input-group-addon input-sm input-sm" <?php echo $popover3.$help7;?>><i class='glyphicon glyphicon-info-sign'></i></span>
										<div class="form-control input-sm" disabled>Debe(Bs.)</div>
									</div>
								</th>
								<th width="13%">
									<div class="input-group col-xs-12">
										<span class="input-group-addon input-sm input-sm" <?php echo $popover3.$help8;?>><i class='glyphicon glyphicon-info-sign'></i></span>
										<div class="form-control input-sm" disabled>Haber(Bs.)</div>
									</div>
								</th>
								<th width="3%"></th>
							</thead>
							<tbody id="content_cuenta"></tbody>
							<thead>
								<tr>
									<th colspan="3" class="text-right">TOTAL (Bs.)</th>
									<th id="t_deb"></th>
									<th id="t_hab"></th>
									<th></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				<div class="col-md-2 col-md-offset-10 col-sm-3 col-sm-offset-9 col-xs-12">
					<button class="btn btn-info col-xs-12" onclick="add_row_cuenta();"><span class='glyphicon glyphicon-plus'></span> Adicionar registro</button>
				</div>
			</div>
		</div>
	</div>
<script language='javascript'>Onfocus("cli");$('[data-toggle="popover"]').popover({html:true});</script>