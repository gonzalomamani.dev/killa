<?php
	$help1='title="<h4>Productos asigandos al cliente<h4>" data-content="Seleccione el producto a ser asigando al pedido, si desea adicionar un nuevo producto al cliente lo puede hacer accediendo por el menu principal izquierdo <b>Cliente/Proveedor</b>, o dando click en el boton <b>+</b>, y dirigirse a la parte de configuración de cliente."';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<tr>
	<td id="auto-num"></td>
	<td>
		<input type="hidden" id="pos<?php echo $row;?>" value="<?php echo $row;?>">
		<select class="form-control input-sm" id="cta<?php echo $row;?>">
			<option value="">Seleccione...</option>
	<?php for ($i=0; $i < count($cuentas) ; $i++) { $cuenta=$cuentas[$i];?>
			<option value="<?php echo $cuenta->idpl;?>"><?php echo $cuenta->cuenta_text;?></option>
	<?php }?>
		</select>
	</td>
	<td><textarea id="ref<?php echo $row;?>" class="form-control input-sm" placeholder="Referencia del registro" maxlength='200'></textarea></td>
	<td>
		<form onsubmit="return save_comprobante()">
			<input type="number" class="form-control input-sm" id="deb<?php echo $row; ?>" onkeyup="refresh_totales_cuentas()" onclick="refresh_totales_cuentas()" value='0' step="any" min='0' max="9999999.9">
		</form>
	</td>
	<td>
		<form onsubmit="return save_comprobante()">
			<input type="number" class="form-control input-sm" id="hab<?php echo $row; ?>" onkeyup="refresh_totales_cuentas()" onclick="refresh_totales_cuentas()" value='0' step="any" min='0' max="9999999.9">
		</form>
	</td>
	<td>
		<?php $this->load->view('estructura/botones/botones_registros',['eliminar'=>"drop_row_cuenta($(this));"]);?>
	</td>
</tr>
<script language='javascript'>Onfocus("prod<?php echo $row;?>");$('[data-toggle="popover"]').popover({html:true});</script>