<?php 
	$productos=json_decode($productos);
	$contPagina=1;
	$contReg=0;
	$url=base_url().'libraries/img/pieza_productos/miniatura/';
?>
<div class="pagina">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="88%"><div class='encabezado_titulo'>REGISTRO DE PRODUCTOS EN ALMACEN</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Almacen: </b><span><?php echo $almacen->nombre;?></span><br>
					<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'></span></div>
				</td></tr>
			</table>
		</div>
	<table class="tabla tabla-border-true">
		<tr class="fila">
				<?php $cols=0;?>
				<?php if(!isset($v1)){?><th class="celda th" width="5%" >#Item</th><?php $cols++;} ?>
				<?php if(!isset($v2)){?><th class="celda th" width="6%" >Fotografía</th><?php $cols++;} ?>
				<?php if(!isset($v3)){?><th class="celda th" width="12%">Código</th><?php $cols++;} ?>
				<?php if(!isset($v4)){?><th class="celda th" width="20%">Nombre</th><?php $cols++;} ?>
				<?php if(!isset($v5)){?><th class="celda th" width="10%">Color</th><?php $cols++;} ?>
				<?php if(!isset($v6)){?><th class="celda th" width="7%" >Cant.</th><?php $cols++;} ?>
				<?php if(!isset($v7)){?><th class="celda th" width="7%" >C/U (Bs.)</th><?php $cols++;} ?>
				<?php if(!isset($v8)){?><th class="celda th" width="7%" >Total (Bs.)</th><?php } ?>
				<?php if(!isset($v9)){?><th class="celda th" width="26%">Observaciónes</th><?php } ?>
		</tr>
	<?php $cont=0; $sub_total=0; $total=0;
		foreach ($productos as $key => $producto){
			$img='default.png';
			$color=$this->M_material->get_material_color($producto->idm); 
			if(!empty($color)){
				$imagen=$this->M_imagen_producto->get_pieza_material($producto->idpim);
				if(count($imagen)>0){
					if($imagen[0]->nombre!='' && $imagen[0]->nombre!=NULL){$img=$imagen[0]->nombre;} 
				}
				if($contReg>=$nro){
					$contReg=0;
					$contPagina++;

			?>
			<tr class="fila">
				<?php if($cols>0){?><th class="celda th" colspan="<?php echo $cols;?>">Sub Total: </th><?php } ?>
				<?php if(!isset($v9)){?><th class="celda th"><?php echo $sub_total;?></th><?php } ?>
				<?php if(!isset($v10)){?><th class="celda th"></th><?php } ?>
			</tr>
			<tr class="fila">
				<?php if($cols>0){?><th class="celda th" colspan="<?php echo $cols;?>">TOTAL: </th><?php } ?>
				<?php if(!isset($v9)){?><th class="celda th"><?php echo $total;?></th><?php } ?>
				<?php if(!isset($v10)){?><th class="celda th"></th><?php } ?>
			</tr>
			</table><!--cerramos tabla-->
		</div><!--cerramos pagina-->
	<div class="pagina">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="90%"><div class='encabezado_titulo'>REGISTRO DE PRODUCTOS EN ALMACEN</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Almacen: </b><span><?php echo $almacen->nombre;?></span><br>
					<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'></span></div>
				</td></tr>
			</table>
		</div>
	<table class="tabla tabla-border-true">
		<tr class="fila">
				<?php $cols=0;?>
				<?php if(!isset($v1)){?><th class="celda th" width="5%" >#Item</th><?php $cols++;} ?>
				<?php if(!isset($v2)){?><th class="celda th" width="6%" >Fotografía</th><?php $cols++;} ?>
				<?php if(!isset($v3)){?><th class="celda th" width="12%">Código</th><?php $cols++;} ?>
				<?php if(!isset($v4)){?><th class="celda th" width="20%">Nombre</th><?php $cols++;} ?>
				<?php if(!isset($v5)){?><th class="celda th" width="10%">Color</th><?php $cols++;} ?>
				<?php if(!isset($v6)){?><th class="celda th" width="7%" >Cant.</th><?php $cols++;} ?>
				<?php if(!isset($v7)){?><th class="celda th" width="7%" >C/U (Bs.)</th><?php $cols++;} ?>
				<?php if(!isset($v8)){?><th class="celda th" width="7%" >Total (Bs.)</th><?php } ?>
				<?php if(!isset($v9)){?><th class="celda th" width="26%">Observaciónes</th><?php } ?>
		</tr>
		<?php $sub_total=0;}//end if ?>
		<tr class="fila">
			<?php if(!isset($v1)){?><td class="celda td"><?php echo $cont+1;$cont++;?></td><?php } ?>
			<?php if(!isset($v2)){?><td class="celda td"><img src="<?php echo $url.$img;?>" style='width:100%'></td><?php } ?>
			<?php if(!isset($v3)){?><td class="celda td"><?php echo $producto->codigo;?></td><?php } ?>
			<?php if(!isset($v4)){?><td class="celda td"><?php echo $producto->nombre;?></td><?php } ?>
			<?php if(!isset($v5)){?><td class="celda td"><?php echo $color[0]->nombre;?></td><?php } ?>
			<?php if(!isset($v6)){?><td class="celda td"><?php if($producto->cantidad>0){ echo $producto->cantidad." u."; } ?></td><?php } ?>
			<?php if(!isset($v7)){?><td class="celda td">
			<?php 
				$accesorios=$this->M_producto->get_accesorios($producto->idp);
				$materiales_liquidos=$this->M_producto->get_materiales_liquidos($producto->idp);
				$materiales=$this->M_producto->get_materiales($producto->idp);
				$indirectos=$this->M_producto->get_materiales_indirectos($producto->idp);
				$procesos=$this->M_producto->get_procesos($producto->idp);
				$procesos_pieza=$this->M_producto->get_procesos_pieza($producto->idp);
				$costo=number_format($this->lib->costo_total_produccion($accesorios,$materiales,$producto->idpim,$materiales_liquidos,$indirectos,$procesos,$procesos_pieza),1,'.','');
				echo $costo;
			?></td><?php } ?>
			<?php if(!isset($v8)){?><td class="celda td"><?php if($producto->cantidad>0){ echo number_format($producto->cantidad*$costo,1,'.',',');  $sub_total+=($producto->cantidad*$costo); $total+=($producto->cantidad*$costo);?></td><?php } ?></td><?php } ?>
			<?php if(!isset($v9)){?><td class="celda td"><?php echo $producto->descripcion; ?></td><?php } ?>
		</tr>
	<?php $contReg++;
		}//end if
	}// end for ?>
			<tr class="fila">
				<?php if($cols>0){?><th class="celda th" colspan="<?php echo $cols;?>">Sub Total: </th><?php } ?>
				<?php if(!isset($v9)){?><th class="celda th"><?php echo $sub_total;?></th><?php } ?>
				<?php if(!isset($v10)){?><th class="celda th"></th><?php } ?>
			</tr>
			<tr class="fila">
				<?php if($cols>0){?><th class="celda th" colspan="<?php echo $cols;?>">TOTAL: </th><?php } ?>
				<?php if(!isset($v9)){?><th class="celda th"><?php echo $total; ?></th><?php } ?>
				<?php if(!isset($v10)){?><th class="celda th"></th><?php } ?>
			</tr>
	</table>
</div>
<script type="text/javascript"> $(".nroPagina").html(<?php echo $contPagina;?>); </script>