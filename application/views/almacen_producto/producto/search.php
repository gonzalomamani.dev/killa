	<table class="tabla tabla-border-false">
		<tr class="fila">
			<td class='celda g-thumbnail'><div class="g-img"></div></td>
			<td class='celda celda-sm-10'>
			<form onsubmit="return view_producto()">
				<input type="search" id="search_cod" placeholder='Código' onkeyup="reset_all_view(this.id)" class="form-control input-sm">
			</form>
			</td>
			<td class='celda celda-sm-20'>
			<form onsubmit="return view_producto()">
				<input type="search" id="search_nom" placeholder='Nombre de Producto' onkeyup="reset_all_view(this.id)" class="form-control input-sm">
			</form>
			</td>
			<td class='celda celda-sm-10'>
				<form onsubmit="return view_producto()">
					<input type="number" id="search_can" placeholder='Stock de producto' onkeyup="reset_all_view(this.id)" class="form-control input-sm" min='0'>
				</form>
			</td>
			<td class='celda-sm' style="width:47%"></td>
			<td class='celda' style="width:13%">
				<?php $this->load->view('estructura/botones/buscador',['f_buscar'=>"view_producto()",'f_ver'=>"view_all_producto()",'nuevo'=>'Adicionar Producto','f_nuevo'=>'form_select_producto()','f_imprimir'=>'imprimir()']);?>
			</td>
		</tr>
	</table>
	<script type="text/javascript">Onfocus('search_cod');</script>