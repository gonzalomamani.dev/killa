<?php $url=base_url().'libraries/img/pieza_productos/miniatura/'; ?>
<div class="panel-group" id="accordion">
<?php for ($i=0; $i < count($productos) ; $i++) { $producto=$productos[$i]; $img="default.png"; ?>
  <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title" style="text-align:left;">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $producto->idp; ?>">
        <?php $swap=$this->M_categoria_producto->get_portada_producto($producto->idp);
          if(count($swap)>0){
            $im=$this->M_imagen_producto->get_pieza_material($swap[0]->idpim);
            $img="default.png";
            if(count($im)>0){ $img=$im[0]->nombre; }
        ?>
              <img src="<?php echo $url.$img;?>" width='50px' class='img-thumbnail'>
            <?php
          }//end if?>
           <?php echo $producto->codigo." - <strong>".$producto->nombre."</strong>";?>
        </a>
      </h4>
    </div>
    <div id="collapse<?php echo $producto->idp; ?>" class="panel-collapse collapse">
      <div class="panel-body">    
      <?php $categorias=$this->M_categoria_producto->get_row('idcp',$producto->idcp); //echo count($categorias).' '.$producto->idcp;?>
      <?php if(count($categorias)>0){?>
      <table class="table table-bordered table-striped">
 	<?php for ($ca=0; $ca < count($categorias); $ca++) { $categoria=$categorias[$ca];
    	   $mat=$this->M_material->get($categoria->idm);
      	$color=$this->M_color->get($mat[0]->idco);
      	$imagenes=$this->M_imagen_producto->get_pieza_material($categoria->idpim);
 	    	$resto=3-count($imagenes);
 	?>
      	<tr><?php for ($img=0; $img < count($imagenes) ; $img++){$imagene=$imagenes[$img];  ?>
      		<td width="10%"><img src="<?php echo $url.$imagene->nombre;?>" width='30px'></td>
      	<?php
      		}
      		for ($img=0; $img < $resto ; $img++) { 
      	?>
      		<td><img src="<?php echo $url.'default.png';?>" width='30px'></td>
      	<?php
      		}//end for
      	?>
      		<td width="65%" style="color:<?php echo $color[0]->codigo;?>; font-weight:bold;"><?php echo $producto->nombre,' - '.$color[0]->nombre;?></td>
      	<?php
      		$almacen_producto=$this->M_almacen_producto->get_row_2n('ida',$ida,'idpim',$categoria->idpim);
      	?>
      		<th>
      		<?php if(count($almacen_producto)>0){ $dp=$almacen_producto[0];?>
      			<div title='Quitar Producto'onclick="change_almacen_produco(this,'red','25','<?php echo $categoria->idpim;?>')" class='btn-circle btn-circle-red btn-circle-25'></div>
			<?php }else{?>
        		<div title='Seleccionar Producto'onclick="change_almacen_produco(this,'red','25','<?php echo $categoria->idpim;?>')" class='btn-circle btn-circle-default btn-circle-25'></div>
			<?php }?>
      		</th>
      	</tr>
      	<?php
    }?>
      </table>
    <?php }?>
      </div>
    </div>
  </div>
 <?php } ?>
</div>
<!--

<div id="sub_search">
<form method="post" action="" onsubmit="return search_add_producto()">
    <table border="1" cellpadding="5" cellspacing="0">
        <tr>
            <td width="17%"></td>
            
            <td width="19%"><input type="text" placeholder='Código' id="cod"></td>
            <td width="45%"><input type="text" placeholder='Nombre' id="nom"></td>
            
            <td width="19%"><button type='submit' class="btn btn-primary"><i class='glyphicon glyphicon-search'></i> Buscar</button></td>
            
        </tr>
    </table>
</form>
</div>
<div class="tabla">  
    <div class="t_titulo"></div>  
    <div class="fila">
   	 	<div class="celda th" style='width: 20%'></div> 
	    <div class="celda th" style='width: 20%'>Código</div>  
		<div class="celda th" style='width: 50%'>Nombre</div>
		<div class="celda th" style='width: 10%'></div>
    </div>
    <?php
    	for($i=0; $i <count($categorias_productos) ; $i++) { $categoria_producto=$categorias_productos[$i];
    		$imagen='default.png';
    		$producto=$this->M_categoria_pieza->get_producto($categoria_producto->idpim);
    		if(!empty($producto)){
			$fotografia=$this->M_imagen_producto->get_col_prod($producto[0]->idp,'nombre');
			//if(count($fotografia)>0){ if($fotografia[0]->nombre!='' && $fotografia[0]->nombre!=NULL){$imagen=$fotografia[0]->nombre;}}
    	?>
    		<div class="fila">
    			<div class="celda td"><img src="<?php echo $url.$imagen;?>" width='95%'></div> 
			    <div class="celda td"><?php echo $producto[0]->codigo;?></div>  
				<div class="celda td"><?php echo $producto[0]->nombre;?></div>
				<div class="celda td"><a href="javascript:" onclick="add_material('<?php echo $producto[0]->idp;?>');" class="btn btn-success">Seleccionar</a></div>
		    </div>
    	<?php
    		}
    	}
    ?>
</div>-->