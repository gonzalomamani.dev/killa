<?php $url=base_url().'libraries/img/pieza_productos/miniatura/'; 
	$j_categoria_productos=json_encode($categoria_productos);
?>
<table class="table table-bordered table-striped table-hover">
	<thead>
	    <tr class="fila">
	    	<th class='celda g-thumbnail'><div class="g-img">#Item</div></th>  
		    <th class="celda-sm-10">Código</th>  
			<th class="celda-sm-20">Nombre</th>
			<th class="celda-sm" style="width: 6%">Color</th>
			<th style='width: 10%'>Stock</th>
			<th style='width: 17%;'>Fecha de Ingreso/Salida</th>
			<th style='width: 7.5%;'>Ingreso</th>
			<th style='width: 7.5%;'>Salida</th>
			<th style='width: 10%;'>Saldo</th>
			<th style='width: 5%;'></th>
	    </tr>
	</thead>
	<tbody>
	<?php
	if(count($categoria_productos)>0){
		for($i=0; $i < count($categoria_productos); $i++){$categoria_producto=$categoria_productos[$i];
			$imagen='default.png';
			$color=$this->M_material->get_material_color($categoria_producto->idm);
			if(!empty($color)){
				$img=$this->M_imagen_producto->get_pieza_material($categoria_producto->idpim);
				if(count($img)>0){
					if($img[0]->nombre!='' && $img[0]->nombre!=NULL){$imagen=$img[0]->nombre;} 
				}
			?>
			<tr class="fila">
		    	<td><div id="item"><?php echo $i+1;?></div><img src="<?php echo $url.$imagen; ?>" width='100%' class='img-thumbnail'></td>  
			    <td><?php echo $categoria_producto->codigo;?></td>  
				<td><?php echo $categoria_producto->nombre;?><span class="sm-cel" style="font-weight:bold; color: <?php echo $color[0]->codigo;?>"><?php echo " (".$color[0]->nombre.")";?></span>
				</td>
				<td class="celda-sm">
					<ul class="list-group" style="font-size:10px; padding: 1px">
						<li class="list-group-item" style="padding-top: 5px;padding-bottom: 5px;color:white; background:<?php echo $color[0]->codigo;?>"><?php echo $color[0]->nombre;?></li>
					</ul>
				</td>
				<td><input class="form-control input-sm input-100" type="number" id="c<?php echo $categoria_producto->idap;?>" disabled="disabled" value="<?php echo $categoria_producto->cantidad;?>"></td>
				<td><input class="form-control input-sm" type="datetime-local" id="fech" value="<?php echo str_replace(' ', 'T', date('Y-m-d H:i'));?>"></td>
				<td>
				<form onsubmit="return IngresoSalida('<?php echo $categoria_producto->idap;?>')">
					<input class="form-control input-sm input-100" type="number" id="i<?php echo $categoria_producto->idap;?>" value="0" placeholder='ingrese número' onkeyup="block(this,'<?php echo $categoria_producto->idap;?>')">
				</form>
				</td>
				<td>
				<form onsubmit="return IngresoSalida('<?php echo $categoria_producto->idap;?>')">
					<input class="form-control input-sm input-100" type="number" id="s<?php echo $categoria_producto->idap;?>" value="0" placeholder='ingrese número' onkeyup="block(this,'<?php echo $categoria_producto->idap;?>')">
				</form>
				</td>
				<td><input class="form-control input-sm input-100" type="number" id="sa<?php echo $categoria_producto->idap;?>" value="<?php echo $categoria_producto->cantidad;?>" placeholder='ingrese número' disabled='disabled'></td>
				<td>
					<?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"IngresoSalida('".$categoria_producto->idap."')",'eliminar'=>"confirmar('".$categoria_producto->idap."','".$categoria_producto->nombre."','".$imagen."')"]);?>
				</td>
		    </tr>
			<?php
		}//end dif
		}
	}else{
		echo "<tr><td colspan='10'><h2>0 registros encontrados...</h2></td></tr>";
	}?>
	</tbody>
</table>
<script language="javascript">
	$("#print").removeAttr("onclick");
	$("#print").unbind("click");
	$("#print").click(function(){ imprimir_producto('<?php echo $j_categoria_productos;?>'); });
</script><br>
