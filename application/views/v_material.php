<?php
	$cargo=$this->session->userdata("cargo");
	if($this->session->userdata("nombre2")!=""){
		$usuario=$this->session->userdata("nombre")." ".$this->session->userdata("nombre2")." ".$this->session->userdata("paterno");
	}else{
		$usuario=$this->session->userdata("nombre")." ".$this->session->userdata("paterno");
	}
    $ci=$this->session->userdata("ci");
?>						

<!DOCTYPE html>
<html lang="es" class="no-js">
<head><?php $this->load->view('estructura/head',['title'=>'Materiales','css'=>'']);?></head>
	<body>
		<?php $this->load->view('estructura/modal');?>
		<div class="contenedor">
		<?php $this->load->view('estructura/menu_izq',['usuario'=>$usuario,'cargo'=>$cargo,'ventana'=>'almacen','privilegio'=>$privilegio[0]]);?>
		<?php $v_menu="Materiales/material/icon-shipping|Ingresos/ingreso/glyphicon glyphicon-arrow-down|Salidas/salida/glyphicon glyphicon-arrow-up|Configuración/config/glyphicon glyphicon-cog"?>
		<?php $this->load->view('estructura/menu_top',['usuario'=>$usuario,'cargo'=>$cargo,'menu'=>$v_menu]);?>
			<div id="search"></div>
			<div id="contenido"></div>
		</div>
		<?php $this->load->view('estructura/js',['js'=>'almacen/material.js']);?>
		<script type="text/javascript">$(document).ready(function(){inicio(<?php echo $almacen;?>)})</script>
	</body>
	<?php 
		switch ($pestania) {
			case '1': $activo='material'; $title="Materiales"; $search="../../material/view_search"; $view="../../material/view_material/".$almacen; break;
			case '2': $activo='ingreso'; $title="Ingreso de materiales"; $search="../../material/search_ingreso"; $view="../../material/view_ingreso/".$almacen; break;
			case '3': $activo='salida'; $title="Salida de materiales"; $search="../../material/search_salida"; $view="../../material/view_salida/".$almacen; break;
			case '5': $activo='config'; $title="Configuración de materiales"; $search=""; $view="../../material/view_config"; break;
		}
	?>
	<script type="text/javascript">activar("<?php echo $activo;?>","<?php echo $title;?>","<?php echo base_url().'almacen/material/'.$almacen;?>?p=<?php echo $pestania;?>"); get_2n('<?php echo $search; ?>',{},'search',false,'<?php echo $view; ?>',{},'contenido',true); </script>
</html>