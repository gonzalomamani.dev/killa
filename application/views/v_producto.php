<?php
	$cargo=$this->session->userdata("cargo");
	if($this->session->userdata("nombre2")!=""){
		$usuario=$this->session->userdata("nombre")." ".$this->session->userdata("nombre2")." ".$this->session->userdata("paterno");
	}else{
		$usuario=$this->session->userdata("nombre")." ".$this->session->userdata("paterno");
	}
    $ci=$this->session->userdata("ci");
?>						

<!DOCTYPE html>
<html lang="es" class="no-js">
	<head><?php $this->load->view('estructura/head',['title'=>'Productos','css'=>'']);?></head>
	<body>
		<?php $this->load->view('estructura/modal');?>
		<div class="contenedor">
		<?php $this->load->view('estructura/menu_izq',['usuario'=>$usuario,'cargo'=>$cargo,'ventana'=>'almacen','privilegio'=>$privilegio[0]]);?>
		<?php $v_menu="Ingreso - Salida/reg/glyphicon glyphicon-sort"?>
		<?php $this->load->view('estructura/menu_top',['usuario'=>$usuario,'cargo'=>$cargo,'menu'=>$v_menu]);?>
			<div id="search"></div>
			<div id="contenido"></div>
		</div>
		<?php $this->load->view('estructura/js',['js'=>'almacen/producto.js']);?>
		<script type="text/javascript">$(document).ready(function(){inicio(<?php echo $almacen;?>)})</script>
	</body>
</html>