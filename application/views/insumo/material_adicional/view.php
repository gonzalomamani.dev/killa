<?php 
	$j_materiales=json_encode($materiales);
	$url=base_url().'libraries/img/materiales_adicionales/miniatura/';
?>
<table class="table table-bordered table-striped">
	<thead><tr>
    	<th class='celda g-thumbnail'><div class="g-img">#Item</div></th>  
	    <th class='celda-sm-10'>Código</th>  
		<th class='celda-sm-30'>Nombre</th>
		<th class='celda-sm' style='width:17%'>Unidad de medida</th>
		<th class='celda-sm' style='width:33%'>Descripcion</th>
		<td style='width:10%'></td>
    </tr></thead>
    <tbody>
    <?php 
    if(count($materiales)>0){
    for($i=0;$i<count($materiales);$i++){ $res=$materiales[$i];?>
    <tr>
    	<td><?php $fotografia="default.png";
				if($res->fotografia!="" & $res->fotografia!=NULL){
					$fotografia=$res->fotografia;
				}
			?>
			<div id="item"><?php echo $i+1;?></div>
			<img src="<?php echo $url.$fotografia;?>" class='img-thumbnail' width='100%'>
    	</td>  
	    <td><?php echo $res->codigo; ?></td>  
		<td><?php echo $res->nombre; ?></td>
		<td class='celda-sm'><?php echo $res->nombre_u;?></td>
		<td class='celda-sm'><?php echo $res->observaciones;?></td>
		<td>
			<?php $mod="form_update_material('".$res->idmi."')"; if($privilegio[0]->ot1u!="1"){ $mod="";}?>
			<?php $del="confirmar_material('".$res->idmi."')"; if($privilegio[0]->ot1d!="1"){ $del="";}?>
			<?php $this->load->view('estructura/botones/botones_registros',['reportes'=>"reportes_material('".$res->idmi."')",'configuracion'=>$mod,'eliminar'=>$del]);?>
		</td>
    </tr>
	<?php }}else{
		echo "<tr><td colspan='8'><h2>0 registros encontrados...</h2></td></tr>";
	} ?>
</tbody></table>
<script language="javascript" type="text/javascript"> 
	$("#print").removeAttr("onclick"); 
	$("#print").unbind("click"); 
	$("#print").click(function(){ imprimir_materiales('<?php echo $j_materiales;?>'); }); 
</script>