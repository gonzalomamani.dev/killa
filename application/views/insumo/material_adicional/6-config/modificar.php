<?php
	$material=$material[0];
	$help1='title="<h4>Subir Fotografía</h4>" data-content="Seleccione una fotografía, preferiblemente una imagen que no sobrepase las dimenciones de <strong>700x700[px]</strong>, para evitar sobre cargar al sistema"';
	$help2='title="<h4>Ingresar nombre de material<h4>" data-content="Ingrese un Nombre alfanumerico de 3 a 200 caracteres puede incluir espacios, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help3='title="<h4>Ingresar codigo de material<h4>" data-content="Ingrese un Código alfanumerico de 2 a 15 caracteres <b>sin espacios</b>, ademas el codigo solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help6='title="<h4>Unidad de medida de material<h4>" data-content="Seleccione una unidad de medida del material, El valor vacio no es aceptado, si desea adicionar una nueva unidad de material lo puede hacer en el menu superior (Configuracion) o dando click en el boton +"';
	$help7='title="<h4>Descripcion del material<h4>" data-content="Ingrese un descripcion alfanumerica de 5 a 700 caracteres puede incluir espacios, ademas la descripcion solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	if(!empty($proveedores)){
		$help8='title="<h4>Proveedor<h4>" data-content="Seleccione el proveedor del material, en caso de no tener un proveedor puede dejar el campo vacio, si desea adicionar un nuevo proveedor lo puede hacer dando click en el boton +, o accediendo por en el menu lateral"';
	}else{
		$help8='title="<h4>Proveedor<h4>" data-content="Imposible modificar el proveedor, es imposible que el material tenga mas de 2 proveedor, para modificar los proveedores en el material debe realizarlo en la seccion de proveedores en el menu lateral, o dando click en el boton +"';
	}
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<div class="row">
	<div class="col-sm-3 col-sm-offset-9 col-xs-12"><strong><span class='text-danger'>(*)</span> Campo obligatorio</strong></div>
	<div class="col-sm-2 col-xs-12"><strong>Fotografía: </strong></div>
	<div class="col-sm-10 col-xs-12">
		<div class="input-group col-xs-12">
			<input class='form-control input-xs' type="file" id="fot">
			<span class="input-group-addon" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div><i class='clear-fix'></i>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Nombre:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<form onsubmit="return update_material('<?php echo $idmi;?>')">
			<div class="input-group col-xs-12">
				<input class='form-control input-xs' type="text" id="nom" placeholder='Nombre de Material' maxlength="200" value="<?php echo $material->nombre;?>">
				<span class="input-group-addon" <?php echo $popover.$help2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div><i class='clear-fix'></i>
	<div class="col-sm-2 col-xs-12"><strong>Código:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<form onsubmit="return update_material('<?php echo $idmi;?>')">
			<div class="input-group">
				<input class='form-control input-xs' type="text" id="cod" placeholder='Código' maxlength="15" value="<?php echo $material->codigo;?>">
				<span class="input-group-addon" <?php echo $popover.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Unidad de medida:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<div class="input-group">
		<a href="<?php echo base_url().'insumo?p=3';?>" target="_blank" title="Ver Configuraciónes" class="input-group-addon"><i class='glyphicon glyphicon-plus'></i></a>
			<select class='form-control input-xs' id="med">
				<option value="">Seleccione...</option>
			<?php
			for ($i=0; $i < count($unidad);$i++){ $u=$unidad[$i]; 
				?>
				<option value="<?php echo $u->idu;?>" <?php if($material->idu==$u->idu){ echo "selected";}?>><?php echo $u->nombre;?></option>
				<?php } ?>
			</select>
			<span class="input-group-addon" <?php echo $popover.$help6;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div><i class='clearfix'></i>
	<div class="col-sm-2 col-xs-12"><strong>Descripción:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<div class="input-group col-xs-12">
			<textarea class="form-control input-xs" id="des" placeholder='Descripción del material'><?php echo $material->observaciones;?></textarea>
			<span class="input-group-addon" <?php echo $popover.$help7;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div><i class='clear-fix'></i>
	<div class="col-sm-2 col-xs-12"><strong>Proveedor:</strong></div>
	<div class="col-sm-10 col-xs-12">
	<div class="input-group col-xs-12">
		<a href="<?php echo base_url().'cliente_proveedor?p=2';?>" target="_blank" title="Ver Proveedores" class="input-group-addon"><i class='glyphicon glyphicon-plus'></i></a>
		<select class='form-control input-xs' id="pro" <?php if(empty($proveedores)){ echo "disabled";}?>>
			<option value="">Seleccione...</option>
		<?php if(isset($proveedores)){
				for ($i=0; $i<count($proveedores); $i++) { $p=$proveedores[$i]; ?>
				<option value="<?php echo $p->idpro;?>" <?php if(!empty($proveedor)){ if($proveedor->idpro==$p->idpro){ echo "selected";}}?>><?php echo $p->nombre;?></option>
		<?php 	}
			}//end if ?>
		</select>
		<span class="input-group-addon" <?php echo $popover.$help8;?>><i class='glyphicon glyphicon-info-sign'></i></span>
	</div>
	</div>	
</div>
<script language='javascript'>Onfocus("nom");$('[data-toggle="popover"]').popover({html:true});</script>