<?php
	$help1='title="<h4>Subir Fotografía</h4>" data-content="Seleccione una fotografía, preferiblemente una imagen que no sobrepase las dimenciones de <strong>700x700[px]</strong>, para evitar sobre cargar al sistema"';
	$help2='title="<h4>Ingresar nombre de material<h4>" data-content="Ingrese un Nombre alfanumerico de 3 a 200 caracteres puede incluir espacios, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help3='title="<h4>Ingresar codigo de material<h4>" data-content="Ingrese un Código alfanumerico de 2 a 15 caracteres <b>sin espacios</b>, ademas el codigo solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help6='title="<h4>Unidad de medida de material<h4>" data-content="Seleccione una unidad de medida del material, El valor vacio no es aceptado, si desea adicionar una nueva unidad de material lo puede hacer en el menu superior (Configuracion)"';
	$help7='title="<h4>Descripcion del material<h4>" data-content="Ingrese un descripcion alfanumerica de 5 a 700 caracteres puede incluir espacios, ademas la descripcion solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help8='title="<h4>Proveedor<h4>" data-content="Seleccione el proveedor del material, en caso de no tener un proveedor puede dejar el campo vacio, si desea adicionar un nuevo proveedor lo puede hacer dando click en el boton +, o accediendo por en el menu lateral"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<div class="row">
	<div class="col-sm-3 col-sm-offset-9 col-xs-12"><strong><span class='text-danger'>(*)</span> Campo obligatorio</strong></div>
	<div class="col-sm-2 col-xs-12"><strong>Fotografía: </strong></div>
	<div class="col-sm-10 col-xs-12">
		<div class="input-group col-xs-12">
			<input class='form-control input-xs' type="file" id="fot">
			<span class="input-group-addon" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div><i class='clear-fix'></i>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Nombre:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<form onsubmit="return save_material()">
			<div class="input-group col-xs-12">
				<input class='form-control input-xs' type="text" id="nom" placeholder='Nombre de Material' maxlength="200">
				<span class="input-group-addon" <?php echo $popover.$help2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div><i class='clear-fix'></i>
	<div class="col-sm-2 col-xs-12"><strong>Código:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<form onsubmit="return save_material()">
			<div class="input-group">
				<input class='form-control input-xs' type="text" id="cod" placeholder='Código' maxlength="15">
				<span class="input-group-addon" <?php echo $popover.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Unidad de medida:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<div class="input-group">
		<a href="<?php echo base_url().'insumo?p=3';?>" target="_blank" title="Ver Configuraciónes" class="input-group-addon"><i class='glyphicon glyphicon-plus'></i></a>
			<select class='form-control input-xs' id="med">
				<option value="">Seleccione...</option>
			<?php
			for ($i=0; $i < count($unidad);$i++){ $u=$unidad[$i]; 
				?>
				<option value="<?php echo $u->idu;?>"><?php echo $u->nombre;?></option>
				<?php } ?>
			</select>
			<span class="input-group-addon" <?php echo $popover.$help6;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div><i class='clearfix'></i>
	<div class="col-sm-2 col-xs-12"><strong>Descripción:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<div class="input-group col-xs-12">
			<textarea class="form-control input-xs" id="des" placeholder='Descripción del material'></textarea>
			<span class="input-group-addon" <?php echo $popover.$help7;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div><i class='clear-fix'></i>
	<div class="col-sm-2 col-xs-12"><strong>Proveedor:</strong></div>
	<div class="col-sm-10 col-xs-12">
	<div class="input-group col-xs-12">
		<a href="<?php echo base_url().'cliente_proveedor?p=2';?>" target="_blank" title="Ver Proveedores" class="input-group-addon"><i class='glyphicon glyphicon-plus'></i></a>
		<select class='form-control input-xs' id="pro">
			<option value="">Seleccione...</option>
			<?php
				for ($i=0; $i<count($proveedores); $i++) { $proveedor=$proveedores[$i];
					?>
						<option value="<?php echo $proveedor->idpro;?>"><?php echo $proveedor->nombre;?></option>
					<?php
				}
			?>
		</select>
		<span class="input-group-addon" <?php echo $popover.$help8;?>><i class='glyphicon glyphicon-info-sign'></i></span>
	</div>
	</div>	
</div>
<script language='javascript'>Onfocus("nom");$('[data-toggle="popover"]').popover({html:true});</script>