<?php 
	$materiales=json_decode($materiales);
	$contPagina=1;
	$contReg=0;
	$url=base_url().'libraries/img/materiales_varios/miniatura/';
?>
<div class="pagina table-responsive">
	<div class="encabezado_pagina">
		<table class="tabla tabla-border-false"><tr class="fila">
			<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
			<td class="celda td" width="88%"><div class='encabezado_titulo'>REGISTRO DE OTROS MATERIALES E INSUMOS</div></td>
			<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
				<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
				<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
				<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'></span></div>
			</td></tr>
		</table>
	</div>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php if(!isset($v1)){?><th class="celda th" width="5%" >#Item</th><?php } ?>
			<?php if(!isset($v2)){?><th class="celda th" width="6%" >Fotografía</th><?php } ?>
			<?php if(!isset($v3)){?><th class="celda th" width="10%">Código</th><?php } ?>
			<?php if(!isset($v4)){?><th class="celda th" width="36%">Nombre del material o insumo</th><?php } ?>
			<?php if(!isset($v5)){?><th class="celda th" width="10%" >Unidad de medida</th><?php } ?>
			<?php if(!isset($v6)){?><th class="celda th" width="33%">Observaciónes</th><?php } ?>
		</tr>
		<?php $cont=0;
			foreach ($materiales as $key => $material){ 
				$img='default.png';
				if($material->fotografia!=NULL && $material->fotografia!=""){ $img=$material->fotografia;} ?>
			<?php if($contReg>=$nro){
					$contReg=0;
					$contPagina++;
			?>
	</table><!--cerramos tabla-->
</div><!--cerramos pagina-->
<div class="pagina table-responsive">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="90%"><div class='encabezado_titulo'>REGISTRO DE OTROS MATERIALES E INSUMOS</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'></span></div>
				</td></tr>
			</table>
		</div>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php if(!isset($v1)){?><th class="celda th" width="5%" >#Item</th><?php } ?>
			<?php if(!isset($v2)){?><th class="celda th" width="6%" >Fotografía</th><?php } ?>
			<?php if(!isset($v3)){?><th class="celda th" width="10%">Código</th><?php } ?>
			<?php if(!isset($v4)){?><th class="celda th" width="36%">Nombre del material o insumo</th><?php } ?>
			<?php if(!isset($v5)){?><th class="celda th" width="10%" >Unidad de medida</th><?php } ?>
			<?php if(!isset($v6)){?><th class="celda th" width="33%">Observaciónes</th><?php } ?>
		</tr>
		<?php }//end if ?>
		<tr class="fila">
			<?php if(!isset($v1)){?><td class="celda td"><?php echo $cont+1;$cont++;?></td><?php } ?>
			<?php if(!isset($v2)){?><td class="celda td"><img src="<?php echo $url.$img;?>" style='width:100%'></td><?php } ?>
			<?php if(!isset($v3)){?><td class="celda td"><?php echo $material->codigo;?></td><?php } ?>
			<?php if(!isset($v4)){?><td class="celda td"><?php echo $material->nombre;?></td><?php } ?>
			<?php if(!isset($v5)){?><td class="celda td"><?php echo $material->nombre_u.'['.$material->abreviatura.']';?></td><?php } ?>
			<?php if(!isset($v6)){?><td class="celda td"><?php echo $material->observaciones;?></td><?php } ?>
		</tr>
	<?php $contReg++;}// end for ?>
	</table>
</div>
<script type="text/javascript"> $(".nroPagina").html(<?php echo $contPagina;?>); </script>