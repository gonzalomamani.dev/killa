<?php 
	$url=base_url().'libraries/img/materiales_varios/miniatura/';
	$img="default.png";
	$material=$material[0];
	if($material->fotografia!=NULL && $material->fotografia!=""){ $img=$material->fotografia;}
?>
<div class="row">
	<div class="col-sm-3 col-xs-12 text-center"><img src="<?php echo $url.$img;?>" class="img-thumbnail" alt=""><hr></div>
	<div class="col-sm-9 cols-xs-12">
	<table border="0" class="tabla tabla-border-true">
		<tr class="fila">
			<th class="celda th" width="30%">Código del material:</th>
			<td class="celda td" width="70%"><?php echo $material->codigo;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Nombre del material:</th><td class="celda td" colspan="3"><?php echo $material->nombre;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Unidad de medida:</th><td class="celda td" colspan="3"><?php echo $material->nombre_u.' ['.$material->abreviatura.']';?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Observaciónes:</th><td class="celda td" colspan="3"><?php echo $material->observaciones;?></td>
		</tr>
	</table>
	</div>
</div>