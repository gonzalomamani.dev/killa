	<table class="tabla tabla-border-false">
	<tr class='fila'>
		<td class='celda g-thumbnail'><div class="g-img"></div></td>
		<td class='celda celda-sm-10'>
			<form onsubmit="return view_insumo()"><input class='form-control input-sm' type="search" id="s_cod" placeholder='Código' onkeyup="reset_view(this.id)"></form>
		</td>
		<td class='celda celda-sm-30'>
			<form onsubmit="return view_insumo()"><input class='form-control input-sm' type="search" id="s_nom" placeholder='Nombre de insumo' onkeyup="reset_view(this.id)"/></form>
		</td>
		<td class='celda-sm' style="width:17%">
			<select class='form-control input-sm' id="s_uni" onchange="reset_view(this.id); view_insumo();">
				<option value="">Seleccionar... Unidad de medida</option>
				<?php for($i=0; $i<count($unidades);$i++){$unidad=$unidades[$i];?>
					<option value="<?php echo $unidad->idu; ?>"><?php echo $unidad->nombre;?></option>	
				<?php } ?>
			</select>
		</td>
		<td class='celda-sm' style="width:30%"></td>
		<td class="celda" style="width:13%">
			<?php $new="new_insumo()"; if($privilegio[0]->ot2c!="1"){ $new="";}?>
			<?php $print="print()"; if($privilegio[0]->ot2p!="1"){ $print="";}?>
			<?php $this->load->view('estructura/botones/buscador',['f_buscar'=>"view_insumo()",'f_ver'=>"view_all_insumo()",'nuevo'=>'Nuevo insumo adicional','f_nuevo'=>$new,'f_imprimir'=>$print]);?>
		</td>	
		
	</tr>		
	</table>
<script type="text/javascript">Onfocus('s_cod');</script>