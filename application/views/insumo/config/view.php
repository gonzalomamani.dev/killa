<?php
	$help1='title="<h4>Nombre de unidad de medida<h4>" data-content="Ingrese un nombre alfanumerico <strong>de 2 a 40 caracteres con espacios</strong>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help2='title="<h4>Abreviatura de unidad de medida<h4>" data-content="Ingrese una abreviatura alfanumerica <strong>de 1 a 10 caracteres con espacios</strong>, ademas la abreviatura solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>. Ej. Centímetros = cm"';
	$help3='title="<h4>Equivalencia de unidad<h4>" data-content="Ingrese una equivalencia numerica mayor que cero y con un maximo de 7 decimales. Esta equivalencia en usada por el sistema para calcular los materiales necesarios en producción."';
	$help4='title="<h4>Descripción de equivalencia<h4>" data-content="Ingrese una descripción de equivalencia alfanumerica de 0 a 200 caracteres <b>con espacios</b>, ademas la descripción solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>. Esta descripción debe contener a que unidad pertenece la equivalencia."';
	$popover='data-toggle="popover" data-placement="right" data-trigger="hover"';
	$popover2='data-toggle="popover" data-placement="left" data-trigger="hover"';	
?>
<div class="col-md-6 col-xs-12">
	<h3>Material: Unidades de Medida</h3>
	<div class="table-responsive">
	<table class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th style="width:7%">#</th>
				<th style="width:23%">
				<div class="input-group col-xs-12">
					<span class="input-group-addon" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					<div class="form-control input-sm" disabled>NOMBRE</div>					
				</div></th>
				<th style="width:10%">
				<div class="input-group col-xs-12">
					<span class="input-group-addon" <?php echo $popover.$help2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					<div class="form-control input-sm" disabled>ABR.</div>
				</div>
				</th>
				<th style="width:10%">
				<div class="input-group col-xs-12">
					<span class="input-group-addon" <?php echo $popover.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					<div class="form-control input-sm" disabled>EQU.</div>
				</div>
				</th>
				<th style="width:40%">
				<div class="input-group col-xs-12">
					<span class="input-group-addon" <?php echo $popover2.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					<div class="form-control input-sm" disabled>DES.</div>
				</div>
				</th>
				<th style="width:10%"></th>
			</tr>
		</thead>
		<tbody>
		<?php for ($i=0; $i < count($unidades) ; $i++) { $unidad=$unidades[$i]; $m=$this->M_material_item->get_row('idu',$unidad->idu); ?>
			<tr>
				<td><?php echo $i+1;?></td>
				<td><form onsubmit="return update_unidad('<?php echo $unidad->idu;?>')"><input type="text" id="nom_u<?php echo $unidad->idu;?>" value='<?php echo $unidad->nombre;?>' class="form-control input-sm" placeholder='Nombre de unidad de medida' maxlength="40"></form></td>
				<td><form onsubmit="return update_unidad('<?php echo $unidad->idu;?>')"><input type="text" id="abr_u<?php echo $unidad->idu;?>" value="<?php echo $unidad->abreviatura;?>" class="form-control input-sm" placeholder='Abreviatura de unidad de medida' maxlength="10"></form></td>
				<td><form onsubmit="return update_unidad('<?php echo $unidad->idu;?>')"><input type="number" id="equ_u<?php echo $unidad->idu;?>" value="<?php echo $unidad->equivalencia;?>" class="form-control input-sm" placeholder='Equivalencia de unidad de medida' min="0" value="0"></form></td>
				<td><textarea id="des_equ<?php echo $unidad->idu;?>" class="form-control input-sm" placeholder='Descripcion de equivalencia' maxlength="200"><?php echo $unidad->descripcion_equ;?></textarea></td>
				<?php $str="";
				if(count($m)>0){
					$str="<hr><strong class='text-danger'>¡Imposible Eliminar la unidad de medida, esta siendo usado por ".count($m)." materiales en almacenes, materiales en producto o materiales varios!</strong>";
					$fun="disabled";
				}else{
					$fun="alerta_unidad('".$unidad->idu."')";
				}
				$help='title="<h4>'.$unidad->nombre.'<h4>" data-content="<b>Abreviatura: </b>'.$unidad->abreviatura.'.<br><b>Equivalencia: </b>'.$unidad->equivalencia.'<br><b>Descripción de equivalencia: </b>'.$unidad->descripcion_equ.'<br>'.$str.'"';
				?>
				<td><?php $this->load->view('estructura/botones/botones_registros',['detalle'=>$popover2.$help,'guardar'=>"update_unidad('".$unidad->idu."')",'eliminar'=>$fun]);?></td>
			</tr>
		<?php } ?>
		</tbody>
		<thead>
			<tr>
				<th colspan="6" class="text-center">NUEVO</th>
			</tr>
			<tr>
				<td colspan="2"><form onsubmit="return save_unidad()"><input type="text" id="nom_u" class="form-control input-sm" placeholder='Nombre de unidad de medida' maxlength="40"></form></td>
				<td><form onsubmit="return save_unidad()"><input type="text" id="abr_u" class="form-control input-sm" placeholder='Abreviatura de unidad de medida' maxlength="10"></form></td>
				<td><form onsubmit="return save_unidad()"><input type="number" id="equ_u" class="form-control input-sm" placeholder='Equivalencia de unidad de medida' min="0" value="0"></form></td>
				<td><textarea id="des_equ" class="form-control input-sm" placeholder='Descripcion de equivalencia' maxlength="200"></textarea></td>
				<td><?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"save_unidad()",'detalle'=>"",'eliminar'=>""]);?></td>
			</tr>
		</thead>
	</table>
	</div>
</div>
<script language='javascript'>$('[data-toggle="popover"]').popover({html:true});</script>