<?php
	$cargo=$this->session->userdata("cargo");
	if($this->session->userdata("nombre2")!=""){
		$usuario=$this->session->userdata("nombre")." ".$this->session->userdata("nombre2")." ".$this->session->userdata("paterno");
	}else{
		$usuario=$this->session->userdata("nombre")." ".$this->session->userdata("paterno");
	}
    $ci=$this->session->userdata("ci");
?>						
<!DOCTYPE html>
<html lang="es" class="no-js">
	<head><?php $this->load->view('estructura/head',['title'=>'Pedidos y Ventas','css'=>'']);?></head>
	<body>
		<?php $this->load->view('estructura/modal');?>
		<div class="contenedor">
			<?php $this->load->view('estructura/menu_izq',['usuario'=>$usuario,'cargo'=>$cargo,'ventana'=>'pedidos_ventas','privilegio'=>$privilegio[0]]);?>
			<?php $v_menu="||"?>
			<?php $v_menu="";
				if($privilegio[0]->mo1r==1){ $v_menu.="Pedidos/pedido/icon-clipboard2|"; }
				if($privilegio[0]->mo2r==1){ $v_menu.="Ventas/venta/icon-clipboard|";}
				if($privilegio[0]->mo3r==1){ $v_menu.="Compras o egresos/compra/icon-cart";}
			?>
			<?php $this->load->view('estructura/menu_top',['usuario'=>$usuario,'cargo'=>$cargo,'menu'=>$v_menu]);?>
			<div id="search"></div>
			<div id="contenido"></div>
		</div>
		<?php $this->load->view('estructura/js',['js'=>'pedidos_ventas/pedidos_ventas.js']);?>
	</body>
	<?php 
		switch ($pestania) {
			case '1': if($privilegio[0]->mo1r==1){ $title="Pedidos";$activo='pedido'; $search="pedidos_ventas/search_pedido"; $view="pedidos_ventas/view_pedido";} break;
			case '2': if($privilegio[0]->mo2r==1){ $title="Ventas";$activo='venta'; $search="pedidos_ventas/search_venta"; $view="pedidos_ventas/view_venta";} break;
			case '3': if($privilegio[0]->mo3r==1){ $title="Compras";$activo='compra'; $search="pedidos_ventas/search_compra"; $view="pedidos_ventas/view_compra";} break;
		}
	?>
	<script type="text/javascript">activar('<?php echo $activo;?>','<?php echo $title;?>','pedidos_ventas?p=<?php echo $pestania;?>'); get_2n('<?php echo $search; ?>',{},'search',false,'<?php echo $view; ?>',{},'contenido',true); </script>
</html>