<div class="tabla">  
  
        <div class="t_titulo"></div>  
        <div class="fila">
        	<div class="celda th" style='width: 5%'>Item</div> 
        	<div class="celda th" style='width: 10%'>Nº Pedido</div>  
	  		<div class="celda th" style='width: 35%'>Fecha de Pedido</div>
	  		<div class="celda th" style='width: 35%'>Fecha de Entrega</div>
	  		<div class="celda th" style='width: 15%'>Asignar Tareas</div>
        </div>
        <?php for ($i=0; $i < count($pedidos) ; $i++) { $pedido=$pedidos[$i];
        	?>
        	<div class="fila">
	        	<div class="celda th"><?php echo $i+1;?></div> 
	        	<div class="celda td"><?php echo $pedido->idpe;?></div>  
		  		<div class="celda td"><?php echo $this->validaciones->formato_fecha($pedido->fecha_pedido,'li,d,m,Y');?></div>
		  		<div class="celda td"><?php echo $this->validaciones->formato_fecha($pedido->fecha_entrega,'li,d,m,Y');?></div>
		  		<div class="celda td"><a href="javascript:" onclick="view_orden_trabajo('<?php echo $pedido->idpe;?>')">Ir</a></div>
	        </div>
        	<?php
        }?>
</div>  
