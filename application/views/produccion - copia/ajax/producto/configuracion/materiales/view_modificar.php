<script type="text/javascript">button_modal('modal_ok','','','modal_print','','','modal_closed');</script>
<?php 
	if(count($materiales)>0){
		?>
		<table class="tabla">
			<?php
				for ($i=0; $i < count($materiales) ; $i++) { $material=$materiales[$i];
					$name_img="default.png";
					if($material->fotografia!="" & $material->fotografia!=null){
						$name_img=$material->fotografia;
					}
					$unidad=$this->M_unidad->get_col($material->idu,'nombre');
					?>
					<tr class="fila">
						<th class='celda th' width="15%"><img src="<?php echo base_url().'libraries/img/materiales/miniatura/'.$name_img;?>" width='95%'></th>
						<th class='celda th' width="15%"><?php echo $material->cod;?></th>
						<th class='celda td' width="50%"><?php echo $material->nombre;?></th>
						<th class='celda th' width="20%"><a href="javascript:" onclick="select_update_material('<?php echo $idpm;?>','<?php echo $idp;?>','<?php echo $material->idi;?>')">Seleccionar</a></th>
					</tr>
					<?php
				}
			?>
		</table>
		<?php
	}else{
		echo "<h3>No existen Materiales en el Almacen</h3>";
	}
?>