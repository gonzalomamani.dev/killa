<?php $res=$producto[0];
	$url_prod=base_url().'libraries/img/pieza_productos/'; 
?>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="config('<?php echo $res->idp; ?>')">Modificar</a></li>
  <li role="presentation"><a href="javascript:" onclick="materiales('<?php echo $res->idp; ?>')">Materiales</a></li>
  <li role="presentation"><a href="javascript:" onclick="piezas('<?php echo $res->idp; ?>')">Piezas</a></li>
  <li role="presentation"><a href="javascript:" onclick="material_indirecto('<?php echo $res->idp; ?>')">Materiales indirectos</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="procesos('<?php echo $res->idp; ?>')">Procesos</a></li>
  <li role="presentation"><a href="javascript:" onclick="tiempos_costos('<?php echo $res->idp; ?>')">Tiempo/Costo de Producción</a></li>
</ul>
<table class="tabla">
	<tr class="fila">
	<?php $swap=$this->M_categoria_producto->get_portada_producto($res->idp);
			$resto=3;
			if(count($swap)>0){
				$im=$this->M_imagen_producto->get_pieza_material($swap[0]->idpim);
				$img="default.png";
				$resto-=count($im);
				 for ($i=0; $i < count($im); $i++) {$img_producto=$im[$i];
				?>
				<td class="celda td" width="9%" rowspan="3"><img src="<?php echo $url_prod.$img_producto->nombre;?>" width='100%'></td>
				<?php
				}
			}
			for($j=0; $j < $resto; $j++){ 
				?>
				<td class="celda td" width="9%" rowspan="3"><img src="<?php echo $url_prod.'default.png';?>" width='100%'></td>
				<?php
			}
	?>
		<th class="celda th" width="73%"><h3><?php echo $res->nombre;?></h3></th>
		
	</tr>

	
	<tr class="fila"><th class="celda th"><br></th></tr>
	<tr class="fila"><th class="celda th"><br></th></tr>
</table>
<table class="tabla">
	<tr class='fila'>
		<td class='celda td' colspan="3"><h3>Procesos</h3></td>
	</tr>

			<?php for ($i=0; $i < count($producto_procesos) ; $i++) { $pm=$producto_procesos[$i];
				?>
					<tr class='fila'><td class='celda td' width="30%"><?php echo $pm->nombre; ?></td>
						<td class='celda td' width="60%"><?php echo $pm->sub_proceso;?></td>
						<td class='celda td' width="10%"><button onclick="drop_proceso('<?php echo $pm->idp;?>','<?php echo $pm->idppr;?>')">eliminar</button></td>
				</tr>
				<?php
			}?>
	<tr class='fila'>
		<td class='celda td' colspan="3"><h4>NUEVO PROCESO</h4></td>
	</tr>

				<tr class='fila'>
					<td class='celda th' width="45%">Proceso</td>
					<td class='celda th' width="45%">Descripción</td>
					<td class='celda th' width="10%"></td>
				</tr>

				<tr class='fila'>
					<td class='celda td'>
					<select id="proc">
						<option value="">Seleccionar...</option>
						<?php for($i=0;$i<count($procesos);$i++){ $material=$procesos[$i]; ?>
							<option value="<?php echo $material->idpr;?>"><?php echo $material->nombre;?></option>
						<?php }?>
					</select> </td>
					<td class='celda td'><textarea id="desc" placeholder='Descripción del proceso'></textarea></td>
					<td class='celda td'></td>
				</tr>
			
</table>
<script type="text/javascript">cerrar_modal('content_modal','modal');</script>