<?php $p=$pieza[0];
	$url=base_url().'libraries/img/piezas/miniatura/';
	$img='default.png';
	if($p->imagen!='' && $p->imagen!=NULL){$img=$p->imagen;}
?>
<h2>PIEZA: Modificar</h2>
<table border="0" cellpadding="3" width="100%">
	<tr>
		<th rowspan="6" width="20%">
			<img src="<?php echo $url.$img;?>" width='95%'>
		</th>
		<th>Fotografía</th>
	<td colspan="3"><input type="file" id="fot"></td></tr>
	<tr><th>Nombre</th>
	<td colspan="3"><input type="text" id="nom" placeholder='Nombre de Pieza' value="<?php echo $p->nombre;?>"></td></tr>
	<tr><th width="20%">Código</th>
	<td width="20%"><input type="text" id="cod" placeholder='Código de Pieza' value="<?php echo $p->codigo;?>"></td>
	<th width="20%">Grupo</th>
	<td width="20%">
		<select id="gru">
			<option value="">Seleccione...</option>
		<?php
		for ($i=0; $i<count($pieza_grupos);$i++){ $g=$pieza_grupos[$i]; 
			?>
			<option value="<?php echo $g->idpig;?>" <?php if($p->idpig==$g->idpig){ echo "selected";} ?>><?php echo $g->nombre;?></option>
			<?php
		}
		?>
		</select>
	</td></tr>
	<tr><th>Alto (cm.)</th>
	<td><input type="number" id="alt" placeholder='Alto' value="<?php echo $p->alto;?>"></td>
	<th>Ancho (cm.)</th>
	<td><input type="number" id="anc" placeholder='Ancho' value="<?php echo $p->ancho;?>"></td></tr>
	<tr>
	<th>Cantidad de Piezas</th>
	<td><input type="number" id="can" placeholder='Cantidad' value="<?php echo $p->unidades;?>"></td>
	<th></th>
	<td></td></tr>
	<tr>
		<th>Descripción</th>
		<td colspan="3"><textarea id="des" placeholder='Descripción del material'><?php echo $p->descripcion;?></textarea></td>
	</tr>
</table>

<!--<script type="text/javascript">addClick_v2('modal_ok','save_pieza',"")</script>-->