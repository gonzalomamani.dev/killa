<h3>PIEZA: Nuevo</h3>
<hr>
<table border="0" cellpadding="3" width="100%">
	<tr><th width="25%">Fotografía</th>
	<td colspan="3"><input type="file" id="fot"></td></tr>
	<tr><th>Nombre</th>
	<td colspan="3"><input type="text" id="nom" placeholder='Nombre de Pieza'></td></tr>
	<tr><th width="20%">Código</th>
	<td width="30%"><input type="text" id="cod" placeholder='Código de Pieza'></td>
	<th width="20%">Grupo</th>
	<td width="30%">
		<select id="gru">
			<option value="">Seleccione...</option>
		<?php
		for ($i=0; $i<count($pieza_grupos);$i++){ $g=$pieza_grupos[$i]; 
			?>
			<option value="<?php echo $g->idpig;?>"><?php echo $g->nombre;?></option>
			<?php
		}
		?>
		</select>
	</td></tr>
	<tr><th>Alto (cm.)</th>
	<td><input type="number" id="alt" placeholder='Alto'></td>
	<th>Ancho (cm.)</th>
	<td><input type="number" id="anc" placeholder='Ancho'></td></tr>
	<tr>
	<th>Cantidad de Piezas</th>
	<td><input type="number" id="can" placeholder='Cantidad' value="1"></td>
	<th></th>
	<td></td></tr>
	<tr>
		<th>Descripción</th>
		<td colspan="3"><textarea id="des" placeholder='Descripción del material'></textarea></td>
	</tr>
</table>
<!--<script type="text/javascript">addClick_v2('modal_ok','save_pieza',"")</script>-->
