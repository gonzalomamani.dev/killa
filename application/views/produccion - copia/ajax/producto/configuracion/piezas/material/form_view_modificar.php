<div id="sub_search">

	<table class="tabla">
		<tr class='fila'>
			<th class='celda th' width="15%">Grupo</th>
			<th class='celda th' width="15%">Código</th>
			<th class='celda th' width="50%">Nombre</th>
			<th class='celda th' width="20%">Almacen</th>
		</tr>
			<th class='celda th'>
				<select id="gru_m" onchange="search_mod_producto_pieza('<?php echo $idpi;?>','<?php echo $idcp;?>','<?php echo $idp;?>')" onclick="reset_all_view(this.id)">
					<option value="">Seleccione...</option>
					<?php
					for ($i=0; $i < count($grupos) ; $i++) { $grupo=$grupos[$i];
						?>
						<option value="<?php echo $grupo->idg;?>"><?php echo $grupo->nombre;?></option>
						<?php
					}
					?>
				</select>
			</th>
			<th class='celda th'><input type="text" id="cod_m" onkeyup="search_mod_producto_pieza('<?php echo $idpi;?>','<?php echo $idcp;?>','<?php echo $idp;?>')" onclick="reset_all_view(this.id)" placeholder='código de material'></th>
			
			<th class='celda th'><input type="text" id="nom_m" onkeyup="search_mod_producto_pieza('<?php echo $idpi;?>','<?php echo $idcp;?>','<?php echo $idp;?>')" onclick="reset_all_view(this.id)" placeholder='nombre de material'></th>
			<th class='celda th'>
				<select id="alm_m" onchange="search_mod_producto_pieza('<?php echo $idpi;?>','<?php echo $idcp;?>','<?php echo $idp;?>')">
				<?php 
					for ($i=0; $i < count($almacenes); $i++) { $almacen=$almacenes[$i];
						?>
						<option value="<?php echo $almacen->ida;?>"><?php echo $almacen->nombre;?></option>
						<?php
					}
				?>
				</select>
			</th>
		</tr>

	</table>
</div>
<div id="sub_result"></div>