<?php $res=$producto[0];
	$url_prod=base_url().'libraries/img/pieza_productos/'; 
?>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="config('<?php echo $res->idp; ?>')">Modificar</a></li>
  <li role="presentation"><a href="javascript:" onclick="materiales('<?php echo $res->idp; ?>')">Materiales</a></li>
  <li role="presentation"><a href="javascript:" onclick="piezas('<?php echo $res->idp; ?>')">Piezas</a></li>
  <li role="presentation"><a href="javascript:" onclick="material_indirecto('<?php echo $res->idp; ?>')">Materiales indirectos</a></li>
  <li role="presentation"><a href="javascript:" onclick="procesos('<?php echo $res->idp; ?>')">Procesos</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="tiempos_costos('<?php echo $res->idp; ?>')">Tiempo/Costo de Producción</a></li>
</ul>
<table class="tabla">
	<tr class="fila">
	<?php $swap=$this->M_categoria_producto->get_portada_producto($res->idp);
			$resto=3;
			if(count($swap)>0){
				$im=$this->M_imagen_producto->get_pieza_material($swap[0]->idpim);
				$img="default.png";
				$resto-=count($im);
				 for ($i=0; $i < count($im); $i++) {$img_producto=$im[$i];
				?>
				<td class="celda td" width="9%" rowspan="3"><img src="<?php echo $url_prod.$img_producto->nombre;?>" width='100%'></td>
				<?php
				}
			}
			for($j=0; $j < $resto; $j++){ 
				?>
				<td class="celda td" width="9%" rowspan="3"><img src="<?php echo $url_prod.'default.png';?>" width='100%'></td>
				<?php
			}
	?>
		<th class="celda th" width="73%"><h3><?php echo $res->nombre;?></h3></th>
		
	</tr>

	
	<tr class="fila"><th class="celda th"><br></th></tr>
	<tr class="fila"><th class="celda th"><br></th></tr>
</table>
<div>
<?php if(count($producto_procesos)>0){?>
	<div class="tabla">  
  
        <div class="t_titulo">Tiempo y costo de producción</div>  
        <div class="fila">
        	<div class="celda th" style='width: 55%'>Proceso</div>  
	        <div class="celda th" style='width: 25%'>Tiempo</div>  
	  		<div class="celda th" style='width: 10%'>Costo por trabajo(Bs.)</div>
	  		<div class="celda th" style='width: 10%'></div>
        </div>
        <?php for ($i=0; $i < count($producto_procesos) ; $i++) {$pp=$producto_procesos[$i]; 
        	?>
        	<div class="fila">
	        	<div class="celda td"><?php echo $pp->nombre.': '.$pp->sub_proceso;?></div>
	        	<?php $tiempo=explode(":",$this->validaciones->segundos_a_hms($pp->tiempo_estimado));?>  
		        <div class="celda td">
		        	<input type="number" min='0' max='1000' id="h<?php echo $pp->idppr;?>" placeholder='H' value='<?php echo $tiempo[0];?>' style='width:60px;'>
					<input type="number" min='0' max='59' id="m<?php echo $pp->idppr;?>" placeholder='m' value='<?php echo $tiempo[1];?>' style='width:45px;'>
					<input type="number" min='0' max='59' id="s<?php echo $pp->idppr;?>" placeholder='s' value='<?php echo $tiempo[2];?>' style='width:45px;'>
		        </div>  
		  		<div class="celda td"><input type="number" id="costo<?php echo $pp->idppr;?>" placeholder='0,00' value='<?php echo $pp->costo;?>'></div>
		  		<div class="celda td"><a href="javascript:" onclick="save_tiempo_costo('<?php echo $pp->idp;?>','<?php echo $pp->idppr;?>')" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-search"></i> Guadar </a></div>
	        </div>
        	<?php
        }?>
    </div>
    <?php }else{?>
    	<h2>No existen Procesos en el producto</h2>
    <?php }?>
</div>
