<?php 
	$res=$producto[0];
	$img_prod=base_url().'libraries/img/pieza_productos/miniatura/'; 
	$url1=base_url().'libraries/img/materiales/miniatura/'; 
	
	$materiales=count($producto_materiales);
?>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="config('<?php echo $res->idp; ?>')">Modificar</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="materiales('<?php echo $res->idp; ?>')">Materiales</a></li>
  <li role="presentation"><a href="javascript:" onclick="piezas('<?php echo $res->idp; ?>')">Piezas</a></li>
  <li role="presentation"><a href="javascript:" onclick="material_indirecto('<?php echo $res->idp; ?>')">Materiales indirectos</a></li>
  <li role="presentation"><a href="javascript:" onclick="procesos('<?php echo $res->idp; ?>')">Procesos</a></li>
  <li role="presentation"><a href="javascript:" onclick="tiempos_costos('<?php echo $res->idp; ?>')">Tiempo/Costo de Producción</a></li>
</ul>
<table class="tabla">
	<tr class="fila">
		<td class="celda th" width="20%" rowspan="2"><?php echo $res->nombre;?></td>
		<td class="celda th" colspan="4">MATERIALES</td>
		<th class="celda th" colspan="3"><a href="javascript:" onclick="view_form_add_material('<?php echo $res->idp;?>')">Adicional Material</a></th>
	</tr>
	<tr class="fila">
		
		<td class="celda th" width="8%"></td>
		<td class="celda th" width="30%">Nombre</td>
		<td class="celda th" width="10%">Cantidad</td>
		<td class="celda th" width="5%">Unid.</td>
		<td class="celda th" width="9%"></td>
		<td class="celda th" width="9%"></td>
		<td class="celda th" width="9%"></td>
	</tr>
<?php if($materiales>0){?>
	<tr class="fila">
		<td class="celda td" rowspan="<?php echo $materiales+1;?>" style='vertical-align: top;'>
		<?php
			$swap=$this->M_categoria_producto->get_portada_producto($res->idp);
			
			if(count($swap)>0){
				$im=$this->M_imagen_producto->get_pieza_material($swap[0]->idpim);
				$img="default.png";
				for ($i=0; $i < count($im) ; $i++) { $imagen=$im[$i];
					if($imagen->nombre!='' && $imagen->nombre!=NULL){ $img=$imagen->nombre;}
					?>
					<img src="<?php echo $img_prod.$img;?>" width="100%"><br>
					<?php
				}//end for
			}
		?>
		</td>
		
	</tr>
<?php }//end if?>
<?php
	for($i=0; $i < $materiales; $i++){ $material=$producto_materiales[$i];
		$img="default.png";
		if($material->fotografia!='' && $material->fotografia!=NULL){ $img=$material->fotografia;}
		$swap=$this->M_unidad->get_col($material->idu,'nombre');
		$unidad=$swap[0]->nombre;
	?>
	<tr class="fila">
		<td class="celda td"><img src="<?php echo $url1.$img;?>" width="100%"></td>
		<td class="celda td"><?php echo $material->nombre;?></td>
		<td class="celda td"><input type="number" id="can<?php echo $material->idpi;?>" value='<?php echo $material->cantidad;?>'></td>
		<td class="celda td"><?php echo $unidad;?></td>
		<td class="celda td"><a href="javascript:" onclick="save_cant_producto_material('<?php echo $material->idpi;?>','<?php echo $material->idp;?>')">Guardar</a></td>
		<td class="celda td"><a href="javascript:" onclick="update_producto_material('<?php echo $material->idpi;?>','<?php echo $material->idp;?>')">Modificar</a></td>
		<td class="celda td"><a href="javascript:" onclick="drop_producto_material('<?php echo $material->idpi;?>','<?php echo $material->idp;?>')">Eliminar</a></td>
	</tr>
	<?php
	}
?>
	<tr class="fila">
		<td class="celda td"></td>
		<td class="celda td" colspan="4"></td>
		<th class="celda th" colspan="3"><a href="javascript:" onclick="view_form_add_material('<?php echo $res->idp;?>')">Adicional Material</a></th>
	</tr>
</table>
<script type="text/javascript">button_modal('modal_ok_1','','','modal_print_1','','','modal_closed_1');cerrar_modal('content_modal','modal');</script>