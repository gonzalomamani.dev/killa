<?php 
	$res=$producto[0];
	$url_prod=base_url().'libraries/img/pieza_productos/'; 
	$url_pieza=base_url().'libraries/img/piezas/miniatura/';
	$url_material=base_url().'libraries/img/materiales/miniatura/';
	//$producto_pieza=count($piezas);
?>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="config('<?php echo $res->idp; ?>')">Modificar</a></li>
  <li role="presentation"><a href="javascript:" onclick="materiales('<?php echo $res->idp; ?>')">Materiales</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="piezas('<?php echo $res->idp; ?>')">Piezas</a></li>
  <li role="presentation"><a href="javascript:" onclick="material_indirecto('<?php echo $res->idp; ?>')">Materiales indirectos</a></li>
  <li role="presentation"><a href="javascript:" onclick="procesos('<?php echo $res->idp; ?>')">Procesos</a></li>
  <li role="presentation"><a href="javascript:" onclick="tiempos_costos('<?php echo $res->idp; ?>')">Tiempo/Costo de Producción</a></li>
</ul>
<table class="tabla">
	<tr class="fila">
	<?php $swap=$this->M_categoria_producto->get_portada_producto($res->idp);;
			$resto=3;
			if(count($swap)>0){
				$im=$this->M_imagen_producto->get_pieza_material($swap[0]->idpim);
				$img="default.png";
				$resto-=count($im);
				 for ($i=0; $i < count($im); $i++) {$img_producto=$im[$i];
				?>
				<td class="celda td" width="9%" rowspan="3"><img src="<?php echo $url_prod.$img_producto->nombre;?>" width='100%'></td>
				<?php
				}
			}
			for($j=0; $j < $resto; $j++){ 
				?>
				<td class="celda td" width="9%" rowspan="3"><img src="<?php echo $url_prod.'default.png';?>" width='100%'></td>
				<?php
			}
	?>
		<th class="celda th" width="73%"><h3><?php echo $res->nombre;?></h3></th>
		
	</tr>

	
	<tr class="fila"><th class="celda th"><br></th></tr>
	<tr class="fila"><th class="celda th"><br></th></tr>
</table>
<table class="tabla">
	<tr class="fila">
		<th class="celda th" colspan="6"><a href="javascript:" onclick="adicionar_grupo('<?php echo $res->idp; ?>')">Crear Grupo</a></th>
	</tr>
	<tr class="fila">
		<td class="celda th" colspan="3">DETALLE DE PIEZAS</td>
	</tr>
<?php 
if(count($grupos)>0){
	for ($i=0; $i < count($grupos) ; $i++) { $grupo=$grupos[$i];
	?>
	
	<tr class="fila">
		<td class="celda th" width="45%">Pieza(s)</td>
		<td class="celda th" width="49%">Material(es)</td> 
		<td class="celda th" width="6%">Color Prod.</td>
	</tr>
	<tr class="fila">
		<th class="celda th">
			<table class="tabla">
	<?php //SACANDO LAS PIEZAS DEL GRUPO
		$piezas=$this->M_pieza->get_pieza_categoria($grupo->idcp);
		for($pi=0; $pi < count($piezas); $pi++){$pieza=$piezas[$pi];
			$img="default.png";
			if($pieza->imagen!='' && $pieza->imagen!=NULL){ $img=$pieza->imagen;}
		?>
			<tr class="fila">
				<td class="celda td" width="15%"><img src="<?php echo $url_pieza.$img;?>" width='100%'></td>
				<td class="celda td" width="75%"><?php echo $pieza->nombre.' ('.$pieza->alto.' X '.$pieza->ancho.' cm.)';?></td>
				<td class="celda td" width="10%"><?php echo $pieza->unidades.' Unid.';?></td>
				<td class="celda td"><a href="javascript:" onclick="modificar_pieza('<?php echo $pieza->idpi;?>','<?php echo $grupo->idcp; ?>','<?php echo $res->idp;?>')">Modificar</a></td>
				<td class="celda td"><a href="javascript:" onclick="drop_producto_pieza('<?php echo $pieza->idpi;?>','<?php echo $res->idp;?>')">Eliminar</a></td>
			</tr>
		<?php
		}//end for piezas
		?>

			<tr class="fila"><td class="celda th" colspan="5"><a href="javascript:" onclick="view_form_new_pieza('<?php echo $res->idp; ?>','<?php echo $grupo->idcp; ?>')">Adicionar Pieza</a></td></tr>
		</table>
		</th>
		
		<th class="celda th">
			<table class="tabla">
		<?php //SACANDO LOS MATERIALES DEL GRUPO DE PIEZAS
			$materiales=$this->M_categoria_producto->get_categoria_grupo($grupo->idcp);
			for($ma=0; $ma < count($materiales); $ma++){$material=$materiales[$ma];
				$img="default.png";
				if($material->fotografia!='' && $material->fotografia!=NULL){ $img=$material->fotografia;}
				$swap_pi=$this->M_imagen_producto->get_pieza_material($material->idpim);//sacando datos del producto en el material
			?>
				<tr class="fila">
					<td class="celda td" width="10%"><img src="<?php echo $url_material.$img;?>" width='100%'></td>
					<td class="celda td" <?php if($grupo->color_producto==1){?>width="37%"<?php }else{?>width="70%"<?php } echo "width='35%'";?> ><?php echo $material->nombre;?></td>

				<?php if(true/*$grupo->color_producto==1*/){?>
				<?php
					$img_mat_prod="default.png";
					if(count($swap_pi)>0){ $img_mat_prod=$swap_pi[0]->nombre;}
					if($grupo->color_producto==1){
					?>
					<td class="celda td" width="10%"><img src="<?php echo $url_prod.$img_mat_prod;?>" width="100%"></td>
					<td class="celda td" width="12%"><a href="javascript:" onclick="add_imagen_pieza_material('<?php echo $res->idp;?>','<?php echo $material->idpim;?>')">Adicionar Imagenes</a></td>
					<?php }?>
					<td class="celda td" width="12%"><a href="javascript:" onclick="mod_producto_pieza_material('<?php echo $material->idpim;?>','<?php echo $grupo->idcp;?>','<?php echo $res->idp;?>')">Modificar Material</a></td>
					<td class="celda td" width="12%"><a href="javascript:" onclick="drop_producto_pieza_material('<?php echo $material->idpim;?>','<?php echo $res->idp;?>')">Eliminar Material</a></td>
					<?php if($grupo->color_producto==1){ ?>
					<th class="celda th" width="7%">
					<?php if($material->portada==0){?>
						<a href="javascript:" onclick="save_portada('<?php echo $res->idp;?>','<?php echo $material->idpim;?>','<?php echo $grupo->idcp;?>')">
							<img src="<?php echo base_url().'libraries/img/buttons/btn_of.png'?>" width='30px'>
						</a>
					<?php }else {?>
						<a href="javascript:" onclick="reset_portada('<?php echo $res->idp;?>','<?php echo $material->idpim;?>','<?php echo $grupo->idcp;?>')">
							<img src="<?php echo base_url().'libraries/img/buttons/btn_green.png'?>" width='30px'>
						</a>
					<?php }?>
					</th>
					<?php }?>
				<?php }// end if?>
				</tr>
			<?php
			}//end for piezas
			?>

				<tr class="fila"><td class="celda th" colspan="7"><a href="javascript:" onclick="add_producto_pieza_material('<?php echo $grupo->idcp;?>','<?php echo $res->idp;?>')">Adicionar Material</a></td></tr>
			</table>
		</th>
		<th class="celda th">
			<?php if($grupo->color_producto==0){?>
				<a href="javascript:" onclick="save_color_producto('<?php echo $grupo->idcp;?>','<?php echo $res->idp;?>')">
					<img src="<?php echo base_url().'libraries/img/buttons/btn_of.png'?>" width='30px'>
				</a>
			<?php }else {?>
				<a href="javascript:" onclick="reset_color_producto('<?php echo $grupo->idcp;?>','<?php echo $res->idp;?>')">
					<img src="<?php echo base_url().'libraries/img/buttons/btn_red.png'?>" width='30px'>
				</a>
			<?php }?>
		</th>
	</tr>
		<?php
	}// end for grupo
	?>
	
	<tr class="fila">
		<th class="celda th" colspan="6"><a href="javascript:" onclick="adicionar_grupo('<?php echo $res->idp; ?>')">Crear Grupo</a></th>
	</tr>
	<?php
} ?>
	
	
</table>
<script type="text/javascript">button_modal('modal_ok_1','','','modal_print_1','','','modal_closed_1');cerrar_modal('content_modal','modal');</script>