<?php 
	$res=$producto[0]; 
	$url1=base_url().'libraries/img/productos/miniatura/'; 
	$url2=base_url().'libraries/img/productos/'; 
	$limImg=3-count($imagenes);
?>
<ul class="nav nav-tabs">
  <li role="presentation" class="active"><a href="javascript:" onclick="config('<?php echo $res->idp; ?>')">Modificar</a></li>
  <li role="presentation"><a href="javascript:" onclick="materiales('<?php echo $res->idp; ?>')">Materiales</a></li>
  <li role="presentation"><a href="javascript:" onclick="piezas('<?php echo $res->idp; ?>')">Piezas</a></li>
  <li role="presentation"><a href="javascript:" onclick="material_indirecto('<?php echo $res->idp; ?>')">Materiales indirectos</a></li>
  <li role="presentation"><a href="javascript:" onclick="procesos('<?php echo $res->idp; ?>')">Procesos</a></li>
  <li role="presentation"><a href="javascript:" onclick="tiempos_costos('<?php echo $res->idp; ?>')">Tiempo/Costo de Producción</a></li>
</ul>
<table class="tabla">
<!--<tr class='fila'><th colspan="4"><H3>PRODUCTO: Modificar</H3></th></tr>-->
<!--<tr class='fila'>
	<th rowspan="6" width="40%"><div id="visor">
			<?php 
				if(count($imagenes)>0){
					for ($i=0; $i < count($imagenes) ; $i++) { ?>
					<div style="float:left; width:32%; margin:0 auto;"><a href="javascript:" onclick="drop_imagen('<?php echo $res->idp; ?>','<?php echo $imagenes[$i]->idim; ?>')">Eliminar</a><img src="<?php echo $url2.$imagenes[$i]->nombre; ?>" width="100%"></div>
				<?php	}
				}else{?>
					<img src="<?php echo $url2.'default.jpg'; ?>" width='33%'>
										
			<?php }?>
	</div></th>
	<th width="25%">Código</th>
	<td width="35%"><input type="text" id="cod" placeholder='Código de Producto' maxlength="20" value="<?php echo $res->cod;?>"></td>
</tr>
<tr class='fila'>
	<th class='celda th'>Nombre</th>
	<td class='celda td'><input type="text"id="nom" placeholder='Nombre de Producto' maxlength="90" value="<?php echo $res->nombre; ?>"></td>			
</tr>
<tr class='fila'>
	<th class='celda th'>Diseñador</th>
	<td class='celda td'><input type="text"id="dis" placeholder="K'illa" maxlength="90" value="<?php echo $res->disenador; ?>"></td>			
</tr>
 	<th class='celda th'>Fecha de Creación</th>
	<td class='celda td'><input type="date" id="fec" placeholder='2000-01-31' value="<?php echo $res->fecha_creacion;?>"> </td>
</tr>
<tr class='fila'>
	<th class='celda th'>Precio (Bs.)</th>
	<td class='celda td'><input type="number" id="cu" placeholder='0.00' value="<?php echo $res->c_u;?>"> </td>
</tr>
<tr class='fila'>
	<th class='celda th'>Peso(gramos)</th>
	<td class='celda td'><input type="number" id="peso" placeholder='0.00' value="<?php echo $res->peso;?>"> </td>
</tr>
</table>
<table border="1" cellpadding="0" cellspacing="0" width="100%">
<tr class='fila'>
	<th colspan="2">fotografia (disponible para <?php echo $limImg;?> archivos)</th>
	<td colspan="2"><input type="file" multiple="multiple" id="fot" <?php if($limImg<=0){ echo "hidden"; }?>></td>
	<td class='celda td'></td>
	<td class='celda td'></td>
</tr>-->
<tr class='fila'>
	<th class='celda th' width="13%">Código</th>
	<td class='celda td' width="20%"><input type="text" id="cod" placeholder='Código de Producto' maxlength="20" onclick="closed_alert(this)" value="<?php echo $res->cod;?>"></td>
	<th class='celda th' width="13%">Nombre</th>
	<td class='celda td' width="20%"><input type="text" id="nom" placeholder='Nombre de Producto' maxlength="90" value="<?php echo $res->nombre; ?>"></td>
	<th class='celda th' width="13%">Diseñador</th>
	<td class='celda td' width="20%"><input type="text"id="dis" placeholder="K'illa" maxlength="90" value="<?php echo $res->disenador; ?>"></td>
</tr>
<tr class='fila'>

	<th class='celda th'>Fecha de Creación</th>
	<td class='celda td' ><input type="date" id="fec" placeholder='2000-01-31' value="<?php echo date($res->fecha_creacion); ?>"></td>
	<th class='celda th'>Precio (Bs.)</th>
	<td class='celda td' ><input type="number" id="cu" placeholder='0.00' value="<?php echo $res->c_u;?>"> </td>
	<th class='celda th'>Peso(gramos)</th>
	<td class='celda td' ><input type="number" id="peso" placeholder='0.00' value="<?php echo $res->peso;?>"> </td>
</tr>
<tr class='fila'>
	<th class='celda th'>Etiqueta Logo:</th>
	<td class='celda td'><textarea placeholder='Descripción' id="etlo"><?php echo $res->etiquetaLogo;?></textarea></td>
	<th class='celda th'>Plaqueta Logo:</th>
	<td class='celda td'><textarea placeholder='Descripción' id="pllo"><?php echo $res->plaquetaLogo;?></textarea></td>
	<th class='celda th'>Correa:</th>
	<td class='celda td'><textarea placeholder='Descripción' id="corr"><?php echo $res->correa;?></textarea></td>
</tr>
<tr class='fila'>
	<th class='celda th'>Repujado Logo:</th>
	<td class='celda td'><textarea placeholder='Descripción' id="relo"><?php echo $res->repujadoLogo;?></textarea></td>
	<th class='celda th'>Etiqueta de cuero:</th>
	<td class='celda td'><textarea placeholder='Descripción' id="etcu"><?php echo $res->etiquetaCuero;?></textarea></td>
	<th class='celda th'>Repujado:</th>
	<td class='celda td'><textarea placeholder='Descripción' id="repu"><?php echo $res->repujado;?></textarea></td>
</tr>
<tr class='fila'>
	<th class='celda th'>Cierre Delante:</th>
	<td class='celda td'><textarea placeholder='con cierre delante' id="cidelante"><?php echo $res->cierreDelante;?></textarea></td>
	<th class='celda th'>Cierre Detras:</th>
	<td class='celda td'><textarea placeholder='con cierre detras' id="cidetras"><?php echo $res->cierreDetras;?></textarea></td>
	<th class='celda th'>Cierre Dentro:</th>
	<td class='celda td'><textarea placeholder='con cierre detras' id="cidentro"><?php echo $res->cierreDentro;?></textarea></td>
</tr>
<tr class='fila'>
	<th class='celda th'>Alto (cm.):</th>
	<td class='celda td'><input type="number" placeholder='0' id="alto" value="<?php echo $res->alto;?>"></td>
	<th class='celda th'>Ancho (cm.):</th>
	<td class='celda td'><input type="number" placeholder='0'  id="ancho" value="<?php echo $res->ancho;?>"></td>
	<th class='celda th' rowspan="2">Descripción</th>
	<td class='celda td' rowspan="2"><textarea  maxlength="900" rows="4" placeholder='Descripcion de Producto' id="des"><?php echo $res->descripcion;?></textarea></td>
</tr>
<tr class='fila'>
	<th class='celda th'>Largo (cm.):</th>
	<td class='celda td'><input type="number" placeholder='0' id="largo" value="<?php echo $res->largo;?>"></td>
	<th class='celda th'></th>
	<td class='celda td'></td>
</tr>
</table>
<script type="text/javascript">button_modal('modal_ok_1','modificar_producto',"'<?php echo $res->idp;?>'",'modal_print_1','','','modal_closed_1')</script>
<!--<script type="text/javascript">addClick_v2('modal_ok','modificar_producto',"'<?php echo $res->idp;?>'")</script>-->
