<?php
$imagen="";
$codigo="";
$producto="";
$idp="";
if(count($prod)>0){
	$idp=$prod[0]->idp;
	$codigo=$prod[0]->cod;
	$producto=$prod[0]->nombre;
	$s_img=$this->M_imagen_producto->get_col_prod($idp,'nombre');
	if(count($s_img)>0){ $imagen=$s_img[0]->nombre;}
}
if($imagen=='' || $imagen==NULL){
	$imagen='default.png';
}
$ruta='libraries/img/productos/miniatura/';
?>

<table width="100%" border="1" cellspacing="0" cellpadding="5px">
	<tr>
		<td colspan="3"><h3>Procesos</h3></td>
	</tr>
	<tr>
		<th width="10%">Código</th>
		<th width="20%"><?php echo $codigo;?></th>
		<td width="70%" rowspan="3">
			<table width="100%">
			<?php for ($i=0; $i < count($producto_procesos) ; $i++) { $pp=$producto_procesos[$i];
				?>
					<tr><td width="30%"><?php echo $pp->nombre; ?></td>
						<td width="60%"><?php echo $pp->sub_proceso;?></td>
						<td width="10%"><button onclick="drop_proceso('<?php echo $pp->idp;?>','<?php echo $pp->idppr;?>')">eliminar</button></td>
				</tr>
				<?php
			}?>
				<tr>
					<th></th>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<th>Nombre</th>
		<th><?php echo $producto;?></th>
	</tr>
	<tr>
		<th colspan="2"><img src="<?php echo $ruta.$imagen;?>" alt="<?php echo $ruta.$producto;?>" width='100%'></th>
	</tr>
	<tr>
		<th colspan="2"></th>
		<td>
			<table width="100%" border="1" cellpadding="7">
				<tr>
					<td width="15%">Proceso</td>
					<td width="45%">Descripción</td>
					<td width="10%"></td>
				</tr>

				<tr>
					<td> <select id="proc">
						<option value="">Seleccionar...</option>
						<?php for($i=0;$i<count($procesos);$i++){ $proceso=$procesos[$i]; ?>
							<option value="<?php echo $proceso->idpr;?>"><?php echo $proceso->nombre;?></option>
						<?php }?>
					</select> </td>
					<td><textarea id="desc" placeholder='Descripción'></textarea> </td>
					<td><button onclick="add_proceso('<?php echo $idp;?>')">Guardar</button></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3"><h3>Material Indirecto</h3></td>
	</tr>
	<tr>
		<th colspan="2"></th>
		<th>
			<table width="100%">
			<?php for ($i=0; $i < count($producto_materiales) ; $i++) { $pm=$producto_materiales[$i];
				?>
					<tr><td width="30%"><?php echo $pm->nombre; ?></td>
						<td width="60%"><?php echo $pm->costo.'Bs.';?></td>
						<td width="10%"><button onclick="drop_producto_insumo('<?php echo $pm->idp;?>','<?php echo $pm->idpm;?>')">eliminar</button></td>
				</tr>
				<?php
			}?>
				<tr>
					<th></th>
				</tr>
			</table>
		</th>
	</tr>
	<tr>
		<th colspan="2"></th>
		<td>
			<table width="100%" border="1" cellpadding="7">
				<tr>
					<td width="45%">Material</td>
					<td width="45%">Costo</td>
					<td width="10%"></td>
				</tr>

				<tr>
					<td>
					<select id="nom_mi">
						<option value="">Seleccionar...</option>
						<?php for($i=0;$i<count($materiales);$i++){ $material=$materiales[$i]; ?>
							<option value="<?php echo $material->idma;?>"><?php echo $material->nombre;?></option>
						<?php }?>
					</select> </td>
					<td><input type="number" placeholder='Costo por material indirecto' id="costo_mi"></td>
					<td><button onclick="add_material_indirecto('<?php echo $idp;?>')">Guardar</button></td>
				</tr>
			</table>
		</td>
	</tr>
</table>