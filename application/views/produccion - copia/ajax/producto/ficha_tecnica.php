<a href="javascript:void(0)" onclick="imprimir('area')">Imprimir</a>
<?php
	$producto=$productos[0];
	$insumo="";
	$imagen1="no.png";
	$imagen2="no.png";
	$imagen3="no.png";
	if(count($productos_insumos)>0)
		$insumo=$productos_insumos[0];
	if(count($imagenes)>0){ $imagen1=$imagenes[0]->nombre;}
	if(count($imagenes)>1){ $imagen2=$imagenes[1]->nombre;}
	if(count($imagenes)>2){ $imagen3=$imagenes[2]->nombre;}
	$ruta=base_url().'libraries/img/productos/';
?>
<div id="area">
	<div id="ficha_tecnica">
	<table width="100%" border="1" cellpadding="2px" cellspacing="0px;">
		<tr>
			<th colspan="4" width="60%"><h2><?php echo $producto->nombre;?></h2></th>
			<td colspan="2" rowspan="9" width="40%"><img src="<?php echo $ruta.$imagen1;?>" alt="" width='100%'></td>
		</tr>
		<tr>
			<th colspan="2">Código</th>
			<th colspan="2">Fecha de Creación</th>
		</tr>
		<tr>
			<td colspan="2"><?php echo $producto->cod;?></td>
			<td colspan="2"><?php echo $producto->fecha_creacion;?></td>
			
		</tr>
		<tr>
			<th colspan="4">Diseñador</th>
		</tr>
		<tr>
			<td colspan="4"><?php echo $producto->disenador;?></td>
		</tr>
		<tr>
			<th width="15%">Logo Etiqueta de Cuero</th>
			<th width="15%">Logo Plaqueta</th>
			<th width="15%">Logo Repujado</th>
			<th width="15%">Repujado Decorado</th>
		</tr>
		<tr>
			<td><?php echo $producto->etiquetaLogo;?></td>
			<td><?php echo $producto->plaquetaLogo;?></td>
			<td><?php echo $producto->repujadoLogo;?></td>
			<td><?php echo $producto->repujado;?></td>
		</tr>
		<tr>
			<th colspan="2">Correa</th>
			<th colspan="2">Cierre Dentro</th>
			
		</tr>
		<tr>
			<td colspan="2"><?php echo $producto->correa?></td>
			<td colspan="2"><?php echo $producto->cierreDentro;?></td>
		</tr>
		<tr>
			<th colspan="2">Cierre Delante</th>
			<th colspan="2">Cierre Detras</th>
			<?php 
			$medida="";
				if($producto->largo!=0 & $producto->largo!=null){
					$medida.='Largo '.$producto->largo.'cm. ';
				}
				if($producto->alto!=0 & $producto->alto!=null){
					$medida.=' x Alto '.$producto->alto.'cm. ';
				}
				if($producto->ancho!=0 & $producto->ancho!=null){
					$medida.='x Ancho '.$producto->ancho.'cm. ';
				}
			?>
			<th rowspan="2" width="20%">Medidas</th>
			<th rowspan="2" width="20%"><?php echo $medida;?></th>
		</tr>
		<tr>
			<td colspan="2"><?php echo $producto->cierreDelante;?></td>
			<td colspan="2"><?php echo $producto->cierreDetras;?></td>
		</tr>
		<tr>
			<th colspan="4">Descripcion</th>
			<th>Costo:</th><th><?php echo $producto->c_u.' Bs.';?></th>
			
		</tr>
		<tr>
		<th colspan="4"><?php echo $producto->descripcion;?>&nbsp;</th>
			<th><img src="<?php echo $ruta.$imagen2;?>" alt="" width='100%'> </th>
			<th><img src="<?php echo $ruta.$imagen2;?>" alt="" width='100%'> </th>
		</tr>
		<tr>
			<th colspan="6">Material Necesario</th>
		</tr>
		<tr>
			<th colspan="4">Material</th>
			<th colspan="2">Cantidad</th>
		</tr>
		<?php for ($i=0; $i < count($productos_insumos); $i++) {$insumo=$productos_insumos[$i];
			$unidad=$this->M_unidad->get_col($insumo->idu,'abreviatura');
			?>
			<tr>
				<td colspan="4"><?php echo $insumo->nombre;?></td>
				<td colspan="2"><?php echo $insumo->cantidad.' '.$unidad[0]->abreviatura;?></td>
			</tr>
			<?php
		}?>
		
	</table>
	</div>
</div>
