<?php 
	$res=$producto[0]; 
	$url1=base_url().'libraries/img/productos/miniatura/'; 
	$url2=base_url().'libraries/img/productos/'; 
	$limImg=3-count($imagen);
?>

<table border="1" cellpadding="0" cellspacing="0" width="100%">
<tr><th colspan="4"><H3>PRODUCTO: Modificar</H3></th></tr>
<tr>
	<th rowspan="6" width="40%"><div id="visor">
			<?php 
				if(count($imagen)>0){
					for ($i=0; $i < count($imagen) ; $i++) { ?>
					<div style="float:left; width:32%; margin:0 auto;"><a href="javascript:void(0)" onclick="drop_imagen('<?php echo $res->idp; ?>','<?php echo $imagen[$i]->idim; ?>')">Eliminar</a><br><img src="<?php echo $url2.$imagen[$i]->nombre; ?>" width="100%"></div>
				<?php	}
				}else{?>
					<img src="<?php echo $url2.'default.jpg'; ?>" width='33%'>
										
			<?php }?>
	</div></th>
	<th width="25%">Código</th>
	<td width="35%"><input type="text" id="cod" placeholder='Código de Producto' maxlength="20" value="<?php echo $res->cod;?>"></td>
</tr>
<tr>
	<th>Nombre</th>
	<td><input type="text"id="nom" placeholder='Nombre de Producto' maxlength="90" value="<?php echo $res->nombre; ?>"></td>			
</tr>
<tr>
	<th>Diseñador</th>
	<td><input type="text"id="dis" placeholder="K'illa" maxlength="90" value="<?php echo $res->disenador; ?>"></td>			
</tr>
 	<th>Fecha de Creación</th>
	<td><input type="date" id="fec" placeholder='2000-01-31' value="<?php echo $res->fecha_creacion;?>"> </td>
</tr>
<tr>
	<th>Precio (Bs.)</th>
	<td><input type="number" id="cu" placeholder='0.00' value="<?php echo $res->c_u;?>"> </td>
</tr>
<tr>
	<th>Peso(gramos)</th>
	<td><input type="number" id="peso" placeholder='0.00' value="<?php echo $res->peso;?>"> </td>
</tr>
</table>
<table border="1" cellpadding="0" cellspacing="0" width="100%">
<tr>
	<th colspan="2">fotografia (disponible para <?php echo $limImg;?> archivos)</th>
	<td colspan="2"><input type="file" multiple="multiple" id="fot" <?php if($limImg<=0){ echo "hidden"; }?>></td>
	<td></td>
	<td></td>
</tr>
<tr>
	<th>Etiqueta Logo:</th>
	<td><textarea placeholder='Descripción' id="etlo"><?php echo $res->etiquetaLogo;?></textarea></td>
	<th>Plaqueta Logo:</th>
	<td><textarea placeholder='Descripción' id="pllo"><?php echo $res->plaquetaLogo;?></textarea></td>
	<th>Correa:</th>
	<td><textarea placeholder='Descripción' id="corr"><?php echo $res->correa;?></textarea></td>
</tr>
<tr>
	<th>Repujado Logo:</th>
	<td><textarea placeholder='Descripción' id="relo"><?php echo $res->repujadoLogo;?></textarea></td>
	<th>Etiqueta de cuero:</th>
	<td><textarea placeholder='Descripción' id="etcu"><?php echo $res->etiquetaCuero;?></textarea></td>
	<th>Repujado:</th>
	<td><textarea placeholder='Descripción' id="repu"><?php echo $res->repujado;?></textarea></td>
</tr>
<tr>
	<th>Cierre Delante:</th>
	<td><textarea placeholder='con cierre delante' id="cidelante"><?php echo $res->cierreDelante;?></textarea></td>
	<th>Cierre Detras:</th>
	<td><textarea placeholder='con cierre detras' id="cidetras"><?php echo $res->cierreDetras;?></textarea></td>
	<th>Cierre Dentro:</th>
	<td><textarea placeholder='con cierre detras' id="cidentro"><?php echo $res->cierreDentro;?></textarea></td>
</tr>
<tr>
	<th>Alto (cm.):</th>
	<td><input type="number" placeholder='0' id="alto" value="<?php echo $res->alto;?>"></td>
	<th>Ancho (cm.):</th>
	<td><input type="number" placeholder='0'  id="ancho" value="<?php echo $res->ancho;?>"></td>
	<th rowspan="2">Descripción</th>
	<td rowspan="2"><textarea  maxlength="900" rows="4" placeholder='Descripcion de Producto' id="des"><?php echo $res->descripcion;?></textarea></td>
</tr>
<tr>
	<th>Largo (cm.):</th>
	<td><input type="number" placeholder='0' id="largo" value="<?php echo $res->largo;?>"></td>
	<th></th>
	<td></td>
</tr>
</table>
<table border="1" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<th width="50%" colspan="3">MATERIALES</th>
		<th width="50%" colspan="3">PIEZAS</th>
	</tr>
	<tr>
		<th rowspan="2" width="30%">Material Utilizado</th>
		<th colspan="2">Material Por Producto</th>
		<th rowspan="2">-</th>
		<th colspan="2">-</th>
	</tr>
	<tr>
		<th width="10%">Cantidad</th>
		<th width="10%">Unidad de Medida</th>
		<th>-</th>
		<th>-</th>
	</tr>
	
	<tr>
					<!--<div>
				<div id='d0'><a href="javascript:void(0)" onclick="eliminar_div_material(this)">eli</a></div>	
				<div id="d1">zxfsd fsdf</div>
				<div id="d2"><input type="number" value="12"></div>
				<div id="d3">lojas</div>
			</div>-->
		<td colspan="3" id="producto_material"><?php 
		 	for ($i=0; $i <count($producto_insumo) ; $i++) {$pi=$producto_insumo[$i]; $unidad=$this->M_unidad->get_col($pi->idu,'nombre');
		 		?><div><div id='d0'><a href="javascript:void(0)" onclick="eliminar_div_material(this)">eli</a></div><div id="d1"><?php echo $pi->nombre;?></div><div id="d2"><input type="number" value="<?php echo $pi->cantidad;?>"><input type="text" value="<?php echo $pi->idi;?>" hidden></div><div id="d3"><?php echo $unidad[0]->nombre;?></div></div><?php
		 	}
		?></td>
		<td colspan="3">-</td>
	</tr>
	<tr>
		<th colspan="3"><a href="javascript:void(0)" onclick="view_form_add_material()">Add Material</a></th>
	</tr>
</table>
<script type="text/javascript">addClick_v2('modal_ok','modificar_producto',"'<?php echo $res->idp;?>','<?php echo count($imagen);?>'")</script>