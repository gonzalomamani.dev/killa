
<?php
	$producto=$productos[0];
	$ruta=base_url().'libraries/img/pieza_productos/';
	$insumo="";
	$imagen1="no.png";
	$imagen2="no.png";
	$imagen3="no.png";

	$swap=$this->M_categoria_producto->get_portada_producto($producto->idp);
	if(count($swap)>0){
		$im=$this->M_imagen_producto->get_pieza_material($swap[0]->idpim);
		for($i=0; $i<count($im); $i++){$img_producto=$im[$i];
			switch ($i) {
				case 0: $imagen1=$img_producto->nombre; break;
				case 1: $imagen2=$img_producto->nombre; break;
				case 2: $imagen3=$img_producto->nombre; break;
			}
		}
	}
	$categoria_pieza=$this->M_categoria_pieza->get_categoria_pieza($producto->idp);
	
?>
<ul class="nav nav-tabs">
  <li role="presentation" class="active"><a href="javascript:" onclick="reportes('<?php echo $producto->idp; ?>')">Ficha Técnica</a></li>
  <li role="presentation"><a href="javascript:" onclick="costo_total_produccion('<?php echo $producto->idp; ?>')">Costo Total de Producción</a></li>
</ul>
<div id="area_imprimir">
	<div id="ficha_tecnica">
	<table class="tabla">
		<tr class='fila'>
			<th class='celda th' colspan="4" width="60%"><h2><?php echo $producto->nombre;?></h2></th>
			<td class='celda td' colspan="2" rowspan="9" width="40%"><img src="<?php echo $ruta.$imagen1;?>" alt="" width='100%'></td>
		</tr>
		<tr class='fila'>
			<th class='celda th' colspan="2">Código</th>
			<th class='celda th' colspan="2">Fecha de Creación</th>
		</tr>
		<tr class='fila'>
			<td class='celda td' colspan="2"><?php echo $producto->cod;?></td>
			<td class='celda td' colspan="2"><?php echo $producto->fecha_creacion;?></td>
			
		</tr>
		<tr class='fila'>
			<th class='celda th' colspan="4">Diseñador</th>
		</tr>
		<tr class='fila'>
			<td class='celda td' colspan="4"><?php echo $producto->disenador;?></td>
		</tr>
		<tr class='fila'>
			<th class='celda th' width="15%">Logo Etiqueta de Cuero</th>
			<th class='celda th' width="15%">Logo Plaqueta</th>
			<th class='celda th' width="15%">Logo Repujado</th>
			<th class='celda th' width="15%">Repujado Decorado</th>
		</tr>
		<tr class='fila'>
			<td class="celda td"><?php echo $producto->etiquetaLogo;?></td>
			<td class="celda td"><?php echo $producto->plaquetaLogo;?></td>
			<td class="celda td"><?php echo $producto->repujadoLogo;?></td>
			<td class="celda td"><?php echo $producto->repujado;?></td>
		</tr>
		<tr class='fila'>
			<th class='celda th' colspan="2">Correa</th>
			<th class='celda th' colspan="2">Cierre Dentro</th>
			
		</tr>
		<tr class='fila'>
			<td class='celda td' colspan="2"><?php echo $producto->correa?></td>
			<td class='celda td' colspan="2"><?php echo $producto->cierreDentro;?></td>
		</tr>
		<tr class='fila'>
			<th class='celda th' colspan="2">Cierre Delante</th>
			<th class='celda th' colspan="2">Cierre Detras</th>
			<?php 
			$medida="";
				if($producto->largo!=0 & $producto->largo!=null){
					$medida.='Largo '.$producto->largo.'cm. ';
				}
				if($producto->alto!=0 & $producto->alto!=null){
					$medida.=' x Alto '.$producto->alto.'cm. ';
				}
				if($producto->ancho!=0 & $producto->ancho!=null){
					$medida.='x Ancho '.$producto->ancho.'cm. ';
				}
			?>
			<th class='celda th' rowspan="2" width="20%">Medidas</th>
			<td class='celda td' rowspan="2" width="20%"><?php echo $medida;?></td>
		</tr>
		<tr class='fila'>
			<td class='celda td' colspan="2"><?php echo $producto->cierreDelante;?></td>
			<td class='celda td' colspan="2"><?php echo $producto->cierreDetras;?></td>
		</tr>
		<tr class='fila'>
			<th class="celda th" colspan="2">Colores Disponibles</th>
			<th class="celda th" colspan="2">Costo de Producto</th>
			<th class="celda th" rowspan="4"><img src="<?php echo $ruta.$imagen2;?>" alt="" width='100%'> </th>
			<th class="celda th" rowspan="4"><img src="<?php echo $ruta.$imagen3;?>" alt="" width='100%'> </th>
		</tr>
		<tr class="fila">
			<td class="celda td" colspan="2">
				<?php $ca_pr=$this->M_categoria_producto->get_categoria_producto_colores($producto->idp);
					for ($i=0; $i < count($ca_pr) ; $i++) {
						$mat=$this->M_material->get($ca_pr[$i]->idma);
						$color=$this->M_color->get($mat[0]->idco); 
					?>
					<div class="g-caja-color">
						<span class="g-color" style="background: <?php echo $color[0]->codigo?>">&#160;&#160;&#160;&#160;</span>
						<span class="g-text-color"><?php echo $color[0]->nombre;?></span>
					</div>
						<?php
					}
				?>

			</td>
			<td class="celda td" colspan="2"><?php if($producto->c_u>0){echo $producto->c_u.' Bs.';}?></td>
		</tr>
		<tr class="fila">
			<th class="celda th" colspan="4">Descripción</th></tr>
		<tr class='fila'>
			<td class='celda td' colspan="4"><?php echo $producto->descripcion;?></td>
		</tr>
		<tr class='fila'>
			<th class='celda th' colspan="6">Material Necesario</th>
		</tr>
		<tr class="fila">
			<td class="celda td" colspan="3">
				<table class="tabla">
					<tr class="fila">
						<th class='celda th'>Accesorios</th>
						<th class='celda th'>Cantidad</th>
					</tr>
					<?php for ($i=0; $i < count($producto_materiales); $i++) {$insumo=$producto_materiales[$i];
						$unidad=$this->M_unidad->get_col($insumo->idu,'abreviatura');
					?>
					<tr class="fila">
						<td class='celda td'><?php echo $insumo->nombre;?></td>
						<td class='celda td'><?php echo number_format($insumo->cantidad,2,'.',',').' '.$unidad[0]->abreviatura;?></td>
					</tr>
					<?php
					}?>
				</table>
			</td>
			<td class="celda td" colspan="3">
				<table class="tabla">
					<tr class="fila">
						<th class='celda th'>Cueros, Telas y Carton</th>
			
						<th class='celda th'>Cantidad Requerida</th>
					</tr>
					<?php for ($i=0; $i < count($categoria_pieza); $i++) {$categoria=$categoria_pieza[$i];
							if($categoria->color_producto!=1){
								$material=$this->M_categoria_producto->get_categoria_grupo($categoria->idcp);
								$unidad=$this->M_unidad->get($material[0]->idu);
								$piezas=$this->M_pieza->get_row('idcp', $categoria->idcp);
								$area=0; 
								for($pi=0; $pi<count($piezas); $pi++){ $area+=(($piezas[$pi]->alto*$piezas[$pi]->ancho)*$piezas[$pi]->unidades);}
								$cuero_necesario=$area/$unidad[0]->equivalencia;
					?>
							<tr class="fila">
								<td class='celda td'><?php echo $material[0]->nombre;?></td>
								<td class='celda td'><?php echo number_format($cuero_necesario,4,'.',',').' '.$unidad[0]->abreviatura;?></td>
							</tr>
					<?php
							}//end if
						}//end for?>

					<tr class="fila">
						<th class='celda th'>Cueros, Telas y Carton (Segun Color de Producto)</th>
			
						<th class='celda th'>Cantidad Requerida</th>
					</tr>
					<?php for ($i=0; $i < count($categoria_pieza); $i++) { $categoria=$categoria_pieza[$i];
							if($categoria->color_producto!=0){
								$material=$this->M_categoria_producto->get_categoria_grupo($categoria->idcp);
								for ($ma=0; $ma < count($material) ; $ma++) { 
									$unidad=$this->M_unidad->get($material[$ma]->idu);
									$piezas=$this->M_pieza->get_row('idcp', $categoria->idcp);
									$area=0; 
									for($pi=0; $pi<count($piezas); $pi++){ $area+=(($piezas[$pi]->alto*$piezas[$pi]->ancho)*$piezas[$pi]->unidades);}
									$cuero_necesario=$area/$unidad[0]->equivalencia;
									//color
									$color=$this->M_color->get($material[$ma]->idco);
								?>
									<tr class="fila">
										<td class='celda td'><?php echo $material[$ma]->nombre.' - '.$color[0]->nombre;?></td>
										<td class='celda td'><?php echo number_format($cuero_necesario,4,'.',',').' '.$unidad[0]->abreviatura;?></td>
									</tr>
								<?php
								}//end for material	
					?>
					<?php
							}//end if $categoria->color_producto!=0
						}//end for categoria_pieza?>
				</table>

			</td>
		</tr>
		
	</table>
	</div>
</div>
