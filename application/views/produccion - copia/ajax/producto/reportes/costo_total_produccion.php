<?php $res=$producto[0]; 
$ruta=base_url().'libraries/img/pieza_productos/';
$prod=$producto[0];
?>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="reportes('<?php echo $res->idp; ?>')">Ficha Técnica</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="costo_total_produccion('<?php echo $res->idp; ?>')">Costo Total de Producción</a></li>
</ul>
<div class="detalle" id="area">
<div id="ficha_tecnica">
<h3>Detalle de Productos</h3>
<table width="100%" border="1" cellspacing="0">
	<tr>
		<th width="40%" rowspan="3"><div id="visor">
			<?php 
			$swap=$this->M_categoria_producto->get_portada_producto($res->idp);
			$resto=3;
			$img="no.png";
			if(count($swap)>0){
				$im=$this->M_imagen_producto->get_pieza_material($swap[0]->idpim);
				$resto-=count($im);
				for ($i=0; $i < count($im) ; $i++) { $imagen=$im[$i];?>
					<div style="float:left; width:33.33%; margin:0 auto;">
						<img src="<?php echo $ruta.$imagen->nombre; ?>" width="100%">
					</div>
				<?php	}// end for
			}//end if
			for ($i=0; $i < $resto ; $i++) {?>
					<div style="float:left; width:33.33%; margin:0 auto;">
						<img src="<?php echo $ruta.$img; ?>" width="100%">
					</div>
			<?php	}// end for
			?>
		</div></th>
		<th width="20%" >Codigo de Producto</th>
		<td width="40%" colspan="2"><?php echo $prod->cod; ?></td>
	</tr>
	<tr>
		<th>Nombre de Producto</th>
		<td colspan="2" ><?php echo $prod->nombre; ?></td>
	</tr>
	<tr>
		<th>Descripcion de Producto</th>
		<td colspan="2" ><?php echo $prod->descripcion; ?></td>
	</tr>

	<tr>
	<td colspan="4">
		<table width="100%" border="1" cellpadding="5" cellspacing="0">
			<tr><th rowspan="2" width="50%">Material Necesario</th>
			<th colspan="2">Material por producto</th>
			<th width='20%' rowspan="2">Costo por Material (Bs.)</th></tr>
			<tr><th width="15%">Cantidad</th>
			<th width="15%">Unid. de Med.</th></tr>
			<?php
			$sumMat=0;
			for($i=0; $i < count($producto_insumo) ; $i++) {$pi=$producto_insumo[$i]; $unidad=$this->M_unidad->get_col($pi->idu,'nombre');
				?>
				<tr>
					<td><?php echo $pi->nombre;?></td>
					<td><?php echo $pi->cantidad;?></td>
					<td><?php echo $unidad[0]->nombre;?></td>
					<td><?php if(($pi->c_u*$pi->cantidad)>0){echo number_format(($pi->c_u*$pi->cantidad),2,'.',',');}?></td>
				</tr>
				<?php
				$sumMat+=($pi->c_u*$pi->cantidad);
			}
		?>
		<tr>
			<th colspan="3">Total en boliviados (Bs.)</th>
			<th><?php echo number_format($sumMat,2,',','.');?></th>
		</tr>
		</table>
	</td>
		
	</tr>
	<tr>
		<td colspan="4">
		<table width="100%" border="1" cellpadding="5" cellspacing="0">
			<tr>
				<th width="60%">Proceso de producción</th>
				<th width="20%">Tiempo de Producción</th>
				<th width='20%'>Costo de Producción (Bs.)</th>
			</tr>
			<?php
			$sumHora=0;
			$sumBsPro=0;
			for($i=0;$i<count($producto_procesos);$i++){$pp=$producto_procesos[$i];
				$tiempo=explode(":", $this->validaciones->segundos_a_hms($pp->tiempo_estimado));
				$hora="";
				if($tiempo[0]!=0 & $tiempo[0]!=''){$hora.=$tiempo[0].'h ';}
				if($tiempo[1]!=0 & $tiempo[1]!=''){$hora.=$tiempo[1].'m ';}
				if($tiempo[2]!=0 & $tiempo[2]!=''){$hora.=$tiempo[2].'s';}
				?>
					<tr>
						<td><?php echo $pp->nombre.': '.$pp->sub_proceso;?></td>
						<td><?php echo $hora; $sumHora+=$pp->tiempo_estimado;?></td>
						<td><?php if($pp->costo>0){echo number_format($pp->costo,2,',','.');} $sumBsPro+=$pp->costo;?></td>
					</tr>
				<?php
			}
			?>
		<tr>
			<th>TOTALES</th>
			<?php $tiempo=explode(":", $this->validaciones->segundos_a_hms($sumHora));
				$sumHora="";
				if($tiempo[0]!=0 & $tiempo[0]!=''){$sumHora.=$tiempo[0].'h ';}
				if($tiempo[1]!=0 & $tiempo[1]!=''){$sumHora.=$tiempo[1].'m ';}
				if($tiempo[2]!=0 & $tiempo[2]!=''){$sumHora.=$tiempo[2].'s';}	
			?>
			<th><?php echo $sumHora;?></th>
			<th><?php if($sumBsPro>0){echo number_format($sumBsPro,2,'.',',');}?></th>
		</tr>
		</table>
		</td>
	</tr>
	
	<tr>
		<td colspan="4">
		<table width="100%" border="1" cellpadding="5" cellspacing="0">
			<tr>
				<th width="80%">Material Indirecto</th>
				<th width='20%'>Costo de Producción (Bs.)</th>
			</tr>
			<?php
			$sumBsMat=0;
			for($i=0;$i<count($producto_materiales);$i++){$pm=$producto_materiales[$i];
			?>
				<tr>
					<td><?php echo $pm->nombre;?></td>
					<td><?php if($pm->costo>0){echo number_format($pm->costo,2,',','.');} $sumBsMat+=$pm->costo;?></td>
				</tr>
			<?php
			}
			?>
		<tr>
			<th>TOTALES</th>
			<th><?php if($sumBsMat>0){echo number_format($sumBsMat,2,'.',',');}?></th>
		</tr>
		</table>
		</td>
	</tr>
	<tr>
		<th colspan="2">TOTAL COSTO DE PRODUCCIÓN (Bs.)</th>
		<th ><?php echo number_format(($sumMat+$sumBsPro+$sumBsMat),2,'.',',');?></th>
	</tr>
</table>
</div>
</div>

