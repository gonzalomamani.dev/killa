<?php if(count($producto)>0){ $url=base_url().'libraries/img/pieza_productos/miniatura/'; ?>
	<table class="tabla">
	<tr class="fila">
		<th class="celda th" width="7%"></th>
		<th class="celda th" width="12%">Código</th>
		
		<th class="celda th" width="22%">Nombre</th>
		<th class="celda th" width="16%">Fecha de Creación</th>
		<th class="celda th" width="8%">Diseñador</th>
		<th class="celda th" width="11%">Colores Disponibles</th>
		<th class="celda th" width="8%">Reportes</th>
		<th class="celda th" width="8%">Configuración</th>
		<th class="celda th" width="8%">Eliminar</th>
		<!--<th class="celda th" rowspan="2" width="10%"><a href="javascript:void(0)" onclick="view_search('result')">Buscar</a></th>
		<th class="celda th" rowspan="2" width="10%"><a href="javascript:void(0)" onclick="view_all('result');">Ver Todo</a></th>
		<th class="celda th" rowspan="2" width="8.7%"><a href="javascript:void(0)" onclick="cargar('home/mod','content','home/all_mod','result')">Modificar</a></th>-->
	</tr>
	<?php for($i=0; $i < count($producto); $i++){ $res=$producto[$i]; 
			$img="default.png";
			$swap=$this->M_categoria_producto->get_portada_producto($res->idp);
			if(count($swap)>0){$im=$this->M_imagen_producto->get_pieza_material($swap[0]->idpim); if(count($im)>0){$img=$im[0]->nombre;}}
		?> 
	<tr class="fila">
		<td class="celda td"><div id="miniatura"><div id="item"><?php echo $i+1;?></div><img src="<?php echo $url.$img;?>" width='100%'></div></td>
		<td class="celda td"><?php echo $res->cod; ?></td>
		<td class="celda td"><?php echo $res->nombre; ?></td>
		<td class="celda td"><?php echo $this->validaciones->formato_fecha($res->fecha_creacion,'d-m-Y'); ?></td>
		<td class="celda td"><?php echo $res->disenador; ?></td>
		<td class="celda td">
			<?php $cp=$this->M_categoria_producto->get_categoria_producto_colores($res->idp); 
			?>
			<ul class="list-group" style="font-size:10px; padding: 1px">
			<?php for ($co=0; $co < count($cp); $co++) { $ca_pr=$cp[$co]; 
				$mat=$this->M_material->get($ca_pr->idma);
				$color=$this->M_color->get($mat[0]->idco);
				?>
					<li class="list-group-item" style="padding-top: 5px;padding-bottom: 5px;color:white; background:<?php echo $color[0]->codigo;?>"><?php echo $color[0]->nombre;?></li>
				<?php
			}?>
			</ul>
		</td>
		<td class="celda td"><a href="javascript:void(0);" onclick="reportes('<?php echo $res->idp; ?>')">Reportes</a></td>
		<td class="celda td"><a href="javascript:" onclick="config('<?php echo $res->idp; ?>')">Configuración</a></td>
		<td class="celda td"><a href="javascript:void(0);" onclick="view_form_eliminar_producto('<?php echo $res->idp; ?>','<?php echo $res->nombre; ?>','<?php echo $img ?>')">Eliminar</a></td>
	</tr>
	<?php } ?>
	</table>
<?php }else{ ?>
	<h2>No existe Registros</h2>
<?php }?>
<script type="text/javascript">cerrar_modal('content_modal','modal');//cerramos el modal cada ves que se ejecute la vista</script>