
<div>
<?php if(count($producto_procesos)>0){?>
	<div class="tabla">  
  
        <div class="t_titulo">Tiempo y costo de producción</div>  
        <div class="fila">
        	<div class="celda th" style='width: 55%'>Proceso</div>  
	        <div class="celda th" style='width: 25%'>Tiempo</div>  
	  		<div class="celda th" style='width: 10%'>Costo por trabajo(Bs.)</div>
	  		<div class="celda th" style='width: 10%'></div>
        </div>
        <?php for ($i=0; $i < count($producto_procesos) ; $i++) {$pp=$producto_procesos[$i]; 
        	?>
        	<div class="fila">
	        	<div class="celda td"><?php echo $pp->nombre.': '.$pp->sub_proceso;?></div>
	        	<?php $tiempo=explode(":",$this->validaciones->segundos_a_hms($pp->tiempo_estimado));?>  
		        <div class="celda td">
		        	<input type="number" min='0' max='1000' id="h<?php echo $pp->idppr;?>" placeholder='H' value='<?php echo $tiempo[0];?>' style='width:60px;'>
					<input type="number" min='0' max='59' id="m<?php echo $pp->idppr;?>" placeholder='m' value='<?php echo $tiempo[1];?>' style='width:45px;'>
					<input type="number" min='0' max='59' id="s<?php echo $pp->idppr;?>" placeholder='s' value='<?php echo $tiempo[2];?>' style='width:45px;'>
		        </div>  
		  		<div class="celda td"><input type="number" id="costo<?php echo $pp->idppr;?>" placeholder='0,00' value='<?php echo $pp->costo;?>'></div>
		  		<div class="celda td"><a href="javascript:" onclick="save_tiempo_costo('<?php echo $pp->idp;?>','<?php echo $pp->idppr;?>')" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-search"></i> Guadar </a></div>
	        </div>
        	<?php
        }?>
    </div>
    <?php }else{?>
    	<h2>No existen Procesos en el producto</h2>
    <?php }?>
</div>
