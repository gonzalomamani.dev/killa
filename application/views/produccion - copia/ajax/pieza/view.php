
<?php if(count($piezas)>0){ 
	$url=base_url().'libraries/img/piezas/miniatura/'; ?>
	<table class="tabla">
	<tr class="fila">
		<th class="celda th" width="10%"></th>
		<th class="celda th" width="10%">Codigo</th>
		<th class="celda th" width="20%">Nombre</th>
		<th class="celda th" width="10%">Grupo</th>
		<th class="celda th" width="10%">Alto (cm.)</th>
		<th class="celda th" width="10%">Ancho (cm.)</th>
		<th class="celda th" width="10%">Detalle</th>
		<th class="celda th" width="10%">Modificar</th>
		<th class="celda th" width="10%">Eliminar</th>
	</tr>
	<?php for($i=0; $i < count($piezas); $i++){ 
			$res=$piezas[$i]; 
			if($res->imagen==NULL || $res->imagen==""){
				$img="default.png";
			}else{
				$img=$res->imagen;
			}
		?> 
	<tr class="fila">
		<td class="celda td"><div id="miniatura"><div id="item"><?php echo $i+1;?></div><img src="<?php echo $url.$img; ?>" width='99%'></div></td>
		<td class="celda td"><?php echo $res->codigo; ?></td>
		<td class="celda td"><?php echo $res->nombre; ?></td>
		<td class="celda td"><?php echo $res->nombre_grupo; ?></td>
		<td class="celda td"><?php echo $res->alto; ?></td>
		<td class="celda td"><?php echo $res->ancho; ?></td>
		<td class="celda td"><a href="javascript:" onclick="view_form_modificar_pieza('<?php echo $res->idpi; ?>')">Detalle</a></td>
		<td class="celda td"><a href="javascript:" onclick="view_form_modificar_pieza('<?php echo $res->idpi;?>')">Modificar</a></td>
		<td class="celda td"><a href="javascript:" onclick="view_form_eliminar_pieza('<?php echo $res->idpi; ?>','<?php echo $res->nombre; ?>','<?php echo $img ?>')">Eliminar</a></td>
	</tr>
	<?php } ?>
	</table>
<?php }else{ ?>
	<h2>No existe Registros</h2>
<?php }?>
<script type="text/javascript">cerrar_modal('content_modal','modal');//cerramos el modal cada ves que se ejecute la vista</script>