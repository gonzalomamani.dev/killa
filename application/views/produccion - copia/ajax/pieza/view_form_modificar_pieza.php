<?php $res=$pieza[0];
	$ruta=base_url().'libraries/img/piezas/';
	$img='default.png';
	if($res->imagen!=NULL & $res->imagen!=''){$img=$res->imagen;}
?>
<h2>PIEZA: Modificar</h2>
<table border="0" cellpadding="3" width="100%">
	<tr>
		<th rowspan="6" width="40%"><img src="<?php echo $ruta.$img;?>" width="90%"></th>
	</tr>
	<tr><th width="15%">Fotografía</th><td colspan="3"><input type="file" id="fot"></td></tr>
	<tr><th>Nombre</th><td colspan="3"><input type="text" id="nom" placeholder='Nombre de Pieza' value="<?php echo $res->nombre;?>"></td></tr>
	<tr><th width="15%">Código</th><td width="15%"><input type="text" id="cod" value="<?php echo $res->codigo;?>" placeholder='Código de Pieza'></td>
	<th width="15%">Grupo</th>
	<td width="15%">
		<select id="gru">
			<option value="">Seleccione...</option>
		<?php
		for ($i=0; $i<count($pieza_grupos);$i++){ $g=$pieza_grupos[$i]; 
			?>
			<option value="<?php echo $g->idpig;?>" <?php if($res->idpig==$g->idpig){echo "selected";}?>><?php echo $g->nombre;?></option>
			<?php
		}
		?>
		</select>
	</td></tr>
	<tr><th>Alto (cm.)</th>
	<td><input type="number" id="alt" placeholder='Alto' value="<?php echo $res->alto;?>"></td>
	<th>Ancho (cm.)</th>
	<td><input type="number" id="anc" placeholder='Ancho' value="<?php echo $res->ancho;?>"></td></tr>
	<tr>
		<th>Descripción</th>
		<td colspan="3"><textarea id="des" placeholder='Descripción del material'><?php echo $res->descripcion;?></textarea></td>
	</tr>
</table>
<script type="text/javascript">addClick_v2('modal_ok','update_pieza',"<?php echo $res->idpi;?>")</script>