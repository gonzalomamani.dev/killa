<table border="0" width="100%">
	<tr>
		<td width="50%">
			<h3>Pieza:Grupo</h3>
			<table class="tabla">
				<tr class="fila">
					<th class="celda th" style="width:40%">Nombre</th>
					<th class="celda th" style="width:30%">Descripcion</th>
					<th class="celda th" style="width:15%">Modificar</th>
					<th class="celda th" style="width:15%">Eliminar</th>
				</tr>
		<?php for ($i=0; $i<count($pieza_grupos); $i++){$pg=$pieza_grupos[$i];
			?>
				<tr class="fila">
					<td class="celda td"><input type='text' value='<?php echo $pg->nombre;?>' id='nom_p<?php echo $pg->idpig;?>'></td>
					<td class="celda td"><textarea id='des_p<?php echo $pg->idpig;?>'><?php echo $pg->descripcion;?></textarea></td>
					<td class="celda td"><a href="javascript:" onclick="update_grupo('<?php echo $pg->idpig;?>')">Guardar</a></td>
					<td class="celda td"><a href="javascript:" onclick="delete_grupo('<?php echo $pg->idpig;?>')">Eliminar</a></td>
				</tr>
			<?php
		}?>
			<tr class="fila">
				<th class="celda th"><input type='text' placeholder='Nombre de Grupo' id="nom_p"></th>
				<th class="celda th" colspan="2"><textarea id="des_p" placeholder='Descripción de Grupo'></textarea></th>
				<th class="celda th"><a href="javascript:" onclick="save_grupo()">Guardar</a></th>
			</tr>
			</table>
		</td>
		<td width="50%"></td>
	</tr>
</table>