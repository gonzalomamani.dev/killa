<?php 
  $help1="Ingrese nombre de usuario, Tenga en cuenta que el sistema de validacion no distingue entre mayuscula y minuscula Ej. (Juan != juan)";
  $popover1='data-toggle="popover" data-placement="left" data-trigger="hover" title="<h4>Nombre de Usuario</h4>" data-content="'.$help1.'"';
  $help2="Ingrese su contraseña evitando usar letras mayusculas";
  $popover2='data-toggle="popover" data-placement="left" data-trigger="hover" title="<h4>Ingresar Contraseña<h4>" data-content="'.$help2.'"';
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Marroquineria-Killa">
   <meta name="author" content="gonzalo235711@gmail.com">
	<title>Killa</title>
  <link rel="icon" type="image/png" href="<?php echo base_url(); ?>favicon.png" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css/bootstrap.min.css">
	 
</head>
<body>
 <div class="modal show" role="dialog" aria-hidden="true" id='total'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">    
                  <img class="img-resposive" src="<?php echo base_url(); ?>libraries/img/sistema/killa.jpg" width="100%">
            </div>
                 <div class="modal-body">
                   <form role='form' action='<?php echo base_url(); ?>login/input' method='POST'>
                      <div class="form-group">
                        <div class="input-group input-group-xs">
                          <span class="input-group-addon" style='background-color: #fff;'><i class='glyphicon glyphicon-user'></i></span>
                            <input id='username' name='username' type="text" class="form-control input-lg" placeholder="Usuario" value="<?php echo set_value('username'); ?>">
                            <span class="input-group-addon" <?php echo $popover1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
                        </div>
                            <?php echo form_error('username');?>

                      </div>
                      <span class='clearfix'></span>
                      <div class="form-group">
                        <div class="input-group input-group-xs">
                          <span class="input-group-addon" style='background-color: #fff;'><i class='glyphicon glyphicon-lock'></i></span>
                            <input id='password' name='password' type="password" class="form-control input-lg" placeholder="Contraseña" value="<?php echo set_value('password'); ?>">
                          <span class="input-group-addon" <?php echo $popover2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
                        </div>
                        <?php echo form_error('password');?>
                       </div>
                         <input type="submit" class="btn btn-primary btn-lg btn-block" value ='Ingresar'>                        
                    </form>
                     <?php
                      if($exist=='falso'){
                        echo "<p>El usuario no existe</p>";
                      }
                    ?>
              </div>
                 <div class="modal-footer"></div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>libraries/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>libraries/js/bootstrap.min.js"></script>
<script language='javascript'>  if($(this).width()>=1100){ $("#username").focus(); }$('[data-toggle="popover"]').popover({html:true});</script>
</body>
</html>
