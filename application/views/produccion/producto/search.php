<table class="tabla tabla-border-false">
	<tr class="fila">
		<td class='celda g-thumbnail'><div class="g-img"></div></td>
		<td class='celda celda-sm-10'><form onsubmit="return view_producto();"><input id='s_cod' type="search" class="form-control input-sm" placeholder="Código" onkeyup="reset_input(this.id)"></form></td>
		<td class='celda celda-sm-30'><form onsubmit="return view_producto();"><input id='s_nom' type="search" class="form-control input-sm" placeholder="Nombre de Producto" onkeyup="reset_input(this.id)"></form></td>
		<td class="celda-sm" style="width:16%"><input id='s_fec' type="date" class="form-control input-sm" placeholder="2000-01-31" onchange="reset_input(this.id)"></td>
		<td class="celda-sm" style="width:20%"><form onsubmit="return view_producto();"><input id='s_dis' type="search" class="form-control input-sm" placeholder="Diseñador" onkeyup="reset_input(this.id)"></form></td>
		<td class="celda-sm" style="width:12%"></td>
		<td class="celda td text-right" style="width:12%">
			<?php $new="new_producto()"; if($privilegio[0]->pr1c!="1"){ $new="";}?>
			<?php $print="print()"; if($privilegio[0]->pr1p!="1"){ $print="";}?>
			<?php $this->load->view('estructura/botones/buscador',['f_buscar'=>"view_producto()",'f_ver'=>"all_producto()",'nuevo'=>'Nuevo Producto','f_nuevo'=>$new,'f_imprimir'=>$print]);?>
		</td>
	</tr>		
</table>
<script type="text/javascript">Onfocus('s_cod');</script>