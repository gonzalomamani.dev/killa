<?php
	$producto=$productos[0];
	$ruta=base_url().'libraries/img/pieza_productos/';
	$insumo="";
	$imagen1="no.png";
	$imagen2="no.png";
	$imagen3="no.png";
	$swap=$this->M_categoria_producto->get_portada_producto($producto->idp);
	if(count($swap)>0){
		$im=$this->M_imagen_producto->get_pieza_material($swap[0]->idpim);
		for($i=0; $i<count($im); $i++){$img_producto=$im[$i];
			switch ($i) {
				case 0: $imagen1=$img_producto->nombre; break;
				case 1: $imagen2=$img_producto->nombre; break;
				case 2: $imagen3=$img_producto->nombre; break;
			}
		}
	}
	$categoria_pieza=$this->M_categoria_pieza->get_categoria_pieza($producto->idp);
?>
<ul class="nav nav-tabs">
  <li role="presentation" class="active"><a href="javascript:" onclick="reportes('<?php echo $producto->idp; ?>')">Ficha Técnica</a></li>
  <li role="presentation"><a href="javascript:" onclick="costo_total_produccion('<?php echo $producto->idp; ?>')">Costo Total de Producción</a></li>
  <li role="presentation"><a href="javascript:" onclick="detalle_procesos('<?php echo $producto->idp; ?>')">Detalle de procesos</a></li>
</ul>
<div class="row-border table-responsive">
<div id="area">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="90%"><div class='encabezado_titulo'>FICHA TÉCNICA</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Página: </b>1 de 1</div>
				</td></tr>
			</table>
		</div>
	<table class="tabla tabla-border-true">
		<tr class='fila'>
			<th class='celda th' colspan="4" width="60%"><h2><?php echo $producto->nombre;?></h2></th>
			<td class='celda td' colspan="2" rowspan="9" width="40%"><img src="<?php echo $ruta.$imagen1;?>" alt="" width='100%'></td>
		</tr>
		<tr class='fila'>
			<th class='celda th' colspan="2">Código</th>
			<th class='celda th' colspan="2">Fecha de Creación</th>
		</tr>
		<tr class='fila'>
			<td class='celda td' colspan="2"><?php echo $producto->cod;?></td>
			<td class='celda td' colspan="2"><?php echo $producto->fecha_creacion;?></td>
			
		</tr>
		<tr class='fila'>
			<th class='celda th' colspan="4">Diseñador</th>
		</tr>
		<tr class='fila'>
			<td class='celda td' colspan="4"><?php echo $producto->disenador;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th" colspan="4">Descripción</th></tr>
		<tr class='fila'>
			<td class='celda td' colspan="4"><?php echo $producto->descripcion;?></td>
		</tr>
		<tr class='fila'>
			<th class='celda th' width="15%">Logo Etiqueta de Cuero</th>
			<th class='celda th' width="15%">Logo Plaqueta</th>
			<th class='celda th' width="15%">Logo Repujado</th>
			<th class='celda th' width="15%">Repujado Decorado</th>
		</tr>
		<tr class='fila'>
			<td class="celda td"><?php echo $producto->etiquetaLogo;?></td>
			<td class="celda td"><?php echo $producto->plaquetaLogo;?></td>
			<td class="celda td"><?php echo $producto->repujadoLogo;?></td>
			<td class="celda td"><?php echo $producto->repujado;?></td>
		</tr>
		<tr class='fila'>
			<th class='celda th' colspan="2">Correa</th>
			<th class='celda th' colspan="2">Cierre Dentro</th>
						<?php 
				$medida="";
				if($producto->largo!=0 & $producto->largo!=null){
					$medida.='Largo '.$producto->largo.'cm. ';
				}
				if($producto->alto!=0 & $producto->alto!=null){
					$medida.=' x Alto '.$producto->alto.'cm. ';
				}
				if($producto->ancho!=0 & $producto->ancho!=null){
					$medida.='x Ancho '.$producto->ancho.'cm. ';
				}
			?>
			<th class='celda th' rowspan="2" width="20%">Medidas</th>
			<td class='celda td' rowspan="2" width="20%"><?php echo $medida;?></td>
		</tr>
		<tr class='fila'>
			<td class='celda td' colspan="2"><?php echo $producto->correa?></td>
			<td class='celda td' colspan="2"><?php echo $producto->cierreDentro;?></td>
		</tr>
		<tr class='fila'>
			<th class='celda th' colspan="2">Cierre Delante</th>
			<th class='celda th' colspan="2">Cierre Detras</th>
			<th class="celda th" rowspan="4"><img src="<?php echo $ruta.$imagen2;?>" alt="" width='100%'> </th>
			<th class="celda th" rowspan="4"><img src="<?php echo $ruta.$imagen3;?>" alt="" width='100%'> </th>
		</tr>
		<tr class='fila'>
			<td class='celda td' colspan="2"><?php echo $producto->cierreDelante;?></td>
			<td class='celda td' colspan="2"><?php echo $producto->cierreDetras;?></td>
		</tr>
		<tr class='fila'>
			<th class="celda th" colspan="2">Colores Disponibles</th>
			<th class="celda th" colspan="2">Costo de Producto</th>
		</tr>
		<tr class="fila">
			<td class="celda td" colspan="2">
				<?php $ca_pr=$this->M_categoria_producto->get_categoria_producto_colores($producto->idp);
					for ($i=0; $i < count($ca_pr) ; $i++) {
					?>
					<div class="g-caja-color">
						<span class="g-color" style="background: <?php echo $ca_pr[$i]->codigo?>">&#160;&#160;&#160;&#160;</span>
						<span class="g-text-color"><?php echo $ca_pr[$i]->nombre;?></span>
					</div>
						<?php
					}
				?>
			</td>
			<td class="celda td" colspan="2"><?php if($producto->c_u){echo $producto->c_u.' Bs.';}?></td>
		</tr>
		<tr class='fila'><th class='celda th' colspan="6">Otras caracteristicas tecnicas</th></tr>
		<tr class='fila'><td class="celda td" colspan="6"><?php echo $producto->otra_caracteristica;?></td></tr>
	</table>
</div>
</div>