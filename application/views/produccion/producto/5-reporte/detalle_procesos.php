<?php $producto=$producto[0]; 
	$procesos=json_decode($procesos);
?>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="reportes('<?php echo $producto->idp; ?>')">Ficha Técnica</a></li>
  <li role="presentation"><a href="javascript:" onclick="costo_total_produccion('<?php echo $producto->idp; ?>')">Costo Total de Producción</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="detalle_procesos('<?php echo $producto->idp; ?>')">Detalle de procesos</a></li>
</ul>
<div id="area">
	<div class="pagina">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="90%"><div class='encabezado_titulo'>REPORTE DE ALMACENES</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'></span></div>
				</td></tr>
			</table>
		</div>
	<div class="tabla tabla-border-true">
		<div class="fila">
			<div class="celda th" style='width: 5%' >#Item</div>
			<div class="celda th" style='width: 7%' >Fotografía</div>
			<div class="celda th" style='width: 10%'>Código</div>
			<div class="celda th" style='width: 36%'>Nombre</div>
			<div class="celda th" style='width: 10%'>Tipo de almacen</div>
			<div class="celda th" style='width: 30%'>Descripción</div>
		</div>
	<?php $cont=0; 
			foreach ($procesos as $clave => $procesos) {  
				if($contReg>=$nro){
					$contReg=0;
					$contPagina++;
			?>
		</div><!--cerramos tabla-->
	</div><!--cerramos pagina-->
<div class="pagina">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="90%"><div class='encabezado_titulo'>REPORTE DE ALMACENES</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'></span></div>
				</td></tr>
			</table>
		</div>
	<div class="tabla tabla-border-true">
			<div class="fila">
			<?php if(!isset($ite)){?><div class="celda th" style='width: 7%' >#Item</div><?php } ?>
			<?php if(!isset($fot)){?><div class="celda th" style='width: 7%' >Fotografía</div><?php } ?>
			<?php if(!isset($cod)){?><div class="celda th" style='width: 10%'>Código</div><?php } ?>
			<?php if(!isset($nom)){?><div class="celda th" style='width: 36%'>Nombre</div><?php } ?>
			<?php if(!isset($tip)){?><div class="celda th" style='width: 10%'>Tipo de almacen</div><?php } ?>
			<?php if(!isset($des)){?><div class="celda th" style='width: 30%'>Descripción</div><?php } ?>
		</div>
		<?php } ?>
		<div class="fila">
			<?php if(!isset($ite)){?><div class="celda td"><?php echo $cont+1;$cont++;?></div><?php } ?>
			<?php if(!isset($fot)){?><div class="celda td"><img src="<?php echo $url.$img;?>" style='width:100%'></div><?php } ?>
			<?php if(!isset($cod)){?><div class="celda td"><?php echo $almacen->codigo;?></div><?php } ?>
			<?php if(!isset($nom)){?><div class="celda td"><?php echo $almacen->nombre;?></div><?php } ?>
			<?php if(!isset($tip)){?><div class="celda td"><?php echo $almacen->tipo;?></div><?php } ?>
			<?php if(!isset($des)){?><div class="celda td"><?php echo $almacen->descripcion;?></div><?php } ?>
		</div>
	<?php $contReg++; }// end for ?>
	</div>
</div>
</div>