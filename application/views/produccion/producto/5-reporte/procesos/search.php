<?php $producto=$producto[0]; ?>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="reportes('<?php echo $producto->idp; ?>')">Ficha Técnica</a></li>
  <li role="presentation"><a href="javascript:" onclick="costo_total_produccion('<?php echo $producto->idp; ?>')">Costo Total de Producción</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="detalle_procesos('<?php echo $producto->idp; ?>')">Detalle de procesos</a></li>
</ul>
<div class="row-border table-responsive">
	<table class="table table-bordered"><thead>
		<tr>
			<th width="17%">Nro. Reg. por hojas</th>
			<th width="23%">
				<select id="nro" class="form-control input-sm" style="width:100%" onchange="search_detalle_procesos('<?php echo $producto->idp;?>')">
				<?php for ($i=1; $i <= 50 ; $i++) { ?>
					<option value="<?php echo $i;?>" <?php if($i==33){echo "selected";}?>><?php echo $i;?></option>
				<?php } ?>
				</select>
			</th>
			<th width="60%"></th>
		</tr>
	</thead></table>
</div>
<div class="row-border table-responsive">
	<div id="area"></div>
</div>