<?php $producto=$producto[0]; 
	$procesos=json_decode($procesos);
	$contPagina=1;
	$contReg=0;
	$tiempo_parcial=0;
	$costo_parcial=0;
	$tiempo_total=0;
	$costo_total=0;
?>
<div class="pagina">
	<div class="encabezado_pagina">
		<table class="tabla tabla-border-false"><tr class="fila">
			<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
			<td class="celda td" width="90%"><div class='encabezado_titulo'>DETALLE DE PROCESOS</div></td>
			<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
				<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
				<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
				<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'></span></div>
			</td></tr>
		</table>
	</div>
	<table class="tabla tabla-border-true">
		<tr class='fila'>
			<th class='celda th' colspan="3" width="60%" rowspan="3"><h2><?php echo $producto->nombre;?></h2></th>
		</tr>
		<tr class="fila">
			<th class="celda th">Código</th>
			<td class="celda td" colspan="2"><?php echo $producto->cod;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Fecha de creación</th>
			<td class="celda td" colspan="2"><?php echo $producto->fecha_creacion;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th" colspan="6">DETALLE DE PROCESOS</th>
		</tr>
		<tr class="fila">
			<th class="celda th" width="5%">#Item</th>
			<th class="celda th" width="54%">Nombre de Proceso</th>
			<th class="celda th" width="11%">Cantidad</th>
			<th class="celda th" width="15%">Tiempo estimado de conclución</th>
			<th class="celda th" width="15%">Costo de producción (Bs.)</th>
		</tr>
<?php $i=1; 
	foreach($procesos as $clave => $proceso) { 
		if($contReg>=$nro){
			$contReg=0;
			$contPagina++;?>
			<tr class="fila">
			<th class="celda th" colspan="3">SUB TOTALES</th>
			<?php $hra=explode(":", $this->lib->hms($tiempo_parcial));?>
			<th class="celda th"><?php if($tiempo_parcial>0){ echo $hra[0].'h '.$hra[1].'m '.$hra[2].'s';} $tiempo_parcial=0;?></th>
			<th class="celda th"><?php echo number_format($costo_parcial,2,'.',',');$costo_parcial=0;?></th>
		</tr>
		<tr class="fila">
			<th class="celda th" colspan="3">TOTALES</th>
			<?php $hra=explode(":", $this->lib->hms($tiempo_total));?>
			<th class="celda th"><?php if($tiempo_total>0){ echo $hra[0].'h '.$hra[1].'m '.$hra[2].'s';}?></th>
			<th class="celda th"><?php echo number_format($costo_total,2,'.',',');?></th>
		</tr>
	</table>
</div>
<div class="pagina">
	<div class="encabezado_pagina">
		<table class="tabla tabla-border-false"><tr class="fila">
			<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
			<td class="celda td" width="90%"><div class='encabezado_titulo'>DETALLE DE PROCESOS</div></td>
			<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
				<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
				<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
				<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'></span></div>
			</td></tr>
		</table>
	</div>
	<table class="tabla tabla-border-true">
		<tr class='fila'>
			<th class='celda th' colspan="3" width="60%" rowspan="3"><h2><?php echo $producto->nombre;?></h2></th>
		</tr>
		<tr class="fila">
			<th class="celda th">Código</th>
			<td class="celda td" colspan="2"><?php echo $producto->cod;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Fecha de creación</th>
			<td class="celda td" colspan="2"><?php echo $producto->fecha_creacion;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th" colspan="6">DETALLE DE PROCESOS</th>
		</tr>
		<tr class="fila">
			<th class="celda th" width="5%">#Item</th>
			<th class="celda th" width="54%">Nombre de Proceso</th>
			<th class="celda th" width="11%">Cantidad</th>
			<th class="celda th" width="15%">Tiempo estimado de conclución</th>
			<th class="celda th" width="15%">Costo de producción (Bs.)</th>
		</tr>
<?php } ?>
		<tr class="fila">
			<td class="celda td"><?php echo $i++;?></td>
			<td class="celda td"><?php echo $proceso->nombre;?></td>
			<td class="celda td"><?php if($proceso->cantidad>0){echo $proceso->cantidad.' [unid.]';}?></td>
			<?php $hra=explode(":", $this->lib->hms($proceso->segundos)); $tiempo_parcial+=$proceso->segundos;$tiempo_total+=$proceso->segundos;?>
			<td class="celda td"><?php if($proceso->segundos>0){ echo $hra[0].'h '.$hra[1].'m '.$hra[2].'s';}?></td>
			<td class="celda td"><?php if($proceso->costo>0){echo $proceso->costo; $costo_parcial+=$proceso->costo; $costo_total+=$proceso->costo;}?></td>
		</tr>
<?php $contReg++; }// end for?>
		<tr class="fila">
			<th class="celda th" colspan="3">SUB TOTALES</th>
			<?php $hra=explode(":", $this->lib->hms($tiempo_parcial));?>
			<th class="celda th"><?php if($tiempo_parcial>0){ echo $hra[0].'h '.$hra[1].'m '.$hra[2].'s';}?></th>
			<th class="celda th"><?php echo number_format($costo_parcial,2,'.',',');?></th>
		</tr>
		<tr class="fila">
			<th class="celda th" colspan="3">TOTALES</th>
			<?php $hra=explode(":", $this->lib->hms($tiempo_total));?>
			<th class="celda th"><?php if($tiempo_total>0){ echo $hra[0].'h '.$hra[1].'m '.$hra[2].'s';}?></th>
			<th class="celda th"><?php echo number_format($costo_total,2,'.',',');?></th>
		</tr>
	</table>
</div>
<script type="text/javascript"> $(".nroPagina").html(<?php echo $contPagina;?>); </script>