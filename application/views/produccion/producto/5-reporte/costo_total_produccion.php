<?php $producto=$producto[0]; ?>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="reportes('<?php echo $producto->idp; ?>')">Ficha Técnica</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="costo_total_produccion('<?php echo $producto->idp; ?>')">Costo Total de Producción</a></li>
  <li role="presentation"><a href="javascript:" onclick="detalle_procesos('<?php echo $producto->idp; ?>')">Detalle de procesos</a></li>
</ul>
<div class="row-border table-responsive">
<div class="detalle" id="area">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="90%"><div class='encabezado_titulo'>COSTO TOTAL DE PRODUCCIÓN</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Página: </b><?php echo '1 de 1';?></div>
				</td></tr>
			</table>
		</div>
<table class="tabla tabla-border-true">
	<tr class='fila'>
		<th class='celda th' colspan="3" width="60%" rowspan="3"><h2><?php echo $producto->nombre;?></h2></th>
	</tr>
	<tr class="fila">
		<th class="celda th">Código</th>
		<td class="celda td" colspan="2"><?php echo $producto->cod;?></td>
	</tr>
	<tr class="fila">
		<th class="celda th">Fecha de creación</th>
		<td class="celda td" colspan="2"><?php echo $producto->fecha_creacion;?></td>
	</tr>
	<tr class="fila">
		<th class="celda th" colspan="6">DETALLE DE PRODUCCIÓN</th>
	</tr>
	<tr class="fila">
		<th class="celda th" rowspan="2" width="5%">#Item</th>
		<th class="celda th" rowspan="2" width="12%">Código</th>
		<th class='celda th' rowspan="2" width="44%">Material</th>
		<th class='celda th' colspan="2">Material por producto</th>
		<th class='celda th' width='12%' rowspan="2">Costo total(Bs.)</th>
	</tr>
	<tr class='fila'>
		<th class='celda th' width="15%">Cantidad</th>
		<th class='celda th' width="12%">C/U (Bs.)</th>
	</tr>
<?php 
	$materiales=json_decode($materiales); $i=1;
	$total_material=0;
	foreach ($materiales as $clave => $material) { ?>
	<tr class="fila">
		<td class="celda td"><?php echo $i++;?></td>
		<td class="celda td"><?php echo $material->codigo;?></td>
		<td class="celda td"><?php if($material->color!=""){echo $material->nombre.' - '.$material->color;}else{ echo $material->nombre;}?></td>
		<td class="celda td"><?php if($material->cantidad!="vacio"){if($material->cantidad>0){ echo $material->cantidad.' ['.$material->abreviatura.']';}}else{ echo "-";}?></td>
		<td class="celda td"><?php if($material->cantidad!="vacio"){if($material->costo_unitario>0){ echo number_format($material->costo_unitario,2,'.',',');}}else{ echo "-";}?></td>
		<?php $costo_total=$material->cantidad*$material->costo_unitario; ?>
		<td class="celda td"><?php if($material->cantidad!="vacio"){if($costo_total>0){ echo number_format($costo_total,2,'.',','); $total_material+=$costo_total;}}else{if($material->costo_unitario>0){echo $material->costo_unitario; $total_material+=$material->costo_unitario;} }?></td>
	</tr>
<?php  }// end for ?>
	<tr class="fila">
		<th class="celda th" colspan="5">TOTAL COSTO DE MATERIALES</th><th class="celda th"><?php echo $total_material;?></th>
	</tr>
	<tr class="fila">
		<td class="celda td" colspan="3">Tiempo y costo total estimado para su producción</td>
		<?php $procesos=json_decode($procesos);
			$total_proceso=0;
			$total_segundos=0;
			foreach($procesos as $clave => $proceso){ $total_proceso+=$proceso->costo; $total_segundos+=$proceso->segundos; }
			$hra=explode(":", $this->lib->hms($total_segundos));
		?>
		<td class="celda td" colspan="2"><?php echo $hra[0].'h '.$hra[1].'m '.$hra[2].'s '?></td>
		<td class="celda td"><?php echo number_format($total_proceso,2,'.',',');?></td>
	</tr>
	<tr class="fila">
		<th class="celda th" colspan="5">TOTAL PARCIAL DEL COSTO DE PRODUCCIÓN</th>
		<?php $total=$total_material+$total_proceso;?>
		<th class="celda th"><?php echo number_format($total,2,'.',',');?></th>
	</tr>
</table>
			<table class='tabla tabla-border-true'>
				<tr class='fila'><th class='celda th' rowspan="2" width="30%">Materiales segun color de Producto</th>
					<th class='celda th' colspan="2">Materiales segun color, por producto</th>

					<th class='celda th' width='10%' rowspan="2">Costo total por material (Bs.)</th>
					<th class='celda th' width='15%' rowspan="2">COLOR</th>
					<th class='celda th' width='15%' rowspan="2">COSTO TOTAL POR COLOR DE PRODUCTO</th>
				</tr>
					<tr class='fila'><th class='celda th' width="15%">Cantidad</th>
					<th class='celda th' width="15%">Costo Unitario</th></tr>
			<?php
			$materiales_color_producto=json_decode($materiales_color_producto);
			foreach ($materiales_color_producto as $clave => $material) {
				?>
				<tr class='fila'>
					<td class='celda td'><?php echo $material->nombre;?></td>
					<td class='celda td'><?php echo number_format($material->cantidad,4,'.',',').' ['.$material->abreviatura.']';?></td>
					<td class='celda td'><?php if($material->costo_unitario>0){echo $material->costo_unitario;}?></td>
					<td class='celda td'><?php if(($material->costo_unitario*$material->cantidad)>0){echo number_format(($material->costo_unitario*$material->cantidad),2,'.',',');}?></td>
					<td class="celda td">
						<div class="g-caja-color">
							<span class="g-color" style="background: <?php echo $material->codigo_color;?>">&#160;&#160;&#160;&#160;</span>
							<span class="g-text-color"><?php echo $material->color;?></span>
						</div>
					</td>
					<th class="celda th">
						<?php echo number_format(($total+($material->costo_unitario*$material->cantidad)),1,'.',',').' Bs.';?>
					</th>
				</tr>
				<?php
			}
		?>
			</table>
</div>
</div>
<!--
<div id="ficha_tecnica">
<table class='tabla tabla-border-true'>
	<tr class='fila'>
		<th class='celda th' width="40%" rowspan="3"><div id="visor">
			<?php 	$total=0;
			$swap=$this->M_categoria_producto->get_portada_producto($res->idp);
			$resto=3;
			$img="no.png";
			if(count($swap)>0){
				$im=$this->M_imagen_producto->get_pieza_material($swap[0]->idpim);
				$resto-=count($im);
				for ($i=0; $i < count($im) ; $i++) { $imagen=$im[$i];?>
					<div style="float:left; width:33.33%; margin:0 auto;">
						<img src="<?php echo $ruta.$imagen->nombre; ?>" width="100%">
					</div>
				<?php	}// end for
			}//end if
			for ($i=0; $i < $resto ; $i++) {?>
					<div style="float:left; width:33.33%; margin:0 auto;">
						<img src="<?php echo $ruta.$img; ?>" width="100%">
					</div>
			<?php	}// end for
			?>
		</div></th>
		<th class='celda th' width="20%" >Codigo de Producto</th>
		<td class='celda td' width="40%" colspan="2"><?php echo $prod->cod; ?></td>
	</tr>
	<tr class='fila'>
		<th class='celda th'>Nombre de Producto</th>
		<td class='celda td' colspan="2" ><?php echo $prod->nombre; ?></td>
	</tr>
	<tr class='fila'>
		<th class='celda th'>Descripcion de Producto</th>
		<td class='celda td' colspan="2" ><?php echo $prod->descripcion; ?></td>
	</tr>

	<tr class='fila'>
	<td class='celda td' colspan="4">
		<table class='tabla tabla-border-true'>
			<tr class='fila'><th class='celda th' rowspan="2" width="55%">Accesorios</th>
			<th class='celda th' colspan="2">Accesorios por producto</th>
			<th class='celda th' width='15%' rowspan="2">Costo total por accesorio (Bs.)</th></tr>
			<tr class='fila'><th class='celda th' width="15%">Cantidad</th>
			<th class='celda th' width="15%">Costo Unitario</th></tr>
			<?php
			$sumMat=0;
			for($i=0; $i < count($producto_materiales) ; $i++) {$pm=$producto_materiales[$i]; 
				$unidad=$this->M_unidad->get_col($pm->idu,'nombre');
				if(!empty($unidad)){
			?>
					<tr class='fila'>
						<td class='celda td'><?php echo $pm->nombre;?></td>
						<td class='celda td'><?php echo $pm->cantidad.' '.$unidad[0]->nombre;?></td>
						<td class='celda td'><?php if($pm->costo_unitario>0){echo $pm->costo_unitario;}?></td>
						<td class='celda td'><?php if(($pm->costo_unitario*$pm->cantidad)>0){echo number_format(($pm->costo_unitario*$pm->cantidad),2,'.',',');}?></td>
					</tr>
			<?php
					$sumMat+=($pm->costo_unitario*$pm->cantidad);
				}//end if
			}//end for
		?>

		<tr class='fila'>
			<th class='celda th' colspan="3">Total costo de accesorios en boliviados (Bs.)</th>
			<th class='celda th'><?php echo number_format($sumMat,2,',','.'); $total+=$sumMat;?></th>
		</tr>
		</table>
	</td>
	</tr>
	<tr class="fila">
		<td class="celda td" colspan="3">
			<table class='tabla tabla-border-true'>
				<tr class='fila'><th class='celda th' rowspan="2" width="55%">Materiales</th>
					<th class='celda th' colspan="2">Materiales por producto</th>
					<th class='celda th' width='15%' rowspan="2">Costo total por material (Bs.)</th></tr>
					<tr class='fila'><th class='celda th' width="15%">Cantidad</th>
					<th class='celda th' width="15%">Costo Unitario</th></tr>
			<?php
			$sumMat2=0;
			for ($i=0; $i < count($categoria_pieza); $i++) {$categoria=$categoria_pieza[$i];
				if($categoria->color_producto!=1){
					$material=$this->M_categoria_producto->get_categoria_grupo($categoria->idcp);
					if(!empty($material)){
						$unidad=$this->M_unidad->get($material[0]->idu);
						$color=$this->M_color->get($material[0]->idco);
						$pmezas=$this->M_pieza->get_row('idcp', $categoria->idcp);

						$area=0; 
						for($pm=0; $pm<count($pmezas); $pm++){ $area+=(($pmezas[$pm]->alto*$pmezas[$pm]->ancho)*$pmezas[$pm]->unidades);}
						$cuero_necesario=$area/$unidad[0]->equivalencia;
					?>
					<tr class='fila'>
						<td class='celda td'><?php echo $material[0]->nombre.' - '.$color[0]->nombre;?></td>
						<td class='celda td'><?php echo number_format($cuero_necesario,4,'.',',').' '.$unidad[0]->abreviatura;?></td>
						<td class='celda td'><?php if($material[0]->costo_unitario>0){echo $material[0]->costo_unitario;}?></td>
						<td class='celda td'><?php if(($material[0]->costo_unitario*$cuero_necesario)>0){echo number_format(($material[0]->costo_unitario*$cuero_necesario),2,'.',',');}?></td>
					</tr>
					<?php
						$sumMat2+=($material[0]->costo_unitario*$cuero_necesario);
					}//end if
				}//end if
			}//end for
		?>
		<tr class='fila'>
			<th class='celda th' colspan="3">Total costo de materiales en boliviados (Bs.)</th>
			<th class='celda th'><?php echo number_format($sumMat2,2,',','.');$total+=$sumMat2;?></th>
		</tr>
			</table>
		</td>
	</tr>
	<tr class="fila">
		<td class="celda td" colspan="3">
			<table class='tabla tabla-border-true'>
				<tr class='fila'><th class='celda th' rowspan="2" width="55%">Materiales Liquidos</th>
					<th class='celda th' colspan="2">Materiales Liquidos por producto</th>
					<th class='celda th' width='15%' rowspan="2">Costo total por material liquido (Bs.)</th></tr>
					<tr class='fila'><th class='celda th' width="15%">Cantidad</th>
					<th class='celda th' width="15%">Costo Unitario</th></tr>
		<?php 
			//$materiales_adicionales=$this->M_material_adicional->get_suma_material_adicional($prod->idp);
			$sumMat3=0;
		for($i=0; $i < count($materiales_liquidos) ; $i++) {$ml=$materiales_liquidos[$i];
			//$material_adicional=$this->M_material_adicional->get($ma->idma);
			//$material=$this->M_material->get($ml->idm);
			$material=$this->M_material->get_material_atributo_idm($ml->idm,"unidad");
			
			//$unidad=$this->M_unidad->get($material[0]->idu);
			if(!empty($material)){
				$color=$this->M_color->get($material[0]->idco);
				$cantidad_necesaria=($ml->area*$ml->variable)/$material[0]->equivalencia;
			?>
			<tr class='fila'>
					<td class='celda td'><?php echo $material[0]->nombre.' - '.$color[0]->nombre;?></td>
					<td class='celda td'><?php echo number_format($cantidad_necesaria,4,'.',',').' '.$material[0]->nombre_u;?></td>
					<td class='celda td'><?php if($material[0]->costo_unitario>0){echo $material[0]->costo_unitario;}?></td>
					<td class='celda td'><?php if(($material[0]->costo_unitario*$cantidad_necesaria)>0){echo number_format(($material[0]->costo_unitario*$cantidad_necesaria),2,'.',',');}?></td>
				</tr>
			<?php $sumMat3+=($material[0]->costo_unitario*$cantidad_necesaria);
			} //end if
		}// end for
		?>
		<tr class='fila'>
			<th class='celda th' colspan="3">Total costo de materiales adicionales en boliviados (Bs.)</th>
			<th class='celda th'><?php echo number_format($sumMat3,2,',','.');$total+=$sumMat3;?></th>
		</tr>
		</table>
		</td>
	</tr>
	<tr class='fila'>
		<td class='celda td' colspan="4">
		<table class='tabla tabla-border-true'>
			<tr class='fila'>
				<th class='celda th' width="65%">Proceso de producción</th>
				<th class='celda th' width="20%">Tiempo de Producción</th>
				<th class='celda th' width='15%'>Costo de Producción (Bs.)</th>
			</tr>
			<?php 
			$sumHora=0;
			$sumBsPro=0;
		for ($i=0; $i < count($procesos_pieza) ; $i++) { $proceso_pieza=$procesos_pieza[$i];
				$proceso=$this->M_proceso->get($proceso_pieza->idpr);
				$tiempo=explode(":", $this->validaciones->segundos_a_hms($proceso_pieza->tiempo_estimado));
				$hora="";
				if($tiempo[0]!=0 & $tiempo[0]!=''){ $hora.=$tiempo[0].'h ';}
				if($tiempo[1]!=0 & $tiempo[1]!=''){ $hora.=$tiempo[1].'m ';}
				if($tiempo[2]!=0 & $tiempo[2]!=''){ $hora.=$tiempo[2].'s';}
				?>
				<tr class='fila'>
					<td class='celda td' width="30%"><?php echo $proceso[0]->nombre.': '.$proceso_pieza->nombre.' ( '.$proceso_pieza->unidades.' Unidades )'; ?></td>
					<td class='celda td' width="50%"><?php echo $hora; $sumHora+=$proceso_pieza->tiempo_estimado;?></td>
					<td class='celda td' width="20%"><?php if($proceso_pieza->costo>0){echo number_format($proceso_pieza->costo,2,',','.');} $sumBsPro+=$proceso_pieza->costo;?></td>
				</tr>
		<?php } ?>
		<?php
			for($i=0;$i<count($producto_procesos);$i++){$pp=$producto_procesos[$i];
				$tiempo=explode(":", $this->validaciones->segundos_a_hms($pp->tiempo_estimado));
				$hora="";
				if($tiempo[0]!=0 & $tiempo[0]!=''){ $hora.=$tiempo[0].'h ';}
				if($tiempo[1]!=0 & $tiempo[1]!=''){ $hora.=$tiempo[1].'m ';}
				if($tiempo[2]!=0 & $tiempo[2]!=''){ $hora.=$tiempo[2].'s';}
				?>
					<tr class='fila'>
						<td class='celda td'><?php echo $pp->nombre.': '.$pp->sub_proceso;?></td>
						<td class='celda td'><?php echo $hora; $sumHora+=$pp->tiempo_estimado;?></td>
						<td class='celda td'><?php if($pp->costo>0){echo number_format($pp->costo,2,',','.');} $sumBsPro+=$pp->costo;?></td>
					</tr>
				<?php
			} ?>
		<tr class='fila'>
			<th class='celda th'>TOTALES</th>
			<?php $tiempo=explode(":", $this->validaciones->segundos_a_hms($sumHora));
				$sumHora="";
				if($tiempo[0]!=0 & $tiempo[0]!=''){$sumHora.=$tiempo[0].'h ';}
				if($tiempo[1]!=0 & $tiempo[1]!=''){$sumHora.=$tiempo[1].'m ';}
				if($tiempo[2]!=0 & $tiempo[2]!=''){$sumHora.=$tiempo[2].'s';}	
			?>
			<th class='celda th'><?php echo $sumHora;?></th>
			<th class='celda th'><?php if($sumBsPro>0){echo number_format($sumBsPro,2,'.',','); $total+=$sumBsPro;}?></th>
		</tr>
		</table>
		</td>
	</tr>
	
	<tr class='fila'>
		<td class='celda td' colspan="4">
		<table class='tabla tabla-border-true'>
			<tr class='fila'>
				<th class='celda th' width="85%">Material Indirecto</th>
				<th class='celda th' width='15%'>Costo de Producción (Bs.)</th>
			</tr>
			<?php $sumBsMat=0; for($i=0;$i<count($materiales_adicionales);$i++){$pm=$materiales_adicionales[$i]; ?>
				<tr class='fila'>
					<td class='celda td'><?php echo $pm->nombre;?></td>
					<td class='celda td'><?php if($pm->costo>0){echo number_format($pm->costo,2,',','.');} $sumBsMat+=$pm->costo;?></td>
				</tr>
			<?php } ?>
		<tr class='fila'>
			<th class='celda th'>TOTALES</th>
			<th class='celda th'><?php if($sumBsMat>0){echo number_format($sumBsMat,2,'.',','); $total+=$sumBsMat;}?></th>
		</tr>
		</table>
		</td>
	</tr>
	<tr class='fila'><th class='celda th' colspan="3">TOTAL COSTO DE PRODUCCIÓN (<?php echo $total;?>)</th></tr>
	<tr class="fila">
		<td class="celda td" colspan="3">
			<table class='tabla tabla-border-true'>
				<tr class='fila'><th class='celda th' rowspan="2" width="35%">Materiales segun color de Producto</th>
					<th class='celda th' colspan="2">Materiales segun color, por producto</th>

					<th class='celda th' width='10%' rowspan="2">Costo total por material (Bs.)</th>
					<th class='celda th' width='10%' rowspan="2">COLOR</th>
					<th class='celda th' width='15%' rowspan="2">COSTO TOTAL POR COLOR DE PRODUCTO</th>
				</tr>
					<tr class='fila'><th class='celda th' width="15%">Cantidad</th>
					<th class='celda th' width="15%">Costo Unitario</th></tr>
			<?php
			$sumMat2=0;
			$categoria_pieza=$this->M_categoria_pieza->get_categoria_pieza($prod->idp);
			for ($i=0; $i < count($categoria_pieza); $i++) { $categoria=$categoria_pieza[$i];
				if($categoria->color_producto!=0){
					$material=$this->M_categoria_producto->get_categoria_grupo($categoria->idcp);
					for ($ma=0; $ma < count($material) ; $ma++) { 
						$unidad=$this->M_unidad->get($material[$ma]->idu);
						$pmezas=$this->M_pieza->get_row('idcp', $categoria->idcp);
						$area=0; 
						for($pm=0; $pm<count($pmezas); $pm++){ $area+=(($pmezas[$pm]->alto*$pmezas[$pm]->ancho)*$pmezas[$pm]->unidades);}
						$cuero_necesario=$area/$unidad[0]->equivalencia;
						//color
						$color=$this->M_color->get($material[$ma]->idco);
				?>
				<tr class='fila'>
					<td class='celda td'><?php echo $material[$ma]->nombre.' - '.$color[0]->nombre;?></td>
					<td class='celda td'><?php echo number_format($cuero_necesario,4,'.',',').' '.$unidad[0]->abreviatura;?></td>
					<td class='celda td'><?php if($material[$ma]->costo_unitario>0){echo $material[$ma]->costo_unitario;}?></td>
					<td class='celda td'><?php if(($material[$ma]->costo_unitario*$cuero_necesario)>0){echo number_format(($material[$ma]->costo_unitario*$cuero_necesario),2,'.',',');}?></td>
					<td class="celda td">
						<div class="g-caja-color">
							<span class="g-color" style="background: <?php echo $color[0]->codigo?>">&#160;&#160;&#160;&#160;</span>
							<span class="g-text-color"><?php echo $color[0]->nombre;?></span>
						</div>
					</td>
					<th class="celda th">
						<?php echo number_format(($total+($material[$ma]->costo_unitario*$cuero_necesario)),1,'.',',').' Bs.';?>
					</th>
				</tr>
				<?php
					}//end for
				}//end if
			}//end for
		?>
			</table>
		</td>
	</tr>
</table>
</div>
</div>-->

