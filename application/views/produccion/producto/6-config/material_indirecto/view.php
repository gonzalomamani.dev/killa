<?php 
	if(count($materiales)>0){
		$url=base_url().'libraries/img/materiales_adicionales/miniatura/'; ?>
		<table class="table table-bordered table-striped">
			<thead>
				<th class="g-thumbnail-modal"><div class="g-img-modal">#Item</div></th>
				<th class="celda-sm-15">Código</th>
				<th class="celda-sm-80">Nombre</th>
				<th style="width:5%"></th>
			</thead>
			<?php for($i=0; $i < count($materiales) ; $i++) { $material=$materiales[$i];
					$img="default.png";
					if($material->fotografia!="" & $material->fotografia!=null){ $img=$material->fotografia; }
			?>
					<tr>
						<td><div id="item"><?php echo $i+1;?></div><img src="<?php echo $url.$img;?>" width='100%' class='img-thumbnail g-thumbnail-modal'></td>
						<td><?php echo $material->codigo;?></td>
						<td><?php echo $material->nombre;?></td>
						<td>
					<?php 
						//$control=$this->M_producto_material->get_control($material->idm,$idp);
						$control=$this->M_producto_material_adicional->get_row_2n('idma',$material->idma,'idp',$idp);
						if(!empty($control)){
					?>
						<div class="btn-circle btn-circle-red btn-circle-25" onclick="select_indirecto('<?php echo $idp;?>','<?php echo $material->idma;?>',this,'default','25');"></div>
					<?php }else{ ?>
						<div class="btn-circle btn-circle-default btn-circle-25" onclick="select_indirecto('<?php echo $idp;?>','<?php echo $material->idma;?>',this,'red','25');"></div>
					<?php	}
					?>
						
						<!--<a href="javascript:" onclick="select_material('<?php echo $idp;?>','<?php echo $material->idm;?>')">Seleccionar</a>--></td>
					</tr>
					<?php
				}
			?>
		</table>
		<?php
	}else{
		echo "<h3>0 registros encontrados...</h3>";
	}
?>

