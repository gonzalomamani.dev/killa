<?php 
	if(count($materiales)>0){
		$url=base_url().'libraries/img/materiales/miniatura/'; ?>
		<table class="table table-bordered table-striped">
			<thead>
				<th class="g-thumbnail-modal"><div class="g-img-modal">#Item</div></th>
				<th class="celda-sm-15">Código</th>
				<th class="celda-sm-70">Nombre</th>
				<th style="width:10%" class="celda-sm">Color</th>
				<th style="width:5%"></th>
			</thead>
			<?php for($i=0; $i < count($materiales) ; $i++) { $material=$materiales[$i];
					$img="default.png";
					if($material->fotografia!="" & $material->fotografia!=null){ $img=$material->fotografia; }
			?>
					<tr>
						<td><div id="item"><?php echo $i+1;?></div><img src="<?php echo $url.$img;?>" width='100%' class='img-thumbnail g-thumbnail-modal'></td>
						<td><?php echo $material->codigo;?></td>
						<td><?php echo $material->nombre;?>
						<span class="sm-cel" style="font-weight:bold; color: <?php echo $material->codigo_c;?>"><br>(<?php echo $material->nombre_c;?>)</span></td>
						<td class="celda-sm">
							<ul class="list-group" style="font-size:10px; padding: 1px"><li class="list-group-item" style="padding-top: 5px;padding-bottom: 5px;color:white; background:<?php echo $material->codigo_c;?>"><?php echo $material->nombre_c;?></li></ul>
						</td>
						<td>
					<?php 
						$control=$this->M_producto_material->get_control($material->idm,$idp);
						if(!empty($control)){
					?>
						<div class="btn-circle btn-circle-red btn-circle-25" onclick="select_material('<?php echo $idp;?>','<?php echo $material->idm;?>',this,'default','25');"></div>
					<?php }else{ ?>
						<div class="btn-circle btn-circle-default btn-circle-25" onclick="select_material('<?php echo $idp;?>','<?php echo $material->idm;?>',this,'red','25');"></div>
					<?php	}
					?>
						
						<!--<a href="javascript:" onclick="select_material('<?php echo $idp;?>','<?php echo $material->idm;?>')">Seleccionar</a>--></td>
					</tr>
					<?php
				}
			?>
		</table>
		<?php
	}else{
		echo "<h3>0 registros encontrados...</h3>";
	}
?>

