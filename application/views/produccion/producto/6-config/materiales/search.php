<?php
  $help1='title="<h4>Almacen<h4>" data-content="Seleccione el almacen donde desea buscar el material, si no selecciona ningun almacen se mostrara por defectos todos los materiales de todos los almacenes"';
  $popover='data-toggle="popover" data-placement="left" data-trigger="hover" ';
?>
<div class="row-border">
<table class="tabla tabla-border-false">
  <tr class="fila">
    <th class="g-thumbnail-modal"><div class="g-img-modal"></div></th>
    <td class="celda celda-sm-15"><form onsubmit="return search_producto_material('<?php echo $idp;?>')"><input type="search" class="form-control input-sm" id="cod_3" onkeyup="reset_input_3(this.id)" placeholder='Código'></form></td>
    <td class="celda celda-sm-50"><form onsubmit="return search_producto_material('<?php echo $idp;?>')"><input type="search" class="form-control input-sm" id="nom_3" onkeyup="reset_input_3(this.id)" placeholder='Nombre de material'></form></td>
    <td class="celda" width="21%">
      <div class="input-group col-xs-12">
        <select class="form-control input-sm" id="alm_3" onchange="search_producto_material('<?php echo $idp?>')">
          <option value="">Seleccione...</option>
            <?php for ($i=0; $i < count($almacenes) ; $i++) { $almacen=$almacenes[$i]; ?>
              <option value="<?php echo $almacen->ida;?>"><?php echo $almacen->nombre;?></option>
            <?php } ?>
        </select>
        <span class="input-group-addon" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
      </div>
    </td>
    <td class="celda" width="5%">
      <?php $this->load->view('estructura/botones/buscador',['f_buscar'=>"search_producto_material('".$idp."')"]);?>
    </td>
  </tr>
</table></div>
<div class="row-border" id="contenido_2"></div>
<script language='javascript'>Onfocus("cod_3");$('[data-toggle="popover"]').popover({html:true});</script>