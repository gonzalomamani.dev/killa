<table class="tabla tabla-border-false">
	<tr class='fila'><th class='celda th' width="25%">Fotografía</th><td class='celda td' colspan="3"><input class='form-control input-xs' type="file" id="fot"></td></tr>
	<tr class='fila'><th class='celda th'>Nombre</th>
	<td class='celda td' colspan="3"><input class='form-control input-xs' type="text" id="nom" placeholder='Nombre de Material'></td></tr>
	<tr class='fila'>
		<tr class='fila'>
			<th class='celda th'>Descripción</th><td class='celda td' colspan="3"><textarea class="form-control input-xs" id="des" placeholder='Descripción del material'></textarea></td>
		</tr>
	</tr>
	
	<tr class='fila'>
		<th class='celda th' width="20%">Unidad de Medida</th>
		<td class='celda td' width="30%">
			<select class='form-control input-xs' id="med">
				<option value="">Seleccione...</option>
			<?php for ($i=0; $i < count($unidades);$i++){ $u=$unidades[$i]; ?>
				<option value="<?php echo $u->idu;?>"><?php echo $u->nombre;?></option>
			<?php } ?>
			</select>
		</td>
		<th class='celda th' width="20%"></th>
		<td class='celda td' width="30%"></td>
	</tr>
</table>