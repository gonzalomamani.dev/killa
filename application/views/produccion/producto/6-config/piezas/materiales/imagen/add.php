<?php 
	$url_prod=base_url().'libraries/img/pieza_productos/'; 
	$resto=3-count($imagenes);
?>
<div class="row row-border">
<?php for ($i=0; $i < count($imagenes) ; $i++){$imagen=$imagenes[$i]; ?>
	<div class="col-sm-4 col-xs-6">
		<div id="item"><button type="button" class="close" title="Eliminar Imagen" onclick="drop_imagen_pieza_material('<?php echo $idp;?>','<?php echo $idpim;?>','<?php echo $imagen->idim;?>')">×</button></div>
		<img src="<?php echo $url_prod.$imagen->nombre?>" width='100%' class='img-thumbnail'>
	</div><?php if($i==1){?><i class='visible-xs-block clearfix'></i><?php }?>
<?php }
	for ($i=0; $i < $resto; $i++) { ?>
	<div class="col-sm-4 col-xs-6">
		<img src="<?php echo $url_prod.'default.png'?>" width='100%' class='img-thumbnail'>
	</div>
<?php } ?>
</div>
<div class="row">
<div class="col-sm-4 col-xs-12"><strong>Usted puede subir <?php echo $resto;?> imagen(es)</strong></div>
<div class="col-sm-8 col-xs-12"><?php if($resto>0){?><input type="file" multiple="multiple" id="fot" class="form-control input-sm"><?php }?></div>
</div>