<?php 
	if(count($materiales)>0){
		$url=base_url().'libraries/img/materiales/miniatura/'; ?>
	<div class="table-responsive g-table-responsive">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th class="g-thumbnail-modal"><div class="g-img-modal">#Item</div></th>
				<th class="celda-sm-15">Código</th>
				<th class="celda-sm-70">Nombre</th>
				<th style="width:15%"></th>
			</thead>
			<?php for($i=0; $i < count($materiales) ; $i++) { $material=$materiales[$i];
					$img="default.png";
					if($material->fotografia!="" & $material->fotografia!=null){ $img=$material->fotografia; }
			?>
					<tr>
						<td><div id="item"><?php echo $i+1;?></div><img src="<?php echo $url.$img;?>" width='100%' class='img-thumbnail g-thumbnail-modal'></td>
						<td><?php echo $material->codigo;?></td>
						<td><?php echo $material->nombre;?><strong style="color: <?php echo $material->codigo_c;?>" class='celda-sm'> - <?php echo $material->nombre_c;?></strong>
						<span class="sm-cel" style="font-weight:bold; color: <?php echo $material->codigo_c;?>"><br>(<?php echo $material->nombre_c;?>)</span></td>
						<td><button class="btn btn-success btn-sm" onclick="adicionar_material_grupo('<?php echo $idcp;?>','<?php echo $material->idm;?>','<?php echo $idp;?>');" title="Adicionar material al grupo de piezas"><span class="glyphicon glyphicon-plus"></span> Adicionar</button></td>
					</tr>
					<?php
				}
			?>
		</table>
	</div>
		<?php
	}else{
		echo "<h3>0 registros encontrados...</h3>";
	}
?>

