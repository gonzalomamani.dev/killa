<?php
	$help1='title="<h4>Subir Fotografía</h4>" data-content="Seleccione una fotografía, preferiblemente una imagen que no sobrepase las dimenciones de <strong>700x700[px]</strong>, para evitar sobre cargar al sistema, en formatos .jpg,.png o .gif"';
	$help2='title="<h4>Ingresar nombre de pieza<h4>" data-content="Ingrese un nombre alfanumerico de 3 a 200 caracteres puede incluir espacios, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help3='title="<h4>Ingresar codigo de pieza<h4>" data-content="Ingrese un código alfanumerico de 2 a 15 caracteres <b>sin espacios</b>, ademas el codigo solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help4='title="<h4>Grupo de pieza<h4>" data-content="Seleccione un grupo de la pieza, El valor vacio no es aceptado, si desea adicionar una nuevo grupo lo puede hacer en el menu superior (Configuracion) o dar click en el boton <b>+</b>"';
	$help5='title="<h4>Alto de la pieza<h4>" data-content="Ingrese un alto de la pieza <b>en centímetros</b>, en caso de ser una pieza con varias altitudes tomar la mayor altitud, los valores deben de estar entre el rango  <b>de 1 a 99999.99, con un máximo de dos decimales.</b>"';
	$help6='title="<h4>Ancho de la pieza<h4>" data-content="Ingrese un ancho de la pieza <b>en centímetros</b>, en caso de ser una pieza con varios valores de ancho tomar el mayor valor de ancho de la pieza, los valores deben de estar entre el rango  <b>de 1 a 99999.99, con un máximo de dos decimales.</b>"';
	$help7='title="<h4>Cantidad de piezas<h4>" data-content="Ingrese la cantidad de unidades necesarias de la pieza en el producto, el valor aceptado debe estar dentro el rango de 1 y 999"';
	$help8='title="<h4>Descripcion del piezas<h4>" data-content="Ingrese un descripcion alfanumerica de 5 a 500 caracteres puede incluir espacios, ademas la descripcion solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$popover='data-toggle="popover" data-placement="right" data-trigger="hover"';
	$pieza=$pieza[0];
?>
<div class="row">
	<div class="col-sm-3 col-sm-offset-9 col-xs-12"><strong><span class='text-danger'>(*)</span> Campo obligatorio</strong></div>
	<div class="col-sm-2 col-xs-12"><strong>Fotografía: </strong></div>
	<div class="col-sm-10 col-xs-12">
		<div class="input-group col-xs-12">
			<span class="input-group-addon input-sm" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			<input class='form-control input-sm' type="file" id="fot">
		</div>
	</div><i class='clear-fix'></i>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Grupo de piezas:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<div class="input-group">
			<span class="input-group-addon input-sm" <?php echo $popover.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			<select class='form-control input-sm' id="gru">
				<option value="">Seleccione...</option>
			<?php
			for ($i=0; $i < count($pieza_grupos);$i++){ $pg=$pieza_grupos[$i]; 
				?>
				<option value="<?php echo $pg->idpig;?>" <?php if($pieza->idpig==$pg->idpig){ echo "selected";}?>><?php echo $pg->nombre;?></option>
				<?php } ?>
			</select>
			<a href="<?php echo base_url().'produccion?p=5';?>" target="_blank" title="Ver Configuraciónes" class="input-group-addon input-sm"><i class='glyphicon glyphicon-plus'></i></a>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Cantidad de Piezas:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<form onsubmit="return update_pieza('<?php echo $pieza->idpi;?>','<?php echo $idcp;?>','<?php echo $idp;?>')">
			<div class="input-group">
				<span class="input-group-addon input-sm" <?php echo $popover.$help7;?>><i class='glyphicon glyphicon-info-sign'></i></span>
				<input class='form-control input-sm' type="number" id="can" placeholder='Unidades de piezas por producto' min='1' max='999' value="<?php echo $pieza->unidades;?>">
			</div>
		</form>
	</div>
	<i class='clearfix'></i>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Alto[cm]:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<form onsubmit="return update_pieza('<?php echo $pieza->idpi;?>','<?php echo $idcp;?>','<?php echo $idp;?>')">
			<div class="input-group">
				<span class="input-group-addon input-sm" <?php echo $popover.$help5;?>><i class='glyphicon glyphicon-info-sign'></i></span>
				<input class='form-control input-sm' type="number" id="alt" placeholder='Alto de pieza' step='any' min='0' max='99999.99' value="<?php echo $pieza->alto;?>">
			</div>
		</form>
	</div>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Ancho[cm]:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<form onsubmit="return update_pieza('<?php echo $pieza->idpi;?>','<?php echo $idcp;?>','<?php echo $idp;?>')">
			<div class="input-group">
				<span class="input-group-addon input-sm" <?php echo $popover.$help6;?>><i class='glyphicon glyphicon-info-sign'></i></span>
				<input class='form-control input-sm' type="number" id="anc" placeholder='Ancho de pieza' step='any' min='0' max='99999.99' value="<?php echo $pieza->ancho;?>">
			</div>
		</form>
	</div><i class='clearfix'></i>
	<div class="col-sm-2 col-xs-12"></div>
	<div class="col-sm-4 col-xs-12">
	</div><i class='clearfix'></i>
	<div class="col-sm-2 col-xs-12"><strong>Descripción:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<div class="input-group col-xs-12">
			<span class="input-group-addon input-sm" <?php echo $popover.$help8;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			<textarea class="form-control input-sm" id="des" placeholder='Descripción de la pieza' maxlength="200"><?php echo $pieza->descripcion;?></textarea>
		</div>
	</div><i class='clear-fix'></i>
</div>
<script language='javascript'>Onfocus("nom");$('[data-toggle="popover"]').popover({html:true});</script>