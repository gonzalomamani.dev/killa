<?php 
	$help1='title="<h4>Procesos de la pieza<h4>" data-content="Seleccionar todos los procesos que la pieza posee, en todo su proceso de producción, si desea adicionar nuevos procesos puede realizarlo en el <b>menu superior configuración</b> o dar dar click en el boton <strong>+</strong>"';
	$help2='title="<h4>Materiales de la pieza<h4>" data-content="Seleccionar todos los materiales que intervienen en el proceso de produccion de la pieza, en todo su proceso de producción, si desea adicionar nuevos materiales puede realizarlo en el <b>menu superior configuración</b> o dar dar click en el boton <strong>+</strong>"';
	$popover='data-toggle="popover" data-placement="right" data-trigger="hover"';

	$url_m=base_url().'libraries/img/materiales/miniatura/';
	$url=base_url().'libraries/img/piezas/miniatura/';
	$pieza=$pieza[0];
?>
<div class="row">
	<?php 	
		$img="default.png";
		if($pieza->imagen!="" && $pieza->imagen!=NULL){ $img=$pieza->imagen;}
	?>
	<div class="col-md-2 col-sm-2 col-xs-3"><img src="<?php echo $url.$img;?>" class="img-thumbnail"></div>
	<div class="col-md-10 col-sm-10 col-xs-9"><div class="table-responsive">
		<table class="table table-bordered table-striped">
			<tbody>
				<tr><th>Tipo de pieza:</th><td><?php echo $pieza->nombre_grupo;?></td></tr>
				<tr><th>Descripción:</th><td><?php echo $pieza->descripcion;?></td></tr>
			</tbody>
		</table>
	</div></div>
</div><hr>
<div class="row">
	<div class="col-md-6 col-xs-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<tr class="fila">
					<th class="celda th" colspan="2">
						<div class="input-group col-xs-12">
							<span class="input-group-addon" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
							<div class="form-control input-sm" disabled>PROCESO DE LA PIEZA</div>
							<a href="<?php echo base_url().'produccion?p=5';?>" target="_blank" title="Ver Configuraciónes" class="input-group-addon"><i class='glyphicon glyphicon-plus'></i></a>
						</div>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php for($i=0; $i < count($procesos); $i++){ $proceso=$procesos[$i];
					$pieza_proceso=$this->M_pieza_proceso->get_row_2n('idpi',$pieza->idpi,'idpr',$proceso->idpr);
					?>
						<tr class="fila">
							<td class="celda td" width="90%"><?php echo $proceso->nombre;?></td>
							<td class="celda td" width="10%">
							<?php if (count($pieza_proceso)>0) { ?>
								<div class="btn-circle btn-circle-red btn-circle-30" onclick="save_pieza_proceso('<?php echo $proceso->idpr;?>','<?php echo $pieza->idpi;?>',this,'red','30')"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="save_pieza_proceso('<?php echo $proceso->idpr;?>','<?php echo $pieza->idpi;?>',this,'red','30')"></div>
							<?php } ?>
							</td>
						</tr>
				<?php
				}?>
			</tbody>
		</table>
	</div>
	<div class="col-md-6 col-xs-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<tr class="fila">
					<th class="celda th" colspan="3">
						<div class="input-group col-xs-12">
							<span class="input-group-addon" <?php echo $popover.$help2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
							<div class="form-control input-sm" disabled>MATERIALES ADICIONALES</div>
							<a href="<?php echo base_url().'produccion?p=5';?>" target="_blank" title="Ver Configuraciónes" class="input-group-addon"><i class='glyphicon glyphicon-plus'></i></a>
						</div>
					</th>
				</tr>
			</thead>
			<tbody>
			<?php for($i=0; $i < count($materiales_liquidos); $i++){ $ma=$materiales_liquidos[$i];
				$material=$this->M_material->get($ma->idm);
				$img="default.png";
				if($material[0]->fotografia!='' && $material[0]->fotografia!=NULL){$img=$material[0]->fotografia;}
				$pieza_material_adicional=$this->M_pieza_material_liquido->get_row_2n('idpi',$pieza->idpi,'idml',$ma->idml);
			?>
				<tr class="fila">
					<td class="g-thumbnail-modal"><div id="item"><?php echo $i+1;?></div><div class="g-img-modal"><img src='<?php echo $url_m.$img;?>' class="img-thumbnail g-thumbnail-modal"></div></td>
					<td class=" celda-sm-90"><?php echo $material[0]->nombre;?></td>
					<td style="width:'10%">
				<?php if (count($pieza_material_adicional)>0) { ?>
					<div class="btn-circle btn-circle-red btn-circle-30" onclick="save_pieza_material_adicional('<?php echo $ma->idml;?>','<?php echo $pieza->idpi;?>',this,'red','30')"></div>
				<?php }else{ ?>
					<div class="btn-circle btn-circle-default btn-circle-30" onclick="save_pieza_material_adicional('<?php echo $ma->idml;?>','<?php echo $pieza->idpi;?>',this,'red','30')"></div>
				<?php }?>
					</td>
				</tr>
		<?php
		}?>
			</tbody>
		</table>
	</div>
</div>
<script language='javascript'>$('[data-toggle="popover"]').popover({html:true});</script>