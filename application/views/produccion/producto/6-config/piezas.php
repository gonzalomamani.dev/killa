<?php 
	//$res=$producto[0];
	$producto=$producto[0];
	$url_prod=base_url().'libraries/img/pieza_productos/miniatura/'; 
	$url_pieza=base_url().'libraries/img/piezas/miniatura/';
	$url_material=base_url().'libraries/img/materiales/miniatura/';
	$help1='title="<h4>Grupo de piezas<h4>" data-content="Adicionar nuevo grupo de piezas, cada grupo debe poseer caracteristicas similares, es decir todas las piezas del grupo deben tener un mismo material con el que se elabora, <b>por ejemplo si dos o mas piezas son hechas con el mismo material entonces deben de ir las piezas juntas en un mismo grupo</b>"';
	$popover2='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$popover3='data-toggle="popover" data-placement="top" data-trigger="hover"';
?>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="config('<?php echo $producto->idp; ?>')">Modificar</a></li>
  <li role="presentation"><a href="javascript:" onclick="materiales('<?php echo $producto->idp; ?>')">Materiales</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="piezas('<?php echo $producto->idp; ?>')">Piezas</a></li>
  <li role="presentation"><a href="javascript:" onclick="procesos('<?php echo $producto->idp; ?>')">Procesos y tiempo</a></li>
</ul><hr>
<div class="row">
	<?php 	$swap=$this->M_categoria_producto->get_portada_producto($producto->idp);
			$img="default.png";
			if(count($swap)>0){
				$im=$this->M_imagen_producto->get_pieza_material($swap[0]->idpim);
				if(!empty($im)){$img=$im[0]->nombre;}
			}
	?>
	<div class="col-md-2 col-sm-2 col-xs-4"><img src="<?php echo $url_prod.$img;?>" class="img-thumbnail"></div>
	<div class="col-md-10 col-sm-10 col-xs-8"><div class="table-responsive">
		<table class="table table-bordered table-striped">
			<tbody>
				<tr><th width="10%">Código:</th><td width="90%"><?php echo $producto->cod;?></td></tr>
				<tr><th>Nombre:</th><td><?php echo $producto->nombre;?></td></tr>
				<tr><th>Descripción:</th><td><?php echo $producto->descripcion;?></td></tr>
			</tbody>
		</table>
	</div></div>
</div><hr>
<div class="row row-border">
	<div class="col-xs-12 text-center"><strong><ins>DETALLE DE PIEZAS</ins></strong></div>
</div>
<?php 
for($i=0; $i < count($categorias_pieza) ; $i++){ $grupo=$categorias_pieza[$i]; 
	$materiales=$this->M_categoria_producto->get_categoria_grupo($grupo->idcp);
	$piezas=$this->M_pieza->get_pieza_categoria($grupo->idcp);
?>
		<div class="row row-border">
			<div class="col-xs-12 row-border">
				<div class="col-sm-1 col-sm-offset-10 col-xs-2 col-xs-offset-8" style="text-align:center">
					<?php if($grupo->color_producto==0){ ?>
						<div class="btn-circle btn-circle-default btn-circle-30" onclick="confirmar_save_color_producto('<?php echo $grupo->idcp;?>','<?php echo $producto->idp;?>')"></div>
					<?php }else {?>
						<div class="btn-circle btn-circle-success btn-circle-30" onclick="confirmar_reset_color_producto('<?php echo $grupo->idcp;?>','<?php echo $producto->idp;?>')"></div>
					<?php }?>
				</div>
				<div class="col-sm-1 col-xs-2">
					<?php $this->load->view('estructura/botones/botones_registros',['configuracion'=>'','eliminar'=>"confirmar_grupo('".$producto->idp."','".$grupo->idcp."')"]);?>
				</div><hr>
			</div>
			<div class="col-md-6 col-xs-12">
				<div class="table-responsive g-table-responsive">
					<table class="table table-bordered table-hover">
						<thead>
							<tr><th colspan="5" class="text-center">PIEZAS</th></tr>
							<tr>
								<th class="g-thumbnail-modal"><div class="g-img-modal">#Item</div></th>
								<th style="width:61%">Nombre</th>
								<th style="width:15%">Cantidad</th>
								<th style="width:11%"></th>
								<th style="width:13%"></th>
							</tr>
						</thead>
						<tbody>
						<?php if(!empty($piezas)){ 
							for ($p=0; $p < count($piezas) ; $p++) { $pieza=$piezas[$p];
								$img="default.png";
								if($pieza->imagen!='' && $pieza->imagen!=NULL){ $img=$pieza->imagen;} 
						?>
							<tr class="fila">
								<td><div id="item"><?php echo $p+1;?></div><img src="<?php echo $url_pieza.$img?>" width="100%" class="img-thumbnail g-thumbnail-modal"></td>
								<?php $desc=""; if($pieza->descripcion!="" || $pieza->descripcion!=NULL){ $desc="(".$pieza->descripcion.")";}?>
								<td><?php echo $pieza->nombre_grupo." ".$desc.' <br><b>'.$pieza->alto.' X '.$pieza->ancho.' [cm]</b>';?></td>
								<td><?php echo $pieza->unidades.' u.';?></td>
								<td><div class="btn-group btn-group-sm btn-group-justified">
									<a href="javascript:" class="btn btn-warning" onclick="add_proceso_material_adicional('<?php echo $pieza->idpi;?>')" title='Adicionar procesos y materiales liquidos a la pieza'><span class="icon-box-add"></span></a>	
								</div></td>
								<td><?php $this->load->view("estructura/botones/botones_registros",['configuracion'=>"modificar_pieza('".$pieza->idpi."','".$grupo->idcp."','".$producto->idp."')","eliminar"=>"confirmar_pieza('".$producto->idp."','".$pieza->idpi."')"]);?></td>
							</tr>
						<?php }// end for
							}// end if ?>
						</tbody>
						<thead>
							<tr>
								<th colspan="4" class="text-center">Adicionar pieza al grupo:</th>
								<th><?php $this->load->view('estructura/botones/buscador',['nuevo'=>'Adicionar nueva pieza','f_nuevo'=>"new_pieza('".$producto->idp."','".$grupo->idcp."')"]);?></th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<div class="col-md-6 col-xs-12">
				<div class="table-responsive g-table-responsive">
					<table class="table table-bordered table-hover">
						<thead><tr><th colspan="6" class="text-center">MATERIAL(ES)</th></tr></thead>
						<thead>
							<tr>
								<th class="celda g-thumbnail-modal"><div class="g-img-modal">#Item</div></th>
								<th style="width:70%">Nombre</th>
								<th width="30%">Color del producto</th>
						<?php if($grupo->color_producto==1){ ?>
								<th class="g-thumbnail-modal"><div class="g-img-modal">Imagen</div></th>
						<?php } ?>
								<th style="<?php if($grupo->color_producto==1){ echo 'width:15%';}else{ echo 'width:10%';} ?>"></th>
						<?php if($grupo->color_producto==1){ ?>
								<th style="width:10%"></th>
						<?php }?>
							</tr>
						</thead>
						<tbody>
				<?php 
					for($m=0; $m < count($materiales); $m++){ $material=$materiales[$m];
						$img="default.png";
						if($material->fotografia!='' && $material->fotografia!=NULL){ $img=$material->fotografia;}
				?>
							<tr>
								<td><div id="item"><?php echo $m+1;?></div><img src="<?php echo $url_material.$img;?>" class='img-thumbnail g-thumbnail-modal'></td>
								<td><?php echo $material->nombre;?><br><strong style='color: <?php echo $material->codigo_c;?>'><?php echo $material->nombre_c;?></strong></td>
								<td><select id="" class="form-control input-sm" onchange="update_color_pieza('<?php echo $producto->idp;?>','<?php echo $material->idpim;?>',this.value);">
									<option value="">Seleccionar....</option>
								<?php for ($co=0; $co < count($colores) ; $co++) { $color=$colores[$co]; ?>
									<option value="<?php echo $color->idco;?>" <?php if($material->idco2==$color->idco){ echo "selected";}?>><?php echo $color->nombre;?></option>
								<?php }?>
								</select></td>
						<?php if($grupo->color_producto==1){ 
								$swap_pi=$this->M_imagen_producto->get_pieza_material($material->idpim);//sacando datos de imagenes
								$img="default.png";
								if(count($swap_pi)>0){ $img=$swap_pi[0]->nombre;}
							?>
								<td><div id="item"><?php echo $m+1;?></div><img src="<?php echo $url_prod.$img;?>" class='img-thumbnail g-thumbnail-modal'></td>
						<?php }?>
						<?php $fun_config=""; if($grupo->color_producto==1){ $fun_config="add_imagen_pieza_material('".$producto->idp."','".$material->idpim."')"; }?>
								<td><?php $this->load->view("estructura/botones/botones_registros",['configuracion'=>$fun_config,"eliminar"=>"confirmar_material_grupo('".$material->idpim."','".$producto->idp."')"]);?></td>
						<?php if($grupo->color_producto==1){ ?>
								<td>
							<?php if($material->portada==0){?>
								<div class="btn-circle btn-circle-default btn-circle-30" onclick="save_portada('<?php echo $producto->idp;?>','<?php echo $material->idpim;?>','<?php echo $grupo->idcp;?>')"></div>
							<?php }else {?>
								<div class="btn-circle btn-circle-info btn-circle-30" onclick="reset_portada('<?php echo $producto->idp;?>','<?php echo $material->idpim;?>','<?php echo $grupo->idcp;?>')"></div>
							<?php }?>
								</td>
						<?php }?>
							</tr>
				<?php } ?>	
						</tbody>
						<thead>
							<tr>
								<th colspan="<?php if($grupo->color_producto==1){ echo '5';}else{ echo '3';} ?>" class="text-center">Adicionar material al grupo:</th>
								<th><?php $this->load->view('estructura/botones/buscador',['nuevo'=>'Adicionar nuevo material al grupo de piezas','f_nuevo'=>"add_material_grupo('".$producto->idp."','".$grupo->idcp."')"]);?></th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<div class="col-xs-12"><hr></div>

		</div>
<?php }// end for ?>
<div class="row row-border">
	<div class="col-md-2 col-md-offset-5 col-sm-3 col-sm-offset-5 col-xs-6 col-xs-offset-3">
		<?php $this->load->view('estructura/botones/buscador',['nuevo'=>'Adicionar nuevo material','popover_n'=>$popover3.$help1,'f_nuevo'=>"adicionar_grupo('".$producto->idp."')"]);?>
	</div>
	<div class="col-md-5 col-sm-4 col-xs-3"></div><hr>
</div>
<script language='javascript'>$('[data-toggle="popover"]').popover({html:true});</script>