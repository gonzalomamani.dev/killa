<?php 
	$producto=$producto[0];
	$url_prod=base_url().'libraries/img/pieza_productos/miniatura/'; 
	$url_mat=base_url().'libraries/img/materiales/miniatura/';
	$url_mat_ind=base_url().'libraries/img/materiales_adicionales/miniatura/'; 
?>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="config('<?php echo $producto->idp; ?>')">Modificar</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="materiales('<?php echo $producto->idp; ?>')">Materiales</a></li>
  <li role="presentation"><a href="javascript:" onclick="piezas('<?php echo $producto->idp; ?>')">Piezas</a></li>
  <li role="presentation"><a href="javascript:" onclick="procesos('<?php echo $producto->idp; ?>')">Procesos y tiempo</a></li>
</ul><hr>
<div class="row">
	<?php 	$swap=$this->M_categoria_producto->get_portada_producto($producto->idp);
			$img="default.png";
			if(count($swap)>0){
				$im=$this->M_imagen_producto->get_pieza_material($swap[0]->idpim);
				if(!empty($im)){$img=$im[0]->nombre;}
			}
	?>
	<div class="col-md-2 col-sm-2 col-xs-4"><img src="<?php echo $url_prod.$img;?>" class="img-thumbnail"></div>
	<div class="col-md-10 col-sm-10 col-xs-8"><div class="table-responsive">
		<table class="table table-bordered table-striped">
			<tbody>
				<tr><th width="10%">Código:</th><td width="90%"><?php echo $producto->cod;?></td></tr>
				<tr><th>Nombre:</th><td><?php echo $producto->nombre;?></td></tr>
				<tr><th>Descripción:</th><td><?php echo $producto->descripcion;?></td></tr>
			</tbody>
		</table>
	</div></div>
</div><hr>
<div class="row">
<div class="col-md-6 col-xs-12">
	<div class="table-responsive g-table-responsive">
	<table class="table table-bordered table-striped table-hover">
		<thead>
			<th colspan="4" class="text-center" width="85%">MATERIALES</th>
			<th><?php $this->load->view('estructura/botones/buscador',['nuevo'=>'Adicionar nuevo material','f_nuevo'=>"add_material_producto('".$producto->idp."')"]);?></th>
		</thead>
	<?php if(count($materiales)>0){?>
		<thead>
			<tr>
				<th class="g-thumbnail-modal"><div class="g-img-modal">#Item</div></th>
				<th class="celda-sm-50">Nombre</th>
				<th class="celda-sm-20">Cantidad</th>
				<th style="width:15%">Unid.</th>
				<th style="width:15%"></th>
			</tr>
		</thead>
		<tbody>
	<?php for ($i=0; $i < count($materiales); $i++) { $material=$materiales[$i];
			$img="default.png";
			if($material->fotografia!=NULL && $material->fotografia!=""){ $img=$material->fotografia;}
		?>
			<tr>
				<td><div id="item"><?php echo $i+1;?></div><img src="<?php echo $url_mat.$img?>" width="100%" class="img-thumbnail g-thumbnail-modal"></td>
				<td><?php echo $material->nombre;?></td>
				<td><form onsubmit="return update_cantidad_material('<?php echo $material->idpm;?>','<?php echo $producto->idp;?>')"><input type="number" class='form-control input-sm' min='0' step='any' id="can_m<?php echo $material->idpm;?>" value='<?php echo $material->cantidad;?>' placeholder='Ingrese Cantidad'></form></td>
				<td><?php echo $material->nombre_unidad;?></td>
				<td>
					<?php $this->load->view("estructura/botones/botones_registros",['guardar'=>"update_cantidad_material('".$material->idpm."','".$producto->idp."')",'eliminar'=>"alerta_material('".$material->idpm."','".$producto->idp."','".$material->nombre."','".$url_mat.$img."')"]);?>
				</td>
			</tr>
	<?php }?>
		</tbody>	
		<thead>
			<th colspan="4"></th>
			<th><?php $this->load->view('estructura/botones/buscador',['nuevo'=>'Adicionar nuevo material','f_nuevo'=>"add_material_producto('".$producto->idp."')"]);?></th>
		</thead>
	<?php }// end if?>
	</table>
	</div>
</div>
<div class="col-md-6 col-xs-12">
	<div class="table-responsive g-table-responsive">
	<table class="table table-bordered table-striped table-hover">
		<thead>
			<th colspan="4" class="text-center" width="85%">MATERIALES INDIRECTOS</th>
			<th><?php $this->load->view('estructura/botones/buscador',['nuevo'=>'Adicionar nuevo material indirecto','f_nuevo'=>"add_indirecto_producto('".$producto->idp."')"]);?></th>
		</thead>
	<?php if(count($materiales_indirectos)>0){?>
		<thead>
			<tr>
				<th class="g-thumbnail-modal"><div class="g-img-modal">#Item</div></th>
				<th class="celda-sm-50">Nombre</th>
				<th class="celda-sm-20">Costo (Bs.)</th>
				<th style="width:15%">Unid.</th>
				<th style="width:15%"></th>
			</tr>
		</thead>
		<tbody>
	<?php for ($i=0; $i < count($materiales_indirectos); $i++) { $material=$materiales_indirectos[$i];
			$img="default.png";
			if($material->fotografia!=NULL && $material->fotografia!=""){ $img=$material->fotografia;}
		?>
			<tr>
				<td><div id="item"><?php echo $i+1;?></div><img src="<?php echo $url_mat_ind.$img?>" width="100%" class="img-thumbnail g-thumbnail-modal"></td>
				<td><?php echo $material->nombre;?></td>
				<td><form onsubmit="return update_costo_indirecto('<?php echo $material->idpma;?>','<?php echo $producto->idp;?>')"><input class='form-control input-sm' type="number" step='any' min='0' max='9999.9999' id="cos_mi<?php echo $material->idpma;?>" value='<?php echo $material->costo;?>' placeholder='Ingrese Cantidad'></form></td>
				<td><?php echo $material->nombre_unidad;?></td>
				<td>
					<?php $this->load->view("estructura/botones/botones_registros",['guardar'=>"update_costo_indirecto('".$material->idpma."','".$producto->idp."')",'eliminar'=>"alerta_indirecto('".$material->idpma."','".$producto->idp."','".$material->nombre."','".$url_mat_ind.$img."')"]);?>
				</td>
			</tr>
	<?php }?>
		</tbody>	
		<thead>
			<th colspan="4"></th>
			<th><?php $this->load->view('estructura/botones/buscador',['nuevo'=>'Adicionar nuevo material','f_nuevo'=>"add_indirecto_producto('".$producto->idp."')"]);?></th>
		</thead>
	<?php }// end if?>
	</table>
	</div>	
</div>
</div>
