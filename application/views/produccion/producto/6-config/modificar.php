<?php
  $producto=$producto[0]; 
  $help1='title="<h4>Código de producto<h4>" data-content="Ingrese un código alfanumerico de 2 a 20 caracteres <b>sin espacios</b>, ademas el codigo solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
  $help2='title="<h4>Nombre de producto<h4>" data-content="Ingrese un nombre alfanumerico de 3 a 150 caracteres <b>puede incluir espacios</b>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
  $help3='title="<h4>Diseñador de producto<h4>" data-content="Ingrese un nombre de diseñador alfanumerico de 3 a 100 caracteres <b>puede incluir espacios</b>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
  $help4='title="<h4>Fecha de creación<h4>" data-content="Seleccione la fecha cuando se creo o finalizo el diseño del producto. en formato 2000-01-31"';
  $help5='title="<h4>Descripcion del producto<h4>" data-content="Ingrese un descripcion alfanumerica de 5 a 900 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas la descripcion solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
  $help51='title="<h4>Grupo del producto<h4>" data-content="Seleccione un grupo del producto, si desea adicionar nuevos grupos puede hacerlo el en menu superior <b>configuración</b> o puede dar click en el boton <b>+</b>, y buscar el grupo <b>PRODUCTO: Grupos.</b>"';
  $help6='title="<h4>Peso del producto<h4>" data-content="Ingrese el peso del producto en gramos, el sistema solo acepta valores hasta un maximo de 6 digitos enteros y 2 decimales es decir <b>valores entre 0 y 999999,99</b>"';
  $help7='title="<h4>Etiqueta de cuero<h4>" data-content="Ingrese las caracteristicas de la etiqueta de cuero si posee, se acepta texto alfanumerico hasta 150 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas el texto solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
  $help8='title="<h4>Correa<h4>" data-content="Ingrese las caracteristicas de la correa si posee, se acepta texto alfanumerico hasta 150 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas el texto solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
  $help9='title="<h4>Plaqueta Logo<h4>" data-content="Ingrese las caracteristicas del logo en tipo plaqueta si corresponde, se acepta texto alfanumerico hasta 150 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas el texto solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
  $help10='title="<h4>Repujado Logo<h4>" data-content="Ingrese las caracteristicas del logo en tipo repujado si corresponde, se acepta texto alfanumerico hasta 150 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas el texto solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
  $help11='title="<h4>Etiqueta de cuero Logo<h4>" data-content="Ingrese las caracteristicas del logo en tipo Etiqueta de cuero si corresponde, se acepta texto alfanumerico hasta 150 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas el texto solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
  $help12='title="<h4>Repujado<h4>" data-content="Ingrese las caracteristicas del repujado si corresponde, se acepta texto alfanumerico hasta 150 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas el texto solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
  $help13='title="<h4>Cierre delante<h4>" data-content="Ingrese las caracteristicas del cierre delantero si corresponde, se acepta texto alfanumerico hasta 150 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas el texto solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
  $help14='title="<h4>Cierre detras<h4>" data-content="Ingrese las caracteristicas del cierre tracero si corresponde, se acepta texto alfanumerico hasta 150 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas el texto solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
  $help15='title="<h4>Cierre dentro<h4>" data-content="Ingrese las caracteristicas del cierre interior si corresponde, se acepta texto alfanumerico hasta 150 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas el texto solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
  $help16='title="<h4>Alto del producto<h4>" data-content="Ingrese la altitud del producto en centímetros,  el sistema solo acepta valores hasta un maximo de 5 digitos enteros y 2 decimales es decir <b>valores entre 0 y 99999,99</b>"';
  $help17='title="<h4>Ancho del producto<h4>" data-content="Ingrese el ancho del producto en centímetros,  el sistema solo acepta valores hasta un maximo de 5 digitos enteros y 2 decimales es decir <b>valores entre 0 y 99999,99</b>"';
  $help18='title="<h4>Largo del producto<h4>" data-content="Ingrese el largo del producto en centímetros,  el sistema solo acepta valores hasta un maximo de 5 digitos enteros y 2 decimales es decir <b>valores entre 0 y 99999,99</b>"';
  $help19='title="<h4>Otras caracteristicas<h4>" data-content="Ingrese un otro tipo de caracteriticas relevantes del prodcuto, el texto acepta contenido alfanumerica hasta 900 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
  $popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<ul class="nav nav-tabs">
  <li role="presentation" class="active"><a href="javascript:" onclick="config('<?php echo $producto->idp; ?>')">Modificar</a></li>
  <li role="presentation"><a href="javascript:" onclick="materiales('<?php echo $producto->idp; ?>')">Materiales</a></li>
  <li role="presentation"><a href="javascript:" onclick="piezas('<?php echo $producto->idp; ?>')">Piezas</a></li>
  <li role="presentation"><a href="javascript:" onclick="procesos('<?php echo $producto->idp; ?>')">Procesos y tiempo</a></li>
</ul>
<div class="row"><div class="col-sm-2 col-sm-offset-10 col-xs-12"><strong><span class='text-danger'>(*)</span>Campos obligatorio</strong></div></div>
<div class="row row-border">
  <div class="col-sm-5 col-xs-12"><strong><ins>DETALLES GENERALES DEL PRODUCTO:</ins></strong></div><i class='clearfix'></i>
  <div class="col-md-1 col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span>Código:</strong></div>
  <div class="col-md-3 col-sm-4 col-xs-12">
  <form onsubmit="return update_producto('<?php echo $producto->idp;?>')">
    <div class="input-group col-xs-12">
      <input class='form-control input-sm'type="text" id="cod" placeholder='Código de Producto' maxlength="20" value="<?php echo $producto->cod;?>">
      <span class="input-group-addon input-sm" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
    </div>
  </form>
  </div>
  <div class="col-md-1 col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span>Nombre:</strong></div>
  <div class="col-md-3 col-sm-4 col-xs-12">
  <form onsubmit="return update_producto('<?php echo $producto->idp;?>')">
    <div class="input-group col-xs-12">
      <input class='form-control input-sm'type="text" id="nom" placeholder='Nombre de Producto' maxlength="150" value="<?php echo $producto->nombre;?>">
      <span class="input-group-addon input-sm" <?php echo $popover.$help2;?>><i class='glyphicon glyphicon-info-sign'></i></span>  
    </div>
  </form>
  </div><i class='visible-sm-block clearfix'></i>
  <div class="col-md-1 col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span>Diseñador:</strong></div>
  <div class="col-md-3 col-sm-4 col-xs-12">
  <form onsubmit="return update_producto('<?php echo $producto->idp;?>')">
    <div class="input-group col-xs-12">
      <input class='form-control input-sm'type="text"id="dis" placeholder="K'illa" maxlength="100" value="<?php echo $producto->disenador;?>">
      <span class="input-group-addon input-sm" <?php echo $popover.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
    </div>
  </form>
  </div><i class='visible-lg-block visible-md-block clearfix'></i>  
  <div class="col-md-1 col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span>Fecha de creación:</strong></div>
  <div class="col-md-3 col-sm-4 col-xs-12">
    <div class="input-group col-xs-12">
      <input class='form-control input-sm'type="date" id="fec" placeholder='2000-01-31' value="<?php echo $producto->fecha_creacion;?>">
      <span class="input-group-addon input-sm" <?php echo $popover.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
    </div>
  </div><i class='visible-sm-block clearfix'></i>
  <div class="col-md-1 col-sm-2 col-xs-12"><strong>Descripción:</strong></div>
  <div class="col-md-7 col-sm-10 col-xs-12">
    <div class="input-group col-xs-12">
      <textarea class='form-control input-sm' rows="2" placeholder='Descripcion de Producto' id="des" maxlength="900"><?php echo $producto->descripcion;?></textarea>
      <span class="input-group-addon input-sm" <?php echo $popover.$help5;?>><i class='glyphicon glyphicon-info-sign'></i></span>  
    </div>
  </div><i class='visible-md-block visible-lg-block clearfix'></i>
  <div class="col-md-1 col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span>Grupo:</strong></div>
  <div class="col-md-3 col-sm-4 col-xs-12">
  <form onsubmit="return update_producto('<?php echo $producto->idp;?>')">
    <div class="input-group col-xs-12">
      <select id="gru" class="form-control input-sm">
        <option value="">Seleccionar...</option>
      <?php for ($i=0; $i < count($grupos); $i++) { $grupo=$grupos[$i]; ?>
        <option value="<?php echo $grupo->idpg?>" <?php if($grupo->idpg==$producto->idpg){ echo "selected"; }?>><?php echo $grupo->nombre;?></option>
      <?php }?>
      </select>
      <a href="http://127.0.0.1/killaDeveloper/produccion?p=5" target="_blank" title="Ver Configuraciónes" class="input-group-addon input-sm"><i class="glyphicon glyphicon-plus"></i></a>
      <span class="input-group-addon input-sm" <?php echo $popover.$help51;?>><i class='glyphicon glyphicon-info-sign'></i></span>  
    </div>
  </form>
  </div><i class='visible-sm-block clearfix'></i>
</div>
<div class="row row-border">
<div class="col-sm-5 col-xs-12"><strong><ins>DETALLES TECNICOS DEL PRODUCTO:</ins></strong></div><i class='clearfix'></i>
  <div class="col-md-1 col-sm-2 col-xs-12"><strong>Peso[gramos]:</strong></div>
  <div class="col-md-3 col-sm-4 col-xs-12">
  <form onsubmit="return update_producto('<?php echo $producto->idp;?>')">
    <div class="input-group col-xs-12">
      <input class='form-control input-sm'type="number" id="peso" placeholder='0.00' min='0' max='999999.99' step='any' value="<?php echo $producto->peso;?>">
      <span class="input-group-addon input-sm" <?php echo $popover.$help6;?>><i class='glyphicon glyphicon-info-sign'></i></span>
    </div>
  </form>
  </div>
  <div class="col-md-1 col-sm-2 col-xs-12"><strong>Etiqueta de cuero:</strong></div>
  <div class="col-md-3 col-sm-4 col-xs-12">
    <div class="input-group col-xs-12">
      <textarea class='form-control input-sm'placeholder='Caracteristicas de etiqueta de cuero' id="etcu" maxlength="150"><?php echo $producto->etiquetaLogo;?></textarea>
      <span class="input-group-addon input-sm" <?php echo $popover.$help7;?>><i class='glyphicon glyphicon-info-sign'></i></span>  
    </div>
  </div><i class='visible-sm-block clearfix'></i>
  <div class="col-md-1 col-sm-2 col-xs-12"><strong>Correa:</strong></div>
  <div class="col-md-3 col-sm-4 col-xs-12">
    <div class="input-group col-xs-12">
      <textarea class='form-control input-sm'placeholder='Caracteristicas de etiqueta de cuero' id="corr" maxlength="150"><?php echo $producto->correa;?></textarea>
      <span class="input-group-addon input-sm" <?php echo $popover.$help8;?>><i class='glyphicon glyphicon-info-sign'></i></span>
    </div>
  </div><i class='visible-lg-block visible-md-block clearfix'></i>



  <div class="col-md-1 col-sm-2 col-xs-12"><strong>Plaqueta logo:</strong></div>
  <div class="col-md-3 col-sm-4 col-xs-12">
    <div class="input-group col-xs-12">
      <textarea class='form-control input-sm'placeholder='Caracteristicas del logo en la plaqueta' id="pllo" maxlength="150"><?php echo $producto->plaquetaLogo;?></textarea>      
      <span class="input-group-addon input-sm" <?php echo $popover.$help9;?>><i class='glyphicon glyphicon-info-sign'></i></span>
    </div>
  </div><i class='visible-sm-block clearfix'></i>
  <div class="col-md-1 col-sm-2 col-xs-12"><strong>Repujado logo:</strong></div>
  <div class="col-md-3 col-sm-4 col-xs-12">
    <div class="input-group col-xs-12">
      <textarea class='form-control input-sm'placeholder='Caracteristicas del logo de en el repujado' id="relo" maxlength="150"><?php echo $producto->repujadoLogo;?></textarea>
      <span class="input-group-addon input-sm" <?php echo $popover.$help10;?>><i class='glyphicon glyphicon-info-sign'></i></span> 
    </div>
  </div>
  <div class="col-md-1 col-sm-2 col-xs-12"><strong>Etiqueta de cuero logo:</strong></div>
  <div class="col-md-3 col-sm-4 col-xs-12">
    <div class="input-group col-xs-12">
      <textarea class='form-control input-sm'placeholder='Caracteriticas de logo en la etiqueta de cuero' id="etlo" maxlength="150"><?php echo $producto->etiquetaLogo;?></textarea>
      <span class="input-group-addon input-sm" <?php echo $popover.$help11;?>><i class='glyphicon glyphicon-info-sign'></i></span>
    </div>
  </div><i class='visible-lg-block visible-md-block visible-sm-block clearfix'></i>



  <div class="col-md-1 col-sm-2 col-xs-12"><strong>Repujado:</strong></div>
  <div class="col-md-3 col-sm-4 col-xs-12">
    <div class="input-group col-xs-12">
      <textarea class='form-control input-sm'placeholder='Caracteristicas del repujado' id="repu" maxlength="150"> <?php echo $producto->repujado;?></textarea>
      <span class="input-group-addon input-sm" <?php echo $popover.$help12;?>><i class='glyphicon glyphicon-info-sign'></i></span>
    </div>
  </div>
  <div class="col-md-1 col-sm-2 col-xs-12"><strong>Cierre delante:</strong></div>
  <div class="col-md-3 col-sm-4 col-xs-12">
    <div class="input-group col-xs-12">
      <textarea class='form-control input-sm'placeholder='con cierre delante' id="cidelante" maxlength="150"><?php echo $producto->cierreDelante;?></textarea>
      <span class="input-group-addon input-sm" <?php echo $popover.$help13;?>><i class='glyphicon glyphicon-info-sign'></i></span> 
    </div>
  </div><i class='visible-sm-block clearfix'></i>
  <div class="col-md-1 col-sm-2 col-xs-12"><strong>Cierrre detras:</strong></div>
  <div class="col-md-3 col-sm-4 col-xs-12">
    <div class="input-group col-xs-12">
      <textarea class='form-control input-sm'placeholder='con cierre detras' id="cidetras" maxlength="150"><?php echo $producto->cierreDetras;?></textarea>
      <span class="input-group-addon input-sm" <?php echo $popover.$help14;?>><i class='glyphicon glyphicon-info-sign'></i></span>
    </div>
  </div><i class='visible-lg-block visible-md-block clearfix'></i>



  <div class="col-md-1 col-sm-2 col-xs-12"><strong>Cierre dentro:</strong></div>
  <div class="col-md-3 col-sm-4 col-xs-12">
    <div class="input-group col-xs-12">
      <textarea class='form-control input-sm'placeholder='con cierre dentro' id="cidentro" maxlength="150"><?php echo $producto->cierreDentro;?></textarea>
      <span class="input-group-addon input-sm" <?php echo $popover.$help15;?>><i class='glyphicon glyphicon-info-sign'></i></span>
    </div>
  </div><i class='visible-sm-block clearfix'></i>
  <div class="col-md-1 col-sm-2 col-xs-12"><strong>Alto [cm]:</strong></div>
  <div class="col-md-3 col-sm-4 col-xs-12">
  <form onsubmit="return update_producto('<?php echo $producto->idp;?>')">
    <div class="input-group col-xs-12">
      <input class='form-control input-sm'type="number" placeholder='0.00' id="alto" min='0' max='99999.99' step='any' value="<?php echo $producto->alto;?>">
      <span class="input-group-addon input-sm" <?php echo $popover.$help16;?>><i class='glyphicon glyphicon-info-sign'></i></span> 
    </div>
  </form>
  </div>
  <div class="col-md-1 col-sm-2 col-xs-12"><strong>Ancho [cm]:</strong></div>
  <div class="col-md-3 col-sm-4 col-xs-12">
  <form onsubmit="return update_producto('<?php echo $producto->idp;?>')">
    <div class="input-group col-xs-12">
      <input class='form-control input-sm'type="number" placeholder='0.00'  id="ancho" min='0' max='99999.99' step='any' value="<?php echo $producto->ancho;?>">
      <span class="input-group-addon input-sm" <?php echo $popover.$help17;?>><i class='glyphicon glyphicon-info-sign'></i></span>
    </div>
  </form>
  </div><i class='visible-lg-block visible-md-block visible-sm-block clearfix'></i>
  

  <div class="col-md-1 col-sm-2 col-xs-12"><strong>Largo [cm]:</strong></div>
  <div class="col-md-3 col-sm-4 col-xs-12">
  <form onsubmit="return update_producto('<?php echo $producto->idp;?>')">
    <div class="input-group col-xs-12">
      <input class='form-control input-sm'type="number" placeholder='0.00' id="largo" min='0' max='99999.99' step='any' value="<?php echo $producto->largo;?>">
      <span class="input-group-addon input-sm" <?php echo $popover.$help18;?>><i class='glyphicon glyphicon-info-sign'></i></span>
    </div>
  </form>
  </div>
  <div class="col-md-1 col-sm-2 col-xs-12"><strong>Otras caracteristicas:</strong></div>
  <div class="col-md-7 col-sm-4 col-xs-12">
    <div class="input-group col-xs-12">
      <textarea class='form-control input-sm' rows="2" placeholder='Otras caracteristicas del producto' id="car" maxlength="900"><?php echo $producto->otra_caracteristica;?></textarea>
      <span class="input-group-addon input-sm" <?php echo $popover.$help19;?>><i class='glyphicon glyphicon-info-sign'></i></span> 
    </div>
  </div><i class='visible-lg-block visible-md-block clearfix'></i>
</div>
<script language='javascript'>Onfocus("cod");$('[data-toggle="popover"]').popover({html:true});</script>