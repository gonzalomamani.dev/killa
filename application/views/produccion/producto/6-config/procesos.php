<?php $producto=$producto[0]; $url_prod=base_url().'libraries/img/pieza_productos/miniatura/'; ?>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="config('<?php echo $producto->idp; ?>')">Modificar</a></li>
  <li role="presentation"><a href="javascript:" onclick="materiales('<?php echo $producto->idp; ?>')">Materiales</a></li>
  <li role="presentation"><a href="javascript:" onclick="piezas('<?php echo $producto->idp; ?>')">Piezas</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="procesos('<?php echo $producto->idp; ?>')">Procesos y tiempo</a></li>
</ul><hr>
<div class="row">
	<?php 	$swap=$this->M_categoria_producto->get_portada_producto($producto->idp);
			$img="default.png";
			if(count($swap)>0){
				$im=$this->M_imagen_producto->get_pieza_material($swap[0]->idpim);
				if(!empty($im)){$img=$im[0]->nombre;}
			}
	?>
	<div class="col-md-2 col-sm-2 col-xs-4"><img src="<?php echo $url_prod.$img;?>" class="img-thumbnail"></div>
	<div class="col-md-10 col-sm-10 col-xs-8"><div class="table-responsive">
		<table class="table table-bordered table-striped">
			<tbody>
				<tr><th width="10%">Código:</th><td width="90%"><?php echo $producto->cod;?></td></tr>
				<tr><th>Nombre:</th><td><?php echo $producto->nombre;?></td></tr>
				<tr><th>Descripción:</th><td><?php echo $producto->descripcion;?></td></tr>
			</tbody>
		</table>
	</div></div>
</div><hr>
<table class="table table-hover table-bordered">
	<thead><tr><th class="text-center"><ins>PROCESOS Y TIEMPO DE PRODUCCIÓN</ins></th></tr></thead>
</table>
<div class="table-responsive">
	<table class="table table-hover table-bordered">
	<?php if(count($procesos_pieza)>0 || count($producto_procesos)>0){ ?>
		<thead>
			<tr><th colspan="4" class="text-center">PROCESOS</th><th colspan="3" class="text-center">TIEMPO DE PRODUCCIÓN</th></tr>
			<tr><th class="text-center" width="7%">#Item</th><th class="text-center" width="15%">Proceso</th><th class="text-center" width="36%">Nombre de Pieza</th><th class="text-center" width="10%">Unid. por<br>producto</th>
			<th class="text-center" width="15%">Tiempo de producción<br><strong>H:m:s</strong></th><th class="text-center" width="10%">Costo de<br>producción</th><th width="6%"></th></tr>
		</thead>
		<tbody>
		<?php $c=1; for ($i=0; $i < count($procesos_pieza) ; $i++) { $proceso_pieza=$procesos_pieza[$i]; ?>
			<tr>
				<td class="text-center"><?php echo $c++;?></td>
				<td><?php echo $proceso_pieza->nombre_proceso; ?></td>
				<?php $desc=""; if($proceso_pieza->descripcion!="" || $proceso_pieza->descripcion!=NULL){ $desc="(".$proceso_pieza->descripcion.")";}?>
				<td><?php echo $proceso_pieza->nombre." ".$desc;?></td>
				<td><?php echo $proceso_pieza->unidades.'[unid.]';?></td>
				<?php $tiempo=explode(":",$this->lib->hms($proceso_pieza->tiempo_estimado));?>  
				<td class="text-center">
					<div style="width:177px">
						<form onsubmit="return save_tiempo_costo_pieza_proceso('<?php echo $producto->idp;?>','<?php echo $proceso_pieza->idpipr;?>');"><input type="number" class="form-control input-sm" min='0' max='9999' id="h1_<?php echo $proceso_pieza->idpipr;?>" placeholder='H' value='<?php if($tiempo[0]!="00"){ echo $tiempo[0];}?>' style='width:70px;float:left;'></form><strong style='float:left;'> : </strong>
						<form onsubmit="return save_tiempo_costo_pieza_proceso('<?php echo $producto->idp;?>','<?php echo $proceso_pieza->idpipr;?>');"><input type="number" class="form-control input-sm" min='0' max='59' id="m1_<?php echo $proceso_pieza->idpipr;?>" placeholder='m' value='<?php if($tiempo[1]!="00"){ echo $tiempo[1];}?>' style='width:50px; float:left;'></form><strong style='float:left;'>:</strong>
						<form onsubmit="return save_tiempo_costo_pieza_proceso('<?php echo $producto->idp;?>','<?php echo $proceso_pieza->idpipr;?>');"><input type="number" class="form-control input-sm" min='0' max='59' id="s1_<?php echo $proceso_pieza->idpipr;?>" placeholder='s' value='<?php if($tiempo[2]!="00"){ echo $tiempo[2];}?>' style='width:50px;float:left;'></form>
					</div>
				</td>
				<td class="text-center">
					<form onsubmit="return save_tiempo_costo_pieza_proceso('<?php echo $producto->idp;?>','<?php echo $proceso_pieza->idpipr;?>');"><input type="number" class="form-control input-sm" id="cos1_<?php echo $proceso_pieza->idpipr;?>" value="<?php echo $proceso_pieza->costo;?>" placeholder='0' min='0' max='9999.99' step='any'></form>
				</td>
				<td width="7%">
					<?php $this->load->view("estructura/botones/botones_registros",['guardar'=>"save_tiempo_costo_pieza_proceso('".$producto->idp."','".$proceso_pieza->idpipr."')"]);?>
				</td>
			</tr>
		<?php }//end for?>
		<?php for ($k=0; $k < count($producto_procesos) ; $k++) { $pm=$producto_procesos[$k]; ?>
			<tr>
				<td class="text-center"><?php echo $c++;?></td>
				<td>
					<select id="pro_<?php echo $pm->idppr;?>" class="form-control input-sm">
						<option value="">Seleccionar...</option>
						<?php for($i=0;$i<count($procesos);$i++){ $proceso=$procesos[$i]; ?>
						<option value="<?php echo $proceso->idpr;?>" <?php if($pm->idpr==$proceso->idpr){ echo "selected";}?>><?php echo $proceso->nombre;?></option>
						<?php }?>
					</select>
				</td>
				<td><textarea type="text" class="form-control input-sm" id="nom_<?php echo $pm->idppr;?>" placeholder='Nombre de proceso' maxlength="200"><?php echo $pm->sub_proceso;?></textarea></td>
				<td>-</td>
				<?php $tiempo=explode(":",$this->lib->hms($pm->tiempo_estimado));?>  
				<td class="text-center">
					<div style="width:177px">
						<form onsubmit="return save_tiempo_costo('<?php echo $producto->idp;?>','<?php echo $pm->idppr;?>');"><input type="number" class="form-control input-sm" min='0' max='9999' id="h2_<?php echo $pm->idppr;?>" placeholder='H' value='<?php if($tiempo[0]!="00"){ echo $tiempo[0];}?>' style='width:70px;float:left;'></form><strong style='float:left;'> : </strong>
						<form onsubmit="return save_tiempo_costo('<?php echo $producto->idp;?>','<?php echo $pm->idppr;?>');"><input type="number" class="form-control input-sm" min='0' max='59' id="m2_<?php echo $pm->idppr;?>" placeholder='m' value='<?php if($tiempo[1]!="00"){ echo $tiempo[1];}?>' style='width:50px; float:left;'></form><strong style='float:left;'>:</strong>
						<form onsubmit="return save_tiempo_costo('<?php echo $producto->idp;?>','<?php echo $pm->idppr;?>');"><input type="number" class="form-control input-sm" min='0' max='59' id="s2_<?php echo $pm->idppr;?>" placeholder='s' value='<?php if($tiempo[2]!="00"){ echo $tiempo[2];}?>' style='width:50px;float:left;'></form>
					</div>
				</td>
				<td class="text-center">
					<form onsubmit="return save_tiempo_costo('<?php echo $producto->idp;?>','<?php echo $pm->idppr;?>');"><input type="number" class="form-control input-sm" id="cos2_<?php echo $pm->idppr;?>" value="<?php echo $pm->costo;?>" placeholder='0' min='0' max='9999.99' step='any'></form>
				</td>
				<td width="7%">
					<?php $this->load->view("estructura/botones/botones_registros",['eliminar'=>"alerta_producto_proceso('".$producto->idp."','".$pm->idppr."')",'guardar'=>"save_tiempo_costo('".$producto->idp."','".$pm->idppr."')"]);?>
				</td>
			</tr>
		<?php }// end for?>
		</tbody>
	<?php }else{?>
		<thead><tr><th><h3>El producto no posee ningun proceso.</h3></th></tr></thead>
	<?php } ?>
	<thead>
		<tr>
			<th colspan="7" class="text-center"><ins>NUEVO PROCESO</ins></th>
		</tr>
		<tr>
			<td width="7%"></td>
			<td width="15%">
				<select id="pro" class="form-control input-sm">
					<option value="">Seleccionar...</option>
					<?php for($i=0;$i<count($procesos);$i++){ $material=$procesos[$i]; ?>
					<option value="<?php echo $material->idpr;?>"><?php echo $material->nombre;?></option>
					<?php }?>
				</select>
			</td>
			<td width="36%"><textarea type="text" class="form-control input-sm" id="nom" placeholder='Nombre de proceso' maxlength="200"></textarea></td>
			<td width="10%"> - </td>
			<td width="15%">
				<div style="width:177px">
					<form onsubmit="return save_producto_proceso('<?php echo $producto->idp;?>');"><input type="number" class="form-control input-sm" min='0' max='9999' id="h" placeholder='H' style='width:70px;float:left;'></form><strong style='float:left;'> : </strong>
					<form onsubmit="return save_producto_proceso('<?php echo $producto->idp;?>');"><input type="number" class="form-control input-sm" min='0' max='59' id="m" placeholder='m' style='width:50px; float:left;'></form><strong style='float:left;'>:</strong>
					<form onsubmit="return save_producto_proceso('<?php echo $producto->idp;?>');"><input type="number" class="form-control input-sm" min='0' max='59' id="s" placeholder='s' style='width:50px;float:left;'></form>
				</div>
			</td>
			<td width="10%">
				<form onsubmit="return save_producto_proceso('<?php echo $producto->idp;?>');"><input type="number" class="form-control input-sm" id="cos" placeholder='0' min='0' max='9999.99' step='any'></form>
			</td>
			<td width="6%">
				<?php $this->load->view("estructura/botones/botones_registros",['guardar'=>"save_producto_proceso('".$producto->idp."')",'eliminar'=>'']);?>				
			</td>
		</tr>
	</thead>
	</table>
</div>