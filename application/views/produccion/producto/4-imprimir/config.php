<div class="row-border table-responsive">
	<table class="table table-bordered"><thead>
		<tr>
			<th width="15%">Nro. Reg. por hojas</th>
			<th width="20%">
				<select id="nro" class="form-control input-sm">
				<?php for ($i=1; $i <= 50 ; $i++) { ?>
					<option value="<?php echo $i;?>" <?php if($i==34){echo "selected";}?>><?php echo $i;?></option>
				<?php } ?>
				</select>
			</th>
			<th width="65%"></th>
		</tr>
	</thead></table>
	<div id="config_area">
		<table class="tabla tabla-border-true">
			<tr class="fila">
				<th class="celda th" width="5%" >#Item</th>
				<th class="celda th" width="6%" >Fotografía</th>
				<th class="celda th" width="10%">Código</th>
				<th class="celda th" width="15%">Nombre</th>
				<th class="celda th" width="10%">Tipo de producto</th>
				<th class="celda th" width="8%">Colores</th>
				<th class="celda th" width="7%" >Fecha de creación</th>
				<th class="celda th" width="7%" >Diseñador</th>
				<th class="celda th" width="5%" >Costo promedio de producción (Bs.)</th>
				<th class="celda th" width="5%" >Costo promedio de venta (Bs.)</th>
				<th class="celda th" width="5%">Tiempo de proceso</th>
				<th class="celda th" width="17%">Descripción</th>
			</tr>
			<tr class="fila">
			<tr class="fila">
				<th class="celda th"><input type="checkbox" id="1" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="2" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="3" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="4" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="5" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="6" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="7" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="8" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="9" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="10" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="11" checked="checked"></th>
				<th class="celda th"><input type="checkbox" id="12" checked="checked"></th>
			</tr>
			</tr>
		</table>
	</div>
</div>
<div class="row-border table-responsive">
	<div id="area"></div>
</div>
<script language="javascript">
	$("#1").unbind("click");$("#1").change(function(){ arma_productos('<?php echo $productos;?>'); }); 
	$("#2").unbind("click");$("#2").change(function(){ arma_productos('<?php echo $productos;?>'); }); 
	$("#3").unbind("click");$("#3").change(function(){ arma_productos('<?php echo $productos;?>'); }); 
	$("#4").unbind("click");$("#4").change(function(){ arma_productos('<?php echo $productos;?>'); }); 
	$("#5").unbind("click");$("#5").change(function(){ arma_productos('<?php echo $productos;?>'); }); 
	$("#6").unbind("click");$("#6").change(function(){ arma_productos('<?php echo $productos;?>'); });
	$("#7").unbind("click");$("#7").change(function(){ arma_productos('<?php echo $productos;?>'); }); 
	$("#8").unbind("click");$("#8").change(function(){ arma_productos('<?php echo $productos;?>'); }); 
	$("#9").unbind("click");$("#9").change(function(){ arma_productos('<?php echo $productos;?>'); });
	$("#9").unbind("click");$("#10").change(function(){ arma_productos('<?php echo $productos;?>'); });
	$("#9").unbind("click");$("#11").change(function(){ arma_productos('<?php echo $productos;?>'); }); 
	$("#nro").unbind("click");$("#nro").change(function(){ arma_productos('<?php echo $productos;?>'); });
</script>