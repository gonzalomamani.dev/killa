<?php 
	$productos=json_decode($productos);
	$contPagina=1;
	$contReg=0;
	$url=base_url().'libraries/img/pieza_productos/miniatura/';
?>
<div class="pagina">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/sistema/killa.jpg'?>"></td>
				<td class="celda td" width="88%"><div class='encabezado_titulo'>REGISTRO DE PRODUCTOS</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'></span></div>
				</td></tr>
			</table>
		</div>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php if(!isset($v1)){?><th class="celda th" width="5%" >#Item</th><?php }?>
			<?php if(!isset($v2)){?><th class="celda th" width="6%" >Fotografía</th><?php }?>
			<?php if(!isset($v3)){?><th class="celda th" width="10%">Código</th><?php }?>
			<?php if(!isset($v4)){?><th class="celda th" width="15%">Nombre</th><?php }?>
			<?php if(!isset($v5)){?><th class="celda th" width="10%">Tipo de producto</th><?php }?>
			<?php if(!isset($v6)){?><th class="celda th" width="8%">Colores</th><?php }?>
			<?php if(!isset($v7)){?><th class="celda th" width="7%" >Fecha de creación</th><?php }?>
			<?php if(!isset($v8)){?><th class="celda th" width="7%" >Diseñador</th><?php }?>
			<?php if(!isset($v9)){?><th class="celda th" width="5%" >Costo promedio de producción (Bs.)</th><?php }?>
			<?php if(!isset($v10)){?><th class="celda th" width="5%" >Costo promedio de venta (Bs.)</th><?php }?>
			<?php if(!isset($v11)){?><th class="celda th" width="5%">Tiempo de proceso</th><?php }?>
			<?php if(!isset($v12)){?><th class="celda th" width="17%">Descripción</th><?php }?>
		</tr>
	<?php $i=1;
	foreach ($productos as $key => $producto){
		$img='default.png';
		/*--- Buscando imagen de producto ---*/
		$aux=$this->M_categoria_producto->get_portada_producto($producto->idp);
		if(count($aux)>0){
			$image=$this->M_imagen_producto->get_pieza_material($aux[0]->idpim); 
			if(count($image)>0){
				$img=$image[0]->nombre;
			}
		}
		/*--- End buscando imagen de producto ---*/
		/*--- Buscando colores y costo total por color ---*/
		$accesorios=$this->M_producto->get_accesorios($producto->idp);
		$materiales_liquidos=$this->M_producto->get_materiales_liquidos($producto->idp);
		$materiales=$this->M_producto->get_materiales($producto->idp);
		$indirectos=$this->M_producto->get_materiales_indirectos($producto->idp);
		$procesos=$this->M_producto->get_procesos($producto->idp);
		$procesos_pieza=$this->M_producto->get_procesos_pieza($producto->idp);
		$colores=json_decode($this->lib->costo_total_produccion_sc($accesorios,$materiales,$materiales_liquidos,$indirectos,$procesos,$procesos_pieza));
		$cont=0;
		$text_colors="";
		$costo_produccion=0;
		foreach($colores as $valor => $color) {
			$text_colors.=($color->color.", ");
			$costo_produccion+=number_format($color->costo_total,1,'.','');
			$cont++;
		}
		if($cont>0){$costo_produccion/=$cont;}
		$total_procesos=json_decode($this->lib->costo_procesos($procesos,$procesos_pieza));
		$total_segundos=0;
		foreach($total_procesos as $clave => $proceso){$total_segundos+=$proceso->segundos; }
		/*--- End buscando colores y costo total por color ---*/
		/*--- Buscando costo de ventas---*/
		$categorias_producto=$this->M_categoria_producto->get_categoria_producto_colores($producto->idp);
		$costo_venta=0;
		for ($cpr=0; $cpr < count($categorias_producto); $cpr++) { $categoria_producto=$categorias_producto[$cpr];
			$costos=$this->M_producto_cliente->get_row('idpim',$categoria_producto->idpim);
			$costo_p=0;
			if(count($costos)>0){
				for ($co=0; $co < count($costos) ; $co++) { $costo_p+=$costos[$co]->costo_unitario;}
				$costo_p=$costo_p/count($costos);
			}
			$costo_venta+=$costo_p;
		}
		if($contReg>=$nro){
			$contReg=0;
			$contPagina++;
	?>
			</table><!--cerramos tabla-->
		</div><!--cerramos pagina-->
	<div class="pagina">
		<div class="encabezado_pagina">
			<table class="tabla tabla-border-false"><tr class="fila">
				<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/fondo.png'?>"></td>
				<td class="celda td" width="88%"><div class='encabezado_titulo'>REGISTRO DE PRODUCTOS</div></td>
				<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
					<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m:s');?></span><br>
					<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
					<b>Página: </b><?php echo $contPagina.' de '?><span class='nroPagina'></span></div>
				</td></tr>
			</table>
		</div>
	<table class="tabla tabla-border-true">
		<tr class="fila">
			<?php if(!isset($v1)){?><th class="celda th" width="5%" >#Item</th><?php }?>
			<?php if(!isset($v2)){?><th class="celda th" width="6%" >Fotografía</th><?php }?>
			<?php if(!isset($v3)){?><th class="celda th" width="10%">Código</th><?php }?>
			<?php if(!isset($v4)){?><th class="celda th" width="15%">Nombre</th><?php }?>
			<?php if(!isset($v5)){?><th class="celda th" width="10%">Tipo de producto</th><?php }?>
			<?php if(!isset($v6)){?><th class="celda th" width="8%">Colores</th><?php }?>
			<?php if(!isset($v7)){?><th class="celda th" width="7%" >Fecha de creación</th><?php }?>
			<?php if(!isset($v8)){?><th class="celda th" width="7%" >Diseñador</th><?php }?>
			<?php if(!isset($v9)){?><th class="celda th" width="5%" >Costo promedio de producción (Bs.)</th><?php }?>
			<?php if(!isset($v10)){?><th class="celda th" width="5%" >Costo promedio de venta (Bs.)</th><?php }?>
			<?php if(!isset($v11)){?><th class="celda th" width="5%">Tiempo de proceso</th><?php }?>
			<?php if(!isset($v12)){?><th class="celda th" width="17%">Descripción</th><?php }?>
		</tr>
		<?php }//end if ?>
		<tr class="fila">
			<?php if(!isset($v1)){?><td class="celda td" ><?php echo $i++;?></td><?php } ?>
			<?php if(!isset($v2)){?><td class="celda td" ><img src="<?php echo $url.$img;?>" style='width:100%'></td><?php } ?>
			<?php if(!isset($v3)){?><td class="celda td" ><?php echo $producto->cod;?></td><?php } ?>
			<?php if(!isset($v4)){?><td class="celda td" ><?php echo $producto->nombre;?></td><?php } ?>
			<?php if(!isset($v5)){?><td class="celda td" ><?php echo $producto->nombre_grupo;?></td><?php } ?>
			<?php if(!isset($v6)){?><td class="celda td" ><?php echo $text_colors;?></td><?php } ?>
			<?php if(!isset($v7)){?><td class="celda td" ><?php echo $producto->fecha_creacion;?></td><?php } ?>
			<?php if(!isset($v8)){?><td class="celda td" ><?php echo $producto->disenador;?></td><?php } ?>
			<?php if(!isset($v9)){?><td class="celda td" ><?php if($costo_produccion>0){echo number_format($costo_produccion,1,'.',',');}?></td><?php } ?>
			<?php if(!isset($v10)){?><td class="celda td" ><?php if($costo_venta>0){echo number_format($costo_venta,1,'.',',');}?></td><?php } ?>
			<?php if(!isset($v11)){?><td class="celda td"><?php if($total_segundos>0){ echo $this->lib->hms($total_segundos);}?></td><?php } ?>
			<?php if(!isset($v12)){?><td class="celda td" ><?php echo $producto->descripcion;;?></td><?php } ?>
		</tr>
	<?php $contReg++;
	}// end for ?>
	</table>
</div>
<script type="text/javascript"> $(".nroPagina").html(<?php echo $contPagina;?>); </script>