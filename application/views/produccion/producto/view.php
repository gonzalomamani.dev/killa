<?php 
	$url=base_url().'libraries/img/pieza_productos/';
	$j_productos=json_encode($productos);
?>
<table class="table table-bordered table-striped table-hover">
	<thead>
	<tr>
		<th class="g-thumbnail">#Item</th>
		<th class="celda-sm-10">Código</th>
		<th class="celda-sm-30">Nombre</th>
		<th style="width:17%" class="celda-sm">Fecha de Creación</th>
		<th style="width:20%" class="celda-sm">Diseñador</th>
		<th style="width:15%" class="celda-sm">Colores Disponibles</th>
		<th style="width:8%"></th>
	</tr>
	</thead>
	<tbody>
<?php 
if(count($productos)>0){
	for($i=0; $i < count($productos); $i++){ $producto=$productos[$i]; 
			$img="default.png";
			/*--- Buscando imagen de producto ---*/
			$aux=$this->M_categoria_producto->get_portada_producto($producto->idp);
			if(count($aux)>0){
				$image=$this->M_imagen_producto->get_pieza_material($aux[0]->idpim); 
				if(count($image)>0){
					$img=$image[0]->nombre;
				}
			}
			/*--- End buscando imagen de producto ---*/
			/*--- Buscando colores de producto ---*/
			$cp=$this->M_categoria_producto->get_categoria_producto_colores($producto->idp);
			$colores = array();
			for ($co=0; $co < count($cp); $co++){
				$color=$this->M_color->get($cp[$co]->idco);
				$colores[count($colores)]=array('nombre' => $color[0]->nombre,'codigo' => $color[0]->codigo);
			}
			$colores=json_decode(json_encode($colores));
			/*--- End buscando colores de producto ---*/
		?> 
	<tr>
		<td class="g-thumbnail"><div id="item"><?php echo $i+1;?></div><div class="g-img">
			<a href="javascript:" onclick="fotografia('<?php echo $url.$img;?>')"><img src="<?php echo $url.'miniatura/'.$img;?>" class="img-thumbnail g-thumbnail"></a>
		</div></td>
		<td><?php echo $producto->cod; ?></td>
		<td><?php echo $producto->nombre." - ".$producto->nombre_grupo; ?>
		<?php foreach ($colores as $clave => $color) { ?>
			<span class="sm-cel" style="font-weight:bold; color: <?php echo $color->codigo?>"><br><?php echo $color->nombre;?></span>
		<?php } ?>
		</td>
		<td class="celda-sm"><?php echo $this->validaciones->formato_fecha($producto->fecha_creacion,'d-m-Y'); ?></td>
		<td class="celda-sm"><?php echo $producto->disenador; ?></td>
		<td class="celda-sm">
			<ul class="list-group" style="font-size:10px; padding: 1px">
			<?php foreach ($colores as $clave => $color) { ?>
				<li class="list-group-item" style="padding-top: 5px;padding-bottom: 5px;color:white; background:<?php echo $color->codigo;?>"><?php echo $color->nombre;?></li>
			<?php } ?>
			</ul>
		</td>
		<td>
			<?php $mod="config('".$producto->idp."')"; if($privilegio[0]->pr1u!="1"){ $mod="";}?>
			<?php $del="confirmar_producto('".$producto->idp."','".$url.$img."')"; if($privilegio[0]->pr1d!="1"){ $del="";}?>
			<?php $this->load->view('estructura/botones/botones_registros',['reportes'=>"reportes('".$producto->idp."')",'configuracion'=>$mod,'eliminar'=>$del]);?>
		</td>
	</tr>
	<?php }//end for
	}else{// end if 
		echo "<tr><td colspan='8'><h2>0 registros encontrados...</h2></td></tr>";
	}?>
	</tbody>
</table>
<script language="javascript" type="text/javascript"> 
	$("#print").removeAttr("onclick"); 
	$("#print").unbind("click"); 
	$("#print").click(function(){ imprimir_productos('<?php echo $j_productos;?>'); }); 
</script>