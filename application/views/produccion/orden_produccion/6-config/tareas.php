<?php 
if(count($productos)>0){
	$pedido=$this->M_pedido->get_complet($productos[0]->idpe);
	$ruta='libraries/img/pieza_productos/miniatura/';
	$estado = array('0' => 'En espera', '1' => 'En proceso', '2' => 'Finalzado');
?>
		<div class="list-group">
		  <div class="list-group-item active">
		    <h5 class="list-group-item-heading" id="g_text">DETALLE DEL PEDIDO</h5>
		  </div>
		  <div class="list-group-item">
		  	<div class="row">
		  		<div class="col-xs-12">
					<strong>Número de pedído: </strong><?php echo $pedido[0]->numero;?><br>
					<strong>Cliente: </strong><?php echo $pedido[0]->nombre;?><br>
					<strong>Fecha de  pedído: </strong><?php echo $this->lib->format_date($pedido[0]->fecha_pedido,'d ml Y');?><br>
					<strong>Fecha de entrega: </strong><?php echo $this->lib->format_date($pedido[0]->fecha_entrega,'d ml Y');?><br>
		  		</div>
		  		<div class="col-sm-3 col-xs-12">
		  			<select class="form-control input-sm" onchange="change_estado('<?php echo $pedido[0]->idpe;?>','1',this.value);">
		  				<option value="">Seleccionar</option>
		  		<?php foreach ($estado as $opcion => $valor) {?>
		  				<option value="<?php echo $opcion;?>" <?php if($pedido[0]->estado==$opcion){ echo "selected";}?>><?php echo $valor;?></option>
		  		<?php }?>
		  			</select>
		  		</div>
		  	</div>
		  </div>
		</div>
		<div class="list-group">
		  <div class="list-group-item active">
		    <h5 class="list-group-item-heading" id="g_text">PRODUCTOS</h5>
		  </div>
		  <div class="list-group-item">
		  	<?php $vidp=array(); 
		  		for($i=0; $i < count($productos) ; $i++){ $vidp[]=$productos[$i]->idp; }
		  		$vidp=array_unique($vidp);
		  		$productos_asignados=$this->M_empleado_producto->get_productos($idpe);
		  	?>
		  	<div class="table-responsive">
			  	<table class="table table-bordered table-striped table-hover">
			  		<thead>
			  			<tr>
			  				<th width="4%">#</th>
			  				<th ><div class="g-thumbnail"></div></th>
			  				<th width="90%"></th>
			  			</tr>
			  		</thead>
			  		<tbody>
			  		<?php $i=1; foreach($vidp as $clave => $idp){ ?>
			  			<tr>
			  				<td><?php echo $i++;?></td>
			  				<td class="g-thumbnail">
			  				<?php $img='default.png';$nombre="";
			  					for($dp=0; $dp < count($productos); $dp++){
			  						if($productos[$dp]->portada==1 && $idp==$productos[$dp]->idp){
			  							$s_img=$this->M_imagen_producto->get_col_prod($productos[$dp]->idpim,'nombre');
			  							$nombre=$productos[$dp]->nombre;
										if(count($s_img)>0){ $img=$s_img[0]->nombre;}
			  							break;
			  						}
			  					}
			  				?>
			  					<strong><?php echo $nombre;?></strong>
								<img src="<?php echo $ruta.$img;?>" alt="" class="img-thumbnail g-thumbnail">
							</td>
			  				<td>
			  					<table class="table table-bordered table-striped table-hover">
			  						<thead>
			  							<tr>
			  								<th width="15%">Color</th>
							  				<th width="5%">Unid.</th>
							  				<th width="40%" colspan="3">Asignar empleado</th>
							  				<th width="40%">Empleados asignados</th>
			  							</tr>
			  						</thead>
			  						<tbody>
			  					<?php for($dp=0; $dp < count($productos) ; $dp++) { $producto=$productos[$dp]; 
			  							if($producto->idp==$idp){
			  								$material=$this->M_material->get_material_atributo_idm($producto->idm,"color");
			  					?>
			  							<tr>
			  								<td><?php echo $material[0]->nombre_c;?></td>
			  								<td><?php echo $producto->cantidad;?></td>
			  								<td width="22%">
			  									<select id="emp_pr<?php echo $producto->idpim;?>" class="form-control input-sm">
			  										<option value="">Seleccionar....</option>
			  									<?php for ($e=0; $e < count($empleados); $e++) { $empleado=$empleados[$e]; ?>
			  										<option value="<?php echo $empleado->ide;?>"><?php echo $empleado->nombre." ".$empleado->nombre2." ".$empleado->paterno." ".$empleado->materno; ?></option>
			  									<?php }?>
			  									</select>
			  								</td>
			  								<td width="14%">
			  									<select id="tip_pr<?php echo $producto->idpim;?>" class="form-control input-sm">
			  										<option value="">Seleccionar....</option>
			  										<option value="0">Pieza</option>
			  										<option value="1">Producto</option>
			  									</select>
			  								</td>
			  								<td width="4%">
			  									<?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"asignar_empleado('".$idpe."','".$producto->idpim."','".$producto->iddp."')","eliminar"=>""]);?>
			  								</td>
			  								<td>
			  									<table class="table table-bordered table-hover table-striped">
			  										<tbody>
			  									<?php for ($pa=0; $pa < count($productos_asignados); $pa++) { 
			  											if($productos_asignados[$pa]->iddp==$producto->iddp){
			  									?>
			  											<tr>
			  												<td width="22%">
							  									<select id="emp_as<?php echo $productos_asignados[$pa]->idepr;?>" class="form-control input-sm" onchange="modificar_producto_empleado('<?php echo $productos_asignados[$pa]->idepr;?>','<?php echo $producto->idpe; ?>','<?php echo $producto->iddp;?>')">
							  									<?php for ($e=0; $e < count($empleados); $e++) { $empleado=$empleados[$e]; ?>
							  										<option value="<?php echo $empleado->ide;?>" <?php if($empleado->ide==$productos_asignados[$pa]->ide){ echo "selected";} ?>><?php echo $empleado->nombre." ".$empleado->nombre2." ".$empleado->paterno." ".$empleado->materno; ?></option>
							  									<?php }?>
							  									</select>
			  												</td>
			  												<td width="14%">
						  									<select id="tip_as<?php echo $productos_asignados[$pa]->idepr;?>" class="form-control input-sm" onchange="modificar_producto_empleado('<?php echo $productos_asignados[$pa]->idepr;?>','<?php echo $producto->idpe; ?>','<?php echo $producto->iddp;?>')">
						  										<option value="0" <?php if($productos_asignados[$pa]->tipo==0){ echo "selected";}?>>Pieza</option>
						  										<option value="1" <?php if($productos_asignados[$pa]->tipo==1){ echo "selected";}?>>Producto</option>
						  									</select>			  													
			  												</td>
			  												<td width="1%">
			  													<?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"","eliminar"=>"alerta_producto_empleado('".$productos_asignados[$pa]->idepr."','".$idpe."')"]);?>
			  												</td>
			  											</tr>
			  									<?php }
			  										}?>
			  										</tbody>
			  									</table>
			  								</td>
			  							</tr>
			  						
			  					<?php 	}//end if
			  						}?>
			  						</tbody>
			  					</table>
			  				</td>
			  			</tr>
			  		<?php
			  			}// end for ?>
			  		</tbody>
			  	</table>	  		
		  	</div>
		  </div>
		</div>
<?php
}else{

}
?>

