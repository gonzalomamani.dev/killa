<?php $estado = array('0' => 'En espera', '1' => 'En proceso', '2' => 'Finalzado');?>
<table class="table table-bordered table-hover">
	<thead>
		<tr>
			<th colspan="4"></th>
			<th>
				<select class="form-control input-sm" id="all_emp<?php echo $idpim;?>" onchange="save_tarea_all_empleado('<?php echo $idp;?>','<?php echo $idpr;?>','<?php echo $iddp;?>','<?php echo $tipo;?>','<?php echo $idpe;?>','<?php echo $idpim;?>','<?php echo $can;?>')">
					<option value="">Seleccionar...</option>
			<?php for ($e=0; $e < count($empleados); $e++) { $empleado=$empleados[$e]; ?>
					<option value="<?php echo $empleado->ide;?>"><?php echo $empleado->nombre." ".$empleado->nombre2." ".$empleado->paterno." ".$empleado->materno;?></option>
			<?php }?>
				</select>
			</th>
			<th>
			  	<select class="form-control input-sm" id="">
			  		<option value="">Seleccionar</option>
			  	<?php foreach ($estado as $opcion => $valor) {?>
			  		<option value="<?php echo $opcion;?>"><?php echo $valor;?></option>
			  	<?php }?>
			  	</select>
			</th>
			<th></th>
		</tr>
		<tr>
			<th width="5%">#</th>
			<th width="35%">Tarea</th>
			<th width="5%">Cant<br>p/unid.</th>
			<th width="5%">Cant<br>reque.</th>
			<th width="25%">Empleado encargado</th>
			<th width="15%">Orden de trabajo</th>
			<th width="10%"></th>
		</tr>
	</thead>
	<tbody>
<?php for($i=0; $i<count($tareas); $i++){ $tarea=$tareas[$i]; ?>
		<tr>
			<td><?php echo $i+1;?></td>
			<?php 
				$nombre_tarea="";
				$id="";
				if($tipo=="pip"){
					$grupo=$this->M_pieza_grupo->get($tarea->idpig);
					$material=$this->M_pieza->get_material($tarea->idpi);
					$nombre_tarea=$grupo[0]->nombre."<strong> de ".$material[0]->nombre."</strong> (".$tarea->nombre_pieza.")";
					$id=$tarea->idpipr;
				}
				if($tipo=="prp"){
					$nombre_tarea=$tarea->nombre_proceso."<strong> ".$tarea->sub_proceso."</strong>";
					$id=$tarea->idppr;
				}
			?>
			<td><?php echo $nombre_tarea; ?></td>
			<td><?php if($tipo=="pip"){echo $tarea->unidades.' u.';}else{echo "-";}?></td>
			<td><?php if($tipo=="pip"){echo $tarea->unidades*$can.' u.';}else{ echo $can.' u.';}?></td>
			<td>
			<?php $idtarea="";$fini="";?>
				<select class="form-control input-sm" id="emp<?php echo $idpim.'-'.$i;?>" onchange="save_tarea_empleado('<?php echo $id; ?>','<?php echo $iddp;?>','<?php echo $idpim;?>','<?php echo $i;?>')">
					<option value="">Seleccionar...</option>
			<?php $estado_tarea="NULL";
				for($e=0; $e<count($empleados); $e++){ $empleado=$empleados[$e];
					$selected="";
					for ($at=0; $at < count($all_tareas) ; $at++) { 
						if($tipo=="pip"){ if($all_tareas[$at]->ide==$empleado->ide && $tarea->idpipr==$all_tareas[$at]->idpipr){ $selected="selected";$idtarea=$all_tareas[$at]->idpt;$fini=$all_tareas[$at]->fecha_inicio; $estado_tarea=$all_tareas[$at]->estado; break;}}
						if($tipo=="prp"){ if($all_tareas[$at]->ide==$empleado->ide && $tarea->idppr==$all_tareas[$at]->idppr){ $selected="selected";$idtarea=$all_tareas[$at]->idpt;$fini=$all_tareas[$at]->fecha_inicio; $estado_tarea=$all_tareas[$at]->estado; break;}}
					}
			?>
				<option value="<?php echo $tipo.'|'.$empleado->ide;?>" <?php echo $selected;?>><?php echo $empleado->nombre." ".$empleado->nombre2." ".$empleado->paterno." ".$empleado->materno;?></option>
			<?php }?>
				</select>
			</td>
			<td>
			  	<select class="form-control input-sm" onchange="change_estado('<?php echo $idtarea;?>','3',this.value);">
			  		<option value="">Seleccionar</option>
			  	<?php foreach ($estado as $opcion => $valor) {?>
			  		<option value="<?php echo $opcion;?>" <?php if($estado_tarea==$opcion){ echo "selected";}?>><?php echo $valor;?></option>
			  	<?php }?>
			  	</select>
			</td>
			<td>
			<?php if($idtarea!=""){ 
					$fun="";
					if($fini!=""){ $fun="reset_date('".$idtarea."','".$idpim."','".$idp."','".$idpe."','". $iddp."','".$can."')";}
					$this->load->view('estructura/botones/botones_registros',['reset'=>$fun,'eliminar'=>"confirmar_tarea('".$idtarea."','".$idpim."','".$idp."','".$idpe."','". $iddp."','".$can."')"]);
			}?>
			</td>
		</tr>
<?php }?>
	</tbody>
</table>