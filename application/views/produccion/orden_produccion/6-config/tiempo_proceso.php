
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="view_orden_trabajo('<?php echo $detalles_pedido[0]->idpe; ?>')">Modificar</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="tiempo_proceso('<?php echo $detalles_pedido[0]->idpe; ?>')">Tiempos</a></li>
</ul>
<div class="list-group">
  <div class="list-group-item active">
    <h5 class="list-group-item-heading" id="g_text">TIEMPOS DE TRABAJO</h5>
  </div>
  <div class="list-group-item">
  	<div class="row">
  		<div class="col-xs-12">

  		</div>
  	</div>
  </div>
</div>


<div class="row row-border">
	<table class="table table-bordered table-striped">
		<thead>
		<tr>
			<th width="7%">#item</th>
			<th width="15%">Tareas</th>
			<th width="15%">Empleado</th>
			<th width="7%">C/Req.</th>
			<th width="10%">Fecha inicio</th>
			<th width="10%">Fecha fin</th>
			<th width="7%">Tiempo total</th>
			<th width="7%">Tiempo por unidad</th>
			<th width="7%">Costo total</th>
			<th width="7%">Costo por unidad</th>
		</tr>
		</thead>
		<tbody>
		<?php for ($i=0; $i < count($pedido_tareas) ; $i++) { $pedido_tarea=$pedido_tareas[$i]; 
			$proceso=$this->M_pieza_proceso->get_proceso($pedido_tarea->idpipr);
			?>
		<tr>
			<td><?php echo $i+1;?></td>
			<td><?php echo $proceso[0]->nombre_proceso.': '.$proceso[0]->nombre_pieza;?></td>
			<td>
			<?php $salario=0; $emp;
				for ($e=0; $e < count($empleados) ; $e++) { $empleado=$empleados[$e];
					if($empleado->ide==$pedido_tarea->ide){
						echo $empleado->nombre." ".$empleado->nombre2." ".$empleado->paterno." ".$empleado->materno;
						$emp=$empleado;
						$salario=$empleado->salario;
						break;
					}
			}  ?>
			</td>
			<td><?php echo $pedido_tarea->cantidad;?></td>
			<td><?php echo $pedido_tarea->fecha_inicio;?></td>
			<td><?php echo $pedido_tarea->fecha_fin;?></td>			
			<?php $horas=$this->M_hora_biometrico->exist_fecha('ide',$pedido_tarea->ide,$pedido_tarea->inicio,$pedido_tarea->fin);
					$result="";
				if($pedido_tarea->fecha_inicio!=NULL && $pedido_tarea->fecha_fin!=NULL){
					if(count($horas)>0 && count($horas)%2==0){
						//$hora=explode("|",$this->validaciones->hora_costo_proceso($pedido_tarea->fecha_inicio,$pedido_tarea->fecha_fin,$horas,$salario));
						$j_horas=json_encode($horas);
						$horas_antes=$this->M_hora_biometrico->horas_antes($pedido_tarea->ide,$pedido_tarea->fecha_inicio);
						$horas_despues=$this->M_hora_biometrico->horas_despues($pedido_tarea->ide,$pedido_tarea->fecha_fin);
						$result=json_decode(json_encode($this->lib->calculo_total_horas_trabajo($j_horas,$feriados,$emp,$pedido_tarea->fecha_inicio,$pedido_tarea->fecha_fin,$horas_antes,$horas_despues)));
					}
				}
			?>
			<td>
			<?php 
			//var_dump($result);
			if($result!=""){
						echo $result->horas_trabajadas." h";
					}else{
						//echo "no existe registro de hora biometrico o registro de horas con errores, revise las horas registradas por favor.";
						echo "Error!! en biometrico";
					}
				?>
			</td>


			<td><?php 
			if($result!=""){
				echo $result->horas_trabajadas/$pedido_tarea->cantidad.' h.';
			}?></td>
			<td><?php 
			if($result!=""){
				echo $result->sueldo;
			}?></td>
			<td><?php 
			if($result!=""){
				echo number_format($result->sueldo/$pedido_tarea->cantidad,1,'.',',').'Bs.';
			}
			?>
			</td>
		</tr>
		<?php }?>
		</tbody>
	</table>
</div>