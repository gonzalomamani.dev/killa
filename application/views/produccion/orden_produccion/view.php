<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th width="5%">#Item</th>
			<th width="5%">Nº de<br>pedido</th>
			<th width="20.5%">Cliente</th>
			<th width="24%" class="celda-sm">Fecha de Pedido</th>
			<th width="24%" class="celda-sm">Fecha de Entrega</th>
			<th width="8%">Cantidad de productos</th>
			<th width="7%">Estado de<br>producción</th>
		  	<th width='6.5%'></th>
	    </tr>
	</thead>
<tbody>
        <?php for ($i=0; $i < count($pedidos) ; $i++) { $pedido=$pedidos[$i]; ?>
    <tr>
	    <td><?php echo $i+1;?></td> 
		<td><?php echo $pedido->numero;?></td>
		<td><?php echo $pedido->nombre;?></td>
		<td class="celda-sm"><?php echo $this->validaciones->formato_fecha($pedido->fecha_pedido,'li,d,m,Y');?></td>
		<td class="celda-sm"><?php echo $this->validaciones->formato_fecha($pedido->fecha_entrega,'li,d,m,Y');?></td>
		<?php $detalle_pedido=$this->M_detalle_pedido->get_row('idpe',$pedido->idpe);
			$c_productos=0;for($dp=0; $dp < count($detalle_pedido); $dp++){ $c_productos+=($detalle_pedido[$dp]->cantidad);}
		?>
		<td><?php echo $c_productos.' Prod.';?></td>
		<td>
			<?php if($pedido->estado==0){?><span class="label label-danger label-lg">Pendiente</span><?php }?>
			<?php if($pedido->estado==1){?><span class="label label-warning label-lg">En producción</span><?php }?>
			<?php if($pedido->estado==2){?><span class="label label-success label-lg">Entregado</span><?php }?>
		</td>
		<td>
			<?php $mod="view_orden_trabajo('".$pedido->idpe."')"; if($privilegio[0]->pr2u!="1"){ $mod="";}?>
			<?php $this->load->view('estructura/botones/botones_registros',['reportes'=>"tiempo_tarea('".$pedido->idpe."')",'configuracion'=>$mod]);?>
		</td>
	</tr>
	<?php }?>
</tbody>
</table>  
