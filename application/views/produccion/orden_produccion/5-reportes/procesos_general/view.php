<?php
	if(count($productos)>0){
		$pedido=$productos[0];
	}
?>
<div class="col-sm-3 col-sm-offset-9 col-xs-12">
    <select id="emp" class="form-control input-sm" onchange="search_procesos_general('<?php echo $idpe;?>',true);">
        <option value="">Seleccionar...</option>
      <?php for ($i=0; $i < count($empleados); $i++) { $empleado=$empleados[$i];?>
        <option value="<?php echo $empleado->ide;?>" <?php if(isset($encargado)){ if($encargado->ide==$empleado->ide){ echo "selected";}}?>><?php echo $empleado->nombre." ".$empleado->nombre2." ".$empleado->paterno." ".$empleado->materno;?></option>
      <?php }?>
    </select>
</div>
<div class="col-xs-12">
  	<div id="area" class="table-responsive">
  		<table class="tabla tabla-border-true">
  			<tr class="fila"><th class="celda th" colspan="15">CONTROL DE PRODUCCION (<?php echo $this->lib->all_mayuscula($proceso->nombre)?> DE PRODUCTOS)</th></tr>
  			<tr class="fila">
  				<th class="celda th"></th>
  				<th class="celda th"><?php echo "PEDIDO: ".$pedido->numero;?></th>
  				<td class="celda td" colspan="2"></td>
  				<th class="celda th">Nombre:</th>
  				<td class="celda td" colspan="14"><?php if(isset($encargado)){ echo $encargado->nombre." ".$encargado->nombre2." ".$encargado->paterno." ".$encargado->materno; } ?></td>
  			</tr>
        <tr class="fila">
          <th class="celda th" width="3%" rowspan="2">#</th>
          <th class="celda th" width="21%" rowspan="2">Producto</th>
          <th class="celda th" width="4%" rowspan="2">Cant.</th>
          <th class="celda th" width="9%" rowspan="2">Fecha inicio</th>
          <th class="celda th" width="" colspan="3">Control 1</th>
          <th class="celda th" width="" colspan="3">Control 2</th>
          <th class="celda th" width="" colspan="3">Control 3</th>
          <th class="celda th" width="3%" rowspan="2">Tot</th>
        </tr>
        <tr class="fila">
          <th class="celda th" width="12%">Fecha y hora</th>
          <th class="celda th" width="4%">Cant.</th>
          <th class="celda th" width="4%">Ayu.</th>
          <th class="celda th" width="12%">Fecha y hora</th>
          <th class="celda th" width="4%">Cant.</th>
          <th class="celda th" width="4%">Ayu.</th>
          <th class="celda th" width="12%">Fecha y hora</th>
          <th class="celda th" width="4%">Cant.</th>
          <th class="celda th" width="4%">Ayu.</th>
        </tr>
  		<?php for ($i=0; $i < count($productos) ; $i++) { $producto=$productos[$i]; ?>
  			<tr class="fila">
          <td class="celda td"><?php echo $i+1;?></td>
  				<td class="celda td"><?php echo $this->lib->all_mayuscula($producto->nombre); ?></td>
  				<td class="celda td"><?php echo $producto->cantidad;?></td>
  				<td class="celda td"></td>
          <td class="celda td"></td>
          <td class="celda td"></td>
          <td class="celda td"></td>
          <td class="celda td"></td>
          <td class="celda td"></td>
          <td class="celda td"></td>
          <td class="celda td"></td>
          <td class="celda td"></td>
          <td class="celda td"></td>
          <td class="celda td"></td>
  			</tr>
  		<?php }// end for ?>
      <tr class="fila">
        <td class="celda td" colspan="14">
          OBS.:<p><br></p><p><br></p><p><br></p>
        </td>
      </tr>
  		</table>
  	</div>
</div>