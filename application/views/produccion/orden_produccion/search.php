<table class="tabla tabla-border-false">
	<tr class="fila">
		<td class='celda celda-sm-10'>
			<form onsubmit="return view_orden();"><input type="number" id="sv_num" placeholder='Número de pedido' class="form-control input-sm" onkeyup="reset_input(this.id)" min='0'></form>
		</td>
		<td class='celda celda-sm-20'>
			<select id="sv_cli" class="form-control input-sm" onchange="reset_input(this.id); view_orden();">
				<option value="">Seleccionar... Nombre o Razon Social</option>
		<?php for ($i=0; $i < count($clientes); $i++) { $cliente=$clientes[$i]; ?>
				<option value="<?php echo $cliente->idcl;?>"><?php echo $cliente->nombre;?></option>
		<?php }//end for ?>
			</select>
		</td>
		<td class='celda celda-sm' style="width:12%"><input type="date" id="sv_fpe" class="form-control input-sm" onchange="reset_input(this.id);"></td>
		<td class='celda celda-sm' style="width:12%"><input type="date" id="sv_fen" class="form-control input-sm" onchange="reset_input(this.id);"></td>
		<td class='celda celda-sm' style="width:15%"><label>p/pedido <input type="radio" name="sv_tip" value="0" checked=""></label></td>
		<td class='celda celda-sm' style="width:17%"><label>p/entrega <input type="radio" name="sv_tip" value="1"></label></td>
		<td class='celda celda-sm' style="width:5.5%"></td>
		<td class='celda' style="width:6.5%"><?php $this->load->view('estructura/botones/buscador',['f_buscar'=>"view_orden()",'f_ver'=>"all_orden()"]);?></td>
	</tr>
</table>