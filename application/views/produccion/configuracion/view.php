<?php
	$help1='title="<h4>Nombre de grupo<h4>" data-content="Ingrese un nombre del grupo <strong>de 2 a 100 caracteres con espacios</strong>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help2='title="<h4>Observación del grupo<h4>" data-content="Ingrese una observaciónes de grupo alfanumerico <strong>de 2 a 200 caracteres con espacios y sin saltos de linea</strong>, ademas la observación solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';

	$help3='title="<h4>Nombre de proceso<h4>" data-content="Ingrese un nombre del proceso <strong>de 2 a 100 caracteres con espacios</strong>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help4='title="<h4>Observación del proceso<h4>" data-content="Ingrese una observaciónes de proceso alfanumerico <strong>de 2 a 200 caracteres con espacios y sin saltos de linea</strong>, ademas la observación solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';

	$help5='title="<h4>Nombre de material liquido o recorrido<h4>" data-content="El nombre es obtenido deacuerdo al material seleccionado"';
	$help6='title="<h4>Variable de espesor y producto<h4>" data-content="Ingrese un valor numérico de 0 a 99.9999 [cm]. el valor del espesor responde a la formula de<b> [perimetro de la pieza*1*variable], donde el perimetro responde al [(alto+ancho)*2], 
	el valor 1 representa al ancho donde se esparcira el liquido y el valor de la VARIABLE es el alto aprox. del liquido que se usara en el contorno de la pieza.</b> Para el caso de los materiales de recorrido responde a la formula <b>[perimetro de la pieza*variable], donde la variable en este caso representa la extencion
	del material de recorrido</b> necesario en todo el contorno de la pieza."';
	$help7='title="<h4>Tipo de material<h4>" data-content="Existen dos tipo de materiales: <b>Liquido que representa principalmente a materiales liquidos necesarios detro la pieza como ser(clefa,pintura,etc),</b> se asume que el material liquido solo se usa en los contornos de la pieza, si usted cree que el valor de la clefa debe ser mayor solo debe incrementar el valor de la <b>variable</b> para poder apalear estos gastos.
	<b>Materiales de recorrido, son principalmente materiales que se usan en el controno de la pieza como ser (Hilo, etc.)</b>. De igual usted puede aumentar el valor de la variable si creee que debe de ingresar mas material por pieza."';

	$help8='title="<h4>Nombre de grupo<h4>" data-content="Ingrese un nombre del grupo para los productos <strong>de 2 a 100 caracteres con espacios</strong>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help9='title="<h4>Observación del grupo<h4>" data-content="Ingrese una observaciónes de grupo alfanumerico <strong>de 2 a 200 caracteres con espacios y sin saltos de linea</strong>, ademas la observación solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';

	$popover='data-toggle="popover" data-placement="right" data-trigger="hover"';
	$popover2='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$popover3='data-toggle="popover" data-placement="top" data-trigger="hover"';
?>
<div class="col-md-6 col-xs-12">
	<h3>PIEZA: Grupos</h3>
	<div class="table-responsive">
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
					<th width="5%">#</th>
					<th width="45%">
						<div class="input-group">
							<span class="input-group-addon" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
							<div class="form-control input-sm" disabled>Nombre</div>
						</div>
					</th>
					<th width="40%">
						<div class="input-group">
							<span class="input-group-addon" <?php echo $popover.$help2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
							<div class="form-control input-sm" disabled>Observaciónes</div>
						</div>
					</th>
					<th width="10%"></th>
				</tr>
			</thead>
			<tbody>
		<?php for ($i=0; $i<count($pieza_grupos); $i++){ $pg=$pieza_grupos[$i]; $c=$this->M_pieza->get_row('idpig',$pg->idpig); ?>
				<tr>
					<td><?php echo $i+1;?></td>
					<td><form onsubmit="return update_pieza_grupo('<?php echo $pg->idpig;?>')"><input class='form-control input-sm' type='text' value='<?php echo $pg->nombre;?>' id='nom_pg<?php echo $pg->idpig;?>' maxlength="100" placeholder='Nombre de Grupo'></form></td>
					<td><textarea class='form-control input-sm' id='des_pg<?php echo $pg->idpig;?>' maxlength="200" placeholder='Observación de Grupo'><?php echo $pg->descripcion;?></textarea></td>
					<?php $str="";
						if(count($c)>0){
							$str="<br><strong class='text-danger'>¡Imposible Eliminar el grupo, esta siendo usado por ".count($c)." Pieza(s)!</strong>";
							$fun="disabled";
						}else{
							$fun="alerta_pieza_grupo('".$pg->idpig."')";
						}
						$help='title="<h4>Detalles de grupo<h4>" data-content="<b>Nombre: </b>'.$pg->nombre.'<br><b>Descripción: </b>'.$pg->descripcion."<br>".$str.'"';
					?>
					<td><?php $this->load->view('estructura/botones/botones_registros',['detalle'=>$popover3.$help,'guardar'=>"update_pieza_grupo('".$pg->idpig."')",'eliminar'=>$fun]);?></td>
				</tr>
			<?php
		}?></tbody>
			<thead>
				<tr><th colspan="4" class="text-center"><ins>NUEVO</ins></th></tr>
				<tr>
					<td colspan="2">
						<form onsubmit="return save_pieza_grupo()">
							<div class="input-group col-xs-12">
								<span class="input-group-addon" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								<input class='form-control input-sm' type='text' placeholder='Nombre de Grupo' id="nom_pg" maxlength="100">
							</div>
						</form>
					</td>
					<td>
						<div class="input-group col-xs-12">
							<span class="input-group-addon" <?php echo $popover3.$help2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
							<textarea class='form-control input-sm' id="des_pg" placeholder='Observación de Grupo' maxlength="200"></textarea>
						</div>
					</td>
					<td><?php $this->load->view('estructura/botones/botones_registros',['detalle'=>"",'guardar'=>"save_pieza_grupo()",'eliminar'=>""]);?></td>
				</tr>
			</thead>
		</table>
	</div>
	<H3>PIEZA - PRODUCTO: Procesos</H3>
		<div class="table-responsive">
			<table class="table table-hover table-bordered">
				<thead>
					<tr>
						<th width="5%">#</th>
						<th width="45%">
							<div class="input-group">
								<span class="input-group-addon" <?php echo $popover.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								<div class="form-control input-sm" disabled>Nombre</div>
							</div>
						</th>
						<th width="40%">
							<div class="input-group">
								<span class="input-group-addon" <?php echo $popover.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								<div class="form-control input-sm" disabled>Observaciónes</div>
							</div>
						</th>
						<th width="10%"></th>
					</tr>
				</thead>
				<tbody>
				<?php for ($i=0; $i < count($procesos); $i++) { $proceso=$procesos[$i]; $c=$this->M_pieza_proceso->get_row('idpr',$proceso->idpr);?>
					<tr>
						<td><?php echo $i+1;?></td>
						<td>
							<form onsubmit="return update_proceso('<?php echo $proceso->idpr;?>')">
								<input class='form-control input-sm' type="text" value="<?php echo $proceso->nombre;?>" id="nom_p<?php echo $proceso->idpr;?>">
							</form>
						</td>
						<td><textarea class='form-control input-sm' placeholder='Observaciones' id="des_p<?php echo $proceso->idpr;?>"><?php echo $proceso->detalle;?></textarea></td>
						<?php $str="";
							if(count($c)>0){
								$str="<br><strong class='text-danger'>¡Imposible Eliminar el proceso, esta siendo usado por ".count($c)." Pieza(s)!</strong>";
								$fun="disabled";
							}else{
								$fun="alerta_proceso('".$proceso->idpr."')";
							}
							$help='title="<h4>Detalles de grupo<h4>" data-content="<b>Nombre: </b>'.$proceso->nombre.'<br><b>Descripción: </b>'.$proceso->detalle."<br>".$str.'"';
						?>
						<td><?php $this->load->view('estructura/botones/botones_registros',['detalle'=>$popover3.$help,'guardar'=>"update_proceso('".$proceso->idpr."')",'eliminar'=>$fun]);?></td>
					</tr>
				<?php } ?>
				</tbody>
				<thead>
					<tr>
						<th  colspan="4" class="text-center"><ins>NUEVO</ins></th>
					</tr>
					<tr>
						<td colspan="2">
							<form onsubmit="return save_proceso()">
								<div class="input-group col-xs-12">
									<span class="input-group-addon" <?php echo $popover.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
									<input class='form-control input-sm' type="text" id="nom_p" placeholder='Nombre de grupo' maxlength="100">
								</div>
							</form>
						</td>
						<td>
							<div class="input-group col-xs-12">
								<span class="input-group-addon" <?php echo $popover3.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								<textarea class='form-control input-sm' id="des_p" placeholder='Observaciones' maxlength="200"></textarea>
							</div>
						</td>
						<td><?php $this->load->view('estructura/botones/botones_registros',['detalle'=>"",'guardar'=>"save_proceso()",'eliminar'=>""]);?></td>
					</tr>
				</thead>
				</table>
			</div>
		</div>
		<div class="col-md-6 col-xs-12">
			<h3>PIEZA: Materiales Liquidos y de Recorrido</h3>
			<div class="table-responsive">
			<table class="table table-hover table-bordered">
				<thead>
				<tr>
					<th class="g-thumbnail-modal"><div class="g-img-modal">#Item</div></th>
					<th style="width:40%">
						<div class="input-group">
							<span class="input-group-addon" <?php echo $popover.$help5;?>><i class='glyphicon glyphicon-info-sign'></i></span>
							<div class="form-control input-sm" disabled>Nombre</div>
						</div>
					</th>
					<th style="width:20%">
						<div class="input-group">
							<span class="input-group-addon" <?php echo $popover.$help6;?>><i class='glyphicon glyphicon-info-sign'></i></span>
							<div class="form-control input-sm" disabled>Var.</div>
						</div>
					</th>
					<th style="width:30%">
						<div class="input-group">
							<span class="input-group-addon" <?php echo $popover.$help7;?>><i class='glyphicon glyphicon-info-sign'></i></span>
							<div class="form-control input-sm" disabled>Tipo</div>
						</div>
					</th>
					<th style="width:10%"></th>
				</tr>
				</thead>
				<tbody>
		<?php 
		$url=base_url().'libraries/img/materiales/miniatura/';
		for ($i=0; $i<count($materiales_liquidos); $i++){$ml=$materiales_liquidos[$i]; $c=$this->M_pieza_material_liquido->get_row('idml',$ml->idml);
			$material=$this->M_material->get($ml->idm);
			$img="default.png";
			if($material[0]->fotografia!=NULL && $material[0]->fotografia!=""){$img=$material[0]->fotografia; }
			?>
				<tr>
					<td><div id="item"><?php echo $i+1;?></div><img src="<?php echo $url.$img;?>" class="img-thumbnail g-thumbnail-modal"></td>
					<td><?php echo $material[0]->nombre;?></td>
					<td>
					<form onsubmit="return update_material_liquido('<?php echo $ml->idml;?>')">
						<input class='form-control input-sm' type='number' value='<?php echo $ml->variable;?>' id='den_<?php echo $ml->idml;?>' step='any'>
					</form>
					</td>
					<td>
						<?php $tipo_material="";?>
						<select class='form-control input-sm' id="tip_<?php echo $ml->idml;?>">
							<option value="">Seleccione...</option>
							<option value="0" <?php if($ml->tipo==0){ echo "selected";$tipo_material="Material Liquido";}?>>Material Liquido</option>
							<option value="1" <?php if($ml->tipo==1){ echo "selected";$tipo_material="Material de recorrido";}?>>Material de recorrido</option>
						</select>
					</td>
					<?php $str="";
						if(count($c)>0){
							$str="<br><strong class='text-danger'>¡Imposible Eliminar el grupo, esta siendo usado por ".count($c)." Pieza(s)!</strong>";
							$fun="disabled";
						}else{
							$fun="alerta_material_liquido('".$ml->idml."','".$material[0]->nombre."','".$url.$img."')";
						}
						$help='title="<h4>Detalles de grupo<h4>" data-content="<b>Nombre: </b>'.$material[0]->nombre.'<br><b>Variable: </b>'.$ml->variable."<br><b>Tipo: </b>".$tipo_material."<br>".$str.'"';
					?>
					<td><?php $this->load->view('estructura/botones/botones_registros',['detalle'=>$popover3.$help,'guardar'=>"update_material_liquido('".$ml->idml."')",'eliminar'=>$fun]);?></td>
				</tr>
			<?php
		}?></tbody><thead>
			<tr><th colspan="5">NUEVO</th></tr>
			<tr>
				<th colspan="4"></th>
				<th><?php $this->load->view('estructura/botones/buscador',['nuevo'=>'Adicionar Material','f_nuevo'=>'material_liquido()']);?></th>
			</tr>
			</thead>
			</table>
			</div>


	<H3>PRODUCTO: Grupos</H3>
		<div class="table-responsive">
			<table class="table table-hover table-bordered">
				<thead>
					<tr>
						<th width="5%">#</th>
						<th width="45%">
							<div class="input-group">
								<span class="input-group-addon" <?php echo $popover.$help8;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								<div class="form-control input-sm" disabled>Nombre</div>
							</div>
						</th>
						<th width="40%">
							<div class="input-group">
								<span class="input-group-addon" <?php echo $popover.$help9;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								<div class="form-control input-sm" disabled>Observaciónes</div>
							</div>
						</th>
						<th width="10%"></th>
					</tr>
				</thead>
				<tbody>
				<?php for ($i=0; $i < count($producto_grupos); $i++) { $producto_grupo=$producto_grupos[$i]; $c=$this->M_producto->get_row('idpg',$producto_grupo->idpg);?>
					<tr>
						<td><?php echo $i+1;?></td>
						<td>
							<form onsubmit="return update_producto_grupo('<?php echo $producto_grupo->idpg;?>')">
								<input class='form-control input-sm' type="text" value="<?php echo $producto_grupo->nombre;?>" id="nom_prg<?php echo $producto_grupo->idpg;?>">
							</form>
						</td>
						<td><textarea class='form-control input-sm' placeholder='Observaciones' id="des_prg<?php echo $producto_grupo->idpg;?>"><?php echo $producto_grupo->descripcion;?></textarea></td>
						<?php $str="";$fun="";
							if(count($c)>0){
								$str="<br><strong class='text-danger'>¡Imposible Eliminar el grupo, esta siendo usado por ".count($c)." producto(s)!</strong>";
								$fun="disabled";
							}else{
								$fun="alerta_producto_grupo('".$producto_grupo->idpg."')";
							}
							$help='title="<h4>Detalles de grupo<h4>" data-content="<b>Nombre: </b>'.$producto_grupo->nombre.'<br><b>Descripción: </b>'.$producto_grupo->descripcion."<br>".$str.'"';
						?>
						<td><?php $this->load->view('estructura/botones/botones_registros',['detalle'=>$popover3.$help,'guardar'=>"update_producto_grupo('".$producto_grupo->idpg."')",'eliminar'=>$fun]);?></td>
					</tr>
				<?php } ?>
				</tbody>
				<thead>
					<tr>
						<th  colspan="4" class="text-center"><ins>NUEVO</ins></th>
					</tr>
					<tr>
						<td colspan="2">
							<form onsubmit="return save_producto_grupo()">
								<div class="input-group col-xs-12">
									<span class="input-group-addon" <?php echo $popover.$help8;?>><i class='glyphicon glyphicon-info-sign'></i></span>
									<input class='form-control input-sm' type="text" id="nom_prg" placeholder='Nombre de Proceso' maxlength="100">
								</div>
							</form>
						</td>
						<td>
							<div class="input-group col-xs-12">
								<span class="input-group-addon" <?php echo $popover3.$help9;?>><i class='glyphicon glyphicon-info-sign'></i></span>
								<textarea class='form-control input-sm' id="des_prg" placeholder='Observaciones' maxlength="200"></textarea>
							</div>
						</td>
						<td><?php $this->load->view('estructura/botones/botones_registros',['detalle'=>"",'guardar'=>"save_producto_grupo()",'eliminar'=>""]);?></td>
					</tr>
				</thead>
				</table>
			</div>
		</div>
<script language='javascript'>$('[data-toggle="popover"]').popover({html:true});</script>