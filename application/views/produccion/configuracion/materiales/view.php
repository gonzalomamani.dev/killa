<?php 
	$help1='title="<h4>Imposible eliminar el material liquido<h4>" data-content="Solo es posible eliminar el material desde la parte de configuración."';
	$popover='data-toggle="popover" data-placement="right" data-trigger="hover"';
	$popover2='data-toggle="popover" data-placement="left" data-trigger="hover"';
	if(count($materiales)>0){
		$url=base_url().'libraries/img/materiales/miniatura/';
?>
	<div class="table-responsive g-table-responsive">
		<table class="table table-bordered table-hover">
			<thead>
				<th class="g-thumbnail-modal"><div class="g-img-modal">#Item</div></th>
				<th class="celda-sm-15">Código</th>
				<th style="width:40%">Nombre</th>
				<th style="width:10%" class="celda-sm">Color</th>
				<th style="width:15%">Variable</th>
				<th style="width:15%">Tipo de material</th>
				<th style="width:5%"></th>
			</thead>
			<?php for($i=0; $i < count($materiales) ; $i++) { $material=$materiales[$i];
					$img="default.png";
					if($material->fotografia!="" & $material->fotografia!=null){ $img=$material->fotografia; }
					//buscado en materiales liquidos
					$material_liquido=$this->M_material_liquido->get_row('idm',$material->idm);
			?>
					<tr>
						<td><div id="item"><?php echo $i+1;?></div><img src="<?php echo $url.$img;?>" width='100%' class='img-thumbnail g-thumbnail-modal'></td>
						<td><?php echo $material->codigo;?></td>
						<td><?php echo $material->nombre;?>
						<span class="sm-cel" style="font-weight:bold; color: <?php echo $material->codigo_c;?>"><br>(<?php echo $material->nombre_c;?>)</span></td>
						<td class="celda-sm">
							<ul class="list-group" style="font-size:10px; padding: 1px"><li class="list-group-item" style="padding-top: 5px;padding-bottom: 5px;color:white; background:<?php echo $material->codigo_c;?>"><?php echo $material->nombre_c;?></li></ul>
						</td>
						<td><input class='form-control input-sm' type='number' value='<?php if(!empty($material_liquido)){ echo $material_liquido[0]->variable;}?>' id='den<?php echo $material->idm.'_'.$ida;?>' placeholder="Variable"></td>
						<td>
							<select class='form-control input-sm' id="tip<?php echo $material->idm.'_'.$ida;?>">
								<option value="">Seleccione...</option>
								<option value="0" <?php if(!empty($material_liquido)){ if($material_liquido[0]->tipo=="0"){ echo "selected";}}?>>Material Liquido</option>
								<option value="1" <?php if(!empty($material_liquido)){ if($material_liquido[0]->tipo=="1"){ echo "selected";}}?>>Material de recorrido</option>
							</select>
						</td>
						<td>
					<?php 
						//$control=$this->M_categoria_producto->get_row_2n('idm',$material->idm,'idcp',$idcp);
						if(!empty($material_liquido)){
					?>
						<div class="btn-circle btn-circle-red btn-circle-30" <?php echo $popover2.$help1;?>></div>
					<?php }else{ ?>
						<div class="btn-circle btn-circle-default btn-circle-30" onclick="save_material_liquido('<?php echo $material->idm;?>','<?php echo $ida;?>',this,'red','30');" title="Seleccionar material"></div>
					<?php	}
					?>
						
						<!--<a href="javascript:" onclick="select_material('<?php echo $idp;?>','<?php echo $material->idm;?>')">Seleccionar</a>--></td>
					</tr>
					<?php
				}
			?>
		</table>
	</div>
		<?php
	}else{
		echo "<h3>0 registros encontrados...</h3>";
	}
?>
<script language='javascript'>$('[data-toggle="popover"]').popover({html:true});</script>

