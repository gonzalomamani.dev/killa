<?php
	$cargo=$this->session->userdata("cargo");
	if($this->session->userdata("nombre2")!=""){
		$usuario=$this->session->userdata("nombre")." ".$this->session->userdata("nombre2")." ".$this->session->userdata("paterno");
	}else{
		$usuario=$this->session->userdata("nombre")." ".$this->session->userdata("paterno");
	}
    $ci=$this->session->userdata("ci");			
?>						

<!DOCTYPE html>
<html lang="es" class="no-js">
	<head><?php $this->load->view('estructura/head',['title'=>'Cliente/Proveedor','css'=>'']);?></head>
	<body>
		<?php $this->load->view('estructura/modal');?>
		<div class="contenedor">
		<?php $this->load->view('estructura/menu_izq',['usuario'=>$usuario,'cargo'=>$cargo,'ventana'=>'cliente_proveedor','privilegio'=>$privilegio[0]]);?>
		<?php $v_menu="";
			if($privilegio[0]->cl1r==1){ $v_menu.="Clientes/cliente/icon-profile|"; }
			if($privilegio[0]->cl2r==1){ $v_menu.="Proveedores/proveedor/icon-addressbook|";}
			if($privilegio[0]->cl3r==1){ $v_menu.="Configuración/config/glyphicon glyphicon-cog";}
		?>
		<?php $this->load->view('estructura/menu_top',['usuario'=>$usuario,'cargo'=>$cargo,'menu'=>$v_menu]);?>
			<div id="search"></div>
			<div id="contenido"></div>
		</div>
		<?php $this->load->view('estructura/js',['js'=>'cliente_proveedor/cliente_proveedor.js']);?>
	</body>
	<?php 
		switch ($pestania) {
			case '1': if($privilegio[0]->cl1r==1){ $activo='cliente'; $search="cliente_proveedor/search_cliente"; $view="cliente_proveedor/view_cliente"; break;}
			case '2': if($privilegio[0]->cl2r==1){ $activo='proveedor'; $search="cliente_proveedor/search_proveedor"; $view="cliente_proveedor/view_proveedor"; break;}
			case '3': if($privilegio[0]->cl3r==1){ $activo='config'; $search=""; $view="cliente_proveedor/view_config"; break;}
		}
	?>
	<script type="text/javascript">activar('<?php echo $activo;?>'); get_2n('<?php echo $search; ?>',{},'search',false,'<?php echo $view; ?>',{},'contenido',true); </script>
</html>