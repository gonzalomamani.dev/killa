<?php 
	$materiales=json_decode($materiales);
	$contPagina=1;
	$contReg=0;
	$url=base_url().'libraries/img/materiales/miniatura/';

?>
<div class="pagina table-responsive">
	<?php $this->load->view('estructura/print/encabezado',['titulo'=>'REGISTRO DE MATERIALES','pagina'=>$contPagina,'sub_titulo'=> '']);?>
	<table class="tabla tabla-border-true">
		<tr class="fila">
				<?php $cols=0;?>
				<?php if(!isset($v1)){?><th class="celda th" width="5%" >#Item</th><?php $cols++;} ?>
				<?php if(!isset($v2)){?><th class="celda th" width="6%" >Fotografía</th><?php $cols++;} ?>
				<?php if(!isset($v3)){?><th class="celda th" width="8%">Código</th><?php $cols++;} ?>
				<?php if(!isset($v4)){?><th class="celda th" width="25%">Nombre</th><?php $cols++;} ?>
				<?php if(!isset($v5)){?><th class="celda th" width="10%">Grupo</th><?php $cols++;} ?>
				<?php if(!isset($v6)){?><th class="celda th" width="10%">Color</th><?php $cols++;} ?>
				<?php if(!isset($v7)){?><th class="celda th" width="7%" >Cant.</th><?php $cols++;} ?>
				<?php if(!isset($v8)){?><th class="celda th" width="7%" >C/U (Bs.)</th><?php $cols++;} ?>
				<?php if(!isset($v9)){?><th class="celda th" width="7%" >Total (Bs.)</th><?php } ?>
				<?php if(!isset($v10)){?><th class="celda th" width="15%">Observaciónes</th><?php } ?>
		</tr>
	<?php $cont=0; $sub_total=0; $total=0;
			foreach ($materiales as $key => $material) { 
				$img='default.png';
				if($material->fotografia!=NULL && $material->fotografia!=""){ $img=$material->fotografia;}
		?>
			<?php 
				if($contReg>=$nro){
					$contReg=0;
					$contPagina++;

			?>
			<tr class="fila">
				<?php if($cols>0){?><th class="celda th" colspan="<?php echo $cols;?>">Sub Total: </th><?php } ?>
				<?php if(!isset($v9)){?><th class="celda th"><?php echo $sub_total;?></th><?php } ?>
				<?php if(!isset($v10)){?><th class="celda th"></th><?php } ?>
			</tr>
			<tr class="fila">
				<?php if($cols>0){?><th class="celda th" colspan="<?php echo $cols;?>">TOTAL: </th><?php } ?>
				<?php if(!isset($v9)){?><th class="celda th"><?php echo $total;?></th><?php } ?>
				<?php if(!isset($v10)){?><th class="celda th"></th><?php } ?>
			</tr>
		</table><!--cerramos tabla-->
	</div><!--cerramos pagina-->
<div class="pagina table-responsive">
	<?php $this->load->view('estructura/print/encabezado',['titulo'=>'REGISTRO DE MATERIALES','pagina'=>$contPagina,'sub_titulo'=> '']);?>
	<table class="tabla tabla-border-true">
		<tr class="fila">
				<?php $cols=0;?>
				<?php if(!isset($v1)){?><th class="celda th" width="5%" >#Item</th><?php $cols++;} ?>
				<?php if(!isset($v2)){?><th class="celda th" width="6%" >Fotografía</th><?php $cols++;} ?>
				<?php if(!isset($v3)){?><th class="celda th" width="8%">Código</th><?php $cols++;} ?>
				<?php if(!isset($v4)){?><th class="celda th" width="25%">Nombre</th><?php $cols++;} ?>
				<?php if(!isset($v5)){?><th class="celda th" width="10%">Grupo</th><?php $cols++;} ?>
				<?php if(!isset($v6)){?><th class="celda th" width="10%">Color</th><?php $cols++;} ?>
				<?php if(!isset($v7)){?><th class="celda th" width="7%" >Cant.</th><?php $cols++;} ?>
				<?php if(!isset($v8)){?><th class="celda th" width="7%" >C/U (Bs.)</th><?php $cols++;} ?>
				<?php if(!isset($v9)){?><th class="celda th" width="7%" >Total (Bs.)</th><?php } ?>
				<?php if(!isset($v10)){?><th class="celda th" width="15%">Observaciónes</th><?php } ?>
		</tr>
		<?php $sub_total=0;}//end if ?>
		<tr class="fila">
			<?php if(!isset($v1)){?><td class="celda td"><?php echo $cont+1;$cont++;?></td><?php } ?>
			<?php if(!isset($v2)){?><td class="celda td"><img src="<?php echo $url.$img;?>" style='width:100%'></td><?php } ?>
			<?php if(!isset($v3)){?><td class="celda td"><?php echo $material->codigo;?></td><?php } ?>
			<?php if(!isset($v4)){?><td class="celda td"><?php echo $material->nombre;?></td><?php } ?>
			<?php if(!isset($v5)){?><td class="celda td"><?php echo $material->nombre_g;?></td><?php } ?>
			<?php if(!isset($v6)){?><td class="celda td"><?php echo $material->nombre_c;?></td><?php } ?>
			<?php if(!isset($v7)){?><td class="celda td"><?php if($material->cantidad>0){ echo $material->cantidad." ".$material->abreviatura; } ?></td><?php } ?>
			<?php if(!isset($v8)){?><td class="celda td"><?php if($material->costo_unitario>0){ echo $material->costo_unitario; } ?></td><?php } ?>
			<?php if(!isset($v9)){?><td class="celda td"><?php if(($material->cantidad*$material->costo_unitario)>0){ echo ($material->cantidad*$material->costo_unitario); $sub_total+=($material->cantidad*$material->costo_unitario); $total+=($material->cantidad*$material->costo_unitario);} ?></td><?php } ?>
			<?php if(!isset($v10)){?><td class="celda td"><?php echo $material->observaciones;?></td><?php } ?>
		</tr>
	<?php $contReg++;}// end for ?>
			<tr class="fila">
				<?php if($cols>0){?><th class="celda th" colspan="<?php echo $cols;?>">Sub Total: </th><?php } ?>
				<?php if(!isset($v9)){?><th class="celda th"><?php echo $sub_total;?></th><?php } ?>
				<?php if(!isset($v10)){?><th class="celda th"></th><?php } ?>
			</tr>
			<tr class="fila">
				<?php if($cols>0){?><th class="celda th" colspan="<?php echo $cols;?>">TOTAL: </th><?php } ?>
				<?php if(!isset($v9)){?><th class="celda th"><?php echo $total;?></th><?php } ?>
				<?php if(!isset($v10)){?><th class="celda th"></th><?php } ?>
			</tr>
	</table>
</div>
<script type="text/javascript"> $(".nroPagina").html(<?php echo $contPagina;?>); </script>