<?php
	$help1='title="<h4>Almacen<h4>" data-content="Seleccione el almacen donde desea buscar el material, si no selecciona ningun almacen se mostrara por defectos todos los materiales de todos los almacenes"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover" ';?>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="new_material()">Crear Material</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="form_add_material()">Add Material Existente</a></li>
</ul>
<div class="row row-not-border">
	<div class="col-sm-5 col-sm-offset-7 col-xs-12">
		<div class="input-group col-xs-12">
			<select class="form-control input-sm" id="alm" onchange="search_add_material()">
				<option value="">Seleccione...</option>
					<?php for ($i=0; $i < count($almacenes) ; $i++) { $almacen=$almacenes[$i]; ?>
						<option value="<?php echo $almacen->ida;?>"><?php echo $almacen->nombre;?></option>
					<?php } ?>
			</select>
			<span class="input-group-addon" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div>
</div>
<div class="row-border" id="contenido_2"></div>
<script language='javascript'>Onfocus("alm");$('[data-toggle="popover"]').popover({html:true});</script>