<?php 
	$url=base_url().'libraries/img/materiales/miniatura/';
	$help1='title="<h4>Imposible quitar el material<h4>" data-content="Verifique que la cantidad de material en el almacen sea cero, y vuelva a intentarlo"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover" ';

?>
<div class="">
	<table class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th class="g-thumbnail-modal">#Item</th>
				<th class="celda-sm-10">Codigo</th>
				<th class="celda-sm-30">Nombre</th>
				<th style="width:30%" class="celda-sm">Almacen</th>
				<th style="width:14%" class="celda-sm">Color</th>
				<th style="width:6%"></th>
			</tr>
		</thead>
		<tbody>
		<?php for ($i=0; $i < count($materiales); $i++) { $material=$materiales[$i]; 
				$img="default.png";
				if($material->fotografia!="" && $material->fotografia!=null){ $img=$material->fotografia;}
				$almacen=$this->M_almacen->get($material->ida);
			?>
			<tr>
				<td class="g-thumbnail-modal"><div id="item"><?php echo $i+1;?></div><img src="<?php echo $url.$img;?>" class='img-thumbnail' width='100%'></td>
				<td ><?php echo $material->codigo;?></td>
				<td ><?php echo $material->nombre;?></td>
				<td class="celda-sm"><?php echo $almacen[0]->nombre;?></td>
				<td class="celda-sm">
				<ul class="list-group" style="font-size:10px; padding: 1px">
					<li class="list-group-item" style="padding-top: 5px;padding-bottom: 5px;color:white; background:<?php echo $material->codigo_c;?>"><?php echo $material->nombre_c; ?></li>
				</ul>
				</td>
				<?php $control=$this->M_almacen_material->get_row_2n('ida',$ida,'idm',$material->idm);?>
				<td>
					<?php if(!empty($control)){ 
							if($control[0]->cantidad>0){ ?>
							<div class="btn-circle btn-circle-red btn-circle-30" <?php echo $popover.$help1;?> ></div>
					<?php }else{ ?>
							<div class="btn-circle btn-circle-red btn-circle-30" onclick="add_material_almacen('<?php echo $material->idm;?>','<?php echo $ida;?>',this,'red','30');"></div>
					<?php } ?>
						
					<?php }else{ ?>
							<div class="btn-circle btn-circle-default btn-circle-30" onclick="add_material_almacen('<?php echo $material->idm;?>','<?php echo $ida;?>',this,'red','30');"></div>
					<?php } ?>
				</td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
</div>
<script language='javascript'>$('[data-toggle="popover"]').popover({html:true});</script>