	<table class="tabla tabla-border-false">
	<tr class='fila'>
		<td class='celda g-thumbnail'><div class="g-img"></div></td>
		<td class='celda celda-sm-10'>
			<form onsubmit="return view_material()"><input class='form-control input-sm' type="search" id="search_cod" placeholder='Código' onkeyup="reset_all_view(this.id)"></form>
		</td>
		<td class='celda celda-sm-30'>
			<form onsubmit="return view_material()"><input class='form-control input-sm' type="search" id="search_nom" placeholder='Nombre de Material' onkeyup="reset_all_view(this.id)"/></form>
		</td>
		<td class='celda-sm' width="11%">
			<select class='form-control input-sm' id="search_col" onchange="reset_all_view(this.id); view_material();">
				<option value="">Color - Seleccionar</option>
				<?php for($i=0; $i<count($color);$i++){$res=$color[$i];?>
					<option value="<?php echo $res->idco; ?>"><?php echo $res->nombre;?></option>	
				<?php } ?>
			</select>
		</td>
		<td class='celda-sm' width="11%">
			<select class='form-control input-sm' id="search_gru" onchange="reset_all_view(this.id); view_material();">
				<option value="">Grupo - Seleccione</option>
				<?php for($i=0; $i<count($grupo);$i++){$res=$grupo[$i];?>
					<option value="<?php echo $res->idg; ?>"><?php echo $res->nombre;?></option>	
				<?php } ?>
			</select>
		</td>
		<td class='celda-sm' width="10%">
			<form onsubmit="return view_material()"><input class='form-control input-sm' type="number" id="search_can" placeholder='Cantidad' onkeyup="reset_all_view(this.id)"></form>
		</td>
		<td class='celda-sm' width="55%"></td>

		<td class="celda" width="13%">
			<?php $this->load->view('estructura/botones/buscador',['f_buscar'=>"view_material()",'f_ver'=>"all_material()",'nuevo'=>'Nuevo','f_nuevo'=>'new_material()','f_imprimir'=>'imprimir()']);?>
		</td>	
		
	</tr>		
	</table>
<script type="text/javascript">Onfocus('search_cod');</script>