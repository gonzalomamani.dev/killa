<?php 
	$j_materiales=json_encode($materiales);
	$url=base_url().'libraries/img/materiales/';
?>
<table class="table table-bordered table-striped table-hover">
	<thead>
    <tr>
    	<th class='celda g-thumbnail'><div class="g-img">#Item</div></th>  
	    <th class='celda-sm-10'>Código</th>  
		<th class='celda celda-sm-30'>Nombre</th>
		<th class='celda-sm' width='9%'>Color</th>
		<th class='celda-sm' width='11%'>Grupo</th>
		<th width='9%'>Cantidad</th>
		<th class='celda-sm' width='61%'>Descripcion</th>
		<td width='10%'></td>
    </tr>
    </thead>
    <tbody>
    <?php 
    if(count($materiales)>0){
    for($i=0;$i<count($materiales);$i++){ $res=$materiales[$i];?>
    <tr>
    	<td>
    		<?php 
			$img="default.png";
				if($res->fotografia!="" & $res->fotografia!=NULL){
					$img=$res->fotografia;
				}
			?>
			<div id="item"><?php echo $i+1;?></div>
			<a href="javascript:" onclick="fotografia('<?php echo $url.$img;?>')"><img src="<?php echo $url.'miniatura/'.$img;?>" class='img-thumbnail' width='100%'></a>
    	</td>  
	    <td><?php echo $res->codigo; ?></td>  
		<td><?php echo $res->nombre; ?></td>
		<td class='celda-sm'>
			<ul class="list-group" style="font-size:10px; padding: 1px">
				<li class="list-group-item" style="padding-top: 5px;padding-bottom: 5px;color:white; background:<?php echo $res->codigo_c;?>"><?php echo $res->nombre_c; ?></li>
			</ul>
		</td>
		<td class='celda-sm'><?php echo $res->nombre_g; ?></td>
		<td><span style="font-size: .85em;" class="label <?php if($res->cantidad>=90){ echo 'label-success';}else{ if($res->cantidad>=10 && $res->cantidad<=89){ echo 'label-warning';}else{echo 'label-danger';}}?> label-lg"><?php echo $res->cantidad."  ".$res->abreviatura."."; ?></span></td>
		<td class='celda-sm'><?php echo $res->observaciones;?></td>
		<td>
			<?php $this->load->view('estructura/botones/botones_registros',['reportes'=>"reportes_material('".$res->idmi."')",'configuracion'=>"configuracion_material('".$res->idmi."')",'eliminar'=>"confirmar_material('".$res->idmi."','".$res->nombre."','".$img."')"]);?>
		</td>
    </tr>
	<?php }}else{
		echo "<tr><td colspan='8'><h2>0 registros encontrados...</h2></td></tr>";
	} ?>
</tbody></table>
<script language="javascript" type="text/javascript"> 
	$("#print").removeAttr("onclick"); 
	$("#print").unbind("click"); 
	$("#print").click(function(){ imprimir_materiales('<?php echo $j_materiales;?>'); }); 
</script>