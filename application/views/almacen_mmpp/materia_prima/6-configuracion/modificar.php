<?php 
	$help1='title="<h4>Subir Fotografía</h4>" data-content="Seleccione una fotografía, preferiblemente una imagen que no sobrepase las dimenciones de <strong>700x700[px]</strong>, para evitar sobre cargar al sistema"';
	$help2='title="<h4>Ingresar nombre de material<h4>" data-content="Ingrese un Nombre alfanumerico de 3 a 200 caracteres puede incluir espacios, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help3='title="<h4>Ingresar codigo de material<h4>" data-content="Ingrese un Código alfanumerico de 2 a 15 caracteres <b>sin espacios</b>, ademas el codigo solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help4='title="<h4>Grupo de material<h4>" data-content="Seleccione el grupo del material, El valor vacio no es aceptado, si desea adicionar un nuevo grupo lo puede hacer en el menu superior (Configuracion)"';
	$help5='title="<h4>Color de material<h4>" data-content="Seleccione el color del material, El valor vacio no es aceptado, si desea adicionar un nuevo color lo puede hacer en el menu superior (Configuracion)"';
	$help6='title="<h4>Unidad de medida de material<h4>" data-content="Seleccione una unidad de medida del material, El valor vacio no es aceptado, si desea adicionar una nueva unidad de material lo puede hacer en el menu superior (Configuracion)"';
	$help7='title="<h4>Descripcion del material<h4>" data-content="Ingrese un descripcion alfanumerica de 0 a 700 caracteres puede incluir espacios, ademas la descripcion solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help75='title="<h4>Costo promedio por unidad<h4>" data-content="El costo es calculado automaticamente por el sistema, representa a el costo promedio de los costos unitarios de cada proveedor, si desea modificar el costo promedio debe modificar el costo por unidad segun el proveedor en el menu superior (Proveedores->configuracion)"';
	if(!empty($proveedores)){
		$help8='title="<h4>Proveedor<h4>" data-content="Seleccione el proveedor del material, en caso de no tener un proveedor puede dejar el campo vacio, si desea adicionar mas de un proveedor lo puede hacer en el menu superior (Proveedores)"';
		$help9='title="<h4>Costo por unidad<h4>" data-content="Ingrese un costo por unidad de medidad segun el proveedor, si desea adicionar mas de un costo por unidad de material lo puede hacer en el menu superior (Proveedores->configuracion)"';
	}else{
		$help8='title="<h4>Proveedor<h4>" data-content="Imposible modificar el proveedor, es imposible que el material tenga mas de 2 proveedor, para modificar los proveedores en el material debe realizarlo en la seccion de proveedores en el menu superior."';
		$help9='title="<h4>Costo por unidad<h4>" data-content="Imposible modificar el costo por proveedor, es imposible que el material tenga mas de 2 proveedor, para modificar los costo por proveedor en el material debe realizarlo en la seccion de proveedores en el menu superior."';
	}	
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover" ';
$material=$material[0];?>
<div class="row">
	<div class="col-sm-3 col-sm-offset-9 col-xs-12"><strong><span class='text-danger'>(*)</span> Campo obligatorio</strong></div>
	<div class="col-sm-2 col-xs-12"><strong>Fotografía: </strong></div>
	<div class="col-sm-10 col-xs-12">
		<div class="input-group col-xs-12">
			<input class='form-control input-xs' type="file" id="fot">
			<span class="input-group-addon input-sm" <?php echo $popover.$help1;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Nombre:</strong></div>
	<div class="col-sm-10 col-xs-12">
		<form onsubmit="return update_material('<?php echo $idmi;?>')">
			<div class="input-group col-xs-12">
				<input class='form-control input-xs' type="text" id="nom" placeholder='Nombre de Material' value="<?php echo $material->nombre;?>">
				<span class="input-group-addon input-sm" <?php echo $popover.$help2;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Código:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<form onsubmit="return update_material('<?php echo $idmi;?>')">
			<div class="input-group col-xs-12">
				<input class='form-control input-xs' type="text" id="cod" placeholder='Código' value="<?php echo $material->codigo;?>">
				<span class="input-group-addon input-sm" <?php echo $popover.$help3;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Grupo:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<div class="input-group col-xs-12">
			<select class='form-control input-xs' id="gru">
				<option value="">Seleccione...</option>
			<?php
			for ($i=0; $i < count($grupo) ; $i++){ $g=$grupo[$i]; 
				?>
				<option value="<?php echo $g->idg;?>" <?php if($g->idg==$material->idg){ echo "selected";}?>><?php echo $g->nombre;?></option>
				<?php
			}
			?>
			</select>
			<span class="input-group-addon input-sm" <?php echo $popover.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Color:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<div class="input-group col-xs-12">
			<select class='form-control input-xs' id="col">
				<option value="">Seleccione...</option>
			<?php
			for ($i=0; $i < count($color);$i++){ $c=$color[$i]; 
				?>
				<option value="<?php echo $c->idco;?>" <?php if($c->idco==$material->idco){ echo "selected";} ?>><?php echo $c->nombre;?></option>
				<?php
			}
			?>
			</select>
			<span class="input-group-addon input-sm" <?php echo $popover.$help5;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'>(*)</span> Unidad de medida:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<div class="input-group col-xs-12">
			<select class='form-control input-xs' id="med">
				<option value="">Seleccione...</option>
			<?php
			for ($i=0; $i < count($unidad);$i++){ $u=$unidad[$i]; 
				?>
				<option value="<?php echo $u->idu;?>" <?php if($u->idu==$material->idu){ echo "selected";} ?>><?php echo $u->nombre;?></option>
				<?php
			}
			?>
			</select>
			<span class="input-group-addon input-sm" <?php echo $popover.$help6;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong>Descripción</strong></div>
	<div class="col-sm-10 col-xs-12">
		<div class="input-group col-xs-12">
			<textarea class="form-control input-xs" id="des" placeholder='Descripción del material'><?php echo $material->observaciones;?></textarea>
			<span class="input-group-addon input-sm" <?php echo $popover.$help7;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div><i class="clearfix"></i>
	<div class="col-sm-2 col-xs-12"><strong> Costo promedio por unidad<?php echo '['.$material->abreviatura.']';?> (Bs.): </strong></div>
	<div class="col-sm-4 col-xs-12">
		<div class="input-group col-xs-12">
			<input class='form-control input-xs' type="text" id="cod" placeholder='Código' value="<?php echo $material->costo_unitario;?>" disabled>
			<span class="input-group-addon input-sm" <?php echo $popover.$help75;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12"><strong><span class='text-danger'></span></strong></div>
	<div class="col-sm-4 col-xs-12"></div><i class='clearfix'></i>
	<div class="col-sm-2 col-xs-12"><strong>Proveedor:</strong></div>
	<div class="col-sm-4 col-xs-12">
		<div class="input-group col-xs-12">
			<select class='form-control input-xs' id="pro" <?php if(empty($proveedores)){ echo "disabled";}?>>
				<option value="">Seleccione...</option>
				<?php
				if(!empty($proveedores)){
					for ($i=0; $i<count($proveedores); $i++) { $prov=$proveedores[$i];
						?>
							<option value="<?php echo $prov->idpro;?>" <?php if(!empty($proveedor)){ if($proveedor->idpro==$prov->idpro){ echo "selected";}}?>><?php echo $prov->nombre;?></option>
						<?php
					}
				}
				?>
			</select>
			<span class="input-group-addon input-sm" <?php echo $popover.$help8;?>><i class='glyphicon glyphicon-info-sign'></i></span>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12"><strong>Costo unitario s/Proveedor (Bs.):</strong></div>
	<div class="col-sm-4 col-xs-12">
		<form onsubmit="return update_material('<?php echo $idmi;?>')">
			<div class="input-group col-xs-12">
				<input class='form-control input-xs' type="number" placeholder='Costo' min='0' <?php if(empty($proveedores)){ echo "disabled";}else{ echo " id='cos_pro'"; if(!empty($proveedor)){ echo "value='".$proveedor->costo_unitario."'";}}?>>
				<span class="input-group-addon input-sm" <?php echo $popover.$help9;?>><i class='glyphicon glyphicon-info-sign'></i></span>
			</div>
		</form>
	</div><i class="clearfix"></i>	
</div>
<script language='javascript'>Onfocus("nom");$('[data-toggle="popover"]').popover({html:true});</script>