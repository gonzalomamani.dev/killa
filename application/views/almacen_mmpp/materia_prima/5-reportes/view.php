<?php 

$material=$material[0]; 
$url=base_url().'libraries/img/materiales/miniatura/';
$img="default.png";
	if($material->fotografia!="" && $material->fotografia!=""){
		$img=$material->fotografia;
	}
?>
<div class="row">
	<div class="col-sm-3 col-xs-12 text-center"><img src="<?php echo $url.$img;?>" class="img-thumbnail" alt=""><hr></div>
	<div class="col-sm-9 cols-xs-12 table-responsive">
	<table border="0" class="tabla tabla-border-true">
		<tr class="fila">
			<th class="celda th">Código:</th>
			<td class="celda td" colspan="3"><?php echo $material->codigo;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Nombre:</th><td class="celda td" colspan="3"><?php echo $material->nombre;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th" width="17.5%">Grupo:</th><td class="celda td" width="17.5%"><?php echo $material->nombre_g;?></td>
			<th class="celda th" width="17.5%">Cantidad:</th><td class="celda td" width="17.5%"><?php echo $material->cantidad;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th" width="17.5%">Unidad de medida:</th><td class="celda td" width="17.5%"><?php echo $material->medida;?></td>
			<th class="celda th" width="17.5%">Color:</th><td class="celda td" width="17.5%"><?php echo $material->nombre_c;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Costo promedio por unidad (Bs.):</th><td class="celda td" colspan="3"><?php echo $material->costo_unitario;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Descripción:</th><td class="celda td" colspan="3"><?php echo $material->observaciones;?></td>
		</tr>
	</table>
	</div>
</div>