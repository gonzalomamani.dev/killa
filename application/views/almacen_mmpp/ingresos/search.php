<div class="table-responsive">
<table class="tabla tabla-border-false">
	<tr class='fila'>
		<td class='celda-sm'><div class="g-img-modal"></div></td>
		<td class='celda' style="width:24%">
		<form onsubmit="return view_ingreso();">
			<input class='form-control input-sm' type="search" id="search_nom" placeholder='Nombre de material' onkeyup="reset_all_view(this.id)" onclick="reset_all_view(this.id)"/>
		</form>
		</td>
		<td class='celda' style="width:12%">
			<form onsubmit="return view_ingreso();">
				<input class='form-control input-sm' type="number" id="search_can" placeholder='Stock' onkeyup="reset_all_view(this.id)" onclick="reset_all_view(this.id)" min='0'>
			</form>
		</td>
		<td class='celda-sm' style="width:10%">
			<select class='form-control input-sm' id="search_uni" onchange="reset_all_view(this.id);view_ingreso();">
				<option value="">Unidad - Seleccionar</option>
				<?php for($i=0; $i<count($unidad);$i++){$res=$unidad[$i];?>
					<option value="<?php echo $res->idu; ?>"><?php echo $res->nombre;?></option>	
				<?php } ?>
			</select>
		</td>
		<td class='celda-sm' style="width:49%"></td>
		<td class='celda' style="width:5%">
			<?php $this->load->view('estructura/botones/buscador',['f_buscar'=>"view_ingreso()",'f_ver'=>"all_ingreso()",'nuevo'=>'','f_nuevo'=>'','f_imprimir'=>'']);?>
		</td>
	</tr>
</table>
</div>
<script type="text/javascript">Onfocus('search_nom');</script>