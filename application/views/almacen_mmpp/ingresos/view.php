<?php date_default_timezone_set("America/La_Paz");
	$url=base_url().'libraries/img/materiales/';
	$popover3='data-toggle="popover" data-placement="left" data-trigger="hover" title="<h4>Observaciónes<h4>" data-content="la observacion puede poseer un formato alfanumerico de 0 a 300 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas la observacion solo acepta los siguientes caracteres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº)<b>"';
?>
<table class="table table-bordered table-striped table-hover">
<thead>
	<tr>
		<th class='celda g-thumbnail'><div class="g-img-modal">#Item</div></th>
		<th width="26%">Nombre</th>
		<th width="12%">Stock actual</th>
		<th width="16%">Fecha de ingreso</th>
		<th width="8%" >Cantidad</th>
		<th width="35%">Observaciónes</th>
		<th width="3%" ></th>
	</tr>
</thead><tbody>
<?php 
if(count($materiales)>0){
	for($i=0;$i<count($materiales);$i++){ $res=$materiales[$i];?>
	<tr>
		<td><?php 
			$fotografia="default.png";
				if($res->fotografia!="" && $res->fotografia!=NULL){
					$fotografia=$res->fotografia;
				}
			?>
			<div id="item"><?php echo $i+1;?></div>
			<a href="javascript:" onclick="fotografia('<?php echo $url.$fotografia;?>')"><img src="<?php echo $url.'miniatura/'.$fotografia;?>" class='img-thumbnail' width='100%'></a>
		</td>
		<td><?php echo $res->nombre; ?></td>
		<td>
			<div class="input-group input-120">
				<input type="text" id="c<?php echo $res->idam;?>" class="form-control input-sm" value="<?php echo $res->cantidad; ?>" disabled>
				<span class="input-group-addon input-sm"><?php echo $res->abreviatura."."; ?></span>
			</div>
		</td>
		<td><input type="datetime-local" id="fech<?php echo $res->idam;?>" value="<?php echo str_replace(' ', 'T', date('Y-m-d H:i'));?>" class="form-control input-sm"></td>
		<td>
			<div class="input-group input-120">
				<form onsubmit="return save_movimiento('<?php echo $res->idam;?>','i')">
					<input type="number" id="can<?php echo $res->idam;?>" class="form-control input-sm input-100" placeholder="0" min='0' step="any" min='0' max="999999999.9999999">
				</form>
				<span class="input-group-addon input-sm"><?php echo $res->abreviatura."."; ?></span>
			</div>
		</td>
		<td>
			<div class="input-group">
				<textarea id="obs<?php echo $res->idam;?>" class="form-control input-sm" placeholder="Observaciónes de ingreso de material"></textarea>	
				<span class="input-group-addon input-sm" <?php echo $popover3;?>><span class='glyphicon glyphicon-info-sign'></span></span>
			</div>
		</td>
		<td><?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"save_movimiento('".$res->idam."','i')"]);?></td>
	</tr>
<?php }}else{
	echo "<tr><td colspan='10'><h2>0 registros encontrados...</h2></td></tr>";
} ?>
</tbody>
</table>
<script language='javascript'>Onfocus("new_nit");$('[data-toggle="popover"]').popover({html:true});</script>