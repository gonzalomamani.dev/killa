<?php
	$cargo=$this->session->userdata("cargo");
	if($this->session->userdata("nombre2")!=""){
		$usuario=$this->session->userdata("nombre")." ".$this->session->userdata("nombre2")." ".$this->session->userdata("paterno");
	}else{
		$usuario=$this->session->userdata("nombre")." ".$this->session->userdata("paterno");
	}
    $ci=$this->session->userdata("ci");
?>					
<!DOCTYPE html>
<html lang="es" class="no-js">
	<head><?php $this->load->view('estructura/head',['title'=>'Almacen','css'=>'']);?></head>
	<body>

		<?php $this->load->view('estructura/modal');?>
		<div class="contenedor">
			<?php $this->load->view('estructura/menu_izq',['usuario'=>$usuario,'cargo'=>$cargo,'ventana'=>'almacen','privilegio'=>$privilegio[0]]);?>
			<?php $v_menu="";
				if($privilegio[0]->al1r==1){ $v_menu.="Almacenes/ver/icon-office|"; }
				if($privilegio[0]->al2r==1){ $v_menu.="Movimientos de Material/hist/glyphicon glyphicon-stats|";}
				if($privilegio[0]->al3r==1){ $v_menu.="Movimientos de Producto/hist2/icon-graph";}
			?>
			<?php $this->load->view('estructura/menu_top',['usuario'=>$usuario,'cargo'=>$cargo,'menu'=>$v_menu]);?>
			<div id="search"></div>
			<div id="contenido"></div>
		</div>
		<?php $this->load->view('estructura/js',['js'=>'almacen/almacen.js']);?>
	</body>
<?php switch($pestania){
			case '1': if($privilegio[0]->al1r==1){ $title="Almacenes"; $activo='ver'; $search="almacen/view_search_almacen"; $view="almacen/view_almacen";} break;
			case '2': if($privilegio[0]->al2r==1){ $title="Historial de movimiento de materiales"; $activo='hist'; $search="almacen/search_movimiento_material"; $view="almacen/view_movimiento_material";} break;
			case '3': if($privilegio[0]->al3r==1){ $title="Historial de movimiento de productos"; $activo='hist2'; $search="almacen/view_search_movimiento_producto"; $view="almacen/view_movimiento_producto";} break;
	} ?>
	<script type="text/javascript">activar('<?php echo $activo;?>','<?php echo $title;?>','almacen?p=<?php echo $pestania;?>'); get_2n('<?php echo $search; ?>',{},'search',false,'<?php echo $view; ?>',{},'contenido',true); </script>
</html>